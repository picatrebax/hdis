#! /usr/bin/env python
#
# Copyright (c) 2006-2013 UAF,
# Author Anton Kulchitsky  mailto:atoku0@gmail.com
#

import sys, os, tempfile, shutil
from optparse import OptionParser
from string import Template
import datetime

from config import *

# file templates
CONFIGURE_AC_TPL = "configure.ac.tpl"
DOXYGEN_TPL = "src/Doxygen.tpl"
CONFIG_LD_TPL = "config.ld.tpl"
HDIS_CONFIG_SH = "hdis-config.sh.tpl"

#-------------------------------------------------------------------------

def info():
    s = '''\
    This is a bootstrapping script for %s program.

    Copyright (C) 2010-2013 UAF, 
                  designed by Anton Kulchitsky

    It is intended to prepare all sources and documentation.
    To run it successfully; you need installed fresh enough:

        Python, GNU Autotools

    '''  % (NAME)
    sys.stdout.write( s )


def addsuffix( s, sfx ):
    '''
    To each object in string s adds suffix sfx. For example:
    addsuffix( "one two three", "c" ) will return
    "one.c two.c three.c"
    '''
    return ' '.join ( [ '%s.%s' % (obj,sfx) for obj in s.split() ] )


def getoptions():
    '''
    Read all user options
    '''
    parser = OptionParser()


    parser.add_option("-q", "--quiet", dest="quiet", default=False,
                      action="store_true",
                      help="Suppress verbose output (default=yes)" )
    parser.add_option("-X", "--hdis-only",dest="hdis_only", default=False,
                      action="store_true",
                      help="Do not generate scripts for hdisD and hdisC" )
    parser.add_option("-D", "--doc-only",dest="doc_only", default=False,
                      action="store_true",
                      help="Update only documentation files" )

    return parser.parse_args()

def inform( s, opts ):
    '''
    Outputs the string to the stdout, if there is verbose output only
    '''
    if not opts.quiet:
        sys.stdout.write( "    %s\n" % s )

def makechangelog( options ):
    # generate Changelog
    cmd = "git log . > ChangeLog"
    inform( cmd, options )
    os.system( cmd )

    cmd = "git log hdisvis > hdisvis/ChangeLog"
    inform( cmd, options )
    os.system( cmd )

    cmd = "git log hdisdesign > hdisdesign/ChangeLog"
    inform( cmd, options )
    os.system( cmd )

def makeautotools( options, extopts="" ):
    '''
    Prepare autotool files and run GNU Autotools to generate configure
    '''

    # We are ready to run autotools
    os.system("autoreconf -v -i --no-recursive %s" % extopts)

def makedate( options ):
    '''
    Creates file doc/date.tex where defines LaTeX commands:
    \hdisdate  for range of dates of HDIS update
    \lasthdisdate   for the last date program was updated
    \branchname    for the current branch name
    \hdisversion   for the current version
    '''

    inform( 'create date records, versions etc. for developer docs', 
            options )

    # we generate the date file in docs
    # the date based on 'git log' command
    # we parse the date out of 'last date'
    git_info = os.popen( "git log -1 --date=iso src/*.c src/*.h doc/*.tex" )
    date_str = [s[8:8+10] for s in git_info.readlines() \
                if s[0:5] == 'Date:' ][0]

    thedate = tuple( map( int, date_str.split( '-' ) ) )
    maindate = datetime.date( *thedate )
    maindate_str = maindate.strftime("%B %d, %Y")

    # add branch name to the date.tex file
    git_branch = os.popen( "git branch" )
    options.current_branch_str = [ s for s in git_branch.readlines() \
                                   if '*' in s ][0][1:].strip()

    git_revision = os.popen( "git rev-parse HEAD" )
    options.current_revision = git_revision.readlines()[0].strip()

    fdate = open( "doc/date.tex", "w" )
    fdate.write( '%% bootstrap -d generated:\n'\
                 '\\newcommand{\\hdisdate}{June 04, 2013 -- %s}\n' %\
                 maindate_str )
    fdate.write( '\\newcommand{\\lasthdisdate}{%s}\n' % maindate_str )
    fdate.write( '\\newcommand{\\branchname}{%s}\n' % options.current_branch_str )
    fdate.write( '\\newcommand{\\hdisversion}{%s}\n' % VERSION )
    fdate.close()

def create_configure_ac( options, dirname ):
    '''
    Creates confiugure.ac files from the template and fills the gaps
    with current version etc.
    '''
    cnfac = open( dirname + '/' + CONFIGURE_AC_TPL ).read()
    cnfac = Template( cnfac ).safe_substitute( NAME =        NAME,
                                               VERSION =     VERSION,
                                               MAINPROGRAM = MAINPROGRAM,
                                               EMAIL =       EMAIL,
                                               BRANCH =      options.current_branch_str )
    fp = open( 'configure.ac', 'w' )
    inform( 'create configure.ac', options )
    fp.write( cnfac )
    fp.close()

def create_config_ld( options, dirname ):
    '''
    Creates lua/hdis/config.ld file from the template and fills the
    gaps with current version etc.

    '''
    cnfac = open( dirname + '/' + CONFIG_LD_TPL ).read()
    cnfac = Template( cnfac ).safe_substitute( NAME =        NAME,
                                               VERSION =     VERSION,
                                               MAINPROGRAM = MAINPROGRAM,
                                               EMAIL =       EMAIL )
    fp = open( dirname + '/' + 'config.ld', 'w' )
    inform( 'create ' + dirname + '/config.ld', options )
    fp.write( cnfac )
    fp.close()


def create_doxyfile( options ):
    '''
    Creates Doxygen file from Doxygen.tpl and config.py
    '''
    cnfac = open( DOXYGEN_TPL ).read()
    cnfac = Template( cnfac ).safe_substitute( NAME =        NAME,
                                               VERSION =     VERSION,
                                               BRIEF =       BRIEF,
                                               LOGOPNG =     LOGOPNG,
                                               MAINPROGRAM = MAINPROGRAM,
                                               EMAIL =       EMAIL )
    fp = open( 'src/Doxygen', 'w' )
    inform( 'create src/Doxygen', options )
    fp.write( cnfac )
    fp.close()

def create_hdis_config_sh( options ):
    '''
    Creates the shell script to being able to run the model without env
    '''
    hdis_path = os.path.dirname(os.path.abspath(__file__))

    cnfac = open( HDIS_CONFIG_SH ).read()
    cnfac = Template( cnfac ).safe_substitute(
        HDIS_PATH    = hdis_path,
        NAME         = NAME,
        VERSION      = VERSION,
        BRANCH       = options.current_branch_str,
        REVISION     = options.current_revision,
        COPYRIGHT    = COPYRIGHT )

    fp = open( 'hdis-config.sh', 'w' )
    inform( 'create hdis-config.sh', options )
    fp.write( cnfac )
    fp.close()

def main():

    info()

    sys.stdout.write( "Bootstrapping... Run with --help "
                      "to see different bootstrapping options\n\n" )

    (options, args) = getoptions()

    sys.stdout.write( "* Preparing Documentation\n" )
    makedate( options )
    create_doxyfile( options )
    create_config_ld( options, 'lua/hdis' )

    if options.doc_only: return 0 # we needed only doc update

    sys.stdout.write( "* Creating hdis-config.sh for local run env.\n" )
    create_hdis_config_sh( options )
    
    sys.stdout.write( "* Preparing GNU Autotools\n" )
    dirname = '.'
    create_configure_ac( options, dirname )
    makechangelog( options )

    sys.stdout.write( "* Running GNU Autotools for HDIS\n" )
    makeautotools( options )

    if not options.hdis_only:
        sys.stdout.write( "* Running GNU Autotools for HDISVIS\n" )
        topdir = os.getcwd()
        dirname = "hdisvis"
        os.chdir( topdir + "/" + dirname )
        makeautotools( options, "-I../m4" )
        os.chdir( topdir )
        
        sys.stdout.write( "* Running GNU Autotools for HDISDESIGN\n" )
        topdir = os.getcwd()
        dirname = "hdisdesign"
        os.chdir( topdir + "/" + dirname )
        makeautotools( options, "-I../m4" )
        os.chdir( topdir )

    return 0

if __name__=="__main__":
    sys.exit( main() )

