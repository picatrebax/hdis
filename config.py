NAME      = "HDIS"
BRIEF     = "Hydrokinetic Debris Intreraction Simulator"
LOGOPDF   = "pics/logo.pdf"
LOGOPNG   = "pics/logo.png"
MAINPROGRAM = "hdis"
VERSION   = "1.0.1"
EMAIL     = "anton.kulchitsky@coupi.us"
COPYRIGHT = "Copyright (C) 2010--2019 UAF"

