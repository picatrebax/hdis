#include "shape.hh"

Vec GetNormal(Vec p0, Vec p1, Vec p2)
{
	Vec p01, p02, n;
	Diff(&p01, p1, p0);
	Diff(&p02, p2, p0);
	X(&n, p02, p01);
	Normalize(&n, n);
	return n;
}

Vec Shape::GetNormalToFace(int i)
{
	return GetNormal(points[faces[i][0]], points[faces[i][2]], points[faces[i][1]]);
}

void Shape::Update()
{
	MakeCCW();
	SetSBB();
}

void Shape::SetSBB()
{
	SBB_center = GetInternalPoint();

	double d = 0;
	for (unsigned int i = 0; i < verts.size(); ++i)
	{
		double cd = Dist(points[verts[i][0]], SBB_center);
		if (cd > d) d = cd;
	}
	// expand dil_r a little to give wiggle room for rounding error
	SBB_radius = d + 1.1*dil_r;
}

vector<int> Shape::GetAdjEdges(int vert_i)
{
	vector<int> adj_e;
	for (unsigned int i = 0; i < edges.size(); ++i)
	{
		if (edges[i][0] == vert_i || edges[i][1] == vert_i)
			adj_e.push_back(i);
	}
	return adj_e;
}

int Shape::GetDegree(int vert_i)
{
	return (int)GetAdjEdges(vert_i).size();
}

int Shape::GetLface(int e)
{
	Vec c = GetInternalPoint();
	for (unsigned int f = 0; f < faces.size(); ++f)
	{
		if ((faces[f][0] == edges[e][0] || faces[f][1] == edges[e][0] || faces[f][2] == edges[e][0]) &&
				(faces[f][0] == edges[e][1] || faces[f][1] == edges[e][1] || faces[f][2] == edges[e][1]))
		{
			int index = -1;
			for (int i = 0; i < 3; ++i)
				if (faces[f][i] != edges[e][0] && faces[f][i] != edges[e][1])
					index = faces[f][i];
			Vec to_head;
			Vec to_tail;
			Vec to_point;
			Vec norm;
			Diff(&to_head, points[edges[e][1]], c);
			Diff(&to_tail, points[edges[e][0]], c);
			Diff(&to_point, points[index], c);
			X(&norm, to_tail, to_head);
			if (Dot(to_point, norm) > 0)
				return f;
		}
	}
	wxLogMessage("Couldn't find the left face =/");
	return -1;
}

int Shape::GetRface(int e)
{
	Vec c = GetInternalPoint();
	for (unsigned int f = 0; f < faces.size(); ++f)
	{
		if ((faces[f][0] == edges[e][0] || faces[f][1] == edges[e][0] || faces[f][2] == edges[e][0]) &&
				(faces[f][0] == edges[e][1] || faces[f][1] == edges[e][1] || faces[f][2] == edges[e][1]))
		{
			int index = -1;
			for (int i = 0; i < 3; ++i)
				if (faces[f][i] != edges[e][0] && faces[f][i] != edges[e][1])
					index = faces[f][i];
			Vec to_head;
			Vec to_tail;
			Vec to_point;
			Vec norm;
			Diff(&to_head, points[edges[e][1]], c);
			Diff(&to_tail, points[edges[e][0]], c);
			Diff(&to_point, points[index], c);
			X(&norm, to_tail, to_head);
			if (Dot(to_point, norm) < 0)
				return f;
		}
	}
	wxLogMessage("Couldn't find the right face =/");
	return -1;
}

int Shape::GetEdge(int i, int j)
{
	for (unsigned int n = 0; n < edges.size(); ++n)
	{
		if ((edges[n][0] == i && edges[n][1] == j) || (edges[n][0] == j && edges[n][1] == i))
			return n;
	}
	wxLogMessage(_("Uh oh didn't find the edge I was looking for..."));
	return -1;
}

Shape::Shape(double in_dil_r)
{
	dil_r = in_dil_r;
}

Shape::Shape(double in_dil_r, 
             vector<Vec> in_points, 
             vector< vector<int> > in_faces,
             vector< vector<int> > in_edges,
             vector< vector<int> > in_verts)
{
  dil_r = in_dil_r;
  points = in_points;
  faces = in_faces;
  edges = in_edges;
  verts = in_verts;
}

Vec Shape::GetInternalPoint()
{
  Vec p(0, 0, 0);
  for (unsigned int i = 0; i < points.size(); ++i)
    Sum(&p, p, points[i]);
  Scale(&p, p, 1.0/points.size());
  return p;
}

// this test requires:
//   1) that the shape is convex
//   2) that the point labeled "center" is inside the shape
bool Shape::Inside(Vec p)
{
	/* first we check the point from being within dil_r of each feature */
	for (unsigned int i = 0; i < points.size(); ++i)
	{
		Vec diff;
		Diff(&diff, p, points[i]);
		double d = Length(diff);
		if (d < dil_r)
		{
			return true;
		}
	}

	for (unsigned int i = 0; i < edges.size(); ++i)
	{
		Vec p0 = points[edges[i][0]];
		Vec p1 = points[edges[i][1]];
		if (PointLineDist(p, p0, p1) < dil_r)
		{
			return true;
		}
	}

	for (unsigned int i = 0; i < faces.size(); ++i)
	{
		Vec p0 = points[faces[i][0]];
		Vec p1 = points[faces[i][1]];
		Vec p2 = points[faces[i][2]];
		Vec n = GetNormal(p0, p1, p2);

		if (PointInTriangle(p, n, p0, p1, p2))
		{
			Vec to_plane;
			Diff(&to_plane, p, p0);
			double d = Dot(to_plane, n);
			if (abs(d) < dil_r)
			{
				return true;
			}
		}
	}

	if (faces.size() == 0) return false;

	for (unsigned int i = 0; i < faces.size(); ++i)
	{
		Vec p0 = points[faces[i][0]];
		Vec p1 = points[faces[i][1]];
		Vec p2 = points[faces[i][2]];
		Vec n = GetNormal(p0, p1, p2);
		Vec to_plane;
		Diff(&to_plane, p, p0);
		if (Dot(to_plane, n) < 0)
		{
			return false;
		}
	}

	// if the point is behind all of the faces, then it's inside the shape
	return true;
}

bool Shape::IsConvex()
{
	/* short circuit if we're dealing with isolated edges or faces */
	if (faces.size() <= 4) return true;

	// to check if the shape is locally convex, we check the dihedral angle at all of the edges
	for (unsigned int e = 0; e < edges.size(); ++e)
	{
		// find the two faces that share this edge
		int face_count = 0;
		int a_index = -1;
		int b_index = -1;
		for (unsigned int f = 0; f < faces.size(); ++f)
		{
			if ((faces[f][0] == edges[e][0] || faces[f][1] == edges[e][0] || faces[f][2] == edges[e][0]) &&
					(faces[f][0] == edges[e][1] || faces[f][1] == edges[e][1] || faces[f][2] == edges[e][1]))
			{
				face_count++;
				int index = -1;
				for (int i = 0; i < 3; ++i)
					if (faces[f][i] != edges[e][0] && faces[f][i] != edges[e][1])
						index = faces[f][i];
				// found the first face
				if (face_count == 1)
				{
					a_index = index;
				}
				// found the second face
				else if (face_count == 2)
				{
					b_index = index;
				}
				// in an ideal situation we can break here, but we don't just to make sure that
				// there aren't more faces on this edge (which would be an error)
			}
		}
		if (face_count != 2)
		{
			wxLogMessage("IsConcave error: found %d faces sharing an edge - expected 2", face_count);
			return false;
		}
		if (a_index < 0 || b_index < 0)
		{
			wxLogMessage("IsConcave error: degenerate face has duplicate points");
			return false;
		}

		Vec test_point;
		Diff(&test_point, points[b_index], points[a_index]);
		Scale(&test_point, test_point, 0.5);
		Sum(&test_point, test_point, points[a_index]);
		if (!Inside(points[a_index]))
		{
			wxLogMessage("IsConcave: external test point for line %d -> %d", a_index, b_index);
			return false;
		}
	}
	return true;
}

void Shape::SetNgon(double r, int n)
{
	if (n < 3)
	{
		wxMessageBox(_("Can't make an n-gon with less than 3 sides"));
		return;
	}

	points.clear();
	faces.clear();
	edges.clear();
	verts.clear();

	dil_r = r/10.0;

	Vec z_axis(0, 0, 1);
	double theta = M_PI*2/n;

	/* top verts */
	for (int i = 0; i < n; ++i)
	{
		Vec p;
		Set(&p, r, 0, r/2);
		Rotate(&p, z_axis, theta*i);

		vector<int> v;
		v.push_back(i);
		verts.push_back(v);
	}

	/* bottom verts */
	for (int i = 0; i < n; ++i)
	{
		Vec p;
		Set(&p, r, 0, -r/2);
		Rotate(&p, z_axis, theta*i);

		vector<int> v;
		v.push_back(i);
		verts.push_back(v);
	}

}

void Shape::SetSphere(double r)
{
	points.clear();
	faces.clear();
	edges.clear();
	verts.clear();

	dil_r = r;

	Vec p0;
	Set(&p0, 0, 0, 0);

	points.push_back(p0);

	vector<int> v;
	v.push_back(0);

	verts.push_back(v);
}

void Shape::SetTface(double r)
{
	points.clear();
	faces.clear();
	edges.clear();
	verts.clear();

	dil_r = r/10;

	Vec p0;
	Vec p1;
	Vec p2;

	Set(&p0, -r, 0, 0);
	Set(&p1, r, 0, 0);
	Set(&p2, 0, r, 0);

	points.push_back(p0);
	points.push_back(p1);
	points.push_back(p2);

	vector<int> v0;
	v0.push_back(0);
	verts.push_back(v0);

	vector<int> v1;
	v1.push_back(1);
	verts.push_back(v1);

	vector<int> v2;
	v2.push_back(2);
	verts.push_back(v2);

	vector<int> e0;
	e0.push_back(0);
	e0.push_back(1);
	edges.push_back(e0);

	vector<int> e1;
	e1.push_back(1);
	e1.push_back(2);
	edges.push_back(e1);

	vector<int> e2;
	e2.push_back(2);
	e2.push_back(0);
	edges.push_back(e2);

	vector<int> f0;
	f0.push_back(0);
	f0.push_back(1);
	f0.push_back(2);
	faces.push_back(f0);
}

void Shape::SetCube(double r)
{
	points.clear();
	faces.clear();
	edges.clear();
	verts.clear();

	Vec p0;
  Vec p1;
  Vec p2;
  Vec p3;
  Vec p4;
  Vec p5;
  Vec p6;
  Vec p7;

  Set(&p0,  r,  r,  r);
  Set(&p1, -r,  r,  r);
  Set(&p2, -r, -r,  r);
  Set(&p3,  r, -r,  r);
  Set(&p4,  r,  r, -r);
  Set(&p5, -r,  r, -r);
  Set(&p6, -r, -r, -r);
  Set(&p7,  r, -r, -r);

  points.push_back(p0);
  points.push_back(p1);
  points.push_back(p2);
  points.push_back(p3);
  points.push_back(p4);
  points.push_back(p5);
  points.push_back(p6);
  points.push_back(p7);

  int cube_faces[12][3] = { { 0, 3, 1 }, { 1, 3, 2 }, { 5, 4, 1 }, { 1, 4, 0 },
           			            { 4, 3, 0 }, { 3, 4, 7 }, { 6, 7, 4 }, { 4, 5, 6 },
                			      { 2, 3, 6 }, { 6, 3, 7 }, { 1, 2, 6 }, { 1, 6, 5 } };
  int cube_edges[18][2] = { { 0, 1 }, { 1, 2 }, { 2, 3 }, { 3, 0 }, // top
                      		  { 4, 5 }, { 5, 6 }, { 6, 7 }, { 7, 4 }, // bottm
                      	   	{ 0, 4 }, { 1, 5 }, { 2, 6 }, { 3, 7 }, // verticals
														{ 1, 3 }, { 3, 4 }, { 4, 1 }, { 3, 6 }, { 6, 1 }, { 6, 4 } }; //diagonals

  for (int i = 0; i < 12; ++i)
  {
		vector<int> v;
    v.push_back(cube_faces[i][0]);
    v.push_back(cube_faces[i][1]);
    v.push_back(cube_faces[i][2]);
    faces.push_back(v);
  }
  for (int i = 0; i < 18; ++i)
  {
		vector<int> v;
    v.push_back(cube_edges[i][0]);
    v.push_back(cube_edges[i][1]);
    edges.push_back(v);
  }

  for (int i = 0; i < 8; ++i)
  {
		vector<int> v;
		v.push_back(i);
    verts.push_back(v);
  }
	Update();
}

void Shape::SetTetra(double r)
{
	points.clear();
	faces.clear();
	edges.clear();
	verts.clear();

	Vec p0;
  Vec p1;
  Vec p2;
  Vec p3;
  
  Set(&p0,  r,  r,  r);
  Set(&p1, -r, -r,  r);
  Set(&p2, -r,  r, -r);
  Set(&p3,  r, -r, -r);

  points.push_back(p0);
  points.push_back(p1);
  points.push_back(p2);
  points.push_back(p3);

  int tetra_faces[4][3] = { { 0, 1, 2 }, { 0, 1, 3 }, { 2, 3, 0 }, { 2, 3, 1 } };
  int tetra_edges[6][2] = { { 0, 1 }, { 0, 2 }, { 0, 3 }, { 1, 2 }, { 1, 3 }, { 2, 3 } };

  for (int i = 0; i < 4; ++i)
  {
		vector<int> v;
    v.push_back(tetra_faces[i][0]);
    v.push_back(tetra_faces[i][1]);
    v.push_back(tetra_faces[i][2]);
    faces.push_back(v);
  }
  for (int i = 0; i < 6; ++i)
  {
		vector<int> v;
    v.push_back(tetra_edges[i][0]);
    v.push_back(tetra_edges[i][1]);
    edges.push_back(v);
  }

  for (int i = 0; i < 4; ++i)
  {
		vector<int> v;
		v.push_back(i);
    verts.push_back(v);
  }
	Update();
}

void Shape::SetRightTetra(double r)
{
	SetTetra(r);
	points[0] = Vec(0, 0, 0);
	points[1] = Vec(1, 0, 0);
	points[2] = Vec(0, 1, 0);
	points[3] = Vec(0, 0, 1);
	Update();
}

void Shape::SetOcta(double r)
{
	points.clear();
	faces.clear();
	edges.clear();
	verts.clear();

	Vec p0;
  Vec p1;
  Vec p2;
  Vec p3;
  Vec p4;
  Vec p5;
  
  Set(&p0,  0,  0,  r);
  Set(&p1,  r,  r,  0);
  Set(&p2,  r, -r,  0);
  Set(&p3, -r, -r,  0);
  Set(&p4, -r,  r,  0);
  Set(&p5,  0,  0, -r);

  points.push_back(p0);
  points.push_back(p1);
  points.push_back(p2);
  points.push_back(p3);
  points.push_back(p4);
  points.push_back(p5);

  int octa_faces[8][3] = { { 0, 1, 2 }, { 0, 2, 3 }, { 0, 3, 4 }, { 0, 4, 1 },
 												   { 5, 2, 1 }, { 5, 3, 2 }, { 5, 4, 3 }, { 5, 1, 4 }	};
  int octa_edges[12][2] = { { 0, 1 }, { 0, 2 }, { 0, 3 }, { 0, 4 }, { 5, 1 }, { 5, 2 },
 														{ 5, 3 }, { 5, 4 }, { 4, 1 }, { 1, 2 }, { 2, 3 }, { 3, 4 } };

  for (int i = 0; i < 8; ++i)
  {
		vector<int> v;
    v.push_back(octa_faces[i][0]);
    v.push_back(octa_faces[i][1]);
    v.push_back(octa_faces[i][2]);
    faces.push_back(v);
  }
  for (int i = 0; i < 12; ++i)
  {
		vector<int> v;
    v.push_back(octa_edges[i][0]);
    v.push_back(octa_edges[i][1]);
    edges.push_back(v);
  }

  for (int i = 0; i < 6; ++i)
  {
		vector<int> v;
		v.push_back(i);
    verts.push_back(v);
  }
	Update();
}

void Shape::MakeCCW()
{
	for (unsigned int i = 0; i < faces.size(); ++i)
	{
		Vec p0 = points[faces[i][0]];
		Vec p1 = points[faces[i][1]];
		Vec p2 = points[faces[i][2]];

		// find two vectors in the plane of the face
		Vec p01;
		Vec p02;
		Diff(&p01, p0, p1);
		Diff(&p02, p0, p2);
		// then find a normal to the plane
		Vec n;
		X(&n, p01, p02);
		// then find a vector from the plane to the center of the shape
		Vec c;
		Diff(&c, p0, GetInternalPoint());
		// and see if they point in the same direction
		double d = Dot(c, n);
		// and if the normal points the wrong way, swap the ordering of the vertices
		if (d < 0)
		{
			wxLogMessage("Point order (%d, %d, %d): WRONG (swapped)", faces[i][0], faces[i][1], faces[i][2]);
			int temp = faces[i][1];
			faces[i][1] = faces[i][2];
			faces[i][2] = temp;
		}
		else
		{
			wxLogMessage("Point order (%d, %d, %d): RIGHT", faces[i][0], faces[i][1], faces[i][2]);
		}
	}
}

/*
bool Sphere::Inside(Vec p)
{
	return Dist(p, center) <= radius;
}

bool Cube::Inside(Vec p)
{
	Vec v;
	Diff(&v, p, center);
	return abs(v.x) <= radius && abs(v.y) <= radius && abs(v.z) <= radius;
}

void Shape::Translate(Vec p) { Sum(&center, center, p); }

Caps::Caps(double r, double d_r)
{
  Vec p0;
  Vec p1;

  radius = r + d_r;
	dil_r = d_r;

  Set(&p0, 0, 0,  r);
  Set(&p1, 0, 0, -r);

  points.push_back(p0);
  points.push_back(p1);

  primitives.push_back(new vector<int>);
  primitives[0]->push_back(0);

  primitives.push_back(new vector<int>);
  primitives[1]->push_back(1);

  primitives.push_back(new vector<int>);
  primitives[2]->push_back(0);
  primitives[2]->push_back(1);
}

Sphere::Sphere(double r)
{
  Vec p0;

  radius = r;
	dil_r = r;

  Set(&p0, 0, 0, 0);

  points.push_back(p0);

  primitives.push_back(new vector<int>);
  primitives[0]->push_back(0);
}

Cube::Cube(double r, double d_r)
{
  Vec p0;
  Vec p1;
  Vec p2;
  Vec p3;
  Vec p4;
  Vec p5;
  Vec p6;
  Vec p7;

  radius = r + d_r;
	dil_r = d_r;
  double x = r;

  Set(&p0,  x,  x,  x);
  Set(&p1, -x,  x,  x);
  Set(&p2, -x, -x,  x);
  Set(&p3,  x, -x,  x);
  Set(&p4,  x,  x, -x);
  Set(&p5, -x,  x, -x);
  Set(&p6, -x, -x, -x);
  Set(&p7,  x, -x, -x);

  points.push_back(p0);
  points.push_back(p1);
  points.push_back(p2);
  points.push_back(p3);
  points.push_back(p4);
  points.push_back(p5);
  points.push_back(p6);
  points.push_back(p7);

  int faces[12][3] = { { 0, 1, 2 }, { 2, 3, 0 }, { 4, 5, 6 }, { 6, 7, 4 },
                       { 0, 1, 4 }, { 4, 5, 1 }, { 2, 3, 6 }, { 6, 7, 3 },
                       { 0, 3, 4 }, { 3, 4, 7 }, { 1, 2, 5 }, { 2, 5, 6 } };
  int edges[12][2] = { { 0, 1 }, { 1, 2 }, { 2, 3 }, { 3, 0 },
                       { 4, 5 }, { 5, 6 }, { 6, 7 }, { 7, 4 },
                       { 0, 4 }, { 1, 5 }, { 2, 6 }, { 3, 7 } };

  for (int i = 0; i < 12; ++i)
  {
    primitives.push_back(new vector<int>);
    primitives[i]->push_back(faces[i][0]);
    primitives[i]->push_back(faces[i][1]);
    primitives[i]->push_back(faces[i][2]);
  }
  for (int i = 0; i < 12; ++i)
  {
    primitives.push_back(new vector<int>);
    primitives[i+12]->push_back(edges[i][0]);
    primitives[i+12]->push_back(edges[i][1]);
  }
  for (int i = 0; i < 8; ++i)
  {
    primitives.push_back(new vector<int>);
    primitives[i+24]->push_back(i);
  }
}

Tetra::Tetra(double r, double d_r)
{

  Vec p0;
  Vec p1;
  Vec p2;
  Vec p3;
  
  radius = r + d_r;
	dil_r = d_r;
  double x = r;

  Set(&p0,  x,  x,  x);
  Set(&p1, -x, -x,  x);
  Set(&p2, -x,  x, -x);
  Set(&p3,  x, -x, -x);

  points.push_back(p0);
  points.push_back(p1);
  points.push_back(p2);
  points.push_back(p3);

  int faces[4][3] = { { 0, 1, 2 }, { 0, 1, 3 }, { 2, 3, 0 }, { 2, 3, 1 } };
  int edges[6][2] = { { 0, 1 }, { 0, 2 }, { 0, 3 }, { 1, 2 }, { 1, 3 }, { 2, 3 } };

  for (int i = 0; i < 4; ++i)
  {
    primitives.push_back(new vector<int>);
    primitives[i]->push_back(faces[i][0]);
    primitives[i]->push_back(faces[i][1]);
    primitives[i]->push_back(faces[i][2]);
  }
  for (int i = 0; i < 6; ++i)
  {
    primitives.push_back(new vector<int>);
    primitives[i+4]->push_back(edges[i][0]);
    primitives[i+4]->push_back(edges[i][1]);
  }

  for (int i = 0; i < 4; ++i)
  {
    primitives.push_back(new vector<int>); primitives[i+10]->push_back(i);
  }
}
*/
