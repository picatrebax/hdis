#ifndef DIALOG_HH
#define DIALOG_HH

#include <wx/wx.h>
#include <wx/dialog.h>
#include <wx/valnum.h>

class NgonDialog : public wxDialog
{
public:
  NgonDialog();

  wxTextCtrl *n_text;

	void OnOk(wxCommandEvent &event);
};

#endif //DIALOG_H
