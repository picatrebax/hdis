#ifndef DEFS_HH
#define DEFS_HH

#include <wx/wx.h>

#define _F wxString::Format

#define VERSION "1.0.0"

#define DIV(a,b) (b) == 0 ? 0 : (a)/(b)
#define SIGN(x) x < 0 ? _("") : _(" ")
#define VECTOR_APPEND(a, b) a.insert(a.end(), b.begin(), b.end());

#define TRANSLATE_SPEED 400
#define ROTATE_SPEED 50
#define CENTER 100
#define RADIUS 100
#define DIL_R 100

#define SPHERE 0
#define OCTA 1
#define CUBE 2
#define TETRA 3
#define TFACE 4
#define NGON 5

#define VERT 0
#define EDGE 1
#define FACE 2

// this is the value we'll use as the first color channel
// when we're drawing the mesh in color picking mode. it needs
// to be higher than the number of shapes, since the color we
// use for shapes in picking mode uses the index of the shape
// as a unique identifier in the first color channel
#define MESH 254

#define IDLE 0
#define ROTATE_WORLD 1
#define ROTATE_SHAPE 2
#define TRANSLATE_SHAPE 3
#define TRANSLATE_VERTEX 4

#define DRAW_MODE_NORMAL 0
#define DRAW_MODE_SPHERES 1

enum
{
  ID_FRAME_CANVAS = wxID_HIGHEST + 1,
  ID_CANVAS,
  ID_SAVE_COUPIC,
  ID_SAVE_COUPI,
	ID_LOAD,
	ID_IMPORT_MESH,
	ID_CLEAR_MESH,
  ID_EXIT,
  ID_ABOUT,
  ID_TRANSLATE_SPEED,
  ID_ROTATE_SPEED,
  ID_RADIUS,
  ID_DILR,
  ID_DILR_TEXT,
  ID_CENTER_X,
  ID_CENTER_Y,
  ID_CENTER_Z,
	ID_CENTER_X_TEXT,
	ID_CENTER_Y_TEXT,
	ID_CENTER_Z_TEXT,
  ID_NEW_SPHERE,
  ID_NEW_OCTA,
  ID_NEW_CUBE,
  ID_NEW_TETRA,

	ID_FIRST_SHAPE
};

#endif
