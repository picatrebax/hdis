#include "hdisdesign.hh"

IMPLEMENT_APP(Coupic)

bool Coupic::OnInit()
{
  CreateCanvasFrame();
  Select(-1);
  return true;
}

void Coupic::CreateCanvasFrame()
{
	// blerg a bunch of initialization junk

  CanvasFrame = new wxFrame(NULL, ID_FRAME_CANVAS, _("Canvas Frame"), wxDefaultPosition, wxDefaultSize);
  
  canvas = new Canvas(CanvasFrame, this, ID_CANVAS, wxDefaultPosition, wxSize(700, 700));
  wxBoxSizer* canvas_sizer = new wxBoxSizer(wxVERTICAL);
  canvas_sizer->Add(canvas, 1, wxEXPAND, 0);

  wxSize text_size(75, wxDefaultSize.GetHeight());
  wxSize title_size(225, wxDefaultSize.GetHeight());

  wxSize slider_size(100, wxDefaultSize.GetHeight());
  wxPoint slider_pos(-1,-1);

	/* create the sliders */
  wxSlider* translate_speed = new wxSlider(CanvasFrame, ID_TRANSLATE_SPEED, 30, 0, 100, slider_pos, slider_size);
  wxSlider* rotate_speed = new wxSlider(CanvasFrame, ID_ROTATE_SPEED, 30, 0, 100, slider_pos, slider_size);

  wxSlider* radius = new wxSlider(CanvasFrame, ID_RADIUS, 50, 1, 100, slider_pos, slider_size);
  wxSlider* dilr = new wxSlider(CanvasFrame, ID_DILR, 50, 1, 100, slider_pos, slider_size);
  wxSlider* center_x = new wxSlider(CanvasFrame, ID_CENTER_X, 50, 0, 100, slider_pos, slider_size);
  wxSlider* center_y = new wxSlider(CanvasFrame, ID_CENTER_Y, 50, 0, 100, slider_pos, slider_size);
  wxSlider* center_z = new wxSlider(CanvasFrame, ID_CENTER_Z, 50, 0, 100, slider_pos, slider_size);

	/* create the buttons */
  wxButton* new_sphere = new wxButton(CanvasFrame, ID_NEW_SPHERE, _("Sphere"));
  wxButton* new_capsule = new wxButton(CanvasFrame, ID_NEW_OCTA, _("Octa"));
  wxButton* new_cube = new wxButton(CanvasFrame, ID_NEW_CUBE, _("Cube"));
  wxButton* new_tetra = new wxButton(CanvasFrame, ID_NEW_TETRA, _("Tetra"));

	/* create the validators we'll use for our text controls */
	wxFloatingPointValidator<float> x_validator(3, NULL, wxNUM_VAL_DEFAULT);
	wxFloatingPointValidator<float> y_validator(3, NULL, wxNUM_VAL_DEFAULT);
	wxFloatingPointValidator<float> z_validator(3, NULL, wxNUM_VAL_DEFAULT);
	wxFloatingPointValidator<float> dilr_validator(3, NULL, wxNUM_VAL_DEFAULT);

	x_validator.SetRange(-1, 1);
	y_validator.SetRange(-1, 1);
	z_validator.SetRange(-1, 1);

	int text_ctrl_width = 60;

	/* create our text controls */
  wxTextCtrl* center_x_text = new wxTextCtrl(CanvasFrame, ID_CENTER_X_TEXT, 
			_("0.000"), wxDefaultPosition, wxSize(text_ctrl_width, -1), wxTE_PROCESS_ENTER, x_validator);
  wxTextCtrl* center_y_text = new wxTextCtrl(CanvasFrame, ID_CENTER_Y_TEXT,
			_("0.000"), wxDefaultPosition, wxSize(text_ctrl_width, -1), wxTE_PROCESS_ENTER, y_validator);
  wxTextCtrl* center_z_text = new wxTextCtrl(CanvasFrame, ID_CENTER_Z_TEXT,
			_("0.000"), wxDefaultPosition, wxSize(text_ctrl_width, -1), wxTE_PROCESS_ENTER, z_validator);
  wxTextCtrl* dilr_text = new wxTextCtrl(CanvasFrame, ID_DILR_TEXT,
			_("0.000"), wxDefaultPosition, wxSize(text_ctrl_width, -1), wxTE_PROCESS_ENTER, dilr_validator);
  
	/* hook up all the event handlers */
  Connect(ID_TRANSLATE_SPEED, wxEVT_COMMAND_SLIDER_UPDATED, wxCommandEventHandler(Coupic::OnTranslateSpeed));
  Connect(ID_ROTATE_SPEED, wxEVT_COMMAND_SLIDER_UPDATED, wxCommandEventHandler(Coupic::OnRotateSpeed));
  Connect(ID_RADIUS, wxEVT_COMMAND_SLIDER_UPDATED, wxCommandEventHandler(Coupic::OnRadius));
  Connect(ID_DILR, wxEVT_COMMAND_SLIDER_UPDATED, wxCommandEventHandler(Coupic::OnDilR));
  Connect(ID_CENTER_X, wxEVT_COMMAND_SLIDER_UPDATED, wxCommandEventHandler(Coupic::OnCenterX));
  Connect(ID_CENTER_Y, wxEVT_COMMAND_SLIDER_UPDATED, wxCommandEventHandler(Coupic::OnCenterY));
  Connect(ID_CENTER_Z, wxEVT_COMMAND_SLIDER_UPDATED, wxCommandEventHandler(Coupic::OnCenterZ));

  Connect(ID_NEW_SPHERE, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(Coupic::OnNewSphere));
  Connect(ID_NEW_OCTA, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(Coupic::OnNewCapsule));
  Connect(ID_NEW_CUBE, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(Coupic::OnNewCube));
  Connect(ID_NEW_TETRA, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(Coupic::OnNewTetra));

	Connect(ID_CENTER_X_TEXT, wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler(Coupic::OnCenterXText));
	Connect(ID_CENTER_Y_TEXT, wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler(Coupic::OnCenterYText));
	Connect(ID_CENTER_Z_TEXT, wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler(Coupic::OnCenterZText));
	Connect(ID_DILR_TEXT, wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler(Coupic::OnDilRText));

  vector<wxBoxSizer*> rows;
  unsigned int nrows = 0;
  int border = 4;

	/* and then set everything up in nice sizers. each row has some controls, a text label, and then an expanding
	 * blank static text that pads on the left to make everything line up nice */

	/* big title text */
  wxStaticText* general_title = new wxStaticText(CanvasFrame, wxID_ANY, _("General Settings"), wxDefaultPosition, title_size);
  wxFont font = general_title->GetFont();
  font.SetWeight(wxFONTWEIGHT_BOLD);
  general_title->SetFont(font);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(general_title, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxLEFT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(CanvasFrame, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(CanvasFrame, wxID_ANY, _("Translate Speed: ")), 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(translate_speed, 0, wxRIGHT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(CanvasFrame, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(CanvasFrame, wxID_ANY, _("Rotate Speed: ")), 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(rotate_speed, 0, wxRIGHT, border);

  wxStaticText* shape_title = new wxStaticText(CanvasFrame, wxID_ANY, _("Current Shape"), wxDefaultPosition, title_size);
  shape_title->SetFont(font);
  
  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(shape_title, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxLEFT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(CanvasFrame, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(CanvasFrame, wxID_ANY, _("Size: ")), 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(radius , 0, wxRIGHT, border);
  rows[nrows - 1]->Add(new wxStaticText(CanvasFrame, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(text_ctrl_width, -1)), 0, wxRIGHT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(CanvasFrame, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(CanvasFrame, wxID_ANY, _("DilR: ")), 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(dilr, 0, wxRIGHT, border);
  rows[nrows - 1]->Add(dilr_text, 0, wxRIGHT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(CanvasFrame, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(CanvasFrame, wxID_ANY, _("Center_x: ")), 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(center_x, 0, wxRIGHT, border);
	rows[nrows - 1]->Add(center_x_text, 0, wxRIGHT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(CanvasFrame, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(CanvasFrame, wxID_ANY, _("Center_y: ")), 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(center_y, 0, wxRIGHT, border);
	rows[nrows - 1]->Add(center_y_text, 0, wxRIGHT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(CanvasFrame, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(CanvasFrame, wxID_ANY, _("Center_z: ")), 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(center_z, 0, wxRIGHT, border);
	rows[nrows - 1]->Add(center_z_text, 0, wxRIGHT, border);

	/* big title text before the buttons */
  wxStaticText* new_title = new wxStaticText(CanvasFrame, wxID_ANY, _("New Shape"), wxDefaultPosition, title_size);
  new_title->SetFont(font);

  int button_border = 15;
  
  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new_title, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxLEFT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows-1]->Add(new_sphere, 1, wxEXPAND | wxLEFT, button_border);
  rows[nrows-1]->Add(new wxStaticText(CanvasFrame, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(4,1)), 0, 0, 0);
  rows[nrows-1]->Add(new_capsule, 1, wxEXPAND | wxRIGHT, button_border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows-1]->Add(new_cube, 1, wxEXPAND | wxLEFT, button_border);
  rows[nrows-1]->Add(new wxStaticText(CanvasFrame, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(4,1)), 0, 0, 0);
  rows[nrows-1]->Add(new_tetra, 1, wxEXPAND | wxRIGHT, button_border);

  wxStaticBoxSizer* settings_sizer = new wxStaticBoxSizer(wxVERTICAL, CanvasFrame);
  for (unsigned int r =  0; r < rows.size(); ++r) settings_sizer->Add(rows[r], 1, wxEXPAND | wxTOP | wxBOTTOM, 2);
  wxBoxSizer* column = new wxBoxSizer(wxVERTICAL);
  column->Add(settings_sizer, 0, wxEXPAND | wxTOP | wxBOTTOM, 0);

  wxBoxSizer* top_sizer = new wxBoxSizer(wxHORIZONTAL);
  top_sizer->Add(column, 0, wxEXPAND, 0);
  top_sizer->Add(canvas_sizer, 1, wxEXPAND | wxLEFT, 10);

	log_text = new wxTextCtrl(CanvasFrame, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(1, 150),
			wxTE_MULTILINE | wxTE_LEFT | wxTE_READONLY);
	log = new wxLogTextCtrl(log_text);
  wxLog::SetActiveTarget(log);
	wxBoxSizer* window_sizer = new wxBoxSizer(wxVERTICAL);
	window_sizer->Add(top_sizer, 0, wxEXPAND | wxTOP | wxLEFT | wxRIGHT, 10);
	window_sizer->Add(log_text, 1, wxEXPAND | wxALL, 10);


  window_sizer->SetSizeHints(CanvasFrame);
  CanvasFrame->SetSizer(window_sizer);
  AddMenus(CanvasFrame);
  CanvasFrame->Show(true);
  canvas->InitGL();

	wxLogMessage("initialization complete");
}

void Coupic::Select(int n)
{
  canvas->selected = n;
  if (n < 0)
  {
		// disable all the sliders
    ((wxSlider*) CanvasFrame->FindWindow(ID_RADIUS))->Disable();
    ((wxSlider*) CanvasFrame->FindWindow(ID_DILR))->Disable();
    ((wxSlider*) CanvasFrame->FindWindow(ID_CENTER_X))->Disable();
    ((wxSlider*) CanvasFrame->FindWindow(ID_CENTER_Y))->Disable();
    ((wxSlider*) CanvasFrame->FindWindow(ID_CENTER_Z))->Disable();

    ((wxTextCtrl*) CanvasFrame->FindWindow(ID_CENTER_X_TEXT))->Disable();
    ((wxTextCtrl*) CanvasFrame->FindWindow(ID_CENTER_Y_TEXT))->Disable();
    ((wxTextCtrl*) CanvasFrame->FindWindow(ID_CENTER_Z_TEXT))->Disable();
    ((wxTextCtrl*) CanvasFrame->FindWindow(ID_DILR_TEXT))->Disable();
  }
	else
	{
		// make sure everything is enabled
		((wxSlider*) CanvasFrame->FindWindow(ID_RADIUS))->Enable();
		((wxSlider*) CanvasFrame->FindWindow(ID_DILR))->Enable();
		((wxSlider*) CanvasFrame->FindWindow(ID_CENTER_X))->Enable();
		((wxSlider*) CanvasFrame->FindWindow(ID_CENTER_Y))->Enable();
		((wxSlider*) CanvasFrame->FindWindow(ID_CENTER_Z))->Enable();
    ((wxTextCtrl*) CanvasFrame->FindWindow(ID_CENTER_X_TEXT))->Enable();
    ((wxTextCtrl*) CanvasFrame->FindWindow(ID_CENTER_Y_TEXT))->Enable();
    ((wxTextCtrl*) CanvasFrame->FindWindow(ID_CENTER_Z_TEXT))->Enable();
    ((wxTextCtrl*) CanvasFrame->FindWindow(ID_DILR_TEXT))->Enable();
		// and in the right position
		((wxSlider*) CanvasFrame->FindWindow(ID_RADIUS))->SetValue((int)(canvas->Shapes[canvas->selected]->SBB_radius*100));
		((wxSlider*) CanvasFrame->FindWindow(ID_DILR))->SetValue((int)(canvas->Shapes[canvas->selected]->dil_r*100));
		((wxSlider*) CanvasFrame->FindWindow(ID_CENTER_X))->SetValue((int)((canvas->Shapes[canvas->selected]->SBB_center.x+1.0)*50));
		((wxSlider*) CanvasFrame->FindWindow(ID_CENTER_Y))->SetValue((int)((canvas->Shapes[canvas->selected]->SBB_center.y+1.0)*50));
		((wxSlider*) CanvasFrame->FindWindow(ID_CENTER_Z))->SetValue((int)((canvas->Shapes[canvas->selected]->SBB_center.z+1.0)*50));
    ((wxTextCtrl*) CanvasFrame->FindWindow(ID_CENTER_X_TEXT))->SetValue(_F(_("%.3f"), canvas->Shapes[canvas->selected]->SBB_center.x));
    ((wxTextCtrl*) CanvasFrame->FindWindow(ID_CENTER_Y_TEXT))->SetValue(_F(_("%.3f"), canvas->Shapes[canvas->selected]->SBB_center.y));
    ((wxTextCtrl*) CanvasFrame->FindWindow(ID_CENTER_Z_TEXT))->SetValue(_F(_("%.3f"), canvas->Shapes[canvas->selected]->SBB_center.z));
    ((wxTextCtrl*) CanvasFrame->FindWindow(ID_DILR_TEXT))->SetValue(_F(_("%.3f"), canvas->Shapes[canvas->selected]->dil_r));
	}
}

void Coupic::OnTranslateSpeed(wxCommandEvent &event)
{
  double temp = (double) event.GetInt();
  canvas->translate_speed = temp/TRANSLATE_SPEED;
}

void Coupic::OnRotateSpeed(wxCommandEvent &event)
{
  double temp = (double) event.GetInt();
  canvas->rotate_speed = temp/ROTATE_SPEED;
}

// no safety checks are done here to see if a shape is selected
// it is assumed that if no shape is selected then the controls are disabled
void Coupic::OnRadius(wxCommandEvent &event)
{
  double pos = (double) event.GetInt();
	double alpha = pos / 100.0 / canvas->Shapes[canvas->selected]->SBB_radius;
  canvas->ResizeShape(alpha);
  canvas->Render();
	Select(canvas->selected);
}
void Coupic::OnDilR(wxCommandEvent &event)
{
	double pos = (double) event.GetInt();
	canvas->Shapes[canvas->selected]->dil_r = pos / 100.0;
	canvas->Shapes[canvas->selected]->SetSBB();
	canvas->Render();
	Select(canvas->selected);
}

void Coupic::OnCenterX(wxCommandEvent &event)
{
	double x = (double) ((wxSlider*) CanvasFrame->FindWindow(ID_CENTER_X))->GetValue();
	double y = (double) ((wxSlider*) CanvasFrame->FindWindow(ID_CENTER_Y))->GetValue();
	double z = (double) ((wxSlider*) CanvasFrame->FindWindow(ID_CENTER_Z))->GetValue();
	x = (x-50.0)/50.0;
	y = (y-50.0)/50.0;
	z = (z-50.0)/50.0;
	canvas->MoveShapeTo(Vec(x, y, z));
	canvas->Render();
	/* to update the text fields */
	Select(canvas->selected);
}
void Coupic::OnCenterY(wxCommandEvent &event)
{
	double x = (double) ((wxSlider*) CanvasFrame->FindWindow(ID_CENTER_X))->GetValue();
	double y = (double) ((wxSlider*) CanvasFrame->FindWindow(ID_CENTER_Y))->GetValue();
	double z = (double) ((wxSlider*) CanvasFrame->FindWindow(ID_CENTER_Z))->GetValue();
	x = (x-50.0)/50.0;
	y = (y-50.0)/50.0;
	z = (z-50.0)/50.0;
	canvas->MoveShapeTo(Vec(x, y, z));
	canvas->Render();
	/* to update the text fields */
	Select(canvas->selected);
}
void Coupic::OnCenterZ(wxCommandEvent &event)
{
	double x = (double) ((wxSlider*) CanvasFrame->FindWindow(ID_CENTER_X))->GetValue();
	double y = (double) ((wxSlider*) CanvasFrame->FindWindow(ID_CENTER_Y))->GetValue();
	double z = (double) ((wxSlider*) CanvasFrame->FindWindow(ID_CENTER_Z))->GetValue();
	x = (x-50.0)/50.0;
	y = (y-50.0)/50.0;
	z = (z-50.0)/50.0;
	canvas->MoveShapeTo(Vec(x, y, z));
	canvas->Render();
	/* to update the text fields */
	Select(canvas->selected);
}

void Coupic::OnCenterXText(wxCommandEvent &event)
{
	/* read the x/y/z text fields and use them as the new center */
	double x, y, z;
	(((wxTextCtrl*) CanvasFrame->FindWindow(ID_CENTER_X_TEXT))->GetValue()).ToDouble(&x);
	(((wxTextCtrl*) CanvasFrame->FindWindow(ID_CENTER_Y_TEXT))->GetValue()).ToDouble(&y);
	(((wxTextCtrl*) CanvasFrame->FindWindow(ID_CENTER_Z_TEXT))->GetValue()).ToDouble(&z);
	canvas->MoveShapeTo(Vec(x, y, z));
	canvas->Render();
	/* to update the sliders */
	Select(canvas->selected);
}

void Coupic::OnCenterYText(wxCommandEvent &event)
{
	/* read the x/y/z text fields and use them as the new center */
	double x, y, z;
	(((wxTextCtrl*) CanvasFrame->FindWindow(ID_CENTER_X_TEXT))->GetValue()).ToDouble(&x);
	(((wxTextCtrl*) CanvasFrame->FindWindow(ID_CENTER_Y_TEXT))->GetValue()).ToDouble(&y);
	(((wxTextCtrl*) CanvasFrame->FindWindow(ID_CENTER_Z_TEXT))->GetValue()).ToDouble(&z);
	canvas->MoveShapeTo(Vec(x, y, z));
	canvas->Render();
	/* to update the sliders */
	Select(canvas->selected);
}

void Coupic::OnCenterZText(wxCommandEvent &event)
{
	/* read the x/y/z text fields and use them as the new center */
	double x, y, z;
	(((wxTextCtrl*) CanvasFrame->FindWindow(ID_CENTER_X_TEXT))->GetValue()).ToDouble(&x);
	(((wxTextCtrl*) CanvasFrame->FindWindow(ID_CENTER_Y_TEXT))->GetValue()).ToDouble(&y);
	(((wxTextCtrl*) CanvasFrame->FindWindow(ID_CENTER_Z_TEXT))->GetValue()).ToDouble(&z);
	canvas->MoveShapeTo(Vec(x, y, z));
	canvas->Render();
	/* to update the sliders */
	Select(canvas->selected);
}

void Coupic::OnDilRText(wxCommandEvent &event)
{
	/* read the x/y/z text fields and use them as the new center */
	double dilr;
	(((wxTextCtrl*) CanvasFrame->FindWindow(ID_DILR_TEXT))->GetValue()).ToDouble(&dilr);
	canvas->Shapes[canvas->selected]->dil_r = dilr;
	canvas->Shapes[canvas->selected]->SetSBB();
	canvas->Render();
	Select(canvas->selected);
}

void Coupic::OnNewSphere(wxCommandEvent &event){ canvas->NewShape(SPHERE); }
void Coupic::OnNewCapsule(wxCommandEvent &event){ canvas->NewShape(OCTA); }
void Coupic::OnNewCube(wxCommandEvent &event){ canvas->NewShape(CUBE); }
void Coupic::OnNewTetra(wxCommandEvent &event){ canvas->NewShape(TETRA); }

void Coupic::AddMenus(wxFrame* frame)
{
  wxMenuBar* bar = new wxMenuBar();
  wxMenu* file = new wxMenu();
  file->Append(ID_ABOUT, _("&About Coupic\tCtrl+A"));
  file->Append(wxID_EXIT, _("&Quit\tCtrl+Q"));
  file->Append(ID_SAVE_COUPIC, _("&Save shape\tCtrl+S"));
  file->Append(ID_SAVE_COUPI, _("&Export for Coupi\tCtrl+E"));
	file->Append(ID_IMPORT_MESH, _("&Import mesh\tCtrl+I"));
	file->Append(ID_CLEAR_MESH, _("&Clear mesh\tCtrl+C"));
  Connect(wxID_ABOUT, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(Coupic::OnMenuAbout));
  Connect(wxID_EXIT, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(Coupic::OnMenuQuit));
  Connect(ID_SAVE_COUPIC, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(Coupic::OnMenuSaveCoupic));
  Connect(ID_SAVE_COUPI, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(Coupic::OnMenuSaveCoupi));
  Connect(ID_IMPORT_MESH, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(Coupic::OnMenuImportMesh));
  Connect(ID_CLEAR_MESH, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(Coupic::OnMenuClearMesh));
  bar->Append(file, "&File");

	shapes_menu = new wxMenu();
	LoadShapesMenu();
	bar->Append(shapes_menu, "Shapes");

  frame->SetMenuBar(bar);
}

void Coupic::LoadShapesMenu()
{
	// clear the menu
	unsigned int n_items = shapes_menu->GetMenuItemCount();
	for (unsigned int i = 0; i < n_items; ++i)
	{
		wxLogMessage("Deleting shape %u", i);
		shapes_menu->Delete(ID_FIRST_SHAPE + i);
		Disconnect(ID_FIRST_SHAPE + i, wxEVT_COMMAND_MENU_SELECTED);
	}
	wxString dir_path = wxStandardPaths::Get().GetExecutablePath().BeforeLast(wxUniChar('/'));
	dir_path.Append(_("/../Resources/Shapes/"));
	wxDir dir(dir_path);
	if (dir.IsOpened())
	{
		int n_shapes = 0;
		wxString shape_name;
		bool cont = dir.GetFirst(&shape_name, _("*.hdisdesign"), wxDIR_DEFAULT);
		while (cont)
		{
			shapes_menu->Append(ID_FIRST_SHAPE + n_shapes, shape_name.BeforeLast(wxUniChar('.')));
			Connect(ID_FIRST_SHAPE + n_shapes, wxEVT_COMMAND_MENU_SELECTED,
					wxCommandEventHandler(Coupic::OnMenuLoadShape));
			n_shapes++;
			cont = dir.GetNext(&shape_name);
		}
	}
}

void Coupic::OnMenuImportMesh(wxCommandEvent& event)
{
	wxFileDialog fopen(NULL, wxEmptyString, wxEmptyString,
        _("name"),
        _("Coupi Mesh (*.cou)|*.cou"), wxFD_OPEN|wxFD_FILE_MUST_EXIST);
	
	if (fopen.ShowModal() != wxID_OK) return;

	ClearMesh();

	wxString fname = fopen.GetPath();

	ifstream file;
	file.open(fname);

	double unused;
	unsigned int ntris, npoints;

	/* scaling factors */
	file >> unused;
	file >> unused;
	file >> unused;

	file >> ntris;
	for (unsigned int i = 0; i < ntris; ++i)
	{
		unsigned int i0, i1, i2;
		file >> unused; // index
		// point indices
		file >> i0;
		file >> i1;
		file >> i2;

		/* subtract 1 for lua indexing */
		canvas->mesh_triangles.push_back(i0-1);
		canvas->mesh_triangles.push_back(i1-1);
		canvas->mesh_triangles.push_back(i2-1);
	}

	double max_p = 0;

	file >> npoints;
	for (unsigned int i = 0; i < npoints; ++i)
	{
		double x, y, z;
		file >> unused; // index
		file >> x;
		file >> y;
		file >> z;

		if (x > max_p) max_p = x;
		if (y > max_p) max_p = y;
		if (z > max_p) max_p = z;

		canvas->mesh_points.push_back(Vec(x, y, z));
	}

	if (max_p != 0)
		ResizeMesh(1/max_p);

	wxLogMessage("Imported mesh:\n\t%u points\n\t%u triangles", npoints, ntris);
}

void Coupic::ResizeMesh(double x)
{
	for (unsigned int i = 0; i < canvas->mesh_points.size(); ++i)
	{
		Scale(&(canvas->mesh_points[i]), canvas->mesh_points[i], x);
	}
}

void Coupic::OnMenuClearMesh(wxCommandEvent& event)
{
	ClearMesh();
}

void Coupic::ClearMesh()
{
	canvas->mesh_points.clear();
	canvas->mesh_triangles.clear();
}

void Coupic::OnMenuAbout(wxCommandEvent& event)
{
  wxMessageBox(_("All you could ever want to know!"));
}

void Coupic::OnMenuQuit(wxCommandEvent& event)
{
  CanvasFrame->Close(true);
}

void Coupic::OnMenuLoadShape(wxCommandEvent& event)
{
	wxLogMessage("Loading shape %s", shapes_menu->GetLabel(event.GetId()));
	LoadShape(shapes_menu->GetLabel(event.GetId()));
}

void Coupic::LoadShape(wxString name)
{
	wxString filename = wxStandardPaths::Get().GetExecutablePath().BeforeLast(wxUniChar('/'));
	filename.Append(_("/../Resources/Shapes/")).Append(name).Append(_(".hdisdesign"));

	vector<Vec> points;
	vector< vector<int> > verts;
	vector< vector<int> > edges;
	vector< vector<int> > faces;
	ifstream file;
	file.open(filename);

	double unused;

	// read in the points
	int n_points = 0;
	file >> n_points;
	for (int i = 0; i < n_points; ++i)
	{
		Vec p;
		file >> p.x;
		file >> p.y;
		file >> p.z;
		points.push_back(p);

		vector<int> v;
		v.push_back(i);
		verts.push_back(v);

		file >> unused; // degree
		file >> unused; // adj_e
	}

	// adjacent edge array for poly representation
	int n_adj;
	file >> n_adj;
	for (int i = 0; i < n_adj; ++i) file >> unused;

	// read in the edges
	int n_edges;
	file >> n_edges;
	for (int i = 0; i < n_edges; ++i)
	{
		vector<int> e;
		int p0, p1;
		file >> p0;
		file >> p1;
		e.push_back(p0);
		e.push_back(p1);
		edges.push_back(e);

		file >> unused; // lface
		file >> unused; // rface
	}

	// read in the faces
	int n_faces;
	file >> n_faces;
	for (int i = 0; i < n_faces; ++i)
	{
		vector<int> f;
		int p0, p1, p2;
		file >> p0;
		file >> p1;
		file >> p2;
		f.push_back(p0);
		f.push_back(p1);
		f.push_back(p2);
		faces.push_back(f);

		file >> unused; // edge0
		file >> unused; // edge1
		file >> unused; // edge2
		
		file >> unused; // n.x
		file >> unused; // n.y
		file >> unused; // n.z
	}
	
	file.close();
	Shape* s = new Shape(0.03, points, faces, edges, verts);
	canvas->Shapes.push_back(s);
	Select(canvas->Shapes.size() - 1);
	canvas->Render();
}


void Coupic::OnMenuSaveCoupic(wxCommandEvent& event)
{
	if (canvas->Shapes.size() != 1)
	{
		wxMessageBox(_("You can only save a single shape to file."), _("Error"));
		return;
	}

	/*
	double V;
	Vec I;
	StandardizeShapes(&V, &I);
	*/

	wxTextEntryDialog* dlg = new wxTextEntryDialog(CanvasFrame, _("Enter shape name"), _(""), _("default_shape"));
	if (dlg->ShowModal() == wxID_OK)
	{
		Shape* s = canvas->Shapes[0];
		wxString filename = wxStandardPaths::Get().GetExecutablePath().BeforeLast(wxUniChar('/'));
		filename.Append(_("/../Resources/Shapes/")).Append(dlg->GetValue()).Append(_(".hdisdesign"));
		wxLogMessage("Saving to: %s", filename);
		ofstream file;
		file.open(filename);

		vector<int> adj_e;

		file << s->points.size() << " ";
		for (unsigned int i = 0; i < s->points.size(); ++i)
		{
			file << "\n";
			file << s->points[i].x << " ";
			file << s->points[i].y << " ";
			file << s->points[i].z << " ";
			vector<int> cur_adj_e = s->GetAdjEdges(i);
			int deg = cur_adj_e.size();
			file << deg << " ";
			file << adj_e.size() << " ";
			VECTOR_APPEND(adj_e, cur_adj_e);
		}

		file << "\n\n";
		file << adj_e.size() << " ";
		for (unsigned int i = 0; i < adj_e.size(); ++i)
		{
			file << adj_e[i] << " ";
		}

		file << "\n\n";
		file << s->edges.size() << " ";
		for (unsigned int i = 0; i < s->edges.size(); ++i)
		{
			file << "\n";
			file << s->edges[i][0] << " ";
			file << s->edges[i][1] << " ";
			file << s->GetLface(i) << " ";
			file << s->GetRface(i) << " ";
		}

		file << "\n\n";
		file << s->faces.size() << " ";
		for (unsigned int i = 0; i < s->faces.size(); ++i)
		{
			file << "\n";
			file << s->faces[i][0] << " ";
			file << s->faces[i][1] << " ";
			file << s->faces[i][2] << " ";
			file << s->GetEdge(s->faces[i][0], s->faces[i][1]) << " ";
			file << s->GetEdge(s->faces[i][1], s->faces[i][2]) << " ";
			file << s->GetEdge(s->faces[i][2], s->faces[i][0]) << " ";

			Vec n = s->GetNormalToFace(i);
			file << n.x << " ";
			file << n.y << " ";
			file << n.z << " ";
		}
		file.close();
		LoadShapesMenu();
	}
	dlg->Destroy();
}

void Coupic::OnMenuSaveCoupi(wxCommandEvent& event)
{
	double V;
	Vec I;
	StandardizeShapes(&V, &I);

	// loop through shape data to accumulate everything into the nice lists
	// that we'll write to file
	
	vector<Vec> faces_norm;
	vector<int> faces_ivert0;
	vector<int> faces_ivert1;
	vector<int> faces_ivert2;
	vector<int> faces_iedge0;
	vector<int> faces_iedge1;
	vector<int> faces_iedge2;

	vector<int> edges_itail;
	vector<int> edges_ihead;
	vector<int> edges_ilface;
	vector<int> edges_irface;

	vector<Vec> verts_local_pos;
	vector<int> verts_deg;
	vector<int> verts_iadj_e;
	
	vector<int> adj_e;

	vector<double> dil_rs;
	vector<Vec> sbb_cs;
	vector<double> sbb_Rs;
	vector<int> nverts;
	vector<int> nedges;
	vector<int> nfaces;
	vector<int> ivert0;
	vector<int> iedge0;
	vector<int> iface0;

	for (unsigned int s = 0; s < canvas->Shapes.size(); ++s)
	{
		unsigned int adj_e_offset = edges_itail.size();
		unsigned int verts_offset = verts_deg.size();
		unsigned int edges_offset = edges_itail.size();
		unsigned int faces_offset = faces_norm.size();

		Shape* shape = canvas->Shapes[s];

		dil_rs.push_back(shape->dil_r);
		sbb_cs.push_back(shape->SBB_center);
		sbb_Rs.push_back(shape->SBB_radius);
		nverts.push_back(shape->verts.size());
		nedges.push_back(shape->edges.size());
		nfaces.push_back(shape->faces.size());
		ivert0.push_back(verts_offset);
		iedge0.push_back(edges_offset);
		iface0.push_back(faces_offset);

		for (unsigned int i = 0; i < shape->points.size(); ++i)
		{
			vector<int> cur_adj_e = shape->GetAdjEdges(i);
			for (unsigned int j = 0; j < cur_adj_e.size(); ++j)
				cur_adj_e[j] += adj_e_offset;

			verts_local_pos.push_back(shape->points[i]);
			verts_deg.push_back(cur_adj_e.size());
			verts_iadj_e.push_back(adj_e.size());
			/* make sure we do this AFTER pushing on to iadj_e */
			VECTOR_APPEND(adj_e, cur_adj_e);
		}
		for (unsigned int i = 0; i < shape->edges.size(); ++i)
		{
			edges_itail.push_back(shape->edges[i][0] + verts_offset);
			edges_ihead.push_back(shape->edges[i][1] + verts_offset);
			edges_ilface.push_back(shape->GetLface(i) + faces_offset);
			edges_irface.push_back(shape->GetRface(i) + faces_offset);
		}
		for (unsigned int i = 0; i < shape->faces.size(); ++i)
		{
			faces_ivert0.push_back(shape->faces[i][0] + verts_offset);
			faces_ivert1.push_back(shape->faces[i][1] + verts_offset);
			faces_ivert2.push_back(shape->faces[i][2] + verts_offset);
			faces_iedge0.push_back(shape->GetEdge(shape->faces[i][0], shape->faces[i][1]) + edges_offset);
			faces_iedge1.push_back(shape->GetEdge(shape->faces[i][1], shape->faces[i][2]) + edges_offset);
			faces_iedge2.push_back(shape->GetEdge(shape->faces[i][2], shape->faces[i][0]) + edges_offset);
			faces_norm.push_back(shape->GetNormalToFace(i));
		}
	}

  wxFileDialog fsave(NULL, wxEmptyString, wxEmptyString,
        _("name"),
        _("Lua (*.lua)|*.lua"), wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
  if (fsave.ShowModal() != wxID_OK) return;
	
	/* get the file name and path we're going to save to */
	wxString fname = fsave.GetPath();
	/* get just the name of the file (the name we'll load it as in the hdisdesign lua table) */
  wxString name = fname.AfterLast(wxUniChar('/')).BeforeLast(wxUniChar('.'));
  
  FILE* fp = fopen(fname, "wb");

  fputs(_F(_("local hdis = require \"hdis\"")), fp);
  fputs(_F(_("\n")), fp);
  fputs(_F(_("\n-------------------------------------------------------------------")), fp);
  fputs(_F(_("\n-- THIS FUNCTION WAS AUTOMATICALLY GENERATED BY COUPIC v%s"), VERSION), fp);
  fputs(_F(_("\n-- input:")), fp);
  fputs(_F(_("\n--  r:     radius")), fp);
  fputs(_F(_("\n--  gname: group name")), fp);
  fputs(_F(_("\n--  mname: material name")), fp);
  fputs(_F(_("\n-------------------------------------------------------------------")), fp);
  fputs(_F(_("\nfunction hdis.hdisdesign.%s(r, gname, mname)"), name), fp);
  fputs(_F(_("\n")), fp);
  fputs(_F(_("\n  local group = hdis.atom.group:resolve( gname )")), fp);
  fputs(_F(_("\n  local imat  = hdis.material:resolve( mname )")), fp);
  fputs(_F(_("\n  local rho   = hdis.material.list[ imat ].rho")), fp);
  fputs(_F(_("\n")), fp);
  fputs(_F(_("\n  local body = {")), fp);
  fputs(_F(_("\n    v = { 0, 0, 0 },")), fp);
  fputs(_F(_("\n    w = { 0, 0, 0 },")), fp);
  fputs(_F(_("\n    m = %.5f*rho*r^3,"), V), fp);
  fputs(_F(_("\n    V = %.5f*r^3,"), V), fp);
  fputs(_F(_("\n    I = { %.5f*rho*r^5, %.5f*rho*r^5, %.5f*rho*r^5 },"), I.x, I.y, I.z), fp);
  fputs(_F(_("\n    q = { 1, 0, 0, 0 }")), fp);
  fputs(_F(_("\n  }")), fp);
  fputs(_F(_("\n")), fp);
  
  fputs(_F(_("\n  local atoms = {")), fp);
  for (unsigned int i = 0; i < dil_rs.size(); ++i)
  {
      fputs(_F(_("\n    {")), fp);
      fputs(_F(_("\n      R        = %.5f*r,"), dil_rs[i]), fp);
      fputs(_F(_("\n      group    = group,")), fp);
      fputs(_F(_("\n      material = imat,")), fp);
      fputs(_F(_("\n      sbb_c = { %.5f*r, %.5f*r, %.5f*r },"),
						sbb_cs[i].x,
						sbb_cs[i].y,
						sbb_cs[i].z), fp);
      fputs(_F(_("\n      sbb_R = %.5f*r,"), sbb_Rs[i]), fp);
      fputs(_F(_("\n      nverts = %d,"), nverts[i]), fp);
      fputs(_F(_("\n      nedges = %d,"), nedges[i]), fp);
      fputs(_F(_("\n      nfaces = %d,"), nfaces[i]), fp);
      fputs(_F(_("\n      ivert0 = %d,"), ivert0[i]+1), fp);
      fputs(_F(_("\n      iedge0 = %d,"), iedge0[i]+1), fp);
      fputs(_F(_("\n      iface0 = %d"), iface0[i]+1), fp);
      fputs(_F(_("\n    }%s"), i==(dil_rs.size()-1) ? _("") : _(",")), fp);
  }
  fputs(_F(_("\n  }")), fp);

	fputs(_F(_("\n  local faces = {")), fp);
  for (unsigned int i = 0; i < faces_norm.size(); ++i)
  {
      fputs(_F(_("\n    {")), fp);
      fputs(_F(_("\n      ivert0 = %d,"), faces_ivert0[i]+1), fp);
      fputs(_F(_("\n      ivert1 = %d,"), faces_ivert1[i]+1), fp);
      fputs(_F(_("\n      ivert2 = %d,"), faces_ivert2[i]+1), fp);
      fputs(_F(_("\n      iedge0 = %d,"), faces_iedge0[i]+1), fp);
      fputs(_F(_("\n      iedge1 = %d,"), faces_iedge1[i]+1), fp);
      fputs(_F(_("\n      iedge2 = %d,"), faces_iedge2[i]+1), fp);
      fputs(_F(_("\n      norm = { %.5f, %.5f, %.5f }"),
						faces_norm[i].x,
						faces_norm[i].y,
						faces_norm[i].z), fp);
      fputs(_F(_("\n    }%s"), i==(faces_norm.size()-1) ? _("") : _(",")), fp);
  }
  fputs(_F(_("\n  }")), fp);
	
	fputs(_F(_("\n  local edges = {")), fp);
  for (unsigned int i = 0; i < edges_itail.size(); ++i)
  {
      fputs(_F(_("\n    {")), fp);
      fputs(_F(_("\n      itail = %d,"), edges_itail[i]+1), fp);
      fputs(_F(_("\n      ihead = %d,"), edges_ihead[i]+1), fp);
      fputs(_F(_("\n      ilface = %d,"), edges_ilface[i]+1), fp);
      fputs(_F(_("\n      irface = %d"), edges_irface[i]+1), fp);
      fputs(_F(_("\n    }%s"), i==(edges_itail.size()-1) ? _("") : _(",")), fp);
  }
  fputs(_F(_("\n  }")), fp);

	fputs(_F(_("\n  local verts = {")), fp);
  for (unsigned int i = 0; i < verts_deg.size(); ++i)
  {
      fputs(_F(_("\n    {")), fp);
      fputs(_F(_("\n      iadj_e = %d,"), verts_iadj_e[i]+1), fp);
      fputs(_F(_("\n      deg = %d,"), verts_deg[i]), fp);
      fputs(_F(_("\n      local_pos = { %.5f*r, %.5f*r, %.5f*r },"),
						verts_local_pos[i].x,
						verts_local_pos[i].y,
						verts_local_pos[i].z), fp);
			fputs(_F(_("\n      global_pos = { %.5f*r, %.5f*r, %.5f*r }"),
						verts_local_pos[i].x,
						verts_local_pos[i].y,
						verts_local_pos[i].z), fp);
      fputs(_F(_("\n    }%s"), i==(verts_deg.size()-1) ? _("") : _(",")), fp);
  }
  fputs(_F(_("\n  }")), fp);

	fputs(_F(_("\n  local adj_e = {")), fp);
	for (unsigned int i = 0; i < adj_e.size(); ++i)
	{
		if (i%10 == 0)
			fputs(_F("\n      "), fp);
		fputs(_F(_("%d%s "), adj_e[i]+1, i==(adj_e.size()-1) ? _("") : _(",")), fp);
	}
  fputs(_F(_("\n  }")), fp);


  fputs(_F(_("\n")), fp);
  fputs(_F(_("\n  return {\n    body=body,\n    atoms=atoms,\n    points=points,\n    faces=faces,\n    edges=edges,\n    verts=verts,\n    adj_e=adj_e\n  }")), fp);
  fputs(_F(_("\n")), fp);
  fputs(_F(_("\nend")), fp);
  fclose(fp);
}

void Coupic::StandardizeShapes(double* V, Vec* I)
{
	double box_max = 1.1;
	double step = 0.02;

	ResizeShapes();
	CenterShapes(V, -box_max, box_max, step);
	RotateShapes(I, -box_max, box_max, step);

	for (unsigned int s = 0; s < canvas->Shapes.size(); ++s)
		canvas->Shapes[s]->SetSBB();

	Select(canvas->selected);
	canvas->Render();
}

void Coupic::ResizeShapes()
{
	Vec a, b;
	double max_dist = SetFurthestPoints(&a, &b);

	wxLogMessage("initial radius: %.3f", max_dist);

	// rescale all shapes so total radius of the compound shape is 1
	double scale = 2.0/max_dist;
	wxLogMessage("rescaling factor: %.3f", scale);

	for (unsigned int i = 0; i < canvas->Shapes.size(); ++i)
	{
		Shape* s = canvas->Shapes[i];
		s->dil_r *= scale;
		for (unsigned int p = 0; p < s->points.size(); ++p)
			Scale(&(s->points[p]), s->points[p], scale);
	}
}

double Coupic::SetFurthestPoints(Vec* a, Vec* b)
{
	vector<Vec> points;     // points in global coordinates
	vector<double> dil_rs;  // dil_r associated with each point

  // First pass to collect information
  for (unsigned int i = 0; i < canvas->Shapes.size(); ++i)
  {
    Shape* s = canvas->Shapes[i];
		for (unsigned int p = 0; p < s->points.size(); ++p)
    {
      points.push_back(s->points[p]);
      dil_rs.push_back(s->dil_r);
    }
  }
	
	// this metric makes sure the shape fits in a sphere with radius 1
	double max_dist = 0;
  for (unsigned int i = 0; i < points.size(); ++i)
	{
    for (unsigned int j = i; j < points.size(); ++j)
		{
			double dist = Dist(points[i], points[j]) + dil_rs[i] + dil_rs[j];
			if (dist > max_dist)
			{
				max_dist = dist;
				Set(a, points[i].x, points[i].y, points[i].z);
				Set(b, points[j].x, points[j].y, points[j].z);
			}
		}
	}
	return max_dist;
}

void Coupic::CenterShapes(double* V, double box_min, double box_max, double step)
{
  // determine center of mass
	Vec CM;
	FindVolumeAndCenter(V, &CM, box_min, box_max, step);

	// translate all shapes towards the center of mass
	for (unsigned int i = 0; i < canvas->Shapes.size(); ++i)
	{
		for (unsigned int p = 0; p < canvas->Shapes[i]->points.size(); ++p)
			Diff(&(canvas->Shapes[i]->points[p]), canvas->Shapes[i]->points[p], CM);
	}
}

void Coupic::FindVolumeAndCenter(double* V, Vec* CM, double box_min, double box_max, double step)
{
	double dv = pow(step, 3);
	*V = 0;
	Set(CM, 0, 0, 0);

	canvas->spheres.clear();
	canvas->sphere_radius = step;
	
	for (double x = box_min; x < box_max; x += step)
	{
		for (double y = box_min; y < box_max; y += step)
		{
			for (double z = box_min; z < box_max; z += step)
			{
				Vec p(x,y,z);
				bool internal = false;
				for (unsigned int s = 0; s < canvas->Shapes.size(); ++s)
				{
					if (canvas->Shapes[s]->Inside(p))
					{
						internal = true;
						break;
					}
				}
				if (internal)
				{
					canvas->spheres.push_back(p);
					*V += dv;
					Scale(&p, p, dv);
					Sum(CM, *CM, p);
				}
			}
		}
	}
	Scale(CM, *CM, 1.0 / *V);
	wxLogMessage("volume: \t%.4f", *V);
	wxLogMessage("center of mass: \t(%.2f, %.2f, %.2f)", CM->x, CM->y, CM->z);
}

void Coupic::RotateShapes(Vec* I, double box_min, double box_max, double step)
{
	// determine principal axes of rotation and moments about them
	double* I_XYZ = new double[9];
	FindMoment(I_XYZ, box_min, box_max, step);

	Matrix3d I_principal;
	for (int i = 0; i < 9; ++i) I_principal(i) = I_XYZ[i];
	delete I_XYZ;
	EigenSolver<Matrix3d> es(I_principal);

	// values off the main diagonal of our eigendecomposed tensor
	Set(I, es.eigenvalues()(0).real(), es.eigenvalues()(1).real(), es.eigenvalues()(2).real());

	wxLogMessage("I (principal axes): (%.3f, %.3f, %.3f)", I->x, I->y, I->z);

	// the eigenvectors (principal axes of rotation)
	Vec evec0(es.eigenvectors()(0).real(), es.eigenvectors()(1).real(), es.eigenvectors()(2).real());
	Vec evec1(es.eigenvectors()(3).real(), es.eigenvectors()(4).real(), es.eigenvectors()(5).real());
	Vec evec2(es.eigenvectors()(6).real(), es.eigenvectors()(7).real(), es.eigenvectors()(8).real());

	// the transformation matrix from XYZ coordinates to coordinates aligned with the principal axes
	// is just the matrix where each column is one of the eigenvector
	//
	// we multiply each shape position by this transformation matrix to rotate everything into the
	// correct orientation
	for (unsigned int s = 0; s < canvas->Shapes.size(); ++s)
	{
		// janky manual matrix multiplaction
		for (unsigned int p = 0; p < canvas->Shapes[s]->points.size(); ++p)
		{
			canvas->Shapes[s]->points[p] = Vec(Dot(canvas->Shapes[s]->points[p], evec0),
																				 Dot(canvas->Shapes[s]->points[p], evec1),
																				 Dot(canvas->Shapes[s]->points[p], evec2));
		}
		// since we just moved a bunch of stuff lets make sure the ordering is still skookum
		canvas->Shapes[s]->MakeCCW();
	}
}

void Coupic::FindMoment(double* I, double box_min, double box_max, double step)
{
	double dv = pow(step, 3);
	for (int i = 0; i < 9; ++i) I[i] = 0.0;

	for (double x = box_min; x < box_max; x += step)
	{
		for (double y = box_min; y < box_max; y += step)
		{
			for (double z = box_min; z < box_max; z += step)
			{
				Vec p(x,y,z);
				bool internal = false;
				for (unsigned int s = 0; s < canvas->Shapes.size(); ++s)
				{
					if (canvas->Shapes[s]->Inside(p))
					{
						internal = true;
						break;
					}
				}
				if (internal)
				{
					// compute inertia tensor contribution
					I[0] +=  dv * (y*y + z*z);
					I[1] += -dv * x*y;
					I[2] += -dv * x*z;
					I[3] += -dv * x*y;
					I[4] +=  dv * (x*x + z*z);
					I[5] += -dv * y*z;
					I[6] += -dv * x*z;
					I[7] += -dv * y*z;
					I[8] +=  dv * (x*x + y*y);
				}
			}
		}
	}
	wxLogMessage("I (unrotated):\n \t%.2f \t%.2f \t%.2f \n \t%.2f \t%.2f \t%.2f \n \t%.2f \t%.2f \t%.2f",
			I[0], I[1], I[2], I[3], I[4], I[5], I[6], I[7], I[8]);
}
