#ifndef CANVAS_HH
#define CANVAS_HH

#define GL_GLEXT_PROTOTYPES
#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glext.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#else
#include <GL/gl.h>
#include <GL/glext.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif
/* LINUX COMPATIBILITY:
      wx/glcanvas.h includes GL/glext.h, so if you need to define GL_GLEXT_PROTOTYPES,
      do it before including wx/glcanvas.h
      */
#include <wx/wx.h>
#include <wx/glcanvas.h>

#include <cmath>
#include <vector>
using namespace std;

#include "hdisdesign.hh"
#include "defs.hh"
#include "vec.hh"
#include "shape.hh"
#include "dialog.hh"

class Coupic;

class Canvas : public wxGLCanvas
{
public:
  Canvas(wxWindow *parent, Coupic* hdisdesign,
        wxWindowID id = wxID_ANY,
        const wxPoint& pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize, long style = 0,
        const wxString& name = wxT("COUPiC"));

  wxGLContext* context;
  vector<Shape*> Shapes;
  Coupic* c;
  Vec eye;
  Vec over;
  Vec up;
	Vec center;

	vector<Vec> spheres;
	double sphere_radius;
	int draw_mode;
	bool dilate;
	bool SBB;
	bool shift_held;
	bool building_new_shape;
	bool only_draw_sel;
	vector<int> new_shape_face;

	vector<Vec> mesh_points;
	vector<unsigned int> mesh_triangles;

  double zoom;
  int mouseX;
  int mouseY;

	bool shift_pressed;
  int mouse_move_effect;

  int selected;
	int selected_vertex;
  double theta;

  double translate_speed;
  double rotate_speed;

  bool InitGL();
  void SetProjection();
  void SetModelview();

  void OnPaint(wxPaintEvent& event);
  void Render(bool picking = false);
  void DrawGrid();
	void DrawSBB(int i);
	void DrawShape(int s_index, bool with_dilation, bool picking);
	void DrawFace(bool with_dilation, double r, Vec p0, Vec p1, Vec p2);
	void DrawEdge(bool with_dilation, double r, Vec p0, Vec p1);
	void DrawVert(bool with_dilation, double r, Vec p0);

	void DrawMesh(bool picking);
	void AddNewPoint(Vec p);
	void AddExistingPoint(unsigned int i);
	void RemoveExistingPoint();

  void NewShape(int type);
  void TranslateShape(double delta, Vec axis);
  void MoveShapeTo(Vec p);
  void RotateShape(double theta, Vec axis);
  void ResizeShape(double delta);
  void AdjustShape(Vec old_normal);

  void OnMouseDown(wxMouseEvent& event);
  void OnMouseUp(wxMouseEvent& event);
  void OnMouseMove(wxMouseEvent& event);
  void OnKeyPress(wxKeyEvent& event);
  void OnKeyUp(wxKeyEvent& event);
  void KeyWithShift(int key);
  void KeyWithCtrl(int key);
};

int GetN();

#endif
