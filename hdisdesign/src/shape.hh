#ifndef SHAPE_HH
#define SHAPE_HH

#define GL_GLEXT_PROTOTYPES
#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glext.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#else
#include <GL/gl.h>
#include <GL/glext.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif

#include <wx/wx.h>

#include <vector>
using namespace std;

#include "defs.hh"
#include "vec.hh"

class Shape
{
public:
  Shape(double in_dil_r);
	Shape(double in_dil_r, vector<Vec> in_points, vector< vector<int> > in_faces,
																								vector< vector<int> > in_edges,
																								vector< vector<int> > in_verts);

  vector<Vec> points;
  vector< vector<int> > faces;
	vector< vector<int> > edges;
	vector< vector<int> > verts;
  double dil_r;

  bool Inside(Vec p);
	void MakeCCW();
	void Update();
	void SetSBB();
	bool IsConvex();
	Vec GetInternalPoint();

	Vec SBB_center;
	double SBB_radius;

	vector<int> GetAdjEdges(int vert_i);
	int GetDegree(int vert_i);

	int GetLface(int edge_i);
	int GetRface(int edge_i);

	int GetEdge(int i, int j);

	void SetSphere(double r);
	void SetTetra(double r);
	void SetCube(double r);
	void SetOcta(double r);
	void SetRightTetra(double r);
	void SetTface(double r);
	void SetNgon(double r, int n);

	Vec GetNormalToFace(int i);
};

Vec GetNormal(Vec p0, Vec p1, Vec p2);

/*
class Sphere : public Shape
{
public:
  Sphere(double r);
  double Volume();
	bool Inside(Vec p);
};

class Cube : public Shape
{
public:
  Cube(double r, double dr);
  double Volume();
	bool Inside(Vec p);
};

class Tetra : public Shape
{
public:
  Tetra(double r, double dr);
  double Volume();
};

class Caps : public Shape
{
public:
  Caps(double r, double dr);
  double Volume();
};
*/

#endif

