#include "canvas.hh"

Canvas::Canvas(wxWindow *parent, Coupic* hdisdesign,
             wxWindowID id, const wxPoint& pos, const wxSize& size,
             long style, const wxString& name)
    : wxGLCanvas(parent, id, 0, pos, size,
                 style | wxFULL_REPAINT_ON_RESIZE | wxWANTS_CHARS, name, wxNullPalette)
{
	mouse_move_effect = IDLE;
  shift_pressed = false;
	dilate = true;
	SBB = false;
  mouseX = 0;
  mouseY = 0;
  c = hdisdesign;
	// orthonormal camera reference frame
  Set(&eye,  1.0, 0.0, 0.0);
  Set(&over, 0.0, 1.0, 0.0);
  Set(&up,   0.0, 0.0, 1.0);
  zoom = 10;
	building_new_shape = false;
	only_draw_sel = false;

	draw_mode = DRAW_MODE_NORMAL;

  rotate_speed = 1.3;
  translate_speed = 0.1;

  context = new wxGLContext(this);

  Connect(wxEVT_PAINT, wxPaintEventHandler(Canvas::OnPaint));
  Connect(wxEVT_LEFT_DOWN, wxMouseEventHandler(Canvas::OnMouseDown));
  Connect(wxEVT_LEFT_UP, wxMouseEventHandler(Canvas::OnMouseUp));
  Connect(wxEVT_MOTION, wxMouseEventHandler(Canvas::OnMouseMove));
  Connect(wxEVT_KEY_DOWN, wxKeyEventHandler(Canvas::OnKeyPress));
  Connect(wxEVT_KEY_UP, wxKeyEventHandler(Canvas::OnKeyUp));
}

bool Canvas::InitGL()
{
  SetCurrent(*context);

  glShadeModel(GL_SMOOTH);
  glEnable(GL_NORMALIZE);

  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);

  glEnable(GL_COLOR_MATERIAL);
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);

  char** null_argv = NULL;
  int null_argc = 0;
  glutInit(&null_argc, null_argv);

  return true;
}

void Canvas::SetProjection()
{
	// reset the projection matrix
  int w, h;
  GetClientSize(&w, &h);
  glViewport(0, 0, (GLint) w, (GLint) h);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(15.0f, (GLfloat)w/(GLfloat)h, 0.1, 1000.0);
}

void Canvas::SetModelview()
{
	// reset the modelview matrix
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  GLfloat pos0[] = {0, 0, 1, 0};
  glLightfv(GL_LIGHT0, GL_POSITION, pos0);
	glTranslatef(center.x, center.y, center.z);
  gluLookAt(eye.x*zoom, eye.y*zoom, eye.z*zoom, 0, 0, 0, up.x, up.y, up.z);
}

void Canvas::OnPaint(wxPaintEvent& WXUNUSED(event)) { Render(); }

void Canvas::Render(bool picking)
{
  // must always be here
  SetCurrent(*context);
  wxClientDC dc(this);

  SetProjection();
  SetModelview();

  glClearColor(1.0, 1.0, 1.0, 1.0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// we hide the grid during click detection to prevent interference
	if (!picking)
		DrawGrid();
	else
		glDisable(GL_LIGHTING);

	if (draw_mode == DRAW_MODE_NORMAL)
	{
		// if we're picking and building, hide all shapes since DrawMesh handles
		// what needs to be drawn
		if (!picking || !building_new_shape)
		{
			for (unsigned int i = 0; i < Shapes.size(); ++i)
			{
				if (!only_draw_sel || i == selected)
					DrawShape(i, dilate, picking);
				// only render sbbs if they're enabled and we're not selecting a shape
				// (so they don't get in the way of the color selection method)
				if (SBB && !picking)
					DrawSBB(i);
			}
		}

		DrawMesh(picking);

		// leave everything colored with the picking colors in the back buffer where it is
		// never rendered on screen
		if (!picking) SwapBuffers();
	}
	else if (draw_mode == DRAW_MODE_SPHERES && !picking)
	{
		unsigned int nsph = spheres.size();
		glColor4f(1, 0, 0, 1);
		for (unsigned int i = 0; i < nsph; ++i)
		{
			glColor3f(i%(nsph/10)/(nsph/10.0), i%(nsph/20)/(nsph/20.0), i%(nsph/5)/(nsph/5.0));
			glPushMatrix();
			glTranslatef(spheres[i].x, spheres[i].y, spheres[i].z);
			glutSolidCube(sphere_radius);
			glPopMatrix();
		}
		SwapBuffers();
	}
	else
	{
		wxLogMessage("Unknown value for draw_mode: %d", draw_mode);
	}
	if (picking)
		glEnable(GL_LIGHTING);
}

void Canvas::DrawMesh(bool picking)
{
	if (picking)
	{
		// draw all the existing verts in the current shape
		if (building_new_shape)
		{
			DrawShape(selected, true, picking);
			for (unsigned int i = 0; i < mesh_points.size(); ++i)
			{
				glColor4ub(MESH, i/256, i%256, 255);
				DrawVert(true, 0.02, mesh_points[i]);
			}
		}
	}
	else
	{
		glColor4f(1,1,0,0.8);

		glBegin(GL_TRIANGLES);
		for (unsigned int i = 0; i < mesh_triangles.size()/3; ++i)
		{
			Vec p0, p1, p2;

			p0 = mesh_points[mesh_triangles[i*3+0]];
			p1 = mesh_points[mesh_triangles[i*3+1]];
			p2 = mesh_points[mesh_triangles[i*3+2]];

			Vec normal;
			Triangle_X(&normal, p0, p2, p1);

			glNormal3f(normal.x, normal.y, normal.z);
			
			glVertex3f(p0.x, p0.y, p0.z);
			glVertex3f(p1.x, p1.y, p1.z);
			glVertex3f(p2.x, p2.y, p2.z);
		}
		glEnd();
	}
}

void Canvas::DrawSBB(int i)
{
	glColor4f(0.8, 0.8, 0.8, 0.2);
	glPushMatrix();
	int subdivs = 15;
	glTranslatef(Shapes[i]->SBB_center.x, Shapes[i]->SBB_center.y, Shapes[i]->SBB_center.z);
	gluSphere(gluNewQuadric(), Shapes[i]->SBB_radius, subdivs, subdivs);
	glPopMatrix();
}

void Canvas::DrawGrid()
{
  int n = 3;
  double step = 2/((float)n-1);

  glLineWidth(1);
  glColor4f(0.0, 0.0, 0.0, 0.2);
	// make sure coloring doesn't change as the camera rotates
  glNormal3f(eye.x, eye.y, eye.z);
  glBegin(GL_LINES);
  for (int x = 0; x < n; ++x)
  {
    for (int y = 0; y < n; ++y)
    {
      glVertex3f(-1.0 + x*step, -1.0 + y*step, -1.0);
      glVertex3f(-1.0 + x*step, -1.0 + y*step,  1.0);
    }
  }
  for (int y = 0; y < n; ++y)
  {
    for (int z = 0; z < n; ++z)
    {
      glVertex3f(-1.0, -1.0 + y*step, -1.0 + z*step);
      glVertex3f( 1.0, -1.0 + y*step, -1.0 + z*step);
    }
  }
  for (int z = 0; z < n; ++z)
  {
    for (int x = 0; x < n; ++x)
    {
      glVertex3f(-1.0 + x*step, -1.0, -1.0 + z*step);
      glVertex3f(-1.0 + x*step,  1.0, -1.0 + z*step);
    }
  }
  glEnd();
}

void Canvas::TranslateShape(double delta, Vec axis)
{
  if (selected < 0) return;

  Vec n(axis.x, axis.y, axis.z);
  Scale(&n, n, delta);
	Sum(&(Shapes[selected]->SBB_center), Shapes[selected]->SBB_center, n);
	for (unsigned int i = 0; i < Shapes[selected]->points.size(); ++i)
		Sum(&(Shapes[selected]->points[i]), Shapes[selected]->points[i], n);
  c->Select(selected); // to update all the sliders
}

void Canvas::MoveShapeTo(Vec p)
{
	if (selected < 0) return;
	Vec change;
	Diff(&change, p, Shapes[selected]->SBB_center);
	Sum(&(Shapes[selected]->SBB_center), Shapes[selected]->SBB_center, change);
	for (unsigned int i = 0; i < Shapes[selected]->points.size(); ++i)
		Sum(&(Shapes[selected]->points[i]), Shapes[selected]->points[i], change);
}

void Canvas::RotateShape(double theta, Vec axis)
{
  if (selected < 0) return;

  for (unsigned int i = 0; i < Shapes[selected]->points.size(); ++i)
    Rotate(&(Shapes[selected]->points[i]), axis, theta);
}

void Canvas::ResizeShape(double n)
{
  if (selected < 0) return;
  Shape* s = Shapes[selected];
	s->dil_r *= n;
	s->SBB_radius *= n;
  for (unsigned int p = 0; p < s->points.size(); ++p)
    Scale(&(s->points[p]), s->points[p], n);
}

void Canvas::OnMouseDown(wxMouseEvent& event)
{
  event.Skip();
  mouseX = event.GetX();
  mouseY = event.GetY();

	// color picking - the render function draws everything with unique colors,
	// then we get the pixel at the mouse's location and check the color to
	// find out what it is
  int x = mouseX;
  int y = GetSize().GetHeight() - mouseY;
  Render(true);
  GLint viewport[4];
  GLubyte pixel[3];
  GLfloat depth;
  glGetIntegerv(GL_VIEWPORT , viewport);

  glReadPixels(x, y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, (void *)pixel);
  glReadPixels(x, y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &depth);

	wxLogMessage("Clicked pixel: [%u, %u, %u]", pixel[0], pixel[1], pixel[2]);

	// check if we clicked on something
	// the first channel is the index of clicked shape, or a flag for if it's the mesh
	// the second channel is the type of feature
	// the third channel is the index of that feature
	
	// first check to see if we clicked a shape
  if (pixel[0] < Shapes.size())
  {
    c->Select(pixel[0]);

		if (pixel[1] == VERT)
		{
			if (building_new_shape)
			{
				AddExistingPoint(pixel[2]);
			}
			else
			{
				selected_vertex = pixel[2];
				mouse_move_effect = TRANSLATE_VERTEX;
			}
		}
		// in building mode we only want to recognize clicking the mesh and vertices
		else if (!building_new_shape)
		{
			if (shift_pressed) mouse_move_effect = ROTATE_SHAPE;
			else mouse_move_effect = TRANSLATE_SHAPE;
		}
  }
	// then check to see if we clicked a trianglular mesh
	else if (pixel[0] == MESH)
	{
		// in order to accomodate enough unique colors for mesh verts,
		// we use the second and third color channels so we can
		// support up to 256^2 verts
		//
		// the pixels are colored: [MESH, i/256, i%256]
		if (building_new_shape && shift_pressed)
			AddNewPoint(mesh_points[pixel[1]*256 + pixel[2]]);
	}
	// otherwise we just rotate
	else
	{
		mouse_move_effect = ROTATE_WORLD;
	}
  Render();
}

void Canvas::OnMouseUp(wxMouseEvent& event)
{
  event.Skip();
	mouse_move_effect = IDLE;
}

void Canvas::OnMouseMove(wxMouseEvent& event)
{
  event.Skip(); // continue to process other events while we drag the mouse
	
	double dx = event.GetX() - mouseX;
	double dy = event.GetY() - mouseY;

	// this function just adjusts small and large movements closer to what
	// we expect the user actually wants to do
	dx = dx == 0 ? 0 : 0.2/(1 + pow(2.5, -0.5*abs(dx) + 5))*abs(dx)/dx;
	dy = dy == 0 ? 0 : 0.2/(1 + pow(2.5, -0.5*abs(dy) + 5))*abs(dy)/dy;
  
	if (mouse_move_effect == ROTATE_WORLD)
  {
		// we update the over camera vector based on dx
    Vec change(eye.x, eye.y, eye.z);
    Scale(&change, eye, dx);
    Sum(&over, over, change);
    X(&eye, over, up);

		// and the up camera vector based on dy
    Scale(&change, up, dy);
    Sum(&eye, eye, change);
    X(&up, eye, over);

		// and make sure everything is still orthonormal
    Normalize(&eye, eye);
    Normalize(&up, up);
    Normalize(&over, over);
  }
  if (mouse_move_effect == ROTATE_SHAPE)
  {
		Shape* s = Shapes[selected];

		/* translate the shape to the origin */
		for (unsigned int i = 0; i < s->points.size(); ++i)
		{
			Diff(&(s->points[i]), s->points[i], s->SBB_center);
		}
		/* perform the rotation */
		RotateShape(dx * rotate_speed, up);
		RotateShape(dy * rotate_speed, over);
		/* translate shape back to its center */
		for (unsigned int i = 0; i < s->points.size(); ++i)
		{
			Sum(&(s->points[i]), s->points[i], s->SBB_center);
		}
		/* we don't need to adjust the SBB due to the way it's constructed (i.e. not minimal) */
	}
	if (mouse_move_effect == TRANSLATE_SHAPE || mouse_move_effect == TRANSLATE_VERTEX)
	{
    GLint viewport[4];
    GLdouble modelview[16];
    GLdouble projection[16];

		Vec screen_old;
		Vec screen_new;
		Vec world_old;
		Vec world_new;
 
    glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
    glGetDoublev(GL_PROJECTION_MATRIX, projection);
    glGetIntegerv(GL_VIEWPORT, viewport);

		Vec depth_dummy;
		if (mouse_move_effect == TRANSLATE_SHAPE) selected_vertex = 0;
		depth_dummy = Shapes[selected]->points[selected_vertex];

		double depth, unused;
		// project the shape location into screen coordinates so we can use the correct depth
    gluProject(depth_dummy.x, depth_dummy.y, depth_dummy.z,
               modelview, projection, viewport,
               &unused, &unused, &depth);

    Set(&screen_old, mouseX, GetSize().GetHeight() - mouseY, depth);
    Set(&screen_new, event.GetX(), GetSize().GetHeight() - event.GetY(), depth);

		// the project the screen clicks into world coordinates
    gluUnProject(screen_old.x, screen_old.y, screen_old.z,
                 modelview, projection, viewport,
                 &world_old.x, &world_old.y, &world_old.z);

    gluUnProject(screen_new.x, screen_new.y, screen_new.z,
                 modelview, projection, viewport,
                 &world_new.x, &world_new.y, &world_new.z);

		Vec world_change;
		Diff(&world_change, world_new, world_old);

		if (mouse_move_effect == TRANSLATE_SHAPE)
		{
			Sum(&(Shapes[selected]->SBB_center), Shapes[selected]->SBB_center, world_change);
			for (unsigned int i = 0; i < Shapes[selected]->points.size(); ++i)
			{
				Sum(&(Shapes[selected]->points[i]), Shapes[selected]->points[i], world_change);
			}
			c->Select(selected);
		}
		else
		{
			Sum(&(Shapes[selected]->points[selected_vertex]),
					Shapes[selected]->points[selected_vertex], world_change);
			// disable putting it back so it just warns instead of stopping concave shapes
			if (0 && Shapes[selected]->IsConvex() == false)
			{
				// put it back
				Diff(&(Shapes[selected]->points[selected_vertex]),
						Shapes[selected]->points[selected_vertex], world_change);
			}
			else
			{
				// change sticks, make sure we didn't flip any normals
				Shapes[selected]->Update();
				c->Select(selected);
			}
		}
  }

	Render();
  mouseX = event.GetX();
  mouseY = event.GetY();
}

void Canvas::OnKeyUp(wxKeyEvent& event)
{
	event.Skip();
	shift_pressed = event.ShiftDown();
}

void Canvas::OnKeyPress(wxKeyEvent& event)
{
	double translate_speed = 0.01;
	double R;
	double V;
	Vec CM;
  event.Skip(); // so we don't get hung up when we press ctrl/shift
  int key = event.GetKeyCode();
	shift_pressed = event.ShiftDown();
  if (shift_pressed) { KeyWithShift(key); return; }
  if (event.ControlDown()) { KeyWithCtrl(key); return; }
  switch (key)
  {
    case WXK_ESCAPE: if (!building_new_shape) c->Select(-1);
										 break;
    case WXK_UP:    if (!building_new_shape && selected >= 0)
                      TranslateShape(translate_speed, up);
										else
											center.y += translate_speed;
                    break;
    case WXK_DOWN:  if (!building_new_shape && selected >= 0)
                      TranslateShape(-translate_speed, up);
										else
											center.y -= translate_speed;
                    break;
    case WXK_LEFT:  if (!building_new_shape && selected >= 0)
                      RotateShape(rotate_speed, up);
										else
											center.x -= translate_speed;
                    break;
    case WXK_RIGHT: if (!building_new_shape && selected >= 0)
                      RotateShape(-rotate_speed, up);
										else
											center.x += translate_speed;
                    break;
    case WXK_DELETE: case WXK_BACK:
                    {
											if (!building_new_shape)
											{
												if (selected >= 0) Shapes.erase(Shapes.begin()+selected);
												// if we deleted the last shape make sure we still have a valid index
												if (selected >= (int)Shapes.size()) c->Select(selected-1);
											}
                      break;
                    }
		case 'S': draw_mode == DRAW_MODE_NORMAL ? draw_mode = DRAW_MODE_SPHERES : draw_mode = DRAW_MODE_NORMAL; break;
		case 'X':
							c->FindVolumeAndCenter(&V, &CM, -1.7, 1.7, 0.02);
							break;
		case 'C':
							{
								building_new_shape = !building_new_shape;
								if (building_new_shape)
								{
									Shape* s = new Shape(0.03);
									Shapes.push_back(s);
									s->SetSBB();
									c->Select(Shapes.size()-1);
									Render();
								}
								else
								{
									new_shape_face.clear();
								}
								break;
							}
		case 'M': c->ClearMesh(); break;
		//case 'D': dilate = !dilate; break;
		case 'B': SBB = !SBB; break;
    case '1': NewShape(SPHERE); break;
    case '2': NewShape(OCTA); break;
    case '3': NewShape(CUBE); break;
    case '4': NewShape(TETRA); break;
		case '5': 
							{
								NewShape(SPHERE);
								NewShape(SPHERE);
								NewShape(SPHERE);
								NewShape(SPHERE);
								R = 0.288675134594813;
								selected = 0;
								MoveShapeTo(Vec(-R,R,R));
								selected = 1;
								MoveShapeTo(Vec(R,-R,R));
								selected = 2;
								MoveShapeTo(Vec(-R,-R,-R));
								selected = 3;
								MoveShapeTo(Vec(R,R,-R));
								break;
							}
		case '6': NewShape(NGON); break;
		case '[': zoom *= 1.1; break;
		case ']': zoom *= 0.9; break;
		case 'U': RemoveExistingPoint(); break;
		case 'H': only_draw_sel = !only_draw_sel; break;
    default: break;

  }
  Render();
}

void Canvas::NewShape(int type)
{
	Shape* s = new Shape(0.1);
	int n = 0;
  switch (type)
  {
		case SPHERE:
		{
			s->SetSphere(0.5);
			break;
		}
		case OCTA:
		{
			s->SetOcta(0.5);
			break;
		}
    case TETRA:
		{
			s->SetTetra(0.5);
			break;
		}
		case CUBE:
		{
			s->SetCube(0.5);
			break;
		}
		case TFACE:
		{
			s->SetTface(0.5);
			break;
		}
		case NGON:
		{
			n = GetN();
			s->SetNgon(0.5, n);
			break;
		}
    default: wxMessageBox(_("Attempted to create unknown shape type.")); return; break;
  }
	Shapes.push_back(s);
	s->SetSBB();
  c->Select(Shapes.size()-1);
  Render();
}

int GetN()
{
	int n = 0;
	NgonDialog dlg;
	if (dlg.ShowModal() == wxID_OK)
	{
		n = (unsigned int) wxAtoi(dlg.n_text->GetValue());
	}
	return n;
}

void Canvas::KeyWithShift(int key)
{
  if (selected < 0) return;
  switch(key)
  {
    case WXK_UP:    ResizeShape(1.1);
  									c->Select(selected); // to update all the sliders
										break;
    case WXK_DOWN:  ResizeShape(0.9); break;
  									c->Select(selected); // to update all the sliders
										break;
    default: break;
  }
  Render();
}

void Canvas::KeyWithCtrl(int key)
{
  if (selected < 0) return;
  switch(key)
  {
    case WXK_UP:    TranslateShape(translate_speed, up); break;
    case WXK_DOWN:  TranslateShape(-translate_speed, up); break;
    case WXK_RIGHT: TranslateShape(translate_speed, over); break;
    case WXK_LEFT:  TranslateShape(-translate_speed, over); break;
    default: break;
  }
  Render();
}

void Canvas::DrawShape(int s_index, bool with_dilation, bool picking)
{
	bool sel = (s_index == selected);
	Shape* s = Shapes[s_index];

	if (building_new_shape)
		glColor4f(0.2 + 0.5*sel, 0.2, 0.2, 0.2 + 0.8*sel);
	else
		glColor3f(0.2 + 0.5*sel, 0.2, 0.2);

  for (unsigned int i = 0; i < s->verts.size(); ++i)
  {
		if (picking) glColor4ub(s_index, VERT, i, 255);
		DrawVert(with_dilation || picking, s->dil_r*(1 + 0.5*building_new_shape*picking), s->points[s->verts[i][0]]);
	}
	for (unsigned int i = 0; i < s->edges.size(); ++i)
	{
		if (picking) glColor4f(s_index, EDGE, i, 255);
		// shrink the edge by 50% in picking mode to make it easier to click on verts
		DrawEdge(with_dilation, s->dil_r*(1-0.5*picking), s->points[s->edges[i][0]],
																											s->points[s->edges[i][1]]);
	}
	for (unsigned int i = 0; i < s->faces.size(); ++i)
	{
		if (picking)
		{
			glColor4ub(s_index, FACE, i, 255);
		}
		else
		{
			// double d = (float)i/(float)(s->faces.size() - 1);
			// color all the faces differently so they're easy to distinguish
			// glColor3f(d + sel*(-d + (i%5)/4.0), d + sel*(1 - d - (i%7)/6.0), d + sel*(1 - d - (i%11)/10.0));
		}
		DrawFace(with_dilation, s->dil_r, s->points[s->faces[i][0]],
																			s->points[s->faces[i][1]],
																			s->points[s->faces[i][2]]);
  }
}

void Canvas::DrawFace(bool with_dilation, double r, Vec p0, Vec p1, Vec p2)
{
  Vec v;
	Vec n = GetNormal(p0, p1, p2);
	if (Dot(n, eye) < 0) Scale(&n, n, -1);

	if (with_dilation)
	{
		Scale(&n, n, r);
		glBegin(GL_TRIANGLES);
		glNormal3f(n.x, n.y, n.z);
		Sum(&v, p0, n); glVertex3f(v.x, v.y, v.z);
		Sum(&v, p1, n); glVertex3f(v.x, v.y, v.z);
		Sum(&v, p2, n); glVertex3f(v.x, v.y, v.z);
		Scale(&n, n, -1);
		glNormal3f(n.x, n.y, n.z);
		Sum(&v, p0, n); glVertex3f(v.x, v.y, v.z);
		Sum(&v, p1, n); glVertex3f(v.x, v.y, v.z);
		Sum(&v, p2, n); glVertex3f(v.x, v.y, v.z);
		glEnd();
	}
	else
	{
		glBegin(GL_TRIANGLES);
		glNormal3f(n.x, n.y, n.z);
		glVertex3f(p0.x, p0.y, p0.z);
		glVertex3f(p1.x, p1.y, p1.z);
		glVertex3f(p2.x, p2.y, p2.z);
		glEnd();
	}
}

void Canvas::DrawEdge(bool with_dilation, double r, Vec p0, Vec p1)
{
	if (with_dilation)
	{
		int subdivs = 15;
		Vec a;
		Diff(&a, p1, p0);
		double base = Length(a);

		glPushMatrix();

		glTranslatef(p0.x, p0.y, p0.z);
		glRotatef(180*acos(a.z/base)/M_PI, -a.y, a.x, 0);
		gluCylinder(gluNewQuadric(), r, r, base, subdivs, subdivs);

		glPopMatrix();
	}
}

void Canvas::DrawVert(bool with_dilation, double r, Vec p0)
{
	if (with_dilation)
	{
		int subdivs = 10;
		glPushMatrix();

		glTranslatef(p0.x, p0.y, p0.z);
		gluSphere(gluNewQuadric(), r, subdivs, subdivs);

		glPopMatrix();
	}
}

void Canvas::AddNewPoint(Vec p)
{
	Shape* s = Shapes[selected];
	unsigned int ip = s->points.size();

	s->points.push_back(p);

	vector<int> v;
	v.push_back(ip);
	s->verts.push_back(v);

	wxLogMessage("Added new point (%.3f %.3f %.3f)", p.x, p.y, p.z);

	AddExistingPoint(ip);
}

void Canvas::AddExistingPoint(unsigned int i)
{
	// we reset the vector when trying to add a fourth point instead of clearing
	// after adding the third to make sure we can undo a face creation
	if (new_shape_face.size() >= 3)
		new_shape_face.clear();
	
	// check to prevent degenerate faces/edges
	for (unsigned int j = 0; j < new_shape_face.size(); j++)
	{
		if ((unsigned int) new_shape_face[j] == i)
		{
			wxLogMessage("Degenerate point rejected");
			return;
		}
	}

	Shape* s = Shapes[selected];
	new_shape_face.push_back(i);
	unsigned int new_shape_npoints = new_shape_face.size();

	if (new_shape_npoints == 2)
	{
		// if the edge doesn't exist already
		if (s->GetEdge(new_shape_face[0], new_shape_face[1]) == -1)
		{
			vector<int> e0;
			e0.push_back(new_shape_face[0]);
			e0.push_back(new_shape_face[1]);
			s->edges.push_back(e0);
		}
	}

	if (new_shape_npoints == 3)
	{
		if (s->GetEdge(new_shape_face[2], new_shape_face[0]) == -1)
		{
			vector<int> e1;
			e1.push_back(new_shape_face[2]);
			e1.push_back(new_shape_face[0]);
			s->edges.push_back(e1);
		}

		if (s->GetEdge(new_shape_face[2], new_shape_face[1]) == -1)
		{
			vector<int> e2;
			e2.push_back(new_shape_face[2]);
			e2.push_back(new_shape_face[1]);
			s->edges.push_back(e2);
		}

		vector<int> f;
		f.push_back(new_shape_face[0]);
		f.push_back(new_shape_face[1]);
		f.push_back(new_shape_face[2]);
		s->faces.push_back(f);
	}

	wxLogMessage("Added existing point %u, currently have %u points", i, new_shape_npoints);
}

void Canvas::RemoveExistingPoint()
{
	if (building_new_shape && new_shape_face.size() > 0)
	{
		int i = new_shape_face.back();
		wxMessageBox(wxString::Format(_("deleting %d"), i));
		new_shape_face.pop_back();

		Shape* s = Shapes[selected];

		for (int j = 0; j < (int) s->verts.size(); ++j)
		{
			if (s->verts[j][0] == i)
			{
				// if it was the mostly recently point added we can pop it off the
				// points vector without messing up indexing
				if (j == s->verts.size() - 1)
				{
					s->points.pop_back();
				}

				s->verts.erase(s->verts.begin() + j);
				j--;
			}
		}

		for (int j = 0; j < (int) s->edges.size(); ++j)
		{
			if (s->edges[j][0] == i || s->edges[j][1] == i)
			{
				s->edges.erase(s->edges.begin() + j);
				j--;
			}
		}

		for (int j = 0; j < (int) s->faces.size(); ++j)
		{
			if (s->faces[j][0] == i || s->faces[j][1] == i || s->faces[j][2] == i)
			{
				s->faces.erase(s->faces.begin() + j);
				j--;
			}
		}

		wxMessageBox(_("all done"));
	}
	else
	{
		wxLogMessage("Couldn't find point to remove");
	}
}
