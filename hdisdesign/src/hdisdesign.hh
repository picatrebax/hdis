#ifndef WXCOUPIC_HH
#define WXCOUPIC_HH

#include <wx/wx.h>
#include <wx/glcanvas.h>
#include <wx/valnum.h>
#include <wx/textdlg.h>
#include <wx/stdpaths.h>
#include <wx/dir.h>

#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

/* dirty workaround for using Eigen on linux */
#undef Success
#include <Eigen/Dense>
using namespace Eigen;

#include "defs.hh"
#include "vec.hh"
#include "canvas.hh"

class Canvas;

class Coupic : public wxApp
{
public:
  virtual bool OnInit();

  wxFrame* CanvasFrame;
  Canvas* canvas;
	wxTextCtrl* log_text;
	wxLogTextCtrl* log;
	wxMenu* shapes_menu;

	/* values for the text controls */
	float cur_x;
	float cur_y;
	float cur_z;

  void CreateCanvasFrame();
  void AddMenus(wxFrame* frame);
	void LoadShapesMenu();

  void Select(int n);

	void StandardizeShapes(double* V, Vec* I);
	void ResizeShapes();
	double SetFurthestPoints(Vec* a, Vec* b);
	void CenterShapes(double* V, double box_min, double box_max, double step);
	void RotateShapes(Vec* I, double box_min, double box_max, double step);
	
	void FindVolumeAndCenter(double* V, Vec* CM, double box_min, double box_max, double step);
	void FindMoment(double* I, double box_min, double box_max, double step);

  void OnMenuAbout(wxCommandEvent& event);
  void OnMenuQuit(wxCommandEvent& event);
  void OnMenuSaveCoupi(wxCommandEvent& event);
	void OnMenuSaveCoupic(wxCommandEvent& event);
	void OnMenuLoadShape(wxCommandEvent& event);
	void OnMenuImportMesh(wxCommandEvent& event);
	void OnMenuClearMesh(wxCommandEvent& event);
	void LoadShape(wxString name);

	void ClearMesh();
	void ResizeMesh(double x);

  void OnTranslateSpeed(wxCommandEvent& event);
  void OnRotateSpeed(wxCommandEvent& event);
  void OnRadius(wxCommandEvent& event);
  void OnDilR(wxCommandEvent& event);
  void OnDilRText(wxCommandEvent& event);
  void OnCenterX(wxCommandEvent& event);
  void OnCenterY(wxCommandEvent& event);
  void OnCenterZ(wxCommandEvent& event);
  void OnCenterXText(wxCommandEvent& event);
  void OnCenterYText(wxCommandEvent& event);
  void OnCenterZText(wxCommandEvent& event);
  void OnNewSphere(wxCommandEvent& event);
  void OnNewCapsule(wxCommandEvent& event);
  void OnNewCube(wxCommandEvent& event);
  void OnNewTetra(wxCommandEvent& event);
};

#endif
