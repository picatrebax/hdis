#ifndef VEC_HH
#define VEC_HH

#include <cmath>
using namespace std;

class Vec
{
public:
	Vec();
	Vec(double i, double j, double k);

	double x;
	double y;
	double z;
};

void Set(Vec* targ, double x, double y, double z);
void Scale(Vec* targ, Vec v, double k);
void Sum(Vec* targ, Vec v, Vec u);
void Diff(Vec* targ, Vec v, Vec u);
void X(Vec* targ, Vec v, Vec u);
void Triangle_X(Vec* targ, Vec v, Vec u, Vec w);
void Normalize(Vec* targ, Vec v);
double Length(Vec v);
double Dot(Vec v, Vec u);
void Rotate(Vec* targ, Vec axis, double theta);
double Dist(Vec v, Vec u);
bool Equal(Vec u, Vec v);
bool PointInTriangle(Vec p, Vec n, Vec p0, Vec p1, Vec p2);
double PointLineDist(Vec p, Vec p0, Vec p1);

#endif
