#include "vec.hh"

Vec::Vec()
{
	x = y = z = 0.0;
}

Vec::Vec(double i, double j, double k)
{
	x = i; y = j; z = k;
}

void Set(Vec* targ, double x, double y, double z)
{
  targ->x = x;
  targ->y = y;
  targ->z = z;
}

void Scale(Vec* targ, Vec v, double k)
{
  targ->x = v.x*k;
  targ->y = v.y*k;
  targ->z = v.z*k;
}

void Sum(Vec* targ, Vec v, Vec u)
{
  targ->x = v.x + u.x;
  targ->y = v.y + u.y;
  targ->z = v.z + u.z;
}

void Diff(Vec* targ, Vec v, Vec u)
{
  targ->x = v.x - u.x;
  targ->y = v.y - u.y;
  targ->z = v.z - u.z;
}

void X(Vec* targ, Vec v, Vec u)
{
  targ->x = v.y*u.z - v.z*u.y;
  targ->y = v.z*u.x - v.x*u.z;
  targ->z = v.x*u.y - v.y*u.x;
}

void Triangle_X(Vec* targ, Vec u, Vec v, Vec w)
{
	Vec x, y;
	Diff(&x, u, v);
	Diff(&y, w, v);
	X(targ, x, y);
}

void Normalize(Vec* targ, Vec v)
{
  double n = Length(v);
  if (n == 0) return;
  targ->x = v.x/n;
  targ->y = v.y/n;
  targ->z = v.z/n;
}

double Length(Vec v)
{
  return sqrt(Dot(v, v));
}

double Dot(Vec v, Vec u)
{
  return v.x*u.x + v.y*u.y + v.z*u.z;
}

void Rotate(Vec* targ, Vec axis, double theta)
{
  Vec a(axis.x, axis.y, axis.z);
  Normalize(&a, a);
  /* equathetaions for rothetaathetaion thetaaken from wikipedia */
  Set(targ,
      a.x*(a.x*targ->x + a.y*targ->y + a.z*targ->z)*(1 - cos(theta)) +
      targ->x*cos(theta) +
      (-a.z*targ->y + a.y*targ->z)*sin(theta),
      
      a.y*(a.x*targ->x + a.y*targ->y + a.z*targ->z)*(1 - cos(theta)) +
      targ->y*cos(theta) +
      (a.z*targ->x - a.x*targ->z)*sin(theta),
      
      a.z*(a.x*targ->x + a.y*targ->y + a.z*targ->z)*(1 - cos(theta)) +
      targ->z*cos(theta) +
      (-a.y*targ->x + a.x*targ->y)*sin(theta));
}

double Dist(Vec v, Vec u)
{
  Vec w;
  Diff(&w, v, u);
  return Length(w);
}

bool Equal(Vec u, Vec v)
{
	return ((u.x == v.x) && (u.y == v.y) && (u.z = v.z));
}

bool PointInTriangle(Vec p, Vec n, Vec p0, Vec p1, Vec p2)
{
	Vec edge, point, cross;
	
	Diff(&edge, p1, p0);
	Diff(&point, p, p0);
	X(&cross, edge, n);
	if (Dot(cross, point) < 0) return false;
	
	Diff(&edge, p2, p1);
	Diff(&point, p, p1);
	X(&cross, edge, n);
	if (Dot(cross, point) < 0) return false;
	
	Diff(&edge, p0, p2);
	Diff(&point, p, p2);
	X(&cross, edge, n);
	if (Dot(cross, point) < 0) return false;

	return true;
}

double PointLineDist(Vec p, Vec p0, Vec p1)
{
	Vec v, w;
	Diff(&v, p1, p0);
	Diff(&w, p, p0);

	double c1 = Dot(w, v);
	if (c1 <= 0) return Dist(p, p0);

	double c2 = Dot(v, v);
	if (c2 <= c1) return Dist(p, p1);

	double b = c1/c2;
	Vec Pb;
	Scale(&Pb, v, b);
	Sum(&Pb, Pb, p0);
	return Dist(p, Pb);
}
