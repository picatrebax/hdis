#include "dialog.hh"

NgonDialog::NgonDialog() :
  wxDialog(NULL, -1, wxEmptyString, wxDefaultPosition, wxDefaultSize)
{
  wxBoxSizer *sizer = new wxBoxSizer(wxHORIZONTAL);

	sizer->Add(new wxStaticText(this, wxID_ANY, _("Number of sides: ")), 0, 0, 0);

	wxIntegerValidator<unsigned int> n_val(NULL, wxNUM_VAL_THOUSANDS_SEPARATOR);
	n_text = new wxTextCtrl(this, wxID_ANY, wxEmptyString,
			wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER, n_val);
	n_text->SetValue("3");
	sizer->Add(n_text, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL, 0);

	wxButton* ok_button = new wxButton(this, wxID_ANY, _("ok"));
	Connect(wxID_ANY, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(NgonDialog::OnOk));
	sizer->Add(ok_button, 0, 0, 0);

  sizer->SetSizeHints(this);
  this->SetSizer(sizer);
}

void NgonDialog::OnOk(wxCommandEvent &event)
{
	EndModal(wxID_OK);
}
