%--------------------------------------------------------------------------------

\section{Fluid-Grain Forces}

The problem of solid-liquid interaction in computational fluid dynamics with
smoothed particle hydrodynamcs (SPH) method and discrete element method (DEM) is
an essential problem that is not satisfactory solved (references). A standard
``engineering'' method of solution is to generate ghost SPH particles on or in
DEM grains that interact with SPH particles in liquid and transform forces to
DEM grains. (more here later\dots).

A straightforward approach to this problem, however, would be finding an
integral of stress vector on the surface of the DEM grain. To find the total
force $\bm{F}$ and total torque $\bm{M}$ on a DEM grain surrounded by the
surface $\Sigma$ one need to compute integrals:
\begin{equation}\label{eq:mainint}
  \bm{F} = \int_\Sigma \bm{p_n}\,d\sigma = \int_\Sigma
  \bm{\sigma}\cdot\bm{n}\,d\sigma, \qquad \bm{M} = \int_\Sigma \bm{r_O}\times
  \bm{p_n}\, d\sigma = \int_\Sigma \bm{r_O}\times ( \bm{\sigma}\cdot\bm{n})\,
  d\sigma,
\end{equation}
where $\bm{p_n}=\bm{\sigma}\cdot\bm{n}$ is stress vector, $\bm{\sigma}$ is
stress tensor, $\bm{r_O}$ is the radius vector from the center of mass of the
grain to the integration point. For ideal fluid if stress tensor is spherical
$\sigma_{ij}=-p\delta_{ij}$ we have
\begin{equation}
  \bm{F} = - \int_\Sigma p \bm{n}\,d\sigma, \qquad \bm{M} = - \int_\Sigma p\,
  \bm{r_O}\times \bm{n}\, d\sigma,
\end{equation}
where $p(x,y,z,t)$ is the pressure field.

%--------------------------------------------------------------------------------
\section{SPH Fundamentals}

SPH is the Lagrangian method that uses ``particles'', 3D points with properties,
moving in space according some laws. These particles carry some properties such
as mass $m_i$, density $\rho_i$, pressure $p_i$, enthropy $s_i$ and such. Where
index $i=1,\dots,N$, and $N$ is the number of these particles.

SPH is based on the following consideration. For any point $\bm{r}\in
\mathbb{R}^3$, and for any continuous function $A(\bm{r})$ we have
\begin{equation}\label{eq:fundamentals-delta}
  A(\bm{r}) = \int A(\bm{r'})\,\delta(\bm{r}-\bm{r'})\, dr' \approx \int
  A(\bm{r'})\,W_h(\bm{r}-\bm{r'})\, dr',
\end{equation}
where
\begin{equation}
  W_h(\bm{r}-\bm{r'})\to \delta(\bm{r}-\bm{r'}),\quad\text{when}\quad h\to0,
\end{equation}
here $h$ is ``smoothing length''.

The natural choices for the kernel are spherically symmetric functions:
\begin{equation}
  W_h(\bm{r}-\bm{r'}) = W_h(\left|\bm{r}-\bm{r'}\right|)
\end{equation}

The natural choice for the kernel would be Gaussian
\begin{equation}
  W_h(x) = \frac{1}{\left(\sqrt{\pi}\,h\right)^3}\,e^{-x^2/h^2}.
\end{equation}
However, Gaussian has an infinite support. The practical choice in SPH is a
spline. The smallest degree possible for approximation is 3. For example:
\begin{equation}
  W_h(x) = M_4(x)/h^3 = \frac{1}{4\pi\,h^3}\, \left\{
  \begin{aligned}
    (2-x)^3-4(1-x)^3, & \qquad \text{if}\quad x<1,\\ (2-x)^3, & \qquad
    \text{if}\quad 1\le x<2,\\ 0, & \qquad \text{if}\quad x\ge 2,\\
  \end{aligned}
  \right.
\end{equation}

As can be seen, the actual ``radius of influence'' of any SPH particle is not
$h$, but proportional to $h$ depending on the kernel. For the kernel based on
$M_4$, it is $2h$. For HDIS we will use only kernels with $2h$ influence
radius. In particular, we use Wendland
kernel~\cite{bib:wendland1995piecewise,bib:dehnen2012improving}:
\begin{equation}
  W_h(x) = \frac{21}{256\pi}(2-x)^4(1+2x), \quad x < 2.
\end{equation}
Both $M_4$ and Wendland kernels are shown in Fig.~\ref{fig:wendland}
\begin{figure}
  %
  % Produced with gnuplot/wendland.gp
  %
  \center
  \includegraphics[width=0.6\textwidth]{gnuplot/wendland.pdf}
  \caption{Kernel functions}
  \label{fig:wendland}
\end{figure}



Based on Eq.~\eqref{eq:fundamentals-delta}, any function at arbitrary point
$\bm{r}$ can be approximated as a sum
\begin{equation}\label{eq:ar}
  A(\bm{r}) \approx \int A(\bm{r'})\,W_h(\bm{r}-\bm{r'})\, dr' \approx
  \sum_{i=1}^{N} A_i W_h\left(\left|\bm{r_i}-\bm{r}\right|\right)\,V_i =
  \sum_{i=1}^{N} A_i
  \frac{m_i}{\rho_i}\,W_h\left(\left|\bm{r_i}-\bm{r}\right|\right).
\end{equation}

%--------------------------------------------------------------------------------
\section{Total Force at a Given Surface}

Let $\Sigma$ be a surface where we need to calculate the total force from our
fluid. Based on Eqs.~\eqref{eq:mainint} and \eqref{eq:ar}, we have
\begin{equation}
  \bm{F} = \int_{\Sigma} \bm{\sigma}\cdot\bm{n}\,d\sigma \approx \int_\Sigma
  \left( \sum_{i=1}^N \frac{m_i}{\rho_i} \, \bm{\sigma_i}\,W_h(\bm{r_i}-\bm{r})
  \right) \cdot\bm{n}\, d\sigma = \sum_{i=1}^N \frac{m_i}{\rho_i} \,
  \bm{\sigma_i}\cdot \int_\Sigma W_h(\bm{r_i}-\bm{r})\, \bm{n}\, d\sigma
\end{equation}
This allows to reduce the problem of the total force computations to a single
integral
\begin{equation}
  \bm{\Theta_i} = \int_\Sigma W_h(\bm{r_i}-\bm{r})\, \bm{n}\, d\sigma.
\end{equation}

%--------------------------------------------------------------------------------
\section{Total Torque of a Rigid Body}

%--------------------------------------------------------------------------------
\section{Solution to Sphere-Point Force Computation}

Let us find $\bm{\Theta_i}$ in the case when the surface $\Sigma$ is a total sphere.
\begin{equation}
  \bm{\Theta_i} = \int_\Sigma W_h(\bm{r_i}-\bm{r})\, \bm{n}\, d\sigma.
\end{equation}
Let $O_c=(x_c,y_c,z_c)^T$ be the center of the rigid sphere with radius $R>0$.

\subsection{Point above the North Pole}
Let $O_c=\bm{0}$, and $\bm{r_i}=(0,0,\tilde z)$. Then, due to symmetry, the
first two components of the integral is 0. Let us calculate the third component
using spherical coordinates:
\begin{equation}
  \left\{
  \begin{array}{l}
    x=R\, \sin\varphi\,\cos\theta \\ y=R\, \sin\varphi\,\sin\theta\\ z=R\,
    \cos\varphi,
  \end{array}
  \right. \quad 0<\varphi< \pi,\quad 0<\theta<2\pi,
\end{equation}
we get
\begin{multline}
  \left( \int_\Sigma W_h\left(\left|\bm{r}-\bm{r_i}\right|\right)\bm{n}\,d\sigma
  \right)_z = \left(\int_\Sigma
  W_h\left(\left|\bm{r}-\bm{r_i}\right|\right)\frac{\bm{r}}{R}\,d\sigma\right)_z
  \\ = R^2 \int_0^\pi\int_0^{2\pi} W_h\left(\sqrt{ \underbrace{R^2+{\tilde
        z}^2-2R{\tilde z}\cos\varphi}_{\text{cosine theorem}}}\right)
  \cos\varphi\,\sin\varphi\,d\theta\,d\varphi \\ = 2\pi R^2\int_0^\pi
  W_h\left(\sqrt{ R^2+{\tilde z}^2-2R{\tilde z}\cos\varphi}\right)
  \cos\varphi\,\sin\varphi\,d\varphi \\ = 2\pi R^2 \int_{-1}^{1}
  W_h\left(\sqrt{R^2+{\tilde z}^2-2R{\tilde z}s}\right)\, s\,ds=
  2\pi R^2 \Lambda(\tilde z).\\
\end{multline}

\subsection{General case}

\begin{figure}
  \center
  \includegraphics[width=0.4\textwidth]{../pics/rotationsphere.pdf}
  \caption{Rotational transformation consists of rotating along axis
    $\bm{e_i}\times\bm{e_z}$ by angle $\alpha\in[0,\pi]$.}
  \label{fig:sphererot}
\end{figure}

As the previous expression has only $z$ component, then in general case we
obviously have $\bm{\Theta_i}$ directed along the line connecting the SPH point
with the center of the sphere. Thus, in general case we have
\begin{equation}
  \bm{\Theta_i} = 2\pi R^2\,\Lambda\left(|\bm{r_i}-\bm{r_O}|\right)\,
  \frac{\bm{r_i}-\bm{r_O}}{|\bm{r_i}-\bm{r_O}|}.
\end{equation}

\subsection{Exact Expression for Main Integral}

The only thing left is to compute the integral
\begin{equation}
  \Lambda = \int_{-1}^{1} W_h\left(\sqrt{B-Cs} \right)\,s\,ds,
\end{equation}
where
\begin{equation}\label{eq:BC}
B=R^2+{\tilde z}^2, \qquad C=2\,{\tilde z}\,R,
\end{equation}
 for different kernels $W_h$. Obviously, $B\ge C$, and $B=C$ if and only if
 $\tilde z = R$.

\subsubsection{Gaussian}
Let
\begin{equation}
  W_h(x) = \frac{1}{\left(\sqrt\pi h\right)^3}\,e^{-x^2/h^2}, \quad x\in{\mathbb R},
\end{equation}
The cube expression in the denominator is present as it is 3D Gaussian to be
used for our 3D problem. Substituting $x=\sqrt{B-Cs}$, we get
\begin{equation}
  W_h(\sqrt{B-Cs}) = \frac{1}{\left(\sqrt\pi h\right)^3}\,e^{(-B+Cs)/h^2}.
\end{equation}
In this case, the integral will have the following form
\begin{equation}
  \Lambda = \frac{1}{\left(\sqrt\pi h\right)^3} \int_{-1}^1
  e^{(-B+Cs)/h^2}\,s\,ds =
  \frac{e^{-B/h^2}}{\left(\sqrt\pi h\right)^3} \int_{-1}^1
  e^{Cs/h^2}\,s\,ds.
\end{equation}
Substituting $t=Cs/h^2$, we have
\begin{equation}
  s=(h^2/C)t, \quad ds = (h^2/C)\,dt, \quad [-1,1]\to \left[-C/h^2, C/h^2\right],
\end{equation}
Hence
\begin{multline}
  \Lambda = \frac{e^{-B/h^2}}{\left(\sqrt\pi h\right)^3}\frac{h^4}{C^2}
  \int_{-C/h^2}^{C/h^2} e^t\,t\,dt =
  \left.
  \frac{e^{-B/h^2}}{\left(\pi^{3/2} h C\right)}\frac{h^2}{C}
  e^t(t-1) \right|_{t=-C/h^2}^{t=C/h^2}
  \\=
  \frac{e^{-B/h^2}}{\left(\pi^{3/2} h C\right)}\frac{h^2}{C}
  \left[ e^{C/h^2}\left(C/h^2-1\right) -
    e^{-C/h^2}\left(-C/h^2-1\right)\right]\\=
  \frac{1}{\left(\pi^{3/2} h C\right)}
  \left[ \left(1-h^2/C\right) e^{(C-B)/h^2} +
    \left(1+h^2/C\right)e^{-(C+B)/h^2}\right].\\
\end{multline}
Taking into account Eq.~\eqref{eq:BC}, we have
\begin{equation}
  \Lambda = 
  \frac{1}{\left(2\pi^{3/2} \tilde z Rh\right)}
  \left[ \left(1-\frac{h^2}{2\tilde z R}\right)\,e^{-(R-\tilde z)^2/h^2} +
    \left(1+\frac{h^2}{2\tilde z R}\right)\,e^{-(R+\tilde z)^2/h^2}\right].\\
\end{equation}

\subsection{Current Choice for Forces}

The main problem with the Gaussian is that it has the infinite support. Thus, in
actual computations, the force will kicks in with a jump when an SPH particle
approaching the DEM grain. On the other hand, the integrals for other kernels
are more complicated, thus we want to fix the Eq.~\eqref{eq:BC} rather than use
exact formulas for the polynomial kernels.  We anyway made many assumptions in
our derivation (the solid has a spherical surface and fully exposed to the
fluid), the expression is going to be an approximation. We have decided to
multiply our final expression with the suppression cap function $C(x)$ as
follows:
\begin{equation}
  C(x) = \left\{
  \begin{array}{lr}
    1, &  0\le x \le 1-\delta,\\
    d + x(c + x(b+ax)),& \delta\le x \le 1,\\
    0, & 1 < x.\\
  \end{array}
  \right.
\end{equation}

\begin{equation}
\end{equation}
where
\begin{equation}
  a = 2/\delta^3, \quad b =
  a(1.5\delta-3), \quad c = 3a-2b, \quad d = 1 - a\delta^3 - b\delta^2 -
  c\delta,
  \qquad 0 < \delta < 1.
\end{equation}
$\delta$ defines the portion of the unit interval with capping effect. The
function is shown in Fig.~\ref{fig:capfunction}

\begin{figure}
  %
  %  figure is generated by gnuplot script gnuplot/cap.gp
  %
  \center
  \includegraphics[width=0.6\textwidth]{gnuplot/cap-function.pdf}
  \caption{Cap function applied to $\Lambda$ value to smooth the edge of it when
    $\delta=0.25$.}
  \label{fig:capfunction}
\end{figure}

As a result, instead of $\Lambda$ we use corrected value $\Lambda^{*}$:
\begin{equation}
  \Lambda^{*}(R,h,\tilde z) =   C\left(\frac{\tilde z - R }{2h}\right)
  \,\Lambda(R,h,\tilde z)
\end{equation}

