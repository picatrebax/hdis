#
# plotting Cap function
#

set output "wendland.eps"

# Fonts
dir_amsfonts = "/usr/share/texlive/texmf-dist/fonts/type1/public/amsfonts/cm"
dir_cmsuper  = "/usr/share/texmf/fonts/type1/public/cm-super"

set terminal postscript fontfile dir_amsfonts."/cmmi10.pfb"
set terminal postscript fontfile dir_cmsuper."/sfrm1000.pfb"
set terminal postscript fontfile dir_cmsuper."/sfbx1000.pfb"
set terminal postscript fontfile dir_cmsuper."/sfti1000.pfb"

### Terminal/output options
set term postscript eps enhanced color 'SFRM1000' 28

set ylabel "" offset 2
set xlabel  "{/SFTI1000 x}"
unset grid

set key nobox spacing 1 right top notitle
set notitle

set xtics #0.002
set ytics

set style line 1 lt 1 lw 2 pt 3 linecolor rgb "red"
set style line 2 lt 1 lw 2 pt 3 linecolor rgb "blue"
set style line 3 lt 1 lw 2 pt 3 linecolor rgb "green"
set style line 4 lt 1 lw 2 pt 3 linecolor rgb "magenta"
set style line 5 lt 1 lw 2 pt 3 linecolor rgb "orange"

W(x) = x < 2 ? 21.0/256.0/pi * ((2-x)**4)*(1+2*x) : 0
M4(x) = x > 2 ? 0 : ( x < 1 ? (2-x)**3-4*(1-x)**3 : (2-x)**3 ) / (4*pi)

plot [0:2] \
     W(x)  with lines lt 1  title 'Wendland', \
     M4(x) with lines lt 2  title 'M4'
