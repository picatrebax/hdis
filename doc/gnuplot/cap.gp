#
# plotting Cap function
#

set output "cap-function.eps"

# Fonts
dir_amsfonts = "/usr/share/texlive/texmf-dist/fonts/type1/public/amsfonts/cm"
dir_cmsuper  = "/usr/share/texmf/fonts/type1/public/cm-super"

set terminal postscript fontfile dir_amsfonts."/cmmi10.pfb"
set terminal postscript fontfile dir_cmsuper."/sfrm1000.pfb"
set terminal postscript fontfile dir_cmsuper."/sfbx1000.pfb"
set terminal postscript fontfile dir_cmsuper."/sfti1000.pfb"

### Terminal/output options
set term postscript eps enhanced color 'SFRM1000' 28

# set ylabel "{/SFTI1000 F}_{/SFTI1000 c}, N" offset 2
# set xlabel "{/SFTI1000 t}, s"
set ylabel "" offset 2
set xlabel  "{/SFTI1000 x}"
unset grid

set key nobox spacing 1 right top notitle
set notitle

set xtics #0.002
set ytics

set style line 1 lt 1 lw 2 pt 3 linecolor rgb "red"
set style line 2 lt 1 lw 2 pt 3 linecolor rgb "blue"
set style line 3 lt 1 lw 2 pt 3 linecolor rgb "green"
set style line 4 lt 1 lw 2 pt 3 linecolor rgb "magenta"
set style line 5 lt 1 lw 2 pt 3 linecolor rgb "orange"


delta = 0.25
a = 2 / ( delta**3 )
b = a * ( 1.5*delta - 3 )
c = - 3 * a - 2 * b
d = 1 - a * (1-delta)**3 - b * (1-delta)**2 - c * (1-delta)

f(x) = x < 1 - delta ? 1 : d + x * ( c + x * ( b + a * x ) )


plot [0:1][0:1.25] f(x) with lines lt 1  title 'Cap function, delta = 0.25'
