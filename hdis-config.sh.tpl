#!/bin/bash
#
#  Shell script to source for avoiding environment file usage
#
#
#  ${NAME} ${VERSION}
#  branch: ${BRANCH}
#  revision: ${REVISION}
#
#  ${COPYRIGHT}
#
#

# hdis
export HDIS_PATH=${HDIS_PATH}

LUA_FULL_VER=`lua -v`
LUAVER=`echo $LUA_FULL_VER | grep -oP "5\.[0-9]"`
echo "Lua version detected: " $LUAVER

export LUA_PATH="\
${HDIS_PATH}/lua/?.lua;\
${HDIS_PATH}/lua/?/init.lua;\
/usr/local/share/lua/${LUAVER}/?.lua;\
/usr/local/share/lua/${LUAVER}/?/init.lua;\
/usr/local/lib/lua/${LUAVER}/?.lua;\
/usr/local/lib/lua/${LUAVER}/?/init.lua;\
/usr/share/lua/${LUAVER}/?.lua;\
/usr/share/lua/${LUAVER}/?/init.lua;\
./?.lua"

export LUA_CPATH="\
./?.so;\
${HDIS_PATH}/lua/lib/?.so;\
/usr/local/lib/lua/${LUAVER}/?.so;\
/usr/local/lib/lua/${LUAVER}/?/loadall.so;\
/usr/lib/x86_64-linux-gnu/lua/${LUAVER}/?.so;\
/usr/lib/x86_64-linux-gnu/lua/${LUAVER}/?/loadall.so;\
/usr/lib/lua/${LUAVER}/?.so;\
/usr/lib/lua/${LUAVER}/?/loadall.so;"

export HDIS="${HDIS_PATH}/src/hdis"
export HDISVIS="${HDIS_PATH}/hdisvis/src/hdisvis"

export USER_MAKE_DIR="${HDIS_PATH}"

echo '* Configuration completed'
echo "  Build directory ${USER_MAKE_DIR}"
echo '  Use $HDIS to run the model'
echo '  Use $HDISVIS to run the visualization'

# Local Variables:
# mode: sh
# comment-start: "#"
# End:
