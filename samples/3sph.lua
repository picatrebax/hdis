--[[--

  3sph.lua

  Example: 3-spherical particle example.

   (c)2016 Coupi, Inc.

  Usage: hdis 3sph.lua

--]]

require 'hdis'

local shape = require 'hdis.shape'
local vec3d = require 'hdis.vec3d'
local mathx = require 'hdis.mathx'

---------------------------------------------------------------------------------
-- Main definitions and gravity
---------------------------------------------------------------------------------

-- Time/saving parameters
hdis.time = {
   start = 0,
   stop = 2,
   dt = 0.00001,
   save = 0.01
}

-- The area of simulation
hdis.box = {
   x = 1,
   y = 1,
   z = 1
}

-- m/s^2, gravity
hdis.g = -9.8

---------------------------------------------------------------------------------
-- Groups and Materials
---------------------------------------------------------------------------------

-- alternative approach with material properties demo
local rock = {
   name = 'rock',
   G = 30,                   -- GPa, shear modulus
   nu = 0.3,                 -- 1, Poisson coef
   mu = 0.7,                 -- 1, friction (static/dyn)
   CR = 0.5,                 -- 1, coef. of restitution
   rho = 3000.0,             -- kg/m^3, density
}

local steel = {
   name = 'steel',
   G = 80,                   -- GPa, shear modulus
   nu = 0.31,                 -- 1, Poisson coef
   mu = 0.5,                 -- 1, friction (static/dyn)
   CR = 0.8,                 -- 1, coef. of restitution
   rho = 7500.0,             -- kg/m^3, density
}

-- we can chain material properties adding but this is not necessary
hdis.material:add( rock ):add( steel )

-- we will use default contact properties that were assigned by the
-- system for this example

-- The only group in the system: 3-sphere
hdis.groups:add( '3-sphere' )

---------------------------------------------------------------------------------
-- Object addition
---------------------------------------------------------------------------------

local R = 0.05

local tsph = shape.threespheres( R, '3-sphere', 'rock' )
tsph.body.c = vec3d:new ( 0.5*hdis.box.x,
                          0.5*hdis.box.y,
                          0.5*hdis.box.z ) -- center

tsph.body.q = mathx.q_random() -- random orientation
hdis.append( tsph )

-- Boundary is made out of steel
hdis.boundary.material = 'steel'


---------------------------------------------------------------------------------
-- Display options
---------------------------------------------------------------------------------

hdis.callback.presave   = hdis.callback.std.presave( 'tsp-' )
hdis.callback.aftersave = hdis.callback.std.aftersave_short()
