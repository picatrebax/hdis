--[[--

  Example: Material addition, contact properties creation and editing
  simple test. It also demonstrates usage of hdis.aux.print_serial
  function for printing the whole tables.

  Author: Anton Kulchitsky

  Usage: lua matest.lua

--]]

require 'hdis'
local aux = require 'hdis.aux' -- aux is a shortcut to hdis.aux now

hdis.material:add{ name = "rock", 
                    rho = 2750.0, G=30, nu=0.3, mu=0.7, CR=0.05 }
hdis.material:add{ name = "steel", 
                    rho = 7850.0, G=80, nu=0.27, mu=0.3, CR=0.9 }

-----------------------------------------------------------------------
-- Now we are going to edit one of the contact properties: friction mu
-----------------------------------------------------------------------

local rock_steel = hdis.material:select_cmprop( "rock", "steel" )

print( '--- '..'Original contact properties between rock and steel'
       ..' ---' )
aux.print_serial( rock_steel )

print( '--- '..'Editing mu between rock and steel'..' ---' )
rock_steel.mu = 0.54
aux.print_serial( rock_steel )




