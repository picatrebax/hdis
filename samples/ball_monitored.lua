--[[--

  Example: Bouncing ball with writing ball's position into a file from
  Lua script using monitor facility.

  Author: Anton Kulchitsky

  Usage: hdis ball_monitored.lua

--]]

require "hdis"               -- this module is always required!
require "hdis.shape"         -- this module contains primitive
                              -- shapes, we require it for
                              -- hdis.shape.sphere function
local aux = require "hdis.aux"

-- Time/saving parameters
hdis.time = {
   start = 0,                   -- starting time
   stop  = 1.85,                -- ending time
   dt    = 0.00001,             -- timestep
   save  = 0.01                 -- saving timestep
}

-- The area of simulation
hdis.box = {
   x = 1,
   y = 1,
   z = 1
}

hdis.g = -9.8     -- m/s^2, gravity

-- Materials (rho - density in kg/m^3, G - stiffness in GPa, nu -
-- Poisson coefficent, mu - Coloumb friction (static/dyn), CR -
-- restitution coefficient.

local steel = { name = "steel",
                rho = 7850.0,
                G=80,
                nu=0.27,
                mu=0.3,
                CR=0.8 }

hdis.material:add( steel )

-- Groups. We have to add all group names we are going to use
hdis.groups:add( "ball" )

-- We create a ball of radius 0.05m, that belongs to group "ball" and
-- made out of our material "steel". This function just forms a
-- "shape": a body, atoms it is built from, and points that are atoms
-- "skeleton"

local ball = hdis.shape.sphere( 0.05, "ball", "steel" )
ball.body.c = { 0.5*hdis.box.x,
                0.5*hdis.box.y,
                0.5*hdis.box.z } -- center of mass location: we have
                                  -- to place our ball in the box.

ball.body.monitor = 1             -- we monitor the ball's position

-- Appending the ball shape to the list of COUPi objects! From now,
-- our ball is in the system and will be recognized by COUPi.

hdis.append( ball )

-- aux.print_serial(ball)

-- Boundary is made of the same material... for now. It can be done by
-- just simple assignement as follows. There are other ways to do this
-- too, but this one seems to be the simplest approach.

hdis.boundary.material = "steel"


-- special global table to contain variables to change in functions
myhdis = {}
myhdis.t = 0
myhdis.dt = 0
myhdis.n = 0

-- Function to set time, timestep and step number to use in other
-- functions
function pretimestep( t, dt, n )
   myhdis.t = t
   myhdis.dt = dt
   myhdis.n = n
end

hdis.callback.pretimestep = pretimestep

local fp = assert( io.open( 'ballpos.txt', 'w' ) )

-- Monitoring all the values related to i-th body.
local function monitor( i, m, V,
                        Ix, Iy, Iz,
                        cx, cy, cz,
                        vx, vy, vz,
                        wx, wy, wz,
                        fx, fy, fz,
                        tx, ty, tz )
   if myhdis.n % 100 == 0 then
      local str = string.format( "%d %f %f %f %f\n",
                                 myhdis.n, myhdis.t, cx, cy, cz )
      fp:write(str)
   end
end
g
-- list of monitor functions
hdis.monitor_functions = {
   monitor,
}

-- Finalizing function

local function finalize( t )
   fp:close()
   print ( "FILE CLOSED; t = ", t )
end

hdis.callback.finalize = finalize

-------------------------------------------------------------------
--                      Display options
-------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave( "monitored-" )
hdis.callback.aftersave = hdis.callback.std.aftersave_short()
