--[[--

   Example: Bouncing ball with alternating gravity! hdis.callback.g function is
   defined to alternate the gravity depending on time. Initially, gravity points
   down, then it changes periodically with period = 2s.

   All the rest is identical to ball.lua example with only exception: longer
   period of model time (5s) and output file names.

   (c)2016 Coupi, Inc.

   Usage: hdis ball_galt.lua

--]]


require 'hdis'               -- this module is always required!
local shape = require 'hdis.shape'         -- this module contains primitive
                                            -- shapes, we require it for
                                            -- whdis.shape.sphere function

local vec3d = require 'hdis.vec3d' -- vec3d is main 3D vector type

-- Time/saving parameters
hdis.time = {
   start = 0,                   -- starting time
   stop  = 5,                -- ending time
   dt    = 0.00001,             -- timestep
   save  = 0.01                 -- saving timestep
}

-- The area of simulation
hdis.box = {
   x = 1,
   y = 1,
   z = 1
}

hdis.g = -9.8     -- m/s^2, gravity

-- Materials (rho - density in kg/m^3, G - stiffness in GPa, nu -
-- Poisson coefficent, mu - Coloumb friction (static/dyn), CR -
-- restitution coefficient.

hdis.material:add{ name = 'steel',
                    rho = 7850.0, G=80, nu=0.27, mu=0.3, CR=0.8 }

-- Table 'hdis.material.cmprops' can be later edited to manually
-- correct the values in it.

-- Groups. We have to add all the group name we use.
hdis.groups:add( 'ball' )

-- We create a ball of radius 0.05m, that belongs to group 'ball' and
-- made out of our material 'steel'. This function just forms a
-- 'shape': a body, atoms it is built from, and points that are atoms
-- 'skeleton'

local ball = hdis.shape.sphere( 0.05, 'ball', 'steel' )
ball.body.c = { 0.5*hdis.box.x,
                0.5*hdis.box.y,
                0.5*hdis.box.z } -- center of mass location: we have
                                  -- to place our ball in the box.

-- Appending the ball shape to the list of COUPi objects! From now,
-- our ball is in the system and will be recognized by COUPi.

hdis.append( ball )

-- Boundary is made of the same material... for now. It can be done by
-- just simple assignement as follows. There are other ways to do this
-- too, but this one seems to be the simplest approach.

hdis.boundary.material = 'steel'

-------------------------------------------------------------------
--                Alternating gravity function
-------------------------------------------------------------------
function hdis.callback.g( gx, gy, gz, t, dt )

   local T = 2                  -- 2 s period
   local theta = 0.5* ( 1 - math.sin( 2 * math.pi * t / T ) )

   -- changing between 9.8 and -9.8 kg/m^3
   local g = - 9.8 * ( 1 - theta ) + 9.8 * theta

  return 0, 0, g
end

-------------------------------------------------------------------
--                      Display options
-------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave( 'alt-g-' )
hdis.callback.aftersave = hdis.callback.std.aftersave_short()

