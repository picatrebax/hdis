--[[--

  Example: COUPi example of basic springs built.

  Author: Anton Kulchitsky

  Usage: hdis spring.lua

--]]

require "hdis"
require "hdis.shape"
require "hdis.mathx"
require "hdis.control"
require "hdis.aux"

-- Time/saving parameters
hdis.time = {
   start = 0,
   stop = 5,
   dt = 0.00001,
   save = 0.01
}

-- The area of simulation
hdis.box = {
   x = 1,
   y = 1,
   z = 1
}

hdis.g = -9.8     -- m/s^2, gravity

hdis.groups:add( "ball", "3sphere" )

hdis.material:add{ name = "steel",
                    rho = 7850.0, G=80, nu=0.27, mu=0.3, CR=0.8 }

local R = 0.05
local ball

ball = hdis.shape.sphere( R, "ball", "steel" )
ball.body.c = { 0.5*hdis.box.x,
                0.5*hdis.box.y,
                0.75*hdis.box.z } -- center
ball.body.control = hdis.control.builtin.steady
hdis.append( ball )

ball = hdis.shape.sphere( R, "ball", "steel" )
ball.body.c = { 0.5*hdis.box.x,
                0.25*hdis.box.y,
                0.5*hdis.box.z } -- center
hdis.append( ball )

ball = hdis.shape.sphere( R, "ball", "steel" )
ball.body.c = { 0.5*hdis.box.x,
                0.75*hdis.box.y,
                0.5*hdis.box.z } -- center
hdis.append( ball )

ball = hdis.shape.threespheres( R, "3sphere", "steel" )
ball.body.c = { 0.75*hdis.box.x,
                0.75*hdis.box.y,
                0.5*hdis.box.z } -- center
ball.body.q = hdis.mathx.q_random()
hdis.append( ball )

--------------------------------------------------

hdis.boundary.material = "steel"

print( "ATOMS:" )
hdis.aux.print_serial( hdis.atoms )

-------------------------------------------------------------------
-- Springs: adding springs on the lowest level possible (no lib)
-------------------------------------------------------------------

hdis.springs = {
   {
      rb1 = { 0, -R, 0 },
      rb2 = { 0, R/math.sqrt(2), R/math.sqrt(2) },
      body1 = 1,
      body2 = 2,
      x0 = 0.15,
      k = 100,
      eta = 10
   },
   {
      rb1 = { 0, R, 0 },
      rb2 = { 0, -R, 0 },
      body1 = 2,
      body2 = 3,
      x0 = 0.15,
      k = 100,
      eta = 10
   },
   {
      rb1 = { 0, R, 0 },
      rb2 = { 0, -R/math.sqrt(2), R/math.sqrt(2) },
      body1 = 1,
      body2 = 3,
      x0 = 0.15,
      k = 100,
      eta = 10
   },
   {
      rb1 = { 0, R, 0 },
      rb2 = { 0, R, 0 },
      body1 = 2,
      body2 = 4,
      x0 = 0.15,
      k = 100,
      eta = 10
   },
}

-------------------------------------------------------------------
--                      Display options
-------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave( "spring-" )
hdis.callback.aftersave = hdis.callback.std.aftersave_short()
