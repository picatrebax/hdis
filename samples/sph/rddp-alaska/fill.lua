--[[---------------------------------------------------------------------------
   rddp-alaska/fill.lua

   Example:

   filling hydro particles in river banks made from spheres with Alaska RDDP
   added from the beginning to the system. Periodic boundary conditions.

   (c)2018 UAF

--]]---------------------------------------------------------------------------

require 'hdis' -- this Coupi module is always required!

local shape = require 'hdis.shape' -- this module contains primitive shapes
local hydro = require 'hdis.hydro' -- hydro particles library
local vec3d = require 'hdis.vec3d' -- 3D vector type for vectors and points
local control = require 'hdis.control' -- control for dem objects
local grid    = require 'hdis.grid' -- grid generator
local sphtool = require 'hdis.sphtool'

local aux   = require 'hdis.aux'

package.path = '../?.lua;'..package.path
local materialdb = require 'materialdb'
local params = require 'parameters'

local samplename = 'fill'

-------------------------------------------------------------------------------
-- Main definitions and gravity
-------------------------------------------------------------------------------
-- Time/saving parameters
hdis.time = { start = 0, stop = 3, dt = 0.0001, save = 0.05 }

-- The area of simulation
hdis.box = params.box

-- m/s^2, gravity
hdis.g = { 0, 0.5, -9.8 }

hdis.contact_detection = {}
hdis.contact_detection.method = 'spsp'

-------------------------------------------------------------------------------
-- Groups and Materials
-------------------------------------------------------------------------------

hdis.groups:add( 'water' ):add( 'floor' ):add( 'banky' ):add( 'ceiling' )

hdis.groups:avert( 'banky', 'floor' )
hdis.groups:avert( 'banky', 'ceiling' )

hdis.material:add( materialdb.polypropylene )

-------------------------------------------------------------------------------
-- Boundary is made of the same material... for now. It can be done by just
-- simple assignment as follows. There are other ways to do this too, but this
-- one seems to be the simplest approach.
-------------------------------------------------------------------------------
hdis.boundary.material = 'polypropylene'
hdis.boundary.periodic = true
hdis.boundary.opentop  = true

---------------------------------------------------------------------------------
-- water properties
---------------------------------------------------------------------------------

local water = params.water

-------------------------------------------------------------------------------
-- boundary properties
-------------------------------------------------------------------------------

local floordata = params.floor( hdis.box )

-------------------------------------------------------------------------------
-- Free Hydro particles addition
-------------------------------------------------------------------------------

watercol = params.watergen( hdis.box, floordata, water,
                            params.water.level, 'water' )
for i, S in ipairs( watercol ) do
   hdis.append(  S  )
end

---------------------------------------------------------------------------------
-- river floor and ceiling
---------------------------------------------------------------------------------

local sphere_counter = 0

local floor

floor = sphtool.rectangle(
   floordata.a,
   floordata.b,
   floordata.R,
   floordata.dr,
   'floor', 'polypropylene' )
floor.body.c = vec3d:new( hdis.box.x / 2,
                          hdis.box.y / 2,
                          floordata.R )
floor.body.control = control.builtin.still

sphere_counter = sphere_counter + #floor.atoms
hdis.append( floor )

local ceiling = sphtool.rectangle(
   floordata.a,
   floordata.b,
   floordata.R,
   floordata.dr,
   'ceiling', 'polypropylene' )
ceiling.body.c = vec3d:new( hdis.box.x / 2,
                            hdis.box.y / 2,
                            floordata.c )
ceiling.body.control = control.builtin.still

sphere_counter = sphere_counter + #floor.atoms
hdis.append( ceiling )


---------------------------------------------------------------------------------

bank = sphtool.rectangle(
   floordata.c,
   floordata.b,
   floordata.R,
   floordata.dr,
   'banky', 'polypropylene' )
bank.body.c = vec3d:new( floordata.R,
                         floordata.center[2],
                         floordata.center[3] )
bank.body.q = quat:new( math.pi/2, { 0, 1, 0 } )
bank.body.control = control.builtin.still

sphere_counter = sphere_counter + #bank.atoms
hdis.append( bank )

---------------------------------------------------------------------------------

bank = sphtool.rectangle(
   floordata.c,
   floordata.b,
   floordata.R,
   floordata.dr,
   'banky', 'polypropylene' )
bank.body.c = vec3d:new( hdis.box.x - floordata.R,
                         floordata.center[2],
                         floordata.center[3] )
bank.body.q = quat:new( math.pi/2, { 0, 1, 0 } )
bank.body.control = control.builtin.still

sphere_counter = sphere_counter + #bank.atoms
hdis.append( bank )

---------------------------------------------------------------------------------

print( "SPH particles in water mass:", #hdis.dots )
print( "Boundary grains in river floor and banks", sphere_counter )

-------------------------------------------------------------------------------
-- Display options
-------------------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave( samplename .. '-' )
hdis.callback.aftersave = hdis.callback.std.aftersave_long()
hdis.callback.finalize = hdis.callback.std.finalize( samplename .. '-final' )
