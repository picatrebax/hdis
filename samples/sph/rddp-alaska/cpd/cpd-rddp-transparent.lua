debug_log = 0
model_width = 1800
model_height = 675
model_distance = 33.372000
zoom_speed = 1.000000
render_method = 1
render_switch = 20
trans_speed = 0.050000
rotate_speed = 0.010000
sphere_resize = 1.100000
redraw_distance = 0.001000
display_mode = 0
arrow_mode = 1
clip = 0
clip_trans_speed = 0.050000
coupid_logo = 1
bounding_box = 1
axis_display = 0
selection = 0
ptriangles = 1
time_display = 1
springs = 1
spring_width = 1
force_max = 1.000000
force_min = 0.000000
speed_width = 1
speed_max = 4.000000
speed_base_size = 0.003000
speed_vec_size = 0.010000
atom_colors = {
{ r = 0.4098, g = 0.7569, b = 1.0000, r_var = 0.0000, g_var = 0.0000, b_var = 0.0000, a = 0.3500 },
{ r = 0.5020, g = 0.5020, b = 0.5020, r_var = 0.0000, g_var = 0.0000, b_var = 0.0000, a = 1.0000 },
{ r = 0.7384, g = 0.8786, b = 0.1576, r_var = 0.0000, g_var = 0.0000, b_var = 0.0000, a = 0.0000 },
{ r = 0.0310, g = 0.1937, b = 0.1377, r_var = 0.0000, g_var = 0.0000, b_var = 0.0000, a = 0.0000 },
{ r = 0.7451, g = 0.7451, b = 0.7451, r_var = 0.0000, g_var = 0.0000, b_var = 0.0000, a = 1.0000 },
{ r = 1.0000, g = 0.9647, b = 0.3804, r_var = 0.0000, g_var = 0.0000, b_var = 0.0000, a = 1.0000 },
{ r = 0.6824, g = 0.6196, b = 0.5216, r_var = 0.0000, g_var = 0.0000, b_var = 0.0000, a = 1.0000 },
{ r = 0.8392, g = 0.8549, b = 0.6941, r_var = 0.0000, g_var = 0.0000, b_var = 0.0000, a = 1.0000 },
{ r = 0.5529, g = 0.4157, b = 0.4157, r_var = 0.0000, g_var = 0.0000, b_var = 0.0000, a = 1.0000 }
	}
spring_colors = {
	}
ptriangle_colors = {
	}
