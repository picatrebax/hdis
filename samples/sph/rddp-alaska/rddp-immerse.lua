--[[---------------------------------------------------------------------------
   rddp-alaska/rddp-immerse.lua

   Example:

   Adding an RDDP into the system. Moving it into the water until it reaches the
   right level.

   (c)2018 UAF

--]]---------------------------------------------------------------------------

require 'hdis' -- this Coupi module is always required!

local shape = require 'hdis.shape' -- this module contains primitive shapes
local hydro = require 'hdis.hydro' -- hydro particles library
local vec3d = require 'hdis.vec3d' -- 3D vector type for vectors and points
local control = require 'hdis.control' -- control for dem objects
local grid    = require 'hdis.grid' -- grid generator
local sphtool = require 'hdis.sphtool'

local aux   = require 'hdis.aux'

package.path = '../?.lua;'..package.path
local materialdb = require 'materialdb'
local params = require 'parameters'

local samplename = 'rddp-immerse'

---------------------------------------------------------------------------------

hdis.runfrom( 'fill-final.h5' )

-------------------------------------------------------------------------------
-- Main definitions and gravity
-------------------------------------------------------------------------------
-- Time/saving parameters
hdis.time = { start = 0, stop = 2, dt = 0.0001, save = 0.01 }

-------------------------------------------------------------------------------
-- Groups and Materials
-------------------------------------------------------------------------------

hdis.groups:add( 'cylinder' ):add( 'cylinder-stripe' ):add( 'ponton' ):add( 'disk' )

hdis.groups:avert( 'cylinder', 'ponton' )
hdis.groups:avert( 'cylinder-stripe', 'ponton' )
hdis.groups:avert( 'cylinder-stripe', 'cylinder' )
hdis.groups:avert( 'cylinder-stripe', 'disk' )
hdis.groups:avert( 'cylinder', 'disk' )

hdis.material:add( materialdb.cylinder )
hdis.material:add( materialdb.ponton )
hdis.material:add( materialdb.wetwood )

-- Friction between the lander and particles are less
local friction_cylinder_debris =
   hdis.material:select_cmprop( 'cylinder', 'wetwood' )
friction_cylinder_debris = params.mu_cylinder_debris

local friction_hull_debris =
   hdis.material:select_cmprop( 'ponton', 'wetwood' )
friction_hull_debris = params.mu_cylinder_debris

---------------------------------------------------------------------------------
-- Generate RDDP here
---------------------------------------------------------------------------------

local count = 0

---------------------------------------------------------------------------------
-- cylinder
---------------------------------------------------------------------------------

local cyl = sphtool.largecylinder( params.cylinder.H,
                                   params.cylinder.R,
                                   params.cylinder.r,
                                   params.cylinder.dr,
                                   'cylinder', 'cylinder' )

cyl.body.c = params.cylinder.c
cyl.body.q = {1,0,0,0}
cyl.body.m = params.cylinder.m
cyl.body.w = { 0, 0, 0 } -- initial rotation

cyl.body.control = 1
cyl.body.monitor = 1
cyl.body.sphneutral = true         -- we don't transmit sph forces to cylinder

print( 'Number of atoms in cylinder: ', #cyl.atoms )

local ig_stripe = hdis.groups:resolve( 'cylinder-stripe' )

-- Adding stripes to the cylinder by changing a group for some atoms
for i,A in pairs(cyl.atoms) do
   if ( A.sbb_c[1]*A.sbb_c[2] < 0 ) then
      A.group = ig_stripe
   end
end

hdis.append( cyl )

count = count + #cyl.atoms

-------------------------------------------------------------------------------
-- disks
-------------------------------------------------------------------------------

local disk

disk = sphtool.sph_disk( params.cylinder.R - params.cylinder.r,
                         params.cylinder.r,
                         params.cylinder.dr,
                         'disk', 'cylinder' )

disk.body.c = { params.cylinder.c[1],
                params.cylinder.c[2],
                params.cylinder.c[3] + params.cylinder.H/2 }
disk.body.q = {1,0,0,0}
disk.body.m = 0.1 * params.cylinder.m
disk.body.w = { 0, 0, 0 } -- initial rotation

disk.body.control = 3
disk.body.monitor = nil
disk.body.sphneutral = true         -- we don't transmit sph forces to cylinder

print( 'Number of atoms in disk 1: ', #disk.atoms )

hdis.append( disk )

count = count + #disk.atoms

-------------------------------------------------------------------------------

disk = sphtool.sph_disk( params.cylinder.R - params.cylinder.r,
                         params.cylinder.r,
                         params.cylinder.dr,
                         'disk', 'cylinder' )

disk.body.c = { params.cylinder.c[1],
                params.cylinder.c[2],
                params.cylinder.c[3] - params.cylinder.H/2 }
disk.body.q = {1,0,0,0}
disk.body.m = 0.1 * params.cylinder.m
disk.body.w = { 0, 0, 0 } -- initial rotation

disk.body.control = 3
disk.body.monitor = nil
disk.body.sphneutral = true         -- we don't transmit sph forces to cylinder

print( 'Number of atoms in disk 2: ', #disk.atoms )

hdis.append( disk )

count = count + #disk.atoms

-------------------------------------------------------------------------------
--                           pontons
-------------------------------------------------------------------------------

local ponton                      -- for both pontons

ponton = sphtool.rectangle( params.ponton.L,
                            params.ponton.H,
                            params.ponton.r,
                            params.ponton.dr,
                            'ponton', 'ponton' )

ponton.body.c = params.ponton.c1
ponton.body.q = params.ponton.q1.q
ponton.body.m = params.ponton.m

ponton.body.control = 2
ponton.body.monitor = nil
ponton.body.sphneutral = true

print( 'Number of atoms in a ponton (each ponton):', #ponton.atoms )

hdis.append( ponton )

-------------------------------------------

ponton = sphtool.rectangle( params.ponton.L,
                            params.ponton.H,
                            params.ponton.r,
                            params.ponton.dr,
                            'ponton', 'ponton' )

ponton.body.c = params.ponton.c2
ponton.body.q = params.ponton.q2.q
ponton.body.m = params.ponton.m

ponton.body.control = 2
ponton.body.monitor = nil
ponton.body.sphneutral = true

print( 'Number of atoms in a ponton (each ponton):', #ponton.atoms )

hdis.append( ponton )

---------------------------------------------------------------------------------
-- control function
---------------------------------------------------------------------------------

hdis.control_functions = {
   params.cylinder.immersing,   -- cylinder
   params.cylinder.immersing,   -- pontons
   params.cylinder.immersing,   -- disks
}

---------------------------------------------------------------------------------
-- monitor
---------------------------------------------------------------------------------

local cyl_data = {}             -- to store cylinder parameters

local function monitor( i, m, V,
                        Ix, Iy, Iz,
                        cx, cy, cz,
                        vx, vy, vz,
                        wx, wy, wz,
                        fx, fy, fz,
                        tx, ty, tz )

   -- just update tree_data to use in aftertimestep
   cyl_data.c = { cx, cy, cz }
   cyl_data.w = { wx, wy, wz }
   cyl_data.f = { fx, fy, fz }
   cyl_data.t = { tx, ty, tz }
end

hdis.monitor_functions = {
   monitor,
}

---------------------------------------------------------------------------------
-- INFO
---------------------------------------------------------------------------------

print( 'Number of spheres in RDDP: ', count )

-------------------------------------------------------------------------------
-- Display options
-------------------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave( samplename .. '-' )
hdis.callback.aftersave = hdis.callback.std.aftersave_long()
hdis.callback.finalize = hdis.callback.std.finalize( samplename .. '-final' )
