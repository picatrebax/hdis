--[[---------------------------------------------------------------------------
   parameters.lua

   RDDP-ALASKA different parameters

   (c)2018 UAF

--]]---------------------------------------------------------------------------

package.path = 'librddp/?.lua;'..package.path

local libdebris = require 'libdebris'

local parameters = {}

---------------------------------------------------------------------------------
-- box
---------------------------------------------------------------------------------

parameters.box = {
   x = 12,
   y = 22,
   z = 7,
}

---------------------------------------------------------------------------------
-- water
---------------------------------------------------------------------------------

parameters.water = {}
parameters.water.h    = 0.32 --0.16
parameters.water.rho  = 1000
parameters.water.nu   = 0
parameters.water.D    = 2 * parameters.water.h

-- unrelated water level parameter
parameters.water.level = 0.85 * parameters.box.z

---------------------------------------------------------------------------------
-- floor and banks
---------------------------------------------------------------------------------

function parameters.floor ( box )
   local floordata = {}
   floordata.Nx = 40
   floordata.R = box.x / floordata.Nx
   floordata.dr = floordata.R
   floordata.a = box.x - 2*floordata.R
   floordata.b = box.y - 2*floordata.R
   floordata.c = 1.2 * box.z - 2*floordata.R
   floordata.center = {
      0.5 * ( floordata.a ) + floordata.R,
      0.5 * ( floordata.b ) + floordata.R,
      0.5 * ( floordata.c ) + floordata.R, }

   return floordata
end

-- water generator

local grid = require 'hdis.grid'
local hydro = require 'hdis.hydro'


-- box = size of the box, typically hdis.box
-- floordata = size of the river banks and floor
-- water = liquid parameters
-- H = height of the column (good values is around 1.5*hdis.box )
-- gname = group name
-- Usage: loop and append all objects from returned table
function parameters.watergen ( box, floordata, water, H, gname )
   local collector = {}
   
   local cutscale_xy = 0.92
   local ds = 1.3 * water.h

   local shift = 2 * floordata.R + 2*water.h
   local seeds = grid.grid(
      { shift, shift, shift },
      { cutscale_xy * box.x - shift,
        cutscale_xy * box.y - shift,
        H },
      { ds, ds, ds } )
   local vscale = 0.1
   
   math.randomseed( os.time() )
   
   for i, C in pairs( seeds ) do
      local R = water.h
      local rndx = 2 * vscale* ( math.random() - 0.5 )
      local rndy = 2 * vscale* ( math.random() - 0.5 )
      local rndz = 2 * vscale* ( math.random() - 0.5 )
      
      local c = vec3d:new( C[1], C[2], C[3] )
      local v = vec3d:new( rndx, rndy, rndz )
      local sphdot = hydro.new( water.h, water.rho, water.nu, v, c, gname )
      table.insert( collector, sphdot )
   end

   return collector
end


---------------------------------------------------------------------------------
-- cylinder
---------------------------------------------------------------------------------

parameters.cylinder    = {}
parameters.cylinder.r  = 0.05 -- radius of spheres it is built from, m
parameters.cylinder.dr = parameters.cylinder.r -- distance between elements
parameters.cylinder.m  = 476.0       -- mass, kg
parameters.cylinder.D  = 1.12        -- cylinder diameter, m
parameters.cylinder.R  = 0.5 * parameters.cylinder.D -- radius of cylinder
parameters.cylinder.H  = 0.56                    -- height
parameters.cylinder.depth = 0.41                 -- m, how deep under water

parameters.cylinder.Cd = 1.0        -- Reynold drag coefficient: it is
                                -- typically around 1, however, we can
                                -- also replace it by a function of
                                -- velocity stream in the future

-- crossection under water m^2:
parameters.cylinder.Ax = parameters.cylinder.depth * parameters.cylinder.D

-- moment of inertia is used for cylindrical shell with open ends
parameters.cylinder.Iz = 
   parameters.cylinder.m * parameters.cylinder.r * parameters.cylinder.r

-- special friction for omega attenuation
parameters.cylinder.cw = 1.0

parameters.cylinder.y  = 0.4 * parameters.box.y

-- calculating the position of center of mass, initial
parameters.cylinder.z = parameters.box.z --parameters.water.level

-- Cylinder position
parameters.cylinder.c = { parameters.box.x/2, 
                          parameters.cylinder.y,
                          parameters.cylinder.z }

-- how fast to immerse in water during stage 2
parameters.cylinder.vz_immerse = -2

-- special variable for keeping cylinder angular velocity
parameters.cylinder.wz = 0

-- cylinder control with free rotation around z axis
-- rotating cylinder
function parameters.cylinder.free_rotation_z ( i, dt, m, 
                                               vx, vy, vz, wx, wy, wz, 
                                               fx, fy, fz, tx, ty, tz )
   local cw = parameters.cylinder.cw
   local Iz = parameters.cylinder.Iz

   parameters.cylinder.wz = wz + dt * ( tz  - cw * wz ) / Iz

   return 0, 0, 0, 0, 0, parameters.cylinder.wz
end

-- cylinder control with free rotation around z axis
-- rotating cylinder
function parameters.cylinder.immersing ( i, dt, m, 
                                         vx, vy, vz, wx, wy, wz, 
                                         fx, fy, fz, tx, ty, tz )
   local vz = parameters.cylinder.vz_immerse

   return 0, 0, vz, 0, 0, 0
end

-- cylinder control with free rotation around z axis
-- rotating cylinder
function parameters.cylinder.still ( i, dt, m, 
                                     vx, vy, vz, wx, wy, wz, 
                                     fx, fy, fz, tx, ty, tz )
   return 0, 0, 0, 0, 0, 0
end

-- disk function that keep disks rotating together with the cylinder
function parameters.cylinder.together ( i, dt, m, 
                                        vx, vy, vz, wx, wy, wz, 
                                        fx, fy, fz, tx, ty, tz )
   return 0, 0, 0, 0, 0, parameters.cylinder.wz
end

---------------------------------------------------------------------------------
-- pontons
---------------------------------------------------------------------------------

parameters.ponton = {}
parameters.ponton.r   = parameters.cylinder.r
parameters.ponton.dr  = parameters.cylinder.dr
parameters.ponton.m   = 544.0         -- kg, each!
parameters.ponton.L   = 6.1           -- m
parameters.ponton.H   = 0.61          -- m
parameters.ponton.angle = 30          -- degrees between pontons

-- degrees in rads
parameters.ponton.angle_rad = math.pi * parameters.ponton.angle / 180.0

parameters.ponton.depth = parameters.cylinder.depth -- under water

-- shifts of edges relative to the cylinder
parameters.ponton.x0 = 0.75 * parameters.cylinder.R
parameters.ponton.y0 = 0.75  * parameters.cylinder.R

-- calculating the position of center of mass
local xdist = parameters.ponton.x0 + 
   0.5 * parameters.ponton.L * math.sin( parameters.ponton.angle_rad/2 )

parameters.ponton.x1 = parameters.cylinder.c[1] + xdist
parameters.ponton.x2 = parameters.cylinder.c[1] - xdist

parameters.ponton.y = parameters.cylinder.c[2] + parameters.ponton.y0 +
   0.5 * parameters.ponton.L * math.cos( parameters.ponton.angle_rad/2 )

parameters.ponton.z = parameters.cylinder.z

-- Cylinder position
parameters.ponton.c1 = { parameters.ponton.x1,
                     parameters.ponton.y,
                     parameters.ponton.z }

parameters.ponton.c2 = { parameters.ponton.x2,
                     parameters.ponton.y,
                     parameters.ponton.z }

---------------------------------------------------------------------------------
--   Restoring rotation quaternions for the hulls (pontons)
---------------------------------------------------------------------------------

local angle = math.pi * parameters.ponton.angle / 180.0

local e1 = quat:new( {0,0,1}, math.pi/2 )
local e2 = quat:new( {0,1,0}, math.pi/2 )

local qplus  = quat:new( { 0,0,1}, - angle / 2 )
local qminus = quat:new( { 0,0,1},   angle / 2 )

parameters.ponton.q1 = qplus * e2 * e1
parameters.ponton.q2 = qminus * e2 * e1

---------------------------------------------------------------------------------
--                           Debris parameters
---------------------------------------------------------------------------------

parameters.tree = {}
parameters.tree.boundingR = 1.8     -- m
parameters.tree.boundingRs =        -- radii of bounding spheres for all 5
                                    -- pieces
   { 1.0 * parameters.tree.boundingR,
     0.9 * parameters.tree.boundingR,
     1.1 * parameters.tree.boundingR,
     1.0 * parameters.tree.boundingR,
     1.05 * parameters.tree.boundingR,
   }

local X, Y, Z = parameters.box.x,  parameters.box.y,  parameters.box.z
parameters.tree.cs = { -- debris centers
   { 0.5 * X, 0.3 * Y,  0.7 * Z },
   { 0.4 * X, 0.25 * Y, 0.9 * Z },
   { 0.6 * X, 0.3 * Y, 1.0 * Z },
   { 0.55 * X, 0.2 * Y, 1.0 * Z },
   { 0.3 * X, 0.17 * Y, 0.7 * Z },
}

parameters.tree.functions = {   -- functions creating debris
   libdebris.tshape01,
   libdebris.tshape02,
   libdebris.tshape03,
   libdebris.tshape04,
   libdebris.tshape05,
}

-- not used for now !!!
parameters.tree.Cd = 1.0            -- Reynold drag coefficient: it is typically
                                    -- around 1, however, we can also replace it
                                    -- by a function of velocity stream in the
                                    -- future


-------------------------------------------------------------------------------
return parameters
