--[[--

   libfloat.lua: library generating the external forces function used
   for buoyancy and water resistance. It also adds periodic boundary
   function generator.

--]]

local shape     = require 'hdis.shape'
local control   = require 'hdis.control'

local vec3d   = require 'hdis.vec3d'
local rot3d   = require 'hdis.rot3d'
local quat    = require 'hdis.quat'

local libfloat = {}

-------------------------------------------------------------------
--   shape_data: Calculates some basic parameters for a shape built
--   from equal spheres.
--
--   Important: assumptions are: spheres are equal.
--
-------------------------------------------------------------------

local function shape_data( shape )
   local data = {}

   data.c = aux.deepcopy( shape.body.c ) -- center of mass
   data.v = aux.deepcopy( shape.body.v ) -- cm velocity
   data.V = shape.body.V                 -- total volume
   data.f = vec3d:new()                  -- force
   data.t = vec3d:new()                  -- torque

   data.A = {}                  -- elements
   
   for i, s in ipairs( shape.atoms ) do
      local A = {}                   -- element
      A.P = vec3d:new( s.sbb_c )     -- center
      A.R = s.R                      -- radius
      A.Vreal = 4.0*math.pi * s.R * s.R * s.R / 3.0 -- real volume
      A.S = math.pi * s.R * s.R    -- crossection
      A.V = shape.body.V / #shape.atoms -- corrected with intersection volume

      -- special buoyancy and resistance values
      A.g_P  = nil    -- global coords of P
      A.g_CP = nil    -- global coords radius vector from CM to P
      A.g_Pb = nil    -- global coords of center of buoyancy for Atom A
      A.k  = 1.0      -- volume portion under water

      -- local forces
      A.Fb = vec3d:new()
      A.Fd = vec3d:new()

      table.insert( data.A, A )
   end

   return data
end


-------------------------------------------------------------------
--   Bouyancy and drag standard extforce function generator. Returns
--   function for extforce_functions based on drag Cd (const) and
--   general params
--  
--   Input: params - parameters with materials
--
-------------------------------------------------------------------
function libfloat.bouy_and_drag_gen( shape,
                                     rho_w,
                                     rho_s,
                                     Cd,
                                     zw,
                                     current_v )

   local data = shape_data( shape )
   local Cdx = ( math.pi / 2.0 ) * Cd * rho_w

   local function extforce( i, m, V,
                            cx, cy, cz,
                            q0, q1, q2, q3,
                            vx, vy, vz,
                            wx, wy, wz )
      

      -- Restoring orientation
      local q = quat:new( q0, q1, q2, q3 )
      local R = rot3d:new(q)
      R:restore( q )
      -- 

      -- center of mass vector
      local C = vec3d:new( cx, cy, cz )

      -- center of buoyancy vector
      local Cb = vec3d:new()

      -- total volume under water
      local Vb = 0

      -- center of mass velocity vector
      local v = vec3d:new( vx, vy, vz )

      -- angular velocity vector
      local w = vec3d:new( wx, wy, wz )

      -- total force and torque on the body
      local Fb = vec3d:new()
      local Fd = vec3d:new()
      local Tb = vec3d:new()
      local Td = vec3d:new()

      local F = vec3d:new()
      local T = vec3d:new()

      -- gravity vector
      local g = vec3d:new( 0,0,-9.8 )

      -- Calculating the centers of buoyancy, and each buoyancy force
      -- loop by each atom
      for i, A in ipairs(data.A) do

         A.g_CP = R:apply( A.P )
         A.g_P =  C + A.g_CP

         -- portion of particle under water
         local h = A.R + zw - A.g_P.v[3] -- cup height
         A.g_Pb = vec3d:new( A.g_P )

         if h <= 0 then         -- all above water
            A.k = 0.0
         elseif h >= 2*A.R then -- emmerse fully
            A.k = 1.0
         else                   -- partially under water
            local mu = 3 * A.R - h
            local xi = 2 * A.R - h
            A.k = 0.25* h*h*mu / ( A.R*A.R*A.R )
            A.g_Pb.v[3] = A.g_Pb.v[3] - 0.75 * xi * xi / mu
         end

         local Vi = A.k * A.V
         Cb = Cb + A.g_Pb:scale( Vi )
         Vb = Vb + Vi

         -- calulate the buoyancy force at atom
         A.Fb.v[3] = - ( A.V * A.k ) * rho_w * hdis.g

         Fb = Fb + A.Fb

         -- calculate the drag force at atom

         -- local body velocity at a point
         local vA = v + A.g_CP:cross( w )

         -- local water velocity
         local vw = current_v( A.g_Pb )
         
         -- relative velocity in the stream
         local dv = vw - vA
         local dvabs = dv:abs()

         -- drag coefficient: TODO: correct for area, not volume portion
         local K = A.k * Cdx * dvabs * A.R * A.R
         
         A.Fd = dv:scale( K )

         Fd = Fd + A.Fd

         -- Torque from resistance
         Td = Td + A.Fd:cross( A.g_CP )

      end

      Cb:inplace_scale( 1.0/Vb )
      local CCb = C - Cb

      Tb = Fb:cross(CCb)

      F = Fb + Fd

      T = Tb + Td

      return F.v[1], F.v[2], F.v[3],    T.v[1], T.v[2], T.v[3]
      
   end

   return extforce

end

-------------------------------------------------------------------
--   Water markers generator: input: level (z), radius of the marker,
--   distnace between markers, box, group name and material
--
--   Returns table with marker shapes
-------------------------------------------------------------------
function libfloat.watermarkers( level, r, dr, box, gname, mname )

   local wl = {}                -- resulting table of shapes

   local Nx = math.ceil( ( box.x - 2*r ) / dr )
   local Ny = math.ceil( ( box.y - 2*r ) / dr )
   
   drx = ( box.x - 2*r ) / Nx
   dry = ( box.y - 2*r ) / Ny

   -- generating the surface of water
   for i = 0, Nx do
      local x = r + i * drx
      for j = 0, Ny do
         local y = r + j * dry
         
         local point = shape.sphere( r, gname, mname )
         point.body.c = { x, y, level }
         point.body.control = control.builtin.steady
         point.body.noxbound = true
         point.body.noybound = true
         table.insert( wl, point )
      end
   end

   return wl
end

-------------------------------------------------------------------
--   Object standard periodic in x and y repositioning function
--   generator. Input is a box with dimensions
-------------------------------------------------------------------
function libfloat.repos_gen( xmin, xmax, ymin, ymax )

   local function repos( i, m, V,
                         cx, cy, cz,
                         vx, vy, vz )
      local Cx = cx
      local Cy = cy
      local Cz = cz
      
      if cx < xmin then
         Cx = xmax
      elseif cx > xmax then
         Cx = xmin
      end
      
      if cy < ymin then
         Cy = ymax
      elseif cy > ymax then
         Cy = ymin
      end
      
      return Cx, Cy, Cz
   end

   return repos

end



----------------------------------------------------------------------

return libfloat
