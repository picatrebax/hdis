--[[---------------------------------------------------------------------------
   rddp-alaska/rddp-immerse.lua

   Example:

   Adding an RDDP into the system. Moving it into the water until it reaches the
   right level.

   (c)2018 UAF

--]]---------------------------------------------------------------------------


require 'hdis' -- this Coupi module is always required!

local shape = require 'hdis.shape' -- this module contains primitive shapes
local hydro = require 'hdis.hydro' -- hydro particles library
local vec3d = require 'hdis.vec3d' -- 3D vector type for vectors and points
local control = require 'hdis.control' -- control for dem objects
local grid    = require 'hdis.grid' -- grid generator
local sphtool = require 'hdis.sphtool'
local mathx = require 'hdis.mathx'

local aux   = require 'hdis.aux'

package.path = '../?.lua;'..package.path
package.path = 'librddp/?.lua;'..package.path
local materialdb = require 'materialdb'
local params = require 'parameters'

local samplename = 'rddp-main'

---------------------------------------------------------------------------------

hdis.runfrom( 'rddp-immerse-final.h5' )

-------------------------------------------------------------------------------
-- Main definitions and gravity
-------------------------------------------------------------------------------
-- Time/saving parameters
hdis.time = { start = 0, stop = 50, dt = 0.00025, save = 0.02 }

-------------------------------------------------------------------------------
-- Groups and Materials
-------------------------------------------------------------------------------

hdis.groups:add( 'debris' )
hdis.groups:avert ( 'debris', 'ceiling' )

---------------------------------------------------------------------------------
-- control function
---------------------------------------------------------------------------------

hdis.control_functions = {
   params.cylinder.free_rotation_z,
   params.cylinder.still,
   params.cylinder.together,
}

---------------------------------------------------------------------------------
-- monitor
---------------------------------------------------------------------------------

local cyl_data = {}             -- to store cylinder parameters

local function monitor( i, m, V,
                        Ix, Iy, Iz,
                        cx, cy, cz,
                        vx, vy, vz,
                        wx, wy, wz,
                        fx, fy, fz,
                        tx, ty, tz )

   -- just update tree_data to use in aftertimestep
   cyl_data.c = { cx, cy, cz }
   cyl_data.w = { wx, wy, wz }
   cyl_data.f = { fx, fy, fz }
   cyl_data.t = { tx, ty, tz }
end

hdis.monitor_functions = {
   monitor,
}

-------------------------------------------------------------------------------
-- objects
-------------------------------------------------------------------------------

local tree

-------------------------------------------------------------------------------

for i, f in ipairs( params.tree.functions ) do
   tree = f( params.tree.boundingRs[i], 'debris', 'wetwood' )

   tree.body.c = params.tree.cs[i]
   tree.body.v = { 0, 0, 0 }

   tree.body.monitor     = nil
   tree.body.control     = nil
   tree.body.extforce    = nil
   tree.body.positioning = nil
   tree.body.teleport    = true

   tree.body.q = mathx.q_random()

   -- Appending the ball shape to the list of COUPi objects! From now,
   -- our ball is in the system and will be recognized by COUPi.
   hdis.append( tree )
end


---------------------------------------------------------------------------------
--                      After time step: save
---------------------------------------------------------------------------------

-- We save position and forces acting on the lander leg
local ff = assert( io.open( 'cylinder.txt', 'w' ) )

local s1 = string.format( '# Cylinder data\n#\n' )
local s2 = string.format( '# Angular velocity, forces and torques:\n'..
                             '# f - all force without water resistance\n' ..
                             '# fw - water resistance force\n' )
local s3 =  string.format( '# [1]t ' .. 
                              '[2]wz,rad/s ' ..
                              '[3]fx,N [4]fy,N [5]fz,N ' ..
                              '[6]tx,Nm [7]ty,Nm [8]tz,Nm ' )

ff:write( s1 )
ff:write( s2 )
ff:write( s3 )

local function aftertimestep( t, dt, n )
   -- Saving to the file each 100th step
   if n % 100 == 0 then
      local output =
         string.format(
            '% 2.10f  %f  %f %f %f  %f %f %f\n',
            t, cyl_data.w[3],
            cyl_data.f[1], cyl_data.f[2], cyl_data.f[3],
            cyl_data.t[1], cyl_data.t[2], cyl_data.t[3] )
      ff:write( output )
      ff:flush()
   end

end

hdis.callback.aftertimestep = aftertimestep

---------------------------------------------------------------------------------
-- Portal: for moving objects from one side to another
---------------------------------------------------------------------------------

local floordata = params.floor( hdis.box )

hdis.portal = {}
hdis.portal.entrance_y = hdis.box.y - 2*floordata.R
hdis.portal.exit_y = params.tree.boundingR + 2*floordata.R
hdis.portal.exit_z = hdis.box.z
hdis.portal.v = { 0, 0, 0 }


-------------------------------------------------------------------------------
-- Display options
-------------------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave( samplename .. '-' )
hdis.callback.aftersave = hdis.callback.std.aftersave_long()
hdis.callback.finalize = hdis.callback.std.finalize( samplename .. '-final' )

