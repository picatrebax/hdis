debug_log = 0
model_width = 1247
model_height = 705
model_distance = 5.544000
zoom_speed = 0.100000
render_method = 2
render_switch = 20
trans_speed = 0.050000
rotate_speed = 0.010000
sphere_resize = 1.100000
redraw_distance = 0.001000
display_mode = 0
arrow_mode = 1
clip = 0
clip_trans_speed = 0.050000
hdisvis_logo = 1
bounding_box = 1
axis_display = 0
selection = 0
ptriangles = 1
time_display = 1
springs = 1
spring_width = 1
force_max = 1.000000
force_min = 0.000000
speed_width = 1
speed_max = 4.000000
speed_base_size = 0.003000
speed_vec_size = 0.010000
atom_colors = {
{ r = 0.7098, g = 0.9569, b = 1.0000, r_var = 0.0000, g_var = 0.0000, b_var = 0.0000, a = 0.4500 },
{ r = 0.9451, g = 0.2000, b = 0.2000, r_var = 0.0000, g_var = 0.0000, b_var = 0.0000, a = 1.0000 },
{ r = 1.0000, g = 1.0000, b = 1.0000, r_var = 0.0000, g_var = 0.0000, b_var = 0.0000, a = 0.0200 },
{ r = 0.0588, g = 0.0902, b = 0.6941, r_var = 0.0000, g_var = 0.0000, b_var = 0.0000, a = 1.0000 },
{ r = 0.1373, g = 0.9529, b = 0.1412, r_var = 0.0000, g_var = 0.0000, b_var = 0.0000, a = 1.0000 }
	}
spring_colors = {
	}
ptriangle_colors = {
	}
