debug_log = 0
model_width = 1721
model_height = 866
model_distance = 6.328000
zoom_speed = 0.100000
render_method = 1
render_switch = 20
trans_speed = 0.050000
rotate_speed = 0.010000
sphere_resize = 1.100000
redraw_distance = 0.001000
display_mode = 0
arrow_mode = 1
clip = 0
clip_trans_speed = 0.050000
hdisvis_logo = 1
bounding_box = 1
axis_display = 1
selection = 0
ptriangles = 1
time_display = 1
springs = 1
spring_width = 1
force_max = 1.000000
force_min = 0.000000
speed_width = 1
speed_max = 4.000000
speed_base_size = 0.003000
speed_vec_size = 0.010000
atom_colors = {
{ r = 0.7098, g = 0.9569, b = 1.0000, r_var = 0.0000, g_var = 0.0000, b_var = 0.0000, a = 0.4500 },
{ r = 0.4745, g = 0.4745, b = 0.4745, r_var = 0.0000, g_var = 0.0000, b_var = 0.0000, a = 1.0000 },
{ r = 0.8902, g = 0.5294, b = 0.5294, r_var = 0.0000, g_var = 0.0000, b_var = 0.0000, a = 0.0000 },
{ r = 0.5554, g = 0.2074, b = 0.4358, r_var = 0.0000, g_var = 0.0000, b_var = 0.0000, a = 0.0000 },
{ r = 0.5028, g = 0.8889, b = 0.0944, r_var = 0.0000, g_var = 0.0000, b_var = 0.0000, a = 1.0000 },
{ r = 0.2784, g = 0.1451, b = 0.9843, r_var = 0.0000, g_var = 0.0000, b_var = 0.0000, a = 1.0000 },
{ r = 0.9756, g = 0.4777, b = 0.3314, r_var = 0.0000, g_var = 0.0000, b_var = 0.0000, a = 1.0000 }
	}
spring_colors = {
	}
ptriangle_colors = {
	}
