--[[---------------------------------------------------------------------------
   portal.lua

   Example:

   Testing the transposition of DEM objects in the model

   (c)2018 UAF

--]]---------------------------------------------------------------------------

require 'hdis' -- this Coupi module is always required!

local shape = require 'hdis.shape' -- this module contains primitive shapes
local vec3d = require 'hdis.vec3d'
local aux   = require 'hdis.aux'

local materialdb = require 'materialdb'

local samplename = 'portal'

-------------------------------------------------------------------------------
-- Main definitions and gravity
-------------------------------------------------------------------------------
-- Time/saving parameters
hdis.time = { start = 0, stop = 10, dt = 0.001, save = 0.1 }

-- The area of simulation
hdis.box = { x = 1, y = 4, z = 1 }

-- m/s^2, gravity
hdis.g = 0

-------------------------------------------------------------------------------
-- Groups and Materials
-------------------------------------------------------------------------------

hdis.material:add( materialdb.wood )

hdis.groups:add( 'ball' )

---------------------------------------------------------------------------------

local R = 0.05
local ball = shape.sphere( R, 'ball', 'wood' )
ball.body.c = vec3d:new( hdis.box.x / 2,
                         hdis.box.y / 2,
                         hdis.box.z / 2  )
ball.body.v = vec3d:new( 0, 1, 0 )
ball.body.teleport = true            -- for the bodies we want to react on portal
hdis.append( ball )

-------------------------------------------------------------------------------
hdis.portal = {}
hdis.portal.entrance_y = hdis.box.y - 4 * R
hdis.portal.exit_y = 4 * R
hdis.portal.exit_z = hdis.box.z - 4 * R

-------------------------------------------------------------------------------
-- Display options
-------------------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave( samplename .. '-' )
hdis.callback.aftersave = hdis.callback.std.aftersave_long()
