--[[---------------------------------------------------------------------------
   materialdb.lua

   Database of some predefined materials

   (c)2018 UAF

--]]---------------------------------------------------------------------------


local materialdb = {}

materialdb.cork = {
   name = 'cork',
   rho = 240,                   -- kg/m^3
   G   = 0.001,                 -- GPa
   nu  = 0.4,
   mu  = 0.8,
   CR  = 0.1,
}

materialdb.wood = {
   name = 'wood',
   rho = 700,
   G = 0.01,
   nu = 0.33,
   mu = 0.5,
   CR = 0.2,
}

materialdb.styrofoam = {
   name = 'styrofoam',
   rho = 700,
   G = 0.01,
   nu = 0.33,
   mu = 0.5,
   CR = 0.2,
}

materialdb.ice = {
   name = 'ice',
   rho = 916.7,
   G = 20,
   nu = 0.27,
   mu = 0.01,
   CR = 0.3,
}

materialdb.polypropylene = {
   name = 'polypropylene',
   rho = 1175,
   G = 1,
   nu = 0.28,
   mu = 0.15,
   CR = 0.8,
}

---------------------------------------------------------------------------------
-- RDDP related materials
---------------------------------------------------------------------------------


materialdb.wetwood = {
   name = 'wetwood',
   rho = 850.0, 
   G   = 0.01, 
   nu  = 0.27, 
   mu  = 0.3, 
   CR  = 0.55
}

materialdb.cylinder = {
   name = "cylinder",
   G   = 1,
   nu  = 0.27,
   mu  = 0.5,
   CR  = 0.6,
   rho = 4000,
   gamma = 0,
}

materialdb.ponton = {
   name = "ponton",
   G   = 1,
   nu  = 0.27,
   mu  = 0.5,
   CR  = 0.6,
   rho = 4000,
   gamma = 0,
}

function materialdb.gcorrected ( k, material )
   material.G = k * material.G
   return material
end

-------------------------------------------------------------------------------

return materialdb
