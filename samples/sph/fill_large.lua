--[[---------------------------------------------------------------------------
   fill_large.lua

   Example:

   (c)2018 UAF

   Usage: hdis fill_large.lua
--]]---------------------------------------------------------------------------

require 'hdis' -- this Coupi module is always required!

local shape = require 'hdis.shape' -- this module contains primitive shapes
local hydro = require 'hdis.hydro' -- hydro particles library
local vec3d = require 'hdis.vec3d' -- 3D vector type for vectors and points
local aux   = require 'hdis.aux'
require 'hdis.control'

-------------------------------------------------------------------------------
-- Main definitions and gravity
-------------------------------------------------------------------------------
-- Time/saving parameters
hdis.time = { start = 0, stop = 5, dt = 0.000005, save = 0.005 }

-- The area of simulation
hdis.box = { x = 1, y = 2, z = 2 }

-- m/s^2, gravity
hdis.g = -9.8

hdis.contact_detection = {}
hdis.contact_detection.method = 'spsp'

-------------------------------------------------------------------------------
-- Groups and Materials
-------------------------------------------------------------------------------
-- Materials (rho - density in kg/m^3, G - stiffness in GPa, nu - Poisson
-- coefficient, mu - Coloumb friction (static/dynamic), CR - restitution
-- coefficient.

local steel = {
   name = 'steel',
   rho = 7850.0,
   G = 80,
   nu = 0.27,
   mu = 0.3,
   CR =0.8
}

hdis.material:add( steel )

hdis.groups:add( 'water' ):add( 'ball' ):add( 'floor' )

-------------------------------------------------------------------------------
-- Boundary is made of the same material... for now. It can be done by just
-- simple assignment as follows. There are other ways to do this too, but this
-- one seems to be the simplest approach.
-------------------------------------------------------------------------------
hdis.boundary.material = 'steel'

---------------------------------------------------------------------------------
-- water properties
---------------------------------------------------------------------------------

local water = {}
water.R     = 0.015
water.rho   = 1000
water.nu    = 0
water.D     = 2 * water.R

water.Xr = 1
water.Lx = water.D
water.Rx = water.Xr * hdis.box.x - water.D
water.Wx = water.Rx - water.Lx

water.Yr = 0.85
water.Ly = water.D
water.Ry = water.Yr * hdis.box.y - water.D
water.Wy = water.Ry - water.Ly

water.Zr = 0.9
water.Lz = water.D
water.Rz = water.Zr * hdis.box.z - water.D
water.Wz = water.Rz - water.Lz

water.freq = 1.2
water.nx = water.freq* math.ceil( water.Wx / water.D )
water.ny = water.freq* math.ceil( water.Wy / water.D )
water.nz = water.freq* math.ceil( water.Wz / water.D )

water.dx = water.Wx / water.nx
water.dy = water.Wy / water.ny
water.dz = water.Wz / water.nz


local floor = {}
floor.R   = water.R
floor.rho = water.rho
floor.nu  = water.nu

-------------------------------------------------------------------------------
-- Smooth particles
-------------------------------------------------------------------------------

vscale = 0.1

math.randomseed( os.time() )

for k = 1, water.nz do
  for j = 1, water.ny do
     for i = 1, water.nx do

        local rndx = 2 * vscale* ( math.random() - 0.5 )
        local rndy = 2 * vscale* ( math.random() - 0.5 )
        local rndz = 2 * vscale* ( math.random() - 0.5 )

        local x = water.Lx + ( i - 1 ) * water.dx
        local y = water.Ly + ( j - 1 ) * water.dy
        local z = water.Lz + ( k - 1 ) * water.dz

        local c = vec3d:new( x, y, z )
        local v = vec3d:new( rndx, rndy, rndz )
        local sphdot = hydro.new( water.R, water.rho, water.nu, v, c, 'water' ) 
        hdis.append(  sphdot  )
    end
  end
end

---------------------------------------------------------------------------------
-- river floor
---------------------------------------------------------------------------------

local freq = 1

local nx = freq* math.ceil( hdis.box.x / floor.R )
local ny = freq* math.ceil( hdis.box.x / floor.R )

local dx = hdis.box.x / nx
local dy = hdis.box.y / ny

local banks = {}
banks.H =  hdis.box.z
local nz = freq* math.ceil( banks.H / floor.R )
local dz = banks.H / nz

local km = 1.4

for j = 1, ny do
   for i = 1, nx do
      
      local R = floor.R
      local c = vec3d:new( dx/2 + (i-1)*dx, dy/2 + (j-1)*dy, 0 )
      local v = vec3d:new()
      
      local sphdot = hydro.new(
         water.R,
         water.rho,
         water.nu,
         v, c, 'floor',
         bit32.bor( hydro.flag.still, hydro.flag.boundary ),
         km )
      
      hdis.append(  sphdot  )
   end
end

for j = 1, ny do
   for i = 1, nx do
      
      local R = floor.R
      local c = vec3d:new( dx/2 + (i-1)*dx, dy/2 + (j-1)*dy, nz * dz - dz/2 )
      local v = vec3d:new()
      
      local sphdot = hydro.new(
         water.R,
         water.rho,
         water.nu,
         v, c, 'floor',
         bit32.bor( hydro.flag.still, hydro.flag.boundary ),
         km )
      
      hdis.append(  sphdot  )
   end
end


---------------------------------------------------------------------------------
-- river banks
---------------------------------------------------------------------------------

for k = 1, nz do
   for j = 1, ny do

      local R = floor.R
      local c = vec3d:new( dx/2, dy/2 + (j-1)*dy, dz/2 + (k-1)*dz )
      local v = vec3d:new()
      
      local sphdot = hydro.new(
         water.R,
         water.rho,
         water.nu,
         v, c, 'floor',
         bit32.bor( hydro.flag.still, hydro.flag.boundary ),
         km )
      
      hdis.append(  sphdot  )
   end
end      

for k = 1, nz do
   for j = 1, ny do

      local R = floor.R
      local c = vec3d:new( hdis.box.x - dx/2, dy/2 + (j-1)*dy, dz/2 + (k-1)*dz )
      local v = vec3d:new()
      
      local sphdot = hydro.new(
         water.R,
         water.rho,
         water.nu,
         v, c, 'floor',
         bit32.bor( hydro.flag.still, hydro.flag.boundary ),
         km )
      
      hdis.append(  sphdot  )
   end
end      

-- walls for x=0 and x=X

for k = 1, nz do
   for i = 1, nx do

      local R = floor.R
      local c = vec3d:new( dx/2 + (i-1)*dx, dy/2, dz/2 + (k-1)*dz )
      local v = vec3d:new()
      
      local sphdot = hydro.new(
         water.R,
         water.rho,
         water.nu,
         v, c, 'floor',
         bit32.bor( hydro.flag.still, hydro.flag.boundary ),
         km )
      
      hdis.append(  sphdot  )
   end
end      

for k = 1, nz do
   for i = 1, nx do

      local R = floor.R
      local c = vec3d:new( dx/2 + (i-1)*dx, hdis.box.y - dy/2, dz/2 + (k-1)*dz )
      local v = vec3d:new()
      
      local sphdot = hydro.new(
         water.R,
         water.rho,
         water.nu,
         v, c, 'floor',
         bit32.bor( hydro.flag.still, hydro.flag.boundary ),
         km )
      
      hdis.append(  sphdot  )
   end
end      


print( 'Number of SPH dots:', #hdis.dots )

-- Object addition
-------------------------------------------------------------------------------
-- We create a ball of radius 0.05 m, that belongs to group 'ball' and made out
-- of our material 'steel'. This function just forms a 'shape': a body, atoms
-- it is built from, and points that are atoms 'skeleton'

local ball = hdis.shape.sphere( 0.05, 'ball', 'steel' )

-- center of mass location: we have to place our ball in the box. vec3d is a
-- vector with 3 components. We don't distinguish between space points and
-- vectors.
ball.body.c = vec3d:new( 0.5 * hdis.box.x, 0.5 * hdis.box.y,
   0.5 * hdis.box.z )
ball.body.control = hdis.control.builtin.still --we will freeze in the space

-- Appending the ball shape to the list of COUPi objects! From now, our ball is
-- in the system and will be recognized by COUPi.
hdis.append( ball )


---------------------------------------------------------------------------------
hdis.boundary = { opentop = true }

-------------------------------------------------------------------------------
-- Display options
-------------------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave( 'large-' )
hdis.callback.aftersave = hdis.callback.std.aftersave_long()
