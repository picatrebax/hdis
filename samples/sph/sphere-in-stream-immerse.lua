--[[---------------------------------------------------------------------------
  sphere-in-stream-immerse.lua

   Example: Cylinder in a flow

   (c)2018 UAF

   Usage: hdis sphere-in-stream-immerse.lua
--]]---------------------------------------------------------------------------

require 'hdis' -- this HDIS module is always required!

local vec3d = require 'hdis.vec3d' -- 3D vector type for vectors and points
local aux   = require 'hdis.aux'
local shape = require 'hdis.shape'
local tool = require 'hdis.sphtool'

local samplename = 'sphere-in-stream-immerse'

local hfile = 'sphere-in-stream-fill-final.h5'
hdis.runfrom( hfile )

hdis.nummethods = {}
hdis.nummethods.omega = "Walton"

-------------------------------------------------------------------------------
-- Main definitions and gravity
-------------------------------------------------------------------------------
-- Time/saving parameters
hdis.time = { start = 0, stop = 4, dt = 0.00002, save = 0.01 }

-------------------------------------------------------------------------------
-- Display options
-------------------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave( samplename .. '-' )
hdis.callback.aftersave = hdis.callback.std.aftersave_long()

-------------------------------------------------------------------------------
-- Groups and Materials
-------------------------------------------------------------------------------
-- Materials (rho - density in kg/m^3, G - stiffness in GPa, nu - Poisson
-- coefficient, mu - Coloumb friction (static/dynamic), CR - restitution
-- coefficient.

local wood = {
   name = 'wood',
   rho = 1000.0, --7850.0,
   G = 0.1,
   nu = 0.27,
   mu = 0.3,
   CR = 0.8
}

local hardwood = {
   name = 'hardwood',
   rho = 500.0, --7850.0,
   G = 0.1,
   nu = 0.27,
   mu = 0.3,
   CR = 0.8
}

local plastic = {
   name = 'plastic',
   rho = 600.0, --7850.0,
   G = 0.1,
   nu = 0.27,
   mu = 0.3,
   CR = 0.8
}

hdis.material:add( hardwood ):add( plastic )

hdis.groups:add( 'sphere' )

hdis.groups:avert( 'sphere', 'ceiling' )
hdis.groups:avert( 'sphere', 'floor' )

---------------------------------------------------------------------------------
-- Object addition
-------------------------------------------------------------------------------
-- We create a ball of radius 0.05 m, that belongs to group 'ball' and made out
-- of our material 'wood'. This function just forms a 'shape': a body, atoms
-- it is built from, and points that are atoms 'skeleton'

local ballD = 0.4
local ballR = ballD / 2
local ball = hdis.shape.sphere( ballR, 'sphere', 'wood' )

-- center of mass location: we have to place our ball in the box. vec3d is a
-- vector with 3 components. We don't distinguish between space points and
-- vectors.
ball.body.c = vec3d:new( 0.5  * hdis.box.x,
                         0.33 * hdis.box.y,
                         hdis.box.z + ballR )
ball.body.monitor = 1
ball.body.control = 1

-- Appending the ball shape to the list of HDIS objects! From now, our ball is
-- in the system and will be recognized by HDIS.
hdis.append( ball )

---------------------------------------------------------------------------------

-- special global table to contain variables to change in functions
myhdis = {}
myhdis.t = 0
myhdis.dt = 0
myhdis.n = 0
myhdis.stop_h = 0.57 * hdis.box.z
myhdis.current_z = 10 * hdis.box.z

-- Function to set time, timestep and step number to use in other
-- functions
function pretimestep( t, dt, n )
   myhdis.t = t
   myhdis.dt = dt
   myhdis.n = n
end

hdis.callback.pretimestep = pretimestep

local fp = assert( io.open( 'sphere.txt', 'w' ) )

-- Monitoring all the values related to i-th body.
local function monitor( i, m, V,
                        Ix, Iy, Iz,
                        cx, cy, cz,
                        vx, vy, vz,
                        wx, wy, wz,
                        fx, fy, fz,
                        tx, ty, tz )
   myhdis.current_z = cz
   
   if myhdis.n % 10 == 0 then
      local str = string.format(
         "%d %f  %f %f %f  %f %f %f\n",
          myhdis.n, myhdis.t, fx, fy, fz, tx, ty, tz )
      fp:write(str)
      fp:flush()
   end
end

-- list of monitor functions
hdis.monitor_functions = {
   monitor,
}

-- Finalizing function

local function finalize( )
   fp:close()
   print ( "FILE CLOSED; t = ", myhdis.t )
   return samplename .. '-final.h5'
end

hdis.callback.finalize = finalize

---------------------------------------------------------------------------------

local function control( n, dt, m, vx, vy, vz, wx, wy, wz, fx, fy, fz, tx, ty, tz )
   -- t0 second to move it down
   local t0 = 0.5
   local vz = -1

   if myhdis.current_z > myhdis.stop_h then
      return 0, 0, vz, 0, 0, 0
   else
      return 0, 0, 0, 0, 0, 0
   end
   
end

hdis.control_functions = {
   control,
}
