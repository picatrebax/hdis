--[[---------------------------------------------------------------------------
   shooting.lua

   Example: 
   
   different sph particles are shot towards a large DEM sphere

   (c)2018 UAF

--]]---------------------------------------------------------------------------

require 'hdis' -- this Coupi module is always required!

local shape = require 'hdis.shape' -- this module contains primitive shapes
local hydro = require 'hdis.hydro' -- hydro particles library
local vec3d = require 'hdis.vec3d' -- 3D vector type for vectors and points
local aux   = require 'hdis.aux'
require 'hdis.control'

local samplename = 'shooting'

-------------------------------------------------------------------------------
-- Main definitions and gravity
-------------------------------------------------------------------------------
-- Time/saving parameters
hdis.time = { start = 0, stop = 0.33, dt = 0.0000001, save = 0.005 }

-- The area of simulation
hdis.box = { x = 1, y = 1, z = 1 }

-- m/s^2, gravity
hdis.g = 0

hdis.contact_detection = {}
hdis.contact_detection.method = 'spsp'

-------------------------------------------------------------------------------
-- Groups and Materials
-------------------------------------------------------------------------------
-- Materials (rho - density in kg/m^3, G - stiffness in GPa, nu - Poisson
-- coefficient, mu - Coloumb friction (static/dynamic), CR - restitution
-- coefficient.

local steel = {
   name = 'steel',
   rho = 7850.0,
   G = 1,
   nu = 0.27,
   mu = 0.3,
   CR =0.8
}

hdis.material:add( steel )

hdis.groups:add( 'water' ):add( 'ball' )

-------------------------------------------------------------------------------
-- Boundary is made of the same material... for now. It can be done by just
-- simple assignment as follows. There are other ways to do this too, but this
-- one seems to be the simplest approach.
-------------------------------------------------------------------------------
hdis.boundary.material = 'steel'

---------------------------------------------------------------------------------
-- water properties
---------------------------------------------------------------------------------

local water = {}
water.R     = 0.015
water.rho   = 1000
water.nu    = 0

---------------------------------------------------------------------------------
-- Object addition
-------------------------------------------------------------------------------
-- We create a ball of radius 0.05 m, that belongs to group 'ball' and made out
-- of our material 'steel'. This function just forms a 'shape': a body, atoms
-- it is built from, and points that are atoms 'skeleton'

local ball = hdis.shape.sphere( 10*water.R, 'ball', 'steel' )

-- center of mass location: we have to place our ball in the box. vec3d is a
-- vector with 3 components. We don't distinguish between space points and
-- vectors.
ball.body.c = vec3d:new(
   0.5 * hdis.box.x,
   0.5 * hdis.box.y,
   0.5 * hdis.box.z
)
--ball.body.control = hdis.control.builtin.still --we will freeze in the space

-- Appending the ball shape to the list of COUPi objects! From now, our ball is
-- in the system and will be recognized by COUPi.
hdis.append( ball )

-------------------------------------------------------------------------------
-- Smooth particles
-------------------------------------------------------------------------------
local R = water.R
local c = vec3d:new( 0.25*hdis.box.x, 0.25* hdis.box.y, 0.5*hdis.box.z )
local v = vec3d:new( 1, 1, 0 )

local sphdot

sphdot = hydro.new( water.R, water.rho, water.nu, v, c, 'water' ) 
hdis.append(  sphdot  )

c = vec3d:new( hdis.box.x-0.4*hdis.box.x, hdis.box.y-0.27* hdis.box.y, 0.5*hdis.box.z )
v = vec3d:new( -1, -1, 0 )
sphdot = hydro.new( water.R, water.rho, water.nu, v, c, 'water' ) 
hdis.append(  sphdot  )

c = vec3d:new( hdis.box.x-0.2*hdis.box.x, hdis.box.y-0.27* hdis.box.y, 0.6*hdis.box.z )
v = vec3d:new( -1, -1, 0 )
sphdot = hydro.new( water.R, water.rho, water.nu, v, c, 'water' ) 
hdis.append(  sphdot  )

c = vec3d:new( 0.2*hdis.box.x, hdis.box.y-0.27* hdis.box.y, 0.4*hdis.box.z )
v = vec3d:new( 1, -1, 0 )
sphdot = hydro.new( water.R, water.rho, water.nu, v, c, 'water' ) 
hdis.append(  sphdot  )

c = vec3d:new( 0.5*hdis.box.x, 0.5* hdis.box.y, 0.1*hdis.box.z )
v = vec3d:new( 0, 0, 1 )
sphdot = hydro.new( water.R, water.rho, water.nu, v, c, 'water' ) 
hdis.append(  sphdot  )

c = vec3d:new( 0.5*hdis.box.x, 0.5* hdis.box.y, 0.9*hdis.box.z )
v = vec3d:new( 0, 0, -1 )
sphdot = hydro.new( water.R, water.rho, water.nu, v, c, 'water' ) 
hdis.append(  sphdot  )

c = vec3d:new( 0.2*hdis.box.x, 0.5* hdis.box.y, 0.61*hdis.box.z )
v = vec3d:new( 1, 0, 0 )
sphdot = hydro.new( water.R, water.rho, water.nu, v, c, 'water' ) 
hdis.append(  sphdot  )

c = vec3d:new( hdis.box.x - 0.2*hdis.box.x, 0.5* hdis.box.y, 0.61*hdis.box.z )
v = vec3d:new( -1, 0, 0 )
sphdot = hydro.new( water.R, water.rho, water.nu, v, c, 'water' ) 
hdis.append(  sphdot  )

c = vec3d:new( hdis.box.x - 0.2*hdis.box.x, 0.5* hdis.box.y, 0.37*hdis.box.z )
v = vec3d:new( -1, 0, 0 )
sphdot = hydro.new( water.R, water.rho, water.nu, v, c, 'water' ) 
hdis.append(  sphdot  )

---------------------------------------------------------------------------------
print( 'Number of SPH dots:', #hdis.dots )

---------------------------------------------------------------------------------
hdis.boundary = { opentop = true, openbottom = false }

-------------------------------------------------------------------------------
-- Display options
-------------------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave( samplename .. '-' )
hdis.callback.aftersave = hdis.callback.std.aftersave_long()
