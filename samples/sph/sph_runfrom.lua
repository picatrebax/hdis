--[[---------------------------------------------------------------------------
   basic_sphere.lua

   Example:

   (c)2017 Coupi, Inc.

   Usage: hdis water_column.lua
--]]---------------------------------------------------------------------------

require 'hdis' -- this Coupi module is always required!

hdis.runfrom ( 'bsphere-00000000ms.h5' )

-------------------------------------------------------------------------------
-- Main definitions and gravity
-------------------------------------------------------------------------------
-- Time/saving parameters
hdis.time = { start = 0, stop = 5, dt = 0.0001, save = 0.02 }

-------------------------------------------------------------------------------
-- Display options
-------------------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave( 'bsphere-' )
hdis.callback.aftersave = hdis.callback.std.aftersave_long()
