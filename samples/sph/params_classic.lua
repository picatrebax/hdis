--[[---------------------------------------------------------------------------
   params_classic.lua

   This file is used to have some basic parameters for water and river banks.

   (c)2018 UAF

--]]---------------------------------------------------------------------------

local params_classic = {}

-- sparse water

params_classic.water = {}
params_classic.water.h    = 0.04
params_classic.water.rho  = 1000
params_classic.water.nu   = 0
params_classic.water.D    = 2 * params_classic.water.h

-- medium water

params_classic.water_medium = {}
params_classic.water_medium.h    = 0.03
params_classic.water_medium.rho  = 1000
params_classic.water_medium.nu   = 0
params_classic.water_medium.D    = 2 * params_classic.water_medium.h

-- fine water

params_classic.water_fine = {}
params_classic.water_fine.h    = 0.02
params_classic.water_fine.rho  = 1000
params_classic.water_fine.nu   = 0
params_classic.water_fine.D    = 2 * params_classic.water_fine.h

-- floor and banks

function params_classic.floor ( box )
   local floordata = {}
   floordata.Nx = 20
   floordata.R = box.x / floordata.Nx
   floordata.dr = floordata.R
   floordata.a = box.x - 2*floordata.R
   floordata.b = box.y - 2*floordata.R
   floordata.c = 2.0 * box.z - 2*floordata.R
   floordata.center = {
      0.5 * ( floordata.a ) + floordata.R,
      0.5 * ( floordata.b ) + floordata.R,
      0.5 * ( floordata.c ) + floordata.R, }

   return floordata
end

-- water generator

local grid = require 'hdis.grid'
local hydro = require 'hdis.hydro'


-- box = size of the box, typically hdis.box
-- floordata = size of the river banks and floor
-- water = liquid parameters
-- H = height of the column (good values is around 1.5*hdis.box )
-- gname = group name
-- Usage: loop and append all objects from returned table
function params_classic.watergen ( box, floordata, water, H, gname )
   local collector = {}
   
   local cutscale_xy = 0.92
   local ds = 1.3 * water.h

   local shift = 2 * floordata.R + 2*water.h
   local seeds = grid.grid(
      { shift, shift, shift },
      { cutscale_xy * box.x - shift,
        cutscale_xy * box.y - shift,
        H },
      { ds, ds, ds } )
   local vscale = 0.1
   
   math.randomseed( os.time() )
   
   for i, C in pairs( seeds ) do
      local R = water.h
      local rndx = 2 * vscale* ( math.random() - 0.5 )
      local rndy = 2 * vscale* ( math.random() - 0.5 )
      local rndz = 2 * vscale* ( math.random() - 0.5 )
      
      local c = vec3d:new( C[1], C[2], C[3] )
      local v = vec3d:new( rndx, rndy, rndz )
      local sphdot = hydro.new( water.h, water.rho, water.nu, v, c, gname )
      table.insert( collector, sphdot )
   end

   return collector
end



-------------------------------------------------------------------------------
return params_classic
