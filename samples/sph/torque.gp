fname = 'torque.txt'

plot [0.1:0.3] fname using 1:(column(6)) with lines lw 2

# [1]t
# [2] wx    [3] wy    [4] wz,rad/s
# [5] fx,N  [6] fy,N  [7] fz,N
# [8] tx,Nm [9] ty,Nm [10] tz
