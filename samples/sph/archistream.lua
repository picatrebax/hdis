--[[---------------------------------------------------------------------------
   archistream.lua

   Example: Placing objects on the surface of water from fillbox.lua example

   (c)2018 UAF

   Usage: hdis contin.lua
--]]---------------------------------------------------------------------------

require 'hdis' -- this Coupi module is always required!

local vec3d = require 'hdis.vec3d' -- 3D vector type for vectors and points
local aux   = require 'hdis.aux'
local shape = require 'hdis.shape'

local materialdb = require 'materialdb'
local params = require 'params_classic'
local water = params.water

local samplename = 'archistream'

local hfile = 'periodic-glass-final.h5'
hdis.runfrom( hfile )

hdis.nummethods = {}
hdis.nummethods.omega = "Walton"

-------------------------------------------------------------------------------
-- Main definitions and gravity
-------------------------------------------------------------------------------
-- Time/saving parameters
hdis.time = { start = 0, stop = 10, dt = 0.0001, save = 0.01 }

-------------------------------------------------------------------------------
-- Groups and Materials
-------------------------------------------------------------------------------
-- Materials (rho - density in kg/m^3, G - stiffness in GPa, nu - Poisson
-- coefficient, mu - Coloumb friction (static/dynamic), CR - restitution
-- coefficient.

local wood = {
   name = 'wood',
   rho = 250.0, --7850.0,
   G = 0.1,
   nu = 0.27,
   mu = 0.3,
   CR = 0.8
}

local hardwood = {
   name = 'hardwood',
   rho = 500.0, --7850.0,
   G = 0.1,
   nu = 0.27,
   mu = 0.3,
   CR = 0.8
}

local plastic = {
   name = 'plastic',
   rho = 900.0, --7850.0,
   G = 0.1,
   nu = 0.27,
   mu = 0.3,
   CR = 0.8
}

hdis.material:add( hardwood ):add( plastic ):add( wood )

hdis.groups:add( 'ball' )
hdis.groups:add( 'ball2' )
hdis.groups:add( 'ball3' )

hdis.groups:avert( 'ball', 'ceiling' )
hdis.groups:avert( 'ball2', 'ceiling' )
hdis.groups:avert( 'ball3', 'ceiling' )

---------------------------------------------------------------------------------
-- Object addition
-------------------------------------------------------------------------------
-- We create a ball of radius 0.05 m, that belongs to group 'ball' and made out
-- of our material 'wood'. This function just forms a 'shape': a body, atoms
-- it is built from, and points that are atoms 'skeleton'

--local ball = hdis.shape.sphere( 0.05, 'ball', 'wood' )

local ballparams = {}
ballparams.R = 0.2

local ball = shape.sphere( ballparams.R, 'ball', 'wood' )


-- center of mass location: we have to place our ball in the box. vec3d is a
-- vector with 3 components. We don't distinguish between space points and
-- vectors.
ball.body.c = vec3d:new( 0.5 * hdis.box.x,
                         0.21 * hdis.box.y,
                         1.2 * hdis.box.z )
ball.body.teleport = true

-- Appending the ball shape to the list of COUPi objects! From now, our ball is
-- in the system and will be recognized by COUPi.
--hdis.append( ball )


local ball = shape.threespheres( 0.5*ballparams.R, 'ball2', 'hardwood' )


-- center of mass location: we have to place our ball in the box. vec3d is a
-- vector with 3 components. We don't distinguish between space points and
-- vectors.
ball.body.c = vec3d:new( 0.5 * hdis.box.x,
                         0.45 * hdis.box.y,
                         1.3 * hdis.box.z )
ball.body.w = vec3d:new( -20, 0, 0 )
ball.body.monitor = 1
ball.body.teleport = true
--ball.body.control = control.builtin.still --we will freeze in the space

-- Appending the ball shape to the list of COUPi objects! From now, our ball is
-- in the system and will be recognized by COUPi.
hdis.append( ball )



local ball = shape.threespheres( 0.5*ballparams.R, 'ball3', 'plastic' )


-- center of mass location: we have to place our ball in the box. vec3d is a
-- vector with 3 components. We don't distinguish between space points and
-- vectors.
ball.body.c = vec3d:new( 0.5 * hdis.box.x,
                         0.18 * hdis.box.y,
                         1.7 * hdis.box.z )
ball.body.teleport = true
--ball.body.control = control.builtin.still --we will freeze in the space

-- Appending the ball shape to the list of COUPi objects! From now, our ball is
-- in the system and will be recognized by COUPi.
hdis.append( ball )

---------------------------------------------------------------------------------

-- special global table to contain variables to change in functions
myhdis = {}
myhdis.t = 0
myhdis.dt = 0
myhdis.n = 0

-- Function to set time, timestep and step number to use in other
-- functions
function pretimestep( t, dt, n )
   myhdis.t = t
   myhdis.dt = dt
   myhdis.n = n
end

hdis.callback.pretimestep = pretimestep

-- recording a 3-sphere position and velocities
local fp = assert( io.open( 'tripos.txt', 'w' ) )
local str = string.format(
   "# [1] n [2] t [3] cx [4] cy [5] cz [6] vx [7] vy [8] vz [9] wx [10] wy [11] wz\n" )
fp:write( str )
fp:flush()

-- Monitoring all the values related to i-th body.
local function monitor( i, m, V,
                        Ix, Iy, Iz,
                        cx, cy, cz,
                        vx, vy, vz,
                        wx, wy, wz,
                        fx, fy, fz,
                        tx, ty, tz )
   if myhdis.n % 10 == 0 then
      local str = string.format(
         "%d %f %f %f %f %f %f %f %f %f %f\n",
         myhdis.n, myhdis.t, cx, cy, cz, vx, vy, vz, wx, wy, wz )
      fp:write(str)
      fp:flush()
   end
end

-- list of monitor functions
hdis.monitor_functions = {
   monitor,
}

-- Finalizing function

local function finalize( )
   fp:close()
   print ( "FILE CLOSED; t = ", myhdis.t )
   local name = samplename .. '-final.h5'
end

hdis.callback.finalize = finalize

---------------------------------------------------------------------------------

hdis.portal = {}
hdis.portal.entrance_y = hdis.box.y - 2*ballparams.R
hdis.portal.exit_y = 4 * ballparams.R
hdis.portal.exit_z = 1.5 * hdis.box.z - 2*ballparams.R
hdis.portal.v = { 0, 0, 0 }

-------------------------------------------------------------------------------
-- Display options
-------------------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave( samplename .. '-' )
hdis.callback.aftersave = hdis.callback.std.aftersave_long()

