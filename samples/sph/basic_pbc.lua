--[[---------------------------------------------------------------------------
   basic_pbc.lua

   Example: 

   Filling 729 hydro spheres into a box of 1525 hydro spheres, run less than 1
   minute, periodic boundary

   (c)2018 UAF
--]]---------------------------------------------------------------------------

require 'hdis' -- this Coupi module is always required!

local shape = require 'hdis.shape' -- this module contains primitive shapes
local hydro = require 'hdis.hydro' -- hydro particles library
local vec3d = require 'hdis.vec3d' -- 3D vector type for vectors and points
local grid  = require 'hdis.grid'  -- grid to place the particles

local aux   = require 'hdis.aux'
require 'hdis.control'

-------------------------------------------------------------------------------
-- Main definitions and gravity
-------------------------------------------------------------------------------
-- Time/saving parameters
hdis.time = { start = 0, stop = 4, dt = 0.0001, save = 0.0001 }

-- The area of simulation
hdis.box = { x = 1, y = 1, z = 1 }

-- m/s^2, gravity
hdis.g = -9.8

hdis.contact_detection = {}
hdis.contact_detection.method = 'spsp'

-------------------------------------------------------------------------------
-- Groups and Materials
-------------------------------------------------------------------------------
-- Materials (rho - density in kg/m^3, G - stiffness in GPa, nu - Poisson
-- coefficient, mu - Coloumb friction (static/dynamic), CR - restitution
-- coefficient.

local steel = {
   name = 'steel',
   rho = 7850.0,
   G = 80,
   nu = 0.27,
   mu = 0.3,
   CR =0.8
}

hdis.material:add( steel )

hdis.groups:add( 'water' ):add( 'ball' ):add( 'floor' )

-------------------------------------------------------------------------------
-- Boundary is made of the same material... for now. It can be done by just
-- simple assignment as follows. There are other ways to do this too, but this
-- one seems to be the simplest approach.
-------------------------------------------------------------------------------
hdis.boundary.material = 'steel'

---------------------------------------------------------------------------------
-- water properties
---------------------------------------------------------------------------------

local water = {}
water.h     = 0.05              -- smoothing length
water.rho   = 1000
water.nu    = 0

local floor = {}                -- the floor is also hydro
floor.h   = water.h
floor.rho = water.rho
floor.nu  = water.nu
floor.km   = 1.4

local banks = {}
banks.H = hdis.box.z

-------------------------------------------------------------------------------
-- Free Hydro particles addition
-------------------------------------------------------------------------------

local cutscale_xy = 0.92
local cutscale_z  = 0.85
local ds = 1.6 * water.h

local shift = floor.h + water.h
local seeds = grid.grid(
   { shift, shift, shift },
   { cutscale_xy * hdis.box.x - shift,
     0.7 * cutscale_xy * hdis.box.y - shift,
     cutscale_z * banks.H },
   { ds, ds, ds } )
local vscale = 0.1

math.randomseed( os.time() )

for i, C in pairs( seeds ) do
   local R = water.h
   local rndx = 2 * vscale* ( math.random() - 0.5 )
   local rndy = 2 * vscale* ( math.random() - 0.5 )
   local rndz = 2 * vscale* ( math.random() - 0.5 )
   
   local c = vec3d:new( C[1], C[2], C[3] )
   local v = vec3d:new( rndx, rndy, rndz )
   local sphdot = hydro.new( water.h, water.rho, water.nu, v, c, 'water' ) 
   hdis.append(  sphdot  )
end

print( "Number of free hydro particles:", #hdis.dots )

---------------------------------------------------------------------------------
-- river floor and ceiling
---------------------------------------------------------------------------------

local nfloors = 0

local cshift = 0.5 * floor.h
local cdr    = floor.h

local wall                      -- storage for the wall


wall = {}
local wall = hydro.wall(
   { cshift, cshift, cshift },
   { hdis.box.x - cshift, hdis.box.y - cshift, cshift },
   { cdr, cdr, cdr },
   floor.h, floor.rho, floor.nu, 'floor' )

for i, hp in pairs( wall ) do
   hdis.append( hp )
   nfloors = nfloors + 1
end

cdr = 1.4 * floor.h                 -- we will use less dense ceiling

wall = {}
wall = hydro.wall(
   { cshift, cshift, hdis.box.z - cshift },
   { hdis.box.x - cshift, hdis.box.y - cshift, hdis.box.z - cshift },
   { cdr, cdr, cdr },
   floor.h, floor.rho, floor.nu, 'floor' )

for i, hp in pairs( wall ) do
   hdis.append( hp )
   nfloors = nfloors + 1
end
   

---------------------------------------------------------------------------------
-- river banks
---------------------------------------------------------------------------------

-- cdr is also relaxed

wall = {}
wall = hydro.wall(
   { cshift, cshift, cshift },
   { hdis.box.x - cshift, cshift, hdis.box.z - cshift },
   { cdr, cdr, cdr },
   floor.h, floor.rho, floor.nu, 'floor' )

for i, hp in pairs( wall ) do
--   hdis.append( hp )
   nfloors = nfloors + 1
end


wall = {}
wall = hydro.wall(
   { cshift, hdis.box.y - cshift, cshift },
   { hdis.box.x - cshift, hdis.box.y - cshift, hdis.box.z - cshift },
   { cdr, cdr, cdr },
   floor.h, floor.rho, floor.nu, 'floor' )

for i, hp in pairs( wall ) do
--   hdis.append( hp )
   nfloors = nfloors + 1
end

wall = {}
wall = hydro.wall(
   { cshift, cshift, cshift },
   { cshift, hdis.box.y - cshift, hdis.box.z - cshift },
   { cdr, cdr, cdr },
   floor.h, floor.rho, floor.nu, 'floor' )

for i, hp in pairs( wall ) do
   hdis.append( hp )
   nfloors = nfloors + 1
end


wall = {}
wall = hydro.wall(
   { hdis.box.x - cshift, cshift, cshift },
   { hdis.box.x - cshift, hdis.box.y - cshift, hdis.box.z - cshift },
   { cdr, cdr, cdr },
   floor.h, floor.rho, floor.nu, 'floor' )

for i, hp in pairs( wall ) do
   hdis.append( hp )
   nfloors = nfloors + 1
end



print( "SPH particles in the floor and the banks:", nfloors )


-------------------------------------------------------------------------------
-- Object addition: for hdisd to work correctly
-------------------------------------------------------------------------------

local ball = hdis.shape.sphere( 0.001, 'ball', 'steel' )
ball.body.c = vec3d:new( 0, 0, 0 )
ball.body.control = hdis.control.builtin.still --we will freeze in the space
hdis.append( ball )

---------------------------------------------------------------------------------
hdis.boundary = {
   opentop = true,
   periodic = true
}

-------------------------------------------------------------------------------
-- Display options
-------------------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave_mcs( 'basic-' )
hdis.callback.aftersave = hdis.callback.std.aftersave_short()
