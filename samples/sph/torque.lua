--[[---------------------------------------------------------------------------
   torque.lua

   Example: 
   
   DEM twosphere 8 is colliding with an SPH particle to check the torques.

   For visulization use cpd/cpd-config-balldrop.lua

   (c)2018 UAF

--]]---------------------------------------------------------------------------

require 'hdis' -- this Coupi module is always required!

local shape = require 'hdis.shape' -- this module contains primitive shapes
local hydro = require 'hdis.hydro' -- hydro particles library
local vec3d = require 'hdis.vec3d' -- 3D vector type for vectors and points
local quat  = require 'hdis.quat'  -- rotation unit quaternions
local aux   = require 'hdis.aux'
require 'hdis.control'

-------------------------------------------------------------------------------
-- Main definitions and gravity
-------------------------------------------------------------------------------
-- Time/saving parameters
hdis.time = { start = 0, stop = 1.0, dt = 0.000001, save = 0.005 }

-- The area of simulation
hdis.box = { x = 1, y = 1, z = 1 }

-- m/s^2, gravity
hdis.g = 0

hdis.contact_detection = {}
hdis.contact_detection.method = 'naive'

-------------------------------------------------------------------------------
-- Groups and Materials
-------------------------------------------------------------------------------
-- Materials (rho - density in kg/m^3, G - stiffness in GPa, nu - Poisson
-- coefficient, mu - Coloumb friction (static/dynamic), CR - restitution
-- coefficient.

local steel = {
   name = 'steel',
   --   rho = 7850.0,
      rho = 500.0,
   G = 1,
   nu = 0.27,
   mu = 0.3,
   CR =0.8
}

local ballsteel = {
   name = 'ballsteel',
   --   rho = 7850.0,
      rho = 5000.0,
   G = 1,
   nu = 0.27,
   mu = 0.3,
   CR =0.8
}


hdis.material:add( steel )
hdis.material:add( ballsteel )

hdis.groups:add( 'water' ):add( 'object' )

-------------------------------------------------------------------------------
-- Boundary is made of the same material... for now. It can be done by just
-- simple assignment as follows. There are other ways to do this too, but this
-- one seems to be the simplest approach.
-------------------------------------------------------------------------------
hdis.boundary.material = 'steel'

---------------------------------------------------------------------------------
-- water properties
---------------------------------------------------------------------------------

local water = {}
water.R     = 0.015
water.rho   = 1000
water.nu    = 0

local objprop = {}
objprop.R   = 0.05

---------------------------------------------------------------------------------
-- Object addition
-------------------------------------------------------------------------------
-- We create a twosphere of radius 0.05 m, that belongs to group 'object' and
-- made out of our material 'steel'. This function just forms a 'shape': a body,
-- atoms it is built from, and points that are atoms 'skeleton'

local object = hdis.shape.twospheres( objprop.R, 'object', 'steel' )
--local object = hdis.shape.sphere( objprop.R, 'object', 'steel' )

object.body.c = vec3d:new(
   0.5 * hdis.box.x,
   0.5 * hdis.box.y,
   0.5 * hdis.box.z )
object.body.q = quat:new( {0,1,0}, math.pi/2 )

--ball.body.control = hdis.control.builtin.still --we will freeze in the space
object.body.monitor = 1

-- Appending the ball shape to the list of COUPi objects! From now, our ball is
-- in the system and will be recognized by COUPi.
hdis.append( object )

-------------------------------------------------------------------------------
-- Smooth particles
-------------------------------------------------------------------------------
local R = water.R
local c = vec3d:new( 0.5 * hdis.box.x,
                     0.25* hdis.box.y,
                     0.5*hdis.box.z + objprop.R )
local v = vec3d:new( 0, 1, 0 )
local sphdot = hydro.new( water.R, water.rho, water.nu, v, c, 'water' )
--local sphdot = shape.sphere( water.R, 'water', 'ballsteel' )
--sphdot.body.c = c
--sphdot.body.v = v

hdis.append(  sphdot  )

---------------------------------------------------------------------------------
hdis.boundary = { opentop = true, openbottom = false }

---------------------------------------------------------------------------------
-- monitor
---------------------------------------------------------------------------------

local objdata = {}             -- to store cylinder parameters

local function monitor( i, m, V,
                        Ix, Iy, Iz,
                        cx, cy, cz,
                        vx, vy, vz,
                        wx, wy, wz,
                        fx, fy, fz,
                        tx, ty, tz )

   -- just update tree_data to use in aftertimestep
   objdata.c = { cx, cy, cz }
   objdata.w = { wx, wy, wz }
   objdata.f = { fx, fy, fz }
   objdata.t = { tx, ty, tz }
end

hdis.monitor_functions = {
   monitor,
}

---------------------------------------------------------------------------------
--                      After time step: save
---------------------------------------------------------------------------------

-- We save position and forces acting on the lander leg
local ff = assert( io.open( 'torque.txt', 'w' ) )

local s1 = string.format( '# Object data\n#\n' )
local s2 = string.format( '# Angular velocity, forces and torques:\n' )
local s3 =  string.format( '# [1]t ' .. 
                              '[2] wx [3] wy [4] wz,rad/s ' ..
                              '[5] fx,N [6] fy,N [7] fz,N ' ..
                              '[8] tx,Nm [9] ty,Nm [10] tz,Nm ' )

ff:write( s1 )
ff:write( s2 )
ff:write( s3 )

local function aftertimestep( t, dt, n )
   -- Saving to the file each 100th step
   if n % 100 == 0 then
      local output =
         string.format(
            '% 2.10f  %f %f %f  %f %f %f  %f %f %f\n',
            t,
            objdata.w[1], objdata.w[2], objdata.w[3],
            objdata.f[1], objdata.f[2], objdata.f[3],
            objdata.t[1], objdata.t[2], objdata.t[3] )
      ff:write( output )
      ff:flush()
   end

end

hdis.callback.aftertimestep = aftertimestep


-------------------------------------------------------------------------------
-- Display options
-------------------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave( 'torque-' )
hdis.callback.aftersave = hdis.callback.std.aftersave_long()
