--[[---------------------------------------------------------------------------
   contin.lua

   Example: Minimalistic runfrom file

   (c)2018 UAF

   Usage: hdis contin.lua
--]]---------------------------------------------------------------------------

require 'hdis' -- this Coupi module is always required!

local vec3d = require 'hdis.vec3d' -- 3D vector type for vectors and points
local aux   = require 'hdis.aux'

local hfile = 'balldrop-00000000ms.h5' --'bsphere-00000910ms.h5'
hdis.runfrom( hfile )

-------------------------------------------------------------------------------
-- Main definitions and gravity
-------------------------------------------------------------------------------
-- Time/saving parameters
hdis.time = { start = 0, stop = 5, dt = 0.00001, save = 0.01 }

-------------------------------------------------------------------------------
-- Display options
-------------------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave( 'bsphereX-' )
hdis.callback.aftersave = hdis.callback.std.aftersave_long()
