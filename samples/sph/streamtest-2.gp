#
#  gnuplot script to plot data from streamtest-2, cylparams forces and torques
#
#  usage: gnuplot streamtest-2.gp -
#

fname = 'cylinder.txt'

plot fname using 2:3 with lines lw 2 title 'Fx', \
     fname using 2:4 with lines lw 2 title 'Fy', \
     fname using 2:5 with lines lw 3 title 'Fz'

plot fname using 2:6 with lines lw 2 title 'Tx', \
     fname using 2:7 with lines lw 2 title 'Ty', \
     fname using 2:8 with lines lw 3 title 'Tz'

