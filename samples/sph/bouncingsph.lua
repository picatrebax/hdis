--[[---------------------------------------------------------------------------
   bouncingsph.lua

   Example:

   SPH particle bounces from the DEM fixed floor.

   (c)2018 UAF
--]]---------------------------------------------------------------------------

require 'hdis' -- this Coupi module is always required!

local shape = require 'hdis.shape' -- this module contains primitive shapes
local hydro = require 'hdis.hydro' -- hydro particles library
local vec3d = require 'hdis.vec3d' -- 3D vector type for vectors and points
local aux   = require 'hdis.aux'
local sphtool = require 'hdis.sphtool'
local control = require 'hdis.control'

-------------------------------------------------------------------------------
-- Main definitions and gravity
-------------------------------------------------------------------------------
-- Time/saving parameters
hdis.time = { start = 0, stop = 4, dt = 0.000001, save = 0.01 }

-- The area of simulation
hdis.box = { x = 1, y = 1, z = 1 }

-- m/s^2, gravity
hdis.g = -9.8

hdis.contact_detection = {}
hdis.contact_detection.method = 'spsp'

-------------------------------------------------------------------------------
-- Groups and Materials
-------------------------------------------------------------------------------
-- Materials (rho - density in kg/m^3, G - stiffness in GPa, nu - Poisson
-- coefficient, mu - Coloumb friction (static/dynamic), CR - restitution
-- coefficient.

local steel = {
   name = 'steel',
   rho = 7850.0,
   G = 80,
   nu = 0.27,
   mu = 0.3,
   CR =0.8
}

hdis.material:add( steel )

hdis.groups:add( 'water' ):add( 'ball' ):add( 'floor' )

-------------------------------------------------------------------------------
-- Boundary is made of the same material... for now. It can be done by just
-- simple assignment as follows. There are other ways to do this too, but this
-- one seems to be the simplest approach.
-------------------------------------------------------------------------------
hdis.boundary.material = 'steel'

---------------------------------------------------------------------------------
-- water properties
---------------------------------------------------------------------------------

local water = {}
water.h     = 0.05              -- smoothing length
water.rho   = 1000
water.nu    = 0

local floor = {}
floor.R = water.h / 2
floor.dR = floor.R
floor.X = hdis.box.x - 2*floor.R
floor.Y = hdis.box.y - 2*floor.R

-------------------------------------------------------------------------------
-- Smooth particles
-------------------------------------------------------------------------------
local vscale = 0.25
local n = 5

math.randomseed( os.time() )

for i = 1, n do
  for j = 1, n do
     for k = 1, n do
        local R = water.h
        local rndx = 2 * vscale* ( math.random() - 0.5 )
        local rndy = 2 * vscale* ( math.random() - 0.5 )
        local rndz = 2 * vscale* ( math.random() - 0.5 )

        local c = vec3d:new( 0.9*2*i*R, 0.9*2*j*R, 0.9*2*k*R )
        local v = vec3d:new( rndx, rndy, rndz )
        local sphdot = hydro.new( water.h, water.rho, water.nu, v, c, 'water' ) 
--        hdis.append(  sphdot  )
    end
  end
end

local c = vec3d:new( 0.5*hdis.box.x, 0.5*hdis.box.y, 0.5*hdis.box.z )
local v = vec3d:new()
local sphdot = hydro.new( water.h, water.rho, water.nu, v, c, 'water' )
hdis.append( sphdot )


---------------------------------------------------------------------------------
-- river floor
---------------------------------------------------------------------------------

local banks = {}
banks.H = hdis.box.z / 2

local floorobj = sphtool.rectangle( floor.X, floor.Y, floor.R, floor.dR,
                                 'floor', 'steel' )

floorobj.body.c = vec3d:new( hdis.box.x/2, hdis.box.y/2, floor.R )
floorobj.body.control = control.builtin.still
hdis.append( floorobj )

---------------------------------------------------------------------------------
-- river banks
---------------------------------------------------------------------------------

-- Object addition
-------------------------------------------------------------------------------
-- We create a ball of radius 0.05 m, that belongs to group 'ball' and made out
-- of our material 'steel'. This function just forms a 'shape': a body, atoms
-- it is built from, and points that are atoms 'skeleton'

local ball = hdis.shape.sphere( 0.05, 'ball', 'steel' )

-- center of mass location: we have to place our ball in the box. vec3d is a
-- vector with 3 components. We don't distinguish between space points and
-- vectors.
ball.body.c = vec3d:new( 0.5 * hdis.box.x, 0.5 * hdis.box.y,
   0.5 * hdis.box.z )
--ball.body.control = control.builtin.still --we will freeze in the space

-- Appending the ball shape to the list of COUPi objects! From now, our ball is
-- in the system and will be recognized by COUPi.
--hdis.append( ball )


---------------------------------------------------------------------------------
hdis.boundary = { opentop = true }

-------------------------------------------------------------------------------
-- Display options
-------------------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave( 'bouncingsph-' )
hdis.callback.aftersave = hdis.callback.std.aftersave_short()
