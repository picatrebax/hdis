--[[---------------------------------------------------------------------------
   archimed.lua

   Example: Placing objects on the surface of water from fillbox.lua example

   (c)2018 UAF

   Usage: hdis contin.lua
--]]---------------------------------------------------------------------------

require 'hdis' -- this Coupi module is always required!

local vec3d = require 'hdis.vec3d' -- 3D vector type for vectors and points
local aux   = require 'hdis.aux'
local shape = require 'hdis.shape'

local hfile = 'periodic-glass-final.h5'
hdis.runfrom( hfile )

-------------------------------------------------------------------------------
-- Main definitions and gravity
-------------------------------------------------------------------------------
-- Time/saving parameters
hdis.time = { start = 0, stop = 10, dt = 0.00025, save = 0.01 }

-------------------------------------------------------------------------------
-- Display options
-------------------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave( 'archimed-' )
hdis.callback.aftersave = hdis.callback.std.aftersave_long()


-------------------------------------------------------------------------------
-- Groups and Materials
-------------------------------------------------------------------------------
-- Materials (rho - density in kg/m^3, G - stiffness in GPa, nu - Poisson
-- coefficient, mu - Coloumb friction (static/dynamic), CR - restitution
-- coefficient.

local wood = {
   name = 'wood',
   rho = 250.0, --7850.0,
   G = 0.1,
   nu = 0.27,
   mu = 0.3,
   CR = 0.8
}

local hardwood = {
   name = 'hardwood',
   rho = 500.0, --7850.0,
   G = 0.1,
   nu = 0.27,
   mu = 0.3,
   CR = 0.8
}

local plastic = {
   name = 'plastic',
   rho = 600.0, --7850.0,
   G = 0.1,
   nu = 0.27,
   mu = 0.3,
   CR = 0.8
}

hdis.material:add( wood ):add( hardwood ):add( plastic )

hdis.groups:add( 'water' ):add( 'ball' ):add( 'floor' )
hdis.groups:add( 'ball2' )
hdis.groups:add( 'ball3' )

hdis.groups:avert( 'floor', 'ball' )
hdis.groups:avert( 'floor', 'ball2' )
hdis.groups:avert( 'floor', 'ball3' )

---------------------------------------------------------------------------------
-- Object addition
-------------------------------------------------------------------------------
-- We create a ball of radius 0.05 m, that belongs to group 'ball' and made out
-- of our material 'wood'. This function just forms a 'shape': a body, atoms
-- it is built from, and points that are atoms 'skeleton'

--local ball = hdis.shape.sphere( 0.05, 'ball', 'wood' )

local ballparams = {}
ballparams.R = 0.2

local ball = shape.sphere( ballparams.R, 'ball', 'wood' )


-- center of mass location: we have to place our ball in the box. vec3d is a
-- vector with 3 components. We don't distinguish between space points and
-- vectors.
ball.body.c = vec3d:new( 0.5 * hdis.box.x,
                         0.5 * hdis.box.y,
                         0.9 * hdis.box.z )
--ball.body.control = control.builtin.still --we will freeze in the space

-- Appending the ball shape to the list of COUPi objects! From now, our ball is
-- in the system and will be recognized by COUPi.
hdis.append( ball )


local ball = shape.threespheres( 0.5*ballparams.R, 'ball2', 'hardwood' )


-- center of mass location: we have to place our ball in the box. vec3d is a
-- vector with 3 components. We don't distinguish between space points and
-- vectors.
ball.body.c = vec3d:new( 0.5 * hdis.box.x,
                         0.25 * hdis.box.y,
                         0.95 * hdis.box.z )
ball.body.w = vec3d:new( 0, 0, 0 )
--ball.body.control = control.builtin.still --we will freeze in the space

-- Appending the ball shape to the list of COUPi objects! From now, our ball is
-- in the system and will be recognized by COUPi.
hdis.append( ball )



local ball = shape.threespheres( 0.5*ballparams.R, 'ball3', 'plastic' )


-- center of mass location: we have to place our ball in the box. vec3d is a
-- vector with 3 components. We don't distinguish between space points and
-- vectors.
ball.body.c = vec3d:new( 0.5 * hdis.box.x,
                         0.55 * hdis.box.y,
                         1.3 * hdis.box.z )
--ball.body.control = control.builtin.still --we will freeze in the space

-- Appending the ball shape to the list of COUPi objects! From now, our ball is
-- in the system and will be recognized by COUPi.
hdis.append( ball )

