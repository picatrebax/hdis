--[[---------------------------------------------------------------------------
   collision.lua

   Example: 
   
   Two spheres colliding: a large solid sphere and a small sph sphere.

   (c)2018 UAF

--]]---------------------------------------------------------------------------

require 'hdis' -- this Hdis module is always required!

local shape = require 'hdis.shape' -- this module contains primitive shapes
local hydro = require 'hdis.hydro' -- hydro particles library
local vec3d = require 'hdis.vec3d' -- 3D vector type for vectors and points
local aux   = require 'hdis.aux'
require 'hdis.control'

-------------------------------------------------------------------------------
-- Main definitions and gravity
-------------------------------------------------------------------------------
-- Time/saving parameters
hdis.time = { start = 0, stop = 0.5, dt = 0.0000001, save = 0.005 }

-- The area of simulation
hdis.box = { x = 1, y = 1, z = 1 }

-- m/s^2, gravity
hdis.g = 0

hdis.contact_detection = {}
hdis.contact_detection.method = 'spsp'

-------------------------------------------------------------------------------
-- Groups and Materials
-------------------------------------------------------------------------------
-- Materials (rho - density in kg/m^3, G - stiffness in GPa, nu - Poisson
-- coefficient, mu - Coloumb friction (static/dynamic), CR - restitution
-- coefficient.

local steel = {
   name = 'steel',
   rho = 7850.0,
   G = 80,
   nu = 0.27,
   mu = 0.3,
   CR =0.8
}

hdis.material:add( steel )

hdis.groups:add( 'water' ):add( 'ball' ):add( 'floor' )

-------------------------------------------------------------------------------
-- Boundary is made of the same material... for now. It can be done by just
-- simple assignment as follows. There are other ways to do this too, but this
-- one seems to be the simplest approach.
-------------------------------------------------------------------------------
hdis.boundary.material = 'steel'

---------------------------------------------------------------------------------
-- water properties
---------------------------------------------------------------------------------

local water = {}
water.R     = 0.015
water.rho   = 1000
water.nu    = 0

local floor = {}
floor.R   = water.R
floor.rho = water.rho
floor.nu  = water.nu

---------------------------------------------------------------------------------
-- Object addition
-------------------------------------------------------------------------------
-- We create a ball of radius 0.05 m, that belongs to group 'ball' and made out
-- of our material 'steel'. This function just forms a 'shape': a body, atoms
-- it is built from, and points that are atoms 'skeleton'

local ball = hdis.shape.sphere( 0.05, 'ball', 'steel' )

-- center of mass location: we have to place our ball in the box. vec3d is a
-- vector with 3 components. We don't distinguish between space points and
-- vectors.
ball.body.c = vec3d:new( 0.5 * hdis.box.x, 0.5 * hdis.box.y,
   0.5 * hdis.box.z )
--ball.body.control = hdis.control.builtin.still --we will freeze in the space

-- Appending the ball shape to the list of Hdis objects! From now, our ball is
-- in the system and will be recognized by Hdis.
hdis.append( ball )

-------------------------------------------------------------------------------
-- Smooth particles
-------------------------------------------------------------------------------
local R = water.R
local c = vec3d:new( 0.25*hdis.box.x, 0.25* hdis.box.y, 0.5*hdis.box.z )
local v = vec3d:new( 1, 1, 0 )
local sphdot = hydro.new( water.R, water.rho, water.nu, v, c, 'water' ) 
hdis.append(  sphdot  )

---------------------------------------------------------------------------------
hdis.boundary = { opentop = true, openbottom = false }

-------------------------------------------------------------------------------
-- Display options
-------------------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave( 'collision-' )
hdis.callback.aftersave = hdis.callback.std.aftersave_long()
