--[[---------------------------------------------------------------------------
   streamtest-1.lua

   Example:

   filling around 15K hydro particles in wooden DEM box made from spheres. Sides
   are open. Periodic boundary conditions. Use for wider stream than other
   samples to see a flow around a circular cylinder in the next example

   (c)2018 UAF

--]]---------------------------------------------------------------------------

require 'hdis' -- this Coupi module is always required!

local shape = require 'hdis.shape' -- this module contains primitive shapes
local hydro = require 'hdis.hydro' -- hydro particles library
local vec3d = require 'hdis.vec3d' -- 3D vector type for vectors and points
local control = require 'hdis.control' -- control for dem objects
local grid = require 'hdis.grid' -- grid generator
local sphtool = require 'hdis.sphtool'

local aux   = require 'hdis.aux'

local samplename = 'streamtest-1'

-------------------------------------------------------------------------------
-- Main definitions and gravity
-------------------------------------------------------------------------------
-- Time/saving parameters
hdis.time = { start = 0, stop = 2, dt = 0.0002, save = 0.01 }

-- The area of simulation
hdis.box = { x = 1.5, y = 2.5, z = 1 }

-- m/s^2, gravity
hdis.g = { 0, 1.0, -9.8 }

hdis.contact_detection = {}
hdis.contact_detection.method = 'spsp'

-------------------------------------------------------------------------------
-- Groups and Materials
-------------------------------------------------------------------------------
-- Materials (rho - density in kg/m^3, G - stiffness in GPa, nu - Poisson
-- coefficient, mu - Coloumb friction (static/dynamic), CR - restitution
-- coefficient.

local wood = {
   name = 'wood',
   rho = 250.0, --7850.0,
   G = 0.1,
   nu = 0.27,
   mu = 0.3,
   CR =0.8
}

hdis.material:add( wood )

hdis.groups:add( 'water' ):add( 'floor' ):add( 'banky' ):add( 'ceiling' )

hdis.groups:avert( 'banky', 'floor' )
hdis.groups:avert( 'banky', 'ceiling' )

-------------------------------------------------------------------------------
-- Boundary is made of the same material... for now. It can be done by just
-- simple assignment as follows. There are other ways to do this too, but this
-- one seems to be the simplest approach.
-------------------------------------------------------------------------------
hdis.boundary.material = 'wood'
hdis.boundary.periodic = true
hdis.boundary.opentop  = true
aux.print_serial( hdis.boundary )

---------------------------------------------------------------------------------
-- water properties
---------------------------------------------------------------------------------

local water = {}
water.h     = 0.04
water.rho   = 1000
water.nu    = 0
water.D     = 2 * water.h

-------------------------------------------------------------------------------
-- boundary properties
-------------------------------------------------------------------------------

local floordata = {}
floordata.Nx = 20
floordata.R = hdis.box.x / floordata.Nx
floordata.dr = floordata.R
floordata.a = hdis.box.x - 2*floordata.R
floordata.b = hdis.box.y - 2*floordata.R
floordata.c = 2.0 * hdis.box.z - 2*floordata.R
floordata.center = {
   0.5 * ( floordata.a ) + floordata.R,
   0.5 * ( floordata.b ) + floordata.R,
   0.5 * ( floordata.c ) + floordata.R, }

-------------------------------------------------------------------------------
-- Free Hydro particles addition
-------------------------------------------------------------------------------

local cutscale_xy = 0.92
local cutscale_z  = 1.5
local ds = 1.3 * water.h

local shift = 2 * floordata.R + 2*water.h
local seeds = grid.grid(
   { shift, shift, shift },
   { cutscale_xy * hdis.box.x - shift,
     cutscale_xy * hdis.box.y - shift,
     cutscale_z * hdis.box.z },
   { ds, ds, ds } )
local vscale = 0.1

math.randomseed( os.time() )

for i, C in pairs( seeds ) do
   local R = water.h
   local rndx = 2 * vscale* ( math.random() - 0.5 )
   local rndy = 2 * vscale* ( math.random() - 0.5 )
   local rndz = 2 * vscale* ( math.random() - 0.5 )
   
   local c = vec3d:new( C[1], C[2], C[3] )
   local v = vec3d:new( rndx, rndy, rndz )
   local sphdot = hydro.new( water.h, water.rho, water.nu, v, c, 'water' ) 
   hdis.append(  sphdot  )
end

---------------------------------------------------------------------------------
-- river floor and ceiling
---------------------------------------------------------------------------------

local sphere_counter = 0

local floor

floor = sphtool.rectangle(
   floordata.a,
   floordata.b,
   floordata.R,
   floordata.dr,
   'floor', 'wood' )
floor.body.c = vec3d:new( hdis.box.x / 2,
                          hdis.box.y / 2,
                          floordata.R )
floor.body.control = control.builtin.still

sphere_counter = sphere_counter + #floor.atoms
hdis.append( floor )

local ceiling = sphtool.rectangle(
   floordata.a,
   floordata.b,
   floordata.R,
   floordata.dr,
   'ceiling', 'wood' )
ceiling.body.c = vec3d:new( hdis.box.x / 2,
                            hdis.box.y / 2,
                            floordata.c + 2 * floordata.R )
ceiling.body.control = control.builtin.still

sphere_counter = sphere_counter + #floor.atoms
hdis.append( ceiling )


---------------------------------------------------------------------------------

bank = sphtool.rectangle(
   floordata.c,
   floordata.b,
   floordata.R,
   floordata.dr,
   'banky', 'wood' )
bank.body.c = vec3d:new( floordata.R,
                         floordata.center[2],
                         floordata.center[3] )
bank.body.q = quat:new( math.pi/2, { 0, 1, 0 } )
bank.body.control = control.builtin.still

sphere_counter = sphere_counter + #bank.atoms
hdis.append( bank )

---------------------------------------------------------------------------------

bank = sphtool.rectangle(
   floordata.c,
   floordata.b,
   floordata.R,
   floordata.dr,
   'banky', 'wood' )
bank.body.c = vec3d:new( hdis.box.x - floordata.R,
                         floordata.center[2],
                         floordata.center[3] )
bank.body.q = quat:new( math.pi/2, { 0, 1, 0 } )
bank.body.control = control.builtin.still

sphere_counter = sphere_counter + #bank.atoms
hdis.append( bank )

---------------------------------------------------------------------------------
-- river banks
---------------------------------------------------------------------------------

print( "SPH particles in water mass:", #hdis.dots )
print( "Boundary grains in river floor and banks", sphere_counter )
-- print( "SPH particles in the floor and the banks:", nfloors )

-------------------------------------------------------------------------------
-- Display options
-------------------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave( samplename .. '-' )
hdis.callback.aftersave = hdis.callback.std.aftersave_long()
hdis.callback.finalize = hdis.callback.std.finalize( samplename .. '-final' )
