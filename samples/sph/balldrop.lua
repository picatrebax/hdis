--[[---------------------------------------------------------------------------
   balldrop.lua

   Example:

   filling 7425 hydro particles in a box of 3842 particles and dropping a large
   sphere in the liquid.

   (c)2018 UAF

--]]---------------------------------------------------------------------------

require 'hdis' -- this Coupi module is always required!

local shape = require 'hdis.shape' -- this module contains primitive shapes
local hydro = require 'hdis.hydro' -- hydro particles library
local vec3d = require 'hdis.vec3d' -- 3D vector type for vectors and points
local control = require 'hdis.control' -- control for dem objects
local grid = require 'hdis.grid' -- grid generator

local aux   = require 'hdis.aux'

-------------------------------------------------------------------------------
-- Main definitions and gravity
-------------------------------------------------------------------------------
-- Time/saving parameters
hdis.time = { start = 0, stop = 10, dt = 0.00025, save = 0.01 }
--  hdis.time = { start = 0, stop = 10, dt = 0.0001, save = 0.01 }

-- The area of simulation
hdis.box = { x = 2, y = 2, z = 1 }

-- m/s^2, gravity
hdis.g = -9.8

hdis.contact_detection = {}
hdis.contact_detection.method = 'spsp'

-------------------------------------------------------------------------------
-- Groups and Materials
-------------------------------------------------------------------------------
-- Materials (rho - density in kg/m^3, G - stiffness in GPa, nu - Poisson
-- coefficient, mu - Coloumb friction (static/dynamic), CR - restitution
-- coefficient.

local wood = {
   name = 'wood',
   rho = 40.0,
   G = 0.1,
   nu = 0.27,
   mu = 0.3,
   CR =0.8
}

hdis.material:add( wood )

hdis.groups:add( 'water' ):add( 'ball' ):add( 'floor' )
hdis.groups:avert( 'floor', 'ball' )

hdis.hydro_props = {
   c0 = 5,                    -- basic sonic speed
   gamma = 7,                   -- adiabatic index
   alpha = 0.8,                 -- viscosity
}
-------------------------------------------------------------------------------
-- Boundary is made of the same material... for now. It can be done by just
-- simple assignment as follows. There are other ways to do this too, but this
-- one seems to be the simplest approach.
-------------------------------------------------------------------------------
hdis.boundary.material = 'wood'

---------------------------------------------------------------------------------
-- water properties
---------------------------------------------------------------------------------

local water = {}
water.h     = 0.04
water.rho   = 1000
water.nu    = 0
water.D     = 2 * water.h

local floor = {}
floor.rho = water.rho
floor.nu  = water.nu
floor.h = water.h

-------------------------------------------------------------------------------
-- Smooth particles
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Free Hydro particles addition
-------------------------------------------------------------------------------

local cutscale_xy = 0.92
local cutscale_z  = 0.9
local ds = 1.3 * water.h

local shift = floor.h + water.h
local seeds = grid.grid(
   { shift, shift, shift },
   { cutscale_xy * hdis.box.x - shift,
     cutscale_xy * hdis.box.y - shift,
     cutscale_z * hdis.box.z },
   { ds, ds, ds } )
local vscale = 0.1

math.randomseed( os.time() )

for i, C in pairs( seeds ) do
   local R = water.h
   local rndx = 2 * vscale* ( math.random() - 0.5 )
   local rndy = 2 * vscale* ( math.random() - 0.5 )
   local rndz = 2 * vscale* ( math.random() - 0.5 )
   
   local c = vec3d:new( C[1], C[2], C[3] )
   local v = vec3d:new( rndx, rndy, rndz )
   local sphdot = hydro.new( water.h, water.rho, water.nu, v, c, 'water' )
   hdis.append(  sphdot  )
end

---------------------------------------------------------------------------------
-- river floor and ceiling
---------------------------------------------------------------------------------

local nfloors = 0

local cshift = 0.5 * floor.h
local cdr    = floor.h

local wall                      -- storage for the wall


wall = {}
local wall = hydro.wall(
   { cshift, cshift, cshift },
   { hdis.box.x - cshift, hdis.box.y - cshift, cshift },
   { cdr, cdr, cdr },
   floor.h, floor.rho, floor.nu, 'floor' )

for i, hp in pairs( wall ) do
   hdis.append( hp )
   nfloors = nfloors + 1
end

cdr = 1.4 * floor.h                 -- we will use less dense ceiling

wall = {}
wall = hydro.wall(
   { cshift, cshift, hdis.box.z - cshift },
   { hdis.box.x - cshift, hdis.box.y - cshift, hdis.box.z - cshift },
   { cdr, cdr, cdr },
   floor.h, floor.rho, floor.nu, 'floor' )

for i, hp in pairs( wall ) do
   hdis.append( hp )
   nfloors = nfloors + 1
end
   

---------------------------------------------------------------------------------
-- river banks
---------------------------------------------------------------------------------

-- cdr is also relaxed

wall = {}
wall = hydro.wall(
   { cshift, cshift, cshift },
   { hdis.box.x - cshift, cshift, hdis.box.z - cshift },
   { cdr, cdr, cdr },
   floor.h, floor.rho, floor.nu, 'floor' )

for i, hp in pairs( wall ) do
   hdis.append( hp )
   nfloors = nfloors + 1
end

wall = {}
wall = hydro.wall(
   { cshift, hdis.box.y - cshift, cshift },
   { hdis.box.x - cshift, hdis.box.y - cshift, hdis.box.z - cshift },
   { cdr, cdr, cdr },
   floor.h, floor.rho, floor.nu, 'floor' )

for i, hp in pairs( wall ) do
   hdis.append( hp )
   nfloors = nfloors + 1
end

wall = {}
wall = hydro.wall(
   { cshift, cshift, cshift },
   { cshift, hdis.box.y - cshift, hdis.box.z - cshift },
   { cdr, cdr, cdr },
   floor.h, floor.rho, floor.nu, 'floor' )

for i, hp in pairs( wall ) do
   hdis.append( hp )
   nfloors = nfloors + 1
end

wall = {}
wall = hydro.wall(
   { hdis.box.x - cshift, cshift, cshift },
   { hdis.box.x - cshift, hdis.box.y - cshift, hdis.box.z - cshift },
   { cdr, cdr, cdr },
   floor.h, floor.rho, floor.nu, 'floor' )

for i, hp in pairs( wall ) do
   hdis.append( hp )
   nfloors = nfloors + 1
end

print( "SPH particles in water mass:", #hdis.dots - nfloors )
print( "SPH particles in the floor and the banks:", nfloors )
print( 'Total number of SPH dots:', #hdis.dots )

---------------------------------------------------------------------------------
-- Object addition
-------------------------------------------------------------------------------
-- We create a ball of radius 0.05 m, that belongs to group 'ball' and made out
-- of our material 'wood'. This function just forms a 'shape': a body, atoms
-- it is built from, and points that are atoms 'skeleton'

--local ball = hdis.shape.sphere( 0.05, 'ball', 'wood' )

local ballparams = {}
ballparams.R = 0.2
ballparams.dr = 0.1 * ballparams.R
ballparams.dphi = ( 180.0 / math.pi ) * ballparams.dr / ballparams.R

-- local ball = sphtool.largesphere( ballparams.R,
--                                   ballparams.dr,
--                                   ballparams.dphi, 'ball', 'wood' )
local ball = shape.sphere( ballparams.R, 'ball', 'wood' )


-- center of mass location: we have to place our ball in the box. vec3d is a
-- vector with 3 components. We don't distinguish between space points and
-- vectors.
ball.body.c = vec3d:new( 0.5 * hdis.box.x, 0.5 * hdis.box.y,
                         10 * hdis.box.z )
--ball.body.control = control.builtin.still --we will freeze in the space

-- Appending the ball shape to the list of COUPi objects! From now, our ball is
-- in the system and will be recognized by COUPi.
hdis.append( ball )


---------------------------------------------------------------------------------
hdis.boundary = { opentop = true }

-------------------------------------------------------------------------------
-- Display options
-------------------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave( 'balldrop-' )
hdis.callback.aftersave = hdis.callback.std.aftersave_long()
