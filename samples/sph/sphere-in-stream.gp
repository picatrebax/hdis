#
#  gnuplot script to plot data from sphere-in-stream
#
#  usage: gnuplot sphere-in-stream.gp -
#

fname = 'sphere.txt'


set grid
set yrange [-800:2000]

set term qt 1 title 'Fy'
plot fname using 2:4 with lines lw 2 lt 6 title 'Fy'

D =  0.4
R = 0.5*D
V = (4 * pi / 3.0) * R * R * R
rho = 250.0
rho_f = 1000.0
g = 9.81
F = rho_f * V * g
print( F )
f(x) = F

set term qt 2 title 'Fz'
plot fname using 2:5 with lines lw 2 lt 1 title 'Fz', \
     f(x) with lines lt 2 title 'Archimed'

