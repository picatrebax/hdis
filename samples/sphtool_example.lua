--[[--

  Example: Bouncing elipsoid on ball. This is a very simple example demonstrating
  basics of COUPi.

  Usage: hdis ball.lua

--]]

require "hdis"               -- this module is always required!
require "hdis.shape"         -- this module contains primitive
                              -- shapes, we require it for
                              -- hdis.shape.sphere function
local aux = require "hdis.aux"

local sphtool = require "hdis.sphtool"

-- Time/saving parameters
hdis.time = {
   start = 0,                   -- starting time
   stop  = 3.0,                -- ending time
   dt    = 0.00001,             -- timestep
   save  = 0.01                 -- saving timestep
}

-- The area of simulation
hdis.box = {
   x = 1,
   y = 1,
   z = 1
}

hdis.g = -9.8     -- m/s^2, gravity

-- Materials (rho - density in kg/m^3, G - stiffness in GPa, nu -
-- Poisson coefficent, mu - Coloumb friction (static/dyn), CR -
-- restitution coefficient.

local steel = { name = "steel",
                rho = 7850.0,
                G=80,
                nu=0.27,
                mu=0.3,
                CR=0.8 }

hdis.material:add( steel )

-- Groups. We have to add all group names we are going to use
hdis.groups:add( "balls" )

-- We create an elipse of radiuses 0.1m, 0.08m and 0.06m,
-- that belongs to group "balls" and made out of our material "steel".
-- This function just forms a "shape": a body, atoms it is built from,
-- and points that are atoms "skeleton"
local ellipse = sphtool.ellipsoid(0.1, 0.08, 0.06, 0.01, 10, "balls", "steel")

ellipse.body.c = { 0.5*hdis.box.x+0.0001,
                0.5*hdis.box.y,
                0.7*hdis.box.z }
hdis.append( ellipse )

--aux.print_serial(elipse)

-- We create a ball of radius 0.2m, that belongs to group "balls" and
-- made out of our material "steel". This function just forms a
-- "shape": a body, atoms it is built from, and points that are atoms
-- "skeleton"

local sphere = sphtool.largesphere(0.2, 0.01, 10,"balls", "steel")
sphere.body.c = { 0.5*hdis.box.x,
                0.5*hdis.box.y,
                0.2*hdis.box.z } -- center of mass location: we have
                                  -- to place our ball in the box.

-- Appending the ball shape to the list of COUPi objects! From now,
-- our ball is in the system and will be recognized by COUPi.

hdis.append( sphere )

 --aux.print_serial(sphere)

-- Boundary is made of the same material... for now. It can be done by
-- just simple assignement as follows. There are other ways to do this
-- too, but this one seems to be the simplest approach.

hdis.boundary.material = "steel"

-------------------------------------------------------------------
--                      Display options
-------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave( "spht_exmpl-" )
hdis.callback.aftersave = hdis.callback.std.aftersave_short()
