--[[--

  Example: Bouncing ball: running from previously saved file with
  altered properties. This example uses one of the files previously
  output by COUPi from 'ball.lua' example.

   (c)2016 Coupi, Inc.

  Usage: hdis 3sph.lua; hdis 3sph_runfrom.lua

--]]

require 'hdis'
local vec3d = require 'hdis.vec3d'
local aux   = require 'hdis.aux'
local shape = require 'hdis.shape'

-- getting data from the previously saved file

local fname = 'tsp-00000400ms.h5'

hdis.runfrom ( fname )

-- Adding a new group 'ball' for our new ball in the system
hdis.groups:add( 'ball' )

-- Made of rubber: new material which we add
local nmat = {
   name = 'rubber',
   rho = 1900,
   G = 0.9,
   nu = 0.28,
   mu = 0.99,
   CR = 0.95,
}

hdis.material:add( nmat )

-- Here it is a new shape and body in the system
local S = shape.sphere( 0.05, 'ball', 'rubber' )
S.body.c = vec3d:new ( 0.5*hdis.box.x, 0.5*hdis.box.y, 0.5*hdis.box.z )
hdis.append( S )

-------------------------------------------------------------------
--                      Display options
-------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave( 'ne-' )
hdis.callback.aftersave = hdis.callback.std.aftersave_short()

