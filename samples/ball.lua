--[[--

  ball.lua

  Example: Bouncing ball. This is a very simple example demonstrating the very
  basics of Coupi model.

   (c)2016 Coupi, Inc.

  Usage: hdis ball.lua

--]]

require 'hdis'   -- this module is always required! It defines that this script
                  -- is Coupi Lua scenario.

local shape = require 'hdis.shape' -- this module contains primitive shapes, we
                                    -- require it for shape.sphere function

local vec3d = require 'hdis.vec3d'  -- 3D vector type for vectors and points

---------------------------------------------------------------------------------
-- Main definitions and gravity
---------------------------------------------------------------------------------

-- Time/saving parameters
hdis.time = {
   start = 0,                   -- starting time
   stop  = 1.85,                -- ending time
   dt    = 0.00001,             -- timestep
   save  = 0.01                 -- saving timestep
}

-- The area of simulation
hdis.box = {
   x = 1,
   y = 1,
   z = 1
}

-- m/s^2, gravity
hdis.g = -9.8

---------------------------------------------------------------------------------
-- Groups and Materials
---------------------------------------------------------------------------------

-- Materials (rho - density in kg/m^3, G - stiffness in GPa, nu - Poisson
-- coefficent, mu - Coloumb friction (static/dyn), CR - restitution coefficient.

local steel = { name = 'steel',
                rho = 7850.0,
                G=80,
                nu=0.27,
                mu=0.3,
                CR=0.8 }

hdis.material:add( steel )

-- Groups. We have to add all group names we are going to use
hdis.groups:add( 'ball' )

---------------------------------------------------------------------------------
-- Object addition
---------------------------------------------------------------------------------

-- We create a ball of radius 0.05m, that belongs to group 'ball' and made out
-- of our material 'steel'. This function just forms a 'shape': a body, atoms it
-- is built from, and points that are atoms 'skeleton'

local ball = hdis.shape.sphere( 0.05, 'ball', 'steel' )

-- center of mass location: we have to place our ball in the box. vec3d is a
-- vector with 3 components. We don't distinguish between space points and
-- vectors.
ball.body.c = vec3d:new( 0.5*hdis.box.x,
                         0.5*hdis.box.y,
                         0.5*hdis.box.z )

-- Appending the ball shape to the list of COUPi objects! From now, our ball is
-- in the system and will be recognized by COUPi.

hdis.append( ball )

-- Boundary is made of the same material... for now. It can be done by just
-- simple assignement as follows. There are other ways to do this too, but this
-- one seems to be the simplest approach.

hdis.boundary.material = 'steel'

---------------------------------------------------------------------------------
-- Display options
---------------------------------------------------------------------------------

hdis.callback.presave   = hdis.callback.std.presave( 'ball-' )
hdis.callback.aftersave = hdis.callback.std.aftersave_short()
