--[[--

  Example: Quaternion library test. Just an example of quaternion
  usage.

  Author: Anton Kulchitsky

  Usage: lua quat-test.lua

--]]

quat = require 'hdis.quat'

-- construction from 4 numbers
q1 = quat:new( -1,1,2,3 )
print( 'q1 = ', q1 )

-- unit quaternion 1
q2 = quat:new()
print( 'q2 = ', q2 )

-- construction from an angle and a vector (can be reversed)
q3 = quat:new( math.pi/4, { 0, 1, 0 } )
print( 'q3 = ', q3 )

-- product
print( 'q1 * q2 = ', q1 * q2 )
print( 'q1 * q3 = ', q1 * q3 )

-- norm
print( '|q3| = ', q3:norm() )
print( '|q1| = ', q1:norm() )

-- square
print( 'q1^2 = ', q1:square() )

-- renormalization
q3:renorm()
print( 'renormalized q3 = ', q3 )

-- sum and sub
print( 'q3 - q1 = ', q3 - q1 )
print( 'q1 + q2 = ', q1 + q2 )

-- conjugate
print( 'q3* = ', q3:conjugate() )

