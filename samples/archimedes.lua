--[[---------------------------------------------------------------------------
   archimedes.lua

   Example: 3 different balls in the liquid.

   (c)2017 Coupi, Inc.

   Usage: hdis archimedes.lua
--]]---------------------------------------------------------------------------

require 'hdis' -- this Coupi module is always required!

local shape = require 'hdis.shape' -- this module contains primitive shapes
local hydro = require 'hdis.hydro' -- hydro particles library
local vec3d = require 'hdis.vec3d' -- 3D vector type for vectors and points

-------------------------------------------------------------------------------
-- Main definitions and gravity
-------------------------------------------------------------------------------
-- Time/saving parameters
hdis.time = { start = 0, stop = 2, dt = 10^(-6), save = 0.01 }

-- The area of simulation
hdis.box = { x = 1, y = 1, z = 1 }

-- m/s^2, gravity
hdis.g = -9.8

-------------------------------------------------------------------------------
-- Groups and Materials
-------------------------------------------------------------------------------
-- Materials (rho - density in kg/m^3, G - stiffness in GPa, nu - Poisson
-- coefficient, mu - Coloumb friction (static/dynamic), CR - restitution
-- coefficient.

local steel = { name = 'steel', rho = 7850.0, G = 80, nu = 0.27, mu = 0.3,
   CR =0.8 }

hdis.material:add( steel )

local group = 'water'
hdis.groups:add( group ):add( 'ball' )

-------------------------------------------------------------------------------
-- Object addition
-------------------------------------------------------------------------------
-- We create a ball of radius 0.05 m, that belongs to group 'ball' and made out
-- of our material 'steel'. This function just forms a 'shape': a body, atoms
-- it is built from, and points that are atoms 'skeleton'

local ball = hdis.shape.sphere( 0.05, 'ball', 'steel' )

-- center of mass location: we have to place our ball in the box. vec3d is a
-- vector with 3 components. We don't distinguish between space points and
-- vectors.
ball.body.c = vec3d:new( 0.5 * hdis.box.x, 0.5 * hdis.box.y,
   0.5 * hdis.box.z )

-- Appending the ball shape to the list of COUPi objects! From now, our ball is
-- in the system and will be recognized by COUPi.
hdis.append( ball )

-------------------------------------------------------------------------------
-- Boundary is made of the same material... for now. It can be done by just
-- simple assignment as follows. There are other ways to do this too, but this
-- one seems to be the simplest approach.
-------------------------------------------------------------------------------
hdis.boundary.material = 'steel'

-------------------------------------------------------------------------------
-- Smooth particles
-------------------------------------------------------------------------------
local R = 0.05     -- radius
local rho = 1000  -- density
local nu = 0      -- viscosity
local N = 500    -- total number of dots
local v = vec3d:new()

while #hdis.dots < N do

   local c = vec3d:new(
      R + math.random() * ( hdis.box.x - 2 * R ),
      R + math.random() * ( hdis.box.y - 2 * R ),
      R + math.random() * ( hdis.box.z - 2 * R )
   )
   local flag = true
   for i=1,#hdis.dots do

      if ((hdis.dots[i].c[1]<(c:x()+R))and(hdis.dots[i].c[1]>(c:x()-R))) and
         ((hdis.dots[i].c[2]<(c:y()+R))and(hdis.dots[i].c[2]>(c:y()-R))) and
         ((hdis.dots[i].c[3]<(c:z()+R))and(hdis.dots[i].c[3]>(c:z()-R))) then

         flag = false
         break
      end
   end

   if flag then hdis.append( hydro.new( R, rho, nu, v, c, group ) ) end
end

-------------------------------------------------------------------------------
-- Display options
-------------------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave( 'archi-' )
hdis.callback.aftersave = hdis.callback.std.aftersave_short()