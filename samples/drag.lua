--[[--

  Example: Drag forces usage. Physical Reynolds drag force.

  Author: Anton Kulchitsky

  Usage: hdis drag.lua

--]]

require "hdis"
require "hdis.shape"

-- Time/saving parameters
hdis.time = {
   start = 0,
   stop = 1,
   dt = 0.00001,
   save = 0.01
}

-- The area of simulation
hdis.box = {
   x = 1,
   y = 2,
   z = 1
}

hdis.g = -9.8     -- m/s^2, gravity

-- Reynolds drag force proportional to square of velocity
hdis.drag = {
   uniform = false,
   Cd = 500,
--   mu = 0.9
}

------------------------------------------------------------
-- Groups (you can list all groups at once or connect all add
-- functions together or add them one by one)
------------------------------------------------------------
hdis.groups:add( "large", "medium", "small" )

-- also legitimate and equivalent:
-- hdis.groups:add( "large" ):add( "medium" ):add( "small" )

-- also legitimate and equivalent:
-- hdis.groups:add( "large" )
-- hdis.groups:add( "medium" )
-- hdis.groups:add( "small" )

------------------------------------------------------------
-- Materials
------------------------------------------------------------

-- Just a single material with no-name
local material =   {
   name = "material",
   rho = 3000.0,             -- kg/m^3, density
   G = 30,                   -- GPa, shear modulus
   nu = 0.3,                 -- 1, Poisson coef
   CR = 0.5,                 -- 1, Restitution coef.
   mu = 0.7,                 -- 1, friction (static/dyn)
}

hdis.material:add( material )

------------------------------------------------------------
-- Objects: 6 spheres of different radius
------------------------------------------------------------

local R = 0.05                  -- 3d sphere's radius

local ball = hdis.shape.sphere( 4*R, "large", "material"  )
ball.body.c = { 0.5*hdis.box.x,
                0.35,
                0.5*hdis.box.z } -- center
hdis.append( ball )

local ball = hdis.shape.sphere( 2*R, "large", "material"  )
ball.body.c = { 0.5*hdis.box.x,
                0.75,
                0.5*hdis.box.z } -- center
hdis.append( ball )

local ball = hdis.shape.sphere( R, "medium", "material" )
ball.body.c = { 0.5*hdis.box.x,
                1.0,
                0.5*hdis.box.z } -- center
hdis.append( ball )

local ball = hdis.shape.sphere( 0.5*R, "medium", "material" )
ball.body.c = { 0.5*hdis.box.x,
                1.25,
                0.5*hdis.box.z } -- center
hdis.append( ball )

local ball = hdis.shape.sphere( 0.25*R, "small", "material"  )
ball.body.c = { 0.5*hdis.box.x,
                1.5,
                0.5*hdis.box.z } -- center
hdis.append( ball )

local ball = hdis.shape.sphere( 0.125*R, "small", "material"  )
ball.body.c = { 0.5*hdis.box.x,
                1.75,
                0.5*hdis.box.z } -- center
hdis.append( ball )

----------------------------------------------------------------
---                   Boundary
----------------------------------------------------------------

hdis.boundary.material = "material"

-------------------------------------------------------------------
--                      Display options
-------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave( "drag-" )
hdis.callback.aftersave = hdis.callback.std.aftersave_short()
