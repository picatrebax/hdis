--[[--

  Example: Demonstration of the functionality of the rotation matrix library. 
  Basically just a couple of hints on how to use it.

  Author: Ihor Durnopianov

  Usage: lua rot3d-test.lua

--]]

local quat = require "hdis.quat"
local vec3d = require "hdis.vec3d"
local rot3d = require "hdis.rot3d"

-- Consider the quaternion, which represents pretty random rotation

-- Two variables below are given only to construct the axis
local alpha =0*math.pi/6
local beta = 0*math.pi/8

local axis = { math.cos( beta )*math.cos( alpha ),
               math.cos( beta )*math.sin( alpha ),
               math.sin( beta ) }
local angle = math.pi/4

Q = quat:new( axis, angle )

-- Now consider the matrix, which represents the same rotation
-- (by definition from Wikipedia) 

local ex = vec3d:new( math.cos( angle )+axis[1]^2*(1-math.cos( angle )),
	                  axis[1]*axis[2]*(1-math.cos( angle ) )-axis[3]*math.sin( angle ),
	                  axis[1]*axis[3]*(1-math.cos( angle ) )+axis[2]*math.sin( angle ) )

local ey = vec3d:new( axis[2]*axis[1]*(1-math.cos( angle ) )+axis[3]*math.sin( angle ),
	                  math.cos( angle )+axis[2]^2*(1-math.cos( angle ) ),
	                  axis[2]*axis[3]*(1-math.cos( angle ) )-axis[1]*math.sin( angle ) )

local ez = vec3d:new( axis[3]*axis[1]*(1-math.cos( angle ) )-axis[2]*math.sin( angle ),
	                  axis[3]*axis[2]*(1-math.cos( angle ) )+axis[1]*math.sin( angle ),
	                  math.cos( angle )+axis[3]^2*(1-math.cos( angle ) ) )

R1 = rot3d:new( ex, ey, ez )


print("1) Creation by definition")

print( R1 )

print("--------------------------")

print("2) Creation from 3 angles: 2 to direct an axis and 1 to rotate around it")

R3 = rot3d:new( alpha, beta, angle )

print( R3 )

print("--------------------------")

-- Now consider the same matrix, but restored from quaternion Q

R2 = rot3d:new( Q )

print("3) Creation from quaternion")

print( R2 )

print("--------------------------")

-- Luckily they seem to be pretty identical

-- Test of the rot3d:transpose()

print("4) Transpose")

print( R2:transpose() )

print("--------------------------")

-- Now it's time to check whether matrix-vector multiplication works

print("5) M-v multiplication")

print( R2*ex )

print("--------------------------")

-- And matrix-matrix multiplication

print("6) M-M multiplication")

print( R1*R2 )

print("--------------------------")

-- And in general (note that since R^T = R^(-1), the expression has
-- to return the first row of the rotation matrix)

print("7) Transpose")

print( R2:transpose()*R2*ex )

print("----------------------")

