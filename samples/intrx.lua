--[[---------------------------------------------------------------------------
   Example: Demonstration of non-interacting groups. Two balls falls on the
   big sphere. One ball is excluded from the interaction with the big sphere
   using hdis.groups:avert function. The group that the ball belongs to does not interact with the group that the big sphere belongs to.

   Author: Anton Kulchitsky
   Usage: hdis intrx.lua
--]]---------------------------------------------------------------------------

require "hdis"                          -- this module is always required!
local primitives = require "hdis.shape" -- this module contains primitives

-------------------------------------------------------------------------------
--- Global parameters
-------------------------------------------------------------------------------
-- Time/saving parameters
hdis.time = {
   start = 0,        -- starting time
   stop  = 2.0,      -- ending time
   dt    = 0.00001,  -- time step
   save  = 0.01      -- saving time step
}

hdis.box = { x = 2, y = 2, z = 1 } -- The area of simulation
hdis.g = -9.8                      -- m/s^2, gravity

-------------------------------------------------------------------------------
-- Materials (rho - density in kg/m^3, G - stiffness in GPa, nu - Poisson
-- coefficient, mu - Coloumb friction (static/dynamic), CR - restitution
-- coefficient.
-------------------------------------------------------------------------------
local steel = { name = "steel", rho = 7850.0, G = 0.8, nu = 0.27, mu = 0.3,
   CR = 0.8 }
hdis.material:add( steel )

-------------------------------------------------------------------------------
-- Groups. We have to add all group names we are going to use
-------------------------------------------------------------------------------
hdis.groups:add( "Sphere", "ball-yes", "ball-no" )
hdis.groups:avert( "Sphere", "ball-no" )

-------------------------------------------------------------------------------
-- Boundary is made of the same material... for now. It can be done by just
-- simple assignment as follows. There are other ways to do this too, but this
-- one seems to be the simplest approach.
-------------------------------------------------------------------------------
hdis.boundary.material = "steel"

-------------------------------------------------------------------------------
--- Objects
-------------------------------------------------------------------------------
local a = 0.05

local Sphere = primitives.sphere( 6 * a, "Sphere", "steel" )
Sphere.body.c = { 0.5 * hdis.box.x, 0.5 * hdis.box.y, 6 * a } -- center
hdis.append( Sphere )

local ball
local shift = 3.5 * a

-- ball that interacts with the cube
ball = hdis.shape.sphere( a, "ball-yes", "steel" )
-- center of mass location: we have to place our ball in the box
ball.body.c = { 0.5*hdis.box.x, 0.5*hdis.box.y + shift, hdis.box.z - a }

hdis.append( ball )

-- ball that does not interact with the cube
ball = hdis.shape.sphere( a, "ball-no", "steel" )
ball.body.c = { 0.5*hdis.box.x, 0.5*hdis.box.y - shift, hdis.box.z - a }

hdis.append( ball )

-------------------------------------------------------------------------------
--- Display options
-------------------------------------------------------------------------------
hdis.callback.presave   = hdis.callback.std.presave( "intrx-" )
hdis.callback.aftersave = hdis.callback.std.aftersave_short()
