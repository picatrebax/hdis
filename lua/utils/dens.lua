--[[--

   COUPi utility for bulk density calculations for particles made from
   spheres (general form). Mainly this is also an example how to use
   @{hdis.sphdens} module.

   ATTENTION: This is an unfinished example. Use it as a template for
   your own density utility.

   Usage: lua dens.lua filename

   filename is HDF5 file.

   <p><em>For licensing and copyright use same license as
   COUPi.</em></p>

   @author Anton Kulchitsky

--]]

PROGRAM_NAME = 'dens.lua'

require 'hdis'
local hdf5    = require 'hdis.hdf5'
local Sphdens = require 'hdis.sphdens'


--*------------------------------------------------------------*--
--*                  Local functions
--*------------------------------------------------------------*--

local function usage()
   io.write( 'Usage: lua ' .. PROGRAM_NAME .. ' file\n' )
end

--*------------------------------------------------------------*--
--*            Reading and setting parameters
--*------------------------------------------------------------*--

local fname = arg[1]
if not fname then
   io.write ( 'Error: no input file provided\n' )
   usage()
   return 1
end

local mysph = Sphdens:new( fname )

local as, ps = mysph:read()
local Rmin = mysph.Rmin         -- computed max and min radius
local Rmax = mysph.Rmax
print ( "# Points (total): ", ps, "Atoms (total): ", as )
print ( "# Maximum radius: ", Rmax, "Minimum radius: ", Rmin )

local r0 = {}
local r1 = {}
local N = { 150, 150, 150 }

local inbox = hdf5.read( fname, "Box (domain)" )

local box = inbox[0].fcorner

local cyl = {}
cyl.R = 0.232 -- 46.4 cm of diameter
cyl.cx = box.x / 2
cyl.cy = box.y / 2
cyl.dil_r = 0.001  -- 1 mm
cyl.Reff = cyl.R - cyl.dil_r

-- Rmax for stepping out of the cylinder walls for accuracy
r0[1] = cyl.cx - (cyl.Reff+Rmax)/math.sqrt(2)
r0[2] = cyl.cy - (cyl.Reff+Rmax)/math.sqrt(2)

r1[1] = cyl.cx + (cyl.Reff+Rmax)/math.sqrt(2)
r1[2] = cyl.cy + (cyl.Reff+Rmax)/math.sqrt(2)


-- We do not use it, just for reference
local function cylinder( x, y, z )
   return ( ( x - cyl.cx )^2 + ( y - cyl.cy )^2 - cyl.R^2 < 0 )
end



local density = 2875.0
local k = 0.67637233194727

print ( "# level coords, packing dens, bulk dens,"..
        " bulk dens as in util, nspheres" )


--*------------------------------------------------------------*--
--*                 Calculations
--*------------------------------------------------------------*--


local dz = 0.05                 -- step in z direction
local zh = 0.1                  -- level hight
local zmax = 0.6                -- up to this coordinate

for z = 0, zmax, dz do

   -- coordinates of the box
   r0[3] = math.max( z, Rmax )       -- we step up for Rmax to avoid
                                -- boundary effect
   r1[3] = z + zh

   local nspheres = mysph:select( r0, r1, N )
   local packing, V = mysph:density( nil )
   local bulkdens = density * packing

   local zlevel = 0.5*(r0[3]+r1[3]) 
   print ( zlevel, packing, bulkdens, k* bulkdens, nspheres )
end
