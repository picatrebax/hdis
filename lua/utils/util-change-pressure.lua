--[[--

   COUPi example utility to change pressure on pressure triangles for
   all Ptriangles table.

   ATTENTION: This is an unfinished example. Use it as a template for
   your own density utility.

   Usage: lua util-changge-pressure.lua input output

   input is input HDF5 file.
   
   output is output HDF5 file with updated pressure value.

   <p><em>For licensing and copyright use same license as
   COUPi.</em></p>

   @author Anton Kulchitsky

--]]

require 'hdis'

local hdf5    = require 'hdis.hdf5'
local Dataset = require 'hdis.dataset'
local aux     = require 'hdis.aux'

local finput  = arg[1]
local foutput = arg[2]
local pressure = arg[3]

if ( not ( type(finput) == 'string' ) or 
     not ( type(foutput) == 'string' ) or
        not ( type(pressure) == 'string' )
) then
   print( 'Error: input and output file are not set' )
   print( 'Usage: lua ./util-stage4-prep.lua finput foutput pressure' )
   return 1
end

pressure = tonumber ( pressure )

hdf5.copy_file( finput, foutput )

dataset = Dataset:new()

local ptriags = hdis.hdf5.read( foutput, 'PTriangles' )
dataset.ptriags_set_pressure( ptriags, pressure )

hdis.hdf5.write( foutput, ptriags )
