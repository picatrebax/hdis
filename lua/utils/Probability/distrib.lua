--[[--

   file: distrib.lua

    File contains calculations of some probability distributions.  

   Author: Kirill Sviderskyi

   Usage: inlude "require distrib" in your lua script. 

--]]

-------------------------------------------------------------------
-- "Heavy" calculating
-------------------------------------------------------------------
function box_muller()
    
    repeat
        x=math.random()*2-1
        y=math.random()*2-1
        s=x*x+y*y
    until s>0 and s<=1
    
    return x*math.sqrt(-2 * math.log(s) / s)
end    

function irvin_hall()
    
    s=0
    for i=1,12 do
        s=s+2*math.random() -1
    end
    return s/2
end 


-------------------------------------------------------------------
--Functions that actually return values
-------------------------------------------------------------------

function get_next_normal(m, s2) 
    if not m then m=0 end
    if not s2 then s2=1 end  
    return irvin_hall() * math.sqrt(s2) + m;
end

function get_next_lognormal(m,s2)
    return math.exp(get_next_normal(m,s2))
end

function get_next_lognormal_in_bounds(left, right)
    assert(left<right, "distrib.lua : incoorect params in segment difinition"..
    ". Right point "..right.." must be bigger then left point "..left)
    assert(left>0, "distrib.lua : cannot be negative-size particles")
    
    local a = math.log(left)
    local b = math.log(right)
    local m = (a + b) / 2
    local D = (b - a) * (b - a) / 12
    return math.exp(irvin_hall() * math.sqrt(D) + m)
end

--- ###############################################################


local Distrib = {}

Distrib.Norm = get_next_normal
Distrib.LogNorm = get_next_lognormal
Distrib.LogNormInBounds = get_next_lognormal_in_bounds

return Distrib

