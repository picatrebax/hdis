--[[--

   file: example_distrib.lua

    File contains comparison of 2 methods of generating Normal 
	Distributed Numbers. We generating 10000 numebers using
	each one and calculating how many numbers is in every 
	segment [N/10, (N+1)/10) where -30<=N<=30.
	Output formatted in the "points.txt" file. 
	With help of points.gp we can visualised it 

   Author: Kirill Sviderskyi


--]]

function box_muller()
    
    repeat
        x=math.random()*2-1
        y=math.random()*2-1
        s=x*x+y*y
    until s>0 and s<=1
    
    return x*math.sqrt(-2 * math.log(s) / s)
end    

function irvin_hall()
    
    s=0
    for i=1,12 do
        s=s+2*math.random() -1
    end
    return s/2
end 
bm = {}
ih = {}

for i=-500,500 do
  bm[i]=0
  ih[i]=0
end

for i=1,10000 do
    t= box_muller()
    t=math.floor(t*10)
    bm[t] =bm[t] +1
end

for i=1,10000 do
    t= irvin_hall()
    t=math.floor(t*10)
    ih[t] =ih[t] +1
end

local fout = io.open("points.txt", "w")
for i=-30,30 do
    io.output(fout)
    --f:write(i/10," ",bm[i]," ",ih[i],"\n")
    io.write(i/10," ",bm[i]," ",ih[i],"\n")
end
fout:close()


