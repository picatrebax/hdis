set output "pic.eps"

plot "points.txt" u 1:2 w l title "Box-Muller", \
     "points.txt" u 1:3 w l title "Irvin-Hall"
