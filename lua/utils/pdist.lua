--[[--

   COUPi utility for particle size distribution calculations in a
   given COUPi HDF5 file. This is also an example how to use
   @{hdis.psd} module.

   Usage: lua pdist.lua filename group [N]

   filename is HDF5 file
   group is group number of atoms to select
   N is number of intervals to divide the dmin,dmax interval 
           (default is 20)

   <p><em>For licensing and copyright use same license as
   COUPi.</em></p>

   @author Anton Kulchitsky

--]]

PROGRAM_NAME = 'pdist.lua'

psd = require 'hdis.psd'

--*------------------------------------------------------------*--
--*                  Local functions
--*------------------------------------------------------------*--

local function usage()
   io.write( 'Usage: lua ' .. PROGRAM_NAME .. ' file group N\n' )
end

--*------------------------------------------------------------*--
--*                  Reading parameters
--*------------------------------------------------------------*--

local fname = arg[1]
if not fname then
   io.write ( 'Error: no input file provided\n' )
   usage()
   return 1
end

local gname = arg[2]
if not gname then
   io.write ( 'Error: no group name provided\n' )
   usage()
   return 2
end

local Nr = 20
if arg[3] then
   Nr = tonumber( arg[3] )
   if ( Nr == nil ) then
      io.write ( 'Error: N is not a number\n' )
      usage()
      return 3
   end
   
end

--*------------------------------------------------------------*--
--*                    Computing PSD
--*------------------------------------------------------------*--

myPSD = psd:new( fname )

io.write( '# Reading <' .. fname .. '>\n' )
myPSD:read()

io.write( '# Selecting bodies\n' )
myPSD:select( gname )

io.write( '# Calculating PSD\n' )
local dist = myPSD:psd( Nr )

io.write( '# equiv. diameter, mass dist., number dist.\n' )
for i,e in ipairs(dist) do
   local s = string.format( '% 5.5f  % 10.5f  % 10.5f\n',
                            e.d, e.dmr, e.dnr )
   io.write( s )
end
