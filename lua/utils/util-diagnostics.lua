--[[--

   COUPi utility to compute total linear momentum, total angular momentum and
   total kinetic energy of the set of particles, and the maximum linear
   momentum, maximum angular momentum and maximum kinetic energy of the
   particle from this set

   Usage: lua diagnostics.lua filename groupname

   filename is HDF5 file.

   <p><em>For licensing and copyright use same license as
   COUPi.</em></p>

   @author Ihor Durnopianov

--]]

-------------------------------------------------------------------------------
-- Beginning. Really underwhelming comment, isn't it?! :)
-------------------------------------------------------------------------------

require 'hdis'

local quat = require 'hdis.quat'
local vec3d = require 'hdis.vec3d'
local rot3d = require 'hdis.rot3d'
local hdf5 = require 'hdis.hdf5'
--local dataset = require 'hdis.dataset' -- Basically for now it is useless

local filename = arg[1]
if not filename then
   print( 'Error: no input file provided' )
   print( 'Usage: lua diagnostics.lua filename groupname' )
   return 1
end

local groupname = arg[2]
if not groupname then
   print( 'Error: no group provided' )
   print( 'Usage: lua diagnostics.lua filename groupname' )
   return 1
end

-------------------------------------------------------------------------------
-- The subtraction of the objects, which correspond to the given group
-- Seems that it can be avoided by using hdis.dataset
-------------------------------------------------------------------------------

number_of_bodies = hdf5.get_dims( filename, "Bodies" )
number_of_atoms = hdf5.get_dims( filename, "Atoms" )
number_of_groups = hdf5.get_dims( filename, "Atom Group" )

bodies = hdf5.read( filename, "Bodies" )
atoms = hdf5.read( filename, "Atoms" )
groups = hdf5.read( filename, "Atom Group" )

for i=0,number_of_groups-1 do
   if groups[i] == groupname then
      group_in_question = i
      break;
   end
end

k=0

bodies_in_question = {}

for i=0,0 do
   atom = atoms[i]
   if atom.group_id == group_in_question then
      body = bodies[atom.body_indx]
      body.body_indx = atom.body_indx
      bodies_in_question[k] = body
      k=k+1
   end
end

for i=1,number_of_atoms-1 do
   atom_previous = atoms[i-1]
   atom = atoms[i]
   if ( ( atom.body_indx~=atom_previous.body_indx and
        atom.group_id==group_in_question ) ) then
      body = bodies[atom.body_indx]
      body.body_indx = atom.body_indx
      bodies_in_question[k] = body
      k=k+1
   end
end

-------------------------------------------------------------------------------
-- The place of the only useful stuff
-------------------------------------------------------------------------------

total_linear_momentum = vec3d:new()
total_angular_momentum = vec3d:new()
total_kinetic_energy = { V=0, W=0 }
maximum_linear_momentum = { P=vec3d:new(), indx=0 }
maximum_angular_momentum = { L=vec3d:new(), indx=0 }
maximum_kinetic_energy = { T=0, W=0, indx=0 }

for i=0,k-1 do
   body = bodies_in_question[i]
   local m = body.m
   local v = vec3d:new( body.v.x, body.v.y, body.v.z )
   local w = vec3d:new( body.w.x, body.w.y, body.w.z )
   local q = quat:new( body.q.s, body.q.vx, body.q.vy, body.q.vz )
   local R = rot3d:new( q )

   local J = rot3d:new( vec3d:new( body.I.x, 0, 0 ),
                       vec3d:new( 0, body.I.y, 0 ),
                       vec3d:new( 0, 0, body.I.z ) )

   -- Clearly it is not the most efficient way to represent the matrix
   -- of principal moments of inertia - it is chosen from the
   -- aesthetic considerations :)

   local P = v:scale( m )

   local L = R*J*R:transpose()*w

   --- By the way, please take a look on the expression above:
   --- {L} = [R][J_local][R]^T {w}, where ^T stands for transpose
   --- curly brackets denote the vector, and square brackets denote
   --- the matrix.
   --- It can be obtained in the following way:
   --- {L} = [R]{L_local} = [R]{J_local}{w_local} =
   --- = [R]{J_local}[R]^T {w} = [J]{w}
   --- Please note that [J] = [R]{J_local}[R]^T
   --- which is nothing more than the eigendecomposition of the
   --- real symmetric matrix.

   total_linear_momentum = total_linear_momentum + P
  
   total_angular_momentum = total_angular_momentum + L

   total_kinetic_energy.V = total_kinetic_energy.V + 0.5*m*( v*v )

   total_kinetic_energy.W = total_kinetic_energy.W + 0.5*( w*L )

   if P:square() > maximum_linear_momentum.P:square() then
      maximum_linear_momentum.P = P
      maximum_linear_momentum.indx = body.body_indx
   end
   
   if L:square() > maximum_angular_momentum.L:square() then
      maximum_angular_momentum.L = L
      maximum_angular_momentum.indx = body.body_indx
   end

   -- Please correct me if it is a mistake, but up to me
   -- in our case if a^2 > b^2 then a > b 
   
   if (0.5*m*(v*v)+0.5*(w*L)) > maximum_kinetic_energy.T +
                                maximum_kinetic_energy.W then
      maximum_kinetic_energy.T = 0.5*m*( v*v )
      maximum_kinetic_energy.W = 0.5*( w*L )
      maximum_kinetic_energy.indx = body.body_indx
   end

end

-------------------------------------------------------------------------------
-- Take a wild guess on what is below
-------------------------------------------------------------------------------

print("*** Output ***")

print("Number of bodies in the chosen group: ",k)

print("Total linear momentum: ", total_linear_momentum,
           "\nTotal angular momentum: ", total_angular_momentum,
           "\nTotal kinetic energy: ", total_kinetic_energy.V +
                                       total_kinetic_energy.W,
           "\nMaximum linear momentum: ", maximum_linear_momentum.P,
           " corresponds to the body ", maximum_linear_momentum.indx,
           "\nMaximum angular momentum: ", maximum_angular_momentum.L,
           " corresponds to the body ", maximum_angular_momentum.indx,
           "\nMaximum kinetic energy: ", maximum_kinetic_energy.T +
                                         maximum_kinetic_energy.W,
           " corresponds to the body ", maximum_kinetic_energy.indx)

print("**************")