--[[---------------------------------------------------------------------------
   Coupi utility to stress tensor of the group of particles.

   Usage: lua util-stress-tensor.lua filename groupname

   @copyright Coupi, Inc.
--]]---------------------------------------------------------------------------

require 'hdis'

local quat = require 'hdis.quat'
local vec3d = require 'hdis.vec3d'
local rot3d = require 'hdis.rot3d'
local hdf5 = require 'hdis.hdf5'

local filename, groupname = arg[1], arg[2]
local is_out_pressure = arg[3]

if not filename then error( 'No input file provided\n' ) end
if not groupname then error( 'No group provided\n' ) end

print( "# Reading from HDF5" )

local hdf5bodies = hdf5.read( filename, "Bodies" )
local hdf5atoms = hdf5.read( filename, "Atoms" )
local groups = hdf5.read( filename, "Atom Group" )
local cl = hdf5.read( filename, "Contact List" )
local box = hdf5.read( filename, "Box (domain)" )
local G = hdf5.read( filename, "Gravity" )
local g = G[0].z

-- looking for the group index
local igroup = -1
for i=0,#groups do

   if groups[i] == groupname then

      igroup = i
      break;
   end
end
if igroup == -1 then

   error( 'There are no such group '..groupname..' in file '..filename..'\n' )
end

print( "# Creating stress tensor for each body" )
for i = 0, #hdf5bodies do
   local body = hdf5bodies[i]
   local c   = { body.c.x, body.c.y, body.c.z }
   local fc  = { body.f.x, body.f.y, body.f.z }
   local phi = 1 / body.V
   local m = body.m
   
   body.S  = rot3d:new( vec3d:new(), vec3d:new(), vec3d:new() )
   body.S2 = rot3d:new( vec3d:new(), vec3d:new(), vec3d:new() )
   
   for j=1,3 do
      for k=1,3 do
         body.S.m[j][k]  = - phi * c[j] * fc[k]
         body.S2.m[j][k]  = - phi * m * g
      end
   end
   -- and assigning it to the right
end

print( "# Building Stress Tensor" )

local S = rot3d:new(vec3d:new(),vec3d:new(),vec3d:new())

for i = 0, #cl do

   local id = { cl[i].id1, cl[i].id2 }

   for m = 1,2 do

      -- sign for the force: + for first particle, - for the second!
      local sign = 1
      if m == 2 then sign = -1 end

      -- only adding to not-walls
      if ( id[m]~=box[0].xid ) and ( id[m]~=box[0].yid ) and ( id[m]~=box[0].zid ) then

         local atom = hdf5atoms[ id[m] ]
         local body = hdf5bodies[ atom.body_indx ]

         if ( atom.group_id == igroup ) then

            local c   = { body.c.x, body.c.y, body.c.z }
            local phi = sign / body.V
            local f   = { cl[i].f.x, cl[i].f.y, cl[i].f.z }
            local x   = { cl[i].pc.x, cl[i].pc.y, cl[i].pc.z }
            local l   = { x[1] - c[1], x[2] - c[2], x[3] - c[3] }
            
            for j=1,3 do
               for k=1,3 do
                  body.S.m[j][k]  = body.S.m[j][k] + phi * x[j] * f[k]
                  body.S2.m[j][k] = body.S2.m[j][k] + phi * l[j] * f[k]
               end
            end

         end
      end
      
   end
end

-- change to false to print out tensors of each particles particles
local silent = true

if not is_out_pressure then
   print( 'Integral stress tensor' )
   print( S )
end

local bodies = {}
for i=0,#hdf5bodies do

   if hdf5bodies[i].S then
      table.insert( bodies, hdf5bodies[i] )
      if not silent then
         print( 'Stress tensor for body', #bodies )
         print( hdf5bodies[i].S )
      end
   end
end

---------------------------------------------------------------------------------

if is_out_pressure then
   for i = 0, #hdf5bodies do
      local c = hdf5bodies[i].c
      
      if ( hdf5bodies[i].S ) then
         local s = hdf5bodies[i].S:value()
         local s2 = hdf5bodies[i].S2:value()
         local T = (1./3.) * ( s[1][1] + s[2][2] + s[3][3] )
         local T2 = (1./3.) * ( s2[1][1] + s2[2][2] + s2[3][3] )
         print( c.x, c.y, c.z, T, T2 )
      end
      
   end
end
