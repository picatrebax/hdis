#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
#include <hdf5.h>
#include <stdlib.h>

#include "defs.h"
#include "hdis-types.h"
#include "atom.h"
#include "bb.h"
#include "body.h"
#include "conmech.h"
#include "lptimer.h"
#include "sph.h"
#include "bc.h"
#include "spring.h"
#include "ptriangle.h"
#include "conhash.h"
#include "bc.h"

/************************************************
 *                     H5F
 ************************************************/

static int lua_H5Fopen(lua_State *L)
{
  const char* name = lua_tostring(L, 1);
  unsigned int flags = (unsigned int) lua_tointeger(L, 2);
  hid_t fapl_id = (hid_t) lua_tointeger(L, 3);

  hid_t result = H5Fopen(name, H5F_ACC_RDWR, H5P_DEFAULT);

  lua_pushinteger(L, result );
  return 1;
}

static int lua_H5Fclose(lua_State *L)
{
  hid_t file_id = lua_tointeger(L, 1);

  lua_pushinteger(L, H5Fclose(file_id));
  return 1;
}

/************************************************
 *                     H5D
 ************************************************/

static int lua_H5Dopen(lua_State *L)
{
  hid_t loc_id = (hid_t) lua_tointeger(L, 1);
  const char* name = lua_tostring(L, 2);

  /* first we check if the dataset exists */
  if ( ! H5Lexists( loc_id, name, H5P_DEFAULT ) )
    {
      lua_pushinteger(L, -1);
    }
  else
    {
      lua_pushinteger(L, H5Dopen2(loc_id, name, H5P_DEFAULT));
    }

  return 1;
}

static int lua_H5Dclose(lua_State *L)
{
  hid_t dataset_id = (hid_t) lua_tointeger(L, 1);

  lua_pushinteger(L, H5Dclose(dataset_id));
  return 1;
}

static int lua_H5Dget_type(lua_State *L)
{
  hid_t dataset_id = (hid_t) lua_tointeger(L, 1);

  lua_pushinteger(L, H5Dget_type(dataset_id));
  return 1;
}

/************************************************
 *                     H5T
 ************************************************/

static int lua_H5Tcreate(lua_State *L)
{
  H5T_class_t class = (H5T_class_t) lua_tointeger(L, 1);
  size_t size = (size_t) lua_tointeger(L, 2);

  lua_pushinteger(L, H5Tcreate(class, size));
  return 1;
}

static int lua_H5Tclose(lua_State *L)
{
  hid_t dtype_id = (hid_t) lua_tointeger(L, 1);

  lua_pushinteger(L, H5Tclose(dtype_id));
  return 1;
}

/************************************************
 *                     H5L
 ************************************************/

static int lua_H5Lexists(lua_State *L)
{
  hid_t loc_id = (hid_t) lua_tointeger(L, 1);
  const char* name = lua_tostring(L, 2);
  hid_t lapl_id = (hid_t) lua_tointeger(L, 3);

  lua_pushinteger(L, H5Lexists(loc_id, name, lapl_id));
  return 1;
}

/************************************************
 *                  Custom
 ************************************************/

static int lua_H5Cget_H5P_DEFAULT(lua_State *L)
{
  lua_pushinteger(L, H5P_DEFAULT);
  return 1;
}

#define ARRAY_SIZE_KEY "n"
#define FILE_ID_KEY    "file_id"
#define DATASET_ID_KEY "dataset_id"
#define TYPE_ID_KEY    "type_id"

static int lua_H5Cread(lua_State *L)
{
  hid_t dataset_id = (hid_t)  lua_tointeger(L, 1);
  hid_t datatype_id = (hid_t) lua_tointeger(L, 2);

  // dataset doesn't exist
  if (dataset_id < 0)
  {
    lua_newuserdata(L, 0);
    lua_pushinteger(L, 0);
    return 2;
  }

  hid_t dataspace_id = H5Dget_space(dataset_id);

  H5S_class_t dataspace_type =
    H5Sget_simple_extent_type(dataspace_id);

  hsize_t dims;
  if (dataspace_type == H5S_SCALAR) dims = 1;
  else H5Sget_simple_extent_dims(dataspace_id, &dims, NULL);

  size_t size = H5Tget_size( datatype_id );

  void *data = lua_newuserdata(L, size*dims);
  
  H5Dread(dataset_id, datatype_id, H5S_ALL, H5S_ALL,
          H5P_DEFAULT, data);

  lua_pushinteger(L, dims);

  return 2;
}

static int lua_H5Cwrite(lua_State *L)
{
  hid_t dataset_id = (hid_t) lua_tointeger(L, 1);
  hid_t datatype_id = (hid_t) lua_tointeger(L, 2);
  void *data = lua_touserdata(L, 3);

  lua_pushinteger(L, H5Dwrite(dataset_id, datatype_id,
                             H5S_ALL, H5S_ALL, H5P_DEFAULT, data));
  return 1;
}

static int lua_H5Cget_dims(lua_State *L)
{
  hid_t dataset_id = (hid_t) lua_tointeger(L, 1);
  hid_t datatype_id = (hid_t) lua_tointeger(L, 2);
  hid_t dataspace_id = H5Dget_space(dataset_id);
  H5S_class_t dataspace_type = H5Sget_simple_extent_type(dataspace_id);
  hsize_t dims;
  if (dataspace_type == H5S_SCALAR) dims = 1;
  else H5Sget_simple_extent_dims(dataspace_id, &dims, NULL);

  lua_pushinteger(L, dims);
  return 1;
}

#define H5Cstruct_insert(dtype, structure, ftype, field) \
  H5Tinsert(dtype, #field, HOFFSET( structure, field ), ftype );

#define H5Ctable_insert(L, p_struct, field)     \
  lua_pushstring(L, #field);                    \
  lua_pushnumber(L, p_struct->field);           \
  lua_rawset(L, -3);

#define H5Ctable_insert_sting(L, p_struct, field)       \
  lua_pushstring(L, #field);                            \
  lua_pushstring(L, p_struct->field);                   \
  lua_rawset(L, -3);

#define H5Ctable_insert_vec3d(L, p_struct, field) \
  lua_pushstring(L, #field);                      \
  lua_newtable(L);                                \
  lua_pushstring(L, "x");                         \
  lua_pushnumber(L, p_struct->field.x);           \
  lua_rawset(L, -3);                              \
  lua_pushstring(L, "y");                         \
  lua_pushnumber(L, p_struct->field.y);           \
  lua_rawset(L, -3);                              \
  lua_pushstring(L, "z");                         \
  lua_pushnumber(L, p_struct->field.z);           \
  lua_rawset(L, -3);                              \
  lua_rawset(L, -3);

#define H5Ctable_insert_rquat(L, p_struct, field) \
  lua_pushstring(L, #field);                      \
  lua_newtable(L);                                \
  lua_pushstring(L, "s");                         \
  lua_pushnumber(L, p_struct->field.s);           \
  lua_rawset(L, -3);                              \
  lua_pushstring(L, "vx");                        \
  lua_pushnumber(L, p_struct->field.vx);          \
  lua_rawset(L, -3);                              \
  lua_pushstring(L, "vy");                        \
  lua_pushnumber(L, p_struct->field.vy);          \
  lua_rawset(L, -3);                              \
  lua_pushstring(L, "vz");                        \
  lua_pushnumber(L, p_struct->field.vz);          \
  lua_rawset(L, -3);                              \
  lua_rawset(L, -3);

#define H5Cset_struct_field(L, p_struct, field) \
  lua_pushstring(L, #field);                    \
  lua_rawget(L, -2);                            \
  p_struct->field = lua_tonumber(L, -1);        \
  lua_pop(L, 1);

#define H5Cset_struct_field_string(L, p_struct, field) \
  lua_pushstring(L, #field);                           \
  lua_rawget(L, -2);                                   \
  p_struct->field = lua_tostring(L, -1);               \
  lua_pop(L, 1);

#define H5Cset_struct_field_vec3d(L, p_struct, field)   \
  lua_pushstring(L, #field);                            \
  lua_rawget(L, -2);                                    \
  lua_pushstring(L, "x");                               \
  lua_rawget(L, -2);                                    \
  p_struct->field.x = lua_tonumber(L, -1);              \
  lua_pop(L, 1);                                        \
  lua_pushstring(L, "y");                               \
  lua_rawget(L, -2);                                    \
  p_struct->field.y = lua_tonumber(L, -1);              \
  lua_pop(L, 1);                                        \
  lua_pushstring(L, "z");                               \
  lua_rawget(L, -2);                                    \
  p_struct->field.z = lua_tonumber(L, -1);              \
  lua_pop(L, 1);                                        \
  lua_pop(L, 1);

#define H5Cset_struct_field_rquat(L, p_struct, field) \
  lua_pushstring(L, #field);                          \
  lua_rawget(L, -2);                                  \
  lua_pushstring(L, "s");                             \
  lua_rawget(L, -2);                                  \
  p_struct->field.s = lua_tonumber(L, -1);            \
  lua_pop(L, 1);                                      \
  lua_pushstring(L, "vx");                            \
  lua_rawget(L, -2);                                  \
  p_struct->field.vx = lua_tonumber(L, -1);           \
  lua_pop(L, 1);                                      \
  lua_pushstring(L, "vy");                            \
  lua_rawget(L, -2);                                  \
  p_struct->field.vy = lua_tonumber(L, -1);           \
  lua_pop(L, 1);                                      \
  lua_pushstring(L, "vz");                            \
  lua_rawget(L, -2);                                  \
  p_struct->field.vz = lua_tonumber(L, -1);           \
  lua_pop(L, 1);                                      \
  lua_pop(L, 1);

/************************************************
 *                  material
 ************************************************/

static int lua_H5Cregister_material(lua_State *L)
{
  hid_t material_type = H5Tcreate(H5T_COMPOUND, sizeof(material_t));

  H5Cstruct_insert(material_type, material_t, H5T_NATIVE_DOUBLE, rho);
  H5Cstruct_insert(material_type, material_t, H5T_NATIVE_DOUBLE, G);
  H5Cstruct_insert(material_type, material_t, H5T_NATIVE_DOUBLE, nu);
  H5Cstruct_insert(material_type, material_t, H5T_NATIVE_DOUBLE, gamma);
  H5Cstruct_insert(material_type, material_t, H5T_NATIVE_DOUBLE, CR0);
  H5Cstruct_insert(material_type, material_t, H5T_NATIVE_DOUBLE, vR0);
  H5Cstruct_insert(material_type, material_t, H5T_NATIVE_DOUBLE,
                   alpha_t);
  H5Cstruct_insert(material_type, material_t, H5T_NATIVE_DOUBLE, mu);

  lua_pushinteger(L, material_type);
  return 1;
}

static int lua_H5Cmaterial_make_table(lua_State *L)
{
  material_t *material = (material_t *) lua_touserdata(L, 1);
  unsigned int n_material = (unsigned int) lua_tointeger(L, 2);

  lua_newtable(L);
  unsigned int n;
  for (n = 0; n < n_material; ++n)
  {
    lua_pushinteger(L, n);
    lua_newtable(L);

    H5Ctable_insert(L, material, rho);
    H5Ctable_insert(L, material, G);
    H5Ctable_insert(L, material, nu);
    H5Ctable_insert(L, material, gamma);
    H5Ctable_insert(L, material, CR0);
    H5Ctable_insert(L, material, vR0);
    H5Ctable_insert(L, material, alpha_t);
    H5Ctable_insert(L, material, mu);

    lua_rawset(L, -3);

    material++;
  }
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_pushinteger(L, n_material);
  lua_rawset(L, -3);

  return 1;
}

static int lua_H5Cmaterial_make_raw(lua_State *L)
{
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_rawget(L, 1);
  unsigned int n_material = (unsigned int) lua_tointeger(L, -1);
  lua_pop(L, 1);

  material_t *material =
    (material_t *) lua_newuserdata(L, sizeof(material_t)*n_material);
  unsigned int n;
  for(n = 0; n < n_material; ++n)
  {
    lua_pushinteger(L, n);
    lua_rawget(L, 1);

    H5Cset_struct_field(L, material, rho);
    H5Cset_struct_field(L, material, G);
    H5Cset_struct_field(L, material, nu);
    H5Cset_struct_field(L, material, gamma);
    H5Cset_struct_field(L, material, CR0);
    H5Cset_struct_field(L, material, vR0);
    H5Cset_struct_field(L, material, alpha_t);
    H5Cset_struct_field(L, material, mu);

    lua_pop(L, 1);

    material++;
  }
  return 1;
}

/************************************************
 *                  atom
 ************************************************/

static int lua_H5Cregister_atom(lua_State *L)
{
  hid_t vec3d_type = H5Tcreate(H5T_COMPOUND, sizeof(vec3d_t));
  H5Cstruct_insert(vec3d_type, vec3d_t, H5T_NATIVE_DOUBLE, x);
  H5Cstruct_insert(vec3d_type, vec3d_t, H5T_NATIVE_DOUBLE, y);
  H5Cstruct_insert(vec3d_type, vec3d_t, H5T_NATIVE_DOUBLE, z);

  hid_t atom_type = H5Tcreate(H5T_COMPOUND, sizeof(atom_t));
  H5Cstruct_insert(atom_type, atom_t, H5T_NATIVE_DOUBLE, R);
  H5Cstruct_insert(atom_type, atom_t, H5T_NATIVE_DOUBLE, ri);
  H5Cstruct_insert(atom_type, atom_t, H5T_NATIVE_INT, body_indx);
  H5Cstruct_insert(atom_type, atom_t, H5T_NATIVE_INT, group_id);
  H5Cstruct_insert(atom_type, atom_t, H5T_NATIVE_INT, material_id);

  H5Cstruct_insert(atom_type, atom_t, H5T_NATIVE_INT, nverts);
  H5Cstruct_insert(atom_type, atom_t, H5T_NATIVE_INT, ivert0);

  H5Cstruct_insert(atom_type, atom_t, vec3d_type, sbb_c);
  H5Cstruct_insert(atom_type, atom_t, H5T_NATIVE_DOUBLE, sbb_Rmin);

  lua_pushinteger(L, atom_type);
  return 1;
}

static int lua_H5Catom_make_table(lua_State *L)
{
  atom_t *atom = (atom_t *) lua_touserdata(L, 1);
  unsigned int n_atom = (unsigned int) lua_tointeger(L, 2);

  lua_newtable(L);
  unsigned int n;
  for (n = 0; n < n_atom; ++n)
  {
    lua_pushinteger(L, n);
    lua_newtable(L);

    H5Ctable_insert(L, atom, R);
    H5Ctable_insert(L, atom, ri);
    H5Ctable_insert(L, atom, body_indx);
    H5Ctable_insert(L, atom, group_id);
    H5Ctable_insert(L, atom, material_id);

    H5Ctable_insert(L, atom, nverts);
    H5Ctable_insert(L, atom, nedges);
    H5Ctable_insert(L, atom, nfaces);
    H5Ctable_insert(L, atom, ivert0);
    H5Ctable_insert(L, atom, iedge0);
    H5Ctable_insert(L, atom, iface0);

    H5Ctable_insert_vec3d(L, atom, sbb_c);
    H5Ctable_insert(L, atom, sbb_Rmin);
    H5Ctable_insert(L, atom, isolated);

    lua_rawset(L, -3);

    atom++;
  }

  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_pushinteger(L, n_atom);
  lua_rawset(L, -3);

  return 1;
}

static int lua_H5Catom_make_raw(lua_State *L)
{
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_rawget(L, 1);
  unsigned int n_atom = (unsigned int) lua_tointeger(L, -1);
  lua_pop(L, 1);

  atom_t *atom = (atom_t *) lua_newuserdata(L, sizeof(atom_t)*n_atom);
  unsigned int n;
  for(n = 0; n < n_atom; ++n)
  {
    lua_pushinteger(L, n);
    lua_rawget(L, 1);

    H5Cset_struct_field(L, atom, R);
    H5Cset_struct_field(L, atom, ri);
    H5Cset_struct_field(L, atom, body_indx);
    H5Cset_struct_field(L, atom, group_id);
    H5Cset_struct_field(L, atom, material_id);

    H5Cset_struct_field(L, atom, nverts);
    H5Cset_struct_field(L, atom, nedges);
    H5Cset_struct_field(L, atom, nfaces);
    H5Cset_struct_field(L, atom, ivert0);
    H5Cset_struct_field(L, atom, iedge0);
    H5Cset_struct_field(L, atom, iface0);

    H5Cset_struct_field_vec3d(L, atom, sbb_c);

    H5Cset_struct_field(L, atom, sbb_Rmin);
    H5Cset_struct_field(L, atom, isolated);

    lua_pop(L, 1);

    atom++;
  }
  return 1;
}

/************************************************
 *                  vert
 ************************************************/

static int lua_H5Cregister_vert(lua_State *L)
{
  hid_t vec3d_type = H5Tcreate(H5T_COMPOUND, sizeof(vec3d_t));
  H5Cstruct_insert(vec3d_type, vec3d_t, H5T_NATIVE_DOUBLE, x);
  H5Cstruct_insert(vec3d_type, vec3d_t, H5T_NATIVE_DOUBLE, y);
  H5Cstruct_insert(vec3d_type, vec3d_t, H5T_NATIVE_DOUBLE, z);

  hid_t vert_type = H5Tcreate(H5T_COMPOUND, sizeof(vert_t));
  H5Cstruct_insert(vert_type, vert_t, vec3d_type, local_pos);
  H5Cstruct_insert(vert_type, vert_t, vec3d_type, global_pos);

  H5Cstruct_insert(vert_type, vert_t, H5T_NATIVE_INT, deg);
  H5Cstruct_insert(vert_type, vert_t, H5T_NATIVE_INT, iadj_e);
  H5Cstruct_insert(vert_type, vert_t, H5T_NATIVE_INT, ibody);

  lua_pushinteger(L, vert_type);
  return 1;
}

static int lua_H5Cvert_make_table(lua_State *L)
{
  vert_t *vert = (vert_t *) lua_touserdata(L, 1);
  unsigned int n_vert = (unsigned int) lua_tointeger(L, 2);

  lua_newtable(L);
  unsigned int n;
  for (n = 0; n < n_vert; ++n)
  {
    lua_pushinteger(L, n);
    lua_newtable(L);

    H5Ctable_insert_vec3d(L, vert, local_pos);
    H5Ctable_insert_vec3d(L, vert, global_pos);

    H5Ctable_insert(L, vert, deg);
    H5Ctable_insert(L, vert, iadj_e);
    H5Ctable_insert(L, vert, ibody);

    lua_rawset(L, -3);

    vert++;
  }

  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_pushinteger(L, n_vert);
  lua_rawset(L, -3);

  return 1;
}

static int lua_H5Cvert_make_raw(lua_State *L)
{
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_rawget(L, 1);
  unsigned int n_vert = (unsigned int) lua_tointeger(L, -1);
  lua_pop(L, 1);

  vert_t *vert = (vert_t *) lua_newuserdata(L, sizeof(vert_t)*n_vert);
  unsigned int n;
  for(n = 0; n < n_vert; ++n)
  {
    lua_pushinteger(L, n);
    lua_rawget(L, 1);

    H5Cset_struct_field_vec3d(L, vert, local_pos);
    H5Cset_struct_field_vec3d(L, vert, global_pos);

    H5Cset_struct_field(L, vert, deg);
    H5Cset_struct_field(L, vert, iadj_e);
    H5Cset_struct_field(L, vert, ibody);

    lua_pop(L, 1);

    vert++;
  }
  return 1;
}

/************************************************
 *                  sbb_params
 ************************************************/

static int lua_H5Cregister_sbb_params(lua_State *L)
{
  hid_t sbb_params_type = H5Tcreate(H5T_COMPOUND,
                                    sizeof(sbb_params_t));

  H5Cstruct_insert(sbb_params_type, sbb_params_t,
                   H5T_NATIVE_DOUBLE, drmax);
  H5Cstruct_insert(sbb_params_type, sbb_params_t,
                   H5T_NATIVE_DOUBLE, drmin);
  H5Cstruct_insert(sbb_params_type, sbb_params_t,
                   H5T_NATIVE_DOUBLE, tau);

  lua_pushinteger(L, sbb_params_type);
  return 1;
}

static int lua_H5Csbb_params_make_table(lua_State *L)
{
  sbb_params_t *params = (sbb_params_t *) lua_touserdata(L, 1);
  unsigned int n_params = (unsigned int) lua_tointeger(L, 2);

  lua_newtable(L);
  unsigned int n;
  for (n = 0; n < n_params; ++n)
  {
    lua_pushinteger(L, n);
    lua_newtable(L);

    H5Ctable_insert(L, params, drmax);
    H5Ctable_insert(L, params, drmin);
    H5Ctable_insert(L, params, tau);

    lua_rawset(L, -3);

    params++;
  }
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_pushinteger(L, n_params);
  lua_rawset(L, -3);

  return 1;
}

static int lua_H5Csbb_params_make_raw(lua_State *L)
{
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_rawget(L, 1);
  unsigned int n_params = (unsigned int) lua_tointeger(L, -1);
  lua_pop(L, 1);

  sbb_params_t *params = (sbb_params_t *)
    lua_newuserdata(L, sizeof(sbb_params_t)*n_params);
  unsigned int n;
  for(n = 0; n < n_params; ++n)
  {
    lua_pushinteger(L, n);
    lua_rawget(L, 1);

    H5Cset_struct_field(L, params, drmax);
    H5Cset_struct_field(L, params, drmin);
    H5Cset_struct_field(L, params, tau);

    lua_pop(L, 1);

    params++;
  }
  return 1;
}

/************************************************
 *                  body
 ************************************************/

static int lua_H5Cregister_body(lua_State *L)
{
  hid_t vec3d_type = H5Tcreate(H5T_COMPOUND, sizeof(vec3d_t));
  H5Cstruct_insert(vec3d_type, vec3d_t, H5T_NATIVE_DOUBLE, x);
  H5Cstruct_insert(vec3d_type, vec3d_t, H5T_NATIVE_DOUBLE, y);
  H5Cstruct_insert(vec3d_type, vec3d_t, H5T_NATIVE_DOUBLE, z);

  hid_t rquat_type = H5Tcreate(H5T_COMPOUND, sizeof(rquat_t));
  H5Cstruct_insert(rquat_type, rquat_t, H5T_NATIVE_DOUBLE, s);
  H5Cstruct_insert(rquat_type, rquat_t, H5T_NATIVE_DOUBLE, vx);
  H5Cstruct_insert(rquat_type, rquat_t, H5T_NATIVE_DOUBLE, vy);
  H5Cstruct_insert(rquat_type, rquat_t, H5T_NATIVE_DOUBLE, vz);

  hid_t body_type = H5Tcreate(H5T_COMPOUND, sizeof(body_t));
  H5Cstruct_insert(body_type, body_t, rquat_type, q);
  H5Cstruct_insert(body_type, body_t, vec3d_type, c);
  H5Cstruct_insert(body_type, body_t, vec3d_type, v);
  H5Cstruct_insert(body_type, body_t, vec3d_type, w);
  H5Cstruct_insert(body_type, body_t, vec3d_type, dwb);
  H5Cstruct_insert(body_type, body_t, vec3d_type, f);
  H5Cstruct_insert(body_type, body_t, vec3d_type, t);
  H5Cstruct_insert(body_type, body_t, vec3d_type, l);
  H5Cstruct_insert(body_type, body_t, vec3d_type, I);
  H5Cstruct_insert(body_type, body_t, H5T_NATIVE_DOUBLE, m);
  H5Cstruct_insert(body_type, body_t, H5T_NATIVE_DOUBLE, V);
  H5Cstruct_insert(body_type, body_t, H5T_NATIVE_INT, ifunc);
  H5Cstruct_insert(body_type, body_t, H5T_NATIVE_INT, imonitor);
  H5Cstruct_insert(body_type, body_t, H5T_NATIVE_USHORT, flag);

  lua_pushinteger(L, body_type);
  return 1;
}

static int lua_H5Cbody_make_table(lua_State *L)
{
  body_t *body = (body_t *) lua_touserdata(L, 1);
  unsigned int n_body = (unsigned int) lua_tointeger(L, 2);

  lua_newtable(L);
  unsigned int n;
  for (n = 0; n < n_body; ++n)
  {
    lua_pushinteger(L, n);
    lua_newtable(L);

    H5Ctable_insert_rquat(L, body, q);
    H5Ctable_insert_vec3d(L, body, c);
    H5Ctable_insert_vec3d(L, body, v);
    H5Ctable_insert_vec3d(L, body, w);
    H5Ctable_insert_vec3d(L, body, dwb);
    H5Ctable_insert_vec3d(L, body, f);
    H5Ctable_insert_vec3d(L, body, t);
    H5Ctable_insert_vec3d(L, body, l);
    H5Ctable_insert_vec3d(L, body, I);
    H5Ctable_insert(L, body, m);
    H5Ctable_insert(L, body, V);
    H5Ctable_insert(L, body, ifunc);
    H5Ctable_insert(L, body, imonitor);
    H5Ctable_insert(L, body, flag);

    lua_rawset(L, -3);

    body++;
  }
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_pushinteger(L, n_body);
  lua_rawset(L, -3);

  return 1;
}

static int lua_H5Cbody_make_raw(lua_State *L)
{
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_rawget(L, 1);
  unsigned int n_body = (unsigned int) lua_tointeger(L, -1);
  lua_pop(L, 1);

  body_t *body = (body_t *) lua_newuserdata(L, sizeof(body_t)*n_body);
  unsigned int n;
  for(n = 0; n < n_body; ++n)
  {
    lua_pushinteger(L, n);
    lua_rawget(L, 1);

    H5Cset_struct_field_rquat(L, body, q);
    H5Cset_struct_field_vec3d(L, body, c);
    H5Cset_struct_field_vec3d(L, body, v);
    H5Cset_struct_field_vec3d(L, body, w);
    H5Cset_struct_field_vec3d(L, body, dwb);
    H5Cset_struct_field_vec3d(L, body, f);
    H5Cset_struct_field_vec3d(L, body, t);
    H5Cset_struct_field_vec3d(L, body, l);
    H5Cset_struct_field_vec3d(L, body, I);
    H5Cset_struct_field(L, body, m);
    H5Cset_struct_field(L, body, V);
    H5Cset_struct_field(L, body, ifunc);
    H5Cset_struct_field(L, body, imonitor);
    H5Cset_struct_field(L, body, flag);

    lua_pop(L, 1);

    body++;
  }
  return 1;
}

/************************************************
 *                  spring
 ************************************************/

static int lua_H5Cregister_spring(lua_State *L)
{
  hid_t vec3d_type = H5Tcreate(H5T_COMPOUND, sizeof(vec3d_t));
  H5Cstruct_insert(vec3d_type, vec3d_t, H5T_NATIVE_DOUBLE, x);
  H5Cstruct_insert(vec3d_type, vec3d_t, H5T_NATIVE_DOUBLE, y);
  H5Cstruct_insert(vec3d_type, vec3d_t, H5T_NATIVE_DOUBLE, z);

  hid_t spring_type = H5Tcreate(H5T_COMPOUND, sizeof(spring_t));
  H5Cstruct_insert(spring_type, spring_t, vec3d_type, rb1);
  H5Cstruct_insert(spring_type, spring_t, vec3d_type, rb2);
  H5Cstruct_insert(spring_type, spring_t, vec3d_type, r1);
  H5Cstruct_insert(spring_type, spring_t, vec3d_type, r2);
  H5Cstruct_insert(spring_type, spring_t, H5T_NATIVE_DOUBLE, x0);
  H5Cstruct_insert(spring_type, spring_t, H5T_NATIVE_DOUBLE, k);
  H5Cstruct_insert(spring_type, spring_t, H5T_NATIVE_DOUBLE, eta);
  H5Cstruct_insert(spring_type, spring_t, H5T_NATIVE_INT, ib1);
  H5Cstruct_insert(spring_type, spring_t, H5T_NATIVE_INT, ib2);
  H5Cstruct_insert(spring_type, spring_t, H5T_NATIVE_INT, group);

  lua_pushinteger(L, spring_type);
  return 1;
}

static int lua_H5Cspring_make_table(lua_State *L)
{
  spring_t *spring = (spring_t *) lua_touserdata(L, 1);
  unsigned int n_spring = (unsigned int) lua_tointeger(L, 2);

  lua_newtable(L);
  unsigned int n;
  for (n = 0; n < n_spring; ++n)
  {
    lua_pushinteger(L, n);
    lua_newtable(L);

    H5Ctable_insert_vec3d(L, spring, rb1);
    H5Ctable_insert_vec3d(L, spring, rb2);
    H5Ctable_insert_vec3d(L, spring, r1);
    H5Ctable_insert_vec3d(L, spring, r2);
    H5Ctable_insert(L, spring, x0);
    H5Ctable_insert(L, spring, k);
    H5Ctable_insert(L, spring, eta);
    H5Ctable_insert(L, spring, ib1);
    H5Ctable_insert(L, spring, ib2);
    H5Ctable_insert(L, spring, group);

    lua_rawset(L, -3);

    spring++;
  }
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_pushinteger(L, n_spring);
  lua_rawset(L, -3);

  return 1;
}

static int lua_H5Cspring_make_raw(lua_State *L)
{
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_rawget(L, 1);
  unsigned int n_spring = (unsigned int) lua_tointeger(L, -1);
  lua_pop(L, 1);

  spring_t *spring = (spring_t *) lua_newuserdata(L, sizeof(spring_t)*n_spring);
  unsigned int n;
  for(n = 0; n < n_spring; ++n)
  {
    lua_pushinteger(L, n);
    lua_rawget(L, 1);

    H5Cset_struct_field_vec3d(L, spring, rb1);
    H5Cset_struct_field_vec3d(L, spring, rb2);
    H5Cset_struct_field_vec3d(L, spring, r1);
    H5Cset_struct_field_vec3d(L, spring, r2);
    H5Cset_struct_field(L, spring, x0);
    H5Cset_struct_field(L, spring, k);
    H5Cset_struct_field(L, spring, eta);
    H5Cset_struct_field(L, spring, ib1);
    H5Cset_struct_field(L, spring, ib2);
    H5Cset_struct_field(L, spring, group);

    lua_pop(L, 1);

    spring++;
  }
  return 1;
}

/************************************************
 *                   ptriangle
 ************************************************/

static int lua_H5Cregister_ptriangle(lua_State *L)
{
  hid_t ptriangle_type = H5Tcreate(H5T_COMPOUND, sizeof(ptriangle_t));
  H5Cstruct_insert(ptriangle_type, ptriangle_t, H5T_NATIVE_INT, ib1);
  H5Cstruct_insert(ptriangle_type, ptriangle_t, H5T_NATIVE_INT, ib2);
  H5Cstruct_insert(ptriangle_type, ptriangle_t, H5T_NATIVE_INT, ib3);
  H5Cstruct_insert(ptriangle_type, ptriangle_t, H5T_NATIVE_DOUBLE, p);
  H5Cstruct_insert(ptriangle_type, ptriangle_t, H5T_NATIVE_INT, group);

  lua_pushinteger(L, ptriangle_type);
  return 1;
}

static int lua_H5Cptriangle_make_table(lua_State *L)
{
  ptriangle_t *ptriangle = (ptriangle_t *) lua_touserdata(L, 1);
  unsigned int n_ptriangle = (unsigned int) lua_tointeger(L, 2);

  lua_newtable(L);
  unsigned int n;
  for (n = 0; n < n_ptriangle; ++n)
  {
    lua_pushinteger(L, n);
    lua_newtable(L);

    H5Ctable_insert(L, ptriangle, ib1);
    H5Ctable_insert(L, ptriangle, ib2);
    H5Ctable_insert(L, ptriangle, ib3);
    H5Ctable_insert(L, ptriangle, p);
    H5Ctable_insert(L, ptriangle, group);

    lua_rawset(L, -3);

    ptriangle++;
  }
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_pushinteger(L, n_ptriangle);
  lua_rawset(L, -3);

  return 1;
}

static int lua_H5Cptriangle_make_raw(lua_State *L)
{
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_rawget(L, 1);
  unsigned int n_ptriangle = (unsigned int) lua_tointeger(L, -1);
  lua_pop(L, 1);

  ptriangle_t *ptriangle = (ptriangle_t *) lua_newuserdata(L, sizeof(ptriangle_t)*n_ptriangle);
  unsigned int n;
  for(n = 0; n < n_ptriangle; ++n)
  {
    lua_pushinteger(L, n);
    lua_rawget(L, 1);

    H5Cset_struct_field(L, ptriangle, ib1);
    H5Cset_struct_field(L, ptriangle, ib2);
    H5Cset_struct_field(L, ptriangle, ib3);
    H5Cset_struct_field(L, ptriangle, p);
    H5Cset_struct_field(L, ptriangle, group);

    lua_pop(L, 1);

    ptriangle++;
  }
  return 1;
}

/************************************************
 *                  lptimer
 ************************************************/

static int lua_H5Cregister_lptimer(lua_State *L)
{
  hid_t lptimer_type = H5Tcreate(H5T_COMPOUND, sizeof(lptimer_t));
  H5Cstruct_insert(lptimer_type, lptimer_t, H5T_NATIVE_DOUBLE, t);
  H5Cstruct_insert(lptimer_type, lptimer_t, H5T_NATIVE_DOUBLE, t0);
  H5Cstruct_insert(lptimer_type, lptimer_t, H5T_NATIVE_DOUBLE, te);
  H5Cstruct_insert(lptimer_type, lptimer_t, H5T_NATIVE_DOUBLE, dt);
  H5Cstruct_insert(lptimer_type, lptimer_t, H5T_NATIVE_DOUBLE, ts);
  H5Cstruct_insert(lptimer_type, lptimer_t, H5T_NATIVE_DOUBLE, dts);
  H5Cstruct_insert(lptimer_type, lptimer_t, H5T_NATIVE_INT, n);
  H5Cstruct_insert(lptimer_type, lptimer_t, H5T_NATIVE_INT, ns);

  lua_pushinteger(L, lptimer_type);
  return 1;
}

static int lua_H5Clptimer_make_table(lua_State *L)
{
  lptimer_t *lptimer = (lptimer_t *) lua_touserdata(L, 1);
  unsigned int n_lptimer = (unsigned int) lua_tointeger(L, 2);

  lua_newtable(L);
  unsigned int n;
  for (n = 0; n < n_lptimer; ++n)
  {
    lua_pushinteger(L, n);
    lua_newtable(L);

    H5Ctable_insert(L, lptimer, t);
    H5Ctable_insert(L, lptimer, t0);
    H5Ctable_insert(L, lptimer, te);
    H5Ctable_insert(L, lptimer, dt);
    H5Ctable_insert(L, lptimer, ts);
    H5Ctable_insert(L, lptimer, dts);
    H5Ctable_insert(L, lptimer, n);
    H5Ctable_insert(L, lptimer, ns);

    lua_rawset(L, -3);

    lptimer++;
  }
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_pushinteger(L, n_lptimer);
  lua_rawset(L, -3);

  return 1;
}

static int lua_H5Clptimer_make_raw(lua_State *L)
{
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_rawget(L, 1);
  unsigned int n_lptimer = (unsigned int) lua_tointeger(L, -1);
  lua_pop(L, 1);

  lptimer_t *lptimer = (lptimer_t *) lua_newuserdata(L, sizeof(lptimer_t)*n_lptimer);
  unsigned int n;
  for(n = 0; n < n_lptimer; ++n)
  {
    lua_pushinteger(L, n);
    lua_rawget(L, 1);

    H5Cset_struct_field(L, lptimer, t);
    H5Cset_struct_field(L, lptimer, t0);
    H5Cset_struct_field(L, lptimer, te);
    H5Cset_struct_field(L, lptimer, dt);
    H5Cset_struct_field(L, lptimer, ts);
    H5Cset_struct_field(L, lptimer, dts);
    H5Cset_struct_field(L, lptimer, n);
    H5Cset_struct_field(L, lptimer, ns);

    lua_pop(L, 1);

    lptimer++;
  }
  return 1;
}


/************************************************
 *                  sph_props
 ************************************************/

static int lua_H5Cregister_sph_props(lua_State *L)
{
  hid_t sph_props_type = H5Tcreate(H5T_COMPOUND, sizeof(sph_props_t));
  H5Cstruct_insert(sph_props_type, sph_props_t, H5T_NATIVE_DOUBLE, rho0);
  H5Cstruct_insert(sph_props_type, sph_props_t, H5T_NATIVE_DOUBLE, c0);
  H5Cstruct_insert(sph_props_type, sph_props_t, H5T_NATIVE_DOUBLE, c2);
  H5Cstruct_insert(sph_props_type, sph_props_t, H5T_NATIVE_DOUBLE, gamma);
  H5Cstruct_insert(sph_props_type, sph_props_t, H5T_NATIVE_DOUBLE, eps);
  H5Cstruct_insert(sph_props_type, sph_props_t, H5T_NATIVE_DOUBLE, alpha);

  lua_pushinteger(L, sph_props_type);
  return 1;
}

static int lua_H5Csph_props_make_table(lua_State *L)
{
  sph_props_t *sph_props = (sph_props_t *) lua_touserdata(L, 1);
  unsigned int n_sph_props = (unsigned int) lua_tointeger(L, 2);

  lua_newtable(L);
  unsigned int n;
  for (n = 0; n < n_sph_props; ++n)
  {
    lua_pushinteger(L, n);
    lua_newtable(L);

    H5Ctable_insert(L, sph_props, rho0);
    H5Ctable_insert(L, sph_props, c0);
    H5Ctable_insert(L, sph_props, c2);
    H5Ctable_insert(L, sph_props, gamma);
    H5Ctable_insert(L, sph_props, eps);
    H5Ctable_insert(L, sph_props, alpha);

    lua_rawset(L, -3);

    sph_props++;
  }
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_pushinteger(L, n_sph_props);
  lua_rawset(L, -3);

  return 1;
}

static int lua_H5Csph_props_make_raw(lua_State *L)
{
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_rawget(L, 1);
  unsigned int n_sph_props = (unsigned int) lua_tointeger(L, -1);
  lua_pop(L, 1);

  sph_props_t *sph_props = (sph_props_t *) lua_newuserdata(L, sizeof(sph_props_t)*n_sph_props);
  unsigned int n;
  for(n = 0; n < n_sph_props; ++n)
  {
    lua_pushinteger(L, n);
    lua_rawget(L, 1);

    H5Cset_struct_field(L, sph_props, rho0);
    H5Cset_struct_field(L, sph_props, c0);
    H5Cset_struct_field(L, sph_props, c2);
    H5Cset_struct_field(L, sph_props, gamma);
    H5Cset_struct_field(L, sph_props, eps);
    H5Cset_struct_field(L, sph_props, alpha);

    lua_pop(L, 1);

    sph_props++;
  }
  return 1;
}

   
/************************************************
 *                  sbb
 ************************************************/

static int lua_H5Cregister_sbb(lua_State *L)
{
  hid_t vec3d_type = H5Tcreate(H5T_COMPOUND, sizeof(vec3d_t));
  H5Cstruct_insert(vec3d_type, vec3d_t, H5T_NATIVE_DOUBLE, x);
  H5Cstruct_insert(vec3d_type, vec3d_t, H5T_NATIVE_DOUBLE, y);
  H5Cstruct_insert(vec3d_type, vec3d_t, H5T_NATIVE_DOUBLE, z);

  hid_t sbb_type = H5Tcreate(H5T_COMPOUND, sizeof(sbb_t));
  H5Cstruct_insert(sbb_type, sbb_t, vec3d_type, c);
  H5Cstruct_insert(sbb_type, sbb_t, vec3d_type, c0);
  H5Cstruct_insert(sbb_type, sbb_t, H5T_NATIVE_DOUBLE, r);
  H5Cstruct_insert(sbb_type, sbb_t, H5T_NATIVE_INT, body_indx);
  H5Cstruct_insert(sbb_type, sbb_t, H5T_NATIVE_INT, group_id);

  lua_pushinteger(L, sbb_type);
  return 1;
}

static int lua_H5Csbb_make_table(lua_State *L)
{
  sbb_t *sbb = (sbb_t *) lua_touserdata(L, 1);
  unsigned int n_sbb = (unsigned int) lua_tointeger(L, 2);

  lua_newtable(L);
  unsigned int n;
  for (n = 0; n < n_sbb; ++n)
  {
    lua_pushinteger(L, n);
    lua_newtable(L);

    H5Ctable_insert_vec3d(L, sbb, c);
    H5Ctable_insert_vec3d(L, sbb, c0);
    H5Ctable_insert(L, sbb, r);
    H5Ctable_insert(L, sbb, body_indx);
    H5Ctable_insert(L, sbb, group_id);

    lua_rawset(L, -3);

    sbb++;
  }
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_pushinteger(L, n_sbb);
  lua_rawset(L, -3);

  return 1;
}

static int lua_H5Csbb_make_raw(lua_State *L)
{
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_rawget(L, 1);
  unsigned int n_sbb = (unsigned int) lua_tointeger(L, -1);
  lua_pop(L, 1);

  sbb_t *sbb = (sbb_t *) lua_newuserdata(L, sizeof(sbb_t)*n_sbb);
  unsigned int n;
  for(n = 0; n < n_sbb; ++n)
  {
    lua_pushinteger(L, n);
    lua_rawget(L, 1);

    H5Cset_struct_field_vec3d(L, sbb, c);
    H5Cset_struct_field_vec3d(L, sbb, c0);
    H5Cset_struct_field(L, sbb, r);
    H5Cset_struct_field(L, sbb, body_indx);
    H5Cset_struct_field(L, sbb, group_id);

    lua_pop(L, 1);

    sbb++;
  }
  return 1;
}

/************************************************
 *                  gravity
 ************************************************/

typedef struct glbl_acc_s
{
  double x;
  double y;
  double z;
} glbl_acc_t;

static int lua_H5Cregister_glbl_acc(lua_State *L)
{
  hid_t glbl_acc_type = H5Tcreate(H5T_COMPOUND, sizeof(glbl_acc_t));
  H5Cstruct_insert(glbl_acc_type, glbl_acc_t, H5T_NATIVE_DOUBLE, x);
  H5Cstruct_insert(glbl_acc_type, glbl_acc_t, H5T_NATIVE_DOUBLE, y);
  H5Cstruct_insert(glbl_acc_type, glbl_acc_t, H5T_NATIVE_DOUBLE, z);

  lua_pushinteger(L, glbl_acc_type);
  return 1;
}

static int lua_H5Cglbl_acc_make_table(lua_State *L)
{
  glbl_acc_t *glbl_acc = (glbl_acc_t *) lua_touserdata(L, 1);
  unsigned int n_glbl_acc = (unsigned int) lua_tointeger(L, 2);

  lua_newtable(L);
  unsigned int n;
  for (n = 0; n < n_glbl_acc; ++n)
  {
    lua_pushinteger(L, n);
    lua_newtable(L);

    H5Ctable_insert(L, glbl_acc, x);
    H5Ctable_insert(L, glbl_acc, y);
    H5Ctable_insert(L, glbl_acc, z);

    lua_rawset(L, -3);

    glbl_acc++;
  }
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_pushinteger(L, n_glbl_acc);
  lua_rawset(L, -3);

  return 1;
}

static int lua_H5Cglbl_acc_make_raw(lua_State *L)
{
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_rawget(L, 1);
  unsigned int n_glbl_acc = (unsigned int) lua_tointeger(L, -1);
  lua_pop(L, 1);

  glbl_acc_t *glbl_acc = (glbl_acc_t *) lua_newuserdata(L, sizeof(glbl_acc_t)*n_glbl_acc);
  unsigned int n;
  for(n = 0; n < n_glbl_acc; ++n)
  {
    lua_pushinteger(L, n);
    lua_rawget(L, 1);

    H5Cset_struct_field(L, glbl_acc, x);
    H5Cset_struct_field(L, glbl_acc, y);
    H5Cset_struct_field(L, glbl_acc, z);

    lua_pop(L, 1);

    glbl_acc++;
  }
  return 1;
}

/************************************************
 *                  settings
 ************************************************/

#ifdef SETTINGS_TODO

static int lua_H5Cregister_settings(lua_State *L)
{
  hid_t settings_type = H5Tcreate(H5T_COMPOUND, sizeof(settings_t));
  H5Cstruct_insert(settings_type, settings_t, H5T_NATIVE_INT, flags);
  H5Cstruct_insert(settings_type, settings_t, H5T_NATIVE_INT, cdmethod);
  H5Cstruct_insert(settings_type, settings_t, H5T_NATIVE_DOUBLE,
                   cdm_dx_min);
  H5Cstruct_insert(settings_type, settings_t, H5T_NATIVE_DOUBLE,
                   cd_sbb_tau0);
  H5Cstruct_insert(settings_type, settings_t, H5T_NATIVE_DOUBLE,
                   cd_sbb_r0);
  H5Cstruct_insert(settings_type, settings_t, H5T_NATIVE_INT,
                   run_from_name);
  H5Cstruct_insert(settings_type, settings_t, H5T_NATIVE_INT, luafile);

  lua_pushinteger(L, settings_type);
  return 1;
}

static int lua_H5Csettings_make_table(lua_State *L)
{
  settings_t *settings = (settings_t *) lua_touserdata(L, 1);
  unsigned int n_settings = (unsigned int) lua_tointeger(L, 2);

  lua_newtable(L);
  unsigned int n;
  for (n = 0; n < n_settings; ++n)
  {
    lua_pushinteger(L, n);
    lua_newtable(L);

    H5Ctable_insert(L, settings, flags);
    H5Ctable_insert(L, settings, cdmethod);
    H5Ctable_insert(L, settings, cdm_dx_min);
    H5Ctable_insert(L, settings, run_from_name);
    H5Ctable_insert(L, settings, luafile);

    lua_rawset(L, -3);

    settings++;
  }
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_pushinteger(L, n_settings);
  lua_rawset(L, -3);

  return 1;
}

static int lua_H5Csettings_make_raw(lua_State *L)
{
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_rawget(L, 1);
  unsigned int n_settings = (unsigned int) lua_tointeger(L, -1);
  lua_pop(L, 1);

  settings_t *settings = (settings_t *) lua_newuserdata(L, sizeof(settings_t)*n_settings);
  unsigned int n;
  for(n = 0; n < n_settings; ++n)
  {
    lua_pushinteger(L, n);
    lua_rawget(L, 1);

    H5Cset_struct_field(L, settings, flags);
    H5Cset_struct_field(L, settings, cdmethod);
    H5Cset_struct_field(L, settings, cdm_dx_min);
    H5Cset_struct_field(L, settings, run_from_name);
    H5Cset_struct_field(L, settings, luafile);

    lua_pop(L, 1);

    settings++;
  }
  return 1;
}

#endif

/************************************************
 *                  sbb_params
 ************************************************/
#ifdef AAA

static int lua_H5Cregister_sbb_params(lua_State *L)
{
  hid_t sbb_params_type = H5Tcreate(H5T_COMPOUND, sizeof(sbb_params_t));
  H5Cstruct_insert(sbb_params_type, sbb_params_t, H5T_NATIVE_DOUBLE, drmax);
  H5Cstruct_insert(sbb_params_type, sbb_params_t, H5T_NATIVE_DOUBLE, drmin);
  H5Cstruct_insert(sbb_params_type, sbb_params_t, H5T_NATIVE_DOUBLE, tau);
  lua_pushinteger(L, sbb_params_type);
  return 1;
}

static int lua_H5Csbb_params_make_table(lua_State *L)
{
  settings_t *settings = (settings_t *) lua_touserdata(L, 1);
  unsigned int n_settings = (unsigned int) lua_tointeger(L, 2);

  lua_newtable(L);
  unsigned int n;
  for (n = 0; n < n_settings; ++n)
  {
    lua_pushinteger(L, n);
    lua_newtable(L);

    H5Ctable_insert(L, settings, flags);
    H5Ctable_insert(L, settings, cdmethod);
    H5Ctable_insert(L, settings, cdm_dx_min);
    H5Ctable_insert(L, settings, run_from_name);
    H5Ctable_insert(L, settings, luafile);

    lua_rawset(L, -3);

    settings++;
  }
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_pushinteger(L, n_settings);
  lua_rawset(L, -3);

  return 1;
}

static int lua_H5Csettings_make_raw(lua_State *L)
{
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_rawget(L, 1);
  unsigned int n_settings = (unsigned int) lua_tointeger(L, -1);
  lua_pop(L, 1);

  settings_t *settings = (settings_t *) lua_newuserdata(L, sizeof(settings_t)*n_settings);
  unsigned int n;
  for(n = 0; n < n_settings; ++n)
  {
    lua_pushinteger(L, n);
    lua_rawget(L, 1);

    H5Cset_struct_field(L, settings, flags);
    H5Cset_struct_field(L, settings, cdmethod);
    H5Cset_struct_field(L, settings, cdm_dx_min);
    H5Cset_struct_field(L, settings, run_from_name);
    H5Cset_struct_field(L, settings, luafile);

    lua_pop(L, 1);

    settings++;
  }
  return 1;
}

#endif

/************************************************
 *                  conhash_comp
 ************************************************/

static int lua_H5Cregister_conhash_comp(lua_State *L)
{
  hid_t conhash_comp_type = H5Tcreate(H5T_COMPOUND, sizeof(conhash_comp_t));
  H5Cstruct_insert(conhash_comp_type, conhash_comp_t, H5T_NATIVE_DOUBLE, last_ulm);
  H5Cstruct_insert(conhash_comp_type, conhash_comp_t, H5T_NATIVE_INT, capacity);
  H5Cstruct_insert(conhash_comp_type, conhash_comp_t, H5T_NATIVE_INT, ULMT);
  H5Cstruct_insert(conhash_comp_type, conhash_comp_t, H5T_NATIVE_INT, nprobes);
  H5Cstruct_insert(conhash_comp_type, conhash_comp_t, H5T_NATIVE_INT, ncleans);
  H5Cstruct_insert(conhash_comp_type, conhash_comp_t, H5T_NATIVE_INT, bsave);

  lua_pushinteger(L, conhash_comp_type);
  return 1;
}

static int lua_H5Cconhash_comp_make_table(lua_State *L)
{
  conhash_comp_t *conhash_comp = (conhash_comp_t *) lua_touserdata(L, 1);
  unsigned int n_conhash_comp = (unsigned int) lua_tointeger(L, 2);

  lua_newtable(L);
  unsigned int n;
  for (n = 0; n < n_conhash_comp; ++n)
  {
    lua_pushinteger(L, n);
    lua_newtable(L);

    H5Ctable_insert(L, conhash_comp, last_ulm);
    H5Ctable_insert(L, conhash_comp, capacity);
    H5Ctable_insert(L, conhash_comp, ULMT);
    H5Ctable_insert(L, conhash_comp, nprobes);
    H5Ctable_insert(L, conhash_comp, ncleans);
    H5Ctable_insert(L, conhash_comp, bsave);

    lua_rawset(L, -3);

    conhash_comp++;
  }
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_pushinteger(L, n_conhash_comp);
  lua_rawset(L, -3);

  return 1;
}

static int lua_H5Cconhash_comp_make_raw(lua_State *L)
{
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_rawget(L, 1);
  unsigned int n_conhash_comp = (unsigned int) lua_tointeger(L, -1);
  lua_pop(L, 1);

  conhash_comp_t *conhash_comp = (conhash_comp_t *) lua_newuserdata(L, sizeof(conhash_comp_t)*n_conhash_comp);
  unsigned int n;
  for(n = 0; n < n_conhash_comp; ++n)
  {
    lua_pushinteger(L, n);
    lua_rawget(L, 1);

    H5Cset_struct_field(L, conhash_comp, last_ulm);
    H5Cset_struct_field(L, conhash_comp, capacity);
    H5Cset_struct_field(L, conhash_comp, ULMT);
    H5Cset_struct_field(L, conhash_comp, nprobes);
    H5Cset_struct_field(L, conhash_comp, ncleans);
    H5Cset_struct_field(L, conhash_comp, bsave);

    lua_pop(L, 1);

    conhash_comp++;
  }
  return 1;
}

/************************************************
 *                  bc
 ************************************************/

static int lua_H5Cregister_bc(lua_State *L)
{
  hid_t bc_type = H5Tcreate(H5T_COMPOUND, sizeof(bc_t));
  H5Cstruct_insert(bc_type, bc_t, H5T_NATIVE_DOUBLE, zout_vel);
  H5Cstruct_insert(bc_type, bc_t, H5T_NATIVE_INT, material_id);
  H5Cstruct_insert(bc_type, bc_t, H5T_NATIVE_INT, flags);

  lua_pushinteger(L, bc_type);
  return 1;
}

static int lua_H5Cbc_make_table(lua_State *L)
{
  bc_t *bc = (bc_t *) lua_touserdata(L, 1);
  unsigned int n_bc = (unsigned int) lua_tointeger(L, 2);

  lua_newtable(L);
  unsigned int n;
  for (n = 0; n < n_bc; ++n)
  {
    lua_pushinteger(L, n);
    lua_newtable(L);

    H5Ctable_insert(L, bc, zout_vel);
    H5Ctable_insert(L, bc, material_id);
    H5Ctable_insert(L, bc, flags);

    lua_rawset(L, -3);

    bc++;
  }
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_pushinteger(L, n_bc);
  lua_rawset(L, -3);

  return 1;
}

static int lua_H5Cbc_make_raw(lua_State *L)
{
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_rawget(L, 1);
  unsigned int n_bc = (unsigned int) lua_tointeger(L, -1);
  lua_pop(L, 1);

  bc_t *bc = (bc_t *) lua_newuserdata(L, sizeof(bc_t)*n_bc);
  unsigned int n;
  for(n = 0; n < n_bc; ++n)
  {
    lua_pushinteger(L, n);
    lua_rawget(L, 1);

    H5Cset_struct_field(L, bc, zout_vel);
    H5Cset_struct_field(L, bc, material_id);
    H5Cset_struct_field(L, bc, flags);

    lua_pop(L, 1);

    bc++;
  }
  return 1;
}

/************************************************
 *                  box
 ************************************************/

static int lua_H5Cregister_box(lua_State *L)
{
  hid_t vec3d_type = H5Tcreate(H5T_COMPOUND, sizeof(vec3d_t));
  H5Cstruct_insert(vec3d_type, vec3d_t, H5T_NATIVE_DOUBLE, x);
  H5Cstruct_insert(vec3d_type, vec3d_t, H5T_NATIVE_DOUBLE, y);
  H5Cstruct_insert(vec3d_type, vec3d_t, H5T_NATIVE_DOUBLE, z);

  hid_t box_type = H5Tcreate(H5T_COMPOUND, sizeof(box_t));
  H5Cstruct_insert(box_type, box_t, vec3d_type, fcorner);
  H5Cstruct_insert(box_type, box_t, H5T_NATIVE_INT, xid);
  H5Cstruct_insert(box_type, box_t, H5T_NATIVE_INT, yid);
  H5Cstruct_insert(box_type, box_t, H5T_NATIVE_INT, zid);

  lua_pushinteger(L, box_type);
  return 1;
}

static int lua_H5Cbox_make_table(lua_State *L)
{
  box_t *box = (box_t *) lua_touserdata(L, 1);
  unsigned int n_box = (unsigned int) lua_tointeger(L, 2);

  lua_newtable(L);
  unsigned int n;
  for (n = 0; n < n_box; ++n)
  {
    lua_pushinteger(L, n);
    lua_newtable(L);

    H5Ctable_insert_vec3d(L, box, fcorner);
    H5Ctable_insert(L, box, xid);
    H5Ctable_insert(L, box, yid);
    H5Ctable_insert(L, box, zid);

    lua_rawset(L, -3);

    box++;
  }
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_pushinteger(L, n_box);
  lua_rawset(L, -3);

  return 1;
}

static int lua_H5Cbox_make_raw(lua_State *L)
{
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_rawget(L, 1);
  unsigned int n_box = (unsigned int) lua_tointeger(L, -1);
  lua_pop(L, 1);

  box_t *box = (box_t *) lua_newuserdata(L, sizeof(box_t)*n_box);
  unsigned int n;
  for(n = 0; n < n_box; ++n)
  {
    lua_pushinteger(L, n);
    lua_rawget(L, 1);

    H5Cset_struct_field_vec3d(L, box, fcorner);
    H5Cset_struct_field(L, box, xid);
    H5Cset_struct_field(L, box, yid);
    H5Cset_struct_field(L, box, zid);

    lua_pop(L, 1);

    box++;
  }
  return 1;
}

/************************************************
 *                  group names
 ************************************************/

static int lua_H5Cregister_groups(lua_State *L)
{
  hid_t string_type = H5Tcopy(H5T_C_S1);
  H5Tset_size(string_type, H5T_VARIABLE);

  lua_pushinteger(L, string_type);
  return 1;
}

static int lua_H5Cgroups_make_table(lua_State *L)
{
  char **names = (char **) lua_touserdata(L, 1);
  unsigned int n_names = (unsigned int) lua_tointeger(L, 2);

  lua_newtable(L);
  unsigned int n;
  for (n = 0; n < n_names; ++n)
  {
    lua_pushinteger(L, n);
    lua_pushstring(L, *names);

    lua_rawset(L, -3);

    names++;
  }
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_pushinteger(L, n_names);
  lua_rawset(L, -3);

  return 1;
}

static int lua_H5Cgroups_make_raw(lua_State *L)
{
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_rawget(L, 1);
  unsigned int n_names = (unsigned int) lua_tointeger(L, -1);
  lua_pop(L, 1);

  char **names = (char **) lua_newuserdata(L, sizeof(char *)*n_names);
  unsigned int n;
  for(n = 0; n < n_names; ++n)
  {
    lua_pushinteger(L, n);
    lua_rawget(L, 1);
    *names = strdup( lua_tostring(L, -1) );
    lua_pop(L, 1);

    names++;
  }
  return 1;
}

/************************************************
 *             group interaction
 ************************************************/

static int lua_H5Cregister_intrx(lua_State *L)
{
  hid_t int_type = H5Tcopy(H5T_NATIVE_INT);
  //H5Tset_size(int_type, 10);

  lua_pushinteger(L, int_type);
  return 1;
}

static int lua_H5Cintrx_make_table(lua_State *L)
{
  int *pintrx = (int *) lua_touserdata(L, 1);
  unsigned int N = (unsigned int) lua_tointeger(L, 2);
  unsigned int n;

  lua_newtable(L);
  for ( n = 0; n < N; ++n )
  {
    lua_pushinteger(L, n);
    lua_pushinteger(L, pintrx[n]);
    lua_rawset(L, -3);
  }

  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_pushinteger(L, N);
  lua_rawset(L, -3);

  return 1;
}

static int lua_H5Cintrx_make_raw(lua_State *L)
{
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_rawget(L, 1);
  unsigned int N = (unsigned int) lua_tointeger(L, -1);
  lua_pop(L, 1);

  int *pintrx = (int*) lua_newuserdata(L, sizeof(int)*N);
  unsigned int n;
  for(n = 0; n < N; ++n)
  {
    lua_pushinteger(L, n);
    lua_rawget(L, 1);
    pintrx[n] = lua_tointeger(L, -1);
    lua_pop(L, 1);
  }
  return 1;
}

/************************************************
 *               material names
 ************************************************/

static int lua_H5Cregister_material_names(lua_State *L)
{
  hid_t string_type = H5Tcopy(H5T_C_S1);
  H5Tset_size(string_type, H5T_VARIABLE);

  lua_pushinteger(L, string_type);
  return 1;
}

static int lua_H5Cmaterial_names_make_table(lua_State *L)
{
  char **names = (char **) lua_touserdata(L, 1);
  unsigned int n_names = (unsigned int) lua_tointeger(L, 2);

  lua_newtable(L);
  unsigned int n;
  for (n = 0; n < n_names; ++n)
  {
    lua_pushinteger(L, n);
    lua_pushstring(L, *names);

    lua_rawset(L, -3);

    names++;
  }
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_pushinteger(L, n_names);
  lua_rawset(L, -3);

  return 1;
}

static int lua_H5Cmaterial_names_make_raw(lua_State *L)
{
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_rawget(L, 1);
  unsigned int n_names = (unsigned int) lua_tointeger(L, -1);
  lua_pop(L, 1);

  char **names = (char **) lua_newuserdata(L, sizeof(char *)*n_names);
  unsigned int n;
  for(n = 0; n < n_names; ++n)
  {
    lua_pushinteger(L, n);
    lua_rawget(L, 1);
    *names = strdup( lua_tostring(L, -1) );
    lua_pop(L, 1);

    names++;
  }
  return 1;
}

/************************************************
 *                  drag
 ************************************************/

static int lua_H5Cregister_drag(lua_State *L)
{
  hid_t drag_type = H5Tcreate(H5T_COMPOUND, sizeof(drag_t));
  H5Cstruct_insert(drag_type, drag_t, H5T_NATIVE_DOUBLE, Cd);
  H5Cstruct_insert(drag_type, drag_t, H5T_NATIVE_DOUBLE, mu);
  H5Cstruct_insert(drag_type, drag_t, H5T_NATIVE_INT, isdrag);
  H5Cstruct_insert(drag_type, drag_t, H5T_NATIVE_INT, isuniform);

  lua_pushinteger(L, drag_type);
  return 1;
}

static int lua_H5Cdrag_make_table(lua_State *L)
{
  drag_t *drag = (drag_t *) lua_touserdata(L, 1);
  unsigned int n_drag = (unsigned int) lua_tointeger(L, 2);

  lua_newtable(L);
  unsigned int n;
  for (n = 0; n < n_drag; ++n)
  {
    lua_pushinteger(L, n);
    lua_newtable(L);

    H5Ctable_insert(L, drag, Cd);
    H5Ctable_insert(L, drag, mu);
    H5Ctable_insert(L, drag, isdrag);
    H5Ctable_insert(L, drag, isuniform);

    lua_rawset(L, -3);

    drag++;
  }
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_pushinteger(L, n_drag);
  lua_rawset(L, -3);

  return 1;
}

static int lua_H5Cdrag_make_raw(lua_State *L)
{
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_rawget(L, 1);
  unsigned int n_drag = (unsigned int) lua_tointeger(L, -1);
  lua_pop(L, 1);

  drag_t *drag = (drag_t *) lua_newuserdata(L, sizeof(drag_t)*n_drag);
  unsigned int n;
  for(n = 0; n < n_drag; ++n)
  {
    lua_pushinteger(L, n);
    lua_rawget(L, 1);

    H5Cset_struct_field(L, drag, Cd);
    H5Cset_struct_field(L, drag, mu);
    H5Cset_struct_field(L, drag, isdrag);
    H5Cset_struct_field(L, drag, isuniform);

    lua_pop(L, 1);

    drag++;
  }
  return 1;
}

/************************************************
 *               cmprops
 ************************************************/

static int lua_H5Cregister_cmprops(lua_State *L)
{
  hid_t cmprops_type = H5Tcreate(H5T_COMPOUND, sizeof(cmprops_t));
  H5Cstruct_insert(cmprops_type, cmprops_t, H5T_NATIVE_DOUBLE, H);
  H5Cstruct_insert(cmprops_type, cmprops_t, H5T_NATIVE_DOUBLE, gamma);
  H5Cstruct_insert(cmprops_type, cmprops_t, H5T_NATIVE_DOUBLE, ka);
  H5Cstruct_insert(cmprops_type, cmprops_t, H5T_NATIVE_DOUBLE, mu);
  H5Cstruct_insert(cmprops_type, cmprops_t, H5T_NATIVE_DOUBLE, alpha_t);
  H5Cstruct_insert(cmprops_type, cmprops_t, H5T_NATIVE_DOUBLE, CR);
  H5Cstruct_insert(cmprops_type, cmprops_t, H5T_NATIVE_DOUBLE, vR);
  H5Cstruct_insert(cmprops_type, cmprops_t, H5T_NATIVE_DOUBLE, Skni);
  H5Cstruct_insert(cmprops_type, cmprops_t, H5T_NATIVE_INT, mati1);
  H5Cstruct_insert(cmprops_type, cmprops_t, H5T_NATIVE_INT, mati2);

  lua_pushinteger(L, cmprops_type);
  return 1;
}

static int lua_H5Ccmprops_make_table(lua_State *L)
{
  cmprops_t *cmprops = (cmprops_t *) lua_touserdata(L, 1);
  unsigned int n_cmprops = (unsigned int) lua_tointeger(L, 2);

  lua_newtable(L);
  unsigned int n;
  for (n = 0; n < n_cmprops; ++n)
  {
    lua_pushinteger(L, n);
    lua_newtable(L);

    H5Ctable_insert(L, cmprops, H);
    H5Ctable_insert(L, cmprops, gamma);
    H5Ctable_insert(L, cmprops, ka);
    H5Ctable_insert(L, cmprops, mu);
    H5Ctable_insert(L, cmprops, alpha_t);
    H5Ctable_insert(L, cmprops, CR);
    H5Ctable_insert(L, cmprops, vR);
    H5Ctable_insert(L, cmprops, Skni);
    H5Ctable_insert(L, cmprops, mati1);
    H5Ctable_insert(L, cmprops, mati2);

    lua_rawset(L, -3);

    cmprops++;
  }
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_pushinteger(L, n_cmprops);
  lua_rawset(L, -3);

  return 1;
}

static int lua_H5Ccmprops_make_raw(lua_State *L)
{
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_rawget(L, 1);
  unsigned int n_cmprops = (unsigned int) lua_tointeger(L, -1);
  lua_pop(L, 1);

  cmprops_t *cmprops = (cmprops_t *) lua_newuserdata(L, sizeof(cmprops_t)*n_cmprops);
  unsigned int n;
  for(n = 0; n < n_cmprops; ++n)
  {
    lua_pushinteger(L, n);
    lua_rawget(L, 1);

    H5Cset_struct_field(L, cmprops, H);
    H5Cset_struct_field(L, cmprops, gamma);
    H5Cset_struct_field(L, cmprops, ka);
    H5Cset_struct_field(L, cmprops, mu);
    H5Cset_struct_field(L, cmprops, alpha_t);
    H5Cset_struct_field(L, cmprops, CR);
    H5Cset_struct_field(L, cmprops, vR);
    H5Cset_struct_field(L, cmprops, Skni);
    H5Cset_struct_field(L, cmprops, mati1);
    H5Cset_struct_field(L, cmprops, mati2);

    lua_pop(L, 1);

    cmprops++;
  }
  return 1;
}

/************************************************
 *                  Contact List
 ************************************************/

static int lua_H5Cregister_contact(lua_State *L)
{
  hid_t vec3d_type = H5Tcreate(H5T_COMPOUND, sizeof(vec3d_t));
  H5Cstruct_insert(vec3d_type, vec3d_t, H5T_NATIVE_DOUBLE, x);
  H5Cstruct_insert(vec3d_type, vec3d_t, H5T_NATIVE_DOUBLE, y);
  H5Cstruct_insert(vec3d_type, vec3d_t, H5T_NATIVE_DOUBLE, z);

  hid_t cl_type = H5Tcreate(H5T_COMPOUND, sizeof(contact_t));

  H5Cstruct_insert(cl_type, contact_t, vec3d_type, ftau_e);
  H5Cstruct_insert(cl_type, contact_t, vec3d_type, ftau_i);
  H5Cstruct_insert(cl_type, contact_t, vec3d_type, pc);
  H5Cstruct_insert(cl_type, contact_t, vec3d_type, f);
  H5Cstruct_insert(cl_type, contact_t, H5T_NATIVE_DOUBLE, delta);
  H5Cstruct_insert(cl_type, contact_t, H5T_NATIVE_INT, id1);
  H5Cstruct_insert(cl_type, contact_t, H5T_NATIVE_INT, id2);

  lua_pushinteger(L, cl_type);
  return 1;
}

static int lua_H5Ccontact_make_table(lua_State *L)
{
  contact_t *cl = (contact_t *) lua_touserdata(L, 1);
  unsigned int n_contacts = (unsigned int) lua_tointeger(L, 2);

  lua_newtable(L);
  unsigned int n;
  for (n = 0; n < n_contacts; ++n)
  {
    lua_pushinteger(L, n);
    lua_newtable(L);

    H5Ctable_insert_vec3d( L, cl, ftau_e );
    H5Ctable_insert_vec3d( L, cl, ftau_i );
    H5Ctable_insert_vec3d( L, cl, pc );
    H5Ctable_insert_vec3d( L, cl, f );

    H5Ctable_insert( L, cl, delta );
    H5Ctable_insert( L, cl, id1 );
    H5Ctable_insert( L, cl, id2 );

    lua_rawset(L, -3);

    cl++;
  }

  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_pushinteger(L, n_contacts);
  lua_rawset(L, -3);

  return 1;
}

static int lua_H5Ccontact_make_raw(lua_State *L)
{
  lua_pushstring(L, ARRAY_SIZE_KEY);
  lua_rawget(L, 1);
  unsigned int n_contacts = (unsigned int) lua_tointeger(L, -1);
  lua_pop(L, 1);

  contact_t *cl = (contact_t *)
    lua_newuserdata( L, sizeof(contact_t)*n_contacts);

  unsigned int n;
  for(n = 0; n < n_contacts; ++n)
  {
    lua_pushinteger(L, n);
    lua_rawget(L, 1);

    H5Cset_struct_field_vec3d( L, cl, ftau_e );
    H5Cset_struct_field_vec3d( L, cl, ftau_i );
    H5Cset_struct_field_vec3d( L, cl, pc );
    H5Cset_struct_field_vec3d( L, cl, f );

    H5Cset_struct_field( L, cl, delta );
    H5Cset_struct_field( L, cl, id1 );
    H5Cset_struct_field( L, cl, id2 );

    lua_pop(L, 1);

    cl++;
  }
  return 1;
}

/************************************************
 *              function registration
 ************************************************/

static const struct luaL_Reg hdf5lib [] = {
  {"H5Fopen", lua_H5Fopen},
  {"H5Fclose", lua_H5Fclose},
  {"H5Dopen", lua_H5Dopen},
  {"H5Dclose", lua_H5Dclose},
  {"H5Tclose", lua_H5Tclose},
  {"H5Lexists", lua_H5Lexists},
  {"H5Cread", lua_H5Cread},
  {"H5Cwrite", lua_H5Cwrite},
  {"H5Cget_dims", lua_H5Cget_dims},

  {"H5Cget_H5P_DEFAULT", lua_H5Cget_H5P_DEFAULT},

  {"H5Cregister_material", lua_H5Cregister_material},
  {"H5Cmaterial_make_table", lua_H5Cmaterial_make_table},
  {"H5Cmaterial_make_raw", lua_H5Cmaterial_make_raw},

  {"H5Cregister_atom", lua_H5Cregister_atom},
  {"H5Catom_make_table", lua_H5Catom_make_table},
  {"H5Catom_make_raw", lua_H5Catom_make_raw},

  {"H5Cregister_vert", lua_H5Cregister_vert},
  {"H5Cvert_make_table", lua_H5Cvert_make_table},
  {"H5Cvert_make_raw", lua_H5Cvert_make_raw},

  {"H5Cregister_body", lua_H5Cregister_body},
  {"H5Cbody_make_table", lua_H5Cbody_make_table},
  {"H5Cbody_make_raw", lua_H5Cbody_make_raw},

  {"H5Cregister_spring", lua_H5Cregister_spring},
  {"H5Cspring_make_table", lua_H5Cspring_make_table},
  {"H5Cspring_make_raw", lua_H5Cspring_make_raw},

  {"H5Cregister_ptriangle", lua_H5Cregister_ptriangle},
  {"H5Cptriangle_make_table", lua_H5Cptriangle_make_table},
  {"H5Cptriangle_make_raw", lua_H5Cptriangle_make_raw},

  {"H5Cregister_lptimer", lua_H5Cregister_lptimer},
  {"H5Clptimer_make_table", lua_H5Clptimer_make_table},
  {"H5Clptimer_make_raw", lua_H5Clptimer_make_raw},

  {"H5Cregister_sph_props", lua_H5Cregister_sph_props},
  {"H5Csph_props_make_table", lua_H5Csph_props_make_table},
  {"H5Csph_props_make_raw", lua_H5Csph_props_make_raw},

  {"H5Cregister_sbb", lua_H5Cregister_sbb},
  {"H5Csbb_make_table", lua_H5Csbb_make_table},
  {"H5Csbb_make_raw", lua_H5Csbb_make_raw},

  {"H5Cregister_glbl_acc", lua_H5Cregister_glbl_acc},
  {"H5Cglbl_acc_make_table", lua_H5Cglbl_acc_make_table},
  {"H5Cglbl_acc_make_raw", lua_H5Cglbl_acc_make_raw},

#ifdef SETTINGS_TODO
  {"H5Cregister_settings", lua_H5Cregister_settings},
  {"H5Csettings_make_table", lua_H5Csettings_make_table},
  {"H5Csettings_make_raw", lua_H5Csettings_make_raw},
#endif

  {"H5Cregister_sbb_params", lua_H5Cregister_sbb_params},
  {"H5Csbb_params_make_table", lua_H5Csbb_params_make_table},
  {"H5Csbb_params_make_raw", lua_H5Csbb_params_make_raw},

  {"H5Cregister_bc", lua_H5Cregister_bc},
  {"H5Cbc_make_table", lua_H5Cbc_make_table},
  {"H5Cbc_make_raw", lua_H5Cbc_make_raw},

  {"H5Cregister_box", lua_H5Cregister_box},
  {"H5Cbox_make_table", lua_H5Cbox_make_table},
  {"H5Cbox_make_raw", lua_H5Cbox_make_raw},

  {"H5Cregister_groups", lua_H5Cregister_groups},
  {"H5Cgroups_make_table", lua_H5Cgroups_make_table},
  {"H5Cgroups_make_raw", lua_H5Cgroups_make_raw},

  {"H5Cregister_intrx", lua_H5Cregister_intrx},
  {"H5Cintrx_make_table", lua_H5Cintrx_make_table},
  {"H5Cintrx_make_raw", lua_H5Cintrx_make_raw},

  {"H5Cregister_material_names", lua_H5Cregister_material_names},
  {"H5Cmaterial_names_make_table", lua_H5Cmaterial_names_make_table},
  {"H5Cmaterial_names_make_raw", lua_H5Cmaterial_names_make_raw},

  {"H5Cregister_drag", lua_H5Cregister_drag},
  {"H5Cdrag_make_table", lua_H5Cdrag_make_table},
  {"H5Cdrag_make_raw", lua_H5Cdrag_make_raw},

  {"H5Cregister_cmprops", lua_H5Cregister_cmprops},
  {"H5Ccmprops_make_table", lua_H5Ccmprops_make_table},
  {"H5Ccmprops_make_raw", lua_H5Ccmprops_make_raw},

  {"H5Cregister_contact", lua_H5Cregister_contact},
  {"H5Ccontact_make_table", lua_H5Ccontact_make_table},
  {"H5Ccontact_make_raw", lua_H5Ccontact_make_raw},

  {NULL, NULL}
};

int luaopen_luahdf5(lua_State *L)
{
  lua_pushglobaltable(L);
  luaL_setfuncs(L, hdf5lib, 0);

  return 0;
}
