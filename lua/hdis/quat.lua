--[[--

   COUPi quaternion library.
   
   A quaternion class with all main operations defined. Quaternion
   object stores its data in the field "q". Use direct access to the
   fields to reach components.

   <p><em>For licensing and copyright use same license as
   COUPi.</em></p>

   @module hdis.quat
   @author Anton Kulchitsky

--]]

local quat = {}

------------------------------------------------------------------

----------------------------------------------------------------
--- Quaternion constructor. Returns a new quaternion.
---
--- If called without parameters or with wrong parameters, it returns
--- a quaternion {1,0,0,0}
---
--- Possible calls:
---
--- quat:new( a, b, c, d )
---
--- quat:new( Q ) -- Q is quaternion of the same class
---
--- quat:new()  -- returns unit quaternion {1,0,0,0}
---
--- quat:new(angle,vector)
---
--- quat:new(vector,angle)
---
--- quat:new( x ) -- x is number, then returns {x,0,0,0}
---
--- @param ... list of parameters
--- @treturn table a new initialized quaternion
----------------------------------------------------------------
function quat:new(...)

   -- creating new quaternion instance
   local Q = { q = {1,0,0,0} }
   setmetatable( Q, self )
   self.__index = self
   self.type = "quat"

   -- filling the q field
   local args = table.pack(...)

   -- If no argument => default unit quaternion, moreover, it is
   -- always unit for all unrecognized cases

   if args.n == 1 then

      if type( args[1] ) == "table" then
         if #args[1] == 4 then
            Q.q[1] = args[1][1]
            Q.q[2] = args[1][2]
            Q.q[3] = args[1][3]
            Q.q[4] = args[1][4]
         elseif args[1].type == "quat" then
            Q.q[1] = args[1].q[1]
            Q.q[2] = args[1].q[2]
            Q.q[3] = args[1].q[3]
            Q.q[4] = args[1].q[4]
         end

      elseif type( args[1] ) == "number" then
         Q.q[1] = args[1]
      end

   elseif args.n == 4 then
      Q.q[1] = args[1]
      Q.q[2] = args[2]
      Q.q[3] = args[3]
      Q.q[4] = args[4]

   elseif args.n == 2 then
      local v
      
      if type( args[1] ) == "table" and type( args[2] ) == "number" then
         -- vector and angle => quaternion
         alpha = 0.5 * args[2]
         cC = math.cos( alpha )
         cS = math.sin( alpha )
         v = args[1]
      elseif type( args[2] ) == "table" and type( args[1] ) == "number" then
         alpha = 0.5 * args[1]
         cC = math.cos( alpha )
         cS = math.sin( alpha )
         v = args[2]
      end

      local vrev = 1 / math.sqrt( v[1]*v[1] + v[2]*v[2] + v[3]*v[3] )
      Q.q = { cC, cS*vrev*v[1], cS*vrev*v[2], cS*vrev*v[3] }

   end
   
   return Q

end

------------------------------------------------------------------
--- Direct access of the data
------------------------------------------------------------------
function quat:value()
   return self.q
end

------------------------------------------------------------------
--- String representing quaternion for pretty print
------------------------------------------------------------------
function quat:__tostring()
   return string.format( "Q(%f,%f,%f,%f)", 
                         self.q[1], self.q[2], 
                         self.q[3], self.q[4] )
end

------------------------------------------------------------------
--- Sum of quaternions
------------------------------------------------------------------
function quat:__add( x )
   local s = quat:new( self.q[1]+x.q[1],
                       self.q[2]+x.q[2],
                       self.q[3]+x.q[3],
                       self.q[4]+x.q[4] )
   return s
end


------------------------------------------------------------------
--- Sub of quaternions
------------------------------------------------------------------
function quat:__sub( x )
   local s = quat:new( self.q[1]-x.q[1],
                       self.q[2]-x.q[2],
                       self.q[3]-x.q[3],
                       self.q[4]-x.q[4] )
   return s
end

------------------------------------------------------------------
--- Returns a quaternion with opposite sign
------------------------------------------------------------------
function quat:__unm()
   local s = quat:new( -self.q[1], -self.q[2], -self.q[3], -self.q[4] )
   return s
end

------------------------------------------------------------------
--- == Compares quaternions
------------------------------------------------------------------
function quat:__eq( x )
   return ( self.q[1] == x.q[1] and 
               self.q[2] == x.q[2] and 
               self.q[3] == x.q[3] and 
               self.q[4] == x.q[4] )
end

------------------------------------------------------------------
--- Quaternion product of two quaternions
------------------------------------------------------------------
function quat:__mul( x )

   local s = quat:new()
   local a = self.q
   local b = x.q

   s.q[1] = a[1]*b[1] - a[2]*b[2] - a[3]*b[3] - a[4]*b[4]
   s.q[2] = a[1]*b[2] + a[2]*b[1] + a[3]*b[4] - a[4]*b[3]
   s.q[3] = a[1]*b[3] - a[2]*b[4] + a[3]*b[1] + a[4]*b[2]
   s.q[4] = a[1]*b[4] + a[2]*b[3] - a[3]*b[2] + a[4]*b[1]
      
   return s
end

------------------------------------------------------------------
--- @treturn number a square of the quaternion
------------------------------------------------------------------
function quat:square()
   local Q = self.q
   return ( Q[1]*Q[1] + Q[2]*Q[2] + Q[3]*Q[3] + Q[4]*Q[4] )
end

------------------------------------------------------------------
--- @treturn number a norm of the quaternion
------------------------------------------------------------------
function quat:norm()
   return math.sqrt( self:square() )
end

------------------------------------------------------------------
--- Modifies the quaternion, normalizes it to unit quaternion
------------------------------------------------------------------
function quat:renorm()
   local r = 1.0 / self:norm()
   self.q[1] = r * self.q[1]
   self.q[2] = r * self.q[2]
   self.q[3] = r * self.q[3]
   self.q[4] = r * self.q[4]
end

------------------------------------------------------------------
--- @treturn quaternion conjugate
------------------------------------------------------------------
function quat:conjugate()
   local Q = self.q
   local s = quat:new( Q[1], -Q[2], -Q[3], -Q[4] )
   return s
end

------------------------------------------------------------------

return quat
