--[[--

   COUPi library for internal functions to be used in different
   functions.

   <p><em>For licensing and copyright use same license as
   COUPi.</em></p>

   @module hdis.loclib
   @author Anton Kulchitsky

--]]

local loclib = {}

--- Maps two indices i,j into single index of pair properties
--- matrix. Material properties table and Atom interaction table are
--- good examples.
--- @tparam number i first index
--- @tparam number j second index
--- @treturn pair matrix index
function loclib.indexmap( i, j )

   -- calculation of the index by i,j: not trivial
   if i > j then i,j = j,i end
   return ( (j-1) * j )/2 + i

end

return loclib
