--[[--

   COUPi functions to use as callbacks and some facilities to build
   call back functions (automatically preloaded and available to use).

   <p><em>For licensing and copyright use same license as
   COUPi.</em></p>

   @module hdis.callback
   @author Anton Kulchitsky

--]]


--- Class callback to be used in COUPi
--- @table callback
--- @tparam function presave presave function that defines the output
--- file name and is called everytime when COUPi wants to save a file
--- @tparam function aftersave function that controls the display on
--- the terminal and is called after every save from COUPi
--- @tparam function g a function that can modify gravity, called at
--- every timestep (if defined)
--- @tparam table std is a table with some pre-defined functions to be
--- used with this module
local callback = {
   presave = nil,
   aftersave = nil,
   g = nil,
   std = {},
}

----------------------------------------------------------------
---               presave standard callbacks
----------------------------------------------------------------

--- Standard pre-save call back function generator. One or both
--- parameters can be omitted.
--- @tparam string prefix Prefix string added to output file names
--- (default is "c")
--- @tparam string suffix Suffix string added to the end of root of
--- output file names (default is "ms")
--- @treturn function Function that need to be assigned to
--- hdis.callback.presave value to have effect on output.
--- @usage hdis.callback.presave = hdis.callback.std.presave( )
--- @usage hdis.callback.presave = hdis.callback.std.presave( "out-" )
--- @usage hdis.callback.presave = hdis.callback.std.presave( "out-", "" )
function callback.std.presave( prefix, suffix )

   -- makes a standard filename with milliseconds as a number index
   local prefix = prefix or "c"
   local suffix = suffix or "ms"
   
   local function cback_presave( time, step )
      -- it must return at least one value: filename where to save.

      -- we trasform seconds in miliseconds and round smartly to avoid
      -- problems when 0.2 is represented as 0.199998 etc.
      local filename = string.format( "%s%08d%s.h5",
                                      prefix,
                                      math.floor((time+0.00001)*1000),
                                      suffix )
      return filename
   end

   return cback_presave
end


--- Standard pre-save call back function generator. Useful is there is
--- a very small timestep and one would like to put microseconds in
--- the name of the file. One or both parameters can be omitted.
--- @tparam string prefix Prefix string added to output file names
--- (default is "c")
--- @tparam string suffix Suffix string added to the end of root of
--- output file names (default is "mcs")
--- @treturn function Function that need to be assigned to
--- hdis.cback_presave value to have effect on output
--- @usage hdis.callback.presave = hdis.callback.std.presave_mcs()
--- @usage hdis.callback.presave = hdis.callback.std.presave_mcs( "out-" )
--- @usage hdis.callback.presave = hdis.callback.std.presave_mcs( "out-", "" )
function callback.std.presave_mcs( prefix, suffix )

   -- makes a standard filename with microseconds as a number index
   local prefix = prefix or "c"
   local suffix = suffix or "mcs"
   
   local function cback_presave( time, step )
      -- it must return at least one value: filename where to save.

      -- we trasform seconds in miliseconds and round smartly to avoid
      -- problems when 0.2 is represented as 0.199998 etc.
      local filename = string.format( "%s%011d%s.h5",
                                      prefix,
                                      math.floor((time+0.00000001)*1000000),
                                      suffix )
      return filename
   end

   return cback_presave
end

----------------------------------------------------------------
---             aftersave callbacks
----------------------------------------------------------------

--- Standard short and convenient after-save call back function
--- generator. The returned function is used usually to monitor model
--- progress by outputing current model time and output file name on
--- terminal.
--- @treturn function Function that need to be assigned to
--- hdis.cback_aftersave value to have effect on output
--- @usage hdis.callback.aftersave = hdis.callback.std.aftersave_short()
function callback.std.aftersave_short()
   local function cback_aftersave( time, dt, step, filename )
      local timestr = string.format( "%.5f", time )
      io.write( "Saved at t = ", timestr,
                "  in file <", filename, ">\n" )
   end
   return cback_aftersave
end


--- Standard rather long and convenient after-save call back function
--- generator. The returned function is used usually to monitor model
--- progress by outputing some useful model information.
--- @treturn function Function that need to be assigned to
--- hdis.cback_aftersave value to have effect on output
--- @usage hdis.callback.aftersave = hdis.callback.std.aftersave_long()
function callback.std.aftersave_long()

   local exe_time_prv = os.time()
   local exe_time_ini = exe_time_prv
   local exe_step_prv = 0

   local function cback_aftersave( time, dt, step, filename,
                                   conhash_ulm, conhash_ncleans )
      -- current time
      local exe_time_new = os.time()

      -- execution time in hours
      local exe_time_diff = ( exe_time_new - exe_time_prv ) / 60.0
      local exe_time_run = ( exe_time_new - exe_time_ini ) / 3600.0

      -- execution step number
      local exe_step_done = step - exe_step_prv
      exe_step_prv = step

      -- output all information
      local timestr = string.format( "%.4f", time )
      local timestr2 = string.format( "%.2f", exe_time_diff )
      local timestr3 = string.format( "%.2f", exe_time_run )
      io.write( "--------------------------------------------\n" )
      io.write( "t = ", timestr,
                "; file <", filename, ">;\n\t",

                exe_step_done, "  steps used ", timestr2, " min;",
                "  runs ", timestr3, " h;\n\t",
                
                "HASH: ULM  ", conhash_ulm,
                ";  ncleans:  ", conhash_ncleans, "\n" )
      -- previous time is current time after computations
      exe_time_prv = exe_time_new

   end

   return cback_aftersave
end

--*------------------------------------------------------------*--

-------------------------------------------------------------------------------
--- Finalize callback functions. Its return function that is used for creation
--- of the name of the last file.
--- @treturn function -- Function that need to be assigned to
--- hdis.callback.finalize value to have effect on output
--- @usage hdis.callback.finalize = hdis.callback.std.finalize( name )
-------------------------------------------------------------------------------
function callback.std.finalize( name )

   local name = name or "final"
   name = name..'.h5'

   local function finalize( )
      return name
   end
   return finalize
end


return callback
