--[[--

      Lua library for reading and writing data from COUPi HDF5
      files. It reads HDF5 data into lua tables, and can write lua
      tables to HDF5 files. For licensing and copyright use same
      license as COUPi.

      @module hdis.hdf5
      @author Ben Nye, Anton Kulchitsky
--]]

local hdf5 = {}

-- C module for calling HDF5 functions
require ("luahdf5")

-- For file copy
require ("pl")

function hdf5.comp(v, u)
  if type(v) == "table" and type(u) == "table" then
    return unpack(v) == unpack(u)
  end
  return v == u
end

--- Copy a file. You must use absolute paths.
-- @tparam string in_file Original file name
-- @tparam string out_file New file name
function hdf5.copy_file(in_file, out_file)
  file.copy(in_file, out_file)
end

function hdf5.get_funcs(table_name)
  local register_func = nil
  local make_table_func = nil
  local make_raw_func = nil
  if table_name == "Group Names" then
    register_func = H5Cregister_groups
    make_table_func = H5Cgroups_make_table
    make_raw_func = H5Cgroups_make_raw
  elseif table_name == "Atoms" then
    register_func = H5Cregister_atom
    make_table_func = H5Catom_make_table
    make_raw_func = H5Catom_make_raw
  elseif table_name == "Group Interaction" then
    register_func = H5Cregister_intrx
    make_table_func = H5Cintrx_make_table
    make_raw_func = H5Cintrx_make_raw
  elseif table_name == "Bodies" then
    register_func = H5Cregister_body
    make_table_func = H5Cbody_make_table
    make_raw_func = H5Cbody_make_raw
  elseif table_name == "Boundary" then
    register_func = H5Cregister_bc
    make_table_func = H5Cbc_make_table
    make_raw_func = H5Cbc_make_raw
  elseif table_name == "Box (domain)" then
    register_func = H5Cregister_box
    make_table_func = H5Cbox_make_table
    make_raw_func = H5Cbox_make_raw
  elseif table_name == "Contact Hash Assosiate" then
    register_func = H5Cregister_conhash_comp
    make_table_func = H5Cconhash_comp_make_table
    make_raw_func = H5Cconhash_comp_make_raw
  elseif table_name == "Contact List" then
    register_func = H5Cregister_contact
    make_table_func = H5Ccontact_make_table
    make_raw_func = H5Ccontact_make_raw
  elseif table_name == "Contact Mech. Properties" then
    register_func = H5Cregister_cmprops
    make_table_func = H5Ccmprops_make_table
    make_raw_func = H5Ccmprops_make_raw
  elseif table_name == "Drag Force Values" then
    register_func = H5Cregister_drag
    make_table_func = H5Cdrag_make_table
    make_raw_func = H5Cdrag_make_raw
  elseif table_name == "General Settings" then
    register_func = H5Cregister_settings
    make_table_func = H5Csettings_make_table
    make_raw_func = H5Csettings_make_raw
  elseif table_name == "Gravity" then
    register_func = H5Cregister_glbl_acc
    make_table_func = H5Cglbl_acc_make_table
    make_raw_func = H5Cglbl_acc_make_raw
  elseif table_name == "Material Names" then
    register_func = H5Cregister_material_names
    make_table_func = H5Cmaterial_names_make_table
    make_raw_func = H5Cmaterial_names_make_raw
  elseif table_name == "Materials" then
    register_func = H5Cregister_material
    make_table_func = H5Cmaterial_make_table
    make_raw_func = H5Cmaterial_make_raw
  elseif table_name == "PTriangles" then
    register_func = H5Cregister_ptriangle
    make_table_func = H5Cptriangle_make_table
    make_raw_func = H5Cptriangle_make_raw
  elseif table_name == "Verts" then
    register_func = H5Cregister_vert
    make_table_func = H5Cvert_make_table
    make_raw_func = H5Cvert_make_raw
  elseif table_name == "SBBs" then
    register_func = H5Cregister_sbb
    make_table_func = H5Csbb_make_table
    make_raw_func = H5Csbb_make_raw
  elseif table_name == "SBB parameters" then
    register_func = H5Cregister_sbb_params
    make_table_func = H5Csbb_params_make_table
    make_raw_func = H5Csbb_params_make_raw
  elseif table_name == "Springs" then
    register_func = H5Cregister_spring
    make_table_func = H5Cspring_make_table
    make_raw_func = H5Cspring_make_raw
  elseif table_name == "Time_Parameters" then
    register_func = H5Cregister_lptimer
    make_table_func = H5Clptimer_make_table
    make_raw_func = H5Clptimer_make_raw
  elseif table_name == "SPH properties" then
    register_func = H5Cregister_sph_props
    make_table_func = H5Csph_props_make_table
    make_raw_func = H5Csph_props_make_raw
  end
  return register_func, make_table_func, make_raw_func
end

--- Check the size of a table in an HDF5 file.
-- @tparam string file_name Absolute path to HDF5 file
-- @tparam string table_name Name of the the desired table in the HDF5
-- file
-- @treturn number Number of entries in the table, or -1 if not found
function hdf5.get_dims(file_name, table_name)
  local register_func, make_table_func, make_raw_func =
     hdf5.get_funcs(table_name)
  if register_func == nil then
     print("Cannot find HDF5 functions for: " .. table_name)
     return {}
  end
  local dim
  local file_id = H5Fopen(file_name)
  local H5P_DEFAULT = H5Cget_H5P_DEFAULT()
  if H5Lexists(file_id, table_name, H5P_DEFAULT) == 0 then
     dim = -1
  else
     local dataset_id = H5Dopen(file_id, table_name)
     local type_id = register_func()
     dim = H5Cget_dims(dataset_id, type_id)
     H5Tclose(type_id)
     H5Dclose(dataset_id)
  end
  H5Fclose(file_id)
  return dim
end

--- Read a table from an HDF5 file and create a corresponding Lua
--  table.
-- The Lua table emulates the HDF5 data by replacing structs with
-- tables that have an entry for each member in the struct. <br>
-- Example:
-- <pre> atoms = hdf5.read("/path/to/file", "Atoms") </pre>
-- will fill the atoms table with entries that look like:
-- <pre> atoms = {
--    0 = {
--      type = 1,
--      body_indx = 0,
--      ppindx = {
--        0 = 0,
--        1 = -1,
--        2 = -1
--      },
--      group_id = 0,
--      R = 0.02
--    },
--    1 = { ...
--    },
--    ...
--  } </pre>
-- @tparam string file_name Absolute path to HDF5 file
-- @tparam string table_name Name of the desired table in the HDF5 file
-- @treturn table Lua table filled with HDF5 data
function hdf5.read(file_name, table_name)

  local register_func, make_table_func, make_raw_func
     = hdf5.get_funcs(table_name)

  if register_func == nil then
    print("Cannot find HDF5 functions for: " .. table_name)
    return {}
  end
  local file_id = H5Fopen(file_name)

  local dataset_id = H5Dopen(file_id, table_name)
  if ( dataset_id == -1 ) then return nil end-- no such table

  local datatype_id = register_func()
  
  local data_raw, data_n = H5Cread(dataset_id, datatype_id)
  
  local data_table = make_table_func(data_raw, data_n)
  data_table["name"] = table_name
  H5Tclose(datatype_id)
  H5Dclose(dataset_id)
  H5Fclose(file_id)
  
  return data_table
end

--- Write a Lua table to the corresponding table HDF5 table.
--- This function writes a Lua table that was read from an HDF5 file
--- and possibly modified back to an HDF5 file.
--
--- @tparam string file_name Absolute path to HDF5 file
--- @tparam table data_table Lua table that you want to write. It is
--- expected to be in the same format as the tables hdf5.read creates
function hdf5.write(file_name, data_table)
  table_name = data_table["name"]
  local register_func, make_table_func, make_raw_func =
     hdf5.get_funcs(table_name)
  if register_func == nil then
    print("Cannot find HDF5 functions for: " .. table_name)
    return {}
  end
  local file_id = H5Fopen(file_name)
  local dataset_id = H5Dopen(file_id, table_name)
  local type_id = register_func()
  local data_raw = make_raw_func(data_table)
  H5Cwrite(dataset_id, type_id, data_raw)
  H5Tclose(type_id)
  H5Dclose(dataset_id)
  H5Fclose(file_id)
end

--*----------------------------------------------------------------*--

return hdf5
