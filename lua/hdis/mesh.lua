--[[--

   COUPi library for building standard spring meshes.

   <p>It contains different standard spring meshes and functions to add
   them to hdis. Mesh is a table object that contains the following
   tables: (1) nodes, which are bodies in the mesh and interact with
   other bodies in the system; (2) springs, connecting nodes; (3)
   pressure triangles (ptriangles). The last table can be empty if
   there is no external pressure applied.</p>

   <p>mesh uses mesh_phys and mesh_conf tables to define the
   meshes.</p>

   <p><code>mesh_phys = { k, eta, x0, R, rho, p, mprops }</code>,
   where k - stiffness of the springs, eta - damping of the springs,
   x0 - neutral length, R - radius of the node, rho - density of the
   node, p - pressure for triangles, mprops - mech. properties of the
   node</p>

   <p><code>mesh_conf = { groupname, groupname_bnd, fixx, fixy
   }</code></p>

   <p><em>For licensing and copyright use same license as
   COUPi.</em></p>

   <p><em>For licensing and copyright use same license as
   COUPi.</em></p>

   @module hdis.mesh
   @author Anton Kulchitsky

--]]

require "hdis"
local control = require 'hdis.control'
local mathx = require "hdis.mathx"
local shapes = require "hdis.shape"
local vec3d = require "hdis.vec3d"

local mesh = {}

-- abbreviation for vec3d library

----------------------------------------------------------------
--- Parallelogram mesh
----------------------------------------------------------------
---
--- @tparam table r0 an array that contains 3 coordinates of one corner
--- @tparam table r1 an array that contains 3 coordinates of another corner
--- @tparam table r2 an array that contains 3 coordinates of third corner
--- @tparam number dx is a step along r0-r1 line
--- @tparam number dy is a step along r0-r2 line
--- @tparam table mesh_phys is a mesh physics table (TODO: explain)
--- @tparam table mesh_conf is a configure mesh table (TODO: explain).
--- @treturn table Parallelogram shaped mesh with parallelogram cells
--- ready for @{hdis.append}
--- @see hdis.append
--- @see hdis.append_mesh
--- @see mesh.lua

function mesh.pgram( r0, r1, r2, dx, dy, mesh_phys, mesh_conf )

   -- Abbreviations
   local k = mesh_phys.k
   local eta = mesh_phys.eta
   local x0 = mesh_phys.x0 -- (*)
   local R = mesh_phys.R
   local rho = mesh_phys.rho
   local p = mesh_phys.p -- (*)
   local mprops = mesh_phys.mprops

   local group, group_bnd, fixx, fixy
   if mesh_conf then
      group = hdis.groups:resolve( mesh_conf.group )
      fixx = mesh_conf.fixx -- (*)
      fixy = mesh_conf.fixy -- (*)
      if mesh_conf.fixed then
         fixx = true
         fixy = true
      end
   end

   group_bnd = mesh_conf.group_bnd -- (*)
   if not group_bnd then
      group_bnd = group
   else
      group_bnd = hdis.groups:resolve( group_bnd )
   end

   -- Default values [ those that are marked with (*) may not be set ]
   if not fixx then fixx = false end
   if not fixy then fixy = false end

   -- if p = false or nil, then no prtiangles by default

   -- Main table
   mesh = { nodes = {}, springs = {}, ptriangles = {} }

   -- Correction of dx, dy; number of spheres
   local X = vec3d.abs( r1 - r0 )
   local Y = vec3d.abs( r2 - r0 )
   local revX = 1 / X
   local revY = 1 / Y
   local nx = math.floor( X / dx )
   local ny = math.floor( Y / dy )
   local dx = X / nx
   local dy = Y / ny

   -- default x0 is chosen to make current length apprx. neutral
   if not x0 then x0 = 0.5*( dx + dy ) end

   -- vectors r10, r20
   local r10 = vec3d.diff( r1, r0 )
   local r20 = vec3d.diff( r2, r0 )

   -- The loop for nodes

   for j = 0, ny do
      for i = 0, nx do

         -- forming the center of the sphere
         local a = revX*i*dx
         local b = revY*j*dy
         c = vec3d.lin( a, r10, b, r20 )
         c = vec3d.sum( c, r0 )

         -- creation of the sphere
         local sphere = shapes.sphere( R, mesh_conf.group, mesh_conf.material )
         sphere.body.c = c

         if ( ( j == 0 or j == ny ) and fixy ) or
            ( ( i == 0 or i == nx ) and fixx ) then
            sphere.body.control = control.builtin.steady
            sphere.atoms[1].group = group_bnd
         end

         table.insert( mesh.nodes, sphere )
      end
   end

   -- local function that returns the index of the node (sphere) by
   -- indices i and j in the square loop
   local function node_index( i, j )
      return 1+i + (1+nx) * j
   end

   -- The loop for springs

   -- (a) horizintal traverse
   local nxspring = 0
   for j = 0, ny do
      for i = 1, nx do
         nxspring = nxspring + 1

         -- we connect nodes i-1,j with i,j

         local spring =  {
            rb1 = { R, 0, 0 },
            rb2 = { -R, 0, 0 },
            body1 = node_index(i-1,j),
            body2 = node_index(i,j),
            x0 = x0,
            k = k,
            eta = eta
         }

         table.insert( mesh.springs, spring )

      end
   end

   -- (b) vertical traverse
   local nxspring = 0
   for i = 0, nx do
      for j = 1, ny do
         nxspring = nxspring + 1

         -- we connect nodes i,j-1 with i,j

         local spring =  {
            rb1 = { 0, R, 0 },
            rb2 = { 0, -R, 0 },
            body1 = node_index(i,j-1),
            body2 = node_index(i,j),
            x0 = x0,
            k = k,
            eta = eta
         }

         table.insert( mesh.springs, spring )

      end
   end

   -- Making pressure triangles if necessary
   if p == nil or p == false then
      mesh.ptriangles = {}
   else
      for j = 1, ny do
         for i = 1, nx do
            local b1 = node_index( i-1, j-1 )
            local b2 = node_index( i  , j-1 )
            local b3 = node_index( i  , j   )
            local b4 = node_index( i-1, j   )
            local t1 = { body1 = b1, body2 = b2, body3 = b3, p = p }
            local t2 = { body1 = b1, body2 = b3, body3 = b4, p = p }

            table.insert( mesh.ptriangles, t1 )
            table.insert( mesh.ptriangles, t2 )
         end
      end
   end

   -- Additional information: indexes of the corners

   return mesh
end


----------------------------------------------------------------
--- Rectangular Cylindrical mesh
----------------------------------------------------------------
---
--- @tparam number H the height of the cylinder
--- @tparam number R the radius of the cylinder
--- @tparam number cx x coordinate of the center of the cylinder
--- @tparam number cy y coordinate of the center of the cylinder
--- @tparam number cz z coordinate of the center of the cylinder
--- @tparam number dz vertical step along the cylinder
--- @tparam number Rdt - horizontal step
--- @tparam table  mesh_phys table of the mech. properties of the mesh
--- @tparam table mesh_conf configuration of the mesh
--- @tparam boolean centric true/false(default) for no momentum mesh
--- when springs attached to the centers of the nodes instead of the
--- surface.
--- @treturn table Cylindrical shgaped mesh with rectangular cells
   --- that ready to be supplied for @{hdis.append}
--- @see hdis.append
--- @see hdis.append_mesh

function mesh.cylinder_rect( H, R, cx, cy, cz, dz, Rdt,
                             mesh_phys, mesh_conf, centric )

   -- Abbreviations
   local k = mesh_phys.k
   local eta = mesh_phys.eta
   local x0 = mesh_phys.x0 -- (*)
   local sphR = mesh_phys.R
   local rho = mesh_phys.rho
   local p = mesh_phys.p -- (*)
   local mprops = mesh_phys.mprops

   local group, group_bnd, group_bottom, group_top
   local fix_top, fix_bottom, fixed

   local groupname = mesh_conf.groupname
   group = hdis.groups.resolve( groupname )

   -- resolving groups
   if mesh_conf.groupname_bnd then
      group_bnd = hdis.groups.resolve( mesh_conf.groupname_bnd )
   else
      group_bnd = group -- (*)
   end

   if mesh_conf.groupname_bottom then
      group_bottom = hdis.groups.resolve( mesh_conf.groupname_bottom )
   else
      group_bottom = group -- (*)
   end

   if mesh_conf.groupname_top then
      group_top = hdis.groups.resolve( mesh_conf.groupname_top )
   else
      group_top = group -- (*)
   end

   fix_top = mesh_conf.fix_top -- (*)
   fix_bottom = mesh_conf.fix_bottom -- (*)
   fixed = mesh_conf.fixed -- (*)

   -- Default values [ those that are marked with (*) may not be set ]
   if not fix_top then fix_top = false end
   if not fix_bottom then fix_bottom = false end
   if not fixed then fixed = false end

   -- if p = false or nil, then no prtiangles by default

   -- Main table
   mesh = { nodes = {}, springs = {}, ptriangles = {} }

   -- calculate the angle step dt. Cylinder has at least 8 sides. We
   -- take care to not overlap
   local dt = math.min ( 0.25 * math.pi, Rdt / R )
   local Nt = math.ceil( ( 2 * math.pi - dt ) / dt )
   dt = 2*math.pi / ( Nt + 1 )

   -- calculate the step along H, at least one
   local dz = dz                -- to avoid changing input parameter
   Nz = math.max( 1, math.ceil( H / dz ) )
   dz = H / Nz

   -- default x0 is chosen to make current length apprx. neutral
   if not x0 then x0 = 0.5*( R*dt + dz ) end

   -- local function that returns the index of the node (sphere) by
   -- indices i and j in the square loop
   local function node_index( i, j )
      return 1+i + (1+Nt) * j
   end

   -- Build the cylinder

   -- nodes:

   for k = 0, Nz do
      local z = k * dz - 0.5 * H

      -- select the boundary
      local levgroup = group
      if k == 0 then
         levgroup = group_bottom
      end

      if k == Nz then
         levgroup = group_top
      end

      for i = 0, Nt do
         local t = i * dt

         local BS = shapes.sphere( sphR, rho, mprops, levgroup )
         BS.body.c = {
            cx + R * math.cos( t ), cy + R * math.sin( t ), cz + z }

         if ( k == 0 and fix_bottom ) then
            BS.body.control = control.builtin.steady
         end
         if ( k == Nz and fix_top ) then
            BS.body.control = control.builtin.steady
         end
         if ( fixed ) then
            BS.body.control = control.builtin.steady
         end

         table.insert( mesh.nodes, BS )
      end
   end

   -- X is a shift for spring attachement
   local X
   if centric then X = 0 else X = sphR end

   -- springs:

   local nxspring = 0

   for i = 0, Nt do
      for j = 1, Nz do
         nxspring = nxspring + 1

         local spring =  {
            rb1 = { 0, 0, X },
            rb2 = { 0, 0, -X },
            body1 = node_index(i,j-1),
            body2 = node_index(i,j),
            x0 = x0,
            k = k,
            eta = eta
         }

         table.insert( mesh.springs, spring )

      end
   end

   for j = 0, Nz do
      for i = 1, Nt do
         nxspring = nxspring + 1

         local spring =  {
            rb1 = { 0, X, 0 },
            rb2 = { 0, -X, 0 },
            body1 = node_index(i-1,j),
            body2 = node_index(i,j),
            x0 = x0,
            k = k,
            eta = eta
         }

         table.insert( mesh.springs, spring )

      end

      nxspring = nxspring + 1

      local spring =  {
         rb1 = { 0, X, 0 },
         rb2 = { 0, -X, 0 },
         body1 = node_index(0,j),
         body2 = node_index(Nt,j),
         x0 = x0,
         k = k,
         eta = eta
      }

      table.insert( mesh.springs, spring )

   end

   -- ptriangles:

   if p == nil or p == false then
      mesh.ptriangles = {}
   else
      for j = 1, Nz do
         for i = 1, Nt do
            local b1 = node_index( i-1, j-1 )
            local b2 = node_index( i  , j-1 )
            local b3 = node_index( i  , j   )
            local b4 = node_index( i-1, j   )
            local t1 = { body1 = b1, body2 = b2, body3 = b3, p = p }
            local t2 = { body1 = b1, body2 = b3, body3 = b4, p = p }

            table.insert( mesh.ptriangles, t1 )
            table.insert( mesh.ptriangles, t2 )
         end

         -- connect beginning and the end (i = Nt+1)

         local b1 = node_index( Nt, j-1 )
         local b2 = node_index( 0  , j-1 )
         local b3 = node_index( 0  , j   )
         local b4 = node_index( Nt, j   )
         local t1 = { body1 = b1, body2 = b2, body3 = b3, p = p }
         local t2 = { body1 = b1, body2 = b3, body3 = b4, p = p }

         table.insert( mesh.ptriangles, t1 )
         table.insert( mesh.ptriangles, t2 )

      end
   end

   return mesh
end


----------------------------------------------------------------
--- Cylindrical mesh with shifted triangles and cross springs
----------------------------------------------------------------
---
--- @tparam number H the height of the cylinder
--- @tparam number R the radius of the cylinder
--- @tparam number cx x coordinate of the center of the cylinder
--- @tparam number cy y coordinate of the center of the cylinder
--- @tparam number cz z coordinate of the center of the cylinder
--- @tparam number dz vertical step along the cylinder
--- @tparam number Rdt - horizontal step
--- @tparam table  mesh_phys table of the mech. properties of the mesh
--- @tparam table mesh_conf configuration of the mesh
--- @tparam boolean centric true/false(default) for no momentum mesh
--- when springs attached to the centers of the nodes instead of the
--- surface.
--- @treturn table Cylindrical shaped mesh with trianlge cells
--- that ready to be supplied for @{hdis.append}
--- @see hdis.append
--- @see hdis.append_mesh

function mesh.cylinder( H, R, cx, cy, cz, dz, Rdt,
                        mesh_phys, mesh_conf, centric )

   -- Abbreviations
   local k     = mesh_phys.k
   local eta   = mesh_phys.eta
   local x0    = mesh_phys.x0 -- (*)
   local sphR  = mesh_phys.R
   local p     = mesh_phys.p -- (*)

   local group, group_bnd, group_bottom, group_top, ptriag_group_num
   local fix_top, fix_bottom, fixed, extrarim

   if not mesh_conf.group_bnd then
      mesh_conf.group_bnd = mesh_conf.group
   end
   if not mesh_conf.group_bottom then
      mesh_conf.group_bottom = mesh_conf.group
   end
   if not mesh_conf.group_top then
      mesh_conf.group_top = mesh_conf.group
   end

   if mesh_conf then
      group = mesh_conf.group
      group_bnd = mesh_conf.group_bnd or mesh_conf.group -- (*)
      group_bottom = mesh_conf.group_bottom or mesh_conf.group-- (*)
      group_top = mesh_conf.group_top or mesh_conf.group-- (*)

      material = mesh_conf.material
      fix_top = mesh_conf.fix_top -- (*)
      fix_bottom = mesh_conf.fix_bottom -- (*)
      fixed = mesh_conf.fixed -- (*)

      extrarim = mesh_conf.extrarim -- (*)
      ptriag_group_num = mesh_conf.ptriag_group_num -- (*)
   end

   -- Default values [ those that are marked with (*) may not be set ]
   if not fix_top then fix_top = false end
   if not fix_bottom then fix_bottom = false end
   if not fixed then fixed = false end
   if not group then group = 0 end
   if not extrarim then extrarim = 0 end
   if not ptriag_group_num then ptriag_group_num = 1 end

   -- if p = false or nil, then no prtiangles by default

   -- Main table
   mesh = { nodes = {}, springs = {}, ptriangles = {} }

   -- calculate the angle step dt. Cylinder has at least 8 sides. We
   -- take care to not overlap
   local dt = math.min ( 0.25 * math.pi, Rdt / R )
   local Nt = math.ceil( ( 2 * math.pi - dt ) / dt )
   dt = 2*math.pi / ( Nt + 1 )

   -- calculate the step along H, at least one
   local dz = dz                -- to avoid changing input parameter
   Nz = math.max( 1, math.ceil( H / dz ) )
   dz = H / Nz

   -- default x0 is chosen to make current length apprx. neutral
   if not x0 then x0 = 0.5*( R*dt + dz ) end

   -- local function that returns the index of the node (sphere) by
   -- indices i and j in the square loop
   local function node_index( i, j )

      -- cylindrical
      if i == -1 then i = Nt
      elseif i == Nt+1 then i = 0
      end

      local ind =  1 + i + (1 + Nt) * j

      return ind
   end

   -- Build the cylinder

   -- nodes:

   for k = 0, Nz + extrarim do
      local z = k * dz - 0.5 * H   -- level z

      -- select the boundary
      local levgroup = group
      if k == 0 then
         levgroup = group_bottom
      end

      if k >= Nz then
         levgroup = group_top
      end

      for i = 0, Nt do
         local t = ( i  + 0.5 * ( k % 2 ) ) * dt

         local BS = shapes.sphere( sphR, levgroup, material )
         BS.body.c = {
            cx + R * math.cos( t ), cy + R * math.sin( t ), cz + z }

         if ( k == 0 and fix_bottom ) then
            BS.body.control = control.builtin.steady
         end
         if ( k == Nz and fix_top ) then
            BS.body.control = control.builtin.steady
         end
         if ( fixed ) then
            BS.body.control = control.builtin.steady
         end

         table.insert( mesh.nodes, BS )
      end
   end

   -- X is a shift for spring attachement
   local X
   if centric then X = 0 else X = sphR end

   -- springs:

   for j = 1, Nz do

      for i = 0, Nt do

         local spring =  {
            rb1 = { 0, 0, X },
            rb2 = { 0, 0, -X },
            body1 = node_index(i,j-1),
            body2 = node_index(i,j),
            x0 = x0,
            k = k,
            eta = eta
         }

         table.insert( mesh.springs, spring )

         if j % 2 == 0 then sign = 1
         else sign = -1
         end

         local spring =  {
            rb1 = { 0, 0, X },
            rb2 = { 0, 0, -X },
            body1 = node_index( i, j - 1 ),
            body2 = node_index( i + sign, j ),
            x0 = x0,
            k = k,
            eta = eta
         }

         table.insert( mesh.springs, spring )

      end
   end


   for j = 0, Nz do
      for i = 0, Nt do

         local spring =  {
            rb1 = { 0, X, 0 },
            rb2 = { 0, -X, 0 },
            body1 = node_index(i-1,j),
            body2 = node_index(i,j),
            x0 = x0,
            k = k,
            eta = eta
         }

         table.insert( mesh.springs, spring )

      end
   end

   -- ptriangles:

   if p == nil or p == false then
      mesh.ptriangles = {}
   else

      for j = 0, Nz-1 do
         for i = 0, Nt do

            local b1 = node_index( i,   j )
            local b2 = node_index( i+1, j )
            local b3 = node_index( i  , j+1 )
            local b4 = node_index( i+1, j+1 )

            local t1 = { body1 = b1, body2 = b2, body3 = b3,
                         p = p, group = ptriag_group_num }
            local t2 = { body1 = b2, body2 = b4, body3 = b3,
                         p = p, group = ptriag_group_num }

            table.insert( mesh.ptriangles, t1 )
            table.insert( mesh.ptriangles, t2 )

         end
      end

   end

   return mesh
end

----------------------------------------------------------------
--- Cylindrical mesh with equilateral triangles
----------------------------------------------------------------
---
--- @tparam number H the height of the cylinder
--- @tparam number R the radius of the cylinder
--- @tparam number cx x coordinate of the center of the cylinder
--- @tparam number cy y coordinate of the center of the cylinder
--- @tparam number cz z coordinate of the center of the cylinder
--- @tparam number Rdt - horizontal step
--- @tparam table  mesh_phys table of the mech. properties of the mesh
--- @tparam table mesh_conf configuration of the mesh
--- @tparam boolean centric true/false(default) for no momentum mesh
--- when springs attached to the centers of the nodes instead of the
--- surface.
--- @treturn table Cylindrical shgaped mesh with equilateral cells
--- that ready to be supplied for @{hdis.append}
--- @see hdis.append
--- @see hdis.append_mesh

function mesh.cylinder_equilateral( H, R, cx, cy, cz, Rdt,
                                          mesh_phys, mesh_conf, centric )
   dz = 0.5 * Rdt * math.sqrt( 3 )
   return mesh.cylinder( H, R, cx, cy, cz, dz, Rdt,
                         mesh_phys, mesh_conf, centric )
end


hdis.mesh = mesh
return mesh
