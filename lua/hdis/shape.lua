--[[---------------------------------------------------------------------------
   Coupi primitive shape library.

   (c)2016 Coupi, Inc.

   @module hdis.shape
   @author Anton Kulchitsky
--]]---------------------------------------------------------------------------

require 'hdis'
local shape = {}

-------------------------------------------------------------------------------
--- Sphere.
--- @tparam number R -- Radius
--- @tparam string gname -- Name of the group
--- @tparam string mname -- Name of the material
--- @treturn table -- Shape table ready for hdis.append
-------------------------------------------------------------------------------
function shape.sphere( R, gname, mname )

   local group  = hdis.groups:resolve( gname )
   local imat   = hdis.material:resolve( mname )
   local rho    = hdis.material.list[ imat ].rho

   local V = ( 4.0 / 3.0 ) * math.pi * R * R * R
   local m = V * rho
   local I = 0.4 * m * R * R

   local body = {

      -- we do not specify center of mass here
      v = { 0, 0, 0 },         -- initial velocity (m/s)
      w = { 0, 0, 0 },         -- initial angular vel (rad/s)
      m = m,                   -- mass (kg)
      V = V,                   -- volume (m^3)
      I = { I, I, I },         -- main values of inertia tensor
      q = { 1, 0, 0, 0 }      -- initial rotation quaternion
   }

   local atoms = {
      {
         R = R,
         sbb_R = R,
         group = group,
         material = imat,
         sbb_c = { 0, 0, 0 },
         nverts = 1,
         nedges = 0,
         nfaces = 0,
         ivert0 = 1,
         iedge0 = 0,
         iface0 = 0,
         isolated = 1
      }
   }

   local faces = {}
   local edges = {}

   local verts = {
      {
         local_pos  = { 0, 0, 0 },
         deg    = 0,
         iadj_e = 1
      }
   }

   local adj_e = {}

   return { body  = body,
            atoms = atoms,
            faces = faces,
            edges = edges,
            verts = verts,
            adj_e = adj_e }

end

-------------------------------------------------------------------------------
--- Eightshape 8, or twospheres
--- @tparam number r radius
--- @tparam string gname -- Name of the group
--- @tparam string mname -- Name of the material
--- @treturn table two-sphere shape
-------------------------------------------------------------------------------
function shape.twospheres( r, gname, mname )

   local group  = hdis.groups:resolve( gname )
   local imat   = hdis.material:resolve( mname )

   local rho = hdis.material.list[ imat ].rho

   local V = (8.0/3.0) * math.pi * r * r * r
   local m = V * rho

   local Ibase = m * r * r

   -- List of Bodies in the system
   local body = {
      -- we do not specify center of mass here
      v = { 0, 0, 0 },         -- initial velocity (m/s)
      w = { 0, 0, 0 },         -- initial angular vel (rad/s)
      m = m,            -- mass (kg)
      V = V,            -- volume (m^3)

      -- main values of inertia tensor
      I = { 0.4*Ibase, 1.4*Ibase, 1.4*Ibase },
      q = { 1, 0, 0, 0 },       -- initial rotation quaternion
   }

   local atoms = {
      {
         R = r,
         group = group,
         material = imat,
         sbb_c = { -r, 0, 0 },
         sbb_R = r,
         nverts = 1,
         nedges = 0,
         nfaces = 0,
         ivert0 = 1,
         iedge0 = 1,
         iface0 = 1,
         isolated = 1,
      },
      {
         R = r,
         group = group,
         material = imat,
         sbb_c = { r, 0, 0 },
         sbb_R = r,
         nverts = 1,
         nedges = 0,
         nfaces = 0,
         ivert0 = 2,
         iedge0 = 1,
         iface0 = 1,
         isolated = 1,
      }
   }

  local faces = {
  }
  local edges = {
  }

  local verts = {
    {
      iadj_e = 1,
      deg = 0,
      local_pos = { -r, 0, 0 },
      global_pos =  { -r, 0, 0 },
    },
    {
      iadj_e = 1,
      deg = 0,
      local_pos =  { r, 0, 0 },
      global_pos = { r, 0, 0 },
    }
  }

  local adj_e = {
  }

  return {
    body=body,
    atoms=atoms,
    faces=faces,
    edges=edges,
    verts=verts,
    adj_e=adj_e
  }

end

shape.eightshape = shape.twospheres

-------------------------------------------------------------------------------
---  Threespheres.
---
---  This is the shape made of a union of three equal spheres that
---  have one and only one common point. The current calculations are
---  based on calculations made by Anton Kulchitsky.
---
--- Center of coordinates is in the centroid which is the common point
--- of the spheres. z direction is perpendicular to the plane formed
--- by centers of the spheres. y direction is going through the center
--- of the second sphere. x direction is going through 3d
--- sphere. Although, x and y directions can be chosen arbitrary
--- because moments of inertia are the same for all directions
--- perpendicular to z.
---
--- @tparam number r radius
--- @tparam string gname -- Name of the group
--- @tparam string mname -- Name of the material
--- @treturn table three-sphere shape
-------------------------------------------------------------------------------
function shape.threespheres( r, gname, mname )

   local group  = hdis.groups:resolve( gname )
   local imat   = hdis.material:resolve( mname )

   local rho = hdis.material.list[ imat ].rho

   local V = ( 9 * math.sqrt( 3 ) / 4 ) * math.pi * r^3
   local m = V * rho
   local Ix = 8.749 * rho * r^5
   local Iy = Ix
   local Iz = 2.5 * rho * r^5

   local body = {
         -- we do not specify center of mass here
      v = { 0, 0, 0 },         -- initial velocity (m/s)
      w = { 0, 0, 0 },         -- initial angular vel (rad/s)
      m = m,                   -- mass (kg)
      V = V,                   -- volume (m^3)
      I = { Ix, Iy, Iz },      -- main values of inertia tensor
      q = { 1, 0, 0, 0 },      -- initial rotation quaternion
   }

   local atoms = {
      -- 3 spherical atoms
      {
         R = r,
         group = group,
         material = imat,
         sbb_c = { -math.sqrt(3)*r/2, -r/2, 0 },
         sbb_R = r,
         nverts = 1,
         nedges = 0,
         nfaces = 0,
         ivert0 = 1,
         iedge0 = 1,
         iface0 = 1,
         isolated = 1,
      },
      {
         R = r,
         group = group,
         material = imat,
         sbb_c = { 0, r, 0 },
         sbb_R = r,
         nverts = 1,
         nedges = 0,
         nfaces = 0,
         ivert0 = 2,
         iedge0 = 1,
         iface0 = 1,
         isolated = 1,
      },
      {
         R = r,
         group = group,
         material = imat,
         sbb_c = { math.sqrt(3)*r/2, -r/2, 0 },
         sbb_R = r,
         nverts = 1,
         nedges = 0,
         nfaces = 0,
         ivert0 = 3,
         iedge0 = 1,
         iface0 = 1,
         isolated = 1,
      },
   }

   local faces = { }
   local edges = { }

   local verts = {
    {
       iadj_e = 1,
       deg = 0,
       local_pos = { -math.sqrt(3)*r/2, -r/2, 0 },
       global_pos = { -math.sqrt(3)*r/2, -r/2, 0 },
    },
    {
      iadj_e = 1,
      deg = 0,
      local_pos = { 0, r, 0 },
      global_pos = { 0, r, 0 },
    },
    {
      iadj_e = 1,
      deg = 0,
      local_pos = { math.sqrt(3)*r/2, -r/2, 0 },
      global_pos = { math.sqrt(3)*r/2, -r/2, 0 },
    }
  }

   local adj_e = { }

   return {
      body=body,
      atoms=atoms,
      faces=faces,
      edges=edges,
      verts=verts,
      adj_e=adj_e
          }

end

-------------------------------------------------------------------------------
---  fourspheres (or tetraspheres)
---
---  This is the shape made of a symmetrical union of 4 equal spheres
---  with centers placed in vertices of a tetrahedron, that have one
---  and only one common point. The current calculations are based on
---  calculations made by Anton Kulchitsky.
---
--- Center of coordinates is in the centroid which is the common point
--- of the spheres.
---
--- @tparam number r radius
--- @tparam string gname -- Name of the group
--- @tparam string mname -- Name of the material
--- @treturn table three-sphere shape
-------------------------------------------------------------------------------
function shape.fourspheres( r, gname, mname )

   local group  = hdis.groups:resolve( gname )
   local imat   = hdis.material:resolve( mname )

   local rho = hdis.material.list[ imat ].rho

   local kv = 7 * math.sqrt(2/3) - 2
   local V = (4/3) * math.pi * r^3 * kv
   local m = V * rho

   -- moments for tetrahedron (temporarily)
   local kI = 17.52
   local h = (1/math.sqrt(3)) * r -- a/2
   local Ix = kI * r^5 * rho
   local Iy = Ix
   local Iz = Ix

   local body = {
         -- we do not specify center of mass here
      v = { 0, 0, 0 },         -- initial velocity (m/s)
      w = { 0, 0, 0 },         -- initial angular vel (rad/s)
      m = m,                   -- mass (kg)
      V = V,                   -- volume (m^3)
      I = { Ix, Iy, Iz },      -- main values of inertia tensor
      q = { 1, 0, 0, 0 },      -- initial rotation quaternion
   }

   local atoms = {
      -- 4 spherical atoms
      {
         R = r,
         group = group,
         material = imat,
         sbb_c = { -h,  h,  h },
         sbb_R = r,
         nverts = 1,
         nedges = 0,
         nfaces = 0,
         ivert0 = 1,
         iedge0 = 1,
         iface0 = 1,
         isolated = 1,
      },
      {
         R = r,
         group = group,
         material = imat,
         sbb_c = {  h,  h, -h },
         sbb_R = r,
         nverts = 1,
         nedges = 0,
         nfaces = 0,
         ivert0 = 2,
         iedge0 = 1,
         iface0 = 1,
         isolated = 1,
      },
      {
         R = r,
         group = group,
         material = imat,
         sbb_c = { -h, -h, -h },
         sbb_R = r,
         nverts = 1,
         nedges = 0,
         nfaces = 0,
         ivert0 = 3,
         iedge0 = 1,
         iface0 = 1,
         isolated = 1,
      },
      {
         R = r,
         group = group,
         material = imat,
         sbb_c = {  h, -h,  h },
         sbb_R = r,
         nverts = 1,
         nedges = 0,
         nfaces = 0,
         ivert0 = 4,
         iedge0 = 1,
         iface0 = 1,
         isolated = 1,
      },
   }

   local faces = { }
   local edges = { }

   local verts = {
    {
       iadj_e = 1,
       deg = 0,
       local_pos = { -h,  h,  h },
       global_pos = { -h,  h,  h },
    },
    {
      iadj_e = 1,
      deg = 0,
      local_pos = {  h,  h, -h },
      global_pos = {  h,  h, -h },
    },
    {
      iadj_e = 1,
      deg = 0,
      local_pos = { -h, -h, -h },
      global_pos = { -h, -h, -h },
    },
    {
      iadj_e = 1,
      deg = 0,
      local_pos = {  h, -h,  h },
      global_pos = {  h, -h,  h },
    }
  }

   local adj_e = { }

   return {
      body=body,
      atoms=atoms,
      faces=faces,
      edges=edges,
      verts=verts,
      adj_e=adj_e
          }
end

--*-------------------------------------------------------------------------*--
hdis.shape = shape
return shape