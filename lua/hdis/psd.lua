--[[--

   COUPi module for particle size distribution calculations.

   <p>Provides a class and set of functions to compute particle size
   distributions by mass, volume, and number for particles in COUPi
   HDF5 output files</p>

   <p>It uses OOP style, so "new" method returns an object and all
   other functions need to be called via this object. See example
   @{pdist.lua} for more usage instructions.</p>

   <p>Not implemented yet: Useful possible additions for the future
   could be selection of bodies by geometrical region and using user's
   function for equivalent diameter (characteristic size)
   calculations. Another possible feature could be to include
   logarithmic or any uneven split of the [dmin,dmax] interval</p>

   <p><em>For licensing and copyright use same license as
   COUPi.</em></p>

   @module hdis.psd
   @author Anton Kulchitsky

--]]

local psd = {}
local mt = {}                   -- metatable

-- Using library for reading HDF5s

require 'hdis'

local hdf5 = require 'hdis.hdf5'
local aux  = require 'hdis.aux'

--*------------------------------------------------------------*--
--*
--*------------------------------------------------------------*--

mt.__index = psd

--*------------------------------------------------------------*--
--*
--*------------------------------------------------------------*--

--- Creates new particle size distribution object for a file "fname".
--- @tparam string fname File name to be read.
--- @treturn new PSD object that can be used to calculate distribution.
function psd:new( fname )

   -- initialization
   local maintable = {}

   maintable.filename = fname
   maintable.bodies   = nil
   maintable.M        = nil

   return setmetatable( maintable, mt )
end

--- Reads bodies, atoms, groups and materials from HDF5
--- @treturn number number of bodies in the file
--- @treturn number of atoms in the file
function psd:read()
   -- Reading of atoms and points
   local h5bodies  = hdf5.read( self.filename, "Bodies" )
   local h5atoms = hdf5.read( self.filename, "Atoms" )

   self.bodies = {}
   for i = 1, h5bodies.n do
      table.insert( self.bodies, h5bodies[i-1] )
   end

   self.atoms = {}
   for i = 1, h5atoms.n do
      table.insert( self.atoms, h5atoms[i-1] )
   end

   hdis.runfrom( self.filename )

   return #self.bodies, #self.atoms
end

--- Selects bodies for atoms that belong to a specific group
--- @tparam string gname
--- @treturn table returns same psd object to make chain selects if
--- necessary
function psd:select( gname )

   local ngroup = hdis.groups:resolve( gname )

   for i,A in ipairs( self.atoms ) do
      local gid   = A.group_id  + 1
      local ibody = A.body_indx + 1

      if gid == ngroup then
         self.bodies[ibody].marked = true
      end
   end

   return self

end

--

local Cequiv = 6.0/math.pi

local function diam_equiv( V )
   return ( V * Cequiv )^(1/3)
end

--- Calculates particle size distributions by volume, mass, and
--- number.
--- @tparam number Nr of subintervals that evenly divide the interval
--- [dmin,dmax] for calculations.
--- @treturn table I, the distribution as a table with main fields
--- d, dvr, dmr, dnr that are the (d) center of the interval (dvr)
--- volume distribution value at d, (dmr) mass distribution value at
--- d, (dnr) number distribution value at d. This table is equal to
--- false if the distribution is delta function.
--- @treturn number dmin minimum characteristic size in the system
--- @treturn number dmax maximum characteristic size in the system
--- @treturn number V total volume for all the particles
--- @treturn number M total mass for all the particles
--- @treturn number N total number for all the particles

function psd:psd( Nr )

   -- calcluate total and max/min values

   local V = 0                  -- total volume
   local M = 0                  -- total mass
   local N = 0                  -- total number of prts
   local dmin = math.huge       -- min equiv. diam
   local dmax = 0               -- max equiv. diam

   for k,B in ipairs( self.bodies ) do

      -- only marked bodies
      if B.marked then
         V = V + B.V
         M = M + B.m
         N = N + 1
         local d = diam_equiv( B.V )

         B.d = d                -- adding d equiv to the body
         if dmin > d then dmin = d end
         if dmax < d then dmax = d end
      end

   end

   self.dmin = dmin
   self.dmax = dmax
   self.V    = V
   self.M    = M
   self.N    = N

   if dmin == dmax then
      return false, dmin, dmax, N, V, M --  delta function indicator
   end

   -- We have [dmin,dmax] segment and we divide it by Nr points
   local I = {}
   local dI = ( dmax - dmin ) / Nr

   -- initialization
   for i = 1, Nr do

      -- forming the table
      local element = {}
      element.V = 0
      element.m = 0             -- mass
      element.n = 0
      element.d = dmin + (i-1)*dI + dI/2 -- center of the segment
      element.dI = dI

      table.insert( I, element )
   end

   -- filling the values of mass intervals
   for k,B in ipairs( self.bodies ) do

      if B.marked then

         local d = B.d

         -- index does not work for dmax only
         local i = 1 + math.floor( ( d - dmin ) / dI )
         if i == Nr+1 then i = Nr end

         I[i].V = I[i].V + B.V
         I[i].m = I[i].m + B.m
         I[i].n = I[i].n + 1
      end

   end

   -- additional values to fulfil the distribution all we do is adding
   -- dmr and dVr values such that dm = m/M/dr
   for i,e in ipairs(I) do
      e.dvr = e.V / ( self.V * e.dI )
      e.dmr = e.m / ( self.M * e.dI )
      e.dnr = e.n / ( self.N * e.dI )
   end

   return I, dmin, dmax, V, M, N

end

--*------------------------------------------------------------*--

return psd
