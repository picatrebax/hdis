--[[--

   COUPi 3D rotational matrix library.

   A 3x3 matrix class with all main operations defined. The cooridnates
   are stored inits in the 'm'. Use direct access to the
   fields to reach components.

   <p><em>For licensing and copyright use same license as
   COUPi.</em></p>

   @module hdis.rot3d
   @author Anton Kulchitsky

   A couple of changes/additions by Ihor Durnopianov :)

--]]

require 'hdis'

local rot3d = {}
local vec3d = require 'hdis.vec3d'
local quat  = require 'hdis.quat'
local aux = require 'hdis.aux'

------------------------------------------------------------------

----------------------------------------------------------------
--- Matrix constructor. Returns a new matrix.
---
--- If called without parameters or with wrong parameters, it returns
--- a matrix {1,0,0}x{0,1,0}x{0,0,1}
---
--- Possible calls:
---
--- rot3d:new() -- unit matrix
---
--- rot3d:new( v1, v2, v3 ) -- a matrix { v1 } x { v2 } x { v3 }
--- v1, v2 and v3 must be vectors ( type == 'vec3d' )
---
--- rot3d:new( a, b, c ) -- a matrix of the rotation on c around an axis
--- which is defined by in-plane angle a and out-of-plane angle b
---
--- rot3d:new( Q ) --- creation of a matrix from quaternion
---
--- @param ... list of parameters
--- @treturn table a new initialized 3D vector
----------------------------------------------------------------

function rot3d:new(...)
   -- creating new matrix (columns)
   local M = { m = {{1,0,0},{0,1,0},{0,0,1}} }
   setmetatable( M, self )
   self.__index = self
   self.type = 'rot3d'

   -- filling the v field
   local args = table.pack(...)

   -- If no argument => default unit matrix

   if args.n == 1 then
      local q = args[1]
      assert(q.type == 'quat')

      local s = q.q[1]
      local x = q.q[2]
      local y = q.q[3]
      local z = q.q[4]

      local tx = 2*x;
      local ty = 2*y;
      local tz = 2*z;

      local tsx = s*tx;
      local tsy = s*ty;
      local tsz = s*tz;

      local txx = x*tx;
      local txy = x*ty;
      local txz = x*tz;

      local tyy = y*ty;
      local tyz = y*tz;

      local tzz = z*tz;

      M.m[1][1] = 1-tyy-tzz
      M.m[1][2] = txy-tsz
      M.m[1][3] = txz+tsy

      M.m[2][1] = txy+tsz
      M.m[2][2] = 1-txx-tzz
      M.m[2][3] = tyz-tsx

      M.m[3][1] = txz-tsy
      M.m[3][2] = tyz+tsx
      M.m[3][3] = 1-txx-tyy
    end

   if args.n == 3 then
      if type( args[1] ) == 'table' and
         type( args[2] ) == 'table' and
         type( args[3] ) == 'table' then
         local ex = args[1]
         local ey = args[2]
         local ez = args[3]
         assert( ex.type == 'vec3d' and ey.type == 'vec3d' and ez.type == 'vec3d' )

         M.m[1][1] = ex.v[1]
         M.m[1][2] = ex.v[2]
         M.m[1][3] = ex.v[3]

         M.m[2][1] = ey.v[1]
         M.m[2][2] = ey.v[2]
         M.m[2][3] = ey.v[3]

         M.m[3][1] = ez.v[1]
         M.m[3][2] = ez.v[2]
         M.m[3][3] = ez.v[3]
      elseif type( args[1] ) == 'number' and
             type( args[2] ) == 'number' and
             type( args[3] ) == 'number' then
         local axis = { math.cos( args[2] )*math.cos( args[1] ),
                        math.cos( args[2] )*math.sin( args[1] ),
                        math.sin( args[2] ) }
         local c = math.cos( args[3] )
         local s = math.sin( args[3] )

         M.m[1][1] = c+axis[1]^2*( 1-c )
         M.m[1][2] = axis[1]*axis[2]*( 1-c )-axis[3]*s
         M.m[1][3] = axis[1]*axis[3]*( 1-c )+axis[2]*s

         M.m[2][1] = axis[2]*axis[1]*( 1-c )+axis[3]*s
         M.m[2][2] = c+axis[2]^2*( 1-c )
         M.m[2][3] = axis[2]*axis[3]*( 1-c )-axis[1]*s

         M.m[3][1] = axis[3]*axis[1]*( 1-c )-axis[2]*s
         M.m[3][2] = axis[3]*axis[2]*( 1-c )+axis[1]*s
         M.m[3][3] = c+axis[3]^2*( 1-c )

      end
   end

   return M

end

------------------------------------------------------------------
--- Direct access of the data
------------------------------------------------------------------
function rot3d:value()
   return self.m
end

------------------------------------------------------------------
--- Sum of vectors
------------------------------------------------------------------
function rot3d:__add( x )
   for i=1,3 do
      for j=1,3 do
         self.m[i][j] = self.m[i][j] + x.m[i][j]
      end
   end
   return self
end

------------------------------------------------------------------
--- String representing 3D vector for pretty print
------------------------------------------------------------------
function rot3d:__tostring()
   return string.format( '[ %f %f %f\n  %f %f %f\n  %f %f %f ]',
                         self.m[1][1], self.m[1][2], self.m[1][3],
                         self.m[2][1], self.m[2][2], self.m[2][3],
                         self.m[3][1], self.m[3][2], self.m[3][3] )
end


------------------------------------------------------------------
--- Matrix transpose/inversion (since it is a ROTATION matrix)
------------------------------------------------------------------
function rot3d:transpose()
   local M_transposed = rot3d:new()

   M_transposed.m[1][1] = self.m[1][1]
   M_transposed.m[1][2] = self.m[2][1]
   M_transposed.m[1][3] = self.m[3][1]

   M_transposed.m[2][1] = self.m[1][2]
   M_transposed.m[2][2] = self.m[2][2]
   M_transposed.m[2][3] = self.m[3][2]

   M_transposed.m[3][1] = self.m[1][3]
   M_transposed.m[3][2] = self.m[2][3]
   M_transposed.m[3][3] = self.m[3][3]

   return M_transposed
end

------------------------------------------------------------------
--- Matrix-vector and matrix-matrix multiplication
--- Up to me it's better that the fuction apply()
--- because it allows to use the expressions like
--- V:transpose()*x
--- There is no way to use apply() the transpose()d
--- matrix in one expression - one would need
--- a temporary variable
------------------------------------------------------------------
function rot3d:__mul( x )

   assert( x.type == 'vec3d' or x.type == 'rot3d' )

   if x.type == 'vec3d' then

      local ax = self.m[1][1] * x.v[1] +
                 self.m[1][2] * x.v[2] +
                 self.m[1][3] * x.v[3]

      local ay = self.m[2][1] * x.v[1] +
                 self.m[2][2] * x.v[2] +
                 self.m[2][3] * x.v[3]

      local az = self.m[3][1] * x.v[1] +
                 self.m[3][2] * x.v[2] +
                 self.m[3][3] * x.v[3]

      return vec3d:new( ax, ay, az )

   elseif x.type == 'rot3d' then

      local result = rot3d:new()

      result.m[1][1] = self.m[1][1]*x.m[1][1] +
                       self.m[1][2]*x.m[2][1] +
                       self.m[1][3]*x.m[3][1]

      result.m[1][2] = self.m[1][1]*x.m[1][2] +
                       self.m[1][2]*x.m[2][2] +
                       self.m[1][3]*x.m[3][2]

      result.m[1][3] = self.m[1][1]*x.m[1][3] +
                       self.m[1][2]*x.m[2][3] +
                       self.m[1][3]*x.m[3][3]

      result.m[2][1] = self.m[2][1]*x.m[1][1] +
                       self.m[2][2]*x.m[2][1] +
                       self.m[2][3]*x.m[3][1]

      result.m[2][2] = self.m[2][1]*x.m[1][2] +
                       self.m[2][2]*x.m[2][2] +
                       self.m[2][3]*x.m[3][2]

      result.m[2][3] = self.m[2][1]*x.m[1][3] +
                       self.m[2][2]*x.m[2][3] +
                       self.m[2][3]*x.m[3][3]

      result.m[3][1] = self.m[3][1]*x.m[1][1] +
                       self.m[3][2]*x.m[2][1] +
                       self.m[3][3]*x.m[3][1]

      result.m[3][2] = self.m[3][1]*x.m[1][2] +
                       self.m[3][2]*x.m[2][2] +
                       self.m[3][3]*x.m[3][2]

      result.m[3][3] = self.m[3][1]*x.m[1][3] +
                       self.m[3][2]*x.m[2][3] +
                       self.m[3][3]*x.m[3][3]

      return result

   end

end

------------------------------------------------------------------
--- @treturn apply matrix to a vector
------------------------------------------------------------------
function rot3d:apply( x )
   assert( x.type == 'vec3d' )

   local ax = self.m[1][1] * x.v[1] +
              self.m[2][1] * x.v[2] +
              self.m[3][1] * x.v[3]

   local ay = self.m[1][2] * x.v[1] +
              self.m[2][2] * x.v[2] +
              self.m[3][2] * x.v[3]

   local az = self.m[1][3] * x.v[1] +
              self.m[2][3] * x.v[2] +
              self.m[3][3] * x.v[3]

   return vec3d:new( ax, ay, az )
end

------------------------------------------------------------------
--- @treturn restore rotation matrix by quaternion
--- I tend to remove this function, since I do not see any
--- reasons for somebody to create a matrix, and then restore()
--- it from quaternion. Why don't we simply make a new()
--- matrix from quaternion?
------------------------------------------------------------------

--[[
function rot3d:restore( q )

   assert( q.type == 'quat' )

   local result = rot3d:new()

   local s = q.q[1]
   local x = q.q[2]
   local y = q.q[3]
   local z = q.q[4]

   local tx = 2*x;
   local ty = 2*y;
   local tz = 2*z;

   local tsx = s*tx;
   local tsy = s*ty;
   local tsz = s*tz;

   local txx = x*tx;
   local txy = x*ty;
   local txz = x*tz;

   local tyy = y*ty;
   local tyz = y*tz;

   local tzz = z*tz;

   result.m[1][1] = 1-tyy-tzz
   result.m[1][2] = txy-tsz
   result.m[1][3] = txz+tsy

   result.m[2][1] = txy+tsz
   result.m[2][2] = 1-txx-tzz
   result.m[2][3] = tyz-tsx

   result.m[3][1] = txz-tsy
   result.m[3][2] = tyz+tsx
   result.m[3][3] = 1-txx-tyy

   return result

end
--]]

------------------------------------------------------------------

return rot3d