--[[---------------------------------------------------------------------------
   Smooth spherical particles.

   @module hdis.hydro
   @copyright 2017 Coupi, Inc.
--]]---------------------------------------------------------------------------

local hydro = {}

hydro.flag = {}
hydro.flag.free     = 0x1
hydro.flag.boundary = 0x2
hydro.flag.still    = 0x4

-- volume multiplier received from max sphere packing
hydro.vmult = 4 * math.sqrt(2)
   
-------------------------------------------------------------------------------
--- Smooth particle constructor.
--- @tparam number h -- smoothing length (constant for this model)
--- @tparam number rho -- density
--- @tparam number nu -- viscosity
--- @tparam vector v -- linear velocity
--- @tparam vector c -- position
--- @tparam string group -- group name
--- @tparam number flags -- binary flags
--- @tparam number km -- mass enhancing coefficient (mostly for boundaries)
--- @treturn table -- dot's table that is ready for hdis.append
-------------------------------------------------------------------------------
function hydro.new( h, rho, nu, v, c, group, flags, km )

   if not flags then flags = hydro.flag.free end
   if not km then km = 1 end
   
   local dot = {}
   dot.type    = 'hydro'
   dot.h       = h
   dot.rho     = rho
   dot.nu      = nu
   dot.v       = v
   dot.c       = c
   dot.group   = hdis.groups:resolve( group )
   dot.flags   = flags

   dot.m = km * hydro.vmult * dot.rho * h * h * h

   -- for bounding radius we use larger value than h. (1) due to SPH-DEM
   -- interaction (2) due to possible different kernels than M4.
   --dot.R = 2 * dot.h
   dot.R =  dot.h

   return dot
end

---------------------------------------------------------------------------------
--- EXTRA HELPER FUNCTIONS
---------------------------------------------------------------------------------

local vec3d = require 'hdis.vec3d'

-------------------------------------------------------------------------------
--- Boarder wall constructor
-------------------------------------------------------------------------------
function hydro.wall( r0, r1, dr, h, rho, nu, gname, km )

   local wall = {}
   local km = km or 1.4
   local v = vec3d:new()
   
   -- forming grid
   
   local xstart  = r0[1]
   local xend    = r1[1]
   local nx = ( xend - xstart ) / dr[1]
   if nx < 1 then nx = 1 end
   local dx = ( xend - xstart ) / nx
   if dx == 0 then dx = 1 end

   local ystart  = r0[2]
   local yend    = r1[2]
   local ny = ( yend - ystart ) / dr[2]
   if ny < 1 then ny = 1 end
   local dy = ( yend - ystart ) / ny
   if dy == 0 then dy = 1 end

   local zstart  = r0[3]
   local zend    = r1[3]
   local nz = ( zend - zstart ) / dr[3]
   if nz < 1 then nz = 1 end
   local dz = ( zend - zstart ) / nz
   if dz == 0 then dz = 1 end

   for z = zstart, zend+dz/100, dz do
      for y = ystart, yend+dy/100, dy do
         for x = xstart, xend+dx/100, dx do

            -- forming hydro particles
            local c = vec3d:new( x, y, z )
            local sphdot = hydro.new(
               h, rho, nu, v, c, gname,
               bit32.bor( hydro.flag.still, hydro.flag.boundary ), km )
            table.insert( wall, sphdot )
            
         end
      end
   end

   return wall
end


---------------------------------------------------------------------------------

return hydro
