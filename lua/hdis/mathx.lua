--[[--

   Coupi library for additional mathematical functions.

   <p><em>For licensing and copyright use same license as Coupi.</em></p>

   @module hdis.mathx
   @copyright 2016 Coupi, Inc.

--]]

require 'hdis'
local mathx = {}
local quat = require 'hdis.quat'

math.randomseed( os.time() ) -- we globally initialize the random
                             -- number generator

--------------------------------------------------------------------------------
--              Probability distribution functions
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--- Gaussian distribution function with mu=0 using polar (Box-Mueller)
--- method. It is very stable, although is not the fastest
--- algorithm. It is very fast anyway.
---
--- @tparam number sigma sigma of normal distribution (default is 1)
--- @treturn number Normally distributed value with mu=0, and set
--- sigma
--- @treturn number another independent normally distributed value
--- with mu=0, and set sigma
--------------------------------------------------------------------------------
function mathx.gaussian_polar( sigma )
   local x, y
   local sigma = sigma or 1.0

   local r2 = 2.0

   while r2 >= 1.0 or r2 == 0 do

      local u1 = math.random()
      local u2 = math.random()

      -- choose x,y in a square from -1,-1 to 1,1
      v1 = -1.0 + 2.0 * u1
      v2 = -1.0 + 2.0 * u2

      -- we want it only in a circle, actually
      r = v1*v1 + v2*v2
   end

   -- Box-Muller transform
   local s = math.sqrt( - 2 * log( r ) / r )
   local x1 = sigma * v1 * s
   local x2 = sigma * v2 * s

   return x1, x2
end

--------------------------------------------------------------------------------
--- Discrete distribution function. Function takes table of probabilities (sum
--- of probabilities should be equal to 1. In any case it will re-normalize it.
--- So you could give a table of sampling and it will transform it into
--- probabilities ) and table of relevant values and returns a value from
--- appeared diapason.
--- @tparam table probability - table of probabilities for each diapason
--- @tparam table value - table of values for each diapason
--- @treturn number - value that correspond appeared diapason.
--------------------------------------------------------------------------------
function mathx.discrete( probability, value )
   --normalization of probability
   local sum = probability[1]
   for i  = 2,#probability do
      sum = sum + probability[i]
   end
   local lpt = { probability[1] / sum } --local table for probabilities
   for i = 2,#probability do
      table.insert( lpt, lpt[i-1] + ( probability[i] / sum ) )
   end

   sum = math.random() --simply to avoid of creating of a new variable
   for i = 1,#lpt-1 do
      if sum < lpt[i] then return value[i] end
   end

   return value[#lpt]
end

--------------------------------------------------------------------------------
---          Log-normal distribution
---
--- @tparam number m mean value
--- @tparam number sigma sigma of normal distribution (default is 1)
--- @treturn number log-normally distributed value with m, and set
--- sigma
--------------------------------------------------------------------------------
function mathx.lognormal_polar( m, sigma )
   return math.exp( m + sigma * hdis.mathx.gaussian_polar() )
end

--------------------------------------------------------------------------------
-- Helper function
--------------------------------------------------------------------------------
function irvin_hall()
    
    s=0
    for i=1,12 do
        s=s+2*math.random() -1
    end
    return s/2
end 

--------------------------------------------------------------------------------
-- Helper function
--------------------------------------------------------------------------------
function box_muller()
    
    repeat
        x=math.random()*2-1
        y=math.random()*2-1
        s=x*x+y*y
    until s>0 and s<=1
    
    return x*math.sqrt(-2 * math.log(s) / s)
end    

--------------------------------------------------------------------------------
---          Normal distribution
---
--- @tparam number m mean value
--- @tparam number variance variance of normal distribution (default is 1)
--- @treturn number normally distributed value with m, and set variance
--------------------------------------------------------------------------------
function mathx.normal( m, variance )
    
    if not m then m = 0 end
    if not s2 then s2 = 1 end  
    return irvin_hall() * math.sqrt( variance ) + m;
end

--------------------------------------------------------------------------------
---          Log-normal distribution
---
--- @tparam number m mean value
--- @tparam number variance variance of normal distribution (default is 1)
--- @treturn number log-normally distributed value with m, and set variance
--------------------------------------------------------------------------------
function mathx.lognormal( m, variance )
   return math.exp( mathx.normal( m, variance ) )
end

--------------------------------------------------------------------------------
---          Log-normal distribution
---
--- @tparam number left left bound
--- @tparam number right right bound
--- @treturn number log-normally distributed value from left to right
--------------------------------------------------------------------------------
function mathx.lognormal_in_bounds( left, right )
   
    local a = math.log(left)
    local b = math.log(right)
    local m = (a + b) / 2
    local D = (b - a) * (b - a) / 12
    local solution = math.lognormal( m, D )
    while ( solution < left ) or ( solution > right) do
      solution = math.lognormal( m, D )
    end
    return solution
end

----------------------------------------------------------------
---          Random Orientation Function
----------------------------------------------------------------

-- The function returns a totally random orientation quaternion. It is
-- sometimes very convenient to set up all bodies randomly oriented in
-- space.

function mathx.q_random()
   local a = 0
   local a1, a2, a3

   -- we avoiding almost impossible case when all generated random
   -- numbers are equal to 0.5
   while a == 0 do
      a1 = math.random() - 0.5
      a2 = math.random() - 0.5
      a3 = math.random() - 0.5
      a = math.sqrt( a1*a1 + a2*a2 + a3*a3 )
   end
      
   local cosa = math.random()
   local sina = math.sqrt( 1.0 - cosa*cosa )
   
   return quat:new( cosa, a1*sina/a, a2*sina/a, a3*sina/a )
end

----------------------------------------------------------------
---          Spline for gradual change of something
---
--- @tparam number x0 left side of the interval
--- @tparam number x1 right side of the interval
--- @tparam number f0 left value of the spline
--- @tparam number f1 right value of the spline
--- @treturn function spline function such that f(x0)=f0, f(x1)=f1,
--- f'(x0)=f'(x1)=0
----------------------------------------------------------------
function mathx.spline_change( x0, x1, f0, f1 )

   local DX = x1 - x0
   local a = - 2.0 * ( f1 - f0 ) / ( DX*DX*DX )
   local b = - 1.5 * DX * a

   return function( x )
      local D = x - x0
      local D2 = D*D
      local D3 = D*D*D
      return f0 + a * D3 + b * D2
   end

end

hdis.mathx = mathx
return mathx