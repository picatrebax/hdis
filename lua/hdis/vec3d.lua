--[[--

   COUPi 3D Vector library.
   
   A vector class with all main operations defined. The cooridnates
   are stored inits in the "v". Use direct access to the
   fields to reach components.

   <p><em>For licensing and copyright use same license as
   COUPi.</em></p>

   @module hdis.vec3d
   @author Anton Kulchitsky

--]]

local vec3d = {}

------------------------------------------------------------------

----------------------------------------------------------------
--- Vector constructor. Returns a new vector.
---
--- If called without parameters or with wrong parameters, it returns
--- a vector {1,0,0}
---
--- Possible calls:
---
--- vec3d:new( x, y, z )
---
--- vec3d:new( v ) -- v is vector of the same class
---
--- vec3d:new()  -- returns ex unit vector {1,0,0}
---
--- @param ... list of parameters
--- @treturn table a new initialized 3D vector
----------------------------------------------------------------
function vec3d:new(...)

   -- creating new vector, default is zero
   local V = { v = {0,0,0} }
   setmetatable( V, self )
   self.__index = self
   self.type = "vec3d"

   -- filling the v field
   local args = table.pack(...)

   -- If no argument => default vx, moreover always ex for all
   -- unrecognized cases
   if args.n == 1 then
      
      if type( args[1] ) == "table" then
         if #args[1] == 3 then
            V.v[1] = args[1][1]
            V.v[2] = args[1][2]
            V.v[3] = args[1][3]
         elseif args[1].type == "vec3d" then
            V.v[1] = args[1].v[1]
            V.v[2] = args[1].v[2]
            V.v[3] = args[1].v[3]
         end
      end         

   elseif args.n == 3 then
      V.v[1] = args[1]
      V.v[2] = args[2]
      V.v[3] = args[3]
   end

   return V

end

------------------------------------------------------------------
--- Direct access of the data
------------------------------------------------------------------
function vec3d:value()
   return self.v
end

------------------------------------------------------------------
--- String representing 3D vector for pretty print
------------------------------------------------------------------
function vec3d:__tostring()
   return string.format( "(%f,%f,%f)", 
                         self.v[1], self.v[2], self.v[3] )
end

------------------------------------------------------------------
--- Sum of vectors
------------------------------------------------------------------
function vec3d:__add( x )
   local s = vec3d:new( self.v[1]+x.v[1],
                        self.v[2]+x.v[2],
                        self.v[3]+x.v[3] )
   return s
end


------------------------------------------------------------------
--- Sub of 3d vectors
------------------------------------------------------------------
function vec3d:__sub( x )
   local s = vec3d:new( self.v[1]-x.v[1],
                       self.v[2]-x.v[2],
                       self.v[3]-x.v[3] )
   return s
end

------------------------------------------------------------------
--- Returns a vector with opposite sign
------------------------------------------------------------------
function vec3d:__unm()
   local s = vec3d:new( -self.v[1], -self.v[2], -self.v[3]  )
   return s
end

------------------------------------------------------------------
--- == Compares vectors
------------------------------------------------------------------
function vec3d:__eq( x )
   return ( self.v[1] == x.v[1] and 
               self.v[2] == x.v[2] and 
               self.v[3] == x.v[3] )
end

------------------------------------------------------------------
--- scalar dot product of two vectors
------------------------------------------------------------------
function vec3d:__mul( x )
	local a = self.v
   local b = x.v
   return a[1]*b[1] + a[2]*b[2] + a[3]*b[3]
end

------------------------------------------------------------------
--- @treturn number a square of the vector
------------------------------------------------------------------
function vec3d:square()
   local V = self.v
   return ( V[1]*V[1] + V[2]*V[2] + V[3]*V[3] )
end

------------------------------------------------------------------
--- @treturn number a norm of the vector
------------------------------------------------------------------
function vec3d:norm()
   return math.sqrt( self:square() )
end

------------------------------------------------------------------
--- @treturn vector a self vector after normalization
------------------------------------------------------------------
function vec3d:normalize()
   local n = self:norm()
   return self:scale( 1/n )
end

------------------------------------------------------------------
--- @treturn number abs value of the vector (same as norm)
------------------------------------------------------------------
function vec3d:abs()
   return math.sqrt( self:square() )
end

------------------------------------------------------------------
--- @treturn vec3d new vector scaled by k
------------------------------------------------------------------
function vec3d:scale( k )
   local s = vec3d:new( k* self.v[1], k* self.v[2], k* self.v[3] )
   return s
end

------------------------------------------------------------------
--- @treturn number x coordinate of the vec3d
------------------------------------------------------------------
function vec3d:x()

   return self.v[1]
end

------------------------------------------------------------------
--- @treturn number y coordinate of the vec3d
------------------------------------------------------------------
function vec3d:y()
   return self.v[2]
end

------------------------------------------------------------------
--- @treturn number z coordinate of the vec3d
------------------------------------------------------------------
function vec3d:z()
   return self.v[3]
end

------------------------------------------------------------------
--- @treturn set vector cooridinates to numbers x, y, z
------------------------------------------------------------------
function vec3d:set_xyz( x, y, z )
   self.v[1] = x
   self.v[2] = y
   self.v[3] = z
end

------------------------------------------------------------------
--- @treturn set vector cooridinates to numbers 0,0,0
------------------------------------------------------------------
function vec3d:set_null()
   self.v[1] = 0
   self.v[2] = 0
   self.v[3] = 0
end

------------------------------------------------------------------
--- @treturn assign vector to values of another vector a
------------------------------------------------------------------
function vec3d:set_assign(a)
   self.v[1] = a.v[1]
   self.v[2] = a.v[2]
   self.v[3] = a.v[3]
end

------------------------------------------------------------------
--- @treturn set vector value to the table (soft, pointerwise)
------------------------------------------------------------------
function vec3d:set_tbl( v )
   self.v = v
end

------------------------------------------------------------------
--- @treturn vec3d new vector cross product
------------------------------------------------------------------
function vec3d:cross( a )
   local s = vec3d:new()
   
   s.v[1] = self.v[2]*a.v[3] - self.v[3]*a.v[2]
   s.v[2] = self.v[3]*a.v[1] - self.v[1]*a.v[3]
   s.v[3] = self.v[1]*a.v[2] - self.v[2]*a.v[3]

   return s
end

------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------
--                In place functions
------------------------------------------------------------------
------------------------------------------------------------------

function vec3d:inplace_scale( k )
   self.v[1] = k * self.v[1]
   self.v[2] = k * self.v[2]
   self.v[3] = k * self.v[3]
end

------------------------------------------------------------------

return vec3d
