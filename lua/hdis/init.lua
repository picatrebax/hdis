--[[--

   The main module of Lua library for COUPi: required for all
   COUPi configuration files.

   <p><em>For licensing and copyright use same license as
   COUPi.</em></p>

   @module hdis
   @author Anton Kulchitsky

--]]

hdis = {}

-- penlight for some file access options
require 'pl'

-- automatically available modules
hdis.material = require 'hdis.material'
hdis.groups   = require 'hdis.groups'
hdis.callback = require 'hdis.callback'
hdis.hdf5     = require 'hdis.hdf5'
aux            = require 'hdis.aux'

--- hdisc generated bodies
hdis.hdisc = {}

--- bodies in system
hdis.bodies = {}

--- atoms in system
hdis.atoms = {}

--- faces in system
hdis.faces = {}

--- edges in system
hdis.edges = {}

--- verts in system
hdis.verts = {}

--- adjacent edges in system
hdis.adj_e = {}

--- points in system
hdis.points = {}

---springs in system
hdis.springs = {}

---pressure triangles in system
hdis.ptriangles = {}

---pressure triangles in system
hdis.dots = {}

--- sph particles
hdis.sph = {}

-------------------------------------------------------------------------------
--- Most general append function that appends shapes and meshes to the
--- model
--- @tparam table object either shape or mesh to be added to the
--- system
-------------------------------------------------------------------------------
function hdis.append( object )
   if object.body then hdis.append_shape( object ) return end
   if object.nodes then hdis.append_mesh( object ) return end
   if ( object.type == 'hydro' ) then hdis.append_hydro( object ) return end

   io.write( "Error (append): wrong object\n" )
end

--- This function adds a body to the list of bodies. It sets all links
--- properly.
--- @tparam table shape Shape to be added to the system
--- @treturn number Index of the added body in hdis.bodies list. This
--- index can be used for adding springs.
--- @see append
function hdis.append_shape ( shape )
  local iB = #hdis.bodies
  local iA = #hdis.atoms
  local iF = #hdis.faces
  local iE = #hdis.edges
  local iV = #hdis.verts
  local iJ = #hdis.adj_e
  local iP = #hdis.points
  local iC = hdis.nCnvxs

  -- We do not want to operate on input tables
  local newB   = shape.body
  local newAs  = shape.atoms
  local newFs  = shape.faces
  local newEs  = shape.edges
  local newVs  = shape.verts
  local newJs  = shape.adj_e
  local newPs  = shape.points

  -- Allows to use vector as coordinates of the center mass
  -- for more details see COUP-117 and COUP-112
  local errmsgnsp = "Error: Coordinates of the center mass were not"..
                    " specified for hdis.append"
  local errmsgwsp = "Error: Coordinates of the center mass were"..
                    " specified not as vector or table of coordinates"
  if newB.c then --it is not allowed to append an object without coordinates
                 -- of its center mass. nil will be perceived by lua as false.
    if type(newB.c) == 'table' then
      if newB.c.type == 'vec3d' then
        newB.c = newB.c.v --as vector is not true Lua table we change it by
                          -- its value. We will change it on its value not as
                          -- deep copy of its value, but as pointer on its
                          -- value because vector is table of tables and
                          -- contain only its value as a table and it will
                          -- allow as do not change old implementation and do
                          -- not lose something important from vector.
      else
        if not (#newB.c == 3) then -- if coordinates were sent as table but
                                   -- them not three
          io.write( errmsgwsp.."\n" )
          error( errmsgwsp )
        end
      end
    else
      io.write( errmsgwsp.."\n" )
      error( errmsgwsp )
    end
  else
    io.write( errmsgnsp.."\n" )
    error( errmsgnsp )
  end
  errmsgnsp = nil
  errmsgwsp = nil
  -- Allows to use vector3d as vector of the initial linear and angular
  -- velocity of the center mass (according to COUP-124)
  if type(newB.v) == 'table' then
    if newB.v.type == 'vec3d' then
      newB.v = newB.v.v -- as vector is not true Lua table we change it by its
                        -- value.
    end
  end
  if type(newB.w) == 'table' then
    if newB.w.type == 'vec3d' then
      newB.w = newB.w.v -- as vector is not true Lua table we change it by its
                        -- value.
    end
  end
  -- Allows to use object quaternion from hdis library as quaternion of
  -- object's body (according to COUP-125)
  if type(newB.q) == 'table' then
    if newB.q.type == 'quat' then
      newB.q = newB.q.q -- as vector is not true Lua table we change it by its
                        -- value.
    end
  end

  -- copying new body to the list
  hdis.bodies[ iB+1 ] = newB

   for ia, atom in ipairs( newAs ) do
      atom.body = iB + 1
      atom.ivert0 = atom.ivert0 + iV
      atom.iedge0 = atom.iedge0 + iE
      atom.iface0 = atom.iface0 + iF

      table.insert( hdis.atoms, atom )
   end

   for ifa, face in ipairs( newFs ) do
      face.iedge0 = face.iedge0 + iE
      face.iedge1 = face.iedge1 + iE
      face.iedge2 = face.iedge2 + iE

      face.ivert0 = face.ivert0 + iV
      face.ivert1 = face.ivert1 + iV
      face.ivert2 = face.ivert2 + iV

      table.insert( hdis.faces, face )
   end

   for ie, edge in ipairs( newEs ) do
      edge.itail = edge.itail + iV
      edge.ihead = edge.ihead + iV

      edge.ilface = edge.ilface + iF
      edge.irface = edge.irface + iF

      table.insert( hdis.edges, edge )
   end

   for iv, vert in ipairs( newVs ) do
      vert.body = iB + 1
      vert.iadj_e = vert.iadj_e + iJ

      table.insert( hdis.verts, vert )
   end

   for ij, adj in ipairs ( newJs ) do
      adj = adj + iE

      table.insert( hdis.adj_e, adj )
   end

   -- returing last body index
   return iB + 1
end

--- This function adds a hydro particle to the hdis.dots list.
--- @tparam table dot be added to the hdis table
--- @treturn number index of the added dot.
--- @see append
function hdis.append_hydro ( dot )

   hdis.dots[ #hdis.dots + 1 ] = {}

   local errmsg_begin = "Error: The dot's "
   local errmsg_end = " should be specified as table with length 3 or vec3d."

   -- dot's center mass
   local errmsg = errmsg_begin.."center of mass"..errmsg_end
   if dot.c then

      if ( type( dot.c ) == 'table' ) then

         if dot.c.type == 'vec3d' then

            hdis.dots[#hdis.dots].c = aux.deepcopy( dot.c.v )
         else

            if ( #dot.c == 3 ) then

               hdis.dots[#hdis.dots].c = aux.deepcopy( dot.c )
            else error( errmsg ) end
         end
      else error( errmsg ) end
   else error( errmsg ) end

   -- dot's linear speed
   errmsg = errmsg_begin.."linear speed"..errmsg_end
   if dot.v then

      if ( type( dot.v ) == 'table' ) then

         if dot.v.type == 'vec3d' then

            hdis.dots[#hdis.dots].v = aux.deepcopy( dot.v.v )
         else

            if ( #dot.v == 3 ) then

               hdis.dots[#hdis.dots].v = aux.deepcopy( dot.v )
            else error( errmsg ) end
         end
      else error( errmsg ) end
   else error( errmsg ) end

   -- radius
   errmsg_end = " should be specified as number."
   errmsg = errmsg_begin.."radius"..errmsg_end
   if dot.R then

      if ( type( dot.R ) == 'number' ) then

         hdis.dots[#hdis.dots].R = aux.deepcopy( dot.R )
      else error( errmsg ) end
   else error( errmsg ) end

   -- smoothing length
   errmsg_end = " should be specified as number."
   errmsg = errmsg_begin.."smoothing length"..errmsg_end
   if dot.h then
      if ( type( dot.h ) == 'number' ) then
         hdis.dots[#hdis.dots].h = aux.deepcopy( dot.h )
      else
         error( errmsg )
      end
   else
      error ( errmsg )
   end
   
   -- density
   errmsg = errmsg_begin.."density"..errmsg_end
   if dot.rho then

      if ( type( dot.rho ) == 'number' ) then

         hdis.dots[#hdis.dots].rho = aux.deepcopy( dot.rho )
      else error( errmsg ) end
   else error( errmsg ) end

   -- viscosity
   errmsg = errmsg_begin.."viscosity"..errmsg_end
   if dot.nu then

      if ( type( dot.nu ) == 'number' ) then

         hdis.dots[#hdis.dots].nu = aux.deepcopy( dot.nu )
      else error( errmsg ) end
   else error( errmsg ) end

   hdis.dots[#hdis.dots].m = dot.m

   hdis.dots[#hdis.dots].group_id = dot.group

   hdis.dots[#hdis.dots].flags = dot.flags
end

--- Adds a mesh to the system. It relies havily on @{append_shape}
--- function
--- @tparam table mesh Mesh usually returned by generators from module
--- hdis_mesh
--- @see append_shape
--- @see append
function hdis.append_mesh ( mesh )
   local iB = #hdis.bodies
   local iA = #hdis.atoms
   local iP = #hdis.points
   local iS = #hdis.springs
   local iT = #hdis.ptriangles

   -- just adding the bodies from the mesh, straightforward
   for ib, b in ipairs( mesh.nodes ) do
      hdis.append_shape( b )
   end

   -- we need to change all indices within springs and ptriangles

   -- spring traverse
   for i, s in ipairs( mesh.springs ) do
      s.body1 = s.body1 + iB
      s.body2 = s.body2 + iB
      table.insert( hdis.springs, s )
   end

   -- ptriangles traverse
   for i, t in ipairs( mesh.ptriangles ) do
      t.body1 = t.body1 + iB
      t.body2 = t.body2 + iB
      t.body3 = t.body3 + iB
      table.insert( hdis.ptriangles, t )
   end

end

--*------------------------------------------------------------*--
--*                    boundary properties
--*------------------------------------------------------------*--

hdis.boundary = {}
hdis.boundary.meta = {}
setmetatable( hdis.boundary, hdis.boundary.meta )

-- on new index we either do normal assignement or we also assign
-- material index
hdis.boundary.meta.__newindex = function( t, key, value )

   if ( key == 'material' ) then
      rawset( t, 'imaterial',
              hdis.material:resolve( value ) )
   end

   -- we always do a regular assignment anyway
   rawset( t, key, value )
end

hdis.boundary.imaterial = 1    -- first material by default

--*------------------------------------------------------------*--
--*                 reading from a file
--*------------------------------------------------------------*--

--- Telling the model that we are running from previously saved file
--- and reading necessary data from that file.
--- @tparam string fname file name to run from
--- @return bool success code: true or false
function hdis.runfrom( fname )

   -- check if the file exists
   if not path.exists( fname ) then
      io.write( "Error: File ".. fname .. " does not exist\n" )
      error( "No such file \"" .. fname .. "\"" )
   end

   -- check if it is the first run
   if hdis.run_from then
      io.write( "Error: Cannot run run_from twice!\n" )
      error( "hdis.runfrom run twice" )
   end

   -- check if it is run before main variables set! Well, not the best
   -- test but will prevent reckless usage!
   if hdis.time or hdis.box or hdis.g then
      io.write( "Error: hdis.runfrom function must run before any" ..
                " initializations in hdis table!\n" )
      error( "hdis.runfrom improper usage" )
   end

   local materials = hdis.hdf5.read( fname, 'Materials' )
   local mnames = hdis.hdf5.read( fname, 'Material Names' )
   local success = false
   
   for i = 0, mnames.n-1 do

      local name = mnames[i]
      local orig = materials[i]
      local tbl = {}

      tbl.name  = name
      tbl.rho   = orig.rho
      tbl.G     = orig.G * 1.0e-9
      tbl.nu    = orig.nu
      tbl.mu    = orig.mu
      tbl.CR    = orig.CR0
      tbl.vR    = orig.vR0
      tbl.alpha_t = orig.alpha_t
      tbl.gamma = orig.gamma

      hdis.material:add( tbl )
   end

   -- Reading and assigning cmprops

   local rcmprops  = hdis.hdf5.read( fname, 'Contact Mech. Properties' )

   for i = 0, #hdis.material.cmprops - 1 do

      -- new and old list of properties refs
     local lst = hdis.material.cmprops[i+1]
     local rst = rcmprops[i]

     lst.mu      = rst.mu
     lst.alpha_t = rst.alpha_t
     lst.gamma   = rst.gamma
     lst.ka      = rst.ka
     lst.H       = rst.H * 1.0e-9
     lst.vR      = rst.vR
     lst.CR      = rst.CR

   end

   -- Reading groups
   local agroups = hdis.hdf5.read( fname, 'Group Names' )
   for i = 0, agroups.n - 1 do
      hdis.groups:add( agroups[i] )
   end

   hdis.groups.intrx.n = #hdis.groups.intrx

   -- Reading Group Interaction table (we already have the intrx table
   -- after Reading groups)
   local intrx = hdis.hdf5.read( fname, 'Group Interaction' )

   if intrx then
      for i = 0, intrx.n-1 do
         hdis.groups.intrx[i+1] = intrx[i]
      end
      -- if there is no Group Interaction group: restore it
   else
      io.write( "Warning: no 'Group Interaction' in "
                .. fname
                .. " (old version?),\n"
                .. "         Restored default values"
                .. " with all groups interacting\n" )
   end

   -- Reading all box information
   local rbox = hdis.hdf5.read( fname, 'Box (domain)' )
   hdis.box = {}
   hdis.box.x = rbox[0].fcorner.x
   hdis.box.y = rbox[0].fcorner.y
   hdis.box.z = rbox[0].fcorner.z

   -- Reading all time information
   local time = hdis.hdf5.read( fname, 'Time_Parameters' )
   hdis.time = {}
   hdis.time.start = time[0].t -- start from current time
   hdis.time.stop = time[0].te
   hdis.time.dt = time[0].dt
   hdis.time.save = time[0].dts

   -- Reading gravity values
   local g = hdis.hdf5.read( fname, 'Gravity' )
   hdis.g = { g[0].x, g[0].y, g[0].z }

   -- Reading SBB parameters for contact detection
   local sbb_params = hdis.hdf5.read( fname, 'SBB parameters' )
   if type(sbb_params) == 'table' then
      hdis.bsphere = {}
      hdis.bsphere.drmax = sbb_params[0].drmax
      hdis.bsphere.drmin = sbb_params[0].drmin
      hdis.bsphere.tau = sbb_params[0].tau
   end

   -- Reading boundary values
   local bc = hdis.hdf5.read( fname, 'Boundary' )
   if type(bc) == 'table' then
      hdis.boundary.imaterial = bc[0].material_id + 1
      hdis.boundary.flags = bc[0].flags
      if ( bit32.band( hdis.boundary.flags, 0x10 ) == 0x10 ) then
         hdis.boundary.zout_vel = bc[0].zout_vel
      end
   end

   -- Reading SPH water properties
   local sphprops = hdis.hdf5.read( fname, 'SPH properties' )
   hdis.hydro_props = {}
   hdis.hydro_props.rho0 = sphprops[0].rho0 -- start from current sphprops
   hdis.hydro_props.c0 = sphprops[0].c0
   hdis.hydro_props.c2 = sphprops[0].c2 -- not necessary , c2=c0^2
   hdis.hydro_props.gamma = sphprops[0].gamma
   hdis.hydro_props.eps = sphprops[0].eps
   hdis.hydro_props.alpha = sphprops[0].alpha

   -- Reading portal data

   -- Reading general settings for special tables

   -- Assignement to hdis.run_from variable
   hdis.run_from = fname
   success = true

   return success

end


--*------------------------------------------------------------*--

return hdis
