--[[--

   Coupi HDF5 auxiliary functions library. The functions here are used to
   receive and compute some information in Coupi HDF5 files.

   <p><em>For licensing and copyright use same license as
   Coupi.</em></p>

   @module hdis.hdf5aux
   @copyright 2016 Coupi, Inc.

--]]--

local hdf5    = require 'hdis.hdf5'
local Dataset = require 'hdis.dataset'
local aux     = require 'hdis.aux'
local vec3d   = require 'hdis.vec3d'
local quat    = require 'hdis.quat'
local sphdens = require 'hdis.sphdens'

local hdf5aux = {}

-------------------------------------------------------------------------------
--- @tparam string fname HDF5 file name with Coupi output
--- @tparam table gname_lst list of groups, the bodies belong to. Or a single
---         group
--- @treturn number z-coordinate of the highest body center of mass for bodies
--- in the list of groups
-------------------------------------------------------------------------------
function hdf5aux.upper_cmax ( fname, gname_lst )
   local dset = Dataset:new()

   local bodies = hdf5.read( fname, 'Bodies' )
   local atoms  = hdf5.read( fname, 'Atoms' )
   local gnames = hdf5.read( fname, 'Atom Group' ) 

   dset:init( bodies, atoms, gnames )

   if type( gname_lst) == 'string' then
      dset:bodies_select_by_group( gname_lst )
   else
      for _, g in pairs(gname_lst) do
         dset:bodies_select_by_group( g )
      end
   end

   local objs = dset:bodies_selected()
   local N = #objs

   local zmax = 0
   for i = 1, N do
      local c = objs[i].c
      if c.z > zmax then zmax = c.z end
   end

   return zmax

end
-------------------------------------------------------------------------------
--- @tparam string fname HDF5 file name with Coupi output
--- @treturn table of tables. Function return table that contain tables with
--- characteristics of each layer. The sub table has next attributes:
--- lname - group name;
--- up - vector of the center of mass of the upper particle;
--- down - vector of the center of mass of the lowest particle;
--- right - vector of the center of mass of the rightmost particle;
--- left - vector of the center of mass of the leftmost particle;
--- near - vector of the center of mass of the nearest particle;
--- far - vector of the center of mass of the furthest particle
-------------------------------------------------------------------------------
function hdf5aux.layer_border( filename, gname )
  
  --!! Error handling need to be added!!
  local result = {} -- form a table for result
  ---[[-- Reading objects from the file
  local number_of_bodies = hdf5.get_dims( filename, 'Bodies' )
  local number_of_atoms = hdf5.get_dims( filename, 'Atoms' )
  local number_of_groups = hdf5.get_dims( filename, 'Atom Group' )

  local bodies = hdf5.read( filename, 'Bodies' )
  local atoms = hdf5.read( filename, 'Atoms' )
  local groups = hdf5.read( filename, 'Atom Group' )
  --]]--
  ---[[-- Creating a dataset
  dataset = Dataset:new()
  dataset:init( bodies, atoms, groups )
  --]]--
  ---[[--
  if gname then
    local layer = {
          lname = gname, -- group name
          up = vec3d:new(), -- center of mass of the upper particle
          down = vec3d:new(), -- center of mass of the lowest particle
          right = vec3d:new(), -- center of mass of the rightmost particle
          left = vec3d:new(), -- center of mass of the leftmost particle
          near = vec3d:new(), -- center of mass of the nearest particle
          far = vec3d:new() -- center of mass of the furthest particle
                  }
    ---[[--puts the elements of the group to the local table
    dataset:bodies_select_by_group(gname)
    local lt = dataset:bodies_selected()
    dataset:bodies_deselect_by_group(gname)
    --]]--
    if #lt > 0 then
      local lvecs = fnd_r_vec( lt )
      layer.up = lvecs[6]
      layer.down = lvecs[5]
      layer.right = lvecs[4]
      layer.left = lvecs[3]
      layer.near = lvecs[1]
      layer.far = lvecs[2]
    end
    result = layer
  else
    for g=0,number_of_groups-1 do   --take each group by group name
      local layer = {
            lname = groups[g], -- group name
            up = vec3d:new(), -- center of mass of the upper particle
            down = vec3d:new(), -- center of mass of the lowest particle
            right = vec3d:new(), -- center of mass of the rightmost particle
            left = vec3d:new(), -- center of mass of the leftmost particle
            near = vec3d:new(), -- center of mass of the nearest particle
            far = vec3d:new() -- center of mass of the furthest particle
                    }
      ---[[--puts the elements of the group to the local table
      dataset:bodies_select_by_group(groups[g])
      local lt = dataset:bodies_selected()
      dataset:bodies_deselect_by_group(groups[g])
      --]]--
      -- returns table of the radius vectors of the border particles
      if #lt > 0 then
        local lvecs = fnd_r_vec( lt )
        layer.up = lvecs[6]
        layer.down = lvecs[5]
        layer.right = lvecs[4]
        layer.left = lvecs[3]
        layer.near = lvecs[1]
        layer.far = lvecs[2]
      end 
      table.insert( result, layer )
    end
  end
  return result
end

-------------------------------------------------------------------------------
--- @tparam string fname HDF5 file name with Coupi output
--- @tparam string gname group name
--- @treturn table of tables. Function return table that contain tables with
--- characteristics of each layer. The sub table has next attributes:
--- lname - group name;
--- up - vector of the center of mass of the upper particle;
--- down - vector of the center of mass of the lowest particle;
--- right - vector of the center of mass of the rightmost particle;
--- left - vector of the center of mass of the leftmost particle;
--- near - vector of the center of mass of the nearest particle;
--- far - vector of the center of mass of the furthest particle
-------------------------------------------------------------------------------
function hdf5aux.read_group( filename, gname )
   
  local result = {} -- form a table for result
  ---[[-- Reading objects from the file
  local number_of_bodies = hdf5.get_dims( filename, 'Bodies' )
  local number_of_atoms = hdf5.get_dims( filename, 'Atoms' )
  local number_of_groups = hdf5.get_dims( filename, 'Atom Group' )

  local bodies = hdf5.read( filename, 'Bodies' )
  local atoms = hdf5.read( filename, 'Atoms' )
  local groups = hdf5.read( filename, 'Atom Group' )
  --]]--
  ---[[-- Creating a dataset
  dataset = Dataset:new()
  dataset:init( bodies, atoms, groups )
  --]]--
  ---[[-- looking for the group number
  local gnum = -1
  for g=0,number_of_groups-1 do
    if groups[g] == gname then
      gnum = g
      break
    end
  end
  ---[[-- error handling
  if ( gnum == -1 ) then
    print('Error. No such group')
    return -1
  end
  --]]--
  ---[[--puts the elements of the group to the local table
  dataset:bodies_select_by_group( groups[gnum] )
  local lt = dataset:bodies_selected()
  dataset:bodies_deselect_by_group( groups[gnum] )
  --]]--
  ---[[-- error handling
  if ( #lt == 0 ) then
    print('Error. No elements in the group ', gname)
    return -1
  end
  --]]--
  result = fnd_b_prprts( lt )
  ---[[-- puts the elements of the group to the local table. We will looks for
  -- the radii for the bodies
  dataset:atoms_select_by_group( groups[gnum] )
  lt = dataset:atoms_selected()
  ---[[-- error handling
  if ( #lt == 0 ) then
    print('Error. No atoms in the group ', gname)
    return -1
  end
  --]]--
  result = add_atm_radii( lt, result )
  --]]--
  return result
end

---[[--
function hdf5aux.layer_density( filename, gname )
  

  local mysph = sphdens:new( filename )

  local as, ps = mysph:read() --number of atoms and number of points
  local Rmin = mysph.Rmin
  local Rmax = mysph.Rmax
  ---[[-- print first part of the information ---------------------------------
  print( "# -----------------------------------------------------------------")
  print( "# Points (total): ", ps )
  print( "# Atoms (total) : ", as )
  print( "# Maximum radius (total): ", Rmax)
  print( "# Minimum radius (total): ", Rmin )
  print( "# -----------------------------------------------------------------")
  --]]-------------------------------------------------------------------------
  ---[[-- find layer properties -----------------------------------------------
  local lb = hdf5aux.layer_border( filename, gname )
  local r0 = vec3d:new( lb.near:x(), lb.left:y(), lb.down:z() )
  local r1 = vec3d:new( lb.far:x(), lb.right:y(), lb.up:z() )
  local rc = r0 + r1
  rc = rc:scale( 0.5 )
  local dr = r1 - r0
  lb = nil -- we do not need the information anymore
  print('# Coordinates of the left far lowest corner of the layer are:',r0,
        '\n# and the right nearest top corner has coordinates: ',r1 )
  print( "# -----------------------------------------------------------------")
  --]]-------------------------------------------------------------------------
  ---[[-- 
  local N = { math.floor( 1.5 * dr:x() / Rmin ),
              math.floor( 1.5 * dr:y() / Rmin ),
              math.floor( 1.5 * dr:z() / Rmin ) }
  local cyl = {}
  cyl.R = 0.5 * math.min( dr:x(), dr:y(), dr:z() ) - math.min( 0.001, Rmin )
  cyl.Reff = ( cyl.R + Rmax ) / math.sqrt(2)
  -- Rmax for stepping out of the cylinder walls for accuracy
  r0 = vec3d:new( rc:x() - cyl.Reff, rc:y() - cyl.Reff, r0:z() )
  r1 = vec3d:new( rc:x() + cyl.Reff, rc:y() + cyl.Reff, r1:z() )

  -- we step up for Rmax to avoid boundary effect
  local nspheres = mysph:select( { r0:x(), r0:y(), r0:z() + Rmax },
                                 { r1:x(), r1:y(), r1:z() - Rmax },
                                 N )
  local packing, V = mysph:density( nil )
  print( "# Packing density of the group: ", packing )
  print( "# -----------------------------------------------------------------")
end
--]]--
--helper-block-----------------------------------------------------------------

--Helper function that return a table of radius vectors of the border particles
function fnd_r_vec( bdstbl )

  --radius vector of the first particle
  local r = vec3d:new( bdstbl[1].c.x, bdstbl[1].c.y, bdstbl[1].c.z )

  local rminx = r -- coordinates of the far particle
  local rmaxx = r -- coordinates of the nearest particle
  local rminy = r -- coordinates of the left particle
  local rmaxy = r -- coordinates of the right particle
  local rminz = r -- coordinates of the lowest particle
  local rmaxz = r -- coordinates of the highest particle
  
  for i=2,#bdstbl do
    --create a radius vector to the current particle
    r = vec3d:new( bdstbl[i].c.x, bdstbl[i].c.y, bdstbl[i].c.z )

    if r:x() < rminx:x() then rminx = r end
    if r:x() > rmaxx:x() then rmaxx = r end
    if r:y() < rminy:y() then rminy = r end
    if r:y() > rmaxy:y() then rmaxy = r end
    if r:z() < rminz:z() then rminz = r end
    if r:z() > rmaxz:z() then rmaxz = r end
  end
  
  return  { rminx, rmaxx, rminy, rmaxy, rminz, rmaxz }

end

-- Helper function that return a table of bodies property
function b_prprts( bd )

  local lp = {} -- particle table
  -- radius vector to the center of the mass
  lp.c = vec3d:new( bd.c.x, bd.c.y, bd.c.z )
  -- vector of the velocity
  lp.v = vec3d:new( bd.v.x, bd.v.y, bd.v.z )
  -- vector of the angular speed
  lp.w = vec3d:new( bd.w.x, bd.w.y, bd.w.z )
  -- quaternion
  lp.q = quat:new( bd.q.s, bd.q.vx, bd.q.vy, bd.q.vz )
  -- number of atoms in the body. Appear to able shape identification
  lp.na = 0
  -- radius of the atoms
  lp.R = 0
   
  return  lp
end

-- Helper function that return a table of vectors ( center of the mass,
-- velocity, angular speed, quaternion ) of the layers bodies
function fnd_b_prprts( bdstbl )

  local lt = {} -- result table
  table.insert( lt, b_prprts( bdstbl[#bdstbl] ) ) -- I do not know why body
                                                  -- with body index 0 ( first 
                                                  -- body in the list ) came 
                                                  -- into bdstbl as last 
                                                  -- element but we in the lt 
                                                  -- table now it will be 
                                                  -- first element
  for i=1,#bdstbl-1 do
    table.insert( lt, b_prprts(bdstbl[i]) )
  end
  return  lt
end

-- Helper function that fill lp.na and lp.R in the table from fnd_b_prprts
function add_atm_radii( atmstbl, result )
   local mbi = math.huge
   for i=1,#atmstbl do
      if atmstbl[i].body_indx < mbi then
         mbi = atmstbl[i].body_indx
      end
   end
   print(mbi)
  for i=1,#atmstbl do
    local bi = atmstbl[i].body_indx - mbi + 1 -- body index
    result[bi].na = result[bi].na + 1 
    ---[[--
    if not ( result[bi].R == 0 ) then
      if not ( atmstbl[i].R == result[bi].R ) then -- Warning if radii of the
                                                   -- atoms are different for
                                                   -- the same body
        print('Warning the body ', bi, ' has different radii of atoms')
      end
    else
      result[bi].R = atmstbl[i].R
    end
    --]]--
  end
  return result
end

--end-of-the-helper-block------------------------------------------------------

-------------------------------------------------------------------------------
hdis.hdf5aux = hdf5aux
return hdf5aux