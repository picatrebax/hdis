--[[--

   COUPi module for control functions.

   <p><em>For licensing and copyright use same license as
   COUPi.</em></p>

   @module hdis.control
   @author Anton Kulchitsky

--]]

require "hdis"

--- Main hdis.control table
--- @table control
--- @tparam table builtin different builtin control constant types for
--- bodies: "free" and "steady"
--- @tparam table std standard control functions available
local control = {
   builtin = nil,
   std = nil,
}

control.builtin = {}
control.builtin.free   =  0     -- defautl the body is free
control.builtin.steady = -1     -- the body v=v0, w=w0
control.builtin.still  = -2     -- the body v=0, w=0
control.builtin.sensitive  = -3  -- the body is free until the first
                                 -- contact, then v=0, w=0

control.std = {}

--- Creates and returns control function for the body that can move
--- only up and down (vertical constrain) under gravity forces and
--- vertical resistance and has a constant angular velocity (wx,wy,wz)
--- @tparam number Wx x component of angular velocity
--- @tparam number Wy y component of angular velocity
--- @tparam number Wz z component of angular velocity
--- @treturn function control function for a body
function control.std.constrain_vert_const_omega( Wx, Wy, Wz )
   local Wx = Wx or 0.0
   local Wy = Wy or 0.0
   local Wz = Wz or 0.0
   return function ( i, dt, m,
                     vx, vy, vz, wx, wy, wz, 
                     fx, fy, fz, tx, ty, tz )
             return 0.0, 0.0, vz + dt * fz / m, Wx, Wy, Wz
          end
end

--- Creates and returns control function for the body that can move
--- only up and down (vertical constrain) under gravity forces and
--- vertical resistance and has a constant angular velocity
--- (wx,wy,wz) and limited maximum vertical velocity vmax.
--- @tparam number Wx x component of angular velocity
--- @tparam number Wy y component of angular velocity
--- @tparam number Wz z component of angular velocity
--- @tparam number vmax maximum vertical velocity (absolute value)
--- @treturn function control function for a body
function control.std.constrain_vert_const_omega_vmax( Wx, Wy, Wz, vmax )
   local Wx = Wx or 0.0
   local Wy = Wy or 0.0
   local Wz = Wz or 0.0
   return function ( i, dt, m,
                     vx, vy, vz, wx, wy, wz, 
                     fx, fy, fz, tx, ty, tz )
             local v = vz + dt * fz / m
             if v > vmax then v = vmax end
             if v < -vmax then v = -vmax end
             return 0.0, 0.0, v, Wx, Wy, Wz
          end
end

--*------------------------------------------------------------*--

hdis.control = control
return control
