--[[--

   COUPi auxiliary functions library. 

   <p><em>For licensing and copyright use same license as
   COUPi.</em></p>

   @module hdis.aux
   @author Anton Kulchitsky

--]]

--require 'hdis'
local aux = {}

--*------------------------------------------------------------*--
--*                   diagnostics/print functions
--*------------------------------------------------------------*--

--- Recursively prints any value including tables that do
--- not have cylcles and cross references in it. It fits all the shape
--- tables for example. Modified from the original in "Programming in
--- Lua" book.
--- @param value an object to print
--- @tparam string space a separator (default '')
function aux.print_serial( value, space )

   -- nice spacing
   if not space then space = ""
   else space = space .. "  "
   end

   -- printing all the different types
   if type( value ) == "number" then
      io.write( value )

   elseif type( value ) == "string" then
      io.write( string.format( "%q", value ) )

   elseif type( value ) == "boolean" then
      if value then
         io.write( "true" )
      else
         io.write( "false" )
      end

   elseif type( value ) == "table" then
      io.write( "{\n" )
      for k, v in pairs( value ) do
         io.write( space, "  ", k, " = " )
         aux.print_serial( v, space )
         io.write( ",\n" )
      end
      io.write( space, "}" )

   elseif type( value ) == "function" then
      io.write( "--FUNCTION--" )

   elseif value == nil then
      io.write( "nil" )

   else
      error( "Cannot ouput the object of type " .. type( value ) )
   end

   -- only for the last call we \n
   if not space or space == "" then io.write( "\n" ) end
end

--*------------------------------------------------------------*--
--*                    Deep copying
--*------------------------------------------------------------*--

--- Deep copying the table with rebuiling all the fields recursively.
--- Taken from http://lua-users.org/wiki/CopyTable and corrected with
--- an optional argument for metatables copy/link: if copymeta is not
--- nil or false, then metatables are not shared.
--- @param object any Lua object to be copied.
--- @tparam boolean copymeta true if metatable need to be copied as
--- well. Otherwise, metatables are shared. 
--- @return Deep copy of a given table. The function below also copies
--- the metatable to the new table if there is one, so the behaviour
--- of the copied table is the same as the original. But the 2 tables
--- share the same metatable, you can avoid this by changing this
--- 'getmetatable(object)' to '_copy( getmetatable(object) )'.
function aux.deepcopy( object, copymeta )

   local lookup_table = {}

   local function _copy(object)
      if type(object) ~= "table" then
         return object
      elseif lookup_table[object] then
         return lookup_table[object]
      end
      local new_table = {}
      lookup_table[object] = new_table
      for index, value in pairs(object) do
         new_table[_copy(index)] = _copy(value)
      end

      -- optional metatable copy
      if copymeta then
         return setmetatable(new_table, _copy(getmetatable(object)))
      else                      -- share same metatable
         return setmetatable(new_table, getmetatable(object))
      end

   end
   return _copy(object)
end

--*------------------------------------------------------------*--

hdis.aux = aux
return aux
