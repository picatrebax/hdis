--[[---------------------------------------------------------------------------

   Coupi module for groups and types of their interaction.

   (c)2017 Coupi, Inc.
   @module hdis.groups
   @author Anton Kulchitsky

--]]---------------------------------------------------------------------------

local loclib = require 'hdis.loclib'

local groups = {}

groups.type = {
   sphere = 1,
   capsule = 2,
   tripsule = 3,
   edge = 8,
   tface = 16,
   vertex = 1
}
groups.type.vertex = groups.type.sphere

groups.list = {}
groups.hash = {}
groups.intrx = {}  -- interaction table

--- Adds a name to the list of groups.
--- @tparam string name name of the group
--- @tparam treturn table self for chaining additions
function groups:add( ... )

   for i, name in ipairs{ ... } do
      if self.hash[name] then
         io.write( "Warning: Attempt to add existing group \"".. name .. "\""..
                   ". Skipping...\n" )
      else
         if type( name ) == 'string' then
            table.insert( self.list, name )
            local n = #self.list
            self.hash[ name ] = n

            -- adding group to the interaction table: interaction on
            for i = 1, n do
               table.insert( self.intrx, 1 )
            end

         else
            error( "group name must be a string" )
         end
      end
   end

   return self
end

--- Resolves the group name into a group number. It is a bit more than just
--- checking hash because all error message generating need to be here.
--- @tparam string groupname name of the group
--- @treturn number group number
function groups:resolve( groupname )

   local group = self.hash[ groupname ]

   if group == nil then
      error( 'No such group "' .. groupname .. '"' )
   end

   return group
end

--- Adds a group pair that do not interact (by default any pair will interact)
--- @tparam string gname1 first group in the pair
--- @tparam string gname2 second group in the pair
--- @treturn table self for chaining aversions
function groups:avert( gname1, gname2 )
   local i = self:resolve( gname1 )
   local j = self:resolve( gname2 )

   -- calculation of the index by i,j: not trivial
   local I = loclib.indexmap( i, j )
   self.intrx[I] = 0

   return self
end

--- Adds a group pair that interact (by default any pair will
--- interact, so this function is needed only to switch interaction
--- on)
--- @tparam string gname1 first group in the pair
--- @tparam string gname2 second group in the pair
--- @treturn table self for chaining avertions
function groups:interact( gname1, gname2 )
   local i = self:resolve( gname1 )
   local j = self:resolve( gname2 )

   -- calculation of the index by i,j: not trivial
   local I = loclib.indexmap( i, j )
   self.intrx[I] = 1

   return self
end

return groups