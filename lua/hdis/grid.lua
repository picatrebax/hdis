--[[--

   COUPi grid generation library. It helps to generate the grids in
   space to place the objects in it.

   <p><em>For licensing and copyright use same license as
   COUPi.</em></p>

   @module hdis.grid
   @author Anton Kulchitsky

--]]

require 'hdis'
local grid = {}

--*------------------------------------------------------------*--
--*-                   Centralized grid
--*------------------------------------------------------------*--

--- Returns a table of grid points equally distant with required
--- parameters.
--- @tparam table r0 {x0,y0,z0} lower left close corner of the grid 
--- @tparam table r1 {x1,y1,z1} upper right further corner of the grid 
--- @tparam table dr {dx,dy,dz} grid step in all directions 
--- @treturn table the grid, with exact step dr, nodes strictly inside
--- r0,r1 box
function grid.grid( r0, r1, dr )

   local grid = {}        -- the table generated

   local xstart  = r0[1]
   local xend    = r1[1]
   local xcenter = 0.5 * ( xstart + xend )
   local dx = dr[1]

   local ystart  = r0[2]
   local yend    = r1[2]
   local ycenter = 0.5 * ( ystart + yend )
   local dy = dr[2]

   local zstart  = r0[3]
   local zend    = r1[3]
   local zcenter = 0.5 * ( zstart + zend )
   local dz = dr[3]

   -- All starts from the center and then we move from it to make a
   -- cymmetrical distribution
   for z = zcenter, zend, dz do
      for y = ycenter, yend, dy do 

         for x = xcenter, xend, dx do
            table.insert( grid, { x, y, z } )
         end
         
         for x = xcenter - dx, xstart, -dx do
            table.insert( grid, { x, y, z } )
         end
         
      end
      
      -- reverse for y
      for y = ycenter - dy, ystart, -dy do 
         
         for x = xcenter, xend, dx do
            table.insert( grid, { x, y, z } )
         end
         
         for x = xcenter - dx, xstart, -dx do
            table.insert( grid, { x, y, z } )
         end
         
      end
      
   end
   
   -- reverse for z
   for z = zcenter-dz, zstart, -dz do
      
      for y = ycenter, yend, dy do 
         
         for x = xcenter, xend, dx do
            table.insert( grid, { x, y, z } )
         end
         
         for x = xcenter - dx, xstart, -dx do
            table.insert( grid, { x, y, z } )
         end
         
      end
      
      -- reverse for y
      for y = ycenter - dy, ystart, -dy do 
         
         for x = xcenter, xend, dx do
            table.insert( grid, { x, y, z } )
         end
         
         for x = xcenter - dx, xstart, -dx do
            table.insert( grid, { x, y, z } )
         end
         
      end
   end
   
   return grid
end

hdis.grid = grid
return grid
