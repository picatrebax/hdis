--[[--

   COUPi module for packing density calculation for spherical atoms.

   <p>It uses OOP style, so 'new' method returns an object and all other 
   functions need to be called via this object. See example 'dens.lua' for 
   more instructions.</p>

   <p>Calculation of packing density works as follows: you select the region 
   as a box by coordinates of two corners and number of cells it need to be 
   split in all directions. Every cell's center is checked and if it is inside 
   any of the sphere, it is marked. The ration between marked and all cell's 
   centers is an approximation to the packing density. Multiplying it by the 
   volume of the box, we get the volume taken by spheres.</p>

   <p>An additional filter function can be submitted to filter out additional 
   geometry. In this case the volume checked is the volume of intersection of 
   that special geometry and the box. It is useful for considering complicated 
   geometries or finding density within the whole cylinder and such. If 
   function = nil or false, the filter is not applied.</p>

   <p>The whole procedure is represented as an object and is split in
   functions for efficiency.</p>

   <p><em>For licensing and copyright use same license as COUPi.</em></p>

   @module hdis.sphdens
   @author Anton Kulchitsky

--]]--

local sphdens = {}

-- Using library for reading HDF5s
require 'hdis'
local hdf5 = require 'hdis.hdf5'
local aux = require 'hdis.aux'

local mt = {} -- metatable
mt.__index = sphdens

-------------------------------------------------------------------------------
--- Creates new density object for a file 'fname'.
--- @tparam string fname File name to be read.
--- @treturn new density object that can be used to calculate density.
-------------------------------------------------------------------------------
function sphdens:new( fname )

   -- initialization
   local maintable = {}

   maintable.filename = fname
   maintable.atoms    = nil
   maintable.points   = nil
   maintable.M        = nil

   return setmetatable( maintable, mt )
end

-------------------------------------------------------------------------------
--- Returns the file name associated with the object
--- @treturn string file name
-------------------------------------------------------------------------------
function sphdens:fname()
   return self.filename
end

-------------------------------------------------------------------------------
--- Reads atoms and points from HDF5 to compute packing density in the future. 
--- Run may take some time depending on how big is HDF5.
--- @treturn number number of atoms in the file
--- @treturn number of points in the file
-------------------------------------------------------------------------------
function sphdens:read()
   -- Reading of atoms and points
   local h5atoms  = hdf5.read( self.filename, 'Atoms' )
   local h5points = hdf5.read( self.filename, 'Verts' )

   self.atoms = {}
   for i = 1, h5atoms.n do
      table.insert( self.atoms, h5atoms[i-1] )
   end

   self.points = {}
   for i = 1, h5points.n do
      table.insert( self.points, h5points[i-1] )
   end

   -- calculating some parameters
   self.Rmin = self.atoms[1].R
   self.Rmax = self.atoms[1].R

   for _, A in ipairs( self.atoms ) do
      if A.R < self.Rmin then self.Rmin = A.R end
      if A.R > self.Rmax then self.Rmax = A.R end
   end

   return #self.atoms, #self.points
end

-------------------------------------------------------------------------------
--- Selects a region where density is computed. It creates a special table in 
--- space with points at some grid and initializes it with 1 if the point is 
--- within some atom or 0 for empty space. Run may take some time depending on 
--- how big is HDF5.
--- @tparam table r0 array of x0, y0, z0, coordinates of the lower left corner 
--- of the region.
--- @tparam table r1 array of x1, y1, z1, coordinates of the high right corner 
--- of the region.
--- @tparam table N array of Nx, Ny, Nz number of points along each dimension
--- @treturn number number of spheres affected density in the box. More 
--- precise: number of spheres inside the box or crossing the box and used for 
--- calculations
-------------------------------------------------------------------------------
function sphdens:select( r0, r1, N )
   self.M = {}
   local M = self.M
   local atoms = self.atoms
   local points = self.points

   -- parameters of the box is copied
   self.r0 = aux.deepcopy( r0 )
   self.r1 = aux.deepcopy( r1 )
   self.N  = aux.deepcopy( N )

   -- Initializing all cells of big array with 0: no spheres
   local Nx = N[1]
   local Ny = N[2]
   local Nz = N[3]

   for s = 1, Nx*Ny*Nz do
      M[s] = 0
   end

   -- building mesh
   local dx = ( r1[1] - r0[1] ) / Nx
   local dy = ( r1[2] - r0[2] ) / Ny
   local dz = ( r1[3] - r0[3] ) / Nz

   assert( dx > 0 and dy > 0 and dz > 0 )

   self.dx = dx
   self.dy = dy
   self.dz = dz

   --*----------------*--
   local nspheres = 0
   for _, A in ipairs( atoms ) do

      if ( A.nverts == 1 ) then -- check if it is a sphere

         local ip = A.ivert0+1 -- index of the point in points array
         local center = points[ip].global_pos --center coordinate (global, box)
         local R = A.R               -- Radius
         local cx = center.x         -- abbrev. for center coordinates
         local cy = center.y
         local cz = center.z

         -- check if it is in the box r0-r1 TODO. We count only spheres that
         -- have centers inside the box. Those spheres that intersect the box 
         -- but not reside in the box do not count. They are compensated by 
         -- the spheres that being reside in the box, actually intersect it 
         -- outside.
         if ( ( r0[1]-R < cx and cx < r1[1]+R ) and
           ( r0[2]-R < cy and cy < r1[2]+R ) and
        ( r0[3]-R < cz and cz < r1[3]+R ) ) then

            -- sphere is counted
            nspheres = nspheres + 1

            -- what cell is it in?
            local ci = math.floor( ( cx - r0[1] ) / dx )
            local cj = math.floor( ( cy - r0[2] ) / dy )
            local ck = math.floor( ( cz - r0[3] ) / dz )

            -- how many cells we check
            local ri = math.ceil( R / dx ) +1
            local rj = math.ceil( R / dy ) +1
            local rk = math.ceil( R / dz ) +1

            local fi = math.max( ci - ri, 0 )
            local fj = math.max( cj - rj, 0 )
            local fk = math.max( ck - rk, 0 )

            local Fi = math.min( ci + ri, Nx )
            local Fj = math.min( cj + rj, Ny )
            local Fk = math.min( ck + rk, Nz )

            for i = fi,Fi do
               for j = fj,Fj do
                  for k = fk,Fk do
                     local Cx = r0[1] + i*dx - 0.5*dx
                     local Cy = r0[2] + j*dy - 0.5*dy
                     local Cz = r0[3] + k*dz - 0.5*dz

                     if (Cx-cx)^2 + (Cy-cy)^2 + (Cz-cz)^2 - R^2 < 0 then
                        M[ i + Nx*(j-1) + Nx * Ny * (k-1) ] = 1
                     end

                  end
               end
            end
         end
      end
   end

   self.nspheres = nspheres
   return nspheres
end

-------------------------------------------------------------------------------
--- Returns a table of grid points equally distant with required parameters.
--- @tparam function detfunc A filter function of x,y,z for a space point. If 
--- function is true for the point, the point is calculated, otherwise, it is 
--- considered to be outside of the volume. If function is not provided or nil 
--- or false, the parameter does not effect the calculations and rectangular 
--- region is taken as it is.
--- @treturn number packing density as a ratio of occupied volume to total 
--- volume defined by the box and (if provided) the filter function.
--- @treturn Volume total defined by the box and (if provided) the filter 
--- function. It is exactly the box volume if the function is not provided.
-------------------------------------------------------------------------------
function sphdens:density( detfunc )
   --- Finding the ratio
   local Sum = 0
   local Total = 0
   local Volume = 0

   local Nx = self.N[1]
   local Ny = self.N[2]
   local Nz = self.N[3]

   local dx = self.dx
   local dy = self.dy
   local dz = self.dz

   local M = self.M

   for i = 1, Nx do
      for j = 1, Ny do
         for k = 1, Nz do

            local Cx = self.r0[1] + i*dx - 0.5*dx
            local Cy = self.r0[2] + j*dy - 0.5*dy
            local Cz = self.r0[3] + k*dz - 0.5*dz

            if ( not detfunc ) or detfunc( Cx, Cy, Cz ) then
               if ( M[ i + Nx*(j-1) + Nx * Ny * (k-1) ] == 1 ) then
                  Sum = Sum + 1
               end
               Total = Total + 1
               Volume = Volume + dx * dy * dz
            end
         end
      end
   end

   local packing = Sum / Total

   return packing, Volume
end

-------------------------------------------------------------------------------
return sphdens
-------------------------------------------------------------------------------