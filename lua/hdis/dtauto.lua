--[[--

   COUPi automatic time step function generator class

   <p><em>For licensing and copyright use same license as
   COUPi.</em></p>

   @module hdis.dtauto
   @author Anton Kulchitsky

--]]

local dtauto = {}

--*------------------------------------------------------------*--
--*                   dt auto functions
--*------------------------------------------------------------*--

----------------------------------------------------------------
--- dtauto object constructor
---
--- Call: dtauto:new( hdis.material, rmin, vcmin )
---
--- @param material table standard table hdis.material with all the
--- materials. Important: materials must be added (unless you would
--- like to skip some materials in auto dt calculations)
--- @param rmin number minimum interaction radius
--- @param vcmin minimum allowed contact velocity in dt calculation,
--- default value is 1 cm/s. This defines the maximum possible
--- dt. This is euristic parameter which determines the time step when
--- all the particles do not move.
--- @treturn table a new initialized dtauto object
----------------------------------------------------------------
function dtauto:new( material, rmin, vcmin )

   -- creating new dtauto object
   local obj = {}
   setmetatable( obj, self )
   self.__index = self
   self.type = 'dtauto'

   -- Calculation of B constant
   local B = math.pow( 5.0 * math.pi / 3.0, 0.4 )

   -- finding minimum super Th
   local Th_min = nil

   for i, m in pairs( material.cmprops ) do
      local rho1 = hdis.material.list[ m.indx[1] ].rho
      local rho2 = hdis.material.list[ m.indx[2] ].rho
      local rho = math.min( rho1, rho2 )

      Th = B * math.pow( rho/ (1.e9 * m.H ), 0.4 ) * rmin
      if ( Th_min == nil or Th < Th_min ) then
         Th_min = Th
      end
   end
   
   obj.Th     = Th_min
   obj.vc_sq_min = vcmin * vcmin

   return obj
   
end

------------------------------------------------------------------
--- %treturn new time step function to initialize hdis.callback.dt
------------------------------------------------------------------
function dtauto:dt()

   local function dt( t, dt, n, vc_sq_max )
      local vc_sq = math.max( self.vc_sq_min, vc_sq_max )
      local dt_new  = 0.05 * self.Th / math.pow( vc_sq, 0.1 )
      return dt_new
   end

   return dt

end

------------------------------------------------------------------
return dtauto
