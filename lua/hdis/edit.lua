--[[-- 


   edit is the Lua library for editing HDF5 files.  It is uses hdf5 to
   read/write the data.

   @module hdis.edit
   @author Ben Nye
--]]

local edit = {}

require "hdis"
require "hdf5"

--- Generate a list of the indices selected atoms.
--
-- This function returns a list of indices that is intended for use
-- with edit.update_mprops and edit.update_atoms. In general, you can
-- select atoms that have a specific value for one of their fields. If
-- you wanted to select only atoms belonging to body 13, you would do
-- it like:
--    <pre>
--    indices = edit.select_atoms("/path/to/file", "body_indx", 13)</pre>
-- <br><br>
-- There are also a few custom strings that replicate hdis-edit
-- functionality:
--    <pre>
--    -- Select only index NUMBER
--    indices = edit.select_atoms("path/to/file", "index", NUMBER)
--
--    -- Select all atoms
--    indices = edit.select_atoms("path/to/file", "all")
--
--    -- Select atoms belonging to free bodies
--    indices = edit.select_atoms("path/to/file", "free")
--
--    -- Select atoms belonging to controlled bodies
--    indices = edit.select_atoms("path/to/file", "controlled")
--
--    -- Look up the group named STRING in the "Atom Group" table and use
--    --   its index
--    indices = edit.select_atoms("path/to/file", "group", STRING)</pre>
--
-- @tparam string file Name of the file containing the Atoms table
-- (absolute path)
-- @tparam string field The field you want to use to make your
-- selection
-- @tparam field value (situational) The value that the field is
-- required to be equal to for that atom to be selected. Not always
-- applicable.
-- @treturn table Table where all selected indices have a value of
-- true

function edit.select_atoms(file, field, value)

  local selection = {}
  local atoms = hdf5.read(file, "Atoms")

  if field == "index" then
    selection[value] = true
    return selection
  end

  if field == "all" then
    for i=0, atoms["n"]-1 do
      selection[i] = true
    end
    return selection
  end

  if field == "free" or field == "controlled" then
    local bodies = hdf5.read(file, "Bodies")
    local free_bodies = {}
    for i=0, bodies["n"]-1 do
      if bodies[i]["ifunc"] == 0 then free_bodies[i] = true end
    end

    if field == "free" then
      for i=0, atoms["n"]-1 do
        if free_bodies[atoms[i]["body_indx"]] then selection[i] = true end
      end
    end

    if field == "controlled" then
      for i=0, atoms["n"]-1 do
        if free_bodies[atoms[i]["body_indx"]] == false then selection[i] = true end
      end
    end

    return selection
  end

  if field == "group" then 
    if type(value) == "string" then                           -- We need to lookup the group_id that goes
      local group_names = hdf5.read(file, "Atom Group") --   with the given name
      for k,v in pairs(group_names) do
        if v == value then -- Look for the entry with the supplied name
          value = k        -- Then update the value we check atoms against to be the index of that name
          break
        end
      end
      if type(value) == "string" then -- We didn't find the give name in the Atom Group table
        print("Could not find " .. value .. " in Atom Group table")
        return {}
      end
    end

    for i=0, atoms["n"]-1 do
      if atoms[i]["group_id"] == value then selection[i] = true end
    end
    return selection
  end

  -- Everything that isn't a special case just selects atoms where atom[field] == value
  for i=0, atoms["n"]-1 do
    if atoms[i][field] == value then selection[i] = true end
  end
  return selection

end

--- Update the Mechanical properties table.
-- For each index in the selection table, set the given field to the given value.
-- If no selection is specified, all entries are updated.
-- 
-- @tparam string file Name of the file to update the mprops in (absolute path) 
-- @tparam string field Name of the field to update
-- @tparam number value The new value to set the field to
-- @tparam table selection (optional) Indices of the mprops to update
--
function edit.update_mprops(file, field, value, selection)

  if selection == nil then selection = edit.select_atoms(file, "all") end

  local mprops = hdf5.read(file, "Mechanical properties")
  for i=0, mprops["n"]-1 do
    if selection[i] then mprops[i][field] = value end
  end

  hdf5.write(file, mprops)
end

--- Update the Atoms table.
-- For each index in the selection table, setthe given field to the given value.
-- If no selection is specified, all entries are updated.
-- <br><br>
-- There is special handling for group names. If you want to use a group's name
-- (the string), pass "group" as the field and the name string as the value.
-- 
-- @tparam string file Name of the file to update the atoms in (absolute path) 
-- @tparam string field Name of the field to update
-- @tparam number value The new value to set the field to
-- @tparam table selection (optional) Indices of the atoms to update
--
function edit.update_atoms(file, field, value, selection)

  if selection == nil then selection = edit.select_atoms(file, "all") end

  if field == "group" then
    field = "group_id"
    if type(value) == "string" then
      local group_names = hdf5.read(file, "Atom Group")
      for k,v in pairs(group_names) do
        if v == value then
          value = k
          break
        end
      end
      if type(value) == "string" then -- We didn't find the give name in the Atom Group table
        print("Could not find " .. value .. " in Atom Group table")
        return
      end
    end
  end

  local atoms = hdf5.read(file, "Atoms")
  for i=0, atoms["n"]-1 do
    if selection[i] then atoms[i][field] = value end
  end

  hdf5.write(file, atoms)

end

--- Resize all atoms and bodies.
-- Adjust atom R and ri, and body m, V and I.
--
-- @tparam string file Name of the file to update the atoms in (absolute path) 
-- @tparam number scale New size as a proportion of the current size (i.e. 1.1 to increase size by 10%)
--
function edit.rescale_particles(file, scale)

  local atoms = hdf5.read(file, "Atoms")

  for i=0, atoms["n"]-1 do
		atoms[i]["R"] = atoms[i]["R"]*scale
		atoms[i]["ri"] = atoms[i]["Ri"]*scale
  end

  hdf5.write(file, atoms)

	local bodies = hdf5.read(file, "Bodies")

	for i=0, bodies["n"]-1 do
		bodies[i]["m"] = bodies[i]["m"]*scale^3
		bodies[i]["V"] = bodies[i]["V"]*scale^3
		-- I is proportional to m*r^2 which is proportional to r^3*r^2
		bodies[i]["I"]["x"] = bodies[i]["I"]["x"]*scale^5
		bodies[i]["I"]["y"] = bodies[i]["I"]["y"]*scale^5
		bodies[i]["I"]["z"] = bodies[i]["I"]["z"]*scale^5
	end
end

hdis.edit = edit
return edit
