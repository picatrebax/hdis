--[[--
   
   COUPi class dataset for updating datasets read from HDF5 by hdf5

   <p>It contains a class for selecting bodies and other elements and updating
   them</p>

   <p><em>For licensing and copyright use same license as COUPi.</em></p>
      
   @module hdis.dataset
   @author Anton Kulchitsky

--]]--

local aux = require 'hdis.aux'

-- the table representing the class, which will double as the metatable for
-- the instances
local dataset = {}
local mt = {}                   -- metatable
mt.__index = dataset

--- syntax equivalent to "MyClass.new = function...".
--- @treturn table new dataset class. All other methods need to be run on it.
function dataset:new()
   local maintable = {}

   maintable.bodies = {}
   maintable.atoms  = {}
   maintable.gnames = {}
   maintable.bodyflags  = {}
   maintable.atomflags  = {}
   maintable.SUCCESS = 0
   maintable.initialized = false

  return setmetatable( maintable, mt )
end

--- Initializes the class by setting indx
--- @tparam table bodies bodies table returned by hdf5_read
--- @tparam table atoms atoms table returned by hdf5_read
--- @tparam table gnames gnames (group nams) returned by hdf5_read
function dataset:init( bodies, atoms, gnames )

   self.bodyflags = {}

   for i = 0, bodies['n']-1 do
      self.bodyflags[i] = false  -- all bodies are unselected initially
   end

   for i = 0, atoms['n']-1 do
      self.atomflags[i] = false -- all atoms are unselected
   end

   self.bodies = bodies
   self.atoms  = atoms
   self.gnames = gnames

end

--- Returns an index of the group in the group array
--- @tparam table dataset dataset class
--- @tparam string gname group name
local function group_resolve_index( dataset, gname )

   local ig = -1

   for i, txt in pairs( dataset.gnames ) do
      if ( gname == txt ) then
         ig = i
         break
      end
   end
   
   return ig
end

--- For the bodies with group name of the atoms they are built from, it set a
--- flag in bodyflags array.
--- @tparam table ds datastructure class
--- @tparam string gname group name
--- @param flag new value for bodyflags (true/false)
--- @return SUCCESS value
local function bodies_set_bodyflag_by_group( ds, gname, flag )

   ig = group_resolve_index( ds, gname )
   
   if ig == -1 then
      print( "Error: no such group as", gname )
      return 2
   end

   -- getting all indx
   for i=0, ds.atoms.n do
      local a = ds.atoms[i]
      if type(a) == "table" and a.group_id == ig then
         ds.bodyflags [ a.body_indx ] = flag
      end
   end

   return ds.SUCCESS
end

--- Select bodies by group name of the atoms they are built from for future
--- modifications
--- @tparam string gname group name
function dataset:bodies_select_by_group( gname )
   return bodies_set_bodyflag_by_group( self, gname, true )
end

--- De-select bodies by group name of the atoms they are built from. This will
--- clear the flags for those bodies and they will be not
--- modified by other calls
--- @tparam string gname group name
function dataset:bodies_deselect_by_group( gname )
   return bodies_set_bodyflag_by_group( self, gname, false )
end

--- De-select all bodies
function dataset:bodies_deselect_all()
   for i = 0,#self.bodyflags do
      self.bodyflags [ i ] = false
   end
end

--- Set the field for all selected bodies
--- @tparam string fname name of the field to update
--- @param fvalue the new value for the field
--- @treturn table bodies table updated (can be ignored as all bodies will be
--- silently updated)
function dataset:bodies_set_field( fname, fvalue )

   for i, b in pairs( self.bodies ) do
      if self.bodyflags[i] then
         b[ fname ] = fvalue
      end
   end

   return self.bodies
end

--- Create the table of selected bodies
--- $treturn table new table that contains all selected bodies in it
function dataset:bodies_selected()

   local new_bodies = {}

   for i, b in pairs( self.bodies ) do
      if self.bodyflags[i] then
         table.insert ( new_bodies, b )
      end
   end

   return new_bodies
end

-------------------------------------------------------------------------------
--- For the atoms with group name gmane it set a flag in bodyflags array.
--- @tparam table ds datastructure class
--- @tparam string gname group name
--- @param flag new value for bodyflags (true/false)
--- @return SUCCESS value
local function atoms_set_bodyflag_by_group( ds, gname, flag )

   ig = group_resolve_index( ds, gname )
   
   if ig == -1 then
      print( "Error: no such group as", gname )
      return 2
   end

   -- getting all indx
   for i=0, ds.atoms.n do
      local a = ds.atoms[i]
      if type(a) == "table" and a.group_id == ig then
         ds.atomflags [ i ] = flag
      end
   end

   return ds.SUCCESS
end

--- Select atoms by group name of the atoms they are built from for future
--- modifications
--- @tparam string gname group name
function dataset:atoms_select_by_group( gname )
   return atoms_set_bodyflag_by_group( self, gname, true )
end

--- Create the table of selected atoms
--- $treturn table new table that contains all selected atoms in it
function dataset:atoms_selected()

   local new_atoms = {}

   for i, a in pairs( self.atoms ) do
      if self.atomflags[i] then
         table.insert ( new_atoms, a )
      end
   end

   return new_atoms
end

-------------------------------------------------------------------------------
--- Setting new pressure for pressure triangles
--- @tparam table ptriags table read by hdf5_read
--- @tparam number value new pressure value
--- @tparam number gnum  group number (or empty if default)
--- @return updated ptriags table (same table as on input)
function dataset.ptriags_set_pressure( ptriags, value, gnum )

   local gnum = gnum or 0

   for i, p in pairs( ptriags ) do
      if type(p) == 'table' and p.group == gnum then
         p.p = value
      end
   end

   return ptriags
end
-------------------------------------------------------------------------------
return dataset