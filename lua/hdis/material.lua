--[[--

   Object Material and material description in COUPi.

   <p>Required by init.lua and should not be required explicitly in
   configuration files.</p>

   <p><em>For licensing and copyright use same license as
   COUPi.</em></p>

   @module hdis.material
   @author Anton Kulchitsky

--]]

local material = {
   list = {},                   -- list of materials
   hash = {},                   -- index( name ) fast resolution
   cmprops = {},                -- contact mechanical properties
   nmats = 0                    -- number of elements in the list
}

-- for map function
local tablex = require "pl.tablex"

-- constant coefficient to restore the adhesion coef.
local adhesion_K = math.sqrt(math.pi) 
   * ( math.sqrt(6.) - math.sqrt( 8./3. ) )

-- Computes ka for mech. properties as afunction of gamma and H
function material.ka( gamma, H )
   local ka =            -- notice 1.0e9 to convert H to Pa
      adhesion_K * math.sqrt( gamma * H * 1.0e9 )
   return ka
end

--- Creates default set of contact mechanical properties by full
--- tables of material properties.
--- @tparam table m1 material 1
--- @tparam number i index of material 1 in material table
--- @tparam table m2 material 2
--- @tparam number j index of material 2 in material table
--- @treturn table default properties for the contact
local function contact_props_default( m1, i, m2, j )
   local props = {}

   props[1] = m1.name
   props[2] = m2.name

   props.indx = { i, j }
   props.H = (8.0/3.0) / ( 
      (1 - m1.nu) / m1.G + ( 1 - m2.nu ) / m2.G )
   if m1.gamma <= 0 or m2.gamma <= 0 then
      props.gamma = 0
   else
      props.gamma = 0.5 * ( m1.gamma + m2.gamma )
   end
   props.CR = 
      math.sqrt( 0.5 * ( m1.CR * m1.CR + m2.CR * m2.CR ) )
   props.vR = 0.5 * ( m1.vR + m2.vR )
   props.ka = material.ka( props.gamma, props.H )
   props.mu = 2 / ( 1/m1.mu + 1/m2.mu )
   props.alpha_t = 0.5 * ( m1.alpha_t + m2.alpha_t )

   return props
end


--- Creates a specific material and adds it to the material
--- list. <em>This is the only function to be used in your COUPi
--- Lua configuration scripts from this module</em>
--- @tparam table tbl material table.
--- @return self object to be able to chain adds
function material:add( tbl )

   if type( tbl.name ) ~= "string" then error( "No name" ) end

   -- do we have this material already?
   if self.hash[ tbl.name ] then
      io.write( "Warning: Attempt to add existing material: ".. 
                tbl.name .. ". No action is performed.\n" )
      return
   end

   -- do we have all necessary parameters specified?
   if type( tbl.rho )  ~= "number" then error( "No rho" ) end
   if type( tbl.G )    ~= "number" then error( "No G" ) end
   if type( tbl.nu )   ~= "number" then error( "No nu" ) end
   if type( tbl.mu )   ~= "number" then error( "No mu" ) end

   -- getting the material into the table
   local newm = {}
   newm.name = tbl.name
   newm.rho = tbl.rho
   newm.G = tbl.G
   newm.nu = tbl.nu
   newm.mu = tbl.mu
   newm.CR = tbl.CR           or 1
   newm.alpha_t = tbl.alpha_t or 0.7
   newm.gamma = tbl.gamma     or 0
   newm.vR = tbl.vR           or 1

   table.insert( self.list, newm )

   self.nmats = self.nmats + 1
   self.hash[ newm.name ] = self.nmats

   -- restoring contact properties
   for i = 1, self.nmats do
      local m1 = self.list[i]
      local m2 = self.list[self.nmats]
      local prop = contact_props_default( m1, i, m2, self.nmats )
      table.insert( self.cmprops, prop )
   end

   return self
end

--- Resolves the material name into a matrial index in the material
--- list. It is a bit more than just checking hash because all error
--- message generating need to be here.
--- @tparam string name name of the material
--- @treturn number material number in the list of registered
--- materials
function material:resolve( name )

   local indx = self.hash[ name ]

   if indx == nil then
      error( 'No such material "' .. name .. '"' )
   end

   return indx
end

--- Create list of material names
--- @treturn table table-list with all material names in it.
function material:names()
   local names = tablex.imap( function (x) return x.name end, 
                              self.list )
   return names
end

--- Select the contact properties record by material names to edit or
--- print it
--- @tparam string name1 String name of one material (order of names
--- is insignificant)
--- @tparam string name2 String name of another material (order of
--- names is insignificant, name2 is optional, default is name1)
--- @treturn table cmprop A reference table that can be edited or
--- otherwise used to modify or display parameters for particular
--- contact
function material:select_cmprop( name1, name2 )

   local name2 = name2 or name1

   local matlist = self.list
   local mathash = self.hash
   local N = self.nmats
   local i = self.hash[ name1 ]
   local j = self.hash[ name2 ]

   -- calculation of the index by i,j: not trivial
   if i > j then i,j = j,i end
   local ind = ( (j-1) * j )/2 + i

   return self.cmprops[ind]
end

--*------------------------------------------------------------*--
--                   Standard Materials
--*------------------------------------------------------------*--

-- Sample material definition to demonstrate material fields
-- Sample material table that represents some abstract "rock"
-- properties to be used in examples or to demonstrate table
-- parameters that need to be submitted to functions like
-- @{material.create}
-- @tfield string name material name
-- @tfield number rho density (kg/m^3)
-- @tfield number G shear modulus (GPa)
-- @tfield number nu Poisson coefficient (1)
-- @tfield number mu friction coefficient for alike material contact (1)
-- @tfield number CR Restitution coefficient (optional, default 1)
-- @tfield number gamma surface energy (optional, default 0, J/m^2)
-- @tfield number alpha_t tangent elastic force reduction
-- coefficient, (optional, default 0.7, 1)
-- @tfield number vR velocity when CR is measured (optional, default
-- 1, m/s)
material.hdis_rock = {
   name = "hdis_rock",
   rho  = 3000.0,
   G    = 30,
   nu   = 0.3,
   mu   = 0.7,
   CR   = 0.5,
   gamma   = 0,
   alpha_t = 0.7,
   vR   = 1
}

--*------------------------------------------------------------*--

return material
