-- -*- Lua -*-

--
--  config.md generated from config.md.tpl file to provide description
--  for ldoc. To use ldoc run "ldoc ." in this directory. This will
--  generate the documentation for Lua facilities for COUPi
--


project = "<p>$NAME $VERSION</p><p>Lua Library</p>"

title = "$NAME Lua Lib"

description = "$NAME Discrete Element Method Model ver. $VERSION: Lua Library"

file = {
     "init.lua",
     "groups.lua",
     "aux.lua",
     "callback.lua",
     "control.lua",
     "dataset.lua",
     "dtauto.lua",
     "edit.lua",
     "grid.lua",
     "hdf5.lua",
     "loclib.lua",
     "material.lua",
     "mathx.lua",
     "mesh.lua",
     "quat.lua",
     "psd.lua",
     "shape.lua",
     "sphdens.lua",
     "sphtool.lua",
}

examples = {
         "../../samples/3sph.lua",
         "../../samples/3sph_runfrom.lua",
         "../../samples/ball.lua",
         "../../samples/ball_galt.lua",
         "../../samples/ball_monitored.lua",
         "../../samples/drag.lua",
         "../../samples/intrx.lua",
         "../../samples/matest.lua",
         "../../samples/spring.lua",

         "../utils/dens.lua",
         "../utils/pdist.lua"
 }

