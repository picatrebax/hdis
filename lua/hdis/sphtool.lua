--[[--

   Building large objects and tools from spheres.

   <p>This library provides objects built from spheres that may represent a
   sphere itself, a rectangular, a disk etc. This can be very useful for fast
   simulations or when it is desireble to have clear properties of the
   tools.</p>

   <p><em>For licensing and copyright use same license as Coupi.</em></p>

   @module hdis.sphtool
   @copyright 2016 Coupi, Inc.

--]]

aux  = require 'hdis.aux'
quat = require 'hdis.quat'
vec3d = require 'hdis.vec3d'
local sphtool = {}

------------------------------------------------------------------
--- Rectangle made from spheres.
---
--- @tparam number a -- width
--- @tparam number b -- height
--- @tparam number R -- Radius of spheres it is made from
--- @tparam number dr -- distance between centers of spheres it is
--- made from
--- @tparam string gname -- Name of the group
--- @tparam string mname -- Name of the material
--- @treturn table -- Shape table ready for hdis.append
------------------------------------------------------------------
function sphtool.rectangle( a, b, R, dr, gname, mname )

   local group  = hdis.groups:resolve( gname )
   local imat   = hdis.material:resolve( mname )
   local rho    = hdis.material.list[ imat ].rho

   ------------------------------------------------

   local S = a * b              -- area
   local D = 2 * R              -- diameter
   local V = D * S              -- approx above estimate of volume
   local m = rho * V            -- mass

   -- need angles for orientation (later) to build I

   local l12 = 1.0 / 12.0
   local Ix = l12 * m * ( D*D + b*b )
   local Iy = l12 * m * ( D*D + a*a )
   local Iz = l12 * m * ( a*a + b*b )

   ------------------------------------------------
   local body = {

      -- we do not specify center of mass here
      v = { 0, 0, 0 },         -- initial velocity (m/s)
      w = { 0, 0, 0 },         -- initial angular vel (rad/s)
      m = m,                   -- mass (kg)
      V = V,                   -- volume (m^3)
      I = { Ix, Iy, Iz },      -- main values of inertia tensor
      q = { 1, 0, 0, 0 },      -- initial rotation quaternion
   }

   ------------------------------------------------
   -- Parameters for building the mesh

   local Nx = math.ceil ( 0.5*a / dr )
   local dx = 0.5*a / Nx

   local Ny = math.ceil ( 0.5*b / dr )
   local dy = 0.5*b / Ny

   ------------------------------------------------
   local atoms = {}
   local verts = {}

   -- loop for all positions
   for j = 1, 2 * Ny + 1 do

      local ypos = ( j - Ny - 1 ) * dy

      for i = 1, 2 * Nx + 1 do

         local pos = 
            {
               ( i - Nx - 1 ) * dx,
               ypos,
               0
            }

         local nV = #verts

         table.insert(
            atoms,
            {
               R = R,
               sbb_R = R,
               group = group,
               material = imat,
               sbb_c = pos,
               nverts = 1,
               nedges = 0,
               nfaces = 0,
               ivert0 = nV + 1,
               iedge0 = 1,
               iface0 = 1,
               isolated = 1
            }
         )
         
         table.insert(
            verts,
            {
               local_pos  = pos,
               global_pos = pos,
               deg    = 0,
               iadj_e = 1
            }
         )
         
      end
   end

   local faces = {}
   local edges = {}
   local adj_e = {}

   return { body  = body,
            atoms = atoms,
            faces = faces,
            edges = edges,
            verts = verts,
            adj_e = adj_e }

end


------------------------------------------------------------------
--- Spherical coordinates for use in largesphere
------------------------------------------------------------------
local function sph_coordinates( R, phi, theta )
   local x = R * math.sin( theta ) * math.cos( phi )
   local y = R * math.sin( theta ) * math.sin( phi )
   local z = R * math.cos( theta )
   return x, y, z
end

------------------------------------------------------------------
--- Large Sphere
--- @tparam number R -- Radius (actual radius of the sphere)
--- @tparam number r -- radii of small spheres
--- @tparam number dphi --- angle in degrees for how often small
--- spheres appear on the meridian. They mean resolution
--- @tparam string gname -- Name of the group
--- @tparam string mname -- Name of the material
--- @treturn table -- Shape table ready for hdis.append
--- The return sphere will be built of small spheres of radius r,
--- places equally in R-r distance from the center such making an
--- outer boundary at distance R. The volume, mass, and momentum will
--- be calculated for radius R.
------------------------------------------------------------------
function sphtool.largesphere( R, r, dphi, gname, mname )

   local group  = hdis.groups:resolve( gname )
   local imat   = hdis.material:resolve( mname )
   local rho    = hdis.material.list[ imat ].rho

   ------------------------------------------------

   local dphi = math.pi * dphi / 180.0

   local Rp = R - r              -- actual radius where to put points
   local V  = (4.0/3.0) * math.pi * R * R * R  -- estimate of volume
   local m  = rho * V            -- mass

   -- need angles for orientation (later) to build I

   local Ix = 0.4 * m * R * R

   ------------------------------------------------
   local body = {

      -- we do not specify center of mass here
      v = { 0, 0, 0 },         -- initial velocity (m/s)
      w = { 0, 0, 0 },         -- initial angular vel (rad/s)
      m = m,                   -- mass (kg)
      V = V,                   -- volume (m^3)
      I = { Ix, Ix, Ix },      -- main values of inertia tensor
      q = { 1, 0, 0, 0 },      -- initial rotation quaternion
   }

   ------------------------------------------------
   --

   -- how many points we get along meridian
   local Ntheta = math.ceil( 2 * math.pi / dphi )
   local dtheta = 2 * math.pi / Ntheta

   ------------------------------------------------
   local atoms = {}
   local verts = {}

   -- loop for all positions
   for j = 1, Ntheta do              -- along meridian: each latitude

      local theta = ( j - 1 ) * dtheta

      local Nphi = 1 + math.floor( 2 * math.pi * math.sin( theta ) / dtheta )
      local dphi = 2 * math.pi / Nphi

      for i = 1, Nphi do

         local phi = ( i - 1 ) * dphi
         local x,y,z  = sph_coordinates( Rp, phi, theta )
         local pos = { x, y, z }

         local nV = #verts

         table.insert(
            atoms,
            {
               R = r,
               sbb_R = r,
               group = group,
               material = imat,
               sbb_c = pos,
               nverts = 1,
               nedges = 0,
               nfaces = 0,
               ivert0 = nV + 1,
               iedge0 = 1,
               iface0 = 1,
               isolated = 1
            }
                     )

         table.insert(
            verts,
            {
               local_pos  = pos,
               global_pos = pos,
               deg    = 0,
               iadj_e = 1
            }
                     )

      end
   end

   local faces = {}
   local edges = {}
   local adj_e = {}

   return { body  = body,
            atoms = atoms,
            faces = faces,
            edges = edges,
            verts = verts,
            adj_e = adj_e }

end


------------------------------------------------------------------
--- Cylindrical coordinates for use in largecylinder
------------------------------------------------------------------
local function cyl_coordinates( R, phi )
   local x = R * math.cos( phi )
   local y = R * math.sin( phi )
   return x, y
end

------------------------------------------------------------------
--- Large Cylinder (vertical)
--- @tparam number H -- Height of the cylinder
--- @tparam number R -- Radius (actual external radius of the cylinder)
--- @tparam number r -- radii of small spheres
--- @tparam number dz --- distance between spheres
--- spheres appear on the meridian. They mean resolution
--- @tparam string gname -- Name of the group
--- @tparam string mname -- Name of the material
--- @treturn table -- Shape table ready for hdis.append
--- The return cylinder will be built of small spheres of radius r,
--- places equally in R-r distance from the center such making an
--- outer boundary at distance R. The volume, mass, and momentum will
--- be calculated for radius R for full solid cylinder.
------------------------------------------------------------------
function sphtool.largecylinder( H, R, r, dz, gname, mname )

   local group  = hdis.groups:resolve( gname )
   local imat   = hdis.material:resolve( mname )
   local rho    = hdis.material.list[ imat ].rho

   ------------------------------------------------

   local Rp = R - r              -- actual radius where to put points
   local dphi = dz / R          -- angular step along circle

   local V  = math.pi * R * R * H  -- estimate of volume
   local m  = rho * V            -- mass

   -- need angles for orientation (later) to build I

   local Ix = 0.4 * m * R * R   -- TODO: update to the correct value
   local Iy = Ix
   local Iz = Ix                -- TODO: change to the correct value

   ------------------------------------------------
   local body = {

      -- we do not specify center of mass here
      v = { 0, 0, 0 },         -- initial velocity (m/s)
      w = { 0, 0, 0 },         -- initial angular vel (rad/s)
      m = m,                   -- mass (kg)
      V = V,                   -- volume (m^3)
      I = { Ix, Iy, Iz },      -- main values of inertia tensor
      q = { 1, 0, 0, 0 },      -- initial rotation quaternion
   }

   ------------------------------------------------
   --

   -- how many points we get along the circle
   local Nphi = 1 + math.ceil( 2 * math.pi / dphi )
   local Nz   = 1 + math.ceil( H / dz )

   local dz   = H / Nz          -- replacing dz with the new value
   dphi = 2 * math.pi / Nphi -- replacing dphi with new value
   local zmin = - 0.5 * H

   ------------------------------------------------
   local atoms = {}
   local verts = {}

   -- loop for all positions
   for j = 1, Nz do              -- along meridian: each latitude

      local z = zmin + ( j - 1 ) * dz

      for i = 1, Nphi do

         local phi = ( i - 1 ) * dphi
         local x,y  = cyl_coordinates( Rp, phi )
         local pos = { x, y, z }

         local nV = #verts

         table.insert(
            atoms,
            {
               R = r,
               sbb_R = r,
               group = group,
               material = imat,
               sbb_c = pos,
               nverts = 1,
               nedges = 0,
               nfaces = 0,
               ivert0 = nV + 1,
               iedge0 = 1,
               iface0 = 1,
               isolated = 1
            }
                     )

         table.insert(
            verts,
            {
               local_pos  = pos,
               global_pos = pos,
               deg    = 0,
               iadj_e = 1
            }
                     )

      end
   end

   local faces = {}
   local edges = {}
   local adj_e = {}

   return { body  = body,
            atoms = atoms,
            faces = faces,
            edges = edges,
            verts = verts,
            adj_e = adj_e }

end


--------------------------------------------------------------------
---       Function that returns a disk made out of spheres
--- Sometimes it is more efficient to make a tool by spheres. Disk is a
--- good example. This disk is convenient to be used in triaxial
--- geotechnical test
--------------------------------------------------------------------
---
--- @tparam number R Radius of the disk, the final radius will be
--- exactly equal to the disk radius. For the "core" radius, use R -
--- r/2 value.
--- @tparam number r sphere radius. 2r will be the thickness of the disk
--- @tparam number dx cell size for placing spheres. Use r value for
--- nice solid disks
--- @tparam string gname -- Name of the group
--- @tparam string mname -- Name of the material
--- @treturn table disk
function sphtool.sph_disk( R, r, dx, gname, mname )

   -- group and material resolution + density
   local group  = hdis.groups:resolve( gname )
   local imat   = hdis.material:resolve( mname )
   local rho = hdis.material.list[ imat ].rho

   local body = {}
   local body = {}
   local atoms = {}
   local faces = {}
   local edges = {}
   local verts = {}
   local adj_e = {}

   local body = {
      v = { 0, 0, 0 },
      w = { 0, 0, 0 },
      V = 2*math.pi*R*R * (2*r),
      I = { 1, 1, 1 },
      q = { 1, 0, 0, 0 },
   }
   body.m = rho * body.V

   local dx = dx
   local n = 1 + math.floor( 2 * R / dx )
   dx = 2 * R / n

   -- points and atom-spheres
   local nverts = 1

   for i = 0, n do
      local x = i * dx - R
      for j = 0, n do
         local y = j * dx - R

         if x*x + y*y < R*R then

            local atom = {
               R = r,
               group = group,
               material = imat,
               sbb_c = { x, y, 0 },
               sbb_R = r,
               nverts = 1,
               nedges = 0,
               nfaces = 0,
               ivert0 = nverts,
               iedge0 = 1,
               iface0 = 1,
               isolated = 0
            }

            table.insert( atoms, atom )

            local vert = {
               local_pos = { x, y, 0 },
               global_pos = { x, y, 0 },
               deg = 0,
               iadj_e = 1,
            }

            table.insert( verts, vert )
            nverts = nverts + 1
         end
      end
   end

   local shape = {
      body=body,
      atoms=atoms,
      faces=faces,
      edges=edges,
      verts=verts,
      adj_e=adj_e }

   return shape
end

---------------------------------------------------------------------------------
------------------------------------------------
--- helper function to calculate angle in radians that is correspond to the arc.
--- Should be called with parameters:
--- @tparam number b -- x half-axis
--- @tparam number a -- y half-axis
--- @tparam number t0 -- starting angle
--- @tparam number arc -- length of the arc
------------------------------------------------
function ellipse_arc_lngth_to_rdns( b, a, t0, arc )

   local dt = math.pi / (180*1000)
   local S = 0
   local t = t0

   while S < arc do
      t = t + dt/2
      S = S + math.sqrt((a*math.sin(t))^2 + (b*math.cos(t))^2)*dt
      t = t + dt/2
   end
   if (t - t0 < 0) then
      t = 0
   else
      t = t - t0
   end
   return t

end
------------------------------------------------
--- helper function to place the point to the arc.
--- Should be called with parameters:
--- @tparam number a -- a half-axis
--- @tparam number b -- b half-axis
--- @tparam number theta -- angle from the x-axis to the radius to the point.
------------------------------------------------
function pnt_on_ellipse( a, b, theta )

   local localx = math.sqrt( 1 / ( ((1 / a)^2) + ((math.tan(theta) / b)^2) ) )
   local localy = 0
   local dtheta = math.pi / (360*10)
   if ( theta > (math.pi / 2) ) and ( theta < (3 * math.pi / 2) ) then
      localx = localx * (-1)
   end
   if ( theta > 0 - dtheta ) and ( theta < 0 + dtheta ) then
      localx = a
      localy = 0
   elseif   ( theta > (math.pi / 2) - dtheta ) and
            ( theta < (math.pi / 2) + dtheta ) then
      localx = 0
      localy = b
   elseif   ( theta > math.pi - dtheta ) and
            ( theta < math.pi + dtheta ) then
      localx = -a
      localy = 0
   elseif   ( theta > (3 * math.pi / 2) - dtheta ) and
            ( theta < (3 * math.pi / 2) + dtheta ) then
      localx = 0
      localy = -b
   elseif   ( theta > (2 * math.pi) - dtheta ) and
            ( theta > (2 * math.pi) + dtheta ) then
      localx = a
      localy = 0
   else
      localy = localx * math.tan(theta)
   end
   return { localx, localy }
end
------------------------------------------------
--- helper function to calculate the length of the arc of the ellipsoid.
---
--- @tparam number a -- a half-axis
--- @tparam number b -- b half-axis
------------------------------------------------
function ellipse_arc_lngth( a , b )

   return 4 * ( math.pi * a * b + ( a - b )^2 ) / ( a + b )
end

-------------------------------------------------------------------------------
--- helper function to insert the object into the atom table.
---
--- @tparam table atoms  -- table of atoms
--- @tparam table verts  -- table of verts
--- @tparam number r     -- radius of the atom
--- @tparam table group  -- group of the atom
--- @tparam string imat  -- material of the atom
--- @tparam number pos   -- position of the atom
--- @treturn table atoms -- same table with inserted atom
-------------------------------------------------------------------------------
local function instr_the_atom( atoms, verts, r, group, imat, pos )

   local nV = #verts

   table.insert(
                  atoms,
                  {
                  R = r,
                  sbb_R = r,
                  group = group,
                  material = imat,
                  sbb_c = pos,
                  nverts = 1,
                  nedges = 0,
                  nfaces = 0,
                  ivert0 = nV + 1,
                  iedge0 = 1,
                  iface0 = 1,
                  isolated = 1
                  }
               )
   return atoms
end
------------------------------------------------
--- helper function to insert the object into the verts table.
---
--- @tparam table verts -- table of verts
--- @tparam number pos  -- position of the atom
------------------------------------------------
local function instr_the_vert( verts, pos )

   table.insert(
                  verts,
                  {
                  local_pos  = pos,
                  global_pos = pos,
                  deg    = 0,
                  iadj_e = 1
                  }
               )
   return verts
end
-------------------------------------------------------------------------------
--- Ellipsoid builded from spheres
--- @tparam number a -- x half-axis
--- @tparam number b -- y half-axis
--- @tparam number c -- z half-axis
--- @tparam number r -- radii of small spheres that make the surface
--- @tparam number dphi – pseudo angle in degrees that is correspond to how often
--- small spheres appear on the meridian. It is means resolution or density porosity
--- @tparam string gname -- Name of the group
--- @tparam string mname -- Name of the material
--- @treturn table -- Shape table ready for hdis.append.
--- It is an ellipsoid which will be built from small spheres of radius r, placed
--- equally in distance r from the outer imaginary boundary. The volume, mass,
--- and momentum will be calculated for ellipsoid which is simulated.
-------------------------------------------------------------------------------
function sphtool.ellipsoid( a, b, c, r, dphi, gname, mname )

   local group  = hdis.groups:resolve( gname )
   local imat   = hdis.material:resolve( mname )
   local rho    = hdis.material.list[ imat ].rho
   ------------------------------------------------
   --calculation of the arc length of the biggest ellipse
   local alcl = 0
   local blcl = 0
   local clcl = math.min(a,b,c)

   if a == clcl then
      alcl = b
      blcl = c
   elseif b == clcl then
      alcl = a
      blcl = c
   else
      alcl = a
      blcl = b
   end

   local mnarclngth = ellipse_arc_lngth( alcl, blcl )
   alcl,blcl,clcl = nil
   ------------------------------------------------
   local dphi = math.ceil(360 / dphi) -- points debcity on the biggest ellipse
   dphi = mnarclngth / dphi -- length of the arc between neighbours
   ------------------------------------------------
   local V  = 4/3 * math.pi * a * b * c   -- estimate of volume
   local m  = rho * V                     -- mass
   -- need angles for orientation (later) to build I
   local Ix = 0.2 * m * (b^2+c^2)
   local Iy = 0.2 * m * (c^2+a^2)
   local Iz = 0.2 * m * (a^2+b^2)
   ------------------------------------------------
   local body = {
      -- we do not specify center of mass here
      v = { 0, 0, 0 },         -- initial velocity (m/s)
      w = { 0, 0, 0 },         -- initial angular vel (rad/s)
      m = m,                   -- mass (kg)
      V = V,                   -- volume (m^3)
      I = { Ix, Iy, Iz },      -- main values of inertia tensor
      q = { 1, 0, 0, 0 },      -- initial rotation quaternion
   }
   ------------------------------------------------
   local atoms = {}
   local verts = {}
   ------------------------------------------------
   --recalculating of the dphi for concrete meridian
   mnarclngth = ellipse_arc_lngth( a, b ) / 2
   Ntheta = math.ceil( mnarclngth / dphi )
   local thetadphi = mnarclngth / Ntheta
   local theta = 0
   -- loop for all positions along main (x0y) meridian: each latitude
   for i = 1,Ntheta do

      local thetax = pnt_on_ellipse( a, b, theta ) -- place the point on the ellipse
      local thetay = thetax[2]
      thetax = thetax[1]

      local arclngth = 0 -- recalculating of the dphi for concrete meridian
      local Nphi = 0
      local phidphi = 0
      local thetaz = (thetay / b) * c
      if not (thetay == 0) then
         arclngth = ellipse_arc_lngth( thetay, thetaz )
         Nphi = math.ceil( arclngth / dphi )
         phidphi = arclngth / Nphi
      end

      if (Nphi == 0) then --processing of the points on the poles
         local pos = { (a-r), 0, 0 }

         atoms = instr_the_atom( atoms, verts , r, group, imat, pos )
         verts = instr_the_vert( verts, pos )

         pos = { -(a-r), 0, 0 }

         atoms = instr_the_atom( atoms, verts , r, group, imat, pos )
         verts = instr_the_vert( verts, pos )
      end

      local phi = 0
      for j = 1,Nphi do
         -- creating point on elipse surface
         local x  = thetax
         local y  = pnt_on_ellipse( thetay, thetaz, phi ) --place point on the ellipse
         local z  = y[2]
         y        = y[1]

         ---[[--all points should be inside ellipsoid
         local norm = vec3d:new( -2 * x / (a^2),
                                 -2 * y / (b^2),
                                 -2 * z / (c^2) )
         norm = norm:normalize()
         norm = norm:scale(r)
         x = x + norm:x()
         y = y + norm:y()
         z = z + norm:z()
         --]]--

         local pos = { x, y, z }

         atoms = instr_the_atom( atoms, verts , r, group, imat, pos )
         verts = instr_the_vert( verts, pos )

         --rotate angle to the next point
         local dphi = ellipse_arc_lngth_to_rdns( thetay, thetaz, phi, phidphi)
         phi = phi + dphi
      end
      --rotate angle to the next meridian
      local dtheta = ellipse_arc_lngth_to_rdns( a, b, theta, thetadphi)
      theta = theta + dtheta
   end

   local faces = {}
   local edges = {}
   local adj_e = {}

   return { body  = body,
            atoms = atoms,
            faces = faces,
            edges = edges,
            verts = verts,
            adj_e = adj_e }
end

--------------------------------------------------------------------
---       Function that returns a ring made out of spheres
---
---  Originally this function was written for Comet lander leg
---  simulation
--------------------------------------------------------------------
---
--- @tparam number R1 Internal radius of the ring,
--- @tparam number R2 External radius of the ring
--- @tparam number r sphere radius. 2r will be the thickness of the ring
--- @tparam number dR cell size for placing spheres. Use r value for
--- nice solid rings
--- @tparam string gname -- Name of the group
--- @tparam string mname -- Name of the material
--- @treturn table disk
function sphtool.sph_ring( R1, R2, r, dR, gname, mname )

   assert ( R1 < R2 )

   -- group and material resolution + density
   local group  = hdis.groups:resolve( gname )
   local imat   = hdis.material:resolve( mname )
   local rho = hdis.material.list[ imat ].rho

   local body = {}
   local atoms = {}
   local faces = {}
   local edges = {}
   local verts = {}
   local adj_e = {}

   local h = 2*r
   local V = math.pi * h * ( R2*R2 - R1*R1 )
   local m = rho * V
   local Iz = 0.5 * m * ( R2*R2 + R1*R1 )
   local Ix = (1.0/12.0) * m * ( 3 * ( R2*R2 + R1*R1 ) + h*h )

   local body = {
      v = { 0, 0, 0 },
      w = { 0, 0, 0 },
      V = V,
      I = { Ix, Ix, Iz },
      q = { 1, 0, 0, 0 },
      m = m,
   }

   local dR = dR
   local nR = 1 + math.floor( ( R2 - R1 - 2*r ) / dR )
   if nR < 2 then nR = 2 end    -- correction
   dR = ( R2 - R1 - 2*r ) / nR               -- correction

   -- points and atom-spheres
   local nverts = 1

   for i = 0, nR do

      local R = R1 + r + i*dR

      -- calculation and correction of dphi
      local dphi = dR / R
      local nphi = 1 + math.floor( 2*math.pi / dphi )

      dphi = 2*math.pi / nphi

      for j = 0, nphi-1 do

         local phi = j * dphi
         local x = R * math.cos( phi )
         local y = R * math.sin( phi )

         local atom = {
            R = r,
            group = group,
            material = imat,
            sbb_c = { x, y, 0 },
            sbb_R = r,
            nverts = 1,
            nedges = 0,
            nfaces = 0,
            ivert0 = nverts,
            iedge0 = 1,
            iface0 = 1,
            isolated = 0
         }

         table.insert( atoms, atom )

         local vert = {
            local_pos = { x, y, 0 },
            global_pos = { x, y, 0 },
            deg = 0,
            iadj_e = 1,
         }

         table.insert( verts, vert )
         nverts = nverts + 1
      end
   end

   local shape = {
      body=body,
      atoms=atoms,
      faces=faces,
      edges=edges,
      verts=verts,
      adj_e=adj_e }

   return shape
end

--------------------------------------------------------------------
---   Function that returns an open cone shell made out of spheres
---
---  Originally this function was written for Comet lander leg
---  simulation
---
---  Attention: I, V, and m are not set for this shape. The center is
---  located in the center of R1 circle (base). This is done only for
---  simplicity.
---
--------------------------------------------------------------------
---
--- @tparam number R1 Lower radius
--- @tparam number R2 Upper radius
--- @tparam number H  Height
--- @tparam number r sphere radius. 2r will be the thickness of the ring
--- @tparam number dR cell size for placing spheres. Use r value for
--- nice solid rings
--- @tparam string gname -- Name of the group
--- @tparam string mname -- Name of the material
--- @treturn table disk
function sphtool.sph_opencone( R1, R2, H, r, dR, gname, mname )

   -- group and material resolution + density
   local group  = hdis.groups:resolve( gname )
   local imat   = hdis.material:resolve( mname )
   local rho = hdis.material.list[ imat ].rho

   local body = {}
   local atoms = {}
   local faces = {}
   local edges = {}
   local verts = {}
   local adj_e = {}

   local L = math.sqrt( (R1-R2)^2 + H^2 )

   -- This parameters are vague
   local V = 1
   local m = 1
   local Ix = 1

   local body = {
      v = { 0, 0, 0 },
      w = { 0, 0, 0 },
      V = V,
      I = { Ix, Ix, Ix },
      q = { 1, 0, 0, 0 },
      m = m,
   }

   local Cos = H / L
   local Sin = ( R1 - R2 ) / L
   local Tan = Sin / Cos

   local dL = dR
   local nL = 1 + math.floor(  L / dR )
   if nL < 2 then nL = 2 end    -- correction
   dL = ( L - 2*r ) / nL

   -- points and atom-spheres
   local nverts = 1

   for i = 0, nL do

      local z = i * dL * Cos
      local R = R2 + ( H - z ) * Tan

      -- calculation and correction of dphi
      local dphi = dR / R
      local nphi = 1 + math.floor( 2*math.pi / dphi )

      dphi = 2*math.pi / nphi

      for j = 0, nphi-1 do

         local phi = j * dphi
         local x = R * math.cos( phi )
         local y = R * math.sin( phi )

         local atom = {
            R = r,
            group = group,
            material = imat,
            sbb_c = { x, y, z },
            sbb_R = r,
            nverts = 1,
            nedges = 0,
            nfaces = 0,
            ivert0 = nverts,
            iedge0 = 1,
            iface0 = 1,
            isolated = 0
         }

         table.insert( atoms, atom )

         local vert = {
            local_pos = { x, y, z },
            global_pos = { x, y, z },
            deg = 0,
            iadj_e = 1,
         }

         table.insert( verts, vert )
         nverts = nverts + 1
      end
   end

   local shape = {
      body=body,
      atoms=atoms,
      faces=faces,
      edges=edges,
      verts=verts,
      adj_e=adj_e }

   return shape
end

--------------------------------------------------------------------
---   Function that returns an open cone shell made out of spheres
---   with a bottom rim
---
---  Originally this function was written for Comet lander leg
---  simulation
---
---  Attention: I, V, and m are not set for this shape. The center is
---  located in the center of R1 circle (base).
---
--------------------------------------------------------------------
---
--- @tparam number RB1 Internal radius of the rim
--- @tparam number RB2 External radius of the rim
--- @tparam number R1 Lower radius of the cone
--- @tparam number R2 Upper radius of the cone
--- @tparam number H  Height
--- @tparam number r sphere radius. 2r will be the thickness of the ring
--- @tparam number dR cell size for placing spheres. Use r value for
--- nice solid rings
--- @tparam string gname -- Name of the group
--- @tparam string mname -- Name of the material
--- @treturn table disk
function sphtool.sph_opencone_rim( RB1, RB2,
                                   R1, R2, H,
                                   r, dR,
                                   gname, mname )

   -- group and material resolution + density
   local group  = hdis.groups:resolve( gname )
   local imat   = hdis.material:resolve( mname )
   local rho = hdis.material.list[ imat ].rho

   local body = {}
   local atoms = {}
   local faces = {}
   local edges = {}
   local verts = {}
   local adj_e = {}

   -- This parameters are vague
   local V = 1
   local m = 1
   local Ix = 1

   local body = {
      v = { 0, 0, 0 },
      w = { 0, 0, 0 },
      V = V,
      I = { Ix, Ix, Ix },
      q = { 1, 0, 0, 0 },
      m = m,
   }

   -- points and atom-spheres
   local nverts = 1

   -- local access to dR
   local dR = dR

   -- Rim Creation ****

   local nR = 1 + math.floor( ( RB2 - RB1 - 2*r ) / dR )
   if nR < 2 then nR = 2 end    -- correction
   local dRim = ( RB2 - RB1 - 2*r ) / nR               -- correction

   for i = 0, nR do

      local R = RB1 + r + i*dRim

      -- calculation and correction of dphi
      local dphi = dRim / R
      local nphi = 1 + math.floor( 2*math.pi / dphi )

      dphi = 2*math.pi / nphi

      for j = 0, nphi-1 do

         local phi = j * dphi
         local x = R * math.cos( phi )
         local y = R * math.sin( phi )

         local atom = {
            R = r,
            group = group,
            material = imat,
            sbb_c = { x, y, 0 },
            sbb_R = r,
            nverts = 1,
            nedges = 0,
            nfaces = 0,
            ivert0 = nverts,
            iedge0 = 1,
            iface0 = 1,
            isolated = 0
         }

         table.insert( atoms, atom )

         local vert = {
            local_pos = { x, y, 0 },
            global_pos = { x, y, 0 },
            deg = 0,
            iadj_e = 1,
         }

         table.insert( verts, vert )
         nverts = nverts + 1
      end
   end

   -- Cone creation ****

   local L = math.sqrt( (R1-R2)^2 + H^2 ) -- slant height

   local Cos = H / L
   local Sin = ( R1 - R2 ) / L
   local Tan = Sin / Cos

   local dL = dR
   local nL = 1 + math.floor(  L / dR )
   if nL < 2 then nL = 2 end    -- correction
   dL = ( L - 2*r ) / nL

   for i = 0, nL do

      local z = i * dL * Cos
      local R = R2 + r + ( H - z ) * Tan

      -- calculation and correction of dphi
      local dphi = dR / R
      local nphi = 1 + math.floor( 2*math.pi / dphi )

      dphi = 2*math.pi / nphi

      for j = 0, nphi-1 do

         local phi = j * dphi
         local x = R * math.cos( phi )
         local y = R * math.sin( phi )

         local atom = {
            R = r,
            group = group,
            material = imat,
            sbb_c = { x, y, z },
            sbb_R = r,
            nverts = 1,
            nedges = 0,
            nfaces = 0,
            ivert0 = nverts,
            iedge0 = 1,
            iface0 = 1,
            isolated = 0
         }

         table.insert( atoms, atom )

         local vert = {
            local_pos = { x, y, z },
            global_pos = { x, y, z },
            deg = 0,
            iadj_e = 1,
         }

         table.insert( verts, vert )
         nverts = nverts + 1
      end
   end

   local shape = {
      body=body,
      atoms=atoms,
      faces=faces,
      edges=edges,
      verts=verts,
      adj_e=adj_e }

   return shape
end


--------------------------------------------------------------------
--- Special function to make a transform:
--- P = (q*) ( (c_i - c) + (q_i)P_i(q*_i) ) (q)
--------------------------------------------------------------------
local function ptransform( Pi, ci, qi, c, q )

   local qiC = qi:conjugate()
   local qC = q:conjugate()
   local dc = { ci[1]-c[1], ci[2]-c[2], ci[3]-c[3] }

   local QPi = quat:new(0, Pi[1], Pi[2], Pi[3] )
   local Qdc = quat:new(0, dc[1], dc[2], dc[3] )

   local Qtr = quat:new()
   Qtr = qC * ( Qdc + qi*QPi*qiC ) * q
--   Qtr = q * ( Qdc + qiC*QPi*qi ) * qC

   local X = { Qtr.q[2], Qtr.q[3], Qtr.q[4] }

   return X
end

--------------------------------------------------------------------
---       Function that returns a new shape that is a correct union of
--- some complete shapes. ATTENTION: ONLY WORKS FOR BODIES MADE FROM
--- SPHERES
--------------------------------------------------------------------
--- @tparam table c vector 3D with new global center
--- @tparam table q new 3D quaternion, as a table {a,b,c,d}
--- @tparam table shapes List of shapes to union (merge)
--- @treturn table new body that is a union with new cooridinates
function sphtool.sph_union( c, q, shapes )

   local newshape = {}
   newshape.atoms = {}
   newshape.verts = {}
   newshape.body = aux.deepcopy( shapes[1].body )
   newshape.body.q = q
   newshape.body.c = c

   newshape.faces = {}
   newshape.edges = {}
   newshape.adj_e = {}

   local dvert = 0              -- each ivert0 will start with shift
   local Q = quat:new( q )
   for i, shape in ipairs( shapes ) do
      local Qi = quat:new( shape.body.q )

      -- atoms
      for j, atom in ipairs( shape.atoms ) do
         local newatom = {}
         newatom = aux.deepcopy( atom )
         newatom.sbb_c = ptransform( atom.sbb_c,
                                     shape.body.c, Qi,
                                     c, Q )
         newatom.ivert0 = atom.ivert0 + dvert
         table.insert( newshape.atoms, newatom )
      end

      -- verts
      for j, vert in ipairs( shape.verts ) do
         local newvert = {}
         newvert = aux.deepcopy( vert )
         newvert.local_pos = ptransform( vert.local_pos,
                                         shape.body.c, Qi,
                                         c, Q )
         newvert.global_pos = newvert.local_pos
         dvert = dvert + 1

         table.insert( newshape.verts, newvert )
      end

      -- body update
      if i ~= 1 then
         newshape.body.m = newshape.body.m + shape.body.m
      end

   end

   return newshape
end

--*------------------------------------------------------------*--

return sphtool
