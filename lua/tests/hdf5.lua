--[[
	
		Lua hdf5 read/write test. Given an input file and an output file,
		it tests copying the file and then reading/writing each table.

--]]

local hdf5 = require "hdis.hdf5"

-- retrieve the file name from the command line and abort if none found
local i_file = arg[1]
local o_file = arg[2]
if not (i_file and o_file) then
	print("Usage is \"lua hdf5.lua INPUT_FILE OUTPUT_FILE\" (use absolute paths for filenames)");
	return
end

local tables = { "Atom Group", "Atoms", "Bodies", "Boundary", "Box (domain)", "CDScheduler",
	"Contact Hash Assosiate", "Contact List", "Contact Mech. Properties", "Drag Force Values",
	"General Settings", "Gravity", "Material Names", "Materials", "PTriangles", "Points", "SBBs",
	"Springs", "Time_Parameters" }

local table

print("\nBEGINNING HDF5 TEST")
print(string.rep("-", 30) .. "\n")

io.write("Copying file" .. string.rep(".", 23))
hdf5.copy_file(i_file, o_file)
io.write("success\n")

for i,v in ipairs(tables) do
	io.write("Reading " .. v .. string.rep(".", 27-string.len(v)))
	table = nil
	local dims = hdf5.get_dims(i_file, v)
	if dims >= 0 then
		table = hdf5.read(i_file, v)
		io.write("success (" .. table["n"] .. " elements found)\n")
		io.write("Writing " .. v .. string.rep(".", 27-string.len(v)))
		hdf5.write(o_file, table)
		io.write("success\n")
	else
		io.write("FAILURE (table not found)\n")
	end
	--io.write("\n")
end

print("\n" .. string.rep("-", 30))
print("TEST COMPLETE\n")
