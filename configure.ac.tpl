# -*- Autoconf -*-

# Process this file with autoconf to produce a configure script.

#
#  configure.ac.tpl is a template used by bootstrap.py script
#  to produce an actual configure.ac
#


# ====================================================================
#               *********  INITIALIZATION **********
# ====================================================================

AC_PREREQ(2.60)
AC_INIT(${NAME}, ${VERSION}, ${EMAIL})

AC_CONFIG_SRCDIR([config.h.in])
AC_CONFIG_HEADER([config.h])

AC_CONFIG_MACRO_DIR([m4])
m4_pattern_allow([AC_MSG_FAILURE])

# Check for a system
AC_CANONICAL_TARGET

# We define the target_os_base variable
case x${target_os} in
  *darwin*) target_os_base='MacOSX' ;;
  *linux*)  target_os_base='GNU/Linux' ;;
  *) AC_MSG_ERROR([Unsupported OS ${target_os}]) ;;
esac
echo "System: ${target_os_base}"

AM_CONDITIONAL( [OSDARWIN], [test x${target_os_base} = x'MacOSX'] )
AM_CONDITIONAL( [OSLINUX],  [test x${target_os_base} = x'GNU/Linux'] )

AM_INIT_AUTOMAKE

LT_INIT([disable-static])
AC_SUBST(LT_SHELL)

# ====================================================================
#            *********  COMPILERS AND UTILITIES **********
# ====================================================================

AC_PROG_CC
AC_PROG_CC_C99
AC_PROG_LN_S
AC_PROG_INSTALL

AM_CONDITIONAL( [ISGCC], [test x${GCC} = xyes] )

# ====================================================================
#                  *********  MAKEFILES *********
# ====================================================================

AC_CONFIG_FILES([Makefile
                 src/Makefile
                 lua/Makefile
                 lua/hdis/Makefile
                 lua/lib/Makefile
                 lua/utils/Makefile
                 lua/tests/Makefile])

# ====================================================================
#                *********  ENABLE OPTIONS **********
# ====================================================================

AC_MSG_NOTICE( [-------------------------------------] )
AC_MSG_NOTICE( [*** Checking for --enable options ***] )
AC_MSG_NOTICE( [-------------------------------------] )

AC_ARG_ENABLE([parallel],
     [AS_HELP_STRING( [--enable-parallel=type],
                      [enable parallel execution,
                       use following types for parallel model:
                       openmp, gpu, none  (default is openmp). Please
                       note that using a parallel mode may require using
                       its special compiler. In this case setting
                       CC variable is also required. For example
                       ./configure CC="gpucc -std=gnu99" ...
                       Do not forget to specify the option that
                       enables compilation of ANSI C99 code, it
                       is different for different compilers] )],
     [case "${enableval}" in
      yes) parallel=openmp;;
      openmp) parallel=openmp ;;
      gpu) parallel=gpu ;;
      none) parallel=none ;;
      *)
      AC_MSG_ERROR([bad value ${enableval} for --enable-parallel]) ;; esac],
     [parallel=none] )

# Attempt to check OpenMP flags.
case "${parallel}" in
   openmp) AC_OPENMP;;
esac

# setting all the parameters for Makefile
AM_CONDITIONAL( [PARALLEL_OPENMP],   [test x$parallel = xopenmp]   )
AM_CONDITIONAL( [PARALLEL_GPU],      [test x$parallel = xgpu]      )
AM_CONDITIONAL( [PARALLEL_NONE],     [test x$parallel = xnone]     )

# --------------------------------------------------------------------
# Enabling profiling with different profilers (TODO: tau)
# --------------------------------------------------------------------

AC_ARG_ENABLE( [profile],
  [AS_HELP_STRING( [--enable-profile],
                   [turn on profiling compilation (for developers)] )],
  [case "${enableval}" in
   yes) profile=true  ;;
    no) profile=false ;;
     *) AC_MSG_ERROR( [bad value ${enableval} for --enable-profile]) ;;
   esac],
  [profile=false])

AM_CONDITIONAL( [PROFILE], [test x$profile = xtrue] )

# --------------------------------------------------------------------
# Check for float point operation correctness, no NaNs allowed
# --------------------------------------------------------------------

AC_ARG_ENABLE( [fp-check],
   [AS_HELP_STRING( [--enable-fp-check],
                    [turn on float point NaN check,
                     if enabled will stop at first NaN or inf] )],
   [case "${enableval}" in
    yes) fpcheck=true  ;;
     no) fpcheck=false ;;
      *) AC_MSG_ERROR( [bad value ${enableval}
                        for --enable-fp-check]) ;;
    esac],
   [fpcheck=false] )

AM_CONDITIONAL( [FPCHECK], [test x$fpcheck = xtrue] )

# --------------------------------------------------------------------
# Enabling hdisvis and hdisdesign
# --------------------------------------------------------------------

AC_ARG_ENABLE( [hdisvis],
   [AS_HELP_STRING( [--enable-hdisvis],
                    [Enable configuring Hdisvis graphical tool] )],
   [case "${enableval}" in
    yes) package_hdisvis_enabled=true  ;;
     no) package_hdisvis_enabled=false ;;
      *) AC_MSG_ERROR( [bad value ${enableval}
                        for --enable-hdisvis]) ;;
    esac],
   [package_hdisvis_enabled=false] )
AM_CONDITIONAL( [HDISVISENABLED], [test x$package_hdisvis_enabled = xtrue] )

AC_ARG_ENABLE( [hdisdesign],
   [AS_HELP_STRING( [--enable-hdisdesign],
                    [Enable configuring HDIS design constructor tool] )],
   [case "${enableval}" in
    yes) package_hdisdesign_enabled=true  ;;
     no) package_hdisdesign_enabled=false ;;
      *) AC_MSG_ERROR( [bad value ${enableval}
                        for --enable-hdisdesign]) ;;
    esac],
   [package_hdisdesign_enabled=false] )
AM_CONDITIONAL( [HDISDESIGNENABLED], [test x$package_hdisdesign_enabled = xtrue] )

# ====================================================================
#              *********  WITH OPTIONS **********
# ====================================================================

# ====================================================================
#          *********  HEADERS AND PROGRAMS CHECK **********
# ====================================================================

# Checks for header files.
AC_HEADER_STDC

# Checking HDF5
AX_LIB_HDF5()
if test "$with_hdf5" = "yes"; then
   AC_MSG_NOTICE([Found HDF5])
else
   AC_MSG_ERROR([Unable to find HDF5.])
fi

# Checking Lua
AX_PROG_LUA([5.2])
AX_LUA_HEADERS
AX_LUA_LIBS

# setting an additional Makefile variable to know what lua version is
LUASHAREDIR=$lua_text_version
AC_SUBST(LUASHAREDIR)

# Checks for typedefs, structures, and compiler characteristics.
AC_TYPE_SIZE_T

# Checks for library functions.
AC_FUNC_MALLOC

# ====================================================================
#             *********  LIBRARIES CHECK **********
# ====================================================================

# Checks for libraries.
AC_CHECK_LIB([m], [cos])
AC_CHECK_LIB([dl], [dlopen])  # for lua -- we should not need those.
dnl AC_CHECK_LIB([hdf5], [H5Fcreate], [],
dnl              [echo "No HDF5 found. Check it at http://www.hdfgroup.org/HDF5";
dnl               exit 1] )

# ====================================================================
#              *********  EXTENTIONS CHECK **********
# ====================================================================

# check for mmx or sse support
AX_EXT

# ====================================================================
#      *********  DEFINING ADDITIONAL VARIABLES **********
# ====================================================================
AC_DEFINE([PROGRAM_BRANCH],["${BRANCH}"],[version branch name])

# ====================================================================
#      *********  CONFIGURING HDISVIS AND HDISDESIGN **********
# ====================================================================

if test "x${package_hdisvis_enabled}" = x'true'; then
  AC_CONFIG_SUBDIRS([hdisvis])
fi

if test "x${package_hdisdesign_enabled}" = x'true'; then
  AC_CONFIG_SUBDIRS([hdisdesign])
fi

# ====================================================================
#                *********  DIAGNOSTICS **********
# ====================================================================

AC_OUTPUT

AC_MSG_NOTICE( [ ] )
AC_MSG_NOTICE( [----------------------------------------------------- ] )
AC_MSG_NOTICE( [System:                              ${target_os_base}] )
AC_MSG_NOTICE( [Output gmon.out for profiling:       ${profile}  ] )
AC_MSG_NOTICE( [Float point correctness check:       ${fpcheck}  ] )
AC_MSG_NOTICE( [Lua version:                         ${LUA_VERSION}] )
AC_MSG_NOTICE( [HDF5 version:                        ${HDF5_VERSION}] )
AC_MSG_NOTICE( [Parallel execution mode:             ${parallel}] )
AC_MSG_NOTICE( [] )
AC_MSG_NOTICE( [HDISVIS configured:                  ${package_hdisvis_enabled}] )
AC_MSG_NOTICE( [HDISDESIGN configured:               ${package_hdisdesign_enabled}] )
AC_MSG_NOTICE( [----------------------------------------------------- ] )
AC_MSG_NOTICE( [ ] )

AC_MSG_NOTICE( [ HDF5 Configuration:] )
AC_MSG_NOTICE( [ HDF5_LIBS    = ${HDF5_LIBS}] )
AC_MSG_NOTICE( [ HDF5_LDFALGS = ${HDF5_LDFLAGS}] )
AC_MSG_NOTICE( [ HDF5_CFLAGS  = ${HDF5_CFLAGS}] )
AC_MSG_NOTICE( [ HDF5_CPPFLAGS= ${HDF5_CPPFLAGS}] )
AC_MSG_NOTICE( [ ] )

