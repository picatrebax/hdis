/*!

  \file luacback.c

  \brief Call back functions of all kind that are called from the core

  All functions defined here are basic call backs. Calling this
  functions up are assume that lua stack top is set to 0.

*/

#include "luacback.h"

/* ----------------------------------------------------------------- */

/*!

  \brief Is called BEFORE each time step

  */
int luacback_pretimestep( lua_State* L,
                          const lptimer_t* restrict pt )
{
  int errcode = LUACBACK_OK;

  lua_getglobal( L, "hdis" );
  lua_getfield( L, -1, "callback" );
  lua_getfield( L, -1, "pretimestep" );

  if ( lua_isfunction( L, -1 ) )
    {
      lua_pushnumber( L, pt->t );
      lua_pushnumber( L, pt->dt );
      lua_pushinteger( L, pt->n );
      if ( lua_pcall( L, 3, 0, 0 ) != 0 )
        {
          errcode = LUACBACK_ERROR_PCALL;
        }
    }
  else
    {
      errcode = LUACBACK_UNDEFINED;
    }

  /* clear the stack only if there are no error messages in it */
  if ( errcode != LUACBACK_ERROR_PCALL )
    lua_settop( L, 0 );

  return errcode;
}

/* ----------------------------------------------------------------- */
/*!

  \brief Is called AFTER each time step is finished. It may stop the
  program or make an extra save.
  
*/
int luacback_aftertimestep( lua_State* L,
                            const lptimer_t* restrict pt,
                            char* restrict filename,
                            unsigned int* fsave,
                            unsigned int* fhalt )
{
  int errcode = LUACBACK_OK;

  lua_getglobal( L, "hdis" );
  lua_getfield( L, -1, "callback" );
  lua_getfield( L, -1, "aftertimestep" );

  if ( lua_isfunction( L, -1 ) )
    {
      lua_pushnumber( L, pt->t );
      lua_pushnumber( L, pt->dt );
      lua_pushinteger( L, pt->n );
      if ( lua_pcall( L, 3, 3, 0 ) != 0 )
        {
          errcode = LUACBACK_ERROR_PCALL;
        }
      else
        {
          /* return the filename */
          *fsave = lua_toboolean( L, -3 );
          *fhalt = lua_toboolean( L, -2 );
          if ( *fsave )
            {
              strncpy( filename, lua_tostring( L, -1 ), FILENAME_MAXLEN );
            }
        }
    }
  else errcode = LUACBACK_UNDEFINED;

  /* clear the stack only if there are no error messages in it */
  if ( errcode != LUACBACK_ERROR_PCALL )
    lua_settop( L, 0 );

  return errcode;
}

/* ----------------------------------------------------------------- */

/*!

  \brief Is called before every save. It defines the HDF5 file name to
  save (except those that are triggered by aftertimestep callback).

  */
int luacback_presave( lua_State* L,
                      char* restrict filename,
                      const lptimer_t* restrict pt )
{
  int errcode = LUACBACK_OK;

  lua_getglobal( L, "hdis" );
  lua_getfield( L, -1, "callback" );
  lua_getfield( L, -1, "presave" );

  if ( lua_isfunction( L, -1 ) )
    {
      lua_pushnumber( L, pt->t );
      lua_pushinteger( L, pt->n );
      if ( lua_pcall( L, 2, 1, 0 ) != 0 )
        {
          errcode = LUACBACK_ERROR_PCALL;
        }
      else
        {
          /* return the filename */
          strncpy( filename, lua_tostring( L, -1 ), FILENAME_MAXLEN );
        }
    }
  else errcode = LUACBACK_UNDEFINED;

  /* default file name */
  if ( errcode == LUACBACK_UNDEFINED ||
       errcode == LUACBACK_ERROR_PCALL )
    {
      /* current milliseconds */
      int ms = (int) ( ( pt->t + 0.00001 ) * 1000.0 );

      /* we define the default file name: c-7digits(ms).h5 */
      snprintf( filename, 12, "c%07d.h5", ms );
    }

  /* clear the stack only if there are no error messages in it */
  if ( errcode != LUACBACK_ERROR_PCALL )
    lua_settop( L, 0 );

  return errcode;
}

/* ----------------------------------------------------------------- */

/*!

  \brief Function is called after every save to report some
  potentially useful information.

 */
int luacback_aftersave( lua_State* L,
                        char* restrict filename,
                        const lptimer_t* restrict pt,
                        const conhash_comp_t* pch_comp )
{
  int errcode = LUACBACK_OK;

  lua_getglobal( L, "hdis" );
  lua_getfield( L, -1, "callback" );
  lua_getfield( L, -1, "aftersave" );
  if ( lua_isfunction( L, -1 ) )
    {
      lua_pushnumber( L, pt->t );
      lua_pushnumber( L, pt->dt );
      lua_pushinteger( L, pt->n );
      lua_pushstring( L, filename );
      lua_pushnumber( L, pch_comp->last_ulm );
      lua_pushnumber( L, pch_comp->ncleans );
      if ( lua_pcall( L, 6, 0, 0 ) != 0 )
        {
          errcode = LUACBACK_ERROR_PCALL;
        }
    }
  else errcode = LUACBACK_UNDEFINED;

  /* clear the stack only if there are no error messages in it */
  if ( errcode != LUACBACK_ERROR_PCALL )
    lua_settop( L, 0 );

  return errcode;
}

/* ----------------------------------------------------------------- */

/*!

  \brief Is called after each time step to update g

  */
int luacback_g( lua_State* L,
                const lptimer_t* restrict pt,
                vec3d_t* restrict pg )
{
  int errcode = LUACBACK_OK;

  lua_getglobal( L, "hdis" );
  lua_getfield( L, -1, "callback" );
  lua_getfield( L, -1, "g" );

  if ( lua_isfunction( L, -1 ) )
    {
      lua_pushnumber( L, vec3d_getx(*pg) );
      lua_pushnumber( L, vec3d_gety(*pg) );
      lua_pushnumber( L, vec3d_getz(*pg) );
      lua_pushnumber( L, pt->t );
      lua_pushnumber( L, pt->dt );
      if ( lua_pcall( L, 5, 3, 0 ) != 0 )
        {
          errcode = LUACBACK_ERROR_PCALL;
        }

      vec3d_set( *pg,
                 lua_tonumber( L, -3 ),
                 lua_tonumber( L, -2 ),
                 lua_tonumber( L, -1 ) );
     }
  else errcode = LUACBACK_UNDEFINED;

  /* clear the stack only if there are no error messages in it */
  if ( errcode != LUACBACK_ERROR_PCALL )
    lua_settop( L, 0 );

  return errcode;
}

/* ----------------------------------------------------------------- */
/*!

  \brief Is called after each contact detection run

  */
int luacback_contact_detection( lua_State* L,
                                const lptimer_t* restrict pt,
                                int* restrict pf_cdrun )
{
  int errcode = LUACBACK_OK;

  lua_getglobal( L, "hdis" );
  lua_getfield( L, -1, "callback" );
  lua_getfield( L, -1, "contact_detection" );

  if ( lua_isfunction( L, -1 ) )
    {
      lua_pushnumber( L, pt->t );
      lua_pushinteger( L, pt->n );
      lua_pushinteger( L, *pf_cdrun );

      if ( lua_pcall( L, 3, 0, 0 ) != 0 )
        {
          errcode = LUACBACK_ERROR_PCALL;
        }
    }
  else errcode = LUACBACK_UNDEFINED;

  /* clear the stack only if there are no error messages in it */
  if ( errcode != LUACBACK_ERROR_PCALL )
    lua_settop( L, 0 );

  return errcode;
}

/* ----------------------------------------------------------------- */

int luacback_diagnostics( lua_State* L,
                          const lptimer_t* restrict pt,
                          diagnostics_t* restrict pdiag )
{
  int errcode = LUACBACK_OK;

  lua_getglobal( L, "hdis" );
  lua_getfield( L, -1, "callback" );
  lua_getfield( L, -1, "diagnostics" );

  if ( lua_isfunction( L, -1 ) )
    {
      lua_pushnumber( L, pt->t );
      lua_pushinteger( L, pt->n );

      lua_pushinteger( L, pdiag->contacts_actual_n );
      lua_pushinteger( L, pdiag->contacts_potential_n );
      lua_pushinteger( L, pdiag->contacts_actual_bb_n );
      lua_pushinteger( L, pdiag->contacts_potential_bb_n );
      lua_pushinteger( L, pdiag->contacts_features_bb_n );
      lua_pushinteger( L, pdiag->contacts_inits_bb_n );
      lua_pushinteger( L, pdiag->contacts_distance_bb_n );

      if ( lua_pcall( L, 9, 0, 0 ) != 0 )
        {
          errcode = LUACBACK_ERROR_PCALL;
        }
    }
  else errcode = LUACBACK_UNDEFINED;

  /* clear the stack only if there are no error messages in it */
  if ( errcode != LUACBACK_ERROR_PCALL )
    lua_settop( L, 0 );

  return errcode;
}

/* ----------------------------------------------------------------- */

int luacback_profile( lua_State* L,
                      const lptimer_t* restrict pt,
                      profile_t* restrict pprof )
{
  int errcode = LUACBACK_OK;

  lua_getglobal( L, "hdis" );
  lua_getfield( L, -1, "callback" );
  lua_getfield( L, -1, "profile" );

  if ( lua_isfunction( L, -1 ) )
    {
      lua_pushnumber( L, pt->t );
      lua_pushnumber( L, pt->dt );
      lua_pushinteger( L, pt->n );

      lua_pushnumber( L, pprof->total );
      lua_pushnumber( L, pprof->callbacks );
      lua_pushnumber( L, pprof->saves );

      lua_pushnumber( L, pprof->dists_bb );
      lua_pushnumber( L, pprof->forces_bb );
      lua_pushnumber( L, pprof->condets );

      lua_pushnumber( L, pprof->forces_bc );
      lua_pushnumber( L, pprof->dynamics );
      lua_pushnumber( L, pprof->placements_norms );

      lua_pushnumber( L, pprof->placements_bodies );
      lua_pushnumber( L, pprof->placements_verts );
      lua_pushnumber( L, pprof->placements_sbb );

      lua_pushnumber( L, pprof->summon_check );
      lua_pushnumber( L, pprof->gravitation );
      lua_pushnumber( L, pprof->energy );

      if ( lua_pcall( L, 18, 0, 0 ) != 0 )
        {
          errcode = LUACBACK_ERROR_PCALL;
        }
    }
  else errcode = LUACBACK_UNDEFINED;

  /* clear the stack only if there are no error messages in it */
  if ( errcode != LUACBACK_ERROR_PCALL )
    lua_settop( L, 0 );

  return errcode;
}

/* ----------------------------------------------------------------- */

int luacback_energy( lua_State* L,
                     const lptimer_t* restrict pt,
                     const real etmax,
                     const real ermax,
                     const real Et, 
                     const real Er, 
                     const real Ep )
{
  int errcode = LUACBACK_OK;

  lua_getglobal( L, "hdis" );
  lua_getfield( L, -1, "callback" );
  lua_getfield( L, -1, "energy" );

  if ( lua_isfunction( L, -1 ) )
    {
      lua_pushnumber( L, pt->t );
      lua_pushnumber( L, pt->dt );
      lua_pushnumber( L, etmax );
      lua_pushnumber( L, ermax );
      lua_pushnumber( L, Et );
      lua_pushnumber( L, Er );
      lua_pushnumber( L, Ep );
      if ( lua_pcall( L, 7, 0, 0 ) != 0 )
        {
          errcode = LUACBACK_ERROR_PCALL;
        }
     }
  else errcode = LUACBACK_UNDEFINED;

  /* clear the stack only if there are no error messages in it */
  if ( errcode != LUACBACK_ERROR_PCALL )
    lua_settop( L, 0 );

  return errcode;
}

/* ----------------------------------------------------------------- */

/*! \brief Call back function to CHANGE the time step */
int luacback_dt( lua_State* L, lptimer_t* pt, const real vc_sq_max )
{
  int errcode = LUACBACK_OK;

  lua_getglobal( L, "hdis" );
  lua_getfield( L, -1, "callback" );
  lua_getfield( L, -1, "dt" );

  if ( lua_isfunction( L, -1 ) )
    {
      lua_pushnumber( L, pt->t );
      lua_pushnumber( L, pt->dt );
      lua_pushnumber( L, pt->n );
      lua_pushnumber( L, vc_sq_max );
      if ( lua_pcall( L, 4, 1, 0 ) != 0 )
        {
          errcode = LUACBACK_ERROR_PCALL;
        }
      else
        {
          pt->dt = lua_tonumber( L, -1 );
        }
     }
  else errcode = LUACBACK_UNDEFINED;

  /* clear the stack only if there are no error messages in it */
  if ( errcode != LUACBACK_ERROR_PCALL )
    lua_settop( L, 0 );

  return errcode;
}

/* ----------------------------------------------------------------- */

/*!

  \brief Is called after the time loop right before freeing all the
  variables and exiting the model core.

  */
int luacback_finalize( lua_State* L,
                       const lptimer_t* restrict pt,
                       char* restrict filename,
                       bool* fsave )

{
  int errcode = LUACBACK_OK;

  lua_getglobal( L, "hdis" );
  lua_getfield( L, -1, "callback" );
  lua_getfield( L, -1, "finalize" );

  if ( lua_isfunction( L, -1 ) )
    {
      lua_pushnumber( L, pt->t );
      if ( lua_pcall( L, 1, 1, 0 ) != 0 )
        {
          errcode = LUACBACK_ERROR_PCALL;
        }
      else
        {
          /* if returns the filename => save */
          if ( lua_isstring( L, -1 ) )
            {
              *fsave = true;
              strncpy( filename, lua_tostring( L, -1 ), FILENAME_MAXLEN );
            }
        }
    }
  else
    {
      errcode = LUACBACK_UNDEFINED;
    }

  /* clear the stack only if there are no error messages in it */
  if ( errcode != LUACBACK_ERROR_PCALL )
    lua_settop( L, 0 );

  return errcode;
}

/* ----------------------------------------------------------------- */
