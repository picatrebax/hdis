#ifndef LPIO_H
#define LPIO_H
#include <stdlib.h>
#include <string.h>
#include <hdf5.h>

#include "defs.h"
#include "vec3d.h"
#include "rquat.h"
#include "box.h"
#include "bb.h"
#include "atom.h"
#include "body.h"
#include "spring.h"
#include "ptriangle.h"
#include "lptimer.h"
#include "conhash.h"
#include "conmech.h"
#include "bc.h"
#include "sph.h"
#include "portal.h"

#define LPIO_ACCESS_F_OVERWRITE H5F_ACC_TRUNC
#define LPIO_ACCESS_F_EXCLUSIVE H5F_ACC_EXCL
#define LPIO_ACCESS_F_READONLY  H5F_ACC_RDONLY
#define LPIO_ACCESS_F_RDWR      H5F_ACC_RDWR
#define LPIO_STDSTR_UNITS_SBB \
  "c [m] | c0 [m] | r [m] | body_indx [1] | atom_type [1] | group_id [1]"
#define LPIO_STDSTR_DESC_SBB \
  "c - center of sphere | c0 - center of sphere (tech.) | r - radius" \
  " | body_indx - index of the body" \
  " the accroding atom belongs to | atom_type - the type of the atom" \
  " SBB was built from | group_id - group ID of atom"

#define LPIO_STDSTR_UNITS_ATOM \
  "R [m] | ri [m] | body_indx [1] | group_id [1] | material_id [1]" \
" | ppindx [1] | type [1]"
#define LPIO_STDSTR_DESC_ATOM \
  "R - dilation radius | " \
  "ri - interaction radius (used for forces computations) | " \
  "body_indx - index of the body it belongs to | " \
  "group_id - ID of the classification group it belongs to " \
  "material_id - ID of the material it is built from " \
  "ppindx - indices (array) of points it consists from | " \
  "type - 1=sphere, 2=capsule, 4=tripsule, etc."

#define LPIO_STDSTR_UNITS_ADJ_E \
	"just int, man"
#define LPIO_STDSTR_DESC_ADJ_E \
	"index into Vlst"

#define LPIO_STDSTR_UNITS_VERT \
	"some vert stuff"
#define LPIO_STDSTR_DESC_VERT \
	"more vert stuff"

#define LPIO_STDSTR_UNITS_EDGE \
	"some edge stuff"
#define LPIO_STDSTR_DESC_EDGE \
	"more edge stuff"

#define LPIO_STDSTR_UNITS_FACE \
	"some face stuff"
#define LPIO_STDSTR_DESC_FACE \
	"more face stuff"

#define LPIO_STDSTR_UNITS_BODY \
  "q [?] | c [m] | v [m/s] | w [1/s] | f [N] | t [m N] " \
  " | I [kg m^2] | m [kg] | V [m^3] | ifunc [-] | imonitor [-] | effunc [-] | posfun [-]"
#define LPIO_STDSTR_DESC_BODY \
  "q - orientation quaternion | " \
  "c - center of mass position | " \
  "v - center of mass velocity | " \
  "w - angular velocity (at c) | " \
  "f - net force acting to the body | " \
  "t - net torque | " \
  "I - principle moments of inertia | " \
  "m - mass | " \
  "V - volume | " \
  "ifunc - index of control function |" \
  "imonitor - index of monitoring function |" \
  "effunc - index of external forces/torque function |" \
  "posfunc - index of positioning correction function |" \
  "flag - additional states of the body"

#define LPIO_STDSTR_UNITS_SPRING				\
  "rb1 [m] | rb2 [m] | x0 [m] | k [N/m] | ib1 [1] | ib2 [1] | group [1] "
#define LPIO_STDSTR_DESC_SPRING \
  "rb1, rb2 - The point coordinate on body (1 or 2) in body local" \
  " coordinates (body frame) where the spring is connected to the" \
  " body. | "							   \
  "x0 - neutral distance when force is 0 | "			   \
  "k  - stiffness of the spring | "				   \
  "eta  - damping coef. of the spring | "			   \
  "ib1, ib2  - indexes of body 1 and 2 | "                         \
  "group - spring group ID it belongs to."

#define LPIO_STDSTR_UNITS_PTRIANGLE			  \
  "ib1 [1] | ib2 [1] | ib3 [1] | p [N] | group [1]"
#define LPIO_STDSTR_DESC_PTRIANGLE \
  "ib1, ib2, ib3  - indexes of body 1, 2, 3 which centers of mass " \
  "define the virtual pressure triangle | "                        \
  "p - pressure applied to the trianlge | "  \
  "group - spring group ID it belongs to."

#define LPIO_STDSTR_UNITS_SPHERE \
  "id [1] | c [m] | r [m] | d [m] | q [1] | m [kg]" \
  " | v [m/s] | w [1] | f [N] | t [mN]"
#define LPIO_STDSTR_DESC_SPHERE \
  "id - particle uniue number | c - center coordinates" \
  "| r - radius | d - dilation radius" \
  " | q - rotation quaternion | m - mass" \
  " | v - center mass velocity | w - angular velocity" \
  " | f - net force | t - net torque"

#define LPIO_STDSTR_UNITS_BOX "x [m] | y [m] | z [m]"
#define LPIO_STDSTR_DESC_BOX "x,y,z - computation domain size"

#define LPIO_STDSTR_UNITS_LPTIMER "all real values are in seconds"
#define LPIO_STDSTR_DESC_LPTIMER \
  "t, current time | t0, initial time |" \
  " te, end time | dt, time step |" \
  " ts, saving time | dts, save time step |" \
  " n, number of steps | ns, number of savings"

#define LPIO_STDSTR_UNITS_CONINFO \
  "id1 [1] | id2 [1] | ftau_e [N] | ftau_i [N] | pc [m] | f [N] | flag [1]"
#define LPIO_STDSTR_DESC_CONINFO \
  "id1 - ID of first contact object or particle" \
  " | id2 - ID of second contact object or particle" \
  " | ftau_e - elastic/friction part of tangent force" \
  " | ftau_i - dapming part of tangent force" \
  " | pc - point of contact" \
  " | f - total force on the contact" \
  " | nlast - step when the contact was updated" \
  " | flag - indicator of occupancy of hash cell"

#define LPIO_STDSTR_UNITS_CONTACT \
  "id1 [1] | id2 [1] | ftau_e [N] | ftau_i [N] | pc [m] | delta [m] | flag [1]"
#define LPIO_STDSTR_DESC_CONTACT \
  "id1 - ID of first contact object or particle" \
  " | id2 - ID of second contact object or particle" \
  " | ftau_e - elastic/friction part of tangent force" \
  " | ftau_i - dapming part of tangent force" \
  " | pc - coordinates of the contact point" \
  " | delta - ovelap between atoms" \
  " | f - total force on the contact (acting from id1 on id2)"

#define LPIO_STDSTR_UNITS_MATERIAL \
  "rho [kg/m^3] | G [Pa] | nu [1] | gamma [J/m^2]" \
  " | CR0 [1] | vR0 [m/s] | alpha_t [1] | mu [1] "
#define LPIO_STDSTR_DESC_MATERIAL \
  "rho - Density" \
  " | G - Shear modulus" \
  " | nu - Poisson coefficient" \
  " | gamma - Surface energy" \
  " | CR0 - Coefficient of restitution for velocity vR0" \
  " | vR0 - Velocity for which CR0 was measured" \
  " | alpha_t - Tangent elastic reduction" \
  " | mu - Friction coefficient"

#define LPIO_STDSTR_UNITS_CMPROPS \
  "H [Pa] | gamma [J/m^2] | ka [?TODO] | mu [1] | alpha_t [1]" \
  " | CR [1] | vR [m/s] | Skni [?TODO]" \
  " | alpha_t [1] | mati1 [1] | mati2 [2] "
#define LPIO_STDSTR_DESC_CMPROPS \
  "H - Hertz modulus" \
  " | gamma - Surface energy" \
  " | ka - Adhesion coefficient" \
  " | mu - Friction coefficient" \
  " | alpha_t - Tangent elastic reduction" \
  " | CR - Coefficient of restitution for velocity vR" \
  " | vR - Velocity for which CR was measured" \
  " | Skni - Precalculated part of Damping coefficient" \
          " (generally depends on CR and vR)" \
  " | mati1 - Index of the first interacting material" \
  " | mati2 - Index of the second interacting material"

#define LPIO_STDSTR_UNITS_PORTAL \
  "v [m/s] | w [1/s] | entrance_y [m] | exit_y [m] | exit_z [m] | flag [1]"
#define LPIO_STDSTR_DESC_PORTAL \
  "v - initial exit linear velocity"                                    \
  " | w - initial angular velocity"                                     \
  " | entrance_y - rightmost boundary teleporting bodies may reach with their SBB" \
  " | exit_y - where the body center of mass appears after teleporting" \
  " | exit_z - where the body center of mass appears after teleporting" \
  " | flag - 1 if teleporting defined, 0 - otherwise"

#define LPIO_STDSTR_UNITS_SPH_PROPS \
  "rho0 [kg/m^3] | c0 [m/s] | c2 [m^2/s^2]=c0^2 | gamma [1] | eps [1] | alpha [1]"
#define LPIO_STDSTR_DESC_SPH_PROPS \
  "rho0 - target density used in state equation" \
  " | c0 - sound velocity for target density" \
  " | c2 - square of sound velocity (auto computed)" \
  " | gamma - adiabatic index in state equation" \
  " | eps - reserved for velocity correction in placement procedure" \
  " | alpha - artificial viscosity coefficient"

#define LPIO_DATASET_MISSING ((lpindex_t)(-1))


typedef hid_t lpio_dtype_t;

#define lpio_dtype_freg( dtype, structure, field, ftype )      \
  {                                                            \
    H5Tinsert( dtype,                                          \
               #field,                                         \
               HOFFSET( structure, field ),                    \
               ftype );                                        \
  }

typedef struct lpio_stdtypes_s
{
  hid_t dtype_int;
  hid_t dtype_uint16;           /*!< uint16_t representation */
  hid_t dtype_ushort;           /*!< unsigned short representation */
  hid_t dtype_real;
  hid_t dtype_vec3d;
  hid_t dtype_rquat;
  hid_t dtype_lptimer;
  hid_t dtype_sbb;
  hid_t dtype_point;
  hid_t dtype_adj_e;
  hid_t dtype_vert;
  hid_t dtype_edge;
  hid_t dtype_face;
  hid_t dtype_atom;
  hid_t dtype_body;
  hid_t dtype_spring;
  hid_t dtype_ptriangle;
  hid_t dtype_dot;
  hid_t dtype_box;
  hid_t dtype_coninfo;          /* contact hash cell */
  hid_t dtype_contact;          /* contact structure */
  hid_t dtype_cmprops;          /* conmech, contact mech. properties */
  hid_t dtype_sphprops;       /* SHP hydro material properties */
  hid_t dtype_material;         /* conmech, material properties */
  hid_t dtype_bc;               /* boundary condition struct */
  hid_t dtype_portal;
} lpio_stdtypes_t;

typedef struct lpio_s
{
  hid_t fileid;
  unsigned int flags;
  char* filename;
} lpio_t;


#define lpio_dtype_create( dtype, structure )                      \
  {                                                                \
    dtype = H5Tcreate ( H5T_COMPOUND, sizeof (structure) );        \
  }

void lpio_dtype_destroy( lpio_dtype_t dtype );

lpio_stdtypes_t * lpio_stdtypes_alloc();

void lpio_stdtypes_free( lpio_stdtypes_t* ptp );

lpio_t * lpio_alloc( char* filename, unsigned int access );

void lpio_free( lpio_t* pio );


void lpio_save_attribute( hid_t dataset, const char* aname, const char* s );

int lpio_save_value( lpio_t* pio,           /* I/O struct */
                     lpio_dtype_t dtype,    /* data type */
                     const char* dataname,  /* name for dataset */
                     void* data,            /* pointer to the data */
                     const char* units,     /* string with units */
                     const char* description /* description string */
                     );

int lpio_save_array( lpio_t* pio,           /* I/O struct */
                     lpio_dtype_t dtype,    /* data type */
                     const char* dataname,  /* name for dataset */
                     void* data,            /* array */
                     int len,               /* length of array */
                     const char* units,     /* string with units */
                     const char* description /* description string */
                     );
void lpio_save_global_string_attribute( lpio_t* pio,
                                        const char* aname,
                                        const char* avalue )
                                            ;
int lpio_save_astrings ( lpio_t *pio,
                         char** pS,              /* array of strings */
                         lpindex_t N,            /* length of the array */
                         const char* dataname,   /* name of the array */
                         const char* description /* description string */
                       )
                               ;

size_t lpio_get_dim( lpio_t * pio, const char * name );

int lpio_read( lpio_t * pio, const char* name, void* buff );

int lpio_read_astrings ( lpio_t *pio,
                         char** pS,              /* array of strings */
                         lpindex_t N,            /* length of the array */
                         const char* dataname    /* name of the array */
                       )
                               ;
#define lpio_is_lpiofile( filename ) H5Fis_hdf5( filename )

#endif
