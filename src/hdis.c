/*!

  \file hdis.c

  \brief Main file with function main and all "global" variables
  descriptions.

*/

#include "defs.h"
#include "hdis-types.h"
#include "hdis.h"

#include "box.h"
#include "conhash.h"
#include "atom.h"
#include "body.h"
#include "sph.h"
#include "spring.h"
#include "ptriangle.h"
#include "conmech.h"
#include "lptimer.h"
#include "bc.h"
#include "ini.h"
#include "forces.h"
#include "luacback.h"
#include "energy.h"
#include "timerec.h"
#include "sphdem.h"
#include "profile.h"

int main( int argc, char* argv[] )
{

  /**** Model Variables *****/

  lua_State *L;              /* Lua state associated with Lua
                                configuration and processing script */
  L = luaL_newstate();          /* initializing it */
  luaL_openlibs( L );

  box_t box;                  /* main box */

  lpindex_t nBs;              /* number of bodies */
  lpindex_t nAs;              /* number of atoms */
  lpindex_t nVs;              /* number of verts */
  lpindex_t nSs;              /* number of springs */
  lpindex_t nTs;              /* number of pressure triangles */
  lpindex_t nHs;              /* number of sph particles */

  atom_t* pAlst = NULL;    /* main list with atoms */
  vert_t* pVlst = NULL;    /* main list with verts */
  body_t*    pBlst = NULL; /* main list with bodies */
  spring_t*  pSlst = NULL; /* main list with springs */
  ptriangle_t* pTlst = NULL;   /* main list with pressure triangles */
  sph_t* pHlst = NULL;         /* main list of SPH particles */

  /* group names (actually groups) */
  char **pA_gnames = NULL;
  lpindex_t nA_gnames = 0;
  int* pA_intrx = NULL;         /* group interactions */
  int nA_intrx = 0;

  /* materials and material names */
  material_t *pA_mats = NULL;
  char **pA_matnames  = NULL;
  lpindex_t nA_mats   = 0;

  /* mechanical properties table */
  cmprops_t* pcmprops = NULL;
  lpindex_t ncmprops  = 0;

  /* SPH hydrodynamics properties */
  sph_props_t sphprops;

  /* gravity or any mass global acceleration */
  vec3d_t g;

  /* maximum contact velocity in the system: used mostly in automatic
     time step computations */
  real vc_sq_max = 0.0;

  /* total system energy (used for monitoring only) */
  real Et = 0;                  /* transitional energy */
  real Er = 0;                  /* rotational energy */
  real Ep = 0;                  /* potential energy (gravitation) */

  /* max values of energy density */
  real etmax = 0;
  real ermax = 0;

  /* global drag forces (defined in hdis-type.h) */
  drag_t drag;
  drag.isdrag = 0;
  drag.isuniform = 0;

  lptimer_t t;                  /* time management */

  conhash_t conhash;            /* hash table for contacts */
  conhash_comp_t conhash_comp;

  conhash_comp.ULMT =  CONHASH_ULMT;
  conhash_comp.nprobes = CONHASH_NPROBES;
  conhash_comp.ncleans = 0;
  conhash_comp.last_ulm = 0;
  conhash_comp.bsave = 0;  /* do not save hash table by default */

  bc_t bc;                      /* boundary conditions */

  settings_t settings;            /* setting structure */
  settings.flags = 0;

  portal_t portal = {0}; /* all to zero first, no portal exists */

  lpdyna_t collisions;   /* collision markers, dynamic array */
  lpindex_t *pxprox, *pyprox, *pzprox;
  lpindex_t nxprox, nyprox, nzprox;
  sbb_t * sbbs;
  sbb_params_t sbb_params;      /* setting to build SBBs */

  coldet_naive_t cdnaive;         /* naive collision detection */
  coldet_spx_t cdspx;             /* SPX method */
  coldet_spsp_t cdspsp;           /* SPSP method */

  int f_cdrun = 1;    /* contact detection run-flag */

 /* flag: set if a callback function defined in Lua file and need to
    run. Initially we suppose all functions have to run, so, ~0. */
  unsigned int luacback_defined_flags = ~0;

  char* outfile;
  outfile = calloc( 1 + FILENAME_MAXLEN, sizeof( char ) );
  assert( outfile );
  diagnostics_t diagnostics;

  /* profiling structures */
  timerec_t timerec, timerec_all_timeloops;
  profile_t profile = {0};      /* all fields to 0 */

  /* block reading options */
  {
    int c;

    while ( (c = getopt(argc, argv, "hV")) != -1)
      {
        switch (c)
          {
          case 'h':
            usage( stdout );
            exit( 0 );
          case 'V':
            info( stdout, 1 );
            exit( 0 );
          case '?':
            if ( optopt == 'i' )
              fprintf( stderr,
                       "Error: option -%c requires an argument.\n",
                       optopt );
            else
              fprintf( stderr,
                       "Error: Unknown option `-%c'.\n", optopt );
            exit( 1 );
          default:
            exit( 2 );
          }
      }
  }

  /* Program banner display */
  info( stdout, 0 );

  /* scenario file */
  if ( argc <= 1 ) settings.luafile = HDIS_DEFAULT_LUA;
  else settings.luafile = argv[1];

  /* initilization from the scenario file */
  ini( &settings,
       &t,
       &pVlst,
       &pAlst,
       &pBlst,
       &pSlst,
       &pTlst,
       &pHlst,
       &pA_gnames,
       &pA_intrx,
       &pA_mats,
       &pA_matnames,
       &pcmprops,
       &sbb_params,
       &nAs,
       &nVs,
       &nBs,
       &nSs,
       &nTs,
       &nHs,
       &nA_gnames,
       &nA_intrx,
       &nA_mats,
       &ncmprops,
       &conhash,
       &conhash_comp,
       &box,
       &g,
       &drag,
       &bc,
       &sphprops,
       &portal,
       settings.luafile,
       argc,
       argv,
       L );

  lpindex_t nsbbs = nAs + nHs;
  sbbs = malloc( nsbbs * sizeof( sbb_t ) );
  assert( sbbs );

  /* we need to assign all paramters to SBBs */
  sbb_build( sbbs, pAlst, nAs, pHlst, nHs );
  placement_sbbs( sbbs, pAlst, pBlst, nAs, pHlst, nHs );

  lpdyna_init( &collisions, sizeof( collision_t ), 6*nsbbs, nsbbs );

  nxprox = 0;
  nyprox = 0;
  nzprox = 0;

  pxprox = malloc( nsbbs* sizeof( lpindex_t ) );
  pyprox = malloc( nsbbs* sizeof( lpindex_t ) );
  pzprox = malloc( nsbbs* sizeof( lpindex_t ) );

  forces_init( nBs );  /* and forces module initialization */

  lpindex_t nVHs = 0;           /* number of virtual hydro parts */
  real rmax = 0;                /* total rmax for spsp method */
  real sph_rmax = 0;            /* radius max for SPH particles periodic
                                   boundary */

  /* special structure for periodic boundary condition */
  struct {
    real y0;                    /* left boundary */
    real y1;                    /* right boundary */
    real L;                     /* y1 - y0 */
  } periodic;
  periodic.y0 = 0;
  periodic.y1 = box.fcorner.y;

  /* Set maximum number of virtual particles */
  lpindex_t nVHs_max = bc.flags & BC_F_PERIODIC ? nHs : 0;
  lpindex_t nsbbs_max = nsbbs + nVHs_max;
  lpindex_t nHs_max = nHs + nVHs_max;

  /* parallel interface for SPH module */
  sph_omp_t sph_omp;
  sph_omp_init( &sph_omp, nHs+nVHs_max );

  /* Calculate rmax (for both spsp and periodic boundaries, if needed) */
  for ( lpindex_t k = 0; k < nAs; ++k )
  {
      if ( rmax < pAlst[k].sbb_Rmin )
        rmax = pAlst[k].sbb_Rmin;
  }
  for ( lpindex_t k = nAs; k < nsbbs; ++k )
  {
    if ( rmax < sbbs[k].rmin ) rmax = sbbs[k].rmin;
    if ( sph_rmax < sbbs[k].rmin ) sph_rmax = sbbs[k].rmin;
  }
  rmax += sbb_params.drmax;
  sph_rmax += sbb_params.drmax;

  if ( bc.flags & BC_F_PERIODIC )
    {
      /* Set storage for virtual particles */
      if ( pHlst )
        {
          pHlst = realloc( pHlst, nHs_max * sizeof( sph_t ) );
          sbbs = realloc( sbbs, nsbbs_max * sizeof( sbb_t ) );

          /* Calculate reduced length of box along y */
          periodic.y0 = 2 * sph_rmax;
          periodic.y1 = box.fcorner.y - periodic.y0;
        }
    }
  periodic.L = periodic.y1 - periodic.y0;

  switch ( settings.cdmethod )
    {
    case SETTINGS_CDM_NAIVE:
      coldet_naive_init( &cdnaive, &box, pA_intrx, nA_gnames );
      break;
    case SETTINGS_CDM_SPX:
      coldet_spx_init( &cdspx, nsbbs_max, &box, pA_intrx, nA_gnames );
      break;
    case SETTINGS_CDM_SPSP:
      coldet_spsp_init( &cdspsp, rmax, &box, nsbbs_max, pA_intrx,
                        nA_gnames );
      break;
    default:
      fatal( 1, "Collision detection method is not properly set" );
    }

  if ( settings.flags & SETTINGS_F_SAVE_START )
    {
      int errcode = luacback_presave( L, outfile, &t );
      if ( errcode == LUACBACK_ERROR_PCALL )
        fatal( 1, "Function luacback_presave failed to run\n\n"
               "Lua error message:\n\n%s\n", lua_tostring(L,-1) );

      highlio_save( outfile,
                    &settings, &t, sbbs, pAlst,
                    pVlst, pBlst, pSlst, pTlst, pHlst,
                    nsbbs, nAs, nVs, nBs, nSs, nTs, nHs,
                    &conhash, &conhash_comp,
                    &bc,
                    &box, &g, &drag,
                    pA_gnames, nA_gnames,
                    pA_intrx, nA_intrx,
                    pA_mats, pA_matnames, nA_mats,
                    pcmprops, ncmprops, &sbb_params, &sphprops, &portal );

      luacback_aftersave( L, outfile, &t, &conhash_comp );
    }

  /* =============================================================== */
  /*                           MAIN LOOP                             */
  /* =============================================================== */
  while ( t.t < t.te ) /* dt loop */
    {
      /* ----------------------------------------------------------- */
      /*                 Before timestep CALLBACKS                   */
      /* ----------------------------------------------------------- */

      timerec_init( &timerec_all_timeloops );
      timerec_init( &timerec );

      LUACBACK_RUN( luacback_pretimestep( L, &t ),
                    luacback_defined_flags,
                    LUACBACK_F_DEFINED_PRETIMESTEP );

      profile.callbacks += timerec_lapsed( &timerec );

      /* ----------------------------------------------------------- */
      /*                      Step forward                           */
      /* ----------------------------------------------------------- */

      diagnostics.contacts_actual_n       = 0;
      diagnostics.contacts_potential_n    = 0;
      diagnostics.contacts_actual_bb_n    = 0;
      diagnostics.contacts_potential_bb_n = 0;
      diagnostics.contacts_features_bb_n  = 0;
      diagnostics.contacts_inits_bb_n     = 0;
      diagnostics.contacts_distance_bb_n  = 0;

      timerec_init( &timerec );
      f_cdrun = coldet_summon_check( sbbs, pAlst, nAs, nHs );
      profile.summon_check += timerec_lapsed( &timerec );

      if ( f_cdrun || t.n == 0 )
        {
          timerec_init( &timerec );
          profile.ncondet += 1;
          profile.rcondet = (double) (t.n ) / (double) profile.ncondet;

          sbb_reset( sbbs, pAlst, pBlst, nAs, pHlst, nHs, &sbb_params );

          coldet_bc( sbbs, nsbbs, &box, pxprox, pyprox, pzprox, &nxprox,
                     &nyprox, &nzprox,
                     periodic.y0, periodic.y1 );

          if ( bc.flags & BC_F_PERIODIC )
            {
              for ( int i = 0; i < nHs; i++ )
                {
                  sph_t *psph = pHlst + i;
                  /* Drop the flags */
                  psph->flags &= ~( SPH_F_HAS_CLONE | SPH_F_CLONE );
                }

              /* Create virtual particles and corresponding bounding boxes */
              nVHs = 0;
              for ( int i = 0; i < nyprox; i++ )
                {
                   lpindex_t sbb_id = pyprox[i];
                   sbb_t *psbb = sbbs + sbb_id;
                   if ( psbb->flag & SBB_F_SPH )
                     {
                       lpindex_t sph_id = sbb_id - nAs;
                       sph_t *psph = pHlst + sph_id;

                       if ( psph->flags & SPH_F_BOUNDARY )
                         continue;

                       int left_bound = psbb->c.y < psbb->r + periodic.y0;
                       sbb_t *psbb_virt = sbbs + nsbbs + nVHs;
                       /* Copy bounding box */
                       *psbb_virt = *psbb;
                       /* Move */
                       if ( left_bound )
                       psbb_virt->c.y += periodic.L;
                       else
                       psbb_virt->c.y -= periodic.L;

                       /* Copy particle itself */
                       sph_t *psph_virt = pHlst + nHs + nVHs;
                       /* Copy particle */
                       *psph_virt = *psph;
                       /* Set info */
                       psph->flags |= SPH_F_HAS_CLONE;
                       psph->clone_id = nHs + nVHs;
                       psph_virt->flags |= SPH_F_CLONE;
                       /* Move */
                       if ( left_bound )
                         psph_virt->c.y += periodic.L;
                       else
                         psph_virt->c.y -= periodic.L;

                       nVHs += 1;
                     }
                  }

                if ( nsbbs + nVHs > nsbbs_max )
                  fatal( 1, "Too many bounding boxes\n" );
            }
          switch ( settings.cdmethod )
            {
            case SETTINGS_CDM_NAIVE:
              coldet_naive_detect( &cdnaive, sbbs, nsbbs + nVHs,
                                   &collisions,
                                   pxprox, pyprox, pzprox,
                                   &nxprox, &nyprox, &nzprox );
              break;
            case SETTINGS_CDM_SPX:
              coldet_spx_detect( &cdspx, sbbs, nsbbs + nVHs,
                                 &collisions,
                                 pxprox, pyprox, pzprox,
                                 &nxprox, &nyprox, &nzprox );
              break;

            case SETTINGS_CDM_SPSP:

              coldet_spsp_detect( &cdspsp, sbbs, nsbbs + nVHs,
                                  &collisions,
                                  pxprox, pyprox, pzprox,
                                  &nxprox, &nyprox, &nzprox );

              break;
            default:
              fatal( 1,
                     "Collision detection method is not properly set" );
            }

          lpindex_t K = lpdyna_length( &collisions );
          collision_t * restrict pC = (collision_t*) collisions.data;

          /* assigning contact type */
          for ( lpindex_t k = 0; k < K; ++k )
            {
              lpindex_t i = pC[k].i;
              lpindex_t j = pC[k].j;

              pC[k].type =
                ( sbbs[i].flag | sbbs[j].flag ) & ( SBB_F_SPH | SBB_F_DEM );
            }

          /* hash table renewal */
          for ( lpindex_t i = 0; i < K; ++i )
            {
              if ( pC[i].type == COLDET_COLLISION_TYPE_DEM_DEM )
                {
                  lpindex_t nhash
                    = conhash_lookup( &conhash, pC[i].i, pC[i].j );

                  if ( nhash == CONHASH_ERROR_LOOKUP )
                    {
                      pC[i].nhash = COLDET_CONHASH_UNKNOWN;
                    }
                  else
                    {
                      pC[i].nhash = nhash;
                    }
                }
            }


          coldet_collision_precompute( &collisions,
                                       pAlst, nAs, pBlst, pcmprops );

          profile.condets += timerec_lapsed( &timerec );

        }

  if ( pHlst != NULL ) {
      sph_reset( pHlst, nHs + nVHs );

      TIMEREC_RECORD( &timerec, profile.sphdem_Thetas,
                      sphdem_Thetas( &collisions, pHlst,
                                     nHs, pAlst, nAs, pVlst, sbbs ) );

#if 1
      TIMEREC_RECORD( &timerec, profile.sph_density_mstream,
                      sph_mstream( &collisions, nAs, pHlst, nHs, &sph_omp ),
                      sph_mstream_collect( pHlst, nHs, &sph_omp ) );

      TIMEREC_RECORD( &timerec, profile.sphdem_mstream,
                      sphdem_mstream( &collisions, pHlst, nHs, pBlst,
                                      nBs, pAlst, nAs, pVlst, sbbs ) );

      TIMEREC_RECORD( &timerec, profile.sph_density,
                      sph_density( pHlst, nHs + nVHs, t.dt ) );
#else
      TIMEREC_RECORD( &timerec, profile.sph_density_sum,
                      sph_density_sum( &collisions, nAs, pHlst, nHs, &sphprops ) );
#endif
      
      TIMEREC_RECORD( &timerec, profile.sph_pressure,
                      sph_pressure( pHlst, nHs + nVHs, &sphprops ) );
      TIMEREC_RECORD( &timerec, profile.sph_forces,
                      sph_forces( &collisions, nAs, pHlst, nHs,
                                  &sphprops, t.dt, &sph_omp ),
                      sph_forces_collect( pHlst, nHs, &sph_omp ) );
      TIMEREC_RECORD( &timerec, profile.sph_gravity,
                      sph_gravity( pHlst, nHs + nVHs, &g ) );
      }

      /* sph_bc( &box, pHlst, nAs, pxprox, pyprox, pzprox, */
      /*                           nxprox, nyprox, nzprox ); */

      /* gravitation is the first forces/torques procedure that clears all old
         forces and torques and assings gravity */
      timerec_init( &timerec );
      forces_gravitation( pBlst, nBs, &g );
      profile.gravitation += timerec_lapsed( &timerec );

      forces_extforces( pBlst, nBs, L );

      if ( drag.isdrag ) forces_drag( pBlst, nBs, &drag );

      /* ----  */

      timerec_init( &timerec );
      forces_bb( pBlst, pAlst, pVlst,
                 &collisions,
                 pcmprops, nBs, nAs, nA_mats, &conhash, t.dt, t.n,
                 &settings,
                 &diagnostics,
                 &vc_sq_max);
      profile.forces_bb_current = timerec_lapsed( &timerec );
      profile.forces_bb += profile.forces_bb_current;

      if ( pSlst ) forces_springs( pBlst, pSlst, nSs );

      if ( pTlst ) forces_ptriangles( pBlst, pTlst, nTs );

      /* SPH_DEM forces */
      if ( pHlst ) {
      TIMEREC_RECORD( &timerec, profile.sphdem_forces,
                      sphdem_forces( &collisions, pHlst, nHs,
                                     pBlst, nBs, pAlst, nAs,
                                     pVlst, sbbs, t.dt ) );
      /* sph dynamics can be done now */
      TIMEREC_RECORD( &timerec, profile.sph_dynamics,
                      sph_dynamics( pHlst, nHs + nVHs, t.dt ) );

      /* "anti-dynamics" */
      TIMEREC_RECORD( &timerec, profile.sphdem_velcorrection,
                      sphdem_velcorrection( &collisions, pHlst, nHs,
                                            pBlst, nBs, pAlst, nAs,
                                            pVlst, sbbs, t.dt ) );

      TIMEREC_RECORD( &timerec, profile.sph_placement,
                      sph_placement( pHlst, nHs + nVHs, t.dt ) );

      if ( bc.flags & BC_F_PERIODIC )
        {
          TIMEREC_RECORD( &timerec, profile.sph_teleport,
                          sph_teleport( pHlst, nHs, &box,
                                        periodic.y0, periodic.y1,
                                        periodic.L ) );
        }
      }
      
#if 0 /* turning off boundary DEM on the box */
      timerec_init( &timerec );
      bc_forces( &bc, pAlst, pVlst, pBlst, nAs,
                 pxprox, pyprox, pzprox,
                 nxprox, nyprox, nzprox,
                 &box, pA_mats, nA_mats, pcmprops, &conhash, &t,
                 &diagnostics );
      profile.forces_bc += timerec_lapsed( &timerec );
#endif

      timerec_init( &timerec );
      dynamics( pBlst, nBs, L, t.dt, settings.flags );
      profile.dynamics += timerec_lapsed( &timerec );

#if 0      
      bc_velocities( &bc, pBlst, nBs, &box );
#endif
      
      timerec_init( &timerec );
      placement_bodies( pBlst, nBs, L, t.dt, settings.flags );
      portal_dem_teleport( &portal, sbbs, pBlst, nAs );
      profile.placements_bodies += timerec_lapsed( &timerec );

      timerec_init( &timerec );
      placement_verts( pVlst, pBlst, nVs );
      profile.placements_verts += timerec_lapsed( &timerec );

      timerec_init( &timerec );
      placement_sbbs( sbbs, pAlst, pBlst, nAs, pHlst, nHs );
      profile.placements_sbb += timerec_lapsed( &timerec );

      if ( pSlst ) placement_springs( pSlst, pBlst, nSs );

      conhash_comp.last_ulm =
        conhash_lookup_miss_cost( &conhash,
                                  conhash_comp.nprobes, nAs );
      if ( conhash_comp.last_ulm > conhash_comp.ULMT )
        {
          conhash_nocontacts_clean( &conhash, t.n );
          conhash_cleanup( &conhash );
          conhash_comp.ncleans ++;
        }

      /* Energy runs only if callback.energy is defined (otherwise no
         reason) */
      if ( luacback_defined_flags & LUACBACK_F_DEFINED_ENERGY )
        {
          timerec_init( &timerec );
          energy( pBlst, nBs, &g, &t, &etmax, &ermax, &Et, &Er, &Ep);
          profile.energy += timerec_lapsed( &timerec );
        }

      /* ----------------------------------------------------------- */
      /*                        SAVING                               */
      /* ----------------------------------------------------------- */
      if ( lptimer_inc( &t ) )
        {
          timerec_init( &timerec );

          int errcode = luacback_presave( L, outfile, &t );
          if ( errcode == LUACBACK_ERROR_PCALL )
            fatal( 1, "Function luacback_presave failed to run\n\n"
                   "Lua error message:\n\n%s\n", lua_tostring(L,-1) );

          highlio_save( outfile,
                        &settings, &t, sbbs, pAlst,
                        pVlst, pBlst, pSlst, pTlst, pHlst,
                        nsbbs, nAs, nVs, nBs, nSs, nTs, nHs,
                        &conhash, &conhash_comp,
                        &bc,
                        &box, &g, &drag,
                        pA_gnames, nA_gnames,
                        pA_intrx, nA_intrx,
                        pA_mats, pA_matnames, nA_mats,
                        pcmprops, ncmprops, &sbb_params, &sphprops, &portal );

          luacback_aftersave( L, outfile, &t, &conhash_comp );

          profile.saves += timerec_lapsed( &timerec );
  
        }

      /* ----------------------------------------------------------- */
      /*                 After timestep CALLBACKS                    */
      /* ----------------------------------------------------------- */

      timerec_init( &timerec );

      LUACBACK_RUN( luacback_g( L, &t, &g ),
                    luacback_defined_flags,
                    LUACBACK_F_DEFINED_G );

      if ( f_cdrun )
        {
          LUACBACK_RUN( luacback_contact_detection( L, &t, &f_cdrun ),
                        luacback_defined_flags,
                        LUACBACK_F_DEFINED_CONTACT_DETECTION );
        }

      LUACBACK_RUN( luacback_energy( L, &t, etmax, ermax, Et, Er, Ep ),
                    luacback_defined_flags,
                    LUACBACK_F_DEFINED_ENERGY );

      LUACBACK_RUN( luacback_diagnostics( L, &t,
                                          &diagnostics ),
                    luacback_defined_flags,
                    LUACBACK_F_DEFINED_DIAGNOSTICS );

      LUACBACK_RUN( luacback_profile( L, &t,
                                      &profile ),
                    luacback_defined_flags,
                    LUACBACK_F_DEFINED_PROFILE );

      /* This function may change timestep if defined! */
      LUACBACK_RUN( luacback_dt( L, &t, vc_sq_max ),
                    luacback_defined_flags,
                    LUACBACK_F_DEFINED_DT );

      /* ----------------------------------------------------------- */
      /* aftertimestep call back is very special, it may report a file
         name for immidiate save, or signal to halt the program. In
         this case there is no presave call back runs. It still runs
         aftersave. */
      /* ----------------------------------------------------------- */

      unsigned int fhalt = 0;
      unsigned int fsave = 0;
      LUACBACK_RUN( luacback_aftertimestep( L, &t, outfile,
                                            &fsave, &fhalt ),
                    luacback_defined_flags,
                    LUACBACK_F_DEFINED_AFTERTIMESTEP );

      profile.callbacks += timerec_lapsed( &timerec );

      if( fsave )
        {
          timerec_init( &timerec );

          highlio_save( outfile,
                        &settings, &t, sbbs, pAlst,
                        pVlst, pBlst, pSlst, pTlst, pHlst,
                        nsbbs, nAs, nVs, nBs, nSs, nTs, nHs,
                        &conhash, &conhash_comp,
                        &bc,
                        &box, &g, &drag,
                        pA_gnames, nA_gnames,
                        pA_intrx, nA_intrx,
                        pA_mats, pA_matnames, nA_mats,
                        pcmprops, ncmprops, &sbb_params, &sphprops, &portal );

          luacback_aftersave( L, outfile, &t, &conhash_comp );

          profile.saves += timerec_lapsed( &timerec );

          fsave = 0;            /* next time no save again */
        }

      /* ----------------------------------------------------------- */

      profile.total += timerec_lapsed( &timerec_all_timeloops );

      if ( fhalt ) break;       /* halting the loop if required */

    } /* dt loop */

  /* =============================================================== */
  /*                         FINALIZING                              */
  /* =============================================================== */

  /* finalize function call */
  bool fsave = false;
  LUACBACK_RUN( luacback_finalize( L, &t, outfile,
                                   &fsave ),
                luacback_defined_flags,
                LUACBACK_F_DEFINED_FINALIZE );

  /* save if necessary */
  if( fsave )
    {
      timerec_init( &timerec );

      highlio_save( outfile,
                    &settings, &t, sbbs, pAlst,
                    pVlst, pBlst, pSlst, pTlst, pHlst,
                    nsbbs, nAs, nVs, nBs, nSs, nTs, nHs,
                    &conhash, &conhash_comp,
                    &bc,
                    &box, &g, &drag,
                    pA_gnames, nA_gnames,
                    pA_intrx, nA_intrx,
                    pA_mats, pA_matnames, nA_mats,
                    pcmprops, ncmprops, &sbb_params, &sphprops, &portal );

      luacback_aftersave( L, outfile, &t, &conhash_comp );

      profile.saves += timerec_lapsed( &timerec );

      fsave = false;            /* next time no save again */
    }

  /* free the memory */

  switch ( settings.cdmethod )
    {
    case SETTINGS_CDM_NAIVE:
      coldet_naive_destroy( &cdnaive );
      break;
    case SETTINGS_CDM_SPX:
      coldet_spx_destroy( &cdspx );
      break;
    case SETTINGS_CDM_SPSP:
      coldet_spsp_destroy( &cdspsp );
      break;
    default: ;
    }

  /* Pointers allocated in ini */
  if (pBlst) free( pBlst );
  if (pAlst) free( pAlst );
  if (pVlst) free( pVlst );
  if (pcmprops) free( pcmprops );
  if (pHlst) free( pHlst );

  if ( pA_gnames )
    {
      for ( lpindex_t i = 0; i < nA_gnames; ++i )
        free( pA_gnames[i] );
      free( pA_gnames );
    }

  if ( pA_intrx ) free( pA_intrx );

  if ( pA_mats )
    {
      for ( lpindex_t i = 0; i < nA_mats; ++i )
        {
          free( pA_matnames[i] );
        }
      free( pA_matnames );
      free( pA_mats );
    }

  /* Pointers allocated in main */
  if ( settings.run_from_name ) free ( settings.run_from_name );

  lua_close( L );
  if (sbbs) free( sbbs );

  lpdyna_destroy( &collisions );
  free( pxprox );
  free( pyprox );
  free( pzprox );

  conhash_destroy( &conhash );

  free( outfile );

  forces_finalize();  /* clear forces static data */
  sph_omp_finalize( &sph_omp ); /* clear parallel structures */

  printf( "The End\n" );

  printf( "** Profiling info **\n" );
  printf( "Total time (time loop only): %lf\n", profile.total );
  printf( "Callbacks: %lf\n", profile.callbacks );
  printf( "B-B Forces: %lf\n", profile.forces_bb );
  printf( "Contact detection etc.: %lf\n", profile.condets );
  printf( "Saves: %lf\n", profile.saves );

  printf( "BC Forces: %lf\n", profile.forces_bc );
  printf( "Dynamics: %lf\n", profile.dynamics );
  printf( "Placements of Bodies: %lf\n",
          profile.placements_bodies );
  printf( "Placements of Verts: %lf\n",
          profile.placements_verts );
  printf( "Placements of SBBs: %lf\n", profile.placements_sbb );
  printf( "Summon Check: %lf\n", profile.summon_check );

  printf( "Gravitation: %lf\n", profile.gravitation );

  printf( "SPH: density-mstream: %lf\n", profile.sph_density_mstream );
  printf( "SPH: density-sum: %lf\n", profile.sph_density_sum );
  printf( "SPH: density (mstream only): %lf\n", profile.sph_density );
  printf( "SPH: pressure: %lf\n", profile.sph_pressure );
  printf( "SPH: sph-sph forces: %lf\n", profile.sph_forces );
  printf( "SPH: gravity: %lf\n", profile.sph_gravity );
  printf( "SPH: dynamics: %lf\n", profile.sph_dynamics );
  printf( "SPH: placement: %lf\n", profile.sph_placement );
  printf( "SPH: teleport: %lf\n", profile.sph_teleport );
  printf( "SPH: reset: %lf\n", profile.sph_reset );

  printf( "SPH-DEM: mstream: %lf\n", profile.sphdem_mstream );
  printf( "SPH-DEM: Thetas: %lf\n", profile.sphdem_Thetas );
  printf( "SPH-DEM: forces: %lf\n", profile.sphdem_forces );
  printf( "SPH-DEM: velcorrection: %lf\n", profile.sphdem_velcorrection );

  printf( "Condet: N / ncondets ratio: %lf\n", profile.rcondet );

  return 0;
}
