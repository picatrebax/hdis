/*!

  \file vec3d.h

  \brief Vector type macros
*/

#ifndef VEC3D_H
#define VEC3D_H

#include <math.h>
#include "defs.h"

typedef struct vec3d_s {
  real x;
  real y;
  real z;
} vec3d_t;

#define vec3d_set( v, x_, y_, z_ ) \
  { (v).x = (x_); (v).y = (y_); (v).z = (z_); }
#define vec3d_setx( v, val ) { (v).x = (val); }
#define vec3d_sety( v, val ) { (v).y = (val); }
#define vec3d_setz( v, val ) { (v).z = (val); }
#define vec3d_setnull( v ) {(v).x = (v).y = (v).z = 0.0;}
#define vec3d_assign( copy, orig ) \
  { (copy).x = (orig).x; (copy).y = (orig).y; (copy).z = (orig).z; }

#define vec3d_sq(v) ( (v).x*(v).x + (v).y*(v).y + (v).z*(v).z )
#define vec3d_abs(v) sqrt(vec3d_sq(v))
#define vec3d_sqdiff(a,b) ( ((a).x-(b).x)*((a).x-(b).x) + \
                            ((a).y-(b).y)*((a).y-(b).y) + \
                            ((a).z-(b).z)*((a).z-(b).z) )

#define vec3d_absdiff(a,b) sqrt( vec3d_sqdiff(a,b) )

/* b = k * a */
#define vec3d_scale( b, a, k )   \
  {                              \
    (b).x = (k)*(a).x;           \
    (b).y = (k)*(a).y;           \
    (b).z = (k)*(a).z;           \
  }

#define vec3d_normalize( v, w ) \
  {                             \
    const real invnorm = 1.0 / vec3d_abs(w);   \
    (v).x = (w).x * invnorm;       \
    (v).y = (w).y * invnorm;       \
    (v).z = (w).z * invnorm;       \
  }

/* result = arg1 + arg2 */
#define vec3d_sum( result, arg1, arg2 ) \
  {                                     \
    (result).x = (arg1).x + (arg2).x;   \
    (result).y = (arg1).y + (arg2).y;   \
    (result).z = (arg1).z + (arg2).z;   \
  }

/* result = arg1 + arg2 */
#define vec3d_diff( result, arg1, arg2 ) \
  {                                      \
    (result).x = (arg1).x - (arg2).x;    \
    (result).y = (arg1).y - (arg2).y;    \
    (result).z = (arg1).z - (arg2).z;    \
  }

#define vec3d_expy( a, b, k )            \
  {                                      \
    (a).x += k * (b).x;                  \
    (a).y += k * (b).y;                  \
    (a).z += k * (b).z;                  \
  }

inline static void
vec3d_expy_atomic( vec3d_t* pa, vec3d_t *pb, real k )
{
#if defined( PARALLEL_OPENMP )
#pragma omp atomic
  pa->x += k*pb->x;
#pragma omp atomic
  pa->y += k*pb->y;
#pragma omp atomic
  pa->z += k*pb->z;
#else
  vec3d_expy( *pa, *pb, k );
#endif
}

#define vec3d_inc( a, b )            \
  {                                  \
    (a).x += (b).x;                  \
    (a).y += (b).y;                  \
    (a).z += (b).z;                  \
  }

inline static void
vec3d_inc_atomic( vec3d_t* pres, vec3d_t* arg )
{
#if defined( PARALLEL_OPENMP )
#pragma omp atomic
  pres->x += arg->x;
#pragma omp atomic
  pres->y += arg->y;
#pragma omp atomic
  pres->z += arg->z;
#else
  vec3d_inc( *pres, *arg );
#endif
}

#define vec3d_dec( a, b )            \
  {                                  \
    (a).x -= (b).x;                  \
    (a).y -= (b).y;                  \
    (a).z -= (b).z;                  \
  }

inline static void
vec3d_dec_atomic( vec3d_t* pres, vec3d_t* arg )
{
#if defined( PARALLEL_OPENMP )
#pragma omp atomic
  pres->x -= arg->x;
#pragma omp atomic
  pres->y -= arg->y;
#pragma omp atomic
  pres->z -= arg->z;
#else
  vec3d_dec( *pres, *arg );
#endif
}

#define vec3d_lincomb( a, b, c, alpha, beta )   \
  {                                             \
    (a).x = (alpha)*(b).x + (beta)*(c).x;       \
    (a).y = (alpha)*(b).y + (beta)*(c).y;       \
    (a).z = (alpha)*(b).z + (beta)*(c).z;       \
  }

#define vec3d_linshift( a, b, c, beta )   \
  {                                       \
    (a).x = (b).x + (beta)*(c).x;         \
    (a).y = (b).y + (beta)*(c).y;         \
    (a).z = (b).z + (beta)*(c).z;         \
  }

#define vec3d_dot(a,b) \
  ( (a).x*(b).x + (a).y*(b).y + (a).z*(b).z )

#define vec3d_x( result, arg1, arg2 )                   \
  {                                                     \
    (result).x = (arg1).y*(arg2).z - (arg1).z*(arg2).y; \
    (result).y = (arg1).z*(arg2).x - (arg1).x*(arg2).z; \
    (result).z = (arg1).x*(arg2).y - (arg1).y*(arg2).x; \
  }

#define vec3d_comp_mult( c, a, b )              \
  {                                             \
    (c).x = (a).x * (b).x;                      \
    (c).y = (a).y * (b).y;                      \
    (c).z = (a).z * (b).z;                      \
  }

#define vec3d_comp_div( c, a, b )               \
  {                                             \
    (c).x = (a).x / (b).x;                      \
    (c).y = (a).y / (b).y;                      \
    (c).z = (a).z / (b).z;                      \
  }

#define vec3d_getx(v) ((v).x)
#define vec3d_gety(v) ((v).y)
#define vec3d_getz(v) ((v).z)

#define vec3d_eular_angular_product( c, a, b )  \
  {                                             \
    (c).x = (a).y * (a).z * ( (b).y - (b).z );  \
    (c).y = (a).z * (a).x * ( (b).z - (b).x );  \
    (c).z = (a).x * (a).y * ( (b).x - (b).y );  \
  }

#define vec3d_proj( vtau, vn, v, n ) \
  {                                  \
    vn = vec3d_dot( v, n );          \
    (vtau).x = (v).x - (vn) * (n).x; \
    (vtau).y = (v).y - (vn) * (n).y; \
    (vtau).z = (v).z - (vn) * (n).z; \
  }

#define vec3d_pprint( v ) \
  printf( #v " = ( %f, %f, %f )\n", (v).x, (v).y, (v).z )

#endif  /* VEC3D_H */
