/*!

  \file profile.h

  \brief Profiling structure

*/

#ifndef PROFILE_H
#define PROFILE_H

typedef struct profile_s
{                              /* BB = body-body */
  double total;                 /*!< total time spent for time loop  */
  double callbacks;             /*!< callback total time  */
  double saves;                 /*!< total time for saves  */
  double dists_bb;              /*!< BB distances time */
  double dists_bb_current;      /*!< BB distances, current time step */
  double forces_bb;             /*!< BB forces time */
  double forces_bb_current;     /*!< BB forces, current time step */
  double condets;               /*!< contact detection time  */
  double forces_bc;             /*!< Boundary condition forces  */
  double dynamics;              /*!< Dynamics computations  */
  double placements_norms;      /*!< Placements of norms */
  double placements_bodies;     /*!< Placements of bodies */
  double placements_verts;      /*!< Placements of verts */
  double placements_sbb;        /*!< Placements of SBBs  */
  double summon_check;          /*!< Summon check  */
  double gravitation;           /*!< Gravitation */
  double energy;                /*!< Energy  */

  double sph_density_mstream;   /*!< mstream computing density  */
  double sph_density_sum;       /*!< summation computing density  */
  double sph_density;           /*!< density via mstream summation  */
  double sph_pressure;
  double sph_forces;
  double sph_gravity;
  double sph_dynamics;
  double sph_placement;
  double sph_teleport;
  double sph_reset;

  double sphdem_Thetas;
  double sphdem_forces;
  double sphdem_velcorrection;
  double sphdem_mstream;

  /* contact detection */
  real rcondet;                 /* ratio condet to total time steps */
  int ncondet;                  /* number of contact detections */
  
  
} profile_t;


#endif      /* PROFILE_H */
