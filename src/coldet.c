/*!

  \file coldet.c

  \brief Implementation of collision detection

*/

#include "coldet.h"

static int cols_compare( const void* pv1, const void* pv2 )
{
  collision_t *pc1 = (collision_t*)pv1;
  collision_t *pc2 = (collision_t*)pv2;
  return (
          ( pc1->i < pc2->i ) ? (-1) :
          ( ( pc1->i > pc2->i ) ? (1) : ( ( pc1->j < pc2->j ) ? (-1) : 1 ) )
          );
}

void coldet_collision_sort( lpdyna_t* pcol )
{
  const lpindex_t ncols = lpdyna_length( pcol );
  collision_t* pcol_data = ( collision_t* ) pcol->data;
  qsort( pcol_data, ncols, sizeof( collision_t ), cols_compare );
}

void coldet_collision_precompute( lpdyna_t* pcols,
                                  atom_t* pAlst,
                                  lpindex_t nAs,
                                  body_t* pBlst,
                                  cmprops_t* pcmprops )
{
  const lpindex_t ncols = lpdyna_length( pcols );
  collision_t* pcols_data = ( collision_t* ) pcols->data;

# if defined( PARALLEL_OPENMP )
# pragma omp parallel
# endif
  {

    /* local parameter for interaction mass */
    real m, R;

# if defined( PARALLEL_OPENMP )
# pragma omp for nowait
# endif
    for ( lpindex_t k = 0; k < ncols; ++k )
      {
        /* indices of Atoms/SBBs that possibly collide */
        lpindex_t i = pcols_data[k].i;
        lpindex_t j = pcols_data[k].j;

if ( i < nAs && j < nAs )
{
        /* direct pointers to the atoms */
        const atom_t* patom_i = pAlst + i;
        const atom_t* patom_j = pAlst + j;

        /* pointers to the corresponding bodies */
        body_t *pbody_i = pBlst + patom_i->body_indx;
        body_t *pbody_j = pBlst + patom_j->body_indx;

        m = conmech_mass_eff( pbody_i->m, pbody_j->m );
        R = conmech_hertz_radius_eff( patom_i->ri, patom_j->ri );
        i = isps( patom_i->material_id, patom_j->material_id );

        /* interacting radius */

        pcols_data[k].rint = R;
        pcols_data[k].kni = pcmprops[i].Skni * pow(m,0.4) * pow(R,0.3);
        pcols_data[k].icmprop = i;
}
else if ( i < nAs || j < nAs ) /* sph - dem contact properties */
{

}
      }
  } /* end of parallel region when applicable */
}

void coldet_bc( const sbb_t* plst,
                const lpindex_t nprts,
                box_t* pbox,
                lpindex_t* pxprox,
                lpindex_t* pyprox,
                lpindex_t* pzprox,
                lpindex_t* pnpx,
                lpindex_t* pnpy,
                lpindex_t* pnpz,
                real y0, real y1 )
{
  *pnpx = *pnpy = *pnpz = 0;

  for ( lpindex_t i = 0; i < nprts; ++i )
    {
      if ( ( plst[i].flag & SBB_F_SPH ) && sbb_yprox( plst[i], y0, y1 ) )
        pyprox[ (*pnpy)++ ] = i;

      /* we remove all boundary check now for DEM! */
#if 0      
      if ( sbb_xprox( plst[i], pbox->fcorner.x ) )
        pxprox[ (*pnpx)++ ] = i;
      if ( sbb_zprox( plst[i], pbox->fcorner.z ) )
        pzprox[ (*pnpz)++ ] = i;
#endif
    }
}

void coldet_naive_init( coldet_naive_t* pmethod, box_t* pbox,
                        const int* pA_intrx,
                        const lpindex_t ngroups )
{
  pmethod->X = vec3d_getx( pbox->fcorner );
  pmethod->Y = vec3d_gety( pbox->fcorner );
  pmethod->Z = vec3d_getz( pbox->fcorner );


pmethod->pintrx = malloc( ngroups*ngroups*sizeof( int ) );
for ( lpindex_t i = 0; i < ngroups; ++i )
  for ( lpindex_t j = 0; j < ngroups; ++j )
    {
      int k = isps( i, j );
      (pmethod->pintrx)[ ngroups*j + i ] = pA_intrx[k];
    }
pmethod->ngroups = ngroups;

                                /* group interactions */
}
void coldet_naive_detect( coldet_naive_t* pmethod,
                          const sbb_t* plst,
                          const lpindex_t nprts,
                          lpdyna_t* pcollisions,
                          lpindex_t* pxprox,
                          lpindex_t* pyprox,
                          lpindex_t* pzprox,
                          lpindex_t* pnpx,
                          lpindex_t* pnpy,
                          lpindex_t* pnpz )

{
  /* light clean of all previous values */
  lpdyna_clean( pcollisions );

  /* finding the contacting SBBs */
# ifdef PARALLEL_OPENMP
# pragma omp parallel for shared( pcollisions )
# endif
  for ( lpindex_t i = 0; i < nprts; ++i )
    {
      collision_t collision;           /* local temp collision */
      int* pintrx = pmethod->pintrx;
      int ngroups = pmethod->ngroups;

      for ( lpindex_t j = i+1; j < nprts; ++j )
        {
          if ( sbb_collide( plst[i], plst[j] ) )
            {
              collision.i = i;
              collision.j = j;
              lpdyna_add( pcollisions, &collision );
            }
        }
    }
}


int coldet_spx_init( coldet_spx_t* pmethod, const lpindex_t nprts,
                     const box_t* pbox,
                     const int* pA_intrx,
                     const lpindex_t ngroups )
{
  pmethod->X = vec3d_getx( pbox->fcorner );
  pmethod->Y = vec3d_gety( pbox->fcorner );
  pmethod->Z = vec3d_getz( pbox->fcorner );

  pmethod->markers = malloc( nprts * sizeof( spx_marker_t ));


pmethod->pintrx = malloc( ngroups*ngroups*sizeof( int ) );
for ( lpindex_t i = 0; i < ngroups; ++i )
  for ( lpindex_t j = 0; j < ngroups; ++j )
    {
      int k = isps( i, j );
      (pmethod->pintrx)[ ngroups*j + i ] = pA_intrx[k];
    }
pmethod->ngroups = ngroups;

                                /* group interactions */

  return pmethod->markers ? 1 : 0;
}

int cmp_spx_markers( const void *pa, const void *pb )
{
  real a = ((spx_marker_t*)pa) -> b;
  real b = ((spx_marker_t*)pb) -> b;
  return a < b ? -1 : 1;
}


void coldet_spx_sort( spx_marker_t * sms, lpindex_t nprts )
{
  qsort( sms, nprts, sizeof( spx_marker_t ), cmp_spx_markers );
}


void coldet_spx_detect( coldet_spx_t* pmethod,
                        const sbb_t* plst,
                        const lpindex_t nprts,
                        lpdyna_t* pcollisions,
                        lpindex_t* pxprox,
                        lpindex_t* pyprox,
                        lpindex_t* pzprox,
                        lpindex_t* pnpx,
                        lpindex_t* pnpy,
                        lpindex_t* pnpz )

{
  spx_marker_t* ms = pmethod->markers; /* markers, to simplify expressions */

  /* light clean of all previous values */
  lpdyna_clean( pcollisions );

  /* initialization of SBB markers */
# ifdef PARALLEL_OPENMP
# pragma omp parallel for shared( ms )
# endif
  for ( lpindex_t k = 0; k < nprts; ++k )
    {
      ms[k].b = vec3d_getx( plst[k].c ) - plst[k].r;
      ms[k].e = vec3d_getx( plst[k].c ) + plst[k].r;
      ms[k].ind = k;
    }

  /* sorting */
  coldet_spx_sort( ms, nprts );

  /* for all markers in the sorted list */
  if ( nprts == 0 ) return;

# ifdef PARALLEL_OPENMP
# pragma omp parallel for
# endif
  for( lpindex_t k = 0; k < nprts-1; ++k )
    {
      collision_t collision;               /* local temp coldet marker */
      real Pe = ms[k].e;
      collision.i = ms[k].ind;

      int* pintrx = pmethod->pintrx;
      int ngroups = pmethod->ngroups;

      for ( lpindex_t l = k+1; l < nprts; ++l )
        {
          real Sb = ms[l].b;

          if ( Sb > Pe ) break; /* we are out of P interval */

          /* collision check */
          collision.j = ms[l].ind;

          if ( sbb_collide( plst[collision.i], plst[collision.j]) )
            {
              /* to ensure that i < j for all collisions */
#if 0
              if ( ( collision.i > collision.j )
                   && ( collision.type == COLDET_COLLISION_TYPE_DEM_DEM ))
                {
                  swap( collision.i, collision.j );
              }
#endif

              lpdyna_add( pcollisions, &collision );
            }
        }
    }

}


int coldet_spsp_init( coldet_spsp_t* pmethod,
                      const real rmax,
                      const box_t* pbox,
                      const lpindex_t nprts,
                      const int* pA_intrx,
                      const lpindex_t ngroups )
{
  /* set the box */
  pmethod->X = vec3d_getx( pbox->fcorner );
  pmethod->Y = vec3d_gety( pbox->fcorner );
  pmethod->Z = vec3d_getz( pbox->fcorner );

  real ds = COLDET_SPSP_CELLFACTOR * rmax;

  pmethod->nx = (lpindex_t) ( vec3d_getx( pbox->fcorner ) / ds );
  pmethod->ny = (lpindex_t) ( vec3d_gety( pbox->fcorner ) / ds );

  pmethod->dx = vec3d_getx( pbox->fcorner ) / pmethod->nx;
  pmethod->dy = vec3d_gety( pbox->fcorner ) / pmethod->ny;
  /* set the dimensions */

  pmethod->pms = malloc( pmethod->nx * pmethod->ny * sizeof( lpdyna_t ) );
  if ( !pmethod->pms ) return 0;

  /* estimation of number of SBBs per column */
  lpindex_t apprlen = 1 +
    (lpindex_t) ( 2*4.0*nprts/(double)(pmethod->nx)/(double)(pmethod->ny) );

  for ( int m = 0; m < pmethod->nx * pmethod->ny; ++m )
    lpdyna_init( pmethod->pms + m, sizeof( spsp_marker_t ),
                 apprlen, apprlen );

  /* allocate memory */
  pmethod->pintrx = malloc( ngroups*ngroups*sizeof( int ) );
  for ( lpindex_t i = 0; i < ngroups; ++i )
    for ( lpindex_t j = 0; j < ngroups; ++j )
      {
        int k = isps( i, j );
        (pmethod->pintrx)[ ngroups*j + i ] = pA_intrx[k];
      }
  pmethod->ngroups = ngroups;

  /* group interactions */

  pmethod->nx_init = pmethod->nx;
  pmethod->ny_init = pmethod->ny;

  return 1;                     /* no error */
}

void coldet_spsp_destroy( coldet_spsp_t * pmethod )
{
  for ( int m = 0; m < pmethod->nx * pmethod->ny; ++m )
    lpdyna_destroy( pmethod->pms + m );
  free( pmethod->pms );
  free( pmethod->pintrx );
}

void coldet_spsp_detector_fill_columns( coldet_spsp_t* pmethod,
                                        const sbb_t* plst,
                                        const lpindex_t nprts )
{

  real dx = pmethod->dx;
  real dy = pmethod->dy;          /* cell sizes */
  lpindex_t nx = pmethod->nx;
  lpindex_t ny = pmethod->ny;     /* domain index limits */


  for ( lpindex_t k=0; k < nprts; ++k )
    {

      real cx, cy, cz;                /* center coordinates */
      real r;                         /* radius */

      lpindex_t ip, jp;               /* p-home cell indices */
      lpindex_t isi, jsi;             /* si-home cell indices */
      lpindex_t isj, jsj;             /* sj-home cell indices */
      lpindex_t it=0, jt=0;           /* t-home cell indices */

      lpindex_t m;                    /* main column index (ip,jp) */
      int si, sj;                     /* shifts for s/t-homes (+/-1) */
      real d_si=0, d_sj=0;            /* distances from center to s-cells */
      int flag_si, flag_sj;           /* set if s-homes exist */
      int flag_tr;                    /* set if t-home exists */

      cx = vec3d_getx( plst[k].c );
      cy = vec3d_gety( plst[k].c );
      cz = vec3d_getz( plst[k].c );
      r = plst[k].r;

      ip = (lpindex_t) ( cx / dx );
      jp = (lpindex_t) ( cy / dy );


      si = cx < ( 0.5 + ip )*dx ? -1 : 1;
      sj = cy < ( 0.5 + jp )*dy ? -1 : 1;

      /* si: */
      isi = ip + si;
      jsi = jp;

      isj = ip;
      jsj = jp + sj;


      /* check if values are legitimate */
      if ( 0 <= isi && isi < nx )
        {
          /* the distance to the si cell: */
          d_si = fmin( cx - ip*dx, (ip+1)*dx - cx );

          /* does SBB crosses si cell? */
          flag_si = d_si < r;
        }
      else flag_si = 0;


      if ( 0 <= jsj && jsj < ny )
        {
          /* the distance to the si cell: */
          d_sj = fmin( cy - jp*dy, (jp+1)*dy - cy );

          flag_sj = d_sj < r;
        }
      else flag_sj = 0;


      /* does SBB crosses t-cell? */
      flag_tr = ( d_si*d_si + d_sj*d_sj < 2*r*r )
        && ( 0 <= jsj && jsj < ny )
        && ( 0 <= isi && isi < nx );
      if ( flag_tr )
        {
          it = ip + si;
          jt = jp + sj;
        }



      spsp_marker_t lmarker;          /* local temp variable marker */

      /* setting local marker: it is the same for all home columns! */
      lmarker.b = cz - r;
      lmarker.e = cz + r;
      lmarker.ind = k;

      /* flag is a special story */
      lmarker.flag = spsp_cell_type( ip, jp ); /* first the p-home type */

      int fhomes = ( 1 << lmarker.flag ) |
        ( flag_si << spsp_cell_type( isi, jsi ) ) |
        ( flag_sj << spsp_cell_type( isj, jsj ) ) |
        ( flag_tr << spsp_cell_type( it, jt ) );

      lmarker.flag = lmarker.flag | ( fhomes << 2 );


      /* m is the real index in array of columns, we add to all home
         columns */
      m = spsp_main_index( ip, jp, nx );

      /* it is possible that the sphere left our area by different reasons
         (too big time step, bad boundary conditions) */

      if ( m >= nx*ny )
        {
          /* fatal( 1, "Indices out of range. The SBB is out of area.\n" */
          /*           "Possible reasons: Too big time step? Bad boundary conditions?\n" */
          /*           "ip: %ld, jp: %ld, nx: %ld, ny: %ld, nx*ny: %ld, m: %ld\n" */
          /*           "cx: %lf, cy: %lf, cz: %lf, r: %lf\n", */
          /*        (long)ip, (long)jp, (long)nx, (long)ny, (long)(nx*ny), (long)m,  */
          /*        (double)cx, (double)cy, (double)cz, (double)r ); */

          /* In this branch we decide just not add the bounding box to play in
             contact detection */
          continue;
        }


      lpdyna_add( pmethod->pms + m, &lmarker );

      if ( flag_si )
        {
          m = spsp_main_index( isi, jsi, nx );
          //if ( m < nx*ny )
            lpdyna_add( pmethod->pms + m, &lmarker );

        }

      if ( flag_sj )
        {
          m = spsp_main_index( isj, jsj, nx );
          //if ( m < nx*ny )
            lpdyna_add( pmethod->pms + m, &lmarker );
        }

      if ( flag_tr )
        {
          m = spsp_main_index( it, jt, nx );
          //if ( m < nx*ny )
            lpdyna_add( pmethod->pms + m, &lmarker );
        }


    }
}


int cmp_spsp_markers( const void *pa, const void *pb )
{
  real a = ((spsp_marker_t*)pa) -> b;
  real b = ((spsp_marker_t*)pb) -> b;
  return a < b ? -1 : 1;
}

void coldet_spsp_single_column_traverse( const lpdyna_t* pcol,
                                         int col_type,
                                         const sbb_t* plst,
                                         lpdyna_t* pcollisions,
                                         const int* restrict pintrx,
                                         const int ngroups )
{
  /* To be safe in the case of unsigned counters, we need this */
  if ( lpdyna_length(pcol) == 0 ) return;

  /* loop for all elements in the column: */
  for ( lpindex_t k=0; k < lpdyna_length(pcol)-1; ++k )
    {
      spsp_marker_t Pmarker;
      lpdyna_get( pcol, &Pmarker, k );
      real Pe = Pmarker.e;
      int Ptype = spsp_flag2ptype( Pmarker.flag );

      /* S markers traverse */

for ( lpindex_t l=k+1; l < lpdyna_length(pcol); ++l )
  {
    spsp_marker_t Smarker;
    lpdyna_get( pcol, &Smarker, l );
    real Sb = Smarker.b;

    /* Stop as soon as no overlapping */
    if ( Sb > Pe ) break;

    int Stype = spsp_flag2ptype( Smarker.flag );

    /* no test if both have no p-home here */
    if ( Stype != col_type && Ptype != col_type)
      continue;

    /* all homes types of P and S markers */
    int fP = spsp_flag2homes( Pmarker.flag );
    int fS = spsp_flag2homes( Smarker.flag );

    /* common homes */
    int ff = fP & fS;

    /* if 'P' SBB type is the same with the traversed column type and
       'S' SBB has type smaller than column's, and both SBBs have a
       common home in 'S' home => no test */

    if ( Ptype == col_type && Stype < col_type
         && ( ff & ( 0x1 << Stype ) ) )
      continue;

    /* if 'S' SBB type is the same with the traversed column type and
       'P' SBB has type smaller than column's, and both SBBs have a
       common home in 'P' home => no test */
    if ( Stype == col_type && Ptype < col_type
         && ( ff & ( 0x1 << Ptype ) ) )
      continue;

    /* only now we do a real check! */
    collision_t collision;
    collision.i = Smarker.ind;
    collision.j = Pmarker.ind;

    if ( sbb_collide( plst[collision.i], plst[collision.j] ) )
      {
        /* to ensure that i < j for all collisions */
#if 0
        if ( ( collision.i > collision.j )
             && ( collision.type == COLDET_COLLISION_TYPE_DEM_DEM ) )
          swap( collision.i, collision.j );
#endif

        lpdyna_add( pcollisions, &collision );
      }
  }

    }
}


void coldet_spsp_detector_find_collisions( coldet_spsp_t* pmethod,
                                           const sbb_t* plst,
                                           lpdyna_t* pcollisions )
{
  const lpindex_t M = (pmethod->nx) * (pmethod->ny);

# ifdef PARALLEL_OPENMP
# pragma omp parallel for
# endif
  for ( lpindex_t m = 0; m < M; ++m )
    {
      /* current column index */
      lpindex_t i = spsp_i_by_main_index( m, pmethod->nx );
      lpindex_t j = spsp_j_by_main_index( m, pmethod->nx );

      /* current column */
      lpdyna_t *pcol = (lpdyna_t*)( pmethod->pms + m );

      /* type of the column */
      int col_type = spsp_cell_type(i,j);

      /* To be safe in the case of unsigned counters, we need this and
         we prefer to do this before entering the function especially
         in parallel runs. Well, if there is just one element in the
         column; then there is no collisions either */
      if ( lpdyna_length(pcol) < 2 ) continue;

      /* sorting */
      qsort( pcol->data, lpdyna_length( pcol ),
             sizeof( spsp_marker_t ), cmp_spsp_markers );

      /* traverse */
      coldet_spsp_single_column_traverse( pcol, col_type,
                                         plst, pcollisions,
                                         pmethod->pintrx,
                                         pmethod->ngroups );
    }

#if defined( COLDET_SORTING )
  coldet_sort( pcollisions );
#endif

#ifdef COLDET_REPETITION_CHECKING
  /* check if there are duplicates */
  const lpindex_t ncols = lpdyna_length( pcollisions );
  collision_t* pcols_data =
                          (collision_t*) pcollisions->data;

  lpindex_t iold = 0;
  lpindex_t jold = 0;
  for ( lpindex_t m = 0; m < ncols; ++m )
    {
      lpindex_t i = pcols_data[m].i;
      lpindex_t j = pcols_data[m].j;

      if ( ( i == iold ) && ( j == jold ) )
        printf ( "HORRIBLE!!! REPETITIONS!!!\n" );

      iold = i; jold = j;
    }
#endif

}


void coldet_spsp_detect( coldet_spsp_t* pmethod,
                         const sbb_t* plst,
                         const lpindex_t nprts,
                         lpdyna_t* pcollisions,
                         lpindex_t* pxprox,
                         lpindex_t* pyprox,
                         lpindex_t* pzprox,
                         lpindex_t* pnpx,
                         lpindex_t* pnpy,
                         lpindex_t* pnpz )

{
  const lpindex_t M = ( pmethod->nx ) * ( pmethod->ny );

  for ( lpindex_t m=0; m < M; ++m )
    lpdyna_clean( pmethod->pms + m );

  /* light clean of all previous collisions */
  lpdyna_clean( pcollisions );


  coldet_spsp_detector_fill_columns( pmethod, plst, nprts );
  coldet_spsp_detector_find_collisions( pmethod, plst, pcollisions );
}

/* I made much testing with breaking the loop and could not find
   faster way in the case of OpenMP to run this summon check. There is
   no easy break from the loop and often summon check do not trigger
   contact detection, so all branches has to be executed in full. It
   is still too long and I am working on figuring out how to improve
   it. */
int coldet_summon_check( const sbb_t* restrict pSlst,
                         const atom_t* restrict pAlst,
                         const lpindex_t nAs,
                         const lpindex_t nHs )
{
  int ans = 0;                  /* default: no check necessary */

# ifdef PARALLEL_OPENMP
# pragma omp parallel for reduction(+:ans)
# endif
  for ( lpindex_t i = 0; i < nAs; ++i )
  {
    real rdiff = 1.0;
    real c2 = 0.0;
    c2 = vec3d_sqdiff( pSlst[i].c0, pSlst[i].c );
    rdiff = pSlst[i].r - pAlst[i].sbb_Rmin;
    ans += ( rdiff*rdiff < c2 );
  }

  for ( lpindex_t i = 0, j = nAs; i < nHs; ++i, ++j )
    {
      real rdiff = 1.0;
      real c2 = 0.0;
      c2 = vec3d_sqdiff( pSlst[j].c0, pSlst[j].c );
      rdiff = pSlst[j].r - pSlst[j].rmin;
      ans += ( rdiff*rdiff < c2 );
    }

  return ans;
}

int colcomp( const void *pa, const void *pb )
{
  const collision_t* pca = ( const collision_t* ) pa;
  const collision_t* pcb = ( const collision_t* ) pb;
  if ( pca->i < pcb->i ) return -1;
  if ( pca->i > pcb->i ) return  1;
  if ( pca->j < pcb->j ) return -1;
  return 1;
}

void coldet_sort( lpdyna_t* pcollisions )

{
  const lpindex_t ncols = lpdyna_length( pcollisions );
  collision_t* pcols_data =
                          (collision_t*) pcollisions->data;

  /* sorting of collision in place */
  qsort( pcols_data, ncols, sizeof( collision_t ), colcomp );
}
