#ifndef ENERGY_H
#define ENERGY_H

#include <lua.h>                /* lua stuff */
#include <lauxlib.h>
#include <lualib.h>

#include "defs.h"
#include "rot3d.h"
#include "lptimer.h"
#include "body.h"

void energy( const body_t* restrict pBlst, 
             const lpindex_t nBs, 
             const vec3d_t* restrict pg, 
             const lptimer_t* restrict pt,
             real* restrict petmax,
             real* restrict permax,
             real* restrict pEt, 
             real* restrict pEr, 
             real* restrict pEp );

#endif

