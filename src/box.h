/*!

  \file box.h

  \brief Domain dimensions.

*/

#ifndef BOX_H
#define BOX_H

#include <assert.h>
#include "defs.h"
#include "vec3d.h"

typedef struct box_s {
  vec3d_t fcorner;           /*!< right-back-upper corner
                               coordinates */
  int xid;                   /*!< ids of each wall */
  int yid;
  int zid;
} box_t;


void box_init( box_t * pB,
               const real x, const real y, const real z );

#endif      /* BOX_H */
