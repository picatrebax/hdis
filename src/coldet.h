/*!

  \file coldet.h

  \brief Collision detection

*/

#ifndef COLDET_H
#define COLDET_H

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "defs.h"
#include "vec3d.h"
#include "lpdyna.h"
#include "box.h"
#include "bb.h"
#include "body.h"
#include "conmech.h"
#include "sph.h"

#define COLDET_CONHASH_UNKNOWN (-1)

#define COLDET_COLLISION_TYPE_DEM_DEM ( SBB_F_DEM | SBB_F_DEM )
#define COLDET_COLLISION_TYPE_SPH_DEM ( SBB_F_SPH | SBB_F_DEM )
#define COLDET_COLLISION_TYPE_SPH_SPH ( SBB_F_SPH | SBB_F_SPH )

typedef struct collision_s
{
  vec3d_t Theta;                /* Theta integral, for sph-dem */
  real rint;                    /* interaction radius */
  real kni;                     /* damping coefficient precomputed */

  lpindex_t i;                  /* indices of possibly colliding */
  lpindex_t j;                  /* particles */
  lpindex_t nhash;              /* index in contact hash */
  lpindex_t icmprop;            /* index in contact property */
  int type;                     /* sph-related or no */
  int status;                   /* status of sph-sph contact */
} collision_t;

void coldet_collision_sort( lpdyna_t* pcol );

/*! \brief

 */
void coldet_collision_precompute( lpdyna_t* pcols,
                                  atom_t* pAlst,
                                  lpindex_t nAs,
                                  body_t* pBlst,
                                  cmprops_t* pcmprops );

void coldet_bc( const sbb_t* plst,
                const lpindex_t nprts,
                box_t* pbox,
                lpindex_t* pxprox,
                lpindex_t* pyprox,
                lpindex_t* pzprox,
                lpindex_t* pnpx,
                lpindex_t* pnpy,
                lpindex_t* pnpz,
                real y0, real y1 );

typedef struct coldet_naive_s
{
  real X;       /* dimensions of the box (domain) */
  real Y;
  real Z;
  int flag;

  int* pintrx;                  /* array of group interactions */
  int ngroups;                  /* number of atom groups */

} coldet_naive_t;

void coldet_naive_init( coldet_naive_t* pmethod, box_t* pbox,
                        const int* pA_intrx,
                        const lpindex_t ngroups );

void coldet_naive_detect( coldet_naive_t* pmethod,
                          const sbb_t* plst,
                          const lpindex_t nprts,
                          lpdyna_t* pcollisions,
                          lpindex_t* pxprox,
                          lpindex_t* pyprox,
                          lpindex_t* pzprox,
                          lpindex_t* pnpx,
                          lpindex_t* pnpy,
                          lpindex_t* pnpz );

#define coldet_naive_destroy( ptype )

typedef struct spx_marker_s
{
  real b;                       /* beginning of the projection */
  real e;                       /* ending of the projection */
  lpindex_t ind;                /* index of the SBB */
} spx_marker_t;

typedef struct coldet_spx_s
{
  real X;       /* dimensions of the box (domain) */
  real Y;
  real Z;
  spx_marker_t* markers;

  int* pintrx;                  /* array of group interactions */
  int ngroups;                  /* number of atom groups */

} coldet_spx_t;

int coldet_spx_init( coldet_spx_t* pmethod, const lpindex_t nprts,
                     const box_t* pbox,
                     const int* pA_intrx,
                     const lpindex_t ngroups );

void coldet_spx_detect( coldet_spx_t* pmethod,
                        const sbb_t* plst,
                        const lpindex_t nprts,
                        lpdyna_t* pcollisions,
                        lpindex_t* pxprox,
                        lpindex_t* pyprox,
                        lpindex_t* pzprox,
                        lpindex_t* pnpx,
                        lpindex_t* pnpy,
                        lpindex_t* pnpz );

#define coldet_spx_destroy( pmethod ) free( (pmethod)->markers )


#define COLDET_SPSP_CELLFACTOR 3.0

typedef struct spsp_marker_t
{
  real b;                       /* beginning of the projection */
  real e;                       /* ending of the projection */
  lpindex_t ind;                /* index of the SBB */
  int flag;                     /* type of primary home + home flags */
} spsp_marker_t;

#define spsp_flag2ptype( flag ) ( (flag) & 0x3 )
#define spsp_flag2homes( flag ) ( (flag) >> 0x2 )

typedef struct coldet_spsp_s
{
  real X;       /* dimensions of the box (domain) */
  real Y;
  real Z;

  real dx;                      /* cell x-size */
  real dy;                      /* cell y-size */
  lpindex_t nx;                 /* number of x-cells (index i) */
  lpindex_t ny;                 /* number of y-cells (index j) */

  lpindex_t nx_init;            /* initial number of x-cells (index i) */
  lpindex_t ny_init;            /* initial number of y-cells (index j) */

  lpdyna_t* pms;                /* main dynamic array of SP/SP
                                   markers */
  int* pintrx;                  /* array of group interactions */
  int ngroups;                  /* number of atom groups */

} coldet_spsp_t;

#define spsp_cell_type(i,j) ( (i)%2 + 2*((j)%2) )

#define spsp_main_index(i,j, nx) ( (nx)*(j) + (i) )

#define spsp_i_by_main_index( m, nx ) ( (m) % (nx) )
#define spsp_j_by_main_index( m, nx ) ( (m) / (nx) )

int coldet_spsp_init( coldet_spsp_t* pmethod,
                      const real rmax,
                      const box_t* pbox,
                      const lpindex_t nprts,
                      const int* pA_intrx,
                      const lpindex_t ngroups );

void coldet_spsp_destroy( coldet_spsp_t * pmethod );

void coldet_spsp_detect( coldet_spsp_t* pmethod,
                         const sbb_t* plst,
                         const lpindex_t nprts,
                         lpdyna_t* pcollisions,
                         lpindex_t* pxprox,
                         lpindex_t* pyprox,
                         lpindex_t* pzprox,
                         lpindex_t* pnpx,
                         lpindex_t* pnpy,
                         lpindex_t* pnpz );


int coldet_summon_check( const sbb_t* restrict pSlst,
                        const atom_t* restrict pAlst,
                        const lpindex_t nAs,
                        const lpindex_t nHs );

void coldet_sort( lpdyna_t* pcollisions );

#endif  /* COLDET_H */
