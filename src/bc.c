
#include "bc.h"

#define bc_sphere_delta_macro( dim )               \
{                                                  \
  /* calculate the distance to the planes */       \
  X = vec3d_get ## dim ( ppoint->global_pos );     \
  C = vec3d_get ## dim ( pbox->fcorner );          \
  delta = (real) fmax( R-X, R+X-C );               \
}


static void bc_forces_X
             (
               bc_t* pbc,
               atom_t* pAlst,     /* atoms to traverse */
               const vert_t* restrict pVlst,    /* points */
               body_t* pBlst,     /* bodies */
               const lpindex_t nAs,     /* number of atoms */
               lpindex_t* pprox,  /* x bound. proximity list */
               const lpindex_t nprox,   /* n. of elements in prox.lists */
               const box_t* restrict pbox,       /* the box */
               material_t *pA_mats,
               lpindex_t nA_mats,
               cmprops_t * restrict pcmprops, /* mech props in the system */
               conhash_t * pch,
               const lptimer_t* restrict pt,
               diagnostics_t* pdiag )
{
  /* Loop for all atoms in the boundary proximity list */

lpindex_t nactual = 0;
# ifdef PARALLEL_OPENMP
# pragma omp parallel for reduction( + : nactual )
# endif
  for ( lpindex_t i = 0; i < nprox; ++i )
    {
if ( pprox[i] < nAs )
{
      atom_t* patom = pAlst + pprox[i]; /* atom */

      body_t* pbody = pBlst + ( patom->body_indx ); /* body */

      /* skip transparent to x bodies */
      if ( pbody->flag & BODY_F_NOXBOUND ) continue;

      /* dilation radius */
      real R = patom->R;


      lpindex_t nactual_loc = 0;

      // TODO: I have no idea if this is right
      /* Loop for all base points in the atom */
      lpindex_t N;
      N = patom->nverts;

      for ( lpindex_t k = 0; k < N; ++k )
        {
          /* center of base sphere */
          const vert_t* ppoint = pVlst + patom->ivert0 + k;
          real X, C;
          real delta;
          bc_sphere_delta_macro( x );

          /* finding the forces */
          if ( delta > 0 )
            {
              /* calculation of the normal vector from atom to wall */
              real sgn = X < R ? 1.0 : -1.0;
              vec3d_t n;       /* from j (wall) to i (particle) */
              vec3d_set( n, sgn, 0, 0 ); /* n=(+/-1,0,0) */


/* radius from center of atom to the point of contact */
vec3d_t rQ;
vec3d_scale( rQ, n, -R );
/* coordinates of the point of contact */
vec3d_t Q;
vec3d_sum( Q, ppoint->global_pos, rQ );
/* vector from center of mass to the point of contact */
vec3d_t rC;
vec3d_diff( rC, Q, pbody->c );
/* vc = relative velocity at the contact (for particle i) */
vec3d_t vQ;
vec3d_x ( vQ, pbody->w, rC );
vec3d_sum( vQ, vQ, pbody->v );
/* receive ids, j = wall, i = particle, i < j */
int j = pbox->xid;
/* contact hash request for the contact and its update */
lpindex_t nhash = conhash_lookup_and_adjust( pch,
                                             i, j,
                                             pt->n );

/* force and torque calculations */
vec3d_t F, T;
lpindex_t K = isps( patom->material_id, pbc->material_id );
real kni = pcmprops[K].Skni * pow(pbody->m,0.4) * pow(0.5*R,0.3);

int ccode =
  conmech_forces( &F, &(pch->cons[nhash].ftau_e),
                  delta, &vQ, &n, pcmprops + K, 0.5*R, kni, pt->dt
                  );
 if ( ccode == CONMECH_CONTACT_NO )
   {
     pch->cons[nhash].nlast = pt->n - 1;
     continue;
   }

vec3d_x( T, rC, F );

# if defined( PARALLEL_OPENMP ) && defined( _OPENMP )
# pragma omp critical (bc_critical)
# endif
{
  /* adding forces and torques to the net body */
  vec3d_sum( pbody->f, pbody->f, F );
  vec3d_sum( pbody->t, pbody->t, T );
}

vec3d_assign( pch->cons[nhash].pc, Q );
vec3d_scale( F, F, -1.0 );  /* because we need forces on body i */
vec3d_assign( pch->cons[nhash].f, F );

              nactual_loc += 1;
            }
        }
      nactual += nactual_loc;
}
else /* sph - boundary */
{
  continue;
}
    }

  pdiag -> contacts_actual_n += nactual;
}

static void bc_forces_Y
             (
               bc_t* pbc,
               atom_t* pAlst,     /* atoms to traverse */
               const vert_t* restrict pVlst,    /* points */
               body_t* pBlst,     /* bodies */
               const lpindex_t nAs,     /* number of atoms */
               lpindex_t* pprox,  /* x bound. proximity list */
               const lpindex_t nprox,   /* n. of elements in prox.lists */
               const box_t* restrict pbox,       /* the box */
               material_t *pA_mats,
               lpindex_t nA_mats,
               cmprops_t * restrict pcmprops, /* mech props in the system */
               conhash_t * pch,
               const lptimer_t* restrict pt,
               diagnostics_t* pdiag )
{
  /* Loop for all atoms in the boundary proximity list */
lpindex_t nactual = 0;
# ifdef PARALLEL_OPENMP
# pragma omp parallel for reduction( + : nactual )
# endif
  for ( lpindex_t i = 0; i < nprox; ++i )
    {
if ( pprox[i] < nAs )
{
      atom_t* patom = pAlst + pprox[i]; /* atom */

      body_t* pbody = pBlst + ( patom->body_indx ); /* body */

      /* skip transparent to x bodies */
      if ( pbody->flag & BODY_F_NOYBOUND ) continue;

      /* dilation radius */
      real R = patom->R;

      lpindex_t nactual_loc = 0;

      /* Loop for all base points in the atom */
      lpindex_t N;
      N = patom->nverts;

      for ( lpindex_t k = 0; k < N; ++k )
        {
          /* center of base sphere */
          const vert_t* ppoint = pVlst + patom->ivert0 + k;
          real X, C;
          real delta;
          bc_sphere_delta_macro( y );

          /* finding the forces */
          if ( delta > 0 )
            {
              /* calculation of the normal vector from atom to wall */
              real sgn = X < R ? 1.0 : -1.0;
              vec3d_t n;       /* from j (wall) to i (particle) */
              vec3d_set( n, 0, sgn, 0 ); /* n=(+/-1,0,0) */


/* radius from center of atom to the point of contact */
vec3d_t rQ;
vec3d_scale( rQ, n, -R );
/* coordinates of the point of contact */
vec3d_t Q;
vec3d_sum( Q, ppoint->global_pos, rQ );
/* vector from center of mass to the point of contact */
vec3d_t rC;
vec3d_diff( rC, Q, pbody->c );
/* vc = relative velocity at the contact (for particle i) */
vec3d_t vQ;
vec3d_x ( vQ, pbody->w, rC );
vec3d_sum( vQ, vQ, pbody->v );
/* receive ids, j = wall, i = particle, i < j */
int j = pbox->xid;
/* contact hash request for the contact and its update */
lpindex_t nhash = conhash_lookup_and_adjust( pch,
                                             i, j,
                                             pt->n );

/* force and torque calculations */
vec3d_t F, T;
lpindex_t K = isps( patom->material_id, pbc->material_id );
real kni = pcmprops[K].Skni * pow(pbody->m,0.4) * pow(0.5*R,0.3);

conmech_forces( &F, &(pch->cons[nhash].ftau_e),
                delta, &vQ, &n, pcmprops + K, 0.5*R, kni, pt->dt
                );
vec3d_x( T, rC, F );

# if defined( PARALLEL_OPENMP ) && defined( _OPENMP )
# pragma omp critical (bc_critical)
# endif
{
  /* adding forces and torques to the net body */
  vec3d_sum( pbody->f, pbody->f, F );
  vec3d_sum( pbody->t, pbody->t, T );
}

vec3d_assign( pch->cons[nhash].pc, Q );
vec3d_scale( F, F, -1.0 );  /* because we need forces on body i */
vec3d_assign( pch->cons[nhash].f, F );

              nactual_loc += 1;
            }
        }
      nactual += nactual_loc;
}
else /* sph - boundary */
{
  continue;
}
    }
  pdiag -> contacts_actual_n += nactual;
}

void bc_forces_Z
             (
               bc_t* pbc,
               atom_t* pAlst,     /* atoms to traverse */
               const vert_t* restrict pVlst,    /* points */
               body_t* pBlst,     /* bodies */
               const lpindex_t nAs,     /* number of atoms */
               lpindex_t* pprox,  /* x bound. proximity list */
               const lpindex_t nprox,   /* n. of elements in prox.lists */
               const box_t* restrict pbox,       /* the box */
               material_t *pA_mats,
               lpindex_t nA_mats,
               cmprops_t * restrict pcmprops, /* mech props in the system */
               conhash_t * pch,
               const lptimer_t* restrict pt,
               diagnostics_t* pdiag )
{
  /* Loop for all atoms in the boundary proximity list */
lpindex_t nactual = 0;
# ifdef PARALLEL_OPENMP
# pragma omp parallel for reduction( + : nactual )
# endif
  for ( lpindex_t i = 0; i < nprox; ++i )
    {
if ( pprox[i] < nAs )
{

atom_t* patom = pAlst + pprox[i]; /* atom */

body_t* pbody = pBlst + ( patom->body_indx ); /* body */

/* dilation radius */
real R = patom->R;


      lpindex_t nactual_loc = 0;

      /* Loop for all base points in the atom */
      lpindex_t N;
      N = patom->nverts;

      for ( lpindex_t k = 0; k < N; ++k )
        {
          /* center of base sphere */
          const vert_t* ppoint = pVlst + patom->ivert0 + k;
          real X, C;
          real delta;
          bc_sphere_delta_macro( z );

          /* finding the forces */
          if ( delta > 0 )
            {
              /* calculation of the normal vector from atom to wall */
              real sgn = X < R ? 1.0 : -1.0;

/* additional check for open top and bottom */
if ( sgn > 0 && (pbc->flags & BC_F_OPENBOTTOM) )
  continue; /* bottom */
if ( sgn < 0 && (pbc->flags & BC_F_OPENTOP) )
  continue; /* bottom */
              vec3d_t n;       /* from j (wall) to i (particle) */
              vec3d_set( n, 0, 0, sgn ); /* n=(+/-1,0,0) */


/* radius from center of atom to the point of contact */
vec3d_t rQ;
vec3d_scale( rQ, n, -R );
/* coordinates of the point of contact */
vec3d_t Q;
vec3d_sum( Q, ppoint->global_pos, rQ );
/* vector from center of mass to the point of contact */
vec3d_t rC;
vec3d_diff( rC, Q, pbody->c );
/* vc = relative velocity at the contact (for particle i) */
vec3d_t vQ;
vec3d_x ( vQ, pbody->w, rC );
vec3d_sum( vQ, vQ, pbody->v );
/* receive ids, j = wall, i = particle, i < j */
int j = pbox->xid;
/* contact hash request for the contact and its update */
lpindex_t nhash = conhash_lookup_and_adjust( pch,
                                             i, j,
                                             pt->n );

/* force and torque calculations */
vec3d_t F, T;
lpindex_t K = isps( patom->material_id, pbc->material_id );
real kni = pcmprops[K].Skni * pow(pbody->m,0.4) * pow(0.5*R,0.3);

conmech_forces( &F, &(pch->cons[nhash].ftau_e),
                delta, &vQ, &n, pcmprops + K, 0.5*R, kni, pt->dt
                );
vec3d_x( T, rC, F );

# if defined( PARALLEL_OPENMP ) && defined( _OPENMP )
# pragma omp critical (bc_critical)
# endif
{
  /* adding forces and torques to the net body */
  vec3d_sum( pbody->f, pbody->f, F );
  vec3d_sum( pbody->t, pbody->t, T );
}

vec3d_assign( pch->cons[nhash].pc, Q );
vec3d_scale( F, F, -1.0 );  /* because we need forces on body i */
vec3d_assign( pch->cons[nhash].f, F );

              nactual_loc += 1;
            }
        }
      nactual += nactual_loc;
}
else /* sph - boundary */
{
  continue;
}
    }
  pdiag -> contacts_actual_n += nactual;
}

void bc_forces( bc_t* pbc,         /* boundary condition structure */
                atom_t* pAlst,     /* atoms to traverse */
                vert_t* pVlst,    /* points */
                body_t* pBlst,     /* bodies */
                lpindex_t nAs,     /* number of atoms */

                lpindex_t* pxprox, /* x bound. proximity list */
                lpindex_t* pyprox, /* y bound. proximity list */
                lpindex_t* pzprox, /* z bound. proximity list */
                lpindex_t nxprox,  /* n. of elements in prox.lists */
                lpindex_t nyprox,
                lpindex_t nzprox,

                box_t* pbox,       /* the box */
                material_t *pA_mats,
                lpindex_t nA_mats,
                cmprops_t * pcmprops,  /* list of contact mech props */
                conhash_t * pch,
                lptimer_t * pt,
                diagnostics_t* pdiag )
{
  bc_forces_X( pbc, pAlst, pVlst, pBlst, nAs,
               pxprox, nxprox,
               pbox, pA_mats, nA_mats, pcmprops, pch, pt, pdiag );

  bc_forces_Y( pbc, pAlst, pVlst, pBlst, nAs,
               pyprox, nyprox,
               pbox, pA_mats, nA_mats, pcmprops, pch, pt, pdiag );

  bc_forces_Z( pbc, pAlst, pVlst, pBlst, nAs,
               pzprox, nzprox,
               pbox, pA_mats, nA_mats, pcmprops, pch, pt, pdiag );

  pdiag -> contacts_potential_n += nxprox + nyprox + nzprox;
}

void bc_velocities( bc_t* pbc,       /* boundary cond. struct */
                    body_t* pBlst,   /* bodies */
                    lpindex_t nBs,   /* number of bodies */
                    box_t* pbox )    /* the box */

{
  /* only if necessary: set the outside velocity */
  if ( pbc->flags & BC_F_ZOUTVEL )
    for( lpindex_t i = 0; i < nBs; ++i )
      {
        body_t* pbody = pBlst + i;
        if ( vec3d_getz( pbody -> c ) < 0 ||
             vec3d_getz( pbox->fcorner ) < vec3d_getz( pbody -> c ) )
          {
            vec3d_setz( pbody->v, pbc->zout_vel );
          }
      }
}
