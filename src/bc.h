/*!

  \file bc.h

  \brief Boundary conditions on the box

*/

#ifndef BC_H
#define BC_H

#include <math.h>               /* for math */
#include <stdio.h>              /* for printf/fprintf */
#include <stdlib.h>             /* for exit */

#include <lua.h>                /* lua stuff */
#include <lauxlib.h>
#include <lualib.h>

#include "defs.h"               /* must for macros */
#include "box.h"                /* box is box */
#include "vec3d.h"              /* vec3d type */
#include "lptimer.h"            /* timer */
#include "body.h"               /* body_t */
#include "atom.h"               /* atom_t */
#include "conmech.h"            /* contact mechanics */
#include "lpdyna.h"             /* dynamic array lpdyna_t */
#include "coldet.h"             /* collision_t */
#include "conhash.h"            /* contact hash */

#include "hdis-types.h"        /* types */


#define BC_F_BCTYPE_REFLECTION   0x1
#define BC_F_BCTYPE_HERTZIAN     0x2

/* Z boundary condition special case: open top or bottom */
#define BC_F_OPENTOP             0x4
#define BC_F_OPENBOTTOM          0x8

/* Velocity outside of the box is fixed along z */
#define BC_F_ZOUTVEL            0x10

enum {
  BC_F_PERIODIC = 0x20,
};

typedef struct bc_s
{
  real zout_vel;          /* velocity outside of the z-domain */
  lpindex_t material_id;  /* index of the material */
  unsigned int flags;     /* flags describing the boundary */
} bc_t;


void bc_forces( bc_t* pbc,         /* boundary condition structure */
                atom_t* pAlst,     /* atoms to traverse */
                vert_t* pVlst,    /* points */
                body_t* pBlst,     /* bodies */
                lpindex_t nAs,     /* number of atoms */

                lpindex_t* pxprox, /* x bound. proximity list */
                lpindex_t* pyprox, /* y bound. proximity list */
                lpindex_t* pzprox, /* z bound. proximity list */
                lpindex_t nxprox,  /* n. of elements in prox.lists */
                lpindex_t nyprox,
                lpindex_t nzprox,

                box_t* pbox,       /* the box */
                material_t *pA_mats,
                lpindex_t nA_mats,
                cmprops_t * pcmprops,  /* list of contact mech props */
                conhash_t * pch,
                lptimer_t * pt,
                diagnostics_t* pdiag )
                         ;

void bc_velocities( bc_t* pbc,       /* boundary cond. struct */
                    body_t* pBlst,   /* bodies */
                    lpindex_t nBs,   /* number of bodies */
                    box_t* pbox )    /* the box */

                             ;

void bc_sph_velocities( box_t* pbox,
                        sph_t* pHlst,
                        lpindex_t nAs,
                        lpindex_t* pxprox,
                        lpindex_t* pyprox,
                        lpindex_t* pzprox,
                        lpindex_t nxprox,
                        lpindex_t nyprox,
                        lpindex_t nzprox );

#endif  /* BC_H */
