/*!
  
  \file timerec.h

  \brief Simple time recording class based on sys/time.h POSIX
  function gettimeofday

 */

#ifndef TIMEREC_H
#define TIMEREC_H

#include <sys/time.h>

/*! \brief main class structure  */
typedef struct timerec_s
{
  struct timeval tv;            /*!< row time values structure */
  double start;                 /*!< stores init time as double in s */
} timerec_t;

/*! \brief save the current time, start timer  */
void timerec_init( timerec_t* ptt );

/*! \brief measure lapse \return lapsed time in s */
double timerec_lapsed( timerec_t* ptt );

/*! \brief runnign macro for the values needed tracking */
#define TIMEREC_RECORD( ptimerec, var, ... )              \
  {                                                       \
    timerec_init( ptimerec );                             \
    __VA_ARGS__;                                          \
    var += timerec_lapsed( ptimerec );                    \
  }

#endif
