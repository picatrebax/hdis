/*!

  \file conhash.h

  \brief Contact hash

*/

#ifndef CONHASH_H
#define CONHASH_H
#include <stdlib.h>
#include <assert.h>

#include "defs.h"
#include "vec3d.h"


#define CONHASH_ERROR_INSERT (-1)
#define CONHASH_ERROR_DELETE (-1)
#define CONHASH_ERROR_LOOKUP (-1)

#define CONHASH_F_FREE       0
#define CONHASH_F_OCCUPIED   1
#define CONHASH_F_VACANT     2

/* default multiplier of number of atoms to
   calcluate the capacity of the table */
#define CONHASH_CAPACITY_MULT 20

/* number of probes to determine
how contact hash is efficient */
#define CONHASH_NPROBES 20

/* unsuccessful lookup mises threashold: the limit
   when the contact hash considered bloated and
   need to be cleaned */
#define CONHASH_ULMT     3



typedef struct coninfo_s
{
  vec3d_t ftau_e;      /* elastic tangent force on the contact */
  vec3d_t ftau_i;      /* inelastic tangent force on the contact */

  vec3d_t pc;          /* point of contact coordinates */
  vec3d_t f;           /* total force on contact (from i to j) */

  real delta;          /* overlap between atoms */

  lpindex_t id1;                /* ids of contacting objects */
  lpindex_t id2;

  lpindex_t nlast;     /* last timestep the contact was updated */

  int flag;                     /* flag stores some information
                                   regarding this cell in the
                                   table. It is 0 for free cell.
                                */
} coninfo_t;

typedef struct conhash_s
{
  lpindex_t maxn;               /* maximum number of elements */
  coninfo_t *cons;              /* contacts, main data */
} conhash_t;

/*! Structure duplicates confinfo_t up to nlast and flag, it is used
    only to store contact list in HDF5 file and read it back. Then,
    coninfo_t is used to store the contacts in hash */
typedef struct contact_s
{
  vec3d_t ftau_e;      /* elastic tangent force on the contact */
  vec3d_t ftau_i;      /* inelastic tangent force on the contact */
  vec3d_t pc;          /* contact point coordinates */
  vec3d_t f;           /* total force from i to j */
  real delta;          /* overlap between atoms */

  lpindex_t id1;                /* ids of contacting objects */
  lpindex_t id2;

} contact_t;

inline static unsigned int conhash_hash_oat ( lpindex_t id1, lpindex_t id2 )
{
  const int llen = (int) sizeof( lpindex_t );
  unsigned char *p;
  unsigned int h = 0;

  p = (unsigned char*)&id1;
  for ( int i = 0; i < llen; i++ )
    {
      h += p[i];
      h += ( h << 10 );
      h ^= ( h >> 6 );
    }

  p = (unsigned char*)&id2;
  for ( int i = 0; i < llen; i++ )
    {
      h += p[i];
      h += ( h << 10 );
      h ^= ( h >> 6 );
    }

  h += ( h << 3 );
  h ^= ( h >> 11 );
  h += ( h << 15 );

  return h;
}

#define CONHASH_SHIFT_LEFT (6) /* default shift */

/* cyclic shift of int type */
#define conhash_int_cycle_shift(x,n)  \
  ( (unsigned int)(x)<<(n) ^ (unsigned int)(x)>>( 8*sizeof(int) - (n) ) )

inline static unsigned int
              conhash_hash_cyclic( lpindex_t id1, lpindex_t id2 )
{
  return  conhash_int_cycle_shift( id1, CONHASH_SHIFT_LEFT ) ^ (id2);
}

#define FNV_BASE_32 2166136261
#define FNV_PRIME_32 16777619

inline static unsigned int conhash_hash_fnv( lpindex_t id1, lpindex_t id2 )
{
  unsigned char *p;
  unsigned int h = FNV_BASE_32;

  p = (unsigned char*) &id1;
  for ( int i = 0; i < (int)sizeof( lpindex_t ); ++ i )
    h = ( h * FNV_PRIME_32 ) ^ p[i];

  p = (unsigned char*) &id2;
  for ( int i = 0; i < (int)sizeof( lpindex_t ); ++ i )
    h = ( h * FNV_PRIME_32 ) ^ p[i];

  return h;
}

#define conhash_hash_fnv2(id1,id2) \
  ( ( ( FNV_BASE_32 * FNV_PRIME_32 ) ^ (id1) ) * FNV_PRIME_32 ) ^ (id2);

#define conhash_hash_fnv3(id1,id2) \
  ( ( ( ( FNV_BASE_32  ^ (id1) ) * FNV_PRIME_32 )  ^ (id2) ) * FNV_PRIME_32 );


/* coerce of two ids is XOR with a shifted value */
#define conhash_hash1( id1, id2 ) conhash_hash_fnv( id1, id2 )

#define conhash_hash2(id1,id2,N) ( 1 + ( id1 ^ id2 ) % ( N - 1 ) )

int is_prime( lpindex_t n );

inline static double uniform_deviate ( int seed )
{
  return seed * ( 1.0 / ( RAND_MAX + 1.0 ) );
}


coninfo_t* conhash_init( conhash_t* pch, const lpindex_t maxn );

void conhash_destroy( conhash_t* pch );

lpindex_t conhash_ins( conhash_t* pch,
                       const coninfo_t* pel );

lpindex_t conhash_size( conhash_t* pch );

#define conhash_load(pch) \
  ( ( (double)conhash_size((pch)) )/ ( (pch)->maxn ) )

lpindex_t conhash_del( conhash_t* pch,
                       const lpindex_t id1, const lpindex_t id2 );

lpindex_t conhash_lookup( conhash_t * pch,
                          const lpindex_t id1, const lpindex_t id2 );

lpindex_t conhash_lookup_and_adjust( conhash_t* pch,
                                     lpindex_t i, lpindex_t j,
                                     size_t ncurrent )
                                     ;

void conhash_lookup_success_cost( conhash_t* pch,
                           double* pload,
                           double* pmisses,
                           lpindex_t* pNvac,
                           lpindex_t* pNfree );

double conhash_lookup_miss_cost( conhash_t* pch,
                                 lpindex_t reps,
                                 lpindex_t range );


void conhash_cleanup( conhash_t* pch );
int conhash_nocontacts_clean( conhash_t* pch, size_t ncurrent );


contact_t *conhash_conlist_make_alloc( conhash_t* pch,
                                       const size_t ncurrent,
                                       size_t* pncs )
                                     ;

#endif
