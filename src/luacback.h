/*!

  \file luacback.h

  \brief Standard callbacks description. Functions that are
  conditionally called from Lua configuration scripts to exchange data
  between Lua and C core.

*/

#ifndef LUACBACK_H
#define LUACBACK_H

#include <stdio.h>              /* for printf/fprintf */
#include <stdlib.h>             /* for exit */
#include <string.h>             /* for strncpy */

#include <lua.h>                /* lua stuff */
#include <lauxlib.h>
#include <lualib.h>

#include "defs.h"               /* must for macros */
#include "box.h"                /* box is box */
#include "vec3d.h"              /* vec3d type */
#include "lptimer.h"            /* timer */
#include "body.h"               /* body_t */
#include "atom.h"               /* atom_t */
#include "conmech.h"            /* contact mechanics */
#include "conhash.h"            /* hash table for contacts */
#include "lpdyna.h"             /* dynamic array lpdyna_t */
#include "coldet.h"             /* collision_t */
#include "bc.h"                 /* bc_t */

#include "hdis-types.h"
#include "hdis.h"
#include "profile.h"

/* Return codes */
#define LUACBACK_OK          0  /*!< No error msg  */
#define LUACBACK_UNDEFINED   1  /*!< Function is undefined  */
#define LUACBACK_ERROR_PCALL 2  /*!< pcall error msg  */

/* Flags */
#define LUACBACK_F_DEFINED_PRETIMESTEP        0x1
#define LUACBACK_F_DEFINED_AFTERTIMESTEP      0x2
#define LUACBACK_F_DEFINED_PRESAVE            0x4
#define LUACBACK_F_DEFINED_AFTERSAVE          0x8
#define LUACBACK_F_DEFINED_G                  0x10
#define LUACBACK_F_DEFINED_CONTACT_DETECTION  0x20
#define LUACBACK_F_DEFINED_ENERGY             0x40
#define LUACBACK_F_DEFINED_DIAGNOSTICS        0x80
#define LUACBACK_F_DEFINED_PROFILE            0x100
#define LUACBACK_F_DEFINED_DT                 0x200
#define LUACBACK_F_DEFINED_FINALIZE           0x400

/* run macro: runs luca callback function only if it was defined
   before. Functions supposed to be defined by default. Once it is
   determined to be undefined, no calls occur */
#define LUACBACK_RUN(F,flag,flagvalue)                    \
  if ( flag & flagvalue )                                 \
    {                                                     \
     int errcode = F;                                     \
      if ( errcode == LUACBACK_UNDEFINED )                \
        flag = flag & (~flagvalue);                       \
      else if ( errcode == LUACBACK_ERROR_PCALL )         \
        {                                                 \
          const char* perrmsg = lua_tostring( L, -1 );    \
          fatal( 1, "Function %s failed to run\n\n"       \
                 "Lua error message:\n\n%s\n",            \
                 #F, perrmsg );                           \
        }                                                 \
    }

int luacback_pretimestep( lua_State* L,
                          const lptimer_t* restrict pt );

int luacback_aftertimestep( lua_State* L,
                            const lptimer_t* restrict pt,
                            char* restrict filename,
                            unsigned int* fsave,
                            unsigned int* fhalt );

int luacback_presave( lua_State* L, 
                      char* restrict filename, 
                      const lptimer_t* restrict pt );

int luacback_aftersave( lua_State* L, 
                        char* restrict filename, 
                        const lptimer_t* restrict pt,
                        const conhash_comp_t* pch_comp );

int luacback_g( lua_State* L,
                const lptimer_t* restrict pt,
                vec3d_t* restrict pg );

int luacback_contact_detection( lua_State* L,
                                const lptimer_t* restrict pt,
                                int* restrict pf_cdrun );

int luacback_diagnostics( lua_State* L,
                          const lptimer_t* restrict pt,
                          diagnostics_t* restrict pdiag );

int luacback_profile( lua_State* L,
                      const lptimer_t* restrict pt,
                      profile_t* restrict pprof );

/*! \brief Callback function to return integral energy values to Lua
    script */
int luacback_energy( lua_State* L,
                     const lptimer_t* restrict pt,
                     const real etmax,
                     const real ermax,
                     const real Et, 
                     const real Er, 
                     const real Ep );

/*! \brief Call back function to CHANGE the time step */
int luacback_dt( lua_State* L, lptimer_t* pt, const real vc_sq_max );

/*! \brief Call back function to call right after the time loop */
int luacback_finalize( lua_State* L, const lptimer_t* restrict pt,
                       char* restrict filename,
                       bool* fsave );

#endif

