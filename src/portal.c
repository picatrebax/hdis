/*!

  \file portal.c

  \brief Implementation of portal

*/

#include "portal.h"

void portal_dem_teleport( portal_t* pportal,
                          const sbb_t* restrict psbb,
                          body_t* restrict pBlst,
                          const lpindex_t nAs )
{
  /* no portal if not set */
  if ( !( pportal->flag & PORTAL_F_OPEN ) ) return;
  
  for ( lpindex_t k = 0; k < nAs; ++k )
    {
      lpindex_t ib = psbb[k].body_indx;

      if ( ( pBlst[ib].flag & BODY_F_TELEPORT ) &&
           ( vec3d_gety( psbb[k].c ) + psbb[k].r > pportal->entrance_y ) )
        {
          /* teleporting */
          vec3d_sety( pBlst[ib].c, pportal->exit_y );
          vec3d_setz( pBlst[ib].c, pportal->exit_z );

          /* setting velocities */
          if ( pportal->flag & PORTAL_F_SET_V )
            vec3d_assign( pBlst[ib].v, pportal->v );
          if ( pportal->flag & PORTAL_F_SET_W )
            vec3d_assign( pBlst[ib].v, pportal->w );
        }
    }
}
