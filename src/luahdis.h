#ifndef LUAHDIS_H
#define LUAHDIS_H

#include <string.h>             /* operations on the strings */
#include <stdbool.h>

#include <lua.h>                /* lua stuff */
#include <lauxlib.h>
#include <lualib.h>

#include "defs.h"               /* must for macros */
#include "vec3d.h"              /* vec3d type */
#include "rquat.h"              /* quaternions */
#include "rot3d.h"              /* rotation matrix */

int lua_hdis_get_real_field  ( lua_State* L, const char* name, real* pval );
int lua_hdis_get_int_field   ( lua_State* L, const char* name, int *pval );
int lua_hdis_get_bool_field  ( lua_State* L, const char* name, bool* pval );
lpindex_t lua_hdis_get_length( lua_State* L, const char* table );
int lua_hdis_get_vec3d       ( lua_State* L, const char* name, vec3d_t* v );
int lua_hdis_get_rquat       ( lua_State* L, const char* name, rquat_t* v );

#define LUA_GET_VQ_BY_NAME( type, sname, vname )        \
{                                                       \
  int success;                                          \
                                                        \
  success = lua_hdis_get_ ## type( L,                  \
                                    #vname,             \
                                    &(sname.vname) );   \
  if ( !success )                                       \
    {                                                   \
      fatal( 1, "Error reading %s %s"                   \
             " from bodies or atoms, index %ld\n",      \
             #type, #vname,                             \
             (long)lua_ind );                           \
    }                                                   \
}

#endif      /* LUAHDIS_H */

