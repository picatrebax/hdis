/*!

  \file dynamics.h

  \brief Dynamics module: it calculates velocity and angular velocity
  for each rigid body based on all net froces and torques calcluated
  on pervious steps.

*/

#ifndef DYNAMICS_H
#define DYNAMICS_H


#include <lua.h>                /* lua stuff */
#include <lauxlib.h>
#include <lualib.h>

#include "defs.h"               /* must for macros */
#include "box.h"                /* box is box */
#include "vec3d.h"              /* vec3d type */
#include "rquat.h"              /* quaternions */
#include "rot3d.h"              /* rotation matrix */
#include "lptimer.h"            /* timer */
#include "body.h"               /* body_t */
#include "atom.h"               /* atom_t */
#include "sph.h"
#include "hdis.h"              /* Flag values */


#define DYNAMICS_WALTON_MAXN 5  /*!< max interations  */

void dynamics( body_t* restrict pBlst,
               const lpindex_t nBs,
               lua_State *L,
               const real dt,
               const int flags );

#endif
