/*!

  \file sphkernel.h

  \brief Collection of kernel cores "w" and their derivatives.

  Collection  of kernel  cores "w"  and their  derivatives. The  kernel and  its
  gradient can later be computed as follows:

  W(r,h) = 1/h^3 * w(q) = 1/h^3 * w( |r|/h )

  grad W(r,h) = 1/h^4 * w'(q) * r/|r|

*/

#ifndef SPHKERNEL_H
#define SPHKERNEL_H

#include "defs.h"

/* --------------------------------------------------------------------------- */

static inline real sphkernel_w4 ( real q )
{
  assert( q >= 0 );

  real w = 0;
  
  if ( q < 1.0 )
    {
      w = 4 + q * q * ( 3 * q - 6 );
    }
  else if ( q < 2.0 )
    {
      w = ( 2 - q ) * ( 2 - q ) * ( 2 - q );
    }

  return w / ( 4 * M_PI );
}

/* --------------------------------------------------------------------------- */

/* dw4/dq */

static inline real sphkernel_dw4 ( real q )
{
  assert( q >= 0 );

  real dw = 0;
  
  if ( q < 1.0 )
    {
      dw = q * ( 9 * q - 12 );
    }
  else if ( q < 2.0 )
    {
      dw = ( 3 * q - 6 ) * ( 2 - q );
    }

  return dw / ( 4 * M_PI );
}

/* --------------------------------------------------------------------------- */
/* --------------------------------------------------------------------------- */

static inline real sphkernel_w5 ( real q )
{
  real w = 0.0;

  if ( q < 0.5 )
    {
      w = pow( 2.5 - q, 4.0 ) - 5.0 * pow( 1.5 - q, 4.0 )
        + 10.0 * pow( 0.5 - q, 4.0 );
    }
  else if ( q < 1.5 )
    {
      w = pow( 2.5 - q, 4.0 ) - 5.0 * pow( 1.5 - q, 4.0 );
    }
  else if ( q < 2.5 )
    {
      w = pow( 2.5 - q, 4.0 );
    }

  return w / ( 20 * M_PI );
}

/* --------------------------------------------------------------------------- */

static inline real sphkernel_dw5 ( real q )
{
  const real k = 1.0 / ( 5 * M_PI );
  real dw = 0;

  if ( q < 0.5 )
    {
      dw = k * ( pow( 2.5 - q, 3.0 ) - 5.0 * pow( 1.5 - q, 3.0 )
                 + 10.0 * pow( 0.5 - q, 3.0 ) );
    }
  else if ( q < 1.5 )
    {
      dw = k * ( pow( 2.5 - q, 3.0 ) - 5.0 * pow( 1.5 - q, 3.0 ) );
    }
  else if ( q < 2.5 )
    {
      dw = k * pow( 2.5 - q, 3.0 );
    }

  return dw;
}

/* --------------------------------------------------------------------------- */
/* Wendland kernel. Manchester thesis, p.54                                    */
/* --------------------------------------------------------------------------- */

static inline real sphkernel_wendland_w ( real q )
{
  assert( q >= 0 );
  real w = 0;

  if ( q < 2.0 )
    {
      real q1 = 2 - q;
      real q2 = 1 + 2 * q;
      w = ( 21 / ( 256 * M_PI ) ) * q1 * q1 * q1 * q1 * q2;
    }

  return w;
}

/* --------------------------------------------------------------------------- */

static inline real sphkernel_wendland_dw ( real q )
{
  assert( q > 0 );
  real dw = 0;

  if ( q < 2.0 )
    {
      real q1 = 2 - q;
      dw = ( - 210 / ( 256 * M_PI ) ) * q * q1 * q1 * q1;
    }

  return dw;
}


#endif      /* SPHKERNEL_H */
