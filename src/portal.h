/*!

  \file portal.h

  \brief Portal for DEM particles motion

*/

#ifndef PORTAL_H
#define PORTAL_H

#include "atom.h"
#include "body.h"
#include "bb.h"

#define PORTAL_F_CLOSED 0x0     /*!< portal does not exist  */
#define PORTAL_F_OPEN   0x1     /*!< portal is open (exists)  */
#define PORTAL_F_SET_V  0x2     /*!< exit velocity is set */
#define PORTAL_F_SET_W  0x4     /*!< exit ang. velocity is set */

/*! \brief DEM transposition along y axis with height change  */
typedef struct portal_s {
  vec3d_t v;                /*!< initial linear velocity  */
  vec3d_t w;                /*!< initial angular velocity */
  
  real entrance_y;          /*!< level at where any sbb crossing => body
                                   teleporting */
  real exit_y;              /*!< where the center of the body appears  */
  real exit_z;              /*!< z where the center appears  */

  int  flag;                /*!< Described above, 0 = portal is closed */
} portal_t;

/*! \brief Check if the body gets to the portal and teleport it if needed, need
  to be called after body placement but before all other placements */
void portal_dem_teleport( portal_t* pportal,
                          const sbb_t* restrict psbb,
                          body_t* restrict pBlst,
                          const lpindex_t nAs );

#endif      /* PORTAL_H */
