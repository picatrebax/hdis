/*!

  \file atom.h

  \brief Atom main type

*/

#ifndef ATOM_H
#define ATOM_H

#include <stdlib.h>             /* for calloc */
#include <stdbool.h>

#include "vec3d.h"
#include "defs.h"               /* definitions */

typedef struct vert_s {
  vec3d_t local_pos;   /* body frame coords */
  vec3d_t global_pos;  /* global frame coords */
  int deg;             /* number of adjacent edges */
  lpindex_t iadj_e;    /* index into Jlst of the first adjacent
                          edge index */
  lpindex_t ibody;     /* for updating global_pos as body moves */
} vert_t;

typedef struct atom_s {
  real R;                    /* dilation radius */
  real ri;                   /* interaction radius */
  lpindex_t body_indx;       /* body this atom belongs to */
  lpindex_t group_id;        /* atom group ID */
  lpindex_t material_id;     /* atom material ID */
  lpindex_t nverts;          /* for boundary condition stuff */
  lpindex_t nedges;          /* number of edges */
  lpindex_t nfaces;          /* number of faces */
  lpindex_t ivert0;          /* index into Vlst of the first vertex */
  lpindex_t iedge0;          /* index into Vlst of the first vertex */
  lpindex_t iface0;          /* index into Vlst of the first vertex */
  vec3d_t sbb_c;             /* local (body frame) coords of SBB center */
  real sbb_Rmin;             /* minimum radius of the bounding sphere */
  int isolated;              /* flag for how to process neighbors */
} atom_t;

typedef struct edge_s {
  lpindex_t itail; /* index into Vlst of the tail vertex */
  lpindex_t ihead; /* index into Vlst of the head vertex */
  lpindex_t ilface; /* index into Flst of the left face */
  lpindex_t irface; /* index into Flst of the right face */
} edge_t;

typedef struct face_s {
  vec3d_t norm; /* */
  int iedge0; /* */
  int iedge1; /* */
  int iedge2; /* */
  int ivert0; /* */
  int ivert1; /* */
  int ivert2; /* */
} face_t;

#endif  /* ATOM_H */
