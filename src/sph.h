/*!

  \file sph.h

  \brief SPH (H) structure definition and main methods.

*/

#ifndef SPH_H
#define SPH_H

#include <stdint.h>

#include "defs.h"
#include "vec3d.h"
#include "lpdyna.h"
#include "box.h"
#include "sph_omp.h"

enum sph_constants {
  //  SPH = 1,
  SPH_NO_INTERACTION = 1,

  /* flags */
  SPH_F_FREE     = 0x1,         /*!< free particle */
  SPH_F_BOUNDARY = 0x2,         /*!< boundary particle */
  SPH_F_STILL    = 0x4,         /*!< still, not moving particle */

  SPH_F_HAS_CLONE = 0x10,       /*!< there is a virtual particle  */
  SPH_F_CLONE = 0x20            /*!< virtual particle  */
};

/* The properties that determines the liquid behavior */
typedef struct sph_props_s {
  real rho0;                    /*!< kg/m^3, target density  */
  real c0;                      /*!< m/s, sound velocity for target density */
  real c2;                      /*!< m^2/s^2, square of target velocity:
                                   computed */
  real gamma;                   /*!< 1, adiabatic power in p(rho) law */
  real eps;                     /*!< 1, reserved and not used for velocity
                                   correction in placement procedure */
  real alpha;                   /*!< 1, artificial viscosity constant */
} sph_props_t;

typedef struct sph_s {
  vec3d_t c;                    /* location, global */
  vec3d_t v;                    /* velocity, m/s */
  vec3d_t f;                    /* force, N */

  real j;                       /* mass flux, kg*m/s */

  real rho;                     /* computed density */
  real p;                       /* pressure */

  real nu;                      /* viscous coefficient */
  real m;                       /* mass */
  real R;                       /* radius */
  real h;                       /* interaction length = R only for M4 */
  real a;                       /* sound velocity */

  lpindex_t group_id;           /* index of the group */
  lpindex_t clone_id;           /*!< index of the clone */

  int flags;                    /*!< state of the particles  */
} sph_t;

/*! Reset forces to 0, densities to rho0 */
void sph_reset( sph_t *pHlst, lpindex_t nHs );

/*! Compute mass streams for density update */
void sph_mstream( lpdyna_t* pcols,
                  lpindex_t nAs,
                  sph_t* pHlst,
                  lpindex_t nHs,
                  sph_omp_t* pomp );

/*! collect mstream data (only for parallel computations)  */
void sph_mstream_collect( sph_t* pHlst, lpindex_t nHs, sph_omp_t* pomp );

/*! this is divergence density computations from continuity equation */
void sph_density( sph_t* pHlst,
                  const lpindex_t nHs,
                  const real dt );

/*! Classic density computations: needed for initial density distribution */
void sph_density_sum( lpdyna_t* pcols,
                      lpindex_t nAs,
                      sph_t* pHlst,
                      lpindex_t nHs,
                      sph_props_t* pprops);

void sph_pressure( sph_t *pHlst, lpindex_t nHs, sph_props_t* props );

void sph_gravity( sph_t * restrict pHlst,
                  const lpindex_t nHs,
                  const vec3d_t* restrict g  );

void sph_bc( box_t* pbox,
             sph_t* pHlst,
             lpindex_t nAs,
             lpindex_t* pxprox,
             lpindex_t* pyprox,
             lpindex_t* pzprox,
             lpindex_t nxprox,
             lpindex_t nyprox,
             lpindex_t nzprox );

void sph_forces( lpdyna_t* pcols,
                 lpindex_t nAs,
                 sph_t* pHlst,
                 lpindex_t nHs,
                 sph_props_t* props,
                 real dt,
                 sph_omp_t* pomp );

void sph_forces_collect( sph_t * restrict pHlst, const lpindex_t nHs,
                         sph_omp_t *pomp );


void sph_dynamics( sph_t* pHlst, const lpindex_t nHs, const real dt );

void sph_placement( sph_t* pHlst,
                    const lpindex_t nHs,
                    const real dt );

/* moving sph if they got into the margins */
void sph_teleport( sph_t *pHlst, lpindex_t nHs, box_t *pbox,
                   real l0,     /* left in-boundary */
                   real l1,     /* right in-boundary */
                   real L       /* length l1-l0 */
                   );

#endif      /* SPH_H */
