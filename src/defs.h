/*!

  \file defs.h

  \brief Global definitions  

 */

#ifndef DEFS_H
#define DEFS_H

#include <stdio.h>
#include <stdlib.h>
#include <float.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#if LUA_VERSION_NUM == 502 || LUA_VERSION_NUM == 503
#  define lua_objlen(L,i)         lua_rawlen(L, (i))
#endif

/* ----------------------------------------------------------------- */

#ifdef REAL_IS_FLOAT
typedef float real;
#define DEFS_INFINITY FLT_MAX
#else
typedef double real;
#define DEFS_INFINITY DBL_MAX
#endif

/* ----------------------------------------------------------------- */

#if defined( INDEX_IS_SIZE_T )

#include <stdlib.h>
typedef size_t lpindex_t;
#define LPINDEX_MAX ULONG_MAX
#define LPINDEX_MIN ULONG_MIN

#elif defined( INDEX_IS_UNSIGNED_LONG )

typedef unsigned long lpindex_t;
#define LPINDEX_MAX ULONG_MAX
#define LPINDEX_MIN ULONG_MIN

#elif defined( INDEX_IS_UNSIGNED_INT )

typedef unsigned int lpindex_t;
#define LPINDEX_MAX UINT_MAX
#define LPINDEX_MIN UINT_MIN

#else

typedef int lpindex_t;
#define LPINDEX_MAX INT_MAX
#define LPINDEX_MIN UINT_MIN

#endif

/* ----------------------------------------------------------------- */

#define FILENAME_MAXLEN 255

/* ----------------------------------------------------------------- */

#define fatal( errcode, ... )                                        \
  {                                                                  \
    fprintf( stderr, __FILE__ ":%d\n", __LINE__ );                   \
    fprintf( stderr, "    Error %d at function %s:\n",               \
             errcode, __func__ );                                    \
    fprintf( stderr, "    " __VA_ARGS__ );                           \
    fprintf( stderr, "\n" );                                         \
    exit( errcode );                                                 \
  }

/* ----------------------------------------------------------------- */

#define warning( ... )                                                \
  {                                                                   \
    fprintf( stderr, __FILE__ ":%d\n", __LINE__ );                    \
    fprintf( stderr, "    Warning at function %s:\n", __func__ );     \
    fprintf( stderr, "    " __VA_ARGS__ );                            \
    fprintf( stderr, "\n" );                                          \
  }

/* ----------------------------------------------------------------- */

#define info( fp, flag )                                          \
  {                                                               \
  if( flag )                                                      \
    {                                                             \
      fprintf( fp,                                                \
               PACKAGE_STRING "    (branch: " PROGRAM_BRANCH ")"  \
               "\n"                                               \
               PROGRAM_DESCRIPTION "\n\n"                         \
               "Author: Anton Kulchitsky\n"  \
               "Copyright (C) 2010--2018 UAF,\n"                  \
           "Check COPYING file for the license\n"                 \
           "There is NO warranty;"                                \
           " not even for MERCHANTABILITY\n"                      \
               " or FITNESS FOR A PARTICULAR PURPOSE.\n\n"        \
               );                                                 \
    }                                                             \
    else                                                          \
      fprintf( fp,                                                \
               PACKAGE_STRING "    (branch: " PROGRAM_BRANCH ")"  \
               "\n"                                               \
               "Author: Anton Kulchitsky\n"  \
               PROGRAM_DESCRIPTION "\n\n" );                      \
                                                                  \
  }

/* ----------------------------------------------------------------- */

#define usage( fp )                                                  \
{                                                                    \
  fprintf( fp,                                                       \
           "Usage: %s [OPTIONS] [LUA-FILE]\n"                        \
           "\n"                                                      \
           "Run %s DEM using configuration/script"                   \
           " LUA-FILE (default is %s.lua)"                           \
           "\n\n"                                                    \
           "Operation modes:\n"                                      \
           "     -h           print this help, then exit\n"          \
           "     -V           print version number, then exit\n\n"   \
           "Reprot bugs to %s\n",                                    \
           PACKAGE_TARNAME, PACKAGE_NAME,                            \
           PACKAGE_TARNAME, PACKAGE_BUGREPORT );                     \
}

/* ----------------------------------------------------------------- */

#define isps(i,j) \
    ( (i) <= (j) ? i + (((j)*((j)+1))>>1) : j + (((i)*((i)+1))>>1) )

/* ----------------------------------------------------------------- */

#ifndef __cplusplus
#define swap(a,b)                    \
  {                                  \
    typeof(a) buff = (a);            \
    (a) = (b);                       \
    (b) = buff;                      \
  }
#endif

/* ----------------------------------------------------------------- */

/* default value for old overlap to ensure non-skip distance
   calculation on the next time step in forces_bb function */
#define DEFS_DEFAULT_ATOM_POSITIVE_DELTA (1.0)

/* -------------------------------------------------------------------------- */

/* print binary flag as binary number */

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0') 

inline static void f_print( unsigned int n )
{
  printf( BYTE_TO_BINARY_PATTERN BYTE_TO_BINARY_PATTERN,
          BYTE_TO_BINARY(n>>8), BYTE_TO_BINARY(n) );
}
  
/* a few self explanatory logic operations on flags */

#define f_is_set(     flag, value ) ( (flag)&(value) )
#define f_is_dropped( flag, value ) ( ((flag)&(value))==0 )

#define f_set(    flag, value ) (flag |= (value))
#define f_drop(   flag, value ) (flag &= ~(value))
#define f_toggle( flag, value ) (flag ^= (value))
#define f_dropall( flag )       (flag = 0)


#endif  /* DEFS_H */
