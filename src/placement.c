/*!

  \file placement.c

  \brief Changing the position of different elements according to
  their velocitites and angular velocities of the bodies:
  implementation.

*/

#include "placement.h"

/* ----------------------------------------------------------------- */

/*! Function to report the body state to Lua script  */
static void monitor_body( lua_State *L,
                          const lpindex_t i,
                          const body_t* restrict pbody )
{
  /* get the monitor_functions array */
  lua_getglobal( L, "hdis" );
  lua_getfield( L, -1, "monitor_functions" );
  if ( !lua_istable( L, 1 ) )
    fatal( 1001,
           "Body %d error, no list of monitor_functions", i+1 );

  lua_rawgeti( L, -1, pbody->imonitor );

  if ( ! lua_isfunction( L, -1 ) )
    fatal( 1002,
           "Monitor function is not defined for body %d\n",
           i+1 );

  /* prepare arguments */
  lua_pushinteger( L, i+1 );
  lua_pushnumber( L, pbody->m );
  lua_pushnumber( L, pbody->V );

  lua_pushnumber( L, vec3d_getx( pbody->I) );
  lua_pushnumber( L, vec3d_gety( pbody->I) );
  lua_pushnumber( L, vec3d_getz( pbody->I) );

  lua_pushnumber( L, vec3d_getx( pbody->c) );
  lua_pushnumber( L, vec3d_gety( pbody->c) );
  lua_pushnumber( L, vec3d_getz( pbody->c) );

  lua_pushnumber( L, vec3d_getx( pbody->v) );
  lua_pushnumber( L, vec3d_gety( pbody->v) );
  lua_pushnumber( L, vec3d_getz( pbody->v) );

  lua_pushnumber( L, vec3d_getx( pbody->w) );
  lua_pushnumber( L, vec3d_gety( pbody->w) );
  lua_pushnumber( L, vec3d_getz( pbody->w) );

  lua_pushnumber( L, vec3d_getx( pbody->f ) );
  lua_pushnumber( L, vec3d_gety( pbody->f ) );
  lua_pushnumber( L, vec3d_getz( pbody->f ) );

  lua_pushnumber( L, vec3d_getx( pbody->t ) );
  lua_pushnumber( L, vec3d_gety( pbody->t ) );
  lua_pushnumber( L, vec3d_getz( pbody->t ) );

  if ( lua_pcall( L, 21, 0, 0 ) != 0 )
    fatal( 101,
           "Cannot call the monitor function for body %d\n",
           i+1 );

  lua_settop( L, 0 );             /* clean the stack */
}

/* ----------------------------------------------------------------- */

/*! repositioning body  */
static void reposition_body( lua_State *L,
                             const lpindex_t i,
                             body_t* restrict pbody )
{
# ifdef PARALLEL_OPENMP
# pragma omp critical (dynamics_controlled_critical)
# endif
  {
    /* get the monitor_functions array */
    lua_getglobal( L, "hdis" );
    lua_getfield( L, -1, "position_functions" );
    if ( !lua_istable( L, 1 ) )
      fatal( 1004,
             "Body %d error, no list of position_functions", i+1 );

    lua_rawgeti( L, -1, pbody->posfunc );

    if ( ! lua_isfunction( L, -1 ) )
      fatal( 1005,
             "Position function is not defined for body %d\n",
             i+1 );

    /* prepare arguments */
    lua_pushinteger( L, i+1 );
    lua_pushnumber( L, pbody->m );
    lua_pushnumber( L, pbody->V );

    lua_pushnumber( L, vec3d_getx( pbody->c) );
    lua_pushnumber( L, vec3d_gety( pbody->c) );
    lua_pushnumber( L, vec3d_getz( pbody->c) );

    lua_pushnumber( L, vec3d_getx( pbody->v) );
    lua_pushnumber( L, vec3d_gety( pbody->v) );
    lua_pushnumber( L, vec3d_getz( pbody->v) );

    if ( lua_pcall( L, 9, 3, 0 ) != 0 )
      fatal( 101,
             "Cannot call the reposition function for body %d\n",
             i+1 );

    vec3d_set( pbody->c,
               lua_tonumber( L, -3 ),
               lua_tonumber( L, -2 ),
               lua_tonumber( L, -1 ) );

    lua_settop( L, 0 );             /* clean the stack */
  }
}

/* ----------------------------------------------------------------- */


void placement_bodies( body_t* restrict pBlst,
                       const lpindex_t nBs,
                       lua_State *L,
                       const real dt,
                       const int flags )

{
  if ( flags & SETTINGS_F_QUAT_TRIG )
    {
# ifdef PARALLEL_OPENMP
# pragma omp parallel for
# endif
      for( lpindex_t i = 0; i < nBs; ++i )
        {
          body_t* pbody = pBlst + i;

          vec3d_expy( pbody->c, pbody->v, dt );

          const real theta_max = 0.0001;
          rquat_t dq;
          real wabs = vec3d_abs( pbody->w );

          if ( wabs != 0 )
            {
              vec3d_t w, a;
              vec3d_assign( w, pbody->w );
              real theta = 0.5 * wabs * dt;
              if ( theta > theta_max )
                {
                  vec3d_scale( a, w, sin(theta)/wabs );  /* renorm */
                  rquat_set_vec3d( dq, cos( theta ), a );
                }
              else
                {
                  vec3d_scale( a, w, sin( 0.5*dt ) );  /* renorm */
                  rquat_set_vec3d( dq, cos( theta ), a );
                  rquat_renorm( dq );
                }
            }
          else
            {
              rquat_set( dq, 1, 0, 0, 0 );
            }

          rquat_product( pbody->q, dq, pbody->q );
          rquat_renorm( pbody->q );

          /* calling reposition function if necessary */
          if ( pbody->posfunc != BODY_POSITIONING_NO )
            reposition_body( L, i, pbody );

          /* calling monitoring function if necessary */
          if ( pbody->imonitor != BODY_MONITOR_NO )
            monitor_body( L, i, pbody );

        }
    }
  else
    {
# ifdef PARALLEL_OPENMP
# pragma omp parallel for
# endif
      for( lpindex_t i = 0; i < nBs; ++i )
        {
          body_t* pbody = pBlst + i;

          vec3d_expy( pbody->c, pbody->v, dt );

          real k = 0.25 * dt;
          vec3d_t beta;

          vec3d_assign( beta, pbody->w );
          vec3d_scale( beta, beta, k );
          real b = 1 + vec3d_sq( beta );

          vec3d_scale( beta, beta, 2.0 / b );
          real s = ( 2.0 - b ) / b;

          rquat_t p;
          rquat_set_vec3d ( p, s, beta );

          rquat_product( pbody->q, p, pbody->q );
          rquat_renorm( pbody->q );

          /* calling reposition function if necessary */
          if ( pbody->posfunc != BODY_POSITIONING_NO )
            reposition_body( L, i, pbody );

          /* calling monitoring function if necessary */
          if ( pbody->imonitor != BODY_MONITOR_NO )
            monitor_body( L, i, pbody );
        }
    }
}

void placement_sbbs( sbb_t* restrict psbb, /* SBB array */
                     const atom_t* restrict pAlst, /* Atoms array */
                     const body_t* restrict pBlst, /* Bodies array */
                     const lpindex_t nAs,  /* Number of atoms/sbbs */
                     const sph_t* restrict pHlst,
                     const lpindex_t nHs )

{
# ifdef PARALLEL_OPENMP
# pragma omp parallel shared(psbb,pAlst,pBlst)
# endif
  {

    lpindex_t ib_old = -1;        /* old index */
    rot3d_t R;                    /* rotation matrix */

# ifdef PARALLEL_OPENMP
# pragma omp for schedule(static)
# endif
    for ( lpindex_t k = 0; k < nAs; ++k )
      {
        lpindex_t ib = psbb[k].body_indx;

        /* rotation matrix need to be recomputed? */
        if ( ib != ib_old )
          {
            rot3d_restore( &R, &(pBlst[ib].q) );
          }

        /* compute new coordinates now */
        rot3d_apply( &R, &psbb[k].c, &(pAlst[k].sbb_c) );
        vec3d_sum( psbb[k].c, psbb[k].c, pBlst[ib].c );

        /* initialize old index */
        ib_old = ib;
      }

    /* loop over sph particles */
    for ( lpindex_t k = 0, m = nAs; k < nHs; k++, m++ )
      {
        vec3d_assign( psbb[m].c, pHlst[k].c );
      }
  }
}

void placement_verts( vert_t* pVlst, body_t* pBlst, lpindex_t nVs )

{
  lpindex_t ib_old = -1;        /* old index */
  rot3d_t R;                    /* rotation matrix */

# ifdef PARALLEL_OPENMP
# pragma omp parallel for private( R ) firstprivate( ib_old )
# endif
  for ( lpindex_t k = 0; k < nVs; ++k )
    {
      vert_t* pv = pVlst + k;  /* current vert */
      lpindex_t ib = pv->ibody; /* current body index */
      body_t *pbody =   pBlst + ib;         /* current body */

      /* rotation matrix need to be recomputed? */
      if ( ib != ib_old )
        rot3d_restore( &R, &(pbody->q) );

      /* compute new coordinates now */
      rot3d_apply( &R, &(pv->global_pos), &(pv->local_pos) );
      vec3d_sum( pv->global_pos, pv->global_pos, pbody->c );

      /* initialize old index */
      ib_old = ib;
    }
}

void placement_springs( spring_t* pSlst, body_t* pBlst, lpindex_t nSs )
{
  rot3d_t R;                    /* rotation matrix */

# ifdef PARALLEL_OPENMP
# pragma omp parallel for private( R )
# endif
  for ( lpindex_t k = 0; k < nSs; ++k )
    {
      spring_t* ps = pSlst + k; /* current spring */
      body_t* pb;               /* temp pointer for a body */

      /* first body */
      pb = pBlst + ps->ib1;
      rot3d_restore( &R, &(pb->q) );
      rot3d_apply( &R, &(ps->r1), &(ps->rb1) );
      vec3d_sum( ps->r1, ps->r1, pb->c );

      /* second body */
      pb = pBlst + ps->ib2;
      rot3d_restore( &R, &(pb->q) );
      rot3d_apply( &R, &(ps->r2), &(ps->rb2) );
      vec3d_sum( ps->r2, ps->r2, pb->c );
    }
}
