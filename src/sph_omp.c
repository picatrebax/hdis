/*!

  \file sph_omp.c

  \brief Implementation of sph_omp parallel system

*/

#include <omp.h>

#include "sph_omp.h"

/* -------------------------------------------------------------------------- */

void sph_omp_init( sph_omp_t *pomp, const lpindex_t nHs )
{
# if defined( PARALLEL_OPENMP )
  pomp->nHs = nHs;
  pomp->nprocs = omp_get_max_threads();
  
  pomp->ppj = malloc( pomp->nprocs * sizeof( real* ) );
  pomp->ppF = malloc( pomp->nprocs * sizeof( vec3d_t* ) );
  
  for ( int i = 0; i < pomp->nprocs; ++i )
    {
      pomp->ppj[i] = calloc( nHs, sizeof( real ) );
      pomp->ppF[i] = calloc( nHs, sizeof( vec3d_t ) );
    }
# else
  pomp->nprocs = 1;
  pomp->ppj = NULL;
  pomp->ppF = NULL;
# endif
}

/* -------------------------------------------------------------------------- */

void sph_omp_finalize( sph_omp_t *pomp )
{
# if defined( PARALLEL_OMPENMP )
  for ( int i = 0; i < pomp->nproc; ++i )
    {
      free( pomp->ppj[i] );
      free( pomp->ppF[i] );
    }
  free ( pomp->ppj );
  free ( pomp->ppF );
# endif
}

