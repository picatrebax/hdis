/*!

  \file sphdem.c

  \brief SPH_DEM forces implementation

*/

#include "sphdem.h"

/* -------------------------------------------------------------------------- */
/* PARALLEL INTERFACE */
/* -------------------------------------------------------------------------- */


/* --------------------------------------------------------------------------- */

const real pi32 = 5.568327996831708; /* \pi^{3/2} */

/* Formula (2.11)  */
real Lambda_Gauss( real R, real h, real z )
{
  const real delta =  h/ 1000.0;

  real zR2 = 2*R*z;
  real L0 = 1.0/( zR2 * h * pi32 );
  real F0 = h*h / ( zR2 + delta );

  /* when the "wall" kicks in seriously */
  //  const real xim = h / 10.0;
  real wall = 0; //pow( xim/( xi + delta ), 4 );

  real e1 = exp( - ( R - z )*( R - z ) / ( h*h ) );
  real e2 = exp( - ( R + z )*( R + z ) / ( h*h ) );

  return ( L0 + wall ) * ( ( 1 - F0 ) * e1 + ( 1 + F0 ) * e2 );
}

/* --------------------------------------------------------------------------- */

/* This is a cap function derived to remove the tails of the Lambda_Gauss
   smoothly */

#define _DELTA 0.25

#define _DELTA1 (1-_DELTA)
#define _A ( 2.0/(_DELTA*_DELTA*_DELTA) )
#define _B ( ( 1.5*_DELTA - 3 ) * _A )
#define _C ( - 2 * _B - 3 * _A )
#define _D ( 1 - _DELTA1*_DELTA1*_DELTA1*_A - _DELTA1*_DELTA1*_B - _DELTA1*_C )

real step ( real x )
{
  if ( x < _DELTA1 ) return 1.0;
  else if ( x < 1 ) return ( ( _A*x + _B )*x + _C )*x + _D;
  else return 0;
}

/* --------------------------------------------------------------------------- */

void sphdem_Thetas( lpdyna_t* pcols,
                    sph_t* pHlst, lpindex_t nHs,
                    atom_t* pAlst, lpindex_t nAs,
                    vert_t* pVlst,
                    sbb_t* pSs )
{
  /* ONLY ATOMIC SPHERES */

  collision_t* pcols_data = ( collision_t* ) pcols->data;
  for ( lpindex_t k = 0; k < lpdyna_length( pcols ); k++ )
    {

      if ( pcols_data[k].type == COLDET_COLLISION_TYPE_SPH_DEM )
        {
          lpindex_t i;          /* SPH - always i */
          lpindex_t j;          /* DEM - always j */

          /* Finding who is SPH(j) and who is DEM(i) */
          i = pcols_data[k].i;
          j = pcols_data[k].j;
          if ( pSs[i].flag & SBB_F_DEM ) swap(i,j);

          /* getting main parameters */
          sph_t *psphi = pHlst + i - nAs;

          atom_t *patomj = pAlst + j;

          vec3d_t ri = psphi->c;
          vec3d_t rj;

          const vert_t* restrict pvertj = pVlst + patomj->ivert0;
          vec3d_assign( rj, pvertj->global_pos );

          vec3d_t ei;
          vec3d_diff( ei, ri, rj );
          const real z = vec3d_abs( ei );
          vec3d_scale( ei, ei, 1 / z ); /* normal */

          const real h = 0.5 * psphi->h; /* !!!! 0.5 for now */

          const real R = patomj->R;
          const real xx = 0.5* ( z - R ) / h;

          real Lambda = Lambda_Gauss( R, h, z ) * step ( xx );

          vec3d_scale( pcols_data[k].Theta, ei, 2*M_PI*R*R*Lambda);
        }
    }
}


/* --------------------------------------------------------------------------- */

void sphdem_forces( lpdyna_t* pcols,
                    sph_t* pHlst, lpindex_t nHs,
                    body_t* pBlst, lpindex_t nBs,
                    atom_t* pAlst, lpindex_t nAs,
                    vert_t* pVlst,
                    sbb_t* pSs,
                    real dt )
{
  /* ONLY ATOMIC SPHERES */

  collision_t* pcols_data = ( collision_t* ) pcols->data;
  for ( lpindex_t k = 0; k < lpdyna_length( pcols ); k++ )
    {

      if ( pcols_data[k].type == COLDET_COLLISION_TYPE_SPH_DEM )
        {
          lpindex_t i;          /* SPH - always i */
          lpindex_t j;          /* DEM - always j */

          /* Finding who is SPH(j) and who is DEM(i) */
          i = pcols_data[k].i;
          j = pcols_data[k].j;
          if ( pSs[i].flag & SBB_F_DEM ) swap(i,j);

          /* getting main parameters */
          sph_t *psphi = pHlst + i - nAs;

          atom_t *patomj = pAlst + j;
          body_t *pbody = pBlst + patomj->body_indx;

          vec3d_t F, Fe;            /* force and elastic part of it */

          /* collision energy */
          real E = - psphi->m * psphi->p / psphi->rho;
          E = E < 0 ? E : 0;    /* demand only repulsion */
          vec3d_scale( Fe, pcols_data[k].Theta, E );

          vec3d_t Fi;           /* viscous part of the force */

          vec3d_t T;            /* torque - only for the body */

          vec3d_t rj;           /* radius to atom */

          const vert_t* restrict pvertj = pVlst + patomj->ivert0;
          vec3d_assign( rj, pvertj->global_pos );

#if 0
          /* damping coefficient */
          const real nu = -0.05;

          /* ----------------------------------------------------------- */

          vec3d_t ri;           /* radius to sph particle */
          vec3d_assign( ri, psphi->c );

          vec3d_t rji;          /* ri - rj */
          vec3d_diff( rji, ri, rj );

          /* "normal", unit vector connecting atom center and SPH particle */
          vec3d_t n;
          real xi = vec3d_abs( rji );
          vec3d_scale( n, rji, 1.0/xi );

          /* point on atom in front of sph particle */
          vec3d_t rs;           /* rs = rj + R ( ri - rj ) / |ri - rj | */
          vec3d_linshift( rs, rj, n, patomj->R );

          /*          vec3d_t vi = psphi->v;
          vec3d_t vj = pbody->v;
          vec3d_t v;
          vec3d_diff( v, vj, vi );
          vec3d_scale( Fi, v, nu ); */

          /* point from the center of atom to the front */
          vec3d_t rcs;
          vec3d_diff( rcs, rs, pbody->c );

          /* point S velocity */
          vec3d_t vs;
          vec3d_x( vs, pbody->w, rcs );
          vec3d_inc( vs, pbody->v );

          vec3d_t vi = psphi->v;
          vec3d_t v;
          vec3d_diff( v, vs, vi ); /* velocity of approaching */

          /* projecting to normal */
          vec3d_t vn;
          vec3d_scale( vn, n, vec3d_dot( v, n ) );

          vec3d_scale( Fi, vn, nu );
#else
          vec3d_setnull( Fi );
#endif

          vec3d_sum( F, Fe, Fi );

          /* assign vector force */
          vec3d_diff ( psphi->f, psphi->f,  F );
          if ( f_is_dropped( pbody->flag, BODY_F_SPHNEUTRAL ) )
            {
              vec3d_sum  ( pbody->f, pbody->f,  F );
            }

          /* computing torque on the DEM body */

#if 0  /* honest torque */
          vec3d_t rcs;
          vec3d_diff( rcs, rs, pbody->c );
          vec3d_x( T, rcs, F );
#else  /* honest but through center */
          vec3d_t rjc;
          vec3d_diff( rjc, rj, pbody->c );
          vec3d_x( T, rjc, F );
#endif
          if ( f_is_dropped( pbody->flag, BODY_F_SPHNEUTRAL ) )
            {
              vec3d_sum( pbody->t, pbody->t, T );
            }
        }
    }
}

/* --------------------------------------------------------------------------- */

void sphdem_mstream( lpdyna_t* pcols,
                     sph_t* pHlst, lpindex_t nHs,
                     body_t* pBlst, lpindex_t nBs,
                     atom_t* pAlst, lpindex_t nAs,
                     vert_t* pVlst,
                     sbb_t* pSs )
{
  collision_t* pcols_data = ( collision_t* ) pcols->data;
  for ( lpindex_t k = 0; k < lpdyna_length( pcols ); k++ )
    {
      if ( pcols_data[k].type == COLDET_COLLISION_TYPE_SPH_DEM )
        {
          lpindex_t i;          /* SPH - always i */
          lpindex_t j;          /* DEM - always j */

          /* Finding who is SPH(i) and who is DEM(j) */
          i = pcols_data[k].i;
          j = pcols_data[k].j;
          if ( pSs[i].flag & SBB_F_DEM ) swap(i,j);


          /* getting main parameters */
          sph_t *psphi = pHlst + i - nAs;

          atom_t *patomj = pAlst + j;
          body_t *pbody = pBlst + patomj->body_indx;

          vec3d_t rj;           /* position of DEM atom sphere */

          const vert_t* restrict pvertj = pVlst + patomj->ivert0;
          vec3d_assign( rj, pvertj->global_pos );

          vec3d_t vj;           /* center of DEM atom velocity */
          vec3d_t rjc;          /* radius from center of mass to the atom center */
          vec3d_t tmp;          /* buffer for w \tiems rjc */

          vec3d_diff( rjc, rj, pbody->c );
          vec3d_x( tmp, pbody->w, rjc );
          vec3d_sum( vj, pbody->v, tmp );

          vec3d_t vij;
          vec3d_diff( vij, vj, psphi->v );

          psphi->j += psphi->rho * vec3d_dot( vij, pcols_data[k].Theta );
        }
    }
}


/* --------------------------------------------------------------------------- */

static inline real lsph( real H, real d )
{
  const real x = d / H;
  return x * x * ( 3 - 2 * x );
  //  return x;
}

void sphdem_velcorrection( lpdyna_t* pcols,
                           sph_t* pHlst, lpindex_t nHs,
                           body_t* pBlst, lpindex_t nBs,
                           atom_t* pAlst, lpindex_t nAs,
                           vert_t* pVlst,
                           sbb_t* pSs,
                           real dt )
{
  //  const lpindex_t ncols = lpdyna_length( pcols );
  collision_t* pcols_data = ( collision_t* ) pcols->data;

  {                             /* omp parallel region */

  for ( lpindex_t k = 0; k < lpdyna_length( pcols ); k++ )
    {

      if ( pcols_data[k].type == COLDET_COLLISION_TYPE_SPH_DEM )
        {
          lpindex_t i;          /* SPH - always i */
          lpindex_t j;          /* DEM - always j */

          /* Finding who is SPH(j) and who is DEM(i) */
          i = pcols_data[k].i;
          j = pcols_data[k].j;
          if ( pSs[i].flag & SBB_F_DEM ) swap(i,j);

          /* getting main parameters */
          sph_t *psphi = pHlst + i - nAs;

          atom_t *patomj = pAlst + j;
          body_t *pbody = pBlst + patomj->body_indx;

          vec3d_t rj;           /* position of DEM atom sphere */
          vec3d_t ri;           /* position of SPH particle */

          const vert_t* restrict pvertj = pVlst + patomj->ivert0;
          vec3d_assign( rj, pvertj->global_pos );
          vec3d_assign( ri, psphi->c );

          vec3d_t rji;          /* ri - rj */
          vec3d_diff( rji, ri, rj );

          /* "normal", unit vector connecting atom center and SPH particle */
          vec3d_t n;
          real xi = vec3d_abs( rji );
          real d = xi - patomj->R;

          real h = 0.5 * psphi->h;

          /* no correction if too far */
          if ( d >= h ) continue;

          vec3d_scale( n, rji, 1/xi );

          /* point on atom in front of sph particle */
          vec3d_t rs;           /* rs = rj + R ( ri - rj ) / |ri - rj | */
          vec3d_linshift( rs, rj, n, patomj->R );

          /* point from centerof atom to the front */
          vec3d_t rcs;
          vec3d_diff( rcs, rs, pbody->c );

          /* point S velocity */
          vec3d_t vs;
          vec3d_x( vs, pbody->w, rcs );
          vec3d_inc( vs, pbody->v );

          /* relative velocity of SPH particle */
          vec3d_t dvi;
          vec3d_diff( dvi, psphi->v, vs );

          /* from here new == */

          /* normal and tangent part of relative velocity */
          real dvi_n_abs = vec3d_dot( dvi, n );
          if ( dvi_n_abs >= 0 ) continue; /* no calculations if moving away for now */

          vec3d_t dvi_n;
          vec3d_scale( dvi_n, n, dvi_n_abs );

          vec3d_t dvi_tau;
          vec3d_diff( dvi_tau, dvi, dvi_n );

          real dvi_tau_abs = vec3d_abs( dvi_tau );

          /* tangent vector */
          vec3d_t tau;

          if ( dvi_tau_abs > 0 )
            {
              vec3d_scale( tau, dvi_tau, 1/dvi_tau_abs );
            }
          else
            {
              vec3d_set( tau, - vec3d_gety(n), vec3d_getx(n), 0 );
            }

          /* tangent vector */
          real dvi_n_new_abs, dvi_tau_new_abs;
          real lambda = lsph(h,d);
          dvi_n_new_abs = lambda * dvi_n_abs;
          dvi_tau_new_abs =
            sqrt( dvi_tau_abs*dvi_tau_abs +
                  ( 1 - lambda*lambda ) * dvi_n_abs *dvi_n_abs );

          /* new velocity */
          vec3d_t dvi_new;
          vec3d_lincomb( dvi_new, n, tau, dvi_n_new_abs, dvi_tau_new_abs );

          /* correction on restitution coefficient */
# if 0
          real dvi_new_abs = vec3d_abs( dvi_new );
          const real CR = 1.0;
          const real mu = - log( CR );
          real kappa = exp( - mu * dvi_new_abs * dt / h );
          printf( "%f\n", kappa );
          vec3d_scale( dvi_new, dvi_new, kappa );
#endif

          vec3d_t vnew;
          vec3d_sum( vnew, vs, dvi_new );

          /* Not working well. If force is transferred via momentum back to DEM
             solids, they just stop moving in liquied and look still on the
             carpet.  */
          if ( f_is_dropped( pbody->flag, BODY_F_SPHNEUTRAL ) )
            {
              /* calculation of momentum and according force & torques, this is for
                 conservation of energy and 2nd Newton law */
              vec3d_t dv, F;
              real nu = 0.025;       /* portion of the impulse going to the body */
              vec3d_diff( dv, vnew, psphi->v );
              vec3d_scale( F, dv, -nu*psphi->m / dt );
              vec3d_inc  ( pbody->f,  F );

              vec3d_t T;            /* torque - only for the body */

              vec3d_t rjc;
              vec3d_diff( rjc, rj, pbody->c );
              vec3d_x( T, rjc, F );
              vec3d_sum( pbody->t, pbody->t, T );
            }

          vec3d_assign( psphi->v, vnew );
        }
    }
  } /* vnew of parallel region */
}


#ifdef LAMBDA_TEST

int main()
{
  const char* fname = "lambda_out.txt";

  const real R = 0.15;
  const real h = 0.015;
  const real dz = R / 10000;

  real z = dz;

  FILE* pf = fopen( fname, "w" );

  while ( z < R + 3*h )
    {
      real xx = 0.5* ( z - R ) / h;
      real G = Lambda_Gauss( R, h, z );
      fprintf( pf, "%lf %lf %lf %lf\n", (z-R)/h, G, G * step(xx), 200*step(xx) );
      z += dz;
    }

  fclose( pf );

}

#endif


#ifdef OLDTEXT
          /* normal and tangential velocitites */
          real v_in_abs = vec3d_dot( psphi->v, n );
          real v_sn_abs = vec3d_dot( vs, n );

          /* no correction if distance is increasing --- ???? */
          if ( v_in_abs > v_sn_abs ) continue;

          vec3d_t v_in, v_sn;
          vec3d_scale( v_in, n, v_in_abs );
          vec3d_scale( v_sn, n, v_sn_abs );

          vec3d_t v_it;
          vec3d_diff( v_it, psphi->v, v_in );

          /* normal differnce and final correction */
          vec3d_t v_isn, vnew;
          vec3d_diff( v_isn, v_in, v_sn );
          vec3d_scale( v_isn, v_isn, lsph(h,d) );
          vec3d_inc( v_isn, v_sn );

          vec3d_sum( vnew, v_it, v_isn );
#endif
