/*!

  \file placement.h

  \brief Changing the position of different elements according to
  their velocitites and angular velocities of the bodies.

*/

#ifndef PLACEMENT_H
#define PLACEMENT_H


#include "defs.h"               /* must for macros */
#include "box.h"                /* box is box */
#include "vec3d.h"              /* vec3d type */
#include "rquat.h"              /* quaternions */
#include "rot3d.h"              /* rotation matrix */
#include "body.h"               /* body_t */
#include "spring.h"             /* spring_t */

#include "hdis.h"              /* Flag values */
#include "bb.h"
#include "sph.h"

/*!

  \brief Updates the location (center of mass) and orientation
  (quaternions) of the bodies according to the calculated velocity and
  angular velocity at dynamics stage and provides the information for
  main lua script about body's parameters if it is a monitored body.

  \param pBlst the list of bodies
  \param[in] nBs number of bodies
  \param L Lua state to call monitor function if necessary
  \param[in] dt timestep
  \param[in] flags settings.flags

 */
void placement_bodies( body_t* restrict pBlst,
                       const lpindex_t nBs,
                       lua_State *L,
                       const real dt,
                       const int flags );


void placement_sbbs( sbb_t* restrict psbb, /* SBB array */
                     const atom_t* restrict pAlst, /* Atoms array */
                     const body_t* restrict pBlst, /* Bodies array */
                     const lpindex_t nAs,  /* Number of atoms/sbbs */
                     const sph_t* restrict pHlst,
                     const lpindex_t nHs );


void placement_verts( vert_t* pVlst, body_t* pBlst, lpindex_t nVs )

                                   ;


void placement_springs( spring_t* pSlst, body_t* pBlst, lpindex_t nSs )
                                     ;


#endif
