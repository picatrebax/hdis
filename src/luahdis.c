#include "luahdis.h"

int lua_hdis_get_real_field( lua_State* L, const char* name,
                              real* pval )
{
  int success_code = 1;
  lua_getfield( L, -1, name );

  if ( lua_isnumber( L, -1 ) )
    *pval = (real) lua_tonumber( L, -1 );
  else
    success_code = 0;

  lua_pop( L, 1 );
  return success_code;
}

int lua_hdis_get_int_field( lua_State* L, const char* name,
                             int *pval )
{
  int success_code = 1;
  lua_getfield( L, -1, name );

  if ( lua_isnumber( L, -1 ) )
    *pval = (real) lua_tointeger( L, -1 );
  else
    success_code = 0;

  lua_pop( L, 1 );
  return success_code;
}

int lua_hdis_get_bool_field( lua_State* L, const char* name,
                              bool* pval )
{
  int success_code = 1;
  lua_getfield( L, -1, name );

  if ( lua_isboolean( L, -1 ) )
    *pval = (bool) lua_toboolean( L, -1 );
  else
    success_code = 0;

  lua_pop( L, 1 );
  return success_code;
}

lpindex_t lua_hdis_get_length( lua_State* L, 
                                const char* table )
{
  lpindex_t result = 0;
  char* currents;
  lpindex_t depth;          /* hierarchy depth */

  char* table_copy = strdup( table );

  /* table can be a dot-separated hierarchy of tables! */
  depth = 1;
  currents = strtok( table_copy, "." );
  lua_getglobal( L, currents );
  if( ! lua_istable( L, -1 ) )
     fatal( 1, "There is no table %s in the file", table );

  /* next cases are special: reading fields */
  currents = strtok( NULL, "." );
  while( currents != NULL )
    {
      depth++;
      lua_getfield( L, -1, currents );
      currents = strtok( NULL, "." );
      
      if( ! lua_istable( L, -1 ) )
        fatal( 1, "There is no table %s in the file", table );
    }
  result = lua_objlen( L, -1 );

  lua_pop( L, depth );
  free( table_copy );

  return result;
}

int lua_hdis_get_vec3d( lua_State* L, const char* name, vec3d_t* v )
{
  int success_code = 1;
  lua_getfield( L, -1, name );

  if ( lua_istable( L, -1 ) )
    {
      /* reading vector */
      real x[3];

      for ( int i = 0; i < 3; ++i )
        {
          lua_rawgeti ( L, -1, i+1 );
          x[i] = (real) lua_tonumber( L, -1 );
          lua_pop( L, 1 );
        }
      vec3d_set( *v, x[0], x[1], x[2] );
    }
  else success_code = 0;

  lua_pop( L, 1 );
  return success_code;
}

int lua_hdis_get_rquat( lua_State* L, const char* name, rquat_t* v )
{
  int success_code = 1;
  lua_getfield( L, -1, name );

  if ( lua_istable( L, -1 ) )
    {
      /* reading vector */
      real x[4];

      for ( int i = 0; i < 4; ++i )
        {
          lua_rawgeti ( L, -1, i+1 );
          x[i] = (real) lua_tonumber( L, -1 );
          lua_pop( L, 1 );
        }
      rquat_set( *v, x[0], x[1], x[2], x[3] );
    }
  else success_code = 0;

  lua_pop( L, 1 );
  return success_code;
}


