#ifndef HDIS_TYPES_H
#define HDIS_TYPES_H

#include <stdbool.h>

typedef struct settings_s
{
  unsigned int flags;           /* flags for different things, see
                                   constants SETTINGS_F_ */
  unsigned int cdmethod;        /* choice of collision detection
                                   method, see SETTINGS_CDM_
                                   constants */
  real zout_vel;                /* outside of the box velocity
                                   along z */
  real cdm_dx_min;              /* minimum size for space partitioning
                                   type collision detection method */

  real contact_tau0;            /* min time before any contact
                                   re-check for contacts in 
                                   collisions list */
  real contact_delta0;          /* maximum allowed overlap for
                                   distance calculation skip system to
                                   start working, negative value */
  real contact_alpha;           /* scaling */

  real cohesion_bound;          /* distance around particles to test
                                   for cohesion in the case of
                                   existing contact. See "forces.c" */

  char* run_from_name;          /* filename of the run_from file */
  char* luafile;                /* filename of the config. script */
} settings_t;

typedef struct diagnostics_s
{
  lpindex_t contacts_actual_n;        /* actual contacts (total) */
  lpindex_t contacts_potential_n;     /* potential contacts (total) */
  lpindex_t contacts_actual_bb_n;     /* actual contacts (body-body) */
  lpindex_t contacts_potential_bb_n;  /* potential contacts
                                         (body-body) */
  lpindex_t contacts_features_bb_n;   /* features tried */
  lpindex_t contacts_inits_bb_n;      /* new contact initializations */
  lpindex_t contacts_distance_bb_n;   /* distance calculations */
} diagnostics_t;

typedef struct conhash_comp_s
{
  double last_ulm;              /* last unsuccessful lookup misses
                                   (ULM) number */
  lpindex_t capacity;           /* capacity of the table */
  int ULMT;                     /* ULM threshold */
  int nprobes;                  /* number of probes to find current
                                   ULM value */
  int ncleans;                  /* number of cleans performed on the
                                   hash table to the moment */
  bool bsave;                    /* true if the table should be saved in
                                    HDF5 file. false if not (default) */
} conhash_comp_t;

typedef struct drag_s
{
  real Cd;                      /* Reynolds drag coefficient */
  real mu;                      /* Stokes' drag coefficient */
  int isdrag;                   /* true if there is a drag */
  int isuniform;                /* true if drag is uniform */
} drag_t;

#endif  /* HDIS_TYPES_H */

