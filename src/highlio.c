#include "highlio.h"
#include "lpio.h"

void highlio_save( char* filename, /* output file name */
                   settings_t * psettings,
                   lptimer_t* pt,
                   sbb_t* restrict sbbs,
                   atom_t* restrict pAlst,
                   vert_t* restrict pVlst,
                   body_t* restrict pBlst,
                   spring_t* restrict pSlst,
                   ptriangle_t* restrict pTlst,
                   sph_t* restrict pHlst,
                   const lpindex_t nsbbs,
                   const lpindex_t nAs,
                   const lpindex_t nVs,
                   const lpindex_t nBs,
                   const lpindex_t nSs,
                   const lpindex_t nTs,
                   const lpindex_t nHs,
                   conhash_t* restrict pch,
                   conhash_comp_t* restrict pch_comp,
                   bc_t* pbc,
                   box_t* restrict pbox,
                   vec3d_t* restrict g,
                   drag_t* restrict pdrag,
                   char** pA_gnames,
                   const lpindex_t nA_gnames,
                   int* pA_intrx,
                   const lpindex_t nA_intrx,
                   material_t* pA_mats,
                   char** pA_matnames,
                   const lpindex_t nA_mats,
                   cmprops_t* pcmprops,
                   const lpindex_t ncmprops,
                   sbb_params_t* psbb_params,
                   sph_props_t* psphprops,
                   portal_t * pportal )
{
  /* creating the contact list */
  contact_t* pcs;               /* list */
  size_t npcs;                  /* number of elements in it */
  pcs = conhash_conlist_make_alloc( pch, pt->n - 1, &npcs );
  /* pcs==NULL means there are no contacts to save */

  /* creating the file */
  lpio_t * pio = lpio_alloc( filename, LPIO_ACCESS_F_OVERWRITE );
  assert( pio );

  /* standard types declaration */
  lpio_stdtypes_t * pstdt;
  pstdt = lpio_stdtypes_alloc();

  /* new types creation/registration */
  lpio_dtype_t dtype_dem_settings;
  lpio_dtype_create( dtype_dem_settings, settings_t );
  lpio_dtype_freg( dtype_dem_settings, settings_t, flags,
                   pstdt->dtype_int );
  lpio_dtype_freg( dtype_dem_settings, settings_t, cdmethod,
                   pstdt->dtype_int );
  lpio_dtype_freg( dtype_dem_settings, settings_t, zout_vel,
                   pstdt->dtype_real );
  lpio_dtype_freg( dtype_dem_settings, settings_t, cdm_dx_min,
                   pstdt->dtype_real );
  lpio_dtype_freg( dtype_dem_settings, settings_t, contact_tau0,
                   pstdt->dtype_real );
  lpio_dtype_freg( dtype_dem_settings, settings_t, contact_delta0,
                   pstdt->dtype_real );
  lpio_dtype_freg( dtype_dem_settings, settings_t, contact_alpha,
                   pstdt->dtype_real );
  lpio_dtype_freg( dtype_dem_settings, settings_t, run_from_name,
                   pstdt->dtype_int );
  lpio_dtype_freg( dtype_dem_settings, settings_t, luafile,
                   pstdt->dtype_int );

  lpio_dtype_t dtype_conhash_comp;
  lpio_dtype_create( dtype_conhash_comp, conhash_comp_t );
  lpio_dtype_freg( dtype_conhash_comp, conhash_comp_t, last_ulm,
                   pstdt->dtype_real );
  lpio_dtype_freg( dtype_conhash_comp, conhash_comp_t, capacity,
                   pstdt->dtype_int );
  lpio_dtype_freg( dtype_conhash_comp, conhash_comp_t, ULMT,
                   pstdt->dtype_int );
  lpio_dtype_freg( dtype_conhash_comp, conhash_comp_t, nprobes,
                   pstdt->dtype_int );
  lpio_dtype_freg( dtype_conhash_comp, conhash_comp_t, ncleans,
                   pstdt->dtype_int );
  lpio_dtype_freg( dtype_conhash_comp, conhash_comp_t, bsave,
                   pstdt->dtype_int );

  lpio_dtype_t dtype_drag;
  lpio_dtype_create( dtype_drag, drag_t );
  lpio_dtype_freg( dtype_drag, drag_t, Cd, pstdt->dtype_real );
  lpio_dtype_freg( dtype_drag, drag_t, mu, pstdt->dtype_real );
  lpio_dtype_freg( dtype_drag, drag_t, isdrag, pstdt->dtype_int );
  lpio_dtype_freg( dtype_drag, drag_t, isuniform, pstdt->dtype_int );

  lpio_dtype_t dtype_sbb_params;
  lpio_dtype_create( dtype_sbb_params, sbb_params_t );
  lpio_dtype_freg( dtype_sbb_params, sbb_params_t,
                   drmax, pstdt->dtype_real );
  lpio_dtype_freg( dtype_sbb_params, sbb_params_t,
                   drmin, pstdt->dtype_real );
  lpio_dtype_freg( dtype_sbb_params, sbb_params_t,
                   tau, pstdt->dtype_real );

  /* saving data */
  lpio_save_value( pio, pstdt->dtype_lptimer, "Time_Parameters", pt,
                   LPIO_STDSTR_UNITS_LPTIMER, LPIO_STDSTR_DESC_LPTIMER );
  lpio_save_value( pio, pstdt->dtype_box, "Box (domain)", pbox,
                   LPIO_STDSTR_UNITS_BOX, LPIO_STDSTR_DESC_BOX );
  if ( sbbs )
    lpio_save_array( pio, pstdt->dtype_sbb, "SBBs", sbbs, nsbbs,
                     LPIO_STDSTR_UNITS_SBB, LPIO_STDSTR_DESC_SBB );

  if ( pAlst )
    lpio_save_array( pio, pstdt->dtype_atom, "Atoms", pAlst, nAs,
                     LPIO_STDSTR_UNITS_ATOM, LPIO_STDSTR_DESC_ATOM );

  if ( pVlst )
    lpio_save_array( pio, pstdt->dtype_vert, "Verts", pVlst, nVs,
                     LPIO_STDSTR_UNITS_VERT, LPIO_STDSTR_DESC_VERT );

  if ( pBlst )
    lpio_save_array( pio, pstdt->dtype_body, "Bodies", pBlst, nBs,
                     LPIO_STDSTR_UNITS_BODY, LPIO_STDSTR_DESC_BODY );

  if ( pSlst )
    lpio_save_array( pio, pstdt->dtype_spring, "Springs", pSlst, nSs,
                     LPIO_STDSTR_UNITS_SPRING, LPIO_STDSTR_DESC_SPRING );

  if ( pTlst )
    lpio_save_array( pio, pstdt->dtype_ptriangle, "PTriangles", pTlst, nTs,
                     LPIO_STDSTR_UNITS_PTRIANGLE,
                     LPIO_STDSTR_DESC_PTRIANGLE );

  if ( pHlst )
    lpio_save_array( pio, pstdt->dtype_dot, "Dots", pHlst, nHs,
                     NULL,
                     NULL );

  /* contact hash save only if required */
  if ( pch && pch_comp && pch_comp->bsave )
    lpio_save_array( pio, pstdt->dtype_coninfo, "Contact Hash",
                     (pch->cons), (pch->maxn),
                     LPIO_STDSTR_UNITS_CONINFO, LPIO_STDSTR_DESC_CONINFO
                     );
  /* contact list is saved only if there are any contacts */
  if ( pcs )
    lpio_save_array( pio, pstdt->dtype_contact, "Contact List",
                     pcs, npcs,
                     LPIO_STDSTR_UNITS_CONTACT, LPIO_STDSTR_DESC_CONTACT
                     );

  lpio_save_value( pio, pstdt->dtype_vec3d,
                   "Gravity", g,
                   "g [m/s^2]", "g acceleration of free fall" );

  lpio_save_value( pio, dtype_drag,
                   "Drag Force Values", pdrag,
                   "Cd [1] | mu [kg/(m s)]"
                   " | isdrag [bool] | isuniform  [bool]",
                   "Drag force values: Cd is Reynolds drag, mu is"
                   " Stokes coefficient, uniform is artificial drag" );
  if ( pA_mats )
    lpio_save_array( pio, pstdt->dtype_material,
                     "Materials",
                     pA_mats, nA_mats,
                     LPIO_STDSTR_UNITS_MATERIAL,
                     LPIO_STDSTR_DESC_MATERIAL );

  if ( pcmprops )
    lpio_save_array( pio, pstdt->dtype_cmprops,
                     "Contact Mech. Properties",
                     pcmprops, ncmprops,
                     LPIO_STDSTR_UNITS_CMPROPS,
                     LPIO_STDSTR_DESC_CMPROPS );

  if ( pA_intrx )
    lpio_save_array( pio, pstdt->dtype_int,
                     "Group Interaction",
                     pA_intrx, nA_intrx,
                     "intrx [1]",
                     "Table of interacting groups: 1 for yes, 0 for no" );

  lpio_save_value( pio, dtype_dem_settings, "General Settings", psettings,
                   "No comments", "No comments" );

  lpio_save_value( pio, dtype_sbb_params, "SBB parameters", psbb_params,
                   "drmax [m] | drmin [m] | tau [s]",
                   "drmax is max allowed increase"
                   " for SBB radius over circumscribed radius, "
                   "drmin minimum allowed increase allowed, "
                   "tau is semi-maximum time between"
                   " two contact detections");

  if ( pch_comp )
    lpio_save_value( pio, dtype_conhash_comp, "Contact Hash Associate",
                     pch_comp,
                     "No comments", "No comments" );
  lpio_save_value( pio, pstdt->dtype_bc,
                   "Boundary", pbc,
                   "No comments", "No comments" );
  /* global attribute saving */
  lpio_save_global_string_attribute( pio, "Program",
                                     PACKAGE_STRING );

  if ( psettings->luafile )
    lpio_save_global_string_attribute( pio, "Lua Script",
                                       psettings->luafile );

  if ( psettings->run_from_name )
    lpio_save_global_string_attribute( pio, "Initial HDF5",
                                       psettings->run_from_name );
  else
    lpio_save_global_string_attribute( pio, "Initial HDF5",
                                       "N/A" );
  if ( pA_gnames )
    lpio_save_astrings ( pio, pA_gnames, nA_gnames,
                         "Group Names",
                         "List of group names in the file" );

  if ( pA_matnames )
    lpio_save_astrings ( pio, pA_matnames, nA_mats,
                         "Material Names",
                         "List of material names in the file" );

  lpio_save_value( pio, pstdt->dtype_portal,
                   "Portal",
                   pportal,
                   LPIO_STDSTR_UNITS_PORTAL,
                   LPIO_STDSTR_DESC_PORTAL );

  lpio_save_value( pio, pstdt->dtype_sphprops,
                   "SPH properties",
                   psphprops,
                   LPIO_STDSTR_UNITS_SPH_PROPS,
                   LPIO_STDSTR_DESC_SPH_PROPS );


  /* free the memory */
  lpio_stdtypes_free( pstdt );
  lpio_free( pio );
  if ( pcs ) free( pcs );       /* freeing contact list */
}

void highlio_read_dims( char* filename,
                        lpindex_t *pnBs,
                        lpindex_t *pnAs,
                        lpindex_t *pnVs,
                        lpindex_t *pnSs,
                        lpindex_t *pnTs,
                        lpindex_t *pnHs,
                        lpindex_t *pnCs )
{
  lpio_t *pio = lpio_alloc( filename, LPIO_ACCESS_F_READONLY );

  *pnBs = lpio_get_dim( pio, "Bodies" );
  if ( *pnBs == LPIO_DATASET_MISSING ) *pnBs = 0;

  *pnAs = lpio_get_dim( pio, "Atoms" );
  if ( *pnAs == LPIO_DATASET_MISSING ) *pnAs = 0;

  *pnVs = lpio_get_dim( pio, "Verts" );
  if ( *pnVs == LPIO_DATASET_MISSING ) *pnVs = 0;

  *pnSs = lpio_get_dim( pio, "Springs" );
  if ( *pnSs == LPIO_DATASET_MISSING ) *pnSs = 0;

  *pnTs = lpio_get_dim( pio, "PTriangles" );
  if ( *pnTs == LPIO_DATASET_MISSING ) *pnTs = 0;

  *pnHs = lpio_get_dim( pio, "Dots" );
  if ( *pnHs == LPIO_DATASET_MISSING ) *pnHs = 0;

  *pnCs = lpio_get_dim( pio, "Contact List" );
  if ( *pnCs == LPIO_DATASET_MISSING ) *pnCs = 0;

  lpio_free( pio );
}
void highlio_read_data( char* filename,
                        vert_t*    pVlst,
                        atom_t*    pAlst,
                        body_t*    pBlst,
                        spring_t*  pSlst,
                        ptriangle_t* pTlst,
                        sph_t* pHlst,
                        contact_t* pconlist,
                        box_t* pbox,
                        lptimer_t* pt,
                        vec3d_t* pg,
                        drag_t* pdrag,
                        bc_t* pbc,
                        sph_props_t* psphprops,
                        settings_t* psettings )
{
  int errcode;
  lpio_t *pio = lpio_alloc( filename, LPIO_ACCESS_F_READONLY );
  assert ( pio );

  if ( pVlst ) lpio_read( pio, "Verts", pVlst ); /* reading from 0 */
  if ( pAlst ) lpio_read( pio, "Atoms", pAlst ); /* reading from 0 */
  if ( pBlst ) lpio_read( pio, "Bodies", pBlst ); /* reading from 0 */
  if ( pSlst ) lpio_read( pio, "Springs", pSlst );
  if ( pTlst ) lpio_read( pio, "PTriangles", pTlst );
  if ( pHlst ) lpio_read( pio, "Dots", pHlst );

  if ( pconlist )               /* contacts if exist */
    lpio_read( pio, "Contact List", pconlist );

  lpio_read( pio, "Box (domain)", pbox );
  lpio_read( pio, "Time_Parameters", pt );

  lpio_read( pio, "Gravity", pg );

  lpio_read( pio, "Boundary", pbc );

  lpio_read( pio, "SPH properties", psphprops );
  
  /* settings we read although not changing run_from and luafile */
  char* pstmp0 = psettings->run_from_name;
  char* pstmp1 = psettings->luafile;
  lpio_read( pio, "General Settings", psettings );
  psettings->run_from_name = pstmp0;
  psettings->luafile = pstmp1;

  /* reading drag force parameters */
  errcode = lpio_read( pio, "Drag Force Values", pdrag );
  if ( errcode == LPIO_DATASET_MISSING )
    {
      warning( "Cdrag is not found (possibly old HDF5 file),"
               " safe to continue\n" );
    }

  lpio_free( pio );
}
