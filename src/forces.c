#include "forces.h"

#if defined( PARALLEL_OPENMP )
static int nproc = 1;           /* number of processors */
static vec3d_t** ppompf = NULL; /* forces accumulator */
static vec3d_t** ppompt = NULL; /* torques accumulator */
#endif

void forces_init( const lpindex_t nBs )
{
# if defined( PARALLEL_OPENMP )
  nproc = omp_get_max_threads();
  ppompf = malloc( nproc * sizeof( vec3d_t* ) );
  ppompt = malloc( nproc * sizeof( vec3d_t* ) );

  for ( int i = 0; i < nproc; ++i )
    {
      ppompf[i] = calloc( nBs, sizeof( vec3d_t ) );
      ppompt[i] = calloc( nBs, sizeof( vec3d_t ) );
    }
# endif
}

void forces_finalize()
{
# if defined( PARALLEL_OPENMP )
  for ( int i = 0; i < nproc; ++i )
    {
      free( ppompf[i] );
      free( ppompt[i] );
    }
  free( ppompf );
  free( ppompt );
#endif
}

/* --------------------------------------------------------------------------- */

void forces_gravitation( body_t * restrict pBlst,
                         const lpindex_t nBs,
                         const vec3d_t* restrict g  )
{
# if defined( PARALLEL_OPENMP )
# pragma omp parallel for
# endif
  for ( lpindex_t i = 0; i < nBs; ++i )
    {
      /* setting up gravitational force + torque = 0 */
      vec3d_setnull( pBlst[i].t );

      /* gravitation cancels old value of forces at this stage*/
      vec3d_setnull( pBlst[i].f );
      vec3d_scale( pBlst[i].f, *g, pBlst[i].m );
    }
}

/* --------------------------------------------------------------------------- */

void forces_extforces( body_t * restrict pBlst,
                       const lpindex_t nBs,
                       lua_State* L )
{
# if defined( PARALLEL_OPENMP )
# pragma omp parallel for
# endif
  for ( lpindex_t i = 0; i < nBs; ++i )
    {
      if ( pBlst[i].flag & BODY_F_EXTFORCED )
        {
#         ifdef PARALLEL_OPENMP
#           pragma omp critical (dynamics_controlled_critical)
#         endif
          {
            body_t* pbody = &pBlst[i];

            /* get the control_functions array */
            lua_getglobal( L, "hdis" );
            lua_getfield( L, -1, "extforce_functions" );
            if ( !lua_istable( L, 1 ) )
              fatal( 1003,
                     "Body %d error, no list of extforce_functions", i+1 );

            lua_rawgeti( L, -1, pbody->effunc );

            if ( ! lua_isfunction( L, -1 ) )
              fatal( 1003,
                     "Control function is not defined for body %d\n",
                     i+1 );

            /* prepare arguments */
            lua_pushinteger( L, i+1 );
            lua_pushnumber( L, pbody->m );
            lua_pushnumber( L, pbody->V );

            lua_pushnumber( L, vec3d_getx( pbody->c) );
            lua_pushnumber( L, vec3d_gety( pbody->c) );
            lua_pushnumber( L, vec3d_getz( pbody->c) );

            lua_pushnumber( L, rquat_gets( pbody->q) );
            lua_pushnumber( L, rquat_getvx( pbody->q) );
            lua_pushnumber( L, rquat_getvy( pbody->q) );
            lua_pushnumber( L, rquat_getvz( pbody->q) );

            lua_pushnumber( L, vec3d_getx( pbody->v) );
            lua_pushnumber( L, vec3d_gety( pbody->v) );
            lua_pushnumber( L, vec3d_getz( pbody->v) );

            lua_pushnumber( L, vec3d_getx( pbody->w) );
            lua_pushnumber( L, vec3d_gety( pbody->w) );
            lua_pushnumber( L, vec3d_getz( pbody->w) );

            if ( lua_pcall( L, 16, 6, 0 ) != 0 )
              fatal( 102,
                     "Cannot call the external force function for body %d\n",
                     i+1 );

            vec3d_t f, t;

            vec3d_set( f,
                       lua_tonumber( L, -6 ),
                       lua_tonumber( L, -5 ),
                       lua_tonumber( L, -4 ) );

            vec3d_set( t,
                       lua_tonumber( L, -3 ),
                       lua_tonumber( L, -2 ),
                       lua_tonumber( L, -1 ) );

            vec3d_sum( pbody->f, pbody->f, f );
            vec3d_sum( pbody->t, pbody->t, t );

            lua_settop( L, 0 );             /* clean the stack */
          }

        }
    }
}

/* ----------------------------------------------------------------- */

void forces_drag( body_t * restrict pBlst,
                  const lpindex_t nBs,
                  const drag_t * restrict pdrag )

{
  if ( ! pdrag -> isuniform )     /* Natural */
    {
      /* Reynolds */
      if ( pdrag -> Cd > 0 )
        {
# if defined( PARALLEL_OPENMP )
# pragma omp parallel for
# endif
          for ( lpindex_t i = 0; i < nBs; ++i )
            {

              real K = - 0.6 * pdrag->Cd * pow( pBlst[i].V, 2.0/3.0 ) *
                vec3d_abs( pBlst[i].v );
              vec3d_expy( pBlst[i].f, pBlst[i].v, K );
            }
        }
      /* Stokes' */
      if ( pdrag -> mu > 0 )
# if defined( PARALLEL_OPENMP )
# pragma omp parallel for
# endif
        for ( lpindex_t i = 0; i < nBs; ++i )
          {

            real K = - 11.69 * pdrag->mu * pow( pBlst[i].V, 1./3. );
            vec3d_expy( pBlst[i].f, pBlst[i].v, K );
          }
    }
  else                          /* Uniform */
    {
      /* Reynolds */
      if ( pdrag -> Cd > 0 )
        {
# if defined( PARALLEL_OPENMP )
# pragma omp parallel for
# endif
          for ( lpindex_t i = 0; i < nBs; ++i )
            {
              real K = - pdrag->Cd * pBlst[i].V * vec3d_abs( pBlst[i].v );
              vec3d_expy( pBlst[i].f, pBlst[i].v, K );
            }
        }
      /* Stokes' */
      if ( pdrag -> mu > 0 )
# if defined( PARALLEL_OPENMP )
# pragma omp parallel for
# endif
        for ( lpindex_t i = 0; i < nBs; ++i )
          {

            real K = - pdrag->mu * pBlst[i].V;
            vec3d_expy( pBlst[i].f, pBlst[i].v, K );
          }
    }
}

void forces_bb( body_t* pBlst,
                atom_t* restrict pAlst,
                vert_t* restrict pVlst,
                const lpdyna_t* pcollisions,
                const cmprops_t * restrict pcmprops,
                const lpindex_t nBs,
                const lpindex_t nAs,
                const lpindex_t Nmats,
                conhash_t* pch,
                const real dt,
                const size_t ncurrent,
                settings_t *psettings,
                diagnostics_t* pdiag,
                real* pvc_sq_max )
{
  /* direct access to collisions and to cprops */
  const lpindex_t ncols = lpdyna_length( pcollisions );
  collision_t* restrict pcols_data =
    (collision_t*) pcollisions->data;

  /* temp storage for counters of actual contacts */
  lpindex_t nactual = 0;

  /* temp storage for maximum square of contact velocity */
  real vc_sq_max = 0.0;

# if defined( PARALLEL_OPENMP )
# pragma omp parallel
# endif
  {  /* omp parallel region: when applied */

#   if defined( PARALLEL_OPENMP )
    const int nthread = omp_get_thread_num();  /* thread number */
#   endif

    lpindex_t nactual_private = 0;
    real vc_sq_max_private = 0.0;

#   if defined( PARALLEL_OPENMP )
#   pragma omp for
#   endif
    for ( lpindex_t k = 0; k < ncols; ++k )
      {
        /* indices of Atoms/SBBs that possibly collide */
        const lpindex_t i = pcols_data[k].i;
        const lpindex_t j = pcols_data[k].j;

if ( i < nAs && j < nAs )
{
        lpindex_t nhash = pcols_data[k].nhash;

        /* direct pointers to the atoms */
        const atom_t* restrict patom_i = pAlst + i;
        const atom_t* restrict patom_j = pAlst + j;

        /* centers of interaction */
        vec3d_t ri, rj;
        real h, delta;

        const vert_t* restrict pvert_i = pVlst + patom_i->ivert0;
        const vert_t* restrict pvert_j = pVlst + patom_j->ivert0;

        vec3d_assign( ri, pvert_i->global_pos );
        vec3d_assign( rj, pvert_j->global_pos );

        h = vec3d_absdiff( ri, rj );

        delta = patom_i->R + patom_j->R - h;

        if ( delta > 0 )
          {
            /* for negative delta, we are not working on the contact
               that was not a contact on the previous time step */
            if ( delta <= 0 &&
                 pch->cons[nhash].nlast != ncurrent-1 )
              continue;

            /* pointers to the corresponding bodies */
            body_t * restrict pbody_i = pBlst + patom_i->body_indx;
            body_t * restrict pbody_j = pBlst + patom_j->body_indx;

            /* sensitive bodies becoming still immidiately, no forces
               we assign */
            if ( pbody_i->flag & BODY_F_SENSITIVE )
              {
                pbody_i->flag &= ~BODY_F_SENSITIVE;
                pbody_i->flag |=  BODY_F_STILL;
                continue;
              }
            if ( pbody_j->flag & BODY_F_SENSITIVE )
              {
                pbody_j->flag &= ~BODY_F_SENSITIVE;
                pbody_j->flag |=  BODY_F_STILL;
                continue;
              }

            /* getting the contact hash table index for quick access */
            if ( nhash == COLDET_CONHASH_UNKNOWN )
              {
                nhash = conhash_lookup_and_adjust( pch, i, j, ncurrent );
                pcols_data[k].nhash = nhash;
              }
            else
              {
                /* direct update of the nlast value to speed up (we
                   are going to be here much more often due to
                   coherency of contacts */
                pch->cons[nhash].nlast = ncurrent;
              }

            /* get the mechanical property */
            const cmprops_t *pcmprop = pcmprops + pcols_data[k].icmprop;

            /* short cuts to dilation radii */
            const real Ri = patom_i->R;
            const real Rj = patom_j->R;

            /* always sphere-sphere contact! */
            /* geometry at the point of interaction */
            vec3d_t n;            /* normal outwards of particle i */
            vec3d_diff( n, rj, ri );
            vec3d_normalize( n, n );

            real x = 0.5 * ( (Ri-Rj)*(Ri+Rj)/h + h );
            vec3d_t p;

            vec3d_linshift( p, ri, n, x );

            /* kinematics at the point of interaction */
            vec3d_t rci, rcj;     /* radius vector from center of mass
                                     to the point of contact */
            vec3d_t vci, vcj;     /* velocities at point of contact */
            vec3d_t vc;           /* relative velocity at the contact */

            vec3d_diff( rci, p, pbody_i->c );
            vec3d_diff( rcj, p, pbody_j->c );

            vec3d_x( vci, pbody_i->w, rci );
            vec3d_x( vcj, pbody_j->w, rcj );

            vec3d_inc( vci, pbody_i->v );
            vec3d_inc( vcj, pbody_j->v );

            vec3d_diff( vc, vcj, vci );

            /* computing maximum contact velocity square */
            real vc_sq = vec3d_sq( vc );
            if ( vc_sq_max_private < vc_sq ) vc_sq_max_private = vc_sq;

            vec3d_t F, T, TI;  /* TI is minus torque for i-th body */

            int ccode =
              conmech_forces( &F,
                              &(pch->cons[nhash].ftau_e),
                              delta,
                              &vc,
                              &n,
                              pcmprop,
                              pcols_data[k].rint,
                              pcols_data[k].kni,
                              dt );

            /* contact breaks */
            if ( ccode == CONMECH_CONTACT_NO )
              {
                pch->cons[nhash].nlast = ncurrent - 1;
                continue;
              }

            vec3d_x( T, rcj, F );  /* torques */
            vec3d_x( TI, rci, F ); /* minus hidden in decrement below */

# if defined( PARALLEL_OPENMP )
            {
              vec3d_inc( ppompf[nthread][patom_j->body_indx], F );
              vec3d_inc( ppompt[nthread][patom_j->body_indx], T );
              vec3d_dec( ppompf[nthread][patom_i->body_indx], F );
              vec3d_dec( ppompt[nthread][patom_i->body_indx], TI );
            }
# else
            {
              vec3d_inc( pbody_j->f, F );
              vec3d_inc( pbody_j->t, T );
              vec3d_dec( pbody_i->f, F );
              vec3d_dec( pbody_i->t, TI );
            }
#endif

            vec3d_assign( pch->cons[nhash].pc, p );
            vec3d_assign( pch->cons[nhash].f, F );
            pch->cons[nhash].delta = delta;

          }

        nactual_private += ( delta > 0 ); /* has to be at the end for OMP */
}
else if ( i < nAs || j < nAs ) /* sph - dem contact */
{
  continue;
}
else /* sph - sph contact */
{
  continue;
}
      }

#   if defined( PARALLEL_OPENMP )
#   pragma omp for nowait
    for ( lpindex_t k = 0; k < nBs; ++k )
      for ( lpindex_t n = 0; n < nproc; ++n )
        {
          vec3d_inc( pBlst[k].f, ppompf[n][k] );
          vec3d_inc( pBlst[k].t, ppompt[n][k] );
          vec3d_setnull( ppompf[n][k] );
          vec3d_setnull( ppompt[n][k] );
        }
#   endif

#   if defined( PARALLEL_OPENMP )
#   pragma omp critical
    {
      nactual += nactual_private;
      if ( vc_sq_max < vc_sq_max_private )
        vc_sq_max = vc_sq_max_private;
    }
#   else
    vc_sq_max = vc_sq_max_private;
#   endif

  }  /* omp parallel region: when applied */

  pdiag -> contacts_actual_n += nactual;
  pdiag -> contacts_actual_bb_n += nactual;
  pdiag -> contacts_potential_n += ncols;
  pdiag -> contacts_potential_bb_n += ncols;

  *pvc_sq_max = vc_sq_max;
}

void forces_springs( body_t* pBlst,
                     spring_t* restrict pSlst,
                     const lpindex_t nSs )
{

# if defined( PARALLEL_OPENMP )
# pragma omp parallel for
# endif
  for ( lpindex_t k = 0; k < nSs; ++k )
    {
      /* main parameters */
      spring_t* pspring = pSlst + k;
      body_t* pbody1 = pBlst + pspring->ib1;
      body_t* pbody2 = pBlst + pspring->ib2;
      vec3d_t r;                /* r2-r1 */
      vec3d_diff( r, pspring->r2, pspring->r1 );
      real x = vec3d_abs( r );
      vec3d_t v;                /* rel velocity */
      real f;                   /* abs F */


vec3d_t rc1, rc2;     /* radius vector from center of mass
                         to the connection point */
vec3d_t vc1, vc2;     /* velocities at connection points */

vec3d_diff( rc1, pspring->r1, pbody1->c );
vec3d_diff( rc2, pspring->r2, pbody2->c );

vec3d_x( vc1, pbody1->w, rc1 );
vec3d_x( vc2, pbody2->w, rc2 );

vec3d_sum( vc1, vc1, pbody1->v );
vec3d_sum( vc2, vc2, pbody2->v );

vec3d_diff( v, vc2, vc1 );


      /* scalar value of the force */
      f = pspring->k * ( 1.0 - pspring->x0 / x ) +
          pspring->eta * vec3d_dot( r, v )/ ( x * x );

      /* vector Force */
      vec3d_t F;
      vec3d_scale( F, r, f );

      vec3d_sum( pbody1->f, pbody1->f, F );
      vec3d_diff( pbody2->f, pbody2->f, F );

      /* Torques calculation */
      vec3d_t T;

      vec3d_x( T, rc1, F );
      vec3d_sum( pbody1->t, pbody1->t, T );

      vec3d_x( T, F, rc2 );
      vec3d_sum( pbody2->t, pbody2->t, T );
    }
}

void forces_ptriangles( body_t* pBlst,
                        ptriangle_t* restrict pTlst,
                        const lpindex_t nTs )
{

# if defined( PARALLEL_OPENMP )
# pragma omp parallel for
# endif
  for ( lpindex_t k = 0; k < nTs; ++k )
    {
      /* main parameters */
      ptriangle_t* pptriag = pTlst + k;
      body_t* pbody1 = pBlst + pptriag->ib1;
      body_t* pbody2 = pBlst + pptriag->ib2;
      body_t* pbody3 = pBlst + pptriag->ib3;
      real p = pptriag->p;

      vec3d_t r12, r13, F;
      vec3d_diff( r12, pbody2->c, pbody1->c );
      vec3d_diff( r13, pbody3->c, pbody1->c );
      vec3d_x( F, r13, r12 );
      vec3d_scale( F, F, (1.0/6.0)*p );

      vec3d_sum( pbody1->f, pbody1->f, F );
      vec3d_sum( pbody2->f, pbody2->f, F );
      vec3d_sum( pbody3->f, pbody3->f, F );
    }
}
