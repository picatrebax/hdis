/*!

  \file lptimer.c

  \brief Time main structure and functions

*/

#include "lptimer.h"

/* ----------------------------------------------------------------- */

void lptimer_init( lptimer_t * pt, /* lptimer structure */
                   const real t0,  /* initial time */
                   const real te,  /* ending time */
                   const real dt,  /* initial time step */
                   const real ts,  /* next saving time (may be set
                                      to t0 or t0+dts) */
                   const real dts ) /* saving interval */
{
  pt->t0 = t0;
  pt->te = te;
  pt->t = t0;
  pt->dt = dt;
  pt->n = 0;

  pt->ts = ts;
  pt->dts = dts;
  pt->ns = 0;
}

/* ----------------------------------------------------------------- */

int lptimer_inc( lptimer_t * pt )
{
  int to_save = 0;

  pt->t += pt->dt;
  (pt->n) ++;

  /* saving to the output? */
  if ( pt->ts < pt->t + pt->dt / 100000.0  )
    {
      pt->ts += pt->dts;
      (pt->ns) ++;
      to_save = 1;
    }

  return to_save;
}

/* ----------------------------------------------------------------- */

bool lptimer_is_savetime( lptimer_t * pt )
{
  bool to_save = false;

  /* saving to the output? */
  if ( pt->ts < pt->t + pt->dt / 100000.0  )
    {
      pt->ts += pt->dts;
      (pt->ns) ++;
      to_save = true;
    }

  return to_save;
}
