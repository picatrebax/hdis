#include "dynamics.h"

void dynamics( body_t* restrict pBlst,
               const lpindex_t nBs,
               lua_State *L,
               const real dt,
               const int flags )
{
# ifdef PARALLEL_OPENMP
# pragma omp parallel for
# endif
  for( lpindex_t i = 0; i < nBs; ++i )
    {
      body_t* restrict pbody = pBlst + i;

      /* free bodies and controlled bodies are different here */
      if ( ( pbody-> flag & BODY_F_FREE ) |
           ( pbody-> flag & BODY_F_SENSITIVE ) )
        {

          /* Integration of Newton */
          {
            const real K = dt / pbody->m;
            vec3d_expy( pbody->v, pbody->f, K );
          }

          /* Integration of Eular */
          if ( flags & SETTINGS_F_OMEGA_WALTON )

            {
              rot3d_t R;     /* rotation matrix */
              vec3d_t wb;    /* angular vel. in body frame */
              vec3d_t wb_n;  /* angular vel., b.f. int step */
              vec3d_t dwb_old; /* old d\omega */
              vec3d_t Tb;      /* Torque in body frame */
              real ddwb2 = 1;  /* sq difference between d\omega */

              /* restoring rotation matrix by quaternion */
              rot3d_restore( &R, &(pbody->q) );

              /* angular velocity and differential in body frame */
              rot3d_apply_tr( &R, &wb, &(pbody->w) );
              rot3d_apply_tr( &R, &Tb, &pbody->t );

              /* update loop */
              lpindex_t n = 0;
              while ( ddwb2 >=
                      0.001*DEM_MOTION_EPS * DEM_MOTION_EPS * dt * dt ||
                      n++ < DYNAMICS_WALTON_MAXN )
                {
                  vec3d_t tmp_vec;

                  /* index 4: wb_n */
                  vec3d_scale( tmp_vec, pbody->dwb, 0.5 );
                  vec3d_sum( wb_n, wb, tmp_vec );

                  /* index 5: dwb_old */
                  vec3d_assign( dwb_old, pbody->dwb );

                  /* index 6: dwb */
                  vec3d_eular_angular_product( tmp_vec, wb_n, pbody->I );

                  vec3d_sum( tmp_vec, tmp_vec, Tb );
                  vec3d_comp_div( tmp_vec, tmp_vec, pbody->I );
                  vec3d_scale( pbody->dwb, tmp_vec, dt );

                  /* index 7: ddwb >= eps*dt */
                  ddwb2 = vec3d_sqdiff( pbody->dwb, dwb_old );
                }

              /* index 8: converting to global frame */
              vec3d_sum( wb, wb, pbody->dwb );

              rot3d_apply( &R, &pbody->w, &wb );
            }
          else
            {
              /* differential equation */
              vec3d_expy( pbody->l, pbody->t, dt );

              /* omega restoration */
              rot3d_t Rot;                  /* rotational matrix */
              rot3d_restore( &Rot, &pbody->q );
              rot3d_ang_vel( &Rot, &pbody->w, &pbody->I, &pbody->l );
            }
        }
      /* we call the control block unless the body is steady */
      else if ( pbody-> flag & BODY_F_STEADY )
        {
          ;                     /* just do nothing! */
        }
      else if ( pbody-> flag & BODY_F_STILL )
        {
          vec3d_setnull( pbody->v );
          vec3d_setnull( pbody->w );
        }
      else                      /* the body is controlled */
        {
#         ifdef PARALLEL_OPENMP
#           pragma omp critical (dynamics_controlled_critical)
#         endif
          {
            /* get the control_functions array */
            lua_getglobal( L, "hdis" );
            lua_getfield( L, -1, "control_functions" );
            if ( !lua_istable( L, 1 ) )
              fatal( 1001,
                     "Body %d error, no list of control_functions", i+1 );

            lua_rawgeti( L, -1, pbody->ifunc );

            if ( ! lua_isfunction( L, -1 ) )
              fatal( 1002,
                     "Control function is not defined for body %d\n",
                     i+1 );

            /* prepare arguments */
            lua_pushinteger( L, i+1 );
            lua_pushnumber( L, dt );
            lua_pushnumber( L, pbody->m );

            lua_pushnumber( L, vec3d_getx( pbody->v) );
            lua_pushnumber( L, vec3d_gety( pbody->v) );
            lua_pushnumber( L, vec3d_getz( pbody->v) );

            lua_pushnumber( L, vec3d_getx( pbody->w) );
            lua_pushnumber( L, vec3d_gety( pbody->w) );
            lua_pushnumber( L, vec3d_getz( pbody->w) );

            lua_pushnumber( L, vec3d_getx( pbody->f ) );
            lua_pushnumber( L, vec3d_gety( pbody->f ) );
            lua_pushnumber( L, vec3d_getz( pbody->f ) );

            lua_pushnumber( L, vec3d_getx( pbody->t ) );
            lua_pushnumber( L, vec3d_gety( pbody->t ) );
            lua_pushnumber( L, vec3d_getz( pbody->t ) );

            if ( lua_pcall( L, 15, 6, 0 ) != 0 )
              fatal( 101,
                     "Cannot call the control function for body %d\n",
                     i+1 );

            vec3d_set( pbody->v,
                       lua_tonumber( L, -6 ),
                       lua_tonumber( L, -5 ),
                       lua_tonumber( L, -4 ) );

            vec3d_set( pbody->w,
                       lua_tonumber( L, -3 ),
                       lua_tonumber( L, -2 ),
                       lua_tonumber( L, -1 ) );

            lua_settop( L, 0 );             /* clean the stack */
          }

        }
    }
}
