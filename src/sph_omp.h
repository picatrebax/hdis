/*!

  \file sph_omp.h

  \brief Parallel system for SPH

*/

#ifndef SPH_OMP_H
#define SPH_OMP_H

#include "defs.h"
#include "vec3d.h"

/*! Structure for OMP data for parallel computations */

typedef struct sph_omp_s {
  int nprocs;              /*!< number of processors  */
  real **ppj;              /*!< collectors for mstreams, ppj[nproc][i] format */
  vec3d_t** ppF;           /*!< forces accumulator  */
  
  /* auxil data */
  lpindex_t nHs;                /* saving for not memorizing later */
} sph_omp_t;

/*! initializing the parallel structure */
void sph_omp_init( sph_omp_t *pomp, const lpindex_t nHs );

/*! cleaning memory from  the mstream structure  */
void sph_omp_finalize( sph_omp_t *pomp );

#endif      /* SPH_OMP_H */
