#include "lpio.h"

/* ----------------------------------------------------------------- */

void lpio_dtype_destroy( lpio_dtype_t dtype )
{
  H5Tclose( dtype );
}

/* ----------------------------------------------------------------- */

lpio_stdtypes_t * lpio_stdtypes_alloc()
{
  lpio_stdtypes_t * ptp = malloc( sizeof( lpio_stdtypes_t ) );
  if ( !ptp ) return NULL;

  /* basic types */
  ptp->dtype_int = H5T_NATIVE_INT;
  ptp->dtype_uint16 = H5T_NATIVE_B16;
  ptp->dtype_ushort = H5T_NATIVE_USHORT;

# ifdef REAL_IS_FLOAT
  ptp->dtype_real = H5T_NATIVE_FLOAT;
# else
  ptp->dtype_real = H5T_NATIVE_DOUBLE;
# endif

  /* vec3d complex datatype creation */
  lpio_dtype_create( ptp->dtype_vec3d, vec3d_t );
  lpio_dtype_freg( ptp->dtype_vec3d, vec3d_t, x, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_vec3d, vec3d_t, y, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_vec3d, vec3d_t, z, ptp->dtype_real );

  /* rquat_t complex datatype creation */
  lpio_dtype_create( ptp->dtype_rquat, rquat_t );
  lpio_dtype_freg( ptp->dtype_rquat, rquat_t, s,  ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_rquat, rquat_t, vx, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_rquat, rquat_t, vy, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_rquat, rquat_t, vz, ptp->dtype_real );

  /* lptimer */
  lpio_dtype_create( ptp->dtype_lptimer, lptimer_t );
  lpio_dtype_freg( ptp->dtype_lptimer, lptimer_t, t,   ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_lptimer, lptimer_t, t0,  ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_lptimer, lptimer_t, te,  ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_lptimer, lptimer_t, dt,  ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_lptimer, lptimer_t, ts,  ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_lptimer, lptimer_t, dts, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_lptimer, lptimer_t, n,   ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_lptimer, lptimer_t, ns,  ptp->dtype_int );

  /* SBB */
  lpio_dtype_create( ptp->dtype_sbb, sbb_t );
  lpio_dtype_freg( ptp->dtype_sbb, sbb_t, c, ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_sbb, sbb_t, c0, ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_sbb, sbb_t, r, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_sbb, sbb_t, body_indx, ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_sbb, sbb_t, group_id, ptp->dtype_int );

  /* vert */
  lpio_dtype_create( ptp->dtype_vert, vert_t );
  lpio_dtype_freg( ptp->dtype_vert, vert_t, local_pos, ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_vert, vert_t, global_pos, ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_vert, vert_t, deg, ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_vert, vert_t, iadj_e, ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_vert, vert_t, ibody, ptp->dtype_int );

  /* atom */
  lpio_dtype_create( ptp->dtype_atom, atom_t );
  lpio_dtype_freg( ptp->dtype_atom, atom_t, R, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_atom, atom_t, sbb_Rmin, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_atom, atom_t, ri, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_atom, atom_t, body_indx, ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_atom, atom_t, group_id, ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_atom, atom_t, material_id, ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_atom, atom_t, nverts, ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_atom, atom_t, ivert0, ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_atom, atom_t, sbb_c, ptp->dtype_vec3d );

  /* body */
  lpio_dtype_create( ptp->dtype_body, body_t );
  lpio_dtype_freg( ptp->dtype_body, body_t, q, ptp->dtype_rquat );
  lpio_dtype_freg( ptp->dtype_body, body_t, c, ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_body, body_t, v, ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_body, body_t, w, ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_body, body_t, dwb, ptp->dtype_vec3d );

  lpio_dtype_freg( ptp->dtype_body, body_t, f, ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_body, body_t, t, ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_body, body_t, l, ptp->dtype_vec3d );

  lpio_dtype_freg( ptp->dtype_body, body_t, I, ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_body, body_t, m, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_body, body_t, V, ptp->dtype_real );

  lpio_dtype_freg( ptp->dtype_body, body_t, ifunc, ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_body, body_t, imonitor, ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_body, body_t, effunc, ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_body, body_t, posfunc, ptp->dtype_int );

  // lpio_dtype_freg( ptp->dtype_body, body_t, flag, ptp->dtype_uint16 );
  lpio_dtype_freg( ptp->dtype_body, body_t, flag, ptp->dtype_ushort );

  /* spring */
  lpio_dtype_create( ptp->dtype_spring, spring_t );
  lpio_dtype_freg( ptp->dtype_spring, spring_t, rb1, ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_spring, spring_t, rb2, ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_spring, spring_t, r1, ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_spring, spring_t, r2, ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_spring, spring_t, x0, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_spring, spring_t, k, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_spring, spring_t, eta, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_spring, spring_t, ib1, ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_spring, spring_t, ib2, ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_spring, spring_t, group, ptp->dtype_int );

  /* ptriangle */
  lpio_dtype_create( ptp->dtype_ptriangle, ptriangle_t );
  lpio_dtype_freg( ptp->dtype_ptriangle, ptriangle_t, ib1, ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_ptriangle, ptriangle_t, ib2, ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_ptriangle, ptriangle_t, ib3, ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_ptriangle, ptriangle_t, p,  ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_ptriangle, ptriangle_t, group, ptp->dtype_int );

  /* smoothed particle */
  lpio_dtype_create( ptp->dtype_dot, sph_t );
  lpio_dtype_freg( ptp->dtype_dot, sph_t, c,  ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_dot, sph_t, f,  ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_dot, sph_t, v,  ptp->dtype_vec3d );

  lpio_dtype_freg( ptp->dtype_dot, sph_t, j,  ptp->dtype_real );

  lpio_dtype_freg( ptp->dtype_dot, sph_t, rho,  ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_dot, sph_t, p,  ptp->dtype_real );

  lpio_dtype_freg( ptp->dtype_dot, sph_t, nu,  ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_dot, sph_t, m,  ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_dot, sph_t, R,  ptp->dtype_real );

  lpio_dtype_freg( ptp->dtype_dot, sph_t, h,  ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_dot, sph_t, a,  ptp->dtype_real );

  lpio_dtype_freg( ptp->dtype_dot, sph_t, group_id, ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_dot, sph_t, clone_id, ptp->dtype_int );

  lpio_dtype_freg( ptp->dtype_dot, sph_t, flags, ptp->dtype_int );

  /* box */
  lpio_dtype_create( ptp->dtype_box, box_t );
  lpio_dtype_freg( ptp->dtype_box, box_t, fcorner,  ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_box, box_t, xid,  ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_box, box_t, yid,  ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_box, box_t, zid,  ptp->dtype_int );

  /* contact hash */
  lpio_dtype_create( ptp->dtype_coninfo, coninfo_t );
  lpio_dtype_freg( ptp->dtype_coninfo,
                   coninfo_t, ftau_e, ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_coninfo,
                   coninfo_t, ftau_i, ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_coninfo,
                   coninfo_t, pc, ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_coninfo,
                   coninfo_t, f, ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_coninfo,
                   coninfo_t, delta, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_coninfo,
                   coninfo_t, id1, ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_coninfo,
                   coninfo_t, id2, ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_coninfo,
                   coninfo_t, nlast, ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_coninfo,
                   coninfo_t, flag, ptp->dtype_int );

  /* contact list */
  lpio_dtype_create( ptp->dtype_contact, contact_t );
  lpio_dtype_freg( ptp->dtype_contact,
                   contact_t, ftau_e, ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_contact,
                   contact_t, ftau_i, ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_contact,
                   contact_t, pc, ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_contact,
                   contact_t, f, ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_contact,
                   contact_t, delta, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_contact,
                   contact_t, id1, ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_contact,
                   contact_t, id2, ptp->dtype_int );

  /* mechanical properties of contacts */
  lpio_dtype_create( ptp->dtype_cmprops, cmprops_t );
  lpio_dtype_freg( ptp->dtype_cmprops,
                   cmprops_t, H, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_cmprops,
                   cmprops_t, gamma, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_cmprops,
                   cmprops_t, ka, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_cmprops,
                   cmprops_t, mu, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_cmprops,
                   cmprops_t, alpha_t, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_cmprops,
                   cmprops_t, CR, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_cmprops,
                   cmprops_t, vR, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_cmprops,
                   cmprops_t, Skni, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_cmprops,
                   cmprops_t, mati1, ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_cmprops,
                   cmprops_t, mati2, ptp->dtype_int );

  /* materials */
  lpio_dtype_create( ptp->dtype_material, material_t );
  lpio_dtype_freg( ptp->dtype_material,
                   material_t, rho, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_material,
                   material_t, G, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_material,
                   material_t, nu, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_material,
                   material_t, gamma, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_material,
                   material_t, CR0, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_material,
                   material_t, vR0, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_material,
                   material_t, alpha_t, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_material,
                   material_t, mu, ptp->dtype_real );

  /* conditions on the box */
  lpio_dtype_create( ptp->dtype_bc, bc_t );
  lpio_dtype_freg( ptp->dtype_bc, bc_t, zout_vel, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_bc, bc_t, material_id, ptp->dtype_int );
  lpio_dtype_freg( ptp->dtype_bc, bc_t, flags, ptp->dtype_int );

  /* portal */
  lpio_dtype_create( ptp->dtype_portal, portal_t );
  lpio_dtype_freg( ptp->dtype_portal, portal_t, v, ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_portal, portal_t, w, ptp->dtype_vec3d );
  lpio_dtype_freg( ptp->dtype_portal, portal_t, entrance_y, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_portal, portal_t, exit_y, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_portal, portal_t, exit_z, ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_portal, portal_t, flag, ptp->dtype_int );
  
  /* SPH hydro liquid properties */
  lpio_dtype_create( ptp->dtype_sphprops, sph_props_t );
  lpio_dtype_freg( ptp->dtype_sphprops, sph_props_t, rho0,  ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_sphprops, sph_props_t, c0,  ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_sphprops, sph_props_t, c2,  ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_sphprops, sph_props_t, gamma,  ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_sphprops, sph_props_t, eps,  ptp->dtype_real );
  lpio_dtype_freg( ptp->dtype_sphprops, sph_props_t, alpha,  ptp->dtype_real );

  return ptp;
}

/* ----------------------------------------------------------------- */

void lpio_stdtypes_free( lpio_stdtypes_t* ptp )
{
  /* no need to close int and real types */

  H5Tclose( ptp->dtype_vec3d );
  H5Tclose( ptp->dtype_rquat );
  H5Tclose( ptp->dtype_lptimer );
  H5Tclose( ptp->dtype_box );
}

/* ----------------------------------------------------------------- */

lpio_t * lpio_alloc( char* filename, unsigned int access )
{
  lpio_t * pio = malloc( sizeof( lpio_t ) );
  pio->filename = strdup( filename );
  pio->flags = access;

  /* we either create file or open it depending on access type */
  if ( access == LPIO_ACCESS_F_OVERWRITE ||
       access == LPIO_ACCESS_F_EXCLUSIVE )
    pio->fileid = H5Fcreate( filename, access, H5P_DEFAULT, H5P_DEFAULT);
  else
    pio->fileid = H5Fopen ( filename, access, H5P_DEFAULT );

  return pio;
}

/* ----------------------------------------------------------------- */

void lpio_free( lpio_t* pio )
{
  /* close the file */
  H5Fclose( pio->fileid );

  /* release memory */
  free( pio->filename );
  free( pio );
}

/* ----------------------------------------------------------------- */

void lpio_save_attribute( hid_t dataset, const char* aname, const char* s )
{
  hid_t attr_dspace;
  hid_t attr_strtype;
  hid_t attr;

  attr_dspace  = H5Screate( H5S_SCALAR );
  attr_strtype = H5Tcopy( H5T_C_S1 );

  H5Tset_size( attr_strtype, strlen( s ) );
  attr = H5Acreate( dataset, aname,
                    attr_strtype, attr_dspace,
                    H5P_DEFAULT, H5P_DEFAULT );

  H5Awrite( attr, attr_strtype, s );

  H5Aclose( attr );
  H5Sclose ( attr_dspace );
}

/* ----------------------------------------------------------------- */

int lpio_save( lpio_t* pio,           /* I/O struct */
               lpio_dtype_t dtype,    /* data type */
               const char* dataname,  /* name for dataset */
               void* data,            /* array */
               int len,               /* length of array */
               const char* units,     /* string with units */
               const char* description /* description string */
               )
{
  herr_t status;                /* error code */
  hid_t dataspace, dataset;     /* dataspace and dataset for HDF5 */

  /* dataspace creation depends on the len value */
  if ( len == 0 )
    {
      dataspace = H5Screate ( H5S_SCALAR );
    }
  else
    {
      hsize_t dims[1];              /* dims[0] = length of array */
      dims[0] = len;
      dataspace = H5Screate_simple (1, dims, NULL );
    }
  assert( dataspace >= 0 );

  /* dataset creation is the same */
  dataset = H5Dcreate ( pio->fileid, dataname,
                        dtype, dataspace,
                        H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  assert( dataset >= 0 );

  status = H5Dwrite ( dataset, dtype,
                      H5S_ALL, H5S_ALL, H5P_DEFAULT,
                      data );

  /* Saving units and long_names if they are not NULL */
  if ( units )
    lpio_save_attribute( dataset, "Physical units", units );

  if ( description )
    lpio_save_attribute( dataset, "Description", description );

  /* -- END attributes */

  /* freeing memory from dataset and dataspace in HDF */
  H5Dclose (dataset);
  H5Sclose (dataspace);

  return status;

}

/* ----------------------------------------------------------------- */

int lpio_save_value( lpio_t* pio,           /* I/O struct */
                     lpio_dtype_t dtype,    /* data type */
                     const char* dataname,  /* name for dataset */
                     void* data,            /* pointer to the data */
                     const char* units,     /* string with units */
                     const char* description /* description string */
                     )
{
  return lpio_save( pio, dtype, dataname, data, 0, units, description );
}

/* ----------------------------------------------------------------- */

int lpio_save_array( lpio_t* pio,           /* I/O struct */
                     lpio_dtype_t dtype,    /* data type */
                     const char* dataname,  /* name for dataset */
                     void* data,            /* array */
                     int len,               /* length of array */
                     const char* units,     /* string with units */
                     const char* description /* description string */
                     )
{
  return lpio_save( pio, dtype, dataname, data, len, units, description );
}

/* ----------------------------------------------------------------- */

void lpio_save_global_string_attribute( lpio_t* pio,
                                        const char* aname,
                                        const char* avalue )
{
  hid_t attr_dspace;
  hid_t attr_strtype;
  hid_t attr;

  attr_dspace  = H5Screate( H5S_SCALAR );
  attr_strtype = H5Tcopy( H5T_C_S1 );

  H5Tset_size( attr_strtype, strlen( avalue ) );
  attr = H5Acreate( pio->fileid, aname,
                    attr_strtype, attr_dspace,
                    H5P_DEFAULT, H5P_DEFAULT );

  H5Awrite( attr, attr_strtype, avalue );

  H5Aclose( attr );
  H5Sclose ( attr_dspace );
}

/* ----------------------------------------------------------------- */

int lpio_save_astrings ( lpio_t *pio,
                         char** pS,              /* array of strings */
                         lpindex_t N,            /* length of the array */
                         const char* dataname,   /* name of the array */
                         const char* description /* description string */
                       )
{
  herr_t status;                /* error code */
  hid_t dataspace, dataset;   /* dataspace and dataset for HDF5 */
  hid_t hid_str;              /* datatype variable length string */

  /* create a string of variable length */
  hid_str = H5Tcopy( H5T_C_S1 );
  H5Tset_size( hid_str, H5T_VARIABLE );

  /* dataspace for array of something of length N */
  hsize_t dims[1];
  dims[0] = N;
  dataspace = H5Screate_simple( 1, dims, NULL );
  assert( dataspace >= 0 );

  /* dataset creation */
  dataset = H5Dcreate( pio->fileid, dataname,
                       hid_str, dataspace,
                       H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  assert( dataset >= 0 );

  status = H5Dwrite ( dataset, hid_str,
                      H5S_ALL, H5S_ALL, H5P_DEFAULT,
                      pS );

  /* save attribute description */
  if ( description )
    lpio_save_attribute( dataset, "Description", description );

  /* freeing memory from dataset and dataspace in HDF */
  H5Dclose (dataset);
  H5Sclose (dataspace);
  H5Tclose ( hid_str );

  return status;
}

/* ----------------------------------------------------------------- */

size_t lpio_get_dim( lpio_t * pio, const char * name )
{
  /* first we check if the dataset exists */
  if ( ! H5Lexists( pio->fileid, name, H5P_DEFAULT ) )
    return LPIO_DATASET_MISSING;

  hid_t dataset = H5Dopen( pio->fileid, name, H5P_DEFAULT );
  assert( dataset >= 0 );

  hid_t filespace = H5Dget_space( dataset );
  hsize_t dims[1];              /* dims[0] = length of array */
  (void) H5Sget_simple_extent_dims( filespace, dims, NULL );

  H5Sclose( filespace );
  H5Dclose( dataset );

  return dims[0];
}

/* ----------------------------------------------------------------- */

int lpio_read( lpio_t * pio, const char* name, void* buff )
{
  /* first we check if the dataset exists */
  if ( ! H5Lexists( pio->fileid, name, H5P_DEFAULT ) )
    return LPIO_DATASET_MISSING;

  hid_t dataset = H5Dopen( pio->fileid, name, H5P_DEFAULT );
  assert( dataset >= 0 );

  hid_t datatype = H5Dget_type( dataset );

  int status = H5Dread( dataset, datatype, H5S_ALL,
                        H5S_ALL, H5P_DEFAULT, buff );

  H5Dclose( dataset );
  return status;
}

/* ----------------------------------------------------------------- */

int lpio_read_astrings ( lpio_t *pio,
                         char** pS,              /* array of strings */
                         lpindex_t N,            /* length of the array */
                         const char* dataname    /* name of the array */
                       )
{
  herr_t status;              /* error code */

  /* first we check if the dataset exists */
  if ( ! H5Lexists( pio->fileid, dataname, H5P_DEFAULT ) )
    return LPIO_DATASET_MISSING;

  /* create a type of string of variable length */
  hid_t datatype = H5Tcopy( H5T_C_S1 );
  H5Tset_size( datatype, H5T_VARIABLE );

  /* reading  */
  hid_t dataset = H5Dopen( pio->fileid, dataname, H5P_DEFAULT );
  assert( dataset >= 0 );

  status = H5Dread( dataset, datatype, H5S_ALL,
                    H5S_ALL, H5P_DEFAULT, pS );

  H5Tclose( datatype );
  H5Dclose( dataset );

  return (int)status;
}
