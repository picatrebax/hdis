/*!

  \file ptriangle.h

  \brief Pressure triangles structure

*/

#ifndef PTRIANGLE_H
#define PTRIANGLE_H

#include "defs.h"

typedef struct ptriangle_s {
  lpindex_t ib1;		/* body 1 index */
  lpindex_t ib2;		/* body 2 index */
  lpindex_t ib3;		/* body 3 index */
  real p;                       /* pressure */
  lpindex_t group;              /* ptriangle group id */
} ptriangle_t;


#endif  /* PTRIANGLE_H */

