/*!

  \file box.c

  \brief Domain dimensions

*/

#include "box.h"

void box_init( box_t * pB,
               const real x, const real y, const real z )
{
  assert( x > 0 && y > 0 && z > 0 );
  vec3d_set( pB->fcorner, x,y,z );
}

