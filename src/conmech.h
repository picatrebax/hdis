/*!

  \file conmech.h

  \brief Contact mechanics

*/

#ifndef CONMECH_H
#define CONMECH_H

#include <assert.h>

#include "defs.h"
#include "vec3d.h"
#include "atom.h"
#include "body.h"
#include "lpdyna.h"

/* Two statuses for conmech_forces  */
#define CONMECH_CONTACT_NO  0   /*!< no contact detected anymore */
#define CONMECH_CONTACT_YES 1   /*!< there was a contact, forces */

typedef struct material_s {
  real rho;                     /* Density, kg/m^3 */
  real G;                       /* Shear modulus, Pa */
  real nu;                      /* Poisson coef., 1 */
  real gamma;                   /* Surface energy, J/m^2 */
  real CR0;                     /* Standard restitution, 1 */
  real vR0;                     /* Standard impact velocity, m/s */
  real alpha_t;                 /* Tangent elastic reduction, 1 */
  real mu;                      /* Friction coefficient */
} material_t;

typedef struct cmprops_s {
  real H;                       /* Hertz modulus, Pa */
  real gamma;                   /* Surface energy for contact,
                                                 J/m^2 */
  real ka;                      /* Adhesion coefficient */
  real mu;                      /* Friction coefficient, 1 */
  real alpha_t;                 /* Tangent force reduction */
  real CR;                      /* Restitution coefficient */
  real vR;                      /* Velocity when CR was measured */
  real Skni;                    /* Normal damping coefficient,
                                   precalculated part. (CR,vR) */
  
  lpindex_t mati1;              /* First material index */
  lpindex_t mati2;              /* Second material index */
} cmprops_t;


#define conmech_hertz_radius_eff( ri, rj )      \
  ( 1.0 / ( 1.0/(ri) + 1.0/(rj) ) )
#define conmech_mass_eff( mi, mj ) (1.0/( 1.0/(mi) + 1.0/(mj) ))

#define conmech_hertz_coeff( G_i, nu_i, G_j, nu_j )     \
  ( (8.0/3.0) / ( (1.0-(nu_i))/(G_i) + (1.0-(nu_j))/(G_j) ) )

#define conmech_hertz_kne( H, r, delta ) ( (H)*sqrt( (r)*(delta) ) )

#define conmech_hertz_fne( kne, delta ) ( (kne)*(delta) )

#define conmech_surface_energy( gamma1, gamma2 )    \
 ( (gamma1) >= 0 && (gamma2) >= 0 ? 0.5 * ((gamma1)+(gamma2)) : 0.0 )

static const real CONMECH_CR_ALPHA = 0.970634802905809;
static const real CONMECH_CR_BETA0 = 0.147660959584447;
static const real CONMECH_CR_BETA1 = 0.717798161266698;
static const real CONMECH_CR_BETA2 = 0.108231667218011;

static const real CONMECH_CR_B0    = 1.1680344955318;
static const real CONMECH_CR_B1    = 3.364954982140286;

static const real CONMECH_CR_R     = 10.0664633678364;
static const real CONMECH_CR_C     = 11.84791754242951;

real conmech_CR_CR( real b );
real conmech_CR_b( real CR );
real conmech_CR_kni( real b, real v0, real r, real H, real m );

#define conmech_CR_average( CR1, CR2 ) \
                         sqrt( 0.5*((CR1)*(CR1)+(CR2)*(CR2)) )

#define conmech_fni( kni, delta, vn ) \
  ( delta > 0? - (kni) * sqrt(delta) * (vn): 0 )

#define conmech_fte( fres,phi,phin,fold,fn,vctau,n,dt,kt,mu )        \
  {                                                                  \
    /* calculate the scaled shift -kt*ds */                          \
    vec3d_t ds_kt;                                                   \
    vec3d_scale( ds_kt, vctau, kt*dt );                              \
                                                                     \
    /*  calculate Phi_tau */                                         \
    vec3d_proj( phi, phin, fold, n );                                \
                                                                     \
    /* calculate direction and xi */                                 \
    vec3d_diff( fres, phi, ds_kt );                                  \
    real abs_fres = vec3d_abs( fres );                               \
    real xi = abs_fres > 0 ? (mu) * fabs(fn) / abs_fres : 0.0;       \
    if ( xi < 1.0 )                                                  \
      vec3d_scale( fres, fres, xi );                                 \
  }

/* ----------------------------------------------------------------- */

/*! \brief Forces on the contact

  \param pf Total force on the contact
  \param pftau_e elastic tangential force on the contact
  \param[in] d overlap between atoms (can be negative now!)
  \param[in] pv velocity vector on contact
  \param[in] pn normal vector at the contact
  \param[in] pcmprop contact mech. properties
  \param[in] R effective interaction radius
  \param[in] kni normal damping linear coefficient
  \param[in] dt timestep

  \return contact status (CONMECH_CONTACT or CONMECH_NO_CONTACT)

*/
int conmech_forces( vec3d_t* pf,
                    vec3d_t* pftau_e,
                    const real d,
                    const vec3d_t* pv,
                    const vec3d_t* pn,
                    const cmprops_t* pcmprop,
                    const real R,
                    const real kni,
                    const real dt );

#endif  /* CONMECH_H */
