#include "rot3d.h"

/*
   rot3d R   - [input]  rotation operator,
   vec3d a_g - [output] calculated coordinates of a in global frame,
   vec3d a_b - [input]  coordinates of a in body frame
 */
void rot3d_apply( const rot3d_t* restrict pR,
                        vec3d_t* restrict pa_g,
                  const vec3d_t* restrict pa_b )

{
  real exx = vec3d_getx( pR->ex );
  real exy = vec3d_gety( pR->ex );
  real exz = vec3d_getz( pR->ex );

  real eyx = vec3d_getx( pR->ey );
  real eyy = vec3d_gety( pR->ey );
  real eyz = vec3d_getz( pR->ey );

  real ezx = vec3d_getx( pR->ez );
  real ezy = vec3d_gety( pR->ez );
  real ezz = vec3d_getz( pR->ez );

  real ax = vec3d_getx( *pa_b );
  real ay = vec3d_gety( *pa_b );
  real az = vec3d_getz( *pa_b );

  vec3d_setx( *pa_g, exx*ax + eyx*ay + ezx*az );
  vec3d_sety( *pa_g, exy*ax + eyy*ay + ezy*az );
  vec3d_setz( *pa_g, exz*ax + eyz*ay + ezz*az );
}

/*
   rot3d R   - [input]  rotation operator,
   vec3d a_b - [output] calculated coordinates of a in body frame,
   vec3d a_g - [input]  coordinates of a in global frame
 */
void rot3d_apply_tr( const rot3d_t* restrict pR,
                           vec3d_t* restrict pa_b,
                     const vec3d_t* restrict pa_g )


{
  vec3d_set( *pa_b,
             vec3d_dot( pR->ex, *pa_g ),
             vec3d_dot( pR->ey, *pa_g ),
             vec3d_dot( pR->ez, *pa_g ) );
}

/*
   pRt - [output] R transposed
   pR  - [input]  R
 */
void rot3d_tr( rot3d_t * restrict pRt, const rot3d_t * restrict pR )
{
  vec3d_set ( pRt->ex,
              vec3d_getx(pR->ex),
              vec3d_getx(pR->ey),
              vec3d_getx(pR->ez) );
  vec3d_set ( pRt->ey,
              vec3d_gety(pR->ex),
              vec3d_gety(pR->ey),
              vec3d_gety(pR->ez) );
  vec3d_set ( pRt->ez,
              vec3d_getz(pR->ex),
              vec3d_getz(pR->ey),
              vec3d_getz(pR->ez) );
}

real rot3d_det( const rot3d_t * restrict pR )
{
  vec3d_t xprod;
  vec3d_x( xprod, pR->ey, pR->ez );
  return vec3d_dot( pR->ex, xprod );
}

void rot3d_restore( rot3d_t * restrict pR, const rquat_t * restrict pq )
{
  real s = pq->s;
  real x = pq->vx;
  real y = pq->vy;
  real z = pq->vz;

  real tx = 2*x;
  real ty = 2*y; 
  real tz = 2*z;

  real tsx = s*tx;
  real tsy = s*ty;
  real tsz = s*tz;

  real txx = x*tx;
  real txy = x*ty;
  real txz = x*tz;

  real tyy = y*ty;
  real tyz = y*tz;

  real tzz = z*tz;

  vec3d_set( pR->ex, 1-tyy-tzz, txy+tsz, txz-tsy );
  vec3d_set( pR->ey, txy-tsz, 1-txx-tzz, tyz+tsx );
  vec3d_set( pR->ez, txz+tsy, tyz-tsx, 1-txx-tyy );
}

void rot3d_ang_vel( const rot3d_t* restrict pR, 
                    vec3d_t* restrict pw, 
                    const vec3d_t* restrict pIb, 
                    const vec3d_t* restrict pL )
{
  /* components of I^{-1} in global frame */
  real Ixx, Iyy, Izz, Ixy, Ixz, Iyz;

  /* rotation matrix coefficients */
  real rxx = vec3d_getx( pR->ex );
  real ryx = vec3d_gety( pR->ex );
  real rzx = vec3d_getz( pR->ex );

  real rxy = vec3d_getx( pR->ey );
  real ryy = vec3d_gety( pR->ey );
  real rzy = vec3d_getz( pR->ey );

  real rxz = vec3d_getx( pR->ez );
  real ryz = vec3d_gety( pR->ez );
  real rzz = vec3d_getz( pR->ez );

  /* calculations */
  real Ix = vec3d_getx( *pIb );
  real Iy = vec3d_gety( *pIb );
  real Iz = vec3d_getz( *pIb );

  Ixx = rxx*rxx/Ix + rxy*rxy/Iy + rxz*rxz/Iz;
  Iyy = ryx*ryx/Ix + ryy*ryy/Iy + ryz*ryz/Iz;
  Izz = rzx*rzx/Ix + rzy*rzy/Iy + rzz*rzz/Iz;

  Ixy = rxx*ryx/Ix + rxy*ryy/Iy + rxz*ryz/Iz;
  Ixz = rxx*rzx/Ix + rxy*rzy/Iy + rxz*rzz/Iz;
  Iyz = ryx*rzx/Ix + ryy*rzy/Iy + ryz*rzz/Iz;

  real lx = vec3d_getx( *pL );
  real ly = vec3d_gety( *pL );
  real lz = vec3d_getz( *pL );

  vec3d_setx( *pw, lx*Ixx + ly*Ixy + lz*Ixz );
  vec3d_sety( *pw, lx*Ixy + ly*Iyy + lz*Iyz );
  vec3d_setz( *pw, lx*Ixz + ly*Iyz + lz*Izz );
}

void rot3d_ang_mom( const rot3d_t* restrict pR, 
                    vec3d_t* restrict pL, 
                    const vec3d_t* restrict pIb, 
                    const vec3d_t* restrict pw )
{
  /* components of I^{-1} in global frame */
  real Ixx, Iyy, Izz, Ixy, Ixz, Iyz;

  /* rotation matrix coefficients */
  real rxx = vec3d_getx( pR->ex );
  real ryx = vec3d_gety( pR->ex );
  real rzx = vec3d_getz( pR->ex );

  real rxy = vec3d_getx( pR->ey );
  real ryy = vec3d_gety( pR->ey );
  real rzy = vec3d_getz( pR->ey );

  real rxz = vec3d_getx( pR->ez );
  real ryz = vec3d_gety( pR->ez );
  real rzz = vec3d_getz( pR->ez );

  /* calculations */
  real Ix = vec3d_getx( *pIb );
  real Iy = vec3d_gety( *pIb );
  real Iz = vec3d_getz( *pIb );

  Ixx = rxx*rxx*Ix + rxy*rxy*Iy + rxz*rxz*Iz;
  Iyy = ryx*ryx*Ix + ryy*ryy*Iy + ryz*ryz*Iz;
  Izz = rzx*rzx*Ix + rzy*rzy*Iy + rzz*rzz*Iz;

  Ixy = rxx*ryx*Ix + rxy*ryy*Iy + rxz*ryz*Iz;
  Ixz = rxx*rzx*Ix + rxy*rzy*Iy + rxz*rzz*Iz;
  Iyz = ryx*rzx*Ix + ryy*rzy*Iy + ryz*rzz*Iz;

  real lx = vec3d_getx( *pw );
  real ly = vec3d_gety( *pw );
  real lz = vec3d_getz( *pw );

  vec3d_setx( *pL, lx*Ixx + ly*Ixy + lz*Ixz );
  vec3d_sety( *pL, lx*Ixy + ly*Iyy + lz*Iyz );
  vec3d_setz( *pL, lx*Ixz + ly*Iyz + lz*Izz );
}

void rot3d_pprint( const rot3d_t* pR )
{
  const char* lformat = "%f\t%f\t%f\n";
  printf( lformat, vec3d_getx(pR->ex), vec3d_getx(pR->ey), vec3d_getx(pR->ez) ); 
  printf( lformat, vec3d_gety(pR->ex), vec3d_gety(pR->ey), vec3d_gety(pR->ez) ); 
  printf( lformat, vec3d_getz(pR->ex), vec3d_getz(pR->ey), vec3d_getz(pR->ez) ); 
}


