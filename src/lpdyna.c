/*!

  \file lpdyna.c

  \brief Dynamic array implementation

*/

#include "lpdyna.h"

void lpdyna_add( lpdyna_t* pa, const void* pe )
{
# if defined( PARALLEL_OPENMP ) && defined( _OPENMP )
# pragma omp critical (lpdyna_add_critical)
# endif
  {
    /* extending the array if necessary */
    if ( pa->n >= pa->capacity )
      {
        while( pa->capacity <= pa->n )
          {
            pa->capacity += pa->dn;
          }
        pa->data = realloc( pa->data,
                            pa->capacity * pa->datasize );
      }

    /* adding the element */
    memcpy( (char*) pa->data + ( pa->datasize * pa->n ),
            pe, pa->datasize );
    pa->n ++;
  }
}

int lpdyna_init( lpdyna_t* pa, size_t datasize,
                 size_t capacity, size_t increment )
{
  int err = LPDYNA_OK;       /* for error codes */

  /* allocation */
  pa->data = malloc( capacity * datasize );
  if ( !pa->data ) return LPDYNA_ERROR_ALLOC;

  /* initialization */
  pa->datasize = datasize;
  pa->capacity = capacity;
  pa->n = 0;
  pa->dn = increment;

  return err;
}

void lpdyna_destroy( lpdyna_t * pa )
{
  free( pa->data );
}
