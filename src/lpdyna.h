/*!

  \file lpdyna.h

  \brief Dynamic array (general)

*/

#ifndef LPDYNA_H
#define LPDYNA_H

#include <string.h>
#include <stdlib.h>

#include "defs.h"


#define LPDYNA_OK   0
#define LPDYNA_ERROR_ALLOC  1
#define LPDYNA_ERROR_MUTEX  2



typedef struct lpdyna_s
{
  lpindex_t n;                  /* number of elements in the array */
  lpindex_t capacity;           /* for how many elements memory is
                                   allocated */
  lpindex_t dn;                 /* increment in number of elements
                                   when reallocating memory */
  void* data;                   /* the actual data */
  size_t datasize;              /* the actual data element size as in
                                   sizeof for polymorphism of
                                   operators */
} lpdyna_t;



int lpdyna_init( lpdyna_t* pa, size_t datasize,
                 size_t capacity, size_t increment );

void lpdyna_destroy( lpdyna_t *pa );

#define lpdyna_clean( pa )                       \
  {                                              \
    (pa)->n = 0;                                 \
  }

void lpdyna_add( lpdyna_t* pa, const void* pe );

#define lpdyna_get( pa, pe, i )                                 \
  memcpy( pe, (typeof(pe))((pa)->data) + (i), (pa)->datasize );

#define lpdyna_length( pa ) ((pa)->n)


#endif  /* LPDYNA_H */

