#include "conhash.h"

coninfo_t* conhash_init( conhash_t* pch, const lpindex_t maxn )
{
  /* adjusting maxn to the nearest bigger prime */
  pch->maxn = maxn;
  while( ! is_prime( pch->maxn ) ) pch->maxn++;

  /* allocating memory */
  pch->cons = malloc( pch->maxn * sizeof( coninfo_t ) );

  /* setting up all flags to free */
  for ( lpindex_t i = 0; i < pch->maxn; ++i )
    {
      pch->cons[i].flag = CONHASH_F_FREE;

      /* below is the code for monitoring in HDF only, can be removed!!!*/
      pch->cons[i].nlast = 0;
      pch->cons[i].id1 = 0;
      pch->cons[i].id2 = 0;
      vec3d_setnull(  pch->cons[i].ftau_e  );
      vec3d_setnull(  pch->cons[i].ftau_i  );
      vec3d_setnull(  pch->cons[i].pc  );
      vec3d_setnull(  pch->cons[i].f  );
    }

  return pch->cons;
}
void conhash_destroy( conhash_t* pch )
{
  free( pch->cons );
  pch->cons = NULL;
}

lpindex_t conhash_ins( conhash_t* pch,
                       const coninfo_t* pel )
{
  unsigned int h1 = conhash_hash1( pel->id1, pel->id2 );
  unsigned int h2 = conhash_hash2( pel->id1, pel->id2, pch->maxn );

  int q = 0;
  lpindex_t i;

  while ( q < pch->maxn )
    {
      i = ( h1 + q * h2 ) % pch->maxn;

      if ( (pch->cons)[i].flag & CONHASH_F_OCCUPIED )
        {
          if ( (pch->cons)[i].id1 == pel->id1      &&
               (pch->cons)[i].id2 == pel->id2 )
            goto ins_finalize;  /* _nice_ way to break the loop */
        }
      else goto ins_finalize;

      ++q;                      /* increase the counter */
    }

  /* number of misses is just the counter */
  return (lpindex_t) CONHASH_ERROR_INSERT;

  /* here we found the spot and inserted the value */
 ins_finalize:
  
(pch->cons)[i].flag = CONHASH_F_OCCUPIED;
(pch->cons)[i].id1 = pel->id1;
(pch->cons)[i].id2 = pel->id2;
(pch->cons)[i].nlast = pel->nlast;
vec3d_assign( (pch->cons)[i].ftau_e, pel->ftau_e );
vec3d_assign( (pch->cons)[i].ftau_i, pel->ftau_i );
vec3d_assign( (pch->cons)[i].pc, pel->pc );
vec3d_assign( (pch->cons)[i].f, pel->f );

  return i;
}

lpindex_t conhash_size( conhash_t* pch )
{
  lpindex_t counter = 0;
  for( lpindex_t i = 0; i < pch->maxn; ++i )
    {
      if ( pch->cons[i].flag & CONHASH_F_OCCUPIED )
        counter ++;
    }

  return counter;
}

lpindex_t conhash_del( conhash_t* pch,
                       const lpindex_t id1, const lpindex_t id2 )
{
  unsigned int h1 = conhash_hash1( id1, id2 );
  unsigned int h2 = conhash_hash2( id1, id2, pch->maxn );
  int q = 0;

  for ( q = 0; q < pch->maxn; ++q )
    {
      lpindex_t i = ( h1 + q * h2 ) % pch->maxn;

      if ( (pch->cons)[i].flag & CONHASH_F_OCCUPIED )
        {
          if ( (pch->cons)[i].id1 == id1      &&
               (pch->cons)[i].id2 == id2 )
            {
              (pch->cons)[i].flag = CONHASH_F_VACANT;
              return i;
            }
        }
      else if ( (pch->cons)[i].flag == 0 )
        return CONHASH_ERROR_DELETE;      /* no element found */
    }

  /* we are not supposed to reach this place */
  return CONHASH_ERROR_DELETE;
}


lpindex_t conhash_lookup( conhash_t * pch,
                          const lpindex_t id1, const lpindex_t id2 )
{
  
unsigned int h1, h2;          /* hashes */
lpindex_t i;                  /* index in the table to find */
int q = 0;                    /* number of misses */

  
/* h1  compute */
h1 = conhash_hash1( id1, id2 );

/* Test for q = 0 with no misses */
i = h1 % pch->maxn;
if ( (pch->cons)[i].flag & CONHASH_F_OCCUPIED )
  {
    if ( (pch->cons)[i].id1 == id1      &&
         (pch->cons)[i].id2 == id2 ) goto lookup_found;
  }
else if ( (pch->cons)[i].flag == 0 ) goto lookup_miss;


/* First miss. We need second hash */
h2 = conhash_hash2( id1, id2, pch->maxn );

for ( q = 1; q < pch->maxn; ++q )
  {
    i = ( h1 + q * h2 ) % pch->maxn;
    
if ( (pch->cons)[i].flag & CONHASH_F_OCCUPIED )
  {
    if ( (pch->cons)[i].id1 == id1      &&
         (pch->cons)[i].id2 == id2 ) goto lookup_found;
  }
else if ( (pch->cons)[i].flag == 0 ) goto lookup_miss;

  }

/* Outside the loop means miss! */
goto lookup_miss;


  /* two different scenarios are made with labels for clearity */

 lookup_miss:   return CONHASH_ERROR_LOOKUP;
 lookup_found:  return i;
}

lpindex_t conhash_lookup_and_adjust( conhash_t* pch,
                                     lpindex_t i, lpindex_t j,
                                     size_t ncurrent )
{
  /* contact hash request for the contact */
  lpindex_t nhash = conhash_lookup( pch, i, j );

  /* is this contact new? */
  if ( nhash == CONHASH_ERROR_LOOKUP )
    {
      coninfo_t ci_old;               /* old forces etc. */
      ci_old.id1 = i;
      ci_old.id2 = j;
			
      vec3d_setnull( ci_old.ftau_e );
      vec3d_setnull( ci_old.ftau_i );
      nhash = conhash_ins( pch, &ci_old );
    }

  /* is this just a reestablished contact? */
  else if ( pch->cons[nhash].nlast != ncurrent-1 )
    {
      vec3d_setnull( pch->cons[nhash].ftau_e );
      vec3d_setnull( pch->cons[nhash].ftau_i );
    }

  /* new contact established */
  pch->cons[nhash].nlast = ncurrent;

  return nhash;
}

int is_prime( lpindex_t n )
{
  /* 2 and 3 are primes */
  if ( n == 2 || n == 3 )   return 1;

  /* 0 and 1 are not primes */
  if ( n == 1 || n == 0 )   return 0;

  /* all other primes do not divide 2 and 3 */
  if ( n % 2 == 0 || n % 3 == 0 )   return 0;

  /* trying all prime candidates up to sqrt(n) */
  lpindex_t sqn = (lpindex_t) sqrt( n + 0.001 );

  for( lpindex_t i = 5; i <= sqn; i += 6 )
    {
      lpindex_t i0 = i;
      lpindex_t i1 = i + 2;
      if ( n % i0 == 0 || n % i1 == 0 )   return 0;
    }

  /* only primes left here */
  return 1;
}


static lpindex_t conhash_lookup_spec( conhash_t * pch,
                                      const lpindex_t id1,
                                      const lpindex_t id2,
                                      int* qrel )
{
  
unsigned int h1, h2;          /* hashes */
lpindex_t i;                  /* index in the table to find */
int q = 0;                    /* number of misses */

  
/* h1  compute */
h1 = conhash_hash1( id1, id2 );

/* Test for q = 0 with no misses */
i = h1 % pch->maxn;
if ( (pch->cons)[i].flag & CONHASH_F_OCCUPIED )
  {
    if ( (pch->cons)[i].id1 == id1      &&
         (pch->cons)[i].id2 == id2 ) goto lookup_found;
  }
else if ( (pch->cons)[i].flag == 0 ) goto lookup_miss;


/* First miss. We need second hash */
h2 = conhash_hash2( id1, id2, pch->maxn );

for ( q = 1; q < pch->maxn; ++q )
  {
    i = ( h1 + q * h2 ) % pch->maxn;
    
if ( (pch->cons)[i].flag & CONHASH_F_OCCUPIED )
  {
    if ( (pch->cons)[i].id1 == id1      &&
         (pch->cons)[i].id2 == id2 ) goto lookup_found;
  }
else if ( (pch->cons)[i].flag == 0 ) goto lookup_miss;

  }

/* Outside the loop means miss! */
goto lookup_miss;


  /* two different scenarios are made with labels for clearity */

 lookup_miss:
  (*qrel) = q;
  return CONHASH_ERROR_LOOKUP;

 lookup_found:
  (*qrel) = q;
  return i;
}


void conhash_lookup_success_cost( conhash_t* pch,
                                  double* pload,
                                  double* pmisses,
                                  lpindex_t* pNvac,
                                  lpindex_t* pNfree )
{
  /* big test of the table */
  (*pNvac)  = 0;
  (*pNfree) = 0;
  lpindex_t Sq = 0;          /* sum of all misses */
  lpindex_t N = 0;           /* number of traversed elements */

  for ( lpindex_t i = 0; i < pch->maxn; ++i )
    {
      /* skip an element if not occupied */
      if ( ! ( pch->cons[i].flag & CONHASH_F_OCCUPIED ) )
        {
          /* count free/vacant positions */
          if ( pch->cons[i].flag == 0 ) (*pNfree) ++;
          else (*pNvac) ++;
          continue;
        }

      /* count misses by a special lookup function */
      int id1 = pch->cons[i].id1;
      int id2 = pch->cons[i].id2;
      int q = 0;
      (void) conhash_lookup_spec( pch, id1, id2, &q );
      Sq += q;
      N++;
    }

  /* average number of misses */
  if ( N == 0 ) (*pmisses) = 0;
  else (*pmisses) = (double) Sq / (double) N;

  /* load calculation */
  (*pload) = (double) N / (double) pch->maxn;
}

double conhash_lookup_miss_cost( conhash_t* pch,
                                 lpindex_t reps,
                                 lpindex_t range )
{
  double S = 0;                    /* number of misses */
  double irep = 0;

  while ( irep < reps )
    {
      /* generate a pair if indices as two equally distributed
         numbers by a generator */
      lpindex_t a = (lpindex_t) ( range * uniform_deviate( rand() ) );
      lpindex_t b = (lpindex_t) ( range * uniform_deviate( rand() ) );

      /* make the pair ordered as we have in DEM */
      if ( a > b )
        {
          lpindex_t tmp = a; a = b; b = tmp;
        }

      /* look up if we have them in the table */
      int q = 0;
      if ( conhash_lookup_spec( pch, a, b, &q ) == CONHASH_ERROR_LOOKUP )
        {
          S += q;
          irep ++;
        }
    }

  return S/reps;
}

void conhash_cleanup( conhash_t* pch )
{
  /* allocating new empty memory */
  coninfo_t* newcons = malloc( pch->maxn * sizeof( coninfo_t ) );

  /* setting up all flags to free */
  for ( lpindex_t i = 0; i < pch->maxn; ++i )
    newcons[i].flag = CONHASH_F_FREE;

  /* swapping the pointers */
  coninfo_t* tmp = pch->cons;
  pch->cons = newcons;

  /* rebuilding the hash in reverse order (it has slightly better
     property) */
  for( lpindex_t i = pch->maxn-1; i >=0; --i )
    {
      if ( tmp[i].flag == CONHASH_F_OCCUPIED )
        {
          coninfo_t a;
          a.id1 = tmp[i].id1;
          a.id2 = tmp[i].id2;
          a.nlast = tmp[i].nlast;
          vec3d_assign( a.ftau_e, tmp[i].ftau_e );
          vec3d_assign( a.ftau_i, tmp[i].ftau_i );
          vec3d_assign( a.pc, tmp[i].pc );
          vec3d_assign( a.f, tmp[i].f );
          conhash_ins( pch, &a );
        }
    }

  /* freeing old data */
  free( tmp );
}

int conhash_nocontacts_clean( conhash_t* pch, size_t ncurrent )
{
  int total = 0;
  for( lpindex_t j = 0; j < pch->maxn; ++j )
    {
      if ( pch->cons[j].flag == CONHASH_F_OCCUPIED &&
           pch->cons[j].nlast != ncurrent )
        {
          pch->cons[j].flag = CONHASH_F_VACANT;
          total ++;
        }
    }
  return total;
}



contact_t *conhash_conlist_make_alloc( conhash_t* pch, 
                                       const size_t ncurrent, 
                                       size_t* pncs )
{
  /* return value */
  contact_t* pconlist = NULL;

  /* counting elements */
  (*pncs) = 0;
  for( lpindex_t j = 0; j < pch->maxn; ++j )
    {
      if ( pch->cons[j].flag == CONHASH_F_OCCUPIED &&
           pch->cons[j].nlast == ncurrent ) (*pncs) ++;
    }

  /* special case: no contacts */
  if ( *pncs == 0 ) return NULL;

  /* allocation */
  pconlist = malloc( (*pncs) * sizeof( contact_t ) );
  assert( pconlist );          /* just break it out */
  
  /* filling with elements */
  size_t total = 0;
  for( lpindex_t j = 0; j < pch->maxn; ++j )
    {
      if ( pch->cons[j].flag == CONHASH_F_OCCUPIED &&
           pch->cons[j].nlast == ncurrent )
        {
          vec3d_assign( pconlist[total].ftau_e,
                        pch->cons[j].ftau_e );
          vec3d_assign( pconlist[total].ftau_i,
                        pch->cons[j].ftau_i );
          vec3d_assign( pconlist[total].pc,
                        pch->cons[j].pc );
          vec3d_assign( pconlist[total].f,
                        pch->cons[j].f );
          pconlist[total].delta = pch->cons[j].delta;
          pconlist[total].id1 = pch->cons[j].id1;
          pconlist[total].id2 = pch->cons[j].id2;
          total++;
        }
    }

  assert( total == *pncs );
  return pconlist;
}
