#ifndef HIGHLIO_H
#define HIGHLIO_H

#include <math.h>               /* for math */
#include <stdio.h>              /* for printf/fprintf */
#include <stdlib.h>             /* for exit */

#include <lua.h>                /* lua stuff */
#include <lauxlib.h>
#include <lualib.h>

#include "defs.h"               /* must for macros */
#include "box.h"                /* box is box */
#include "vec3d.h"              /* vec3d type */
#include "lptimer.h"            /* timer */
#include "body.h"               /* body_t */
#include "atom.h"               /* atom_t */
#include "spring.h"             /* spring_t */
#include "ptriangle.h"          /* ptriangle_t */
#include "conmech.h"            /* contact mechanics */
#include "conhash.h"            /* hash table for contacts */
#include "lpdyna.h"             /* dynamic array lpdyna_t */
#include "coldet.h"             /* collision_t */
#include "bc.h"                 /* bc_t */
#include "sph.h"
#include "portal.h"

#include "hdis-types.h"
#include "hdis.h"


void highlio_save( char* filename, /* output file name */
                   settings_t * psettings,
                   lptimer_t* pt,
                   sbb_t* restrict sbbs,
                   atom_t* restrict pAlst,
                   vert_t* restrict pVlst,
                   body_t* restrict pBlst,
                   spring_t* restrict pSlst,
                   ptriangle_t* restrict pTlst,
                   sph_t* restrict pHlst,
                   const lpindex_t nsbbs,
                   const lpindex_t nAs,
                   const lpindex_t nVs,
                   const lpindex_t nBs,
                   const lpindex_t nSs,
                   const lpindex_t nTs,
                   const lpindex_t nHs,
                   conhash_t* restrict pch,
                   conhash_comp_t* restrict pch_comp,
                   bc_t* pbc,
                   box_t* restrict pbox,
                   vec3d_t* restrict g,
                   drag_t* restrict pdrag,
                   char** pA_gnames,
                   const lpindex_t nA_gnames,
                   int* pA_intrx,
                   const lpindex_t nA_intrx,
                   material_t* pA_mats,
                   char** pA_matnames,
                   const lpindex_t nA_mats,
                   cmprops_t* pcmprops,
                   const lpindex_t ncmprops,
                   sbb_params_t* psbb_params,
                   sph_props_t* psphprops,
                   portal_t * pportal );

void highlio_read_dims( char* filename,
                        lpindex_t *pnBs,
                        lpindex_t *pnAs,
                        lpindex_t *pnVs,
                        lpindex_t *pnSs,
                        lpindex_t *pnTs,
                        lpindex_t *pnHs,
                        lpindex_t *pnCs );

void highlio_read_data( char* filename,
                        vert_t* pVlst,
                        atom_t* pAlst,
                        body_t* pBlst,
                        spring_t* pSlst,
                        ptriangle_t* pTlst,
                        sph_t* pHlst,
                        contact_t* pconlist,
                        box_t* pbox,
                        lptimer_t* pt,
                        vec3d_t* pg,
                        drag_t* pdrag,
                        bc_t* pbc,
                        sph_props_t* psphprops,
                        settings_t* psettings );

#endif
