/*!

  \file conmech.c

  \brief Implementation contact mechanics

*/

#include "conmech.h"

real conmech_CR_CR( real b )
{
  assert( b >= 0 );
  
  if ( b < CONMECH_CR_B0 )
    return exp( - CONMECH_CR_ALPHA * b );
  else if ( b < CONMECH_CR_B1 )
    /* we just save some operations on polynomial computation */
    return exp( - ( CONMECH_CR_BETA0 + 
                    b * ( CONMECH_CR_BETA1 
                          + CONMECH_CR_BETA2 * b ) ) );
  else return CONMECH_CR_R / ( CONMECH_CR_C + b*b*b*b*b );
}
real conmech_CR_b( real CR )
{
  /* we do not allow CR = 0 */
  if ( CR == 0 ) return 1000.0;
  
  /* we use that approximation is monotone */

  real lnCR = log( CR );
  real b;

  /* try first part */
  b = - lnCR / CONMECH_CR_ALPHA;
  if ( b < CONMECH_CR_B0 ) return fabs(b);

  /* OK, then try second  */
  real D = CONMECH_CR_BETA1 * CONMECH_CR_BETA1 - 
    4 * CONMECH_CR_BETA2 * ( CONMECH_CR_BETA0 + lnCR );
  if ( D > 0 )                  /* otherwise no solution */
    {
      b = 0.5 * ( sqrt( D ) - CONMECH_CR_BETA1 ) / CONMECH_CR_BETA2;
      if ( b < CONMECH_CR_B1 ) return b; /* we obviously higher than
                                            b0 */
    }
  
  /* All right, we are in the tail then */
  return pow( CONMECH_CR_R / CR - CONMECH_CR_C, 0.2 );
}

real conmech_CR_kni( real b, real v0, real r, real H, real m )
{
  return
    0.8746896591546225 * b * pow( m, 0.4 ) * 
    pow( H, 0.6 ) * pow( r, 0.3 ) / pow( v0, 0.2 );
}


/* ----------------------------------------------------------------- */

int conmech_forces( vec3d_t* pf,
                    vec3d_t* pftau_e,
                    const real d,
                    const vec3d_t* pv,
                    const vec3d_t* pn,
                    const cmprops_t* pcmprop,
                    const real R,
                    const real kni,
                    const real dt )
  
{
  vec3d_t vt;                     /* tangent relative vel. at contact */
  real vn;                        /* normal component of it */
  vec3d_proj( vt, vn, *pv, *pn );
  
  /* normal force: */

  const real kne = conmech_hertz_kne( pcmprop->H, R, d );
  const real fn = conmech_hertz_fne( kne, d ) + conmech_fni( kni, d, vn );

  /* tangential forces  */
  vec3d_t phi_tau;
  real phi_n;
  real kt = pcmprop->alpha_t * kne;
  conmech_fte( *pftau_e, phi_tau, phi_n, *pftau_e,
               fn, vt, *pn, dt, kt, pcmprop->mu );

#ifdef FTAU_I
  vec3d_t ftau_i;
  vec3d_setnull( ftau_i );
#endif
  /* we add all tangent values to the total force now */
  vec3d_scale( *pf, *pn, fn );
  vec3d_sum( *pf, *pf, *pftau_e );

#ifdef FTAU_I
  vec3d_sum( *pf, *pf, ftau_i );
#endif

  return CONMECH_CONTACT_YES;
}

