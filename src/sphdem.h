/*!

  \file sphdem.h

  \brief SPH-DEM interaction description

*/

#ifndef SPHDEM_H
#define SPHDEM_H

#include "lpdyna.h"
#include "sph.h"
#include "atom.h"
#include "bb.h"
#include "coldet.h"

/*! \brief Computing Thetas integrals for all DEM-SPH contacts */
void sphdem_Thetas( lpdyna_t* pcols,
                    sph_t* pHlst, lpindex_t nHs,
                    atom_t* pAlst, lpindex_t nAs,
                    vert_t* pVlst,
                    sbb_t* pSs );

/*! forces and torques of SPH-DEM interaction */
void sphdem_forces( lpdyna_t* pcols,
                     sph_t* pHlst, lpindex_t nHs,
                     body_t* pBlst, lpindex_t nBs,
                     atom_t* pAlst, lpindex_t nAs,
                     vert_t* pVlst,
                     sbb_t* pSs,
                     real dt );

/* mass streams from sph-dem interaction */
void sphdem_mstream( lpdyna_t* pcols,
                     sph_t* pHlst, lpindex_t nHs,
                     body_t* pBlst, lpindex_t nBs,
                     atom_t* pAlst, lpindex_t nAs,
                     vert_t* pVlst,
                     sbb_t* pSs );

/* velocity correction to enforce non penetration condition */
void sphdem_velcorrection( lpdyna_t* pcols,
                           sph_t* pHlst, lpindex_t nHs,
                           body_t* pBlst, lpindex_t nBs,
                           atom_t* pAlst, lpindex_t nAs,
                           vert_t* pVlst,
                           sbb_t* pSs, real dt );

#endif      /* SPHDEM_H */
