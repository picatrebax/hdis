/*!

  \file spring.h

  \brief spring structure

*/

#ifndef SPRING_H
#define SPRING_H

#include "defs.h"
#include "vec3d.h"

typedef struct spring_s {
  vec3d_t rb1;			/* connection points, body frames */
  vec3d_t rb2;
  vec3d_t r1;			/* connection points, global frame */
  vec3d_t r2;
  real x0;			/* neutral distance */
  real k;			/* stiffness */
  real eta;			/* damping */
  lpindex_t ib1;		/* body 1 index */
  lpindex_t ib2;		/* body 2 index */
  lpindex_t group;              /* spring group id */
} spring_t;

#endif  /* SPRING_H */

