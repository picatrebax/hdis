/*!

  \file bb.h

  \brief Bunding boxes

*/

#ifndef BB_H
#define BB_H

#include "defs.h"
#include "vec3d.h"
#include "atom.h"               /* for building sbb by atoms */
#include "body.h"              /* atoms link to points */
#include "box.h"                /* box_t */
#include "sph.h"

/* Default values */
#define SBB_DFLT_DRMAX_RMIN_SCALE 1  /*!< How many Rmins you can add  */
#define SBB_DFLT_DRMIN_RMIN_SCALE 0.001 /*!< Portion of Rmin you must add */
#define SBB_DFLT_TAU_DTS          100   /*!< Timesteps equal to tau  */

#define SBB_F_DEM (0x1)          /*!< DEM particle  */
#define SBB_F_SPH (0x2)          /*!< SPH particle */

/*! SBB main structure  */
typedef struct sbb_s
{
  vec3d_t c;              /* center coordinates (global frame) */
  vec3d_t c0;             /* old center coordinates (global frame) */
  real r;                 /* radius */
  lpindex_t body_indx;    /* index of the body */
  lpindex_t group_id;     /* atom group ID */

  real rmin;              /* limit radius */
  int flag;               /* sph or no */
} sbb_t;


/*! Parameters that define the bounding boxes */
typedef struct sbb_params_s
{
  real drmax; /*!< Maximum allowed increase to Rmin  */
  real drmin; /*!< Minimum allowed increase to Rmin  */
  real tau;   /*!< Semi-maximum time between two contact detections */
} sbb_params_t;

/* Just assign values to body_indx and group_id */
void sbb_build( sbb_t* pSlst,
                const atom_t* restrict pAlst,
                const lpindex_t nAs,
                const sph_t* restrict pHlst,
                const lpindex_t nHs );

/* build SBB by Atom */
void sbb_reset( sbb_t* pSlst, atom_t* pAlst,
                body_t* pBlst, const lpindex_t nAs,
                const sph_t* restrict pHlst, const lpindex_t nHs,
                const sbb_params_t* restrict psbb_params );

/* check collision */
#define sbb_collide( a, b )                                     \
  ( ( ( (a).body_indx != (b).body_indx ) )                      \
    && ( pintrx[ ngroups*(a).group_id + (b).group_id ] )        \
    && ( vec3d_sqdiff((a).c,(b).c) <                            \
         ((a).r + (b).r)*((a).r + (b).r) ) )

/* check boundary proximity */
#define sbb_xprox( a, X )                             \
  ( ( vec3d_getx((a).c) < (a).r )                     \
    || ( (X) - (a).r < vec3d_getx((a).c) ) )

#define sbb_yprox( a, y0, y1 )                  \
  ( ( vec3d_gety((a).c) - (a).r < y0 )          \
    || ( vec3d_gety((a).c) + (a).r > y1 ) )


#define sbb_zprox( a, Z )                             \
  ( ( vec3d_getz((a).c) < (a).r )                     \
    || ( Z - (a).r < vec3d_getz((a).c) ) )


#endif  /* BC_H */
