#ifdef _OPENMP
#include <omp.h>
#endif

#include "sph.h"

#include "coldet.h"
#include "bb.h"
#include "sphkernel.h"

static inline real max( real a, real b ) { return a > b ? a : b; }

/* Defining which kernel to use */
//#define sph_w  sphkernel_w4
//#define sph_dw sphkernel_dw4

#define sph_w  sphkernel_wendland_w
#define sph_dw sphkernel_wendland_dw


/* --------------------------------------------------------------------------- */

const real min_rho = 0.01;

void sph_reset( sph_t *pHlst, lpindex_t nHs )
{
# if defined( PARALLEL_OPENMP )
# pragma omp parallel for
# endif
  for ( int i = 0; i < nHs; i++ )
    {
      vec3d_setnull( pHlst[i].f );
      pHlst[i].j = 0;          /* clear the mass streams out */

      /* pHlst[i].rho = ( pHlst[i].flags & SPH_F_BOUNDARY ) ? */
      /*   pHlst[i].rho_0 : min_rho; */
    }

  return;
}

/* --------------------------------------------------------------------------- */

void sph_mstream( lpdyna_t* pcols, lpindex_t nAs, sph_t* pHlst, lpindex_t nHs,
                  sph_omp_t* pomp )
{
  /* pSs collect all changes here */
  
  collision_t* pcols_data = ( collision_t* ) pcols->data;
  const lpindex_t ncols = lpdyna_length( pcols );

# if defined( PARALLEL_OPENMP )
# pragma omp parallel
# endif
  {  /* omp parallel region: when applied */

# if defined( PARALLEL_OPENMP )
  const int nthread = omp_get_thread_num();  /* thread number */
# endif

# if defined( PARALLEL_OPENMP )
# pragma omp for
# endif
  for ( lpindex_t k = 0; k < ncols; k++ )
    {
      lpindex_t i = pcols_data[k].i,
                j = pcols_data[k].j;

      /* ---------------------------------------------------------------------- */

      if ( pcols_data[k].type == COLDET_COLLISION_TYPE_SPH_SPH )
        {
          sph_t *psphi = pHlst + i - nAs,
                *psphj = pHlst + j - nAs;

          if ( psphi->flags & SPH_F_CLONE
               && psphj->flags & SPH_F_CLONE )
            continue;

          if ( pcols_data[k].status == SPH_NO_INTERACTION )
            continue;

          if ( ( ( psphi->flags & SPH_F_CLONE ) &&
                 ( psphj->flags & SPH_F_BOUNDARY ) )
               ||
               ( ( psphi->flags & SPH_F_BOUNDARY ) &&
                 ( psphj->flags & SPH_F_CLONE ) ) )
            continue;

          vec3d_t ri = psphi->c,
                  rj = psphj->c;

          vec3d_t vi = psphi->v,
                  vj = psphj->v;

          vec3d_t rij;
          vec3d_diff( rij, rj, ri );

          vec3d_t vij;
          vec3d_diff( vij, vj, vi );

          real h = psphi->h;

          /* real */
          /*   rhoi = psphi->rho, */
          /*   rhoj = psphj->rho; */

          real q = vec3d_abs( rij ) / h;
          real dw = sph_dw( q );

          vec3d_t grad;         /* gradient W */

          /* compute gradient */
          vec3d_scale( grad, rij, dw / ( q * h*h*h*h*h ) );

          real A = vec3d_dot( vij, grad );
          
          /* mass streams */
#         if defined( PARALLEL_OPENMP )
          {
            pomp->ppj[nthread][i-nAs] += psphj->m * A;
            pomp->ppj[nthread][j-nAs] += psphi->m * A;
          }
#         else
          {
            psphi->j += psphj->m * A;
            psphj->j += psphi->m * A;
          }
#         endif
        }
    }
  }

  /* Collect mass streams from virtual particles? */
#ifdef VIRT  
  for ( lpindex_t i = 0; i < nHs; i++ )
    {
      sph_t *psph = pHlst + i;
      if ( psph->flags & SPH_F_HAS_CLONE )
        {
          sph_t *psph_virt = pHlst + psph->clone_id;
          /* Collect from clone */
          psph->j += psph_virt->j;
          /* Assign full value to clone */
          psph_virt->j = psph->j;
        }
    }
#endif
}

/* -------------------------------------------------------------------------- */

void sph_mstream_collect( sph_t * restrict pHlst, const lpindex_t nHs,
                          sph_omp_t *pomp )
{
# if defined( PARALLEL_OPENMP )
# pragma omp for nowait
  for( lpindex_t k = 0; k < nHs; ++k )
    for( int n = 0; n < pomp->nprocs; ++n )
      {
        pHlst[k].j += pomp->ppj[n][k];
        pomp->ppj[n][k] = 0;    /* don't forget cleaning */
      }
# endif
}

/* --------------------------------------------------------------------------- */

void sph_gravity( sph_t * restrict pHlst,
                  const lpindex_t nHs,
                  const vec3d_t* restrict g  )
{
# if defined( PARALLEL_OPENMP )
# pragma omp parallel for
# endif
  for ( lpindex_t i = 0; i < nHs; ++i )
    vec3d_expy( pHlst[i].f, *g, pHlst[i].m );
}

/* --------------------------------------------------------------------------- */

void sph_pressure( sph_t *pHlst, lpindex_t nHs, sph_props_t* props )
{
  for ( int i = 0; i < nHs; i++ )
  {
    sph_t *psph = pHlst + i;
    if (  psph->flags & SPH_F_BOUNDARY )
      {
        psph->p = 0;
        psph->a = 0;            /* sound velocity */
      }
    else
      {
        psph->p = props->c2 *
          ( props->rho0 / props->gamma ) *
          ( pow( psph->rho / props->rho0, props->gamma ) - 1 );

        //TODO:::: SERIOUSLY, where is c0 ???
        psph->a = sqrt( props->c0 * pow( psph->rho/props->rho0, props->gamma-1 ) );
      }
  }
}

/* --------------------------------------------------------------------------- */

void sph_forces( lpdyna_t* pcols,
                 lpindex_t nAs,
                 sph_t* pHlst,
                 lpindex_t nHs,
                 sph_props_t* props,
                 real dt,
                 sph_omp_t* pomp )
{
  collision_t* pcols_data = ( collision_t* ) pcols->data;

# if defined( PARALLEL_OPENMP )
# pragma omp parallel
# endif
  {  /* omp parallel region: when applied */

# if defined( PARALLEL_OPENMP )
  const int nthread = omp_get_thread_num();  /* thread number */
# endif

# if defined( PARALLEL_OPENMP )
# pragma omp for
# endif
  for ( lpindex_t k = 0; k < lpdyna_length( pcols ); k++ )
    {
      lpindex_t i = pcols_data[k].i,
                j = pcols_data[k].j;

      if ( pcols_data[k].type == COLDET_COLLISION_TYPE_SPH_SPH )
        {
          sph_t *psphi = pHlst + i - nAs,
                *psphj = pHlst + j - nAs;

          if ( psphi->flags & SPH_F_CLONE
               && psphj->flags & SPH_F_CLONE )
            continue;

          if ( pcols_data[k].status == SPH_NO_INTERACTION )
            continue;

          if ( ( ( psphi->flags & SPH_F_CLONE ) &&
                 ( psphj->flags & SPH_F_BOUNDARY ) )
               ||
               ( ( psphi->flags & SPH_F_BOUNDARY ) &&
                 ( psphj->flags & SPH_F_CLONE ) ) )
            continue;

          vec3d_t ri = psphi->c,
                  rj = psphj->c;

          vec3d_t vi = psphi->v,
                  vj = psphj->v;

          vec3d_t rij;
          vec3d_diff( rij, rj, ri );

          vec3d_t vij;
          vec3d_diff( vij, vj, vi );

          real h = psphi->h;

          real
            rhoi = psphi->rho,
            rhoj = psphj->rho;

          real q = vec3d_abs( rij ) / h;
          real dw = sph_dw( q );

          vec3d_t grad;         /* gradient W */
          real X;               /* Elastic force/m^2 value */
          real Y = 0;           /* Viscous force/m^2 value */

          /* compute gradient */
          vec3d_scale( grad, rij, dw / ( q * h*h*h*h*h ) );
          
          /* compute F values */
          X = psphi->p / ( rhoi * rhoi ) + psphj->p / ( rhoj * rhoj );

          /* compute viscousity */
          real rvdot = vec3d_dot( rij, vij );
          if ( rvdot < 0 )
            {
              real mu = h * rvdot / ( vec3d_sq( rij ) + 0.01*h*h );
              Y =  - mu * props->alpha * ( psphi->a + psphj->a ) / ( rhoi + rhoj );
            }
          real F = ( psphi->m * psphj->m ) * ( X + Y );

          /* assign vector force */
          /* mass streams */
#         if defined( PARALLEL_OPENMP )
          {
            if ( !( psphi->flags & SPH_F_CLONE
                    && psphj->flags & SPH_F_HAS_CLONE ) )
              vec3d_expy( pomp->ppF[nthread][i-nAs], grad, F );
            
            if ( !( psphj->flags & SPH_F_CLONE
                    && psphi->flags & SPH_F_HAS_CLONE ) )
              vec3d_expy( pomp->ppF[nthread][j-nAs], grad, -F );
          }
#         else
          {
            if ( !( psphi->flags & SPH_F_CLONE
                    && psphj->flags & SPH_F_HAS_CLONE ) )
              vec3d_expy( psphi->f, grad, F );
            
            if ( !( psphj->flags & SPH_F_CLONE
                    && psphi->flags & SPH_F_HAS_CLONE ) )
              vec3d_expy( psphj->f, grad, - F );
          }
#         endif
          
        }
    }
  }
}


/* -------------------------------------------------------------------------- */

void sph_forces_collect( sph_t * restrict pHlst, const lpindex_t nHs,
                         sph_omp_t *pomp )
{
# if defined( PARALLEL_OPENMP )
# pragma omp for nowait
  for( lpindex_t k = 0; k < nHs; ++k )
    for( int n = 0; n < pomp->nprocs; ++n )
      {
        vec3d_inc( pHlst[k].f, pomp->ppF[n][k] );
        vec3d_setnull( pomp->ppF[n][k] );
      }
# endif
  
  /* Collect forces for virtuals */
# if defined( PARALLEL_OPENMP )
# pragma omp for
# endif
  for ( lpindex_t i = 0; i < nHs; i++ )
    {
      sph_t *psph = pHlst + i;
      if ( psph->flags & SPH_F_HAS_CLONE )
        {
          sph_t *psph_virt = pHlst + psph->clone_id;
          /* Collect from clone */
          vec3d_inc( psph->f, psph_virt->f );
          /* Assign full value to clone */
          vec3d_assign( psph_virt->f, psph->f );
        }
    }

}


/* --------------------------------------------------------------------------- */

void sph_dynamics( sph_t* pHlst, const lpindex_t nHs, const real dt )
{
# ifdef PARALLEL_OPENMP
# pragma omp parallel for
# endif
  for( lpindex_t i = 0; i < nHs; ++i )
    {
      sph_t* psph = pHlst + i;
      if ( ! ( psph->flags & SPH_F_STILL ) )
        vec3d_expy( psph->v, psph->f, dt / psph->m );
    }
}

/* --------------------------------------------------------------------------- */
void sph_density( sph_t* pHlst, const lpindex_t nHs, const real dt )
{
# ifdef PARALLEL_OPENMP
# pragma omp parallel for
# endif
  for( lpindex_t i = 0; i < nHs; ++i )
    {
      sph_t* psph = pHlst + i;
      if ( ! ( psph->flags & SPH_F_STILL ) )
        {
          psph->rho   +=   psph->j * dt;
        }
      
    }
}

/* --------------------------------------------------------------------------- */

void sph_placement( sph_t* pHlst,
                    const lpindex_t nHs,
                    const real dt )
{
# ifdef PARALLEL_OPENMP
# pragma omp parallel for
# endif
  for( lpindex_t i = 0; i < nHs; ++i )
    {
      sph_t* psph = pHlst + i;
      if ( ! ( psph->flags & SPH_F_STILL ) )
        vec3d_expy( psph->c, psph->v, dt );
    }
}

/* --------------------------------------------------------------------------- */

void sph_teleport( sph_t *pHlst, lpindex_t nHs, box_t *pbox,
                   real l0, real l1, real L )
{
  for ( int i = 0; i < nHs; i++ )
    {
      if ( pHlst[i].flags & SPH_F_BOUNDARY )
        continue;

      if      ( pHlst[i].c.y < l0 )  pHlst[i].c.y += L;
      else if ( pHlst[i].c.y > l1 )  pHlst[i].c.y -= L;
    }
}


/* --------------------------------------------------------------------------- */

/* --------------------------------------------------------------------------- */

void sph_bc( box_t* pbox,
             sph_t* pHlst,
             lpindex_t nAs,
             lpindex_t* pxprox,
             lpindex_t* pyprox,
             lpindex_t* pzprox,
             lpindex_t nxprox,
             lpindex_t nyprox,
             lpindex_t nzprox )

{
  real damping = 0.5, stiffness = 100;
  int vel = 0;
  for ( int i = 0; i < nxprox; i++ )
    {
      if ( pxprox[i] >= nAs )
        {
          sph_t* psph = pHlst + pxprox[i] - nAs;
          if (vel) {
            if ( ( psph->c.x < psph->R && psph->v.x < 0.0 ) ||
                 ( pbox->fcorner.x - psph->c.x < psph->R && psph->v.x > 0.0 ) )
            psph->v.x = -damping * psph->v.x;
          }
          else {
          if ( psph->c.x < psph->R )
            psph->f.x += stiffness * ( psph->R - psph->c.x );
          else if ( pbox->fcorner.x - psph->c.x < psph->R )
            psph->f.x -= stiffness * ( psph->R - ( pbox->fcorner.x - psph->c.x ) );
          }
        }
    }
#if 0
  for ( int i = 0; i < nyprox; i++ )
    {
      if ( pyprox[i] >= nAs )
        {
          sph_t* psph = pHlst + pyprox[i] - nAs;
          if (vel) {
            if ( psph->c.y < psph->R && psph->v.y < 0.0 ||
                 pbox->fcorner.y - psph->c.y < psph->R && psph->v.y > 0.0 )
              psph->v.y = -damping * psph->v.y;
          }
          else {
          if ( psph->c.y < psph->R )
            psph->f.y += stiffness * ( psph->R - psph->c.y );
          else if ( pbox->fcorner.y - psph->c.y < psph->R )
            psph->f.y -= stiffness * ( psph->R - ( pbox->fcorner.y - psph->c.y ) );
          }
        }
    }
#endif
  for ( int i = 0; i < nzprox; i++ )
    {
      if ( pzprox[i] >= nAs )
        {
          sph_t* psph = pHlst + pzprox[i] - nAs;
          if (vel) {
            if ( ( ( psph->c.z < psph->R ) && ( psph->v.z < 0.0 ) ) ||
                 ( pbox->fcorner.z - psph->c.z < psph->R && psph->v.z > 0.0 ) )
            psph->v.z = -damping * psph->v.z;
          }
          else {
          if ( psph->c.z < psph->R )
            psph->f.z += stiffness * ( psph->R - psph->c.z );
          else if ( pbox->fcorner.z - psph->c.z < psph->R )
            psph->f.z -= stiffness * ( psph->R - ( pbox->fcorner.z - psph->c.z ) );
          }
      }
    }
}

/* --------------------------------------------------------------------------- */

void sph_density_sum( lpdyna_t* pcols, lpindex_t nAs, sph_t* pHlst, lpindex_t nHs,
                      sph_props_t* pprops)
{
  /* cancel density */
  for ( int i = 0; i < nHs; i++ )
    {
      pHlst[i].rho = ( pHlst[i].flags & SPH_F_BOUNDARY ) ?
        pprops->rho0 : min_rho;
    }

  /* We collect all changes  */
  
  collision_t* pcols_data = ( collision_t* ) pcols->data;
  for ( lpindex_t k = 0; k < lpdyna_length( pcols ); k++ )
    {
      lpindex_t i = pcols_data[k].i,
                j = pcols_data[k].j;
      if ( pcols_data[k].type == COLDET_COLLISION_TYPE_SPH_SPH )
        {
          sph_t *psphi = pHlst + i - nAs,
                *psphj = pHlst + j - nAs;

          if ( psphi->flags & SPH_F_CLONE
               && psphj->flags & SPH_F_CLONE )
            continue;

          vec3d_t ri = psphi->c,
                  rj = psphj->c;
          real h = psphi->h;

          real q = vec3d_absdiff( ri, rj ) / h;
          real W = sph_w ( q ) / ( h * h * h );

          if ( q < 2.0 ) pcols_data[k].status = 0;
          else pcols_data[k].status = SPH_NO_INTERACTION;

          if ( !( psphi->flags & SPH_F_BOUNDARY )
               && !( psphi->flags & SPH_F_CLONE
                     && psphj->flags & SPH_F_HAS_CLONE ) )
            psphi->rho += psphj->m * W;

          if ( ! ( psphj->flags & SPH_F_BOUNDARY )
               && !( psphj->flags & SPH_F_CLONE
                     && psphi->flags & SPH_F_HAS_CLONE ) )
            psphj->rho += psphi->m * W;
        }
    }

  /* Collect density */
  for ( lpindex_t i = 0; i < nHs; i++ )
    {
      sph_t *psph = pHlst + i;
      if ( psph->flags & SPH_F_HAS_CLONE )
        {
          sph_t *psph_virt = pHlst + psph->clone_id;
          /* Collect from clone */
          psph->rho += psph_virt->rho;
          /* Assign full value to clone */
          psph_virt->rho = psph->rho;
        }
    }

}
