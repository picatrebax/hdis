
#include "ini.h"

void ini( settings_t * psettings,
          lptimer_t* pt,
          vert_t**    ppVlst,
          atom_t**    ppAlst,
          body_t**    ppBlst,
          spring_t**  ppSlst,
          ptriangle_t** ppTlst,
          sph_t** ppHlst,
          char ***ppA_gnames,
          int **ppA_intrx,
          material_t **ppA_mats,
          char ***ppA_matnames,
          cmprops_t **ppcmprops,
          sbb_params_t* psbb_params,
          lpindex_t* pnAs,
          lpindex_t* pnVs,
          lpindex_t* pnBs,
          lpindex_t* pnSs,
          lpindex_t* pnTs,
          lpindex_t* pnHs,
          lpindex_t* pnA_gnames,
          lpindex_t* pnA_intrx,
          lpindex_t* pnA_mats,
          lpindex_t* pNcmprops,
          conhash_t*  pch,
          conhash_comp_t*  pch_comp,
          box_t*  pbox,
          vec3d_t*  pg,
          drag_t* pdrag,
          bc_t* pbc,
          sph_props_t* psphprops,
          portal_t* pportal,
          const char* luafile,
          int argc,
          char* argv[],
          lua_State* L )
{

  /* number of bodies, atoms, points, and convexes listed in .lua file*/
  lpindex_t nBs_lua = 0;
  lpindex_t nAs_lua = 0;
  lpindex_t nVs_lua = 0;
  lpindex_t nSs_lua = 0;
  lpindex_t nTs_lua = 0;
  lpindex_t nHs_lua = 0;

  /* number of bodies from HDF input file */
  lpindex_t nBs_hdf = 0;
  lpindex_t nAs_hdf = 0;
  lpindex_t nVs_hdf = 0;
  lpindex_t nSs_hdf = 0;
  lpindex_t nTs_hdf = 0;
  lpindex_t nHs_hdf = 0;

  /* number of contacts in contact list */
  lpindex_t nCs_hdf = 0;

  /* contact list (local) */
  contact_t* pconlist;

  /* Shall we even have default time settings? */
  pt->t0 = 0;
  pt->te = 1;
  pt->ts = 0.01;
  pt->dt = 0.0001;
  pt->n  = 0;

  /* default gravity */
  vec3d_set( *pg, 0, 0, -9.81 );

  psettings -> run_from_name = NULL;  /* by default no run_from option */

  /* by default we save the initial state */
  psettings->flags = psettings->flags | SETTINGS_F_SAVE_START;

  /* we also use angular momentum to calculate angular velocity by
     default (no action here requried ) */

  /* contact detection default values */
  psettings->cdmethod = SETTINGS_CDM_SPSP;
  psettings->cdm_dx_min = -1.0;

  /* boundary condition main values */
  pbc->flags =  BC_F_BCTYPE_HERTZIAN;
  pbc->zout_vel =  0.0;

  pdrag->isdrag = 0;
  pdrag->isuniform = 0;
  pdrag->Cd = -1.0;
  pdrag->mu = -1.0;

  /* Since 5.2 need explicitly load libraries */
  lua_gc(L, LUA_GCSTOP, 0);  /* stop collector during initialization  */
  luaL_openlibs(L);  /* open libraries */

  //luaL_requiref( L, "hdf5", luaopen_luahdf5, 1);
  //luaL_requiref( L, "lfs", luaopen_lfs, 1);
  //lua_pop(L, 1);  /* remove lib */

  lua_gc(L, LUA_GCRESTART, 0);

  /* getting argument line into Lua */
  int nluaargs = 0;
  if ( argc > 2 )
    {
      nluaargs = argc - 2;
      luaL_dostring( L, "arg={}" );
      for ( int i = 2; i < argc && i < 11; ++i )
        {
          int len = strlen( argv[i] );
          char* ps = calloc( (10 + len), sizeof(char)  );
          char psn[2];

          strncat( ps, "arg[", 4 );
          sprintf( psn, "%d", i-1 );
          strncat( ps, psn, 1 );
          strncat( ps, "]='", 3 );
          strncat( ps, argv[i], len );
          strncat( ps, "'", 2 );
          luaL_dostring( L, ps );
          free ( ps );

          nluaargs++;
        }
    }

  /* loading the file */
  if ( luaL_loadfile( L, luafile ) || lua_pcall( L, 0, 0, 0 ) )
    fatal( 1, "Cannot read file \"%s\"\n%s\n",
           luafile, lua_tostring( L, -1 ) );

  lua_settop( L, 0 );             /* clear the stack */

  lua_getglobal( L, "hdis" );
  if ( ! lua_istable( L, -1 ) )
    fatal( 1, "Table \"hdis\" is not defined in configuration\n" );

  nBs_lua = lua_hdis_get_length( L, "hdis.bodies" );
  nAs_lua = lua_hdis_get_length( L, "hdis.atoms" );
  nVs_lua = lua_hdis_get_length( L, "hdis.verts" );
  nSs_lua = lua_hdis_get_length( L, "hdis.springs" );
  nTs_lua = lua_hdis_get_length( L, "hdis.ptriangles" );

  *pnA_gnames = lua_hdis_get_length( L, "hdis.groups.list" );
  nHs_lua = lua_hdis_get_length( L, "hdis.dots" );
  *pnA_mats = lua_hdis_get_length( L, "hdis.material.list" );

  /* ----------------------------------------------------------------- */

  lua_getfield( L, -1, "run_from" );
  if ( lua_isstring( L, -1 ) )
    {
      psettings->run_from_name = strdup( lua_tostring( L, -1 ) );
      assert( psettings->run_from_name );
    }
  lua_pop( L, 1 );

  if ( psettings->run_from_name )
    {
      highlio_read_dims( psettings->run_from_name,
                         &nBs_hdf, &nAs_hdf, &nVs_hdf,
                         &nSs_hdf, &nTs_hdf, &nHs_hdf, &nCs_hdf );
    }

  /* numbers */
  *pnBs = nBs_lua + nBs_hdf;
  *pnAs = nAs_lua + nAs_hdf;
  *pnVs = nVs_lua + nVs_hdf;
  *pnSs = nSs_lua + nSs_hdf;
  *pnTs = nTs_lua + nTs_hdf;
  *pnHs = nHs_lua + nHs_hdf;

  /* global elements allocation */
  if ( *pnBs != 0 )
    *ppBlst = malloc( sizeof( body_t ) * (*pnBs) );

  if ( *pnAs != 0 )
    *ppAlst = malloc( sizeof( atom_t ) * (*pnAs) );

  if ( *pnHs != 0 )
    *ppHlst = (sph_t *) calloc( *pnHs, sizeof( sph_t ) );

  if ( *pnVs != 0 )
    *ppVlst = malloc( sizeof( vert_t ) * (*pnVs) );

  if ( *pnSs != 0 )
    *ppSlst = malloc( sizeof( spring_t ) * (*pnSs) );

  if ( *pnTs != 0 )
    *ppTlst = malloc( sizeof( ptriangle_t ) * (*pnTs) );

  if ( *pnA_mats != 0 )
    {
      *ppA_mats = malloc( sizeof( material_t ) * (*pnA_mats) );
      *ppA_matnames = calloc( *pnA_mats, sizeof( char* ) );
    }

  /* local contact list allocation if exists */
  if ( nCs_hdf > 0 )
    {
      pconlist = malloc( sizeof( contact_t ) * nCs_hdf );
      assert( pconlist );
    }
  else
    pconlist = NULL;

  if ( *pnA_gnames != 0 )
    *ppA_gnames = calloc( *pnA_gnames, sizeof( char* ) );

  if ( psettings->run_from_name )
    {
      highlio_read_data( psettings->run_from_name,
                         *ppVlst,
                         *ppAlst, *ppBlst, *ppSlst, *ppTlst,
                         *ppHlst,
                         pconlist, pbox,
                         pt, pg, pdrag, pbc, psphprops, psettings );
    }

  /* ------------------------------------------------------------ */
  /*               Reading global variables                       */
  /* ------------------------------------------------------------ */


  /* ------------------------------------------------------------ */

  lua_getfield( L, -1, "box" );

  if ( lua_istable( L, -1 ) )
    {
      double x, y, z;
      lua_hdis_get_real_field( L, "x", &x );
      lua_hdis_get_real_field( L, "y", &y );
      lua_hdis_get_real_field( L, "z", &z );

      vec3d_set( pbox->fcorner, (real)x, (real)y, (real)z );
    }
  lua_pop( L, 1 );

  /* standard boundary ids */
  pbox->xid = LPINDEX_MAX - 0;
  pbox->yid = LPINDEX_MAX - 1;
  pbox->zid = LPINDEX_MAX - 2;

  /* ------------------------------------------------------------ */

  lua_getfield( L, -1, "g" );

  if ( lua_istable( L, -1 ) )   /* vector? */
    {
      /* get 3D vector values */
      real x[3];
      for ( int m = 1; m <= 3; ++m )
        {
          lua_rawgeti( L, -1, m );
          x[m-1] = lua_tonumber( L, -1 );
          lua_pop( L, 1 );
        }
      vec3d_set( *pg, x[0], x[1], x[2] );
    }
  else if ( lua_isnumber( L, -1 ) ) /* value? */
    {
      vec3d_set( *pg, 0, 0, lua_tonumber( L, -1 ) );
    }

  lua_pop( L, 1 );

  /* ------------------------------------------------------------ */

  lua_getfield( L, -1, "portal" );

  if ( lua_istable( L, -1 ) ) /* set portal for dem teleport? */
    {
      pportal->flag |= PORTAL_F_OPEN;

      lua_hdis_get_real_field( L, "entrance_y", &pportal->entrance_y );
      lua_hdis_get_real_field( L, "exit_y", &pportal->exit_y );
      lua_hdis_get_real_field( L, "exit_z", &pportal->exit_z );

      if( lua_hdis_get_vec3d( L, "v", &pportal->v ) )
          pportal->flag |= PORTAL_F_SET_V;
      if( lua_hdis_get_vec3d( L, "w", &pportal->w ) )
        pportal->flag |= PORTAL_F_SET_W;
    }

  lua_pop( L, 1 );

  /* ------------------------------------------------------------ */

  lua_getfield( L, -1, "drag" );
  if ( lua_istable( L, -1 ) ) /* set drag or not? */
    {
      pdrag -> isdrag = 1;

      lua_getfield( L, -1, "uniform" );
      if ( lua_isboolean( L, -1 ) && lua_toboolean( L, -1 ) )
        {
          pdrag -> isuniform = 1;
        }
      lua_pop( L, 1 );

      lua_hdis_get_real_field( L, "Cd", &pdrag->Cd );
      lua_hdis_get_real_field( L, "mu", &pdrag->mu );
    }

  lua_pop( L, 1 );

  /* ------------------------------------------------------------ */

  lua_getfield( L, -1, "boundary" );

  if ( lua_istable( L, -1 ) )
    {
      real x;
      char* bc_type;
      lua_getfield( L, -1, "coltype" );
      if ( lua_isstring( L, -1 ) )
        {
          bc_type = (char*)lua_tostring( L, -1 );

          if ( strcmp( bc_type, "reflection" ) == 0 )
            pbc->flags = pbc->flags | BC_F_BCTYPE_REFLECTION;
          else
            pbc->flags =  pbc->flags | BC_F_BCTYPE_HERTZIAN;
        }
      else
        pbc->flags =  pbc->flags | BC_F_BCTYPE_HERTZIAN;

      lua_pop( L, 1 );             /* string out */

      /* Set periodic boundaries */
      lua_getfield( L, -1, "periodic" );
      if ( lua_isboolean( L, -1 ) && (lua_toboolean( L, -1 )) )
        {
          pbc->flags |= BC_F_PERIODIC;
        }
      else
        {
          pbc->flags &= ~BC_F_PERIODIC;
        }
      lua_pop( L, 1 );

      /* boundary top/bottom style */

      lua_getfield( L, -1, "opentop" );
      if ( lua_isboolean( L, -1 ) && (lua_toboolean( L, -1 )) )
        {
          pbc->flags = pbc->flags | BC_F_OPENTOP;
        }
      lua_pop( L, 1 );          /* flag out */

      lua_getfield( L, -1, "openbottom" );
      if ( lua_isboolean( L, -1 ) && (lua_toboolean( L, -1 )) )
        pbc->flags = pbc->flags | BC_F_OPENBOTTOM;
      lua_pop( L, 1 );          /* flag out */

      /* getting zout max velocity */
      if ( lua_hdis_get_real_field( L, "zout_vel", &x ) )
        {
          pbc->flags = pbc->flags | BC_F_ZOUTVEL;
          pbc->zout_vel = x;
        }
      else
        {
          pbc->zout_vel = 0;
        }

      lua_getfield( L, -1, "imaterial" );
      pbc->material_id = lua_tonumber( L, -1 ) - 1;
      lua_pop( L, 1 );

      lua_getfield( L, -1, "flags" );
      if ( lua_isnumber( L, -1 ) )
        pbc->flags |= (unsigned int ) lua_tointeger( L, -1 );
      lua_pop( L, 1 );
    }

  lua_pop( L, 1 );             /* table out */

  /* ------------------------------------------------------------ */

  lua_getfield( L, -1, "nummethods" );

  if ( lua_istable( L, -1 ) )
    {

      /* getting the way to compute omega */
      lua_getfield( L, -1, "omega" );

      if ( lua_isstring( L, -1 ) )
        {
          char* numtype;
          numtype = (char*)lua_tostring( L, -1 );

          if ( strcmp( numtype, "Walton" ) == 0 )
            {
              psettings->flags = psettings->flags |
                SETTINGS_F_OMEGA_WALTON;
            }
          else
            {
              psettings->flags =  psettings->flags &
                (~SETTINGS_F_OMEGA_WALTON);
            }
        }
      lua_pop( L, 1 );             /* string out */

      /* getting the way to compute quaternions */
      lua_getfield( L, -1, "quaternions" );

      if ( lua_isstring( L, -1 ) )
        {
          char* numtype;
          numtype = (char*)lua_tostring( L, -1 );

          if ( strcmp( numtype, "Trig" ) == 0 )
            {
              psettings->flags = psettings->flags |
                SETTINGS_F_QUAT_TRIG;
            }
          else
            {
              psettings->flags =  psettings->flags &
                (~SETTINGS_F_QUAT_TRIG);
            }
        }
      lua_pop( L, 1 );             /* string out */
    }

  lua_pop( L, 1 );             /* table out */

  /* ------------------------------------------------------------ */

  lua_getfield( L, -1, "time" );

  if ( lua_istable( L, -1 ) )
    {
      double t0, te, dt, dts;

      /* previous value must be preserved */
      t0 = pt->t; te = pt->te; dt = pt->dt; dts = pt->dts;

      lua_hdis_get_real_field( L, "start", &t0 );
      lua_hdis_get_real_field( L, "stop", &te );
      lua_hdis_get_real_field( L, "dt", &dt );
      lua_hdis_get_real_field( L, "save", &dts );
      lptimer_init( pt, t0, te, dt, t0+dts, dts );
    }

  lua_pop( L, 1 );

  /* ------------------------------------------------------------ */

  /* set parameters */
  lua_getfield( L, -1, "contact_hash" );

  /* default capacity is known only here */
  pch_comp->capacity = CONHASH_CAPACITY_MULT * ( (*pnBs) + (*pnHs) );

  if ( lua_istable( L, -1 ) )
    {
      lua_hdis_get_int_field( L, "capacity", &(pch_comp->capacity) );
      lua_hdis_get_int_field( L, "nprobes",
                               &(pch_comp->nprobes) );
      lua_hdis_get_int_field( L, "ULMT",
                               &(pch_comp->ULMT) );
      lua_hdis_get_bool_field( L, "save", &(pch_comp->bsave) );
    }

  lua_pop( L, 1 );              /* contact_hash */

  /* allocation */
  conhash_init( pch, pch_comp->capacity );

  /* initialization with contact list (if necessary) */
  if ( pconlist )
    {
      coninfo_t ci;
      for ( lpindex_t i = 0; i < nCs_hdf; ++i )
        {
          vec3d_assign( ci.ftau_e, pconlist[i].ftau_e );
          vec3d_assign( ci.ftau_i, pconlist[i].ftau_i );
          vec3d_assign( ci.f,  pconlist[i].f );
          vec3d_assign( ci.pc, pconlist[i].pc );
          ci.delta = pconlist[i].delta;
          ci.id1 = pconlist[i].id1;
          ci.id2 = pconlist[i].id2;
          ci.nlast = pt->n - 1;
          int errcode = conhash_ins( pch, &ci );
          assert( errcode != CONHASH_ERROR_INSERT );
        }
    }

  /* ------------------------------------------------------------ */

  lua_getfield( L, -1, "save_t0" );

  if ( lua_isboolean( L, -1 ) ) /* if it is not present => default or
                                   HDF value */
    {
      if ( !lua_toboolean( L, -1 ) )    /* drop the flag */
        psettings->flags = psettings->flags & (~SETTINGS_F_SAVE_START);
      else                      /* set the flag */
        psettings->flags = psettings->flags | SETTINGS_F_SAVE_START;
    }
  lua_pop( L, 1 );              /* clean the stack */

  /* ------------------------------------------------------------ */

  lua_getfield( L, -1, "bodies" );

  if ( lua_istable( L, -1 ) && lua_objlen( L, -1) > 0 )
    for ( lpindex_t i = nBs_hdf; i < (*pnBs); ++i )
      {
        /* index in Lua script */
        lpindex_t lua_ind = (i + 1) - nBs_hdf;
        lua_rawgeti( L, -1, lua_ind ); /* get the element */

        /* reading all necessary values */
        LUA_GET_VQ_BY_NAME( vec3d, (*ppBlst)[i], c );
        LUA_GET_VQ_BY_NAME( vec3d, (*ppBlst)[i], v );
        LUA_GET_VQ_BY_NAME( vec3d, (*ppBlst)[i], w );
        LUA_GET_VQ_BY_NAME( vec3d, (*ppBlst)[i], I );
        LUA_GET_VQ_BY_NAME( rquat, (*ppBlst)[i], q );
        lua_hdis_get_real_field( L, "m", &((*ppBlst)[i].m) );
        lua_hdis_get_real_field( L, "V", &((*ppBlst)[i].V) );

        /* Special deal for dw */
        {
          int success;
          success = lua_hdis_get_vec3d( L, "dwb",
                                        &((*ppBlst)[i].dwb ) );
          if ( !success ) vec3d_setnull( (*ppBlst)[i].dwb );
        }

        /* Angular momentum */
        rot3d_t Rot;
        rot3d_restore( &Rot, &(*ppBlst)[i].q );
        rot3d_ang_mom( &Rot, &(*ppBlst)[i].l,
                       &(*ppBlst)[i].I, &(*ppBlst)[i].w );

        /* defaulting the flag */
        (*ppBlst)[i].flag = BODY_F_FREE;

        /* Control Functions if available */
        int ifunc = BODY_CONTROL_FREE;
        lua_hdis_get_int_field( L, "control", &ifunc );
        (*ppBlst)[i].ifunc = ifunc;

        if ( ifunc != BODY_CONTROL_FREE )
          {
            /* drop free body flag */
            (*ppBlst)[i].flag &= ~BODY_F_FREE;

            /* set other state flag */
            switch( ifunc )
              {
              case BODY_CONTROL_STEADY:
                (*ppBlst)[i].flag |= BODY_F_STEADY;
                break;
              case BODY_CONTROL_STILL:
                (*ppBlst)[i].flag |= BODY_F_STILL;
                break;
              case BODY_CONTROL_SENSITIVE:
                (*ppBlst)[i].flag |=  BODY_F_SENSITIVE;
                break;
              }
          }

        /* Monitor Function if available (using the same ifunc) */
        ifunc = BODY_MONITOR_NO;
        lua_hdis_get_int_field( L, "monitor", &ifunc );
        (*ppBlst)[i].imonitor = ifunc;

        if ( ifunc != BODY_MONITOR_NO )
          (*ppBlst)[i].flag = (*ppBlst)[i].flag | BODY_F_MONITORED;

        /* External force function if available (using the same ifunc) */
        ifunc = BODY_EXTFORCE_NO;
        lua_hdis_get_int_field( L, "extforce", &ifunc );
        (*ppBlst)[i].effunc = ifunc;

        if ( ifunc != BODY_EXTFORCE_NO )
          (*ppBlst)[i].flag = (*ppBlst)[i].flag | BODY_F_EXTFORCED;

        /* Position correction function assigned to the body */
        ifunc = BODY_POSITIONING_NO;
        lua_hdis_get_int_field( L, "positioning", &ifunc );
        (*ppBlst)[i].posfunc = ifunc;

        /* SPH-Neutral, when forces/torques are not transmitted from SPH */
        bool is_sphneutral = false;
        lua_hdis_get_bool_field( L, "sphneutral", &is_sphneutral );
        if ( is_sphneutral )
          f_set( (*ppBlst)[i].flag, BODY_F_SPHNEUTRAL );

        if ( ifunc != BODY_POSITIONING_NO )
          (*ppBlst)[i].flag = (*ppBlst)[i].flag | BODY_F_POSITIONED;

        /* Reading boundary transparency options */
        bool is_bound = false;

        lua_hdis_get_bool_field( L, "noxbound", &is_bound );
        if ( is_bound )
          (*ppBlst)[i].flag = (*ppBlst)[i].flag | BODY_F_NOXBOUND;

        is_bound = false;
        lua_hdis_get_bool_field( L, "noybound", &is_bound );
        if ( is_bound )
          (*ppBlst)[i].flag = (*ppBlst)[i].flag | BODY_F_NOYBOUND;

        bool is_teleport = false;
        lua_hdis_get_bool_field( L, "teleport", &is_teleport );
        if ( is_teleport )
          (*ppBlst)[i].flag = (*ppBlst)[i].flag | BODY_F_TELEPORT;
        lua_pop( L, 1 );        /* for geti, do not remove from the end */
      }

  lua_pop( L, 1 );  /* for bodies */

  /* cleaning after */
  lua_pushnil( L );
  lua_setfield( L, -2, "bodies" ); /* hdis.bodies = nil */

  /* ------------------------------------------------------------ */

  lua_getfield( L, -1, "material" );      /* 1 */
  lua_getfield( L, -1, "list" );      /* 2 */

  for ( lpindex_t i = 0; i < *pnA_mats; ++i )
    {
      lua_rawgeti( L, -1, i+1 );      /* 1 */

      lua_getfield( L, -1, "name" );  /* 2 */
      (*ppA_matnames)[i] = strdup( lua_tolstring( L, -1, NULL ) );
      lua_pop( L, 1 );          /* end getfield */

      lua_hdis_get_real_field( L, "rho", &((*ppA_mats)[i].rho) );

      lua_hdis_get_real_field( L, "G", &((*ppA_mats)[i].G) );
      (*ppA_mats)[i].G *= 1.0e9;  /* convert to Pa */

      lua_hdis_get_real_field( L, "nu", &((*ppA_mats)[i].nu) );
      lua_hdis_get_real_field( L, "mu", &((*ppA_mats)[i].mu) );
      lua_hdis_get_real_field( L, "CR", &((*ppA_mats)[i].CR0) );
      lua_hdis_get_real_field( L, "vR", &((*ppA_mats)[i].vR0) );
      lua_hdis_get_real_field( L, "alpha_t", &((*ppA_mats)[i].alpha_t) );
      lua_hdis_get_real_field( L, "gamma", &((*ppA_mats)[i].gamma) );

      lua_pop( L, 1 );          /* end rawgeti */
    }

  lua_pop( L, 2 );  /* 2 back */

  /* ------------------------------------------------------------ */

  lua_getfield( L, -1, "material" );      /* 1 */
  lua_getfield( L, -1, "cmprops" );       /* 2 */

  *pNcmprops = (*pnA_mats) * (*pnA_mats + 1) / 2;

  /* allocation */
  if ( *pNcmprops != 0 )
    *ppcmprops = malloc( (*pNcmprops) * sizeof( cmprops_t ) );

  /* first just reading what we have there */
  for ( lpindex_t k = 0; k < *pNcmprops; ++k )
    {
      lua_rawgeti( L, -1, k+1 ); /* 3 */

      lua_hdis_get_real_field( L, "H", &((*ppcmprops)[k].H) );
      (*ppcmprops)[k].H *= 1.0e9;

      lua_hdis_get_real_field( L, "gamma", &((*ppcmprops)[k].gamma) );
      lua_hdis_get_real_field( L, "ka", &((*ppcmprops)[k].ka) );
      lua_hdis_get_real_field( L, "mu", &((*ppcmprops)[k].mu) );
      lua_hdis_get_real_field( L, "alpha_t",
                                  &((*ppcmprops)[k].alpha_t) );
      lua_hdis_get_real_field( L, "CR", &((*ppcmprops)[k].CR) );
      lua_hdis_get_real_field( L, "vR", &((*ppcmprops)[k].vR) );

      /* kni either read (so, restored from HDF5) or calculated */
      int status =
        lua_hdis_get_real_field( L, "kni", &((*ppcmprops)[k].Skni) );
      if ( !status )
        {
          real b = conmech_CR_b( (*ppcmprops)[k].CR );
          (*ppcmprops)[k].Skni =
             conmech_CR_kni( b, (*ppcmprops)[k].vR, 1,
                                (*ppcmprops)[k].H, 1 );
          /* we left r=1 and m=1 to adjust it later by real data */
        }

      lua_getfield( L, -1, "indx" ); /* 4 */

      lua_rawgeti( L, -1, 1 );    /* 5 */
      (*ppcmprops)[k].mati1 = lua_tointeger( L, -1 ) - 1;
      lua_pop( L, 1 );            /* 4 */

      lua_rawgeti( L, -1, 2 );    /* 5 */
      (*ppcmprops)[k].mati2 = lua_tointeger( L, -1 ) - 1;

      lua_pop( L, 3 );            /* 2, at list level */
    }

  lua_pop( L, 2 );                /* 0 */

  /* ------------------------------------------------------------ */

  lua_getfield( L, -1, "groups" ); /* 1 */

  /* list reading */
  lua_getfield( L, -1, "list" );  /* 2 */

  for( lpindex_t i = 0; i < *pnA_gnames; ++i )
    {
      lua_rawgeti( L, -1, i+1 );
      if ( lua_isstring( L, -1 ) )
        {
          (*ppA_gnames)[i] = strdup( lua_tolstring( L, -1, NULL ) );
        }
      else
        fatal( 32, "Cannot read an atom group name" );
      lua_pop( L, 1 );
    }

  lua_pop( L, 1 );     /* 1 : back to groups */

  /* intrx table reading */
  lua_getfield( L, -1, "intrx" );  /* 2 */

  /* we allow to not have a table */
  *pnA_intrx = (*pnA_gnames) * ( 1 + (*pnA_gnames) ) / 2;
  (*ppA_intrx) = calloc( *pnA_intrx, sizeof( int ) );

  if ( lua_istable( L, -1 ) )
  {
    for ( lpindex_t i = 0; i < *pnA_intrx; ++i )
      {
        lua_rawgeti( L, -1, i+1 );
        if ( !lua_isnumber( L, -1 ) )
          fatal( 33, "Wrong set for groups.intrx\n" );
        (*ppA_intrx)[i] = lua_tointeger( L, -1 );
        lua_pop( L, 1 );
      }
   }
   else  /* if not intrx => all 1 as before 0.50.3 */
  {
    for ( lpindex_t i = 0; i < *pnA_intrx; ++i )
      {
        (*ppA_intrx)[i] = 1;
      }
   }

  lua_pop( L, 2 );

  /* ------------------------------------------------------------ */

  lua_getfield( L, -1, "atoms" );

  if ( lua_istable( L, -1 ) && lua_objlen( L, -1) > 0 )
    for ( lpindex_t i = nAs_hdf; i < (*pnAs); ++i )
      {
        /* index in Lua script */
        lpindex_t lua_ind = (i + 1) - nAs_hdf;
        lua_rawgeti( L, -1, lua_ind ); /* get the element */

        /* setting the group ID */
        lpindex_t group_id = 0;
        lua_hdis_get_int_field( L, "group", &group_id );
        (*ppAlst)[i].group_id = group_id - 1;
                                  /* -1 to adopt to C indexing */

        /* setting the material ID */
        lpindex_t material_id = 0;
        lua_hdis_get_int_field( L, "material", &material_id );
        (*ppAlst)[i].material_id = material_id - 1;
                                  /* -1 to adopt to C indexing */

        /* reading all necessary values */
        lua_hdis_get_real_field( L, "R", &((*ppAlst)[i].R) );
        lua_hdis_get_real_field( L, "sbb_R", &((*ppAlst)[i].sbb_Rmin) );
        lua_hdis_get_int_field( L, "nverts", &((*ppAlst)[i].nverts) );

        /* getting interaction radius if defined or default it (must
           be after type and R read */
        int status =
          lua_hdis_get_real_field( L, "Ri", &((*ppAlst)[i].ri) );
        if ( !status )
          (*ppAlst)[i].ri = (*ppAlst)[i].sbb_Rmin;
        /* ATTENTION: TODO: CHANGE SBB_RMIN TO APPROPRIATE VALUE */

        int bindx_lua;          /* index of body it belongs to */
        lua_hdis_get_int_field( L, "body", &bindx_lua );
        (*ppAlst)[i].body_indx = bindx_lua - 1 + nBs_hdf;

        lpindex_t ivert0 = 0;
        lua_hdis_get_int_field( L, "ivert0", &ivert0 );
        (*ppAlst)[i].ivert0 = ivert0 - 1 + nVs_hdf;

        LUA_GET_VQ_BY_NAME( vec3d, (*ppAlst)[i], sbb_c );

        lua_pop( L, 1 );  /* for geti */
      }

  lua_pop( L, 1 );   /* for atoms */

  /* cleaning after atoms */
  lua_pushnil( L );
  lua_setfield( L, -2, "atoms" ); /* hdis.atoms = nil */

  /* ------------------------------------------------------------ */

  lua_getfield( L, -1, "verts" );

  if ( lua_istable( L, -1 ) && lua_objlen( L, -1) > 0 )
    for ( lpindex_t i = nVs_hdf; i < (*pnVs); ++i )
      {
        /* index in Lua script */
        lpindex_t lua_ind = (i + 1) - nVs_hdf;
        lua_rawgeti( L, -1, lua_ind ); /* get the element */

        //        LUA_GET_VQ_BY_NAME( vec3d, (*ppVlst)[i], global_pos );
        LUA_GET_VQ_BY_NAME( vec3d, (*ppVlst)[i], local_pos );

        (*ppVlst)[i].deg = 0;

        int ibody_lua;
        lua_hdis_get_int_field( L, "body", &ibody_lua );
        (*ppVlst)[i].ibody = ibody_lua - 1 + nBs_hdf;

        lua_pop( L, 1 );  /* for geti */
      }

  lua_pop( L, 1 );   /* for verts */

  /* cleaning after verts */
  lua_pushnil( L );
  lua_setfield( L, -2, "verts" ); /* hdis.atoms = nil */

  /* restore physical coordinates */
  placement_verts( *ppVlst, *ppBlst, (*pnVs) );

  /* ------------------------------------------------------------ */

  if ( nSs_lua != 0 )
    {
      lua_getfield( L, -1, "springs" );

      if ( lua_istable( L, -1 ) && lua_objlen( L, -1) > 0 )
        for ( lpindex_t i = nSs_hdf; i < (*pnSs); ++i )
          {
            /* index in Lua script */
            lpindex_t lua_ind = (i + 1) - nSs_hdf;
            lua_rawgeti( L, -1, lua_ind ); /* get the element */

            /* reading all necessary values */
            LUA_GET_VQ_BY_NAME( vec3d, (*ppSlst)[i], rb1 );
            LUA_GET_VQ_BY_NAME( vec3d, (*ppSlst)[i], rb2 );

            int bindx_lua;      /* index of bodies it connects to */
            lua_hdis_get_int_field( L, "body1", &bindx_lua );
            (*ppSlst)[i].ib1 = bindx_lua - 1 + nBs_hdf;
            lua_hdis_get_int_field( L, "body2", &bindx_lua );
            (*ppSlst)[i].ib2 = bindx_lua - 1 + nBs_hdf;

            lua_hdis_get_real_field( L, "x0", &((*ppSlst)[i].x0) );
            lua_hdis_get_real_field( L, "k", &((*ppSlst)[i].k) );
            lua_hdis_get_real_field( L, "eta", &((*ppSlst)[i].eta) );

            lua_pop( L, 1 );  /* for geti */
          }

      /* restore physical coordinates */
      placement_springs( *ppSlst, *ppBlst, (*pnSs) );

      lua_pop( L, 1 );   /* for springs */

      /* cleaning after */
      lua_pushnil( L );
      lua_setfield( L, -2, "springs" ); /* hdis.springs = nil */
    }

  /* ------------------------------------------------------------ */

  if ( nTs_lua != 0 )
    {
      lua_getfield( L, -1, "ptriangles" );

      if ( lua_istable( L, -1 ) && lua_objlen( L, -1) > 0 )
        for ( lpindex_t i = nTs_hdf; i < (*pnTs); ++i )
          {
            /* index in Lua script */
            lpindex_t lua_ind = (i + 1) - nTs_hdf;
            lua_rawgeti( L, -1, lua_ind ); /* get the element */

            /* reading all necessary values */

            int bindx_lua;      /* index of bodies it connects to */
            lua_hdis_get_int_field( L, "body1", &bindx_lua );
            (*ppTlst)[i].ib1 = bindx_lua - 1 + nBs_hdf;
            lua_hdis_get_int_field( L, "body2", &bindx_lua );
            (*ppTlst)[i].ib2 = bindx_lua - 1 + nBs_hdf;
            lua_hdis_get_int_field( L, "body3", &bindx_lua );
            (*ppTlst)[i].ib3 = bindx_lua - 1 + nBs_hdf;

            lua_hdis_get_real_field( L, "p", &((*ppTlst)[i].p) );

            int group_id = 1;     /* default group */
            lua_hdis_get_int_field( L, "group", &group_id );
            (*ppTlst)[i].group = group_id - 1;

            lua_pop( L, 1 );  /* for geti */
          }

      lua_pop( L, 1 );   /* for ptriangles */

      /* cleaning after */
      lua_pushnil( L );
      lua_setfield( L, -2, "ptriangles" ); /* hdis.ptriangles = nil */
    }

  /* ------------------------------------------------------------ */

  if ( nHs_lua != 0 )
    {
      lua_getfield( L, -1, "dots" );

      if ( lua_istable( L, -1 ) && lua_objlen( L, -1) > 0 )
        for ( lpindex_t i = nHs_hdf; i < (*pnHs); ++i )
          {
            /* index in Lua script */
            lpindex_t lua_ind = (i + 1) - nHs_hdf;
            lua_rawgeti( L, -1, lua_ind ); /* get the element */

            /* reading all necessary values */

            lua_hdis_get_real_field( L, "rho", &((*ppHlst)[i].rho) );

            lua_hdis_get_real_field( L, "nu", &((*ppHlst)[i].nu) );
            lua_hdis_get_real_field( L, "m", &((*ppHlst)[i].m) );
            lua_hdis_get_real_field( L, "R", &((*ppHlst)[i].R) );
            lua_hdis_get_real_field( L, "h", &((*ppHlst)[i].h) );

            lpindex_t group_id = 0;
            lua_hdis_get_int_field( L, "group_id", &group_id );
            (*ppHlst)[i].group_id = group_id - 1;

            LUA_GET_VQ_BY_NAME( vec3d, (*ppHlst)[i], v );
            LUA_GET_VQ_BY_NAME( vec3d, (*ppHlst)[i], c );

            lua_hdis_get_int_field( L, "flags", &((*ppHlst)[i].flags) );

            lua_pop( L, 1 );  /* for geti */
          }

      lua_pop( L, 1 );   /* for dots */

      /* cleaning after */
      lua_pushnil( L );
      lua_setfield( L, -2, "dots" ); /* hdis.dots = nil */
    }

  /* ------------------------------------------------------------ */

  /*----- hdis.bsphere table for SBB build settings ----- */

  /* All atoms are already known, so we can find Rmin */
  real sbb_rmin = FP_INFINITE;

  for ( lpindex_t i = 0; i < *pnAs; ++i )
    {
      if ( sbb_rmin > (*ppAlst)[i].sbb_Rmin )
        sbb_rmin = (*ppAlst)[i].sbb_Rmin;
    }
  for ( lpindex_t i = 0; i < *pnHs; ++i )
    {
      if ( sbb_rmin > (*ppHlst)[i].R )
        sbb_rmin = (*ppHlst)[i].R;
    }

  /* default values for sbb_params */
  psbb_params->drmin = SBB_DFLT_DRMIN_RMIN_SCALE * sbb_rmin;
  psbb_params->drmax = SBB_DFLT_DRMAX_RMIN_SCALE * sbb_rmin;
  psbb_params->tau   = SBB_DFLT_TAU_DTS * pt->dt;

  lua_getfield( L, -1, "bsphere" );
  if ( lua_istable( L, -1 ) )
    {
      lua_hdis_get_real_field( L, "drmin", &(psbb_params->drmin) );
      lua_hdis_get_real_field( L, "drmax", &(psbb_params->drmax) );
      lua_hdis_get_real_field( L, "tau",   &(psbb_params->tau) );
    }
  lua_pop( L, 1 ); /* hdis.bsphere */

  /* ------------------------------------------------------------ */

  lua_getfield( L, -1, "contact_detection" );

  if ( lua_istable( L, -1 ) )
    {
      char* cdtype;
      lua_getfield( L, -1, "method" );
      cdtype = (char*) lua_tostring( L, -1 );

      if ( cdtype ) /* NULL will give segfault on strcmp, skip */
        {
          if ( strcmp( cdtype, "spx" ) == 0 )
            psettings->cdmethod = SETTINGS_CDM_SPX;
          else if ( strcmp( cdtype, "spsp" ) == 0 )
            psettings->cdmethod = SETTINGS_CDM_SPSP;
          else if ( strcmp( cdtype, "naive" ) == 0 )
            psettings->cdmethod = SETTINGS_CDM_NAIVE;
        }

      lua_pop( L, 1 );              /* remove the string */

      switch ( psettings->cdmethod )
        {
        case SETTINGS_CDM_NAIVE:
          break;
        case SETTINGS_CDM_SPX:
          break;
        case SETTINGS_CDM_SPSP:
          /* we get dx_min also */
          lua_hdis_get_real_field( L, "space_partitioning_mindx",
                                    &psettings->cdm_dx_min );

          /* negative value will mean there is no restrictions (auto) */
          break;
        }
    }

  lua_pop( L, 1 );              /*  remove table from stack */

  /* ------------------------------------------------------------ */

  /* default values first */
  psphprops->rho0  = 1000;
  psphprops->c0    = 5;
  psphprops->gamma = 7;
  psphprops->eps   = 0;
  psphprops->alpha = 1;

  lua_getfield( L, -1, "hydro_props" );

  if ( lua_istable( L, -1 ) )
    {
      lua_hdis_get_real_field( L, "rho0", &psphprops->rho0 );
      lua_hdis_get_real_field( L, "c0", &psphprops->c0 );
      lua_hdis_get_real_field( L, "gamma", &psphprops->gamma );
      lua_hdis_get_real_field( L, "eps", &psphprops->eps );
      lua_hdis_get_real_field( L, "alpha", &psphprops->alpha );
    }
  lua_pop( L, 1 );              /*  remove table from stack */

  psphprops->c2 = psphprops->c0 * psphprops->c0;

  /* ------------------------------------------------------------ */

  psettings->contact_tau0 = 50 * pt->dt; /* for now */
  psettings->contact_delta0 = -0.001 * sbb_rmin; /* for now */
  psettings->contact_alpha = 0.5; /* for now */

  lua_getfield( L, -1, "distance_skip_criteria" );

  if ( lua_istable( L, -1 ) )
    {
      lua_hdis_get_real_field( L, "tau0",
                                &psettings->contact_tau0 );
      lua_hdis_get_real_field( L, "delta0",
                                &psettings->contact_delta0 );
      lua_hdis_get_real_field( L, "alpha",
                                &psettings->contact_alpha );
    }

  lua_pop( L, 1 );              /*  remove table from stack */

  /* ------------------------------------------------------------ */

  /* lua_getfield( L, -1, "model" ); */

  /* bool is_cohesion = true; */
  /* lua_hdis_get_bool_field( L, "cohesion", &is_cohesion ); */
  /* if ( is_cohesion ) */
  /*   { */
  /*     psettings->cohesion_bound = -10.0; */
  /*   } */
  /* else */
  /*   { */
  /*     psettings->cohesion_bound = 0; */
  /*   } */


  /* lua_pop( L, 1 );              /\*  remove table from stack *\/ */

  /* ------------------------------------------------------------ */

  /* ***************** hdis table out ************* */

  if ( pconlist ) free( pconlist );
  lua_settop( L, 0 );

  /* ------------------------------------------------------------ */

  /* a big call for lua garbage collector to clean all memory */
  /* double mem0 = */ (void) lua_gc( L, LUA_GCCOUNT, 0 );
  lua_gc( L, LUA_GCCOLLECT, 0 );
  /* double mem1 = */ (void) lua_gc( L, LUA_GCCOUNT, 0 );
  //  printf( "%lf, %lf\n", mem0, mem1 );
}
