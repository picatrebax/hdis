/*!

  \file timerec.c

*/

#include <stdlib.h>

#include "timerec.h"

#define SCALE 1.e-6             /*!< transform to seconds */

void timerec_init( timerec_t* ptt )
{
  gettimeofday( &ptt->tv, NULL );
  ptt->start = (ptt->tv).tv_sec + (ptt->tv).tv_usec * SCALE;
}

double timerec_lapsed( timerec_t* ptt )
{
  gettimeofday( &ptt->tv, NULL );
  return ( (ptt->tv).tv_sec + (ptt->tv).tv_usec*SCALE - ptt->start );
}

