/*!

  \file energy.c

  \brief Calculating total and max energy in the system

  Note: it seems like using private variable instead of reduction
  operator can accomodate more operations in one loop like both total
  values and max/min values. Although, reduction is smarter way to add
  all the values but there is no much difference unless there is a
  huge number of threads.
*/

#include "energy.h"

void energy( const body_t* restrict pBlst, 
             const lpindex_t nBs, 
             const vec3d_t* restrict pg, 
             const lptimer_t* restrict pt,
             real* restrict petmax,
             real* restrict permax,
             real* restrict pEt, 
             real* restrict pEr, 
             real* restrict pEp )
  
{
  real Et=0; /* Transitional kinetic energy */
  real Er=0; /* Rotational kinetic energy */
  real Ep=0; /* Potential energy */
  
  /* these two values are useful for calcluation of max effective
     velocity in the system to calculate the Courant condition */
  real etmax = 0; /* max transitional energy density  */
  real ermax = 0; /* max rotaional energy density  */
  
  /* compute all energies */
# ifdef PARALLEL_OPENMP
# pragma omp parallel
# endif
  {
    
    real Et_private = 0;
    real Er_private = 0;
    real Ep_private = 0;

    real etmax_private = 0;
    real ermax_private = 0;
    
#   ifdef PARALLEL_OPENMP
#   pragma omp for nowait
#   endif
    for ( lpindex_t i = 0; i < nBs; ++i )
      {
        const body_t * restrict pbody = pBlst + i;
        
        vec3d_t wb;
        vec3d_t Iwb;
        rot3d_t Rot;
        
        rot3d_restore( &Rot, &pbody->q );
        rot3d_apply_tr( &Rot, &wb, &pbody->w );
        vec3d_comp_mult( Iwb, pbody->I, wb );
        
        real etdens = 0.5 * vec3d_sq( pbody->v ); /* density of
                                                     transitional
                                                     energy */
        real Etloc = pbody->m * etdens;
        real Erloc = 0.5 * vec3d_dot( wb, Iwb );
        real Eploc = - pbody->m * vec3d_getz( *pg ) *
          ( vec3d_getz( pbody->c ) - 0.5*(pt->dt)*vec3d_getz( pbody->v ) );

        real erdens = Erloc / pbody->m;

        if ( etdens > etmax_private ) etmax_private = etdens;
        if ( erdens > ermax_private ) ermax_private = erdens;
        
        Et_private += Etloc;
        Er_private += Erloc;
        Ep_private += Eploc;
      }
    
# ifdef PARALLEL_OPENMP
# pragma omp critical
# endif
    {
      if ( etmax < etmax_private ) etmax = etmax_private;
      if ( ermax < ermax_private ) ermax = ermax_private;

      Et += Et_private;
      Er += Er_private;
      Ep += Ep_private;
    }

  } /* end of the parallel region */

  *petmax = etmax;
  *permax = ermax;
  
  *pEt = Et;
  *pEr = Er;
  *pEp = Ep;
}
