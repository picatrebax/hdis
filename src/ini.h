#ifndef INI_H
#define INI_H


#include <math.h>               /* for math */
#include <stdio.h>              /* for printf/fprintf */
#include <string.h>             /* for asprintf */
#include <stdlib.h>             /* for exit */
#include <assert.h>             /* for assert */

#include <lua.h>                /* lua stuff */
#include <lauxlib.h>
#include <lualib.h>

/* external libraries */
// extern int luaopen_luahdf5(lua_State *L);
// int luaopen_lfs (lua_State *L);

#include "defs.h"               /* must for macros */
#include "box.h"                /* box is box */
#include "vec3d.h"              /* vec3d type */
#include "rquat.h"              /* quaternions */
#include "rot3d.h"              /* rotation matrix */
#include "lptimer.h"            /* timer */
#include "body.h"               /* body_t */
#include "atom.h"               /* atom_t */
#include "sph.h"                /* sph_t */

#include "lpio.h"               /* I/O library */
#include "conmech.h"            /* contact mechanics */
#include "coldet.h"             /* collision detection system */
#include "conhash.h"            /* hash table for contacts */
#include "forces.h"             /* for forces computation functions */
#include "dynamics.h"           /* for velocities computation functions */
#include "placement.h"          /* for orientation and position */
#include "highlio.h"            /* high level I/O functions (read) */
#include "bc.h"                 /* boundary conditions */
#include "luahdis.h"           /* hdis lua extension library */
#include "bb.h"                 /* SBB parameters */
#include "portal.h"

#include "hdis-types.h"        /* types */


void ini( settings_t * psettings,
          lptimer_t* pt,
          vert_t**    ppVlst,
          atom_t**    ppAlst,
          body_t**    ppBlst,
          spring_t**  ppSlst,
          ptriangle_t** ppTlst,
          sph_t** ppHlst,
          char ***ppA_gnames,
          int **ppA_intrx,
          material_t **ppA_mats,
          char ***ppA_matnames,
          cmprops_t **ppcmprops,
          sbb_params_t* psbb_params,
          lpindex_t* pnAs,
          lpindex_t* pnVs,
          lpindex_t* pnBs,
          lpindex_t* pnSs,
          lpindex_t* pnTs,
          lpindex_t* pnHs,
          lpindex_t* pnA_gnames,
          lpindex_t* pnA_intrx,
          lpindex_t* pnA_mats,
          lpindex_t* pNcmprops,
          conhash_t*  pch,
          conhash_comp_t*  pch_comp,
          box_t*  pbox,
          vec3d_t*  pg,
          drag_t* pdrag,
          bc_t* pbc,
          sph_props_t* psphprops,
          portal_t* pportal,
          const char* luafile,
          int argc,
          char* argv[],
          lua_State* L )
                   ;

#endif  /* INI_H */
