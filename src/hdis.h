/*!

  \file hdis.h

  \brief Model header file with default settings.

*/


#ifndef HDIS_H
#define HDIS_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <math.h>               /* for math */
#include <stdio.h>              /* for printf/fprintf */
#include <string.h>             /* for asprintf */
#include <stdlib.h>             /* for exit */
#include <assert.h>             /* for assert */
#include <unistd.h>             /* for getopt */

#include <lua.h>                /* lua stuff */
#include <lauxlib.h>
#include <lualib.h>
#include <luaconf.h>

#ifdef HDIS_FP_CHECK           /* files included only if FP_CHECK */

#if HAVE_FENV_H
#include <fenv.h>               /* adding NaN check for Linux */
#endif

#if HAVE_XMMINTRIN_H
#include <xmmintrin.h>       /* adding NaN check for SSE and MacOS */
#endif

#endif  /* HDIS_FP_CHECK */

/* default script/configuration file name */
#define HDIS_DEFAULT_LUA "hdisrun.lua"

/* settings flags  */
#define SETTINGS_F_GENERATOR           0x1
#define SETTINGS_F_SAVE_START          0x2

/* walton vs. dL/dt for angular velocity */
#define SETTINGS_F_OMEGA_WALTON        0x4

/* quaternion scheme: Trigonometry */
#define SETTINGS_F_QUAT_TRIG           0x8

/* types of drag force: not exclusive */
#define SETTINGS_F_DRAG_ON             0x10
#define SETTINGS_F_DRAG_REYNOLDS       0x20
#define SETTINGS_F_DRAG_UNI_REYNOLDS   0x40
#define SETTINGS_F_DRAG_STOKES         0x80
#define SETTINGS_F_DRAG_UNI_STOKES     0x100

/*! contact detection method */
#define SETTINGS_CDM_NAIVE  0x0
#define SETTINGS_CDM_SPX    0x1 /* x-dir sort and prune */
#define SETTINGS_CDM_SPSP   0x3 /* space partitioning (xy) - sort and
                                         prune (z) */

/*! Dynamics Wolton-Braun iteration accuracy */
#define DEM_MOTION_EPS  0.01

/*! Name for the program  */
#define PROGRAM_NAME "HDIS"

/*! Brief name for the program for error macros */
#define PROGRAM_NAME_BRIEF "hdis"

/*! Parallel mode */
#if defined ( PARALLEL_OPENMP ) && defined ( _OPENMP )
#  define PROGRAM_PARALLEL_MODE "OpenMP parallel mode"
#elif defined ( PARALLEL_MPI )
#  define PROGRAM_PARALLEL_MODE "MPI mode"
#else
#  define PROGRAM_PARALLEL_MODE "Serial mode"
#endif

/*! Brief description of the program  */
#define PROGRAM_DESCRIPTION \
"Controllable Object Unbound Particles Interaction\n" \
"   Discrete Element Method Model" " (" PROGRAM_PARALLEL_MODE ")"

#endif  /* HDIS_H */

