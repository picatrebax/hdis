/*!

  \file forces.h

  \brief Forces computations on bodies

*/

#ifndef FORCES_H
#define FORCES_H

#include <math.h>               /* for math */
#include <stdio.h>              /* for printf/fprintf */
#include <stdlib.h>             /* for exit */
#include <stddef.h>             /* for offset */

#include <lua.h>                /* lua stuff */
#include <lauxlib.h>
#include <lualib.h>

#ifdef _OPENMP
#include <omp.h>
#endif

#include "defs.h"               /* must for macros */
#include "box.h"                /* box is box */
#include "vec3d.h"              /* vec3d type */
#include "lptimer.h"            /* timer */
#include "body.h"               /* body_t */
#include "atom.h"               /* atom_t */
#include "spring.h"             /* spring_t */
#include "ptriangle.h"          /* ptriangle_t */
#include "conmech.h"            /* contact mechanics */
#include "conhash.h"            /* hash table for contacts */
#include "lpdyna.h"             /* dynamic array lpdyna_t */
#include "coldet.h"             /* collision_t */

#include "hdis-types.h"        /* types */


void forces_init( const lpindex_t nBs );
void forces_finalize();

/* \brief Gravitation is the first forces/torques procedure that clears all old
   forces and torques and assings gravitational forces to all the bodies.

*/
void forces_gravitation( body_t * restrict pBlst,
                         const lpindex_t nBs,
                         const vec3d_t* restrict g  );

/*!  \brief Forces from external sources defined in lua script.
  */
void forces_extforces( body_t * restrict pBlst,
                       const lpindex_t nBs,
                       lua_State* L );

void forces_drag( body_t * restrict pBlst,
                  const lpindex_t nBs,
                  const drag_t * restrict pdrag );

void forces_bb( body_t* pBlst,
                atom_t* restrict pAlst,
                vert_t* restrict pVlst,
                const lpdyna_t* pcollisions,
                const cmprops_t * restrict pcmprops,
                const lpindex_t nBs,
                const lpindex_t nAs,
                const lpindex_t Nmats,
                conhash_t* pch,
                const real dt,
                const size_t ncurrent,
                settings_t *psettings,
                diagnostics_t* pdiag,
                real* pvc_sq_max );

void forces_springs( body_t* pBlst,
                     spring_t* restrict pSlst,
                     const lpindex_t nSs );

void forces_ptriangles( body_t* pBlst,
                        ptriangle_t* restrict pTlst,
                        const lpindex_t nTs );

#endif  /* FORCES_H */
