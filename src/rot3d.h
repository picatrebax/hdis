#ifndef ROT3D_H
#define ROT3D_H

#include <math.h>

#include "vec3d.h"
#include "rquat.h"


typedef struct rot3d_s {
  vec3d_t ex;            /* coordinates of (1,0,0) in old system */
  vec3d_t ey;
  vec3d_t ez;
} rot3d_t;

/*
   rot3d R   - [input]  rotation operator,
   vec3d a_g - [output] calculated coordinates of a in global frame,
   vec3d a_b - [input]  coordinates of a in body frame
 */
void rot3d_apply( const rot3d_t* restrict pR,
                        vec3d_t* restrict pa_g,
                  const vec3d_t* restrict pa_b )

                      ;

/*
   rot3d R   - [input]  rotation operator,
   vec3d a_b - [output] calculated coordinates of a in body frame,
   vec3d a_g - [input]  coordinates of a in global frame
 */
void rot3d_apply_tr( const rot3d_t* restrict pR,
                           vec3d_t* restrict pa_b,
                     const vec3d_t* restrict pa_g )


                         ;

void rot3d_tr( rot3d_t * restrict pRt, const rot3d_t * restrict pR );

real rot3d_det( const rot3d_t * restrict pR );
void rot3d_restore( rot3d_t * restrict pR, const rquat_t * restrict pq );

void rot3d_ang_vel( const rot3d_t* restrict pR, 
                    vec3d_t* restrict pw, 
                    const vec3d_t* restrict pIb, 
                    const vec3d_t* restrict pL );

void rot3d_ang_mom( const rot3d_t* restrict pR, 
                    vec3d_t* restrict pL, 
                    const vec3d_t* restrict pIb, 
                    const vec3d_t* restrict pw );

void rot3d_pprint( const rot3d_t* pR );


#endif

