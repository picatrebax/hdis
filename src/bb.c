#include "bb.h"

void sbb_build( sbb_t* pSlst,
                const atom_t* restrict pAlst,
                const lpindex_t nAs,
                const sph_t* restrict pHlst,
                const lpindex_t nHs )
{
  /* All other parameters */
# ifdef PARALLEL_OPENMP
# pragma omp parallel for
# endif
  for ( lpindex_t i = 0; i < nAs; ++i )
  {
    /* body index and group id are just copied */
    pSlst[i].body_indx = pAlst[i].body_indx;
    pSlst[i].group_id = pAlst[i].group_id;
    pSlst[i].flag = SBB_F_DEM;
  }

  /* loop over sph particles */
  for ( lpindex_t i = 0, j = nAs; i < nHs; i++, j++ )
    {
      /* body index makes no sense for sph, it is just a placeholder */
      pSlst[j].body_indx = j;
      /* rmin, because it seems senseful to have minimum radius of
         bounding box as a parameter of a bounding box */
      pSlst[j].rmin = pHlst[i].R;
      pSlst[j].group_id = pHlst[i].group_id;
      pSlst[j].flag = SBB_F_SPH;
    }
}

void sbb_reset( sbb_t* pSlst, atom_t* pAlst,
                body_t* pBlst, const lpindex_t nAs,
                const sph_t* restrict pHlst, const lpindex_t nHs,
                const sbb_params_t* restrict psbb_params )
{
# ifdef PARALLEL_OPENMP
# pragma omp parallel for
# endif
  for ( lpindex_t i = 0; i < nAs; ++i )
  {
    /* initial center coincide with the center at this stage */
    vec3d_assign( pSlst[i].c0, pSlst[i].c );

    /* calculating velocity of the SBB center */
    body_t *pbody = pBlst + pSlst[i].body_indx;
    vec3d_t v;
    vec3d_t r;
    vec3d_diff( r, pSlst[i].c0, pbody->c );
    vec3d_x( v, pbody->w, r )
    vec3d_sum( v, pbody->v, v );

    /* adjusting radius of SBB to new velocities */
    real dr = fmin ( psbb_params->drmin + psbb_params->tau * vec3d_abs(v),
                     psbb_params->drmax );
    pSlst[i].r = pAlst[i].sbb_Rmin + dr;
  }

  /* loop over sph particles */
  for ( lpindex_t i = 0, j = nAs; i < nHs; ++i, j++ )
    {
      /* initial center coincide with the center at this stage */
      vec3d_assign( pSlst[j].c0, pSlst[j].c );

      /* calculating velocity of the SBB center */
      vec3d_t v;
      vec3d_assign( v, pHlst[i].v );

      /* adjusting radius of SBB to new velocities */
      real dr = fmin ( psbb_params->drmin + psbb_params->tau * vec3d_abs(v),
                       psbb_params->drmax );
      pSlst[j].r = pSlst[j].rmin + dr;
    }
}
