/*!

  \file body.h

  \brief Rigid body main structure

*/

#ifndef BODY_H
#define BODY_H

#include <stdint.h>

#include "defs.h"               /* definitions */
#include "vec3d.h"              /* vec3d_t */
#include "rquat.h"              /* rquat_t */

#define BODY_CONTROL_FREE       ((lpindex_t)( 0))
#define BODY_CONTROL_STEADY     ((lpindex_t)(-1))
#define BODY_CONTROL_STILL      ((lpindex_t)(-2))
#define BODY_CONTROL_SENSITIVE  ((lpindex_t)(-3))

#define BODY_MONITOR_NO ((lpindex_t)(0))

#define BODY_EXTFORCE_NO ((lpindex_t)(0))

#define BODY_POSITIONING_NO ((lpindex_t)(0))

/*! Flags values for body state */
#define BODY_F_FREE       0x1
#define BODY_F_CONTROLLED 0x2
#define BODY_F_MONITORED  0x4
#define BODY_F_STEADY     0x8
#define BODY_F_STILL      0x10

#define BODY_F_SENSITIVE  0x20
#define BODY_F_EXTFORCED  0x40
#define BODY_F_POSITIONED 0x80

#define BODY_F_NOXBOUND   0x100
#define BODY_F_NOYBOUND   0x200

#define BODY_F_TELEPORT   0x400
#define BODY_F_SPHNEUTRAL 0x800 /* sph-dem: forces are not transmitted to the body */

typedef struct body_s {

  rquat_t q;           /* orientation quaternion */

  vec3d_t c;           /* center of mass position */
  vec3d_t v;           /* center of mass velocity */
  vec3d_t w;           /* angular velocity (center of mass) */
  vec3d_t dwb;         /* change in angular velocity at time step in
                          body frame */

  vec3d_t f;           /* net force acting to the particle */
  vec3d_t t;           /* net torque */
  vec3d_t l;           /* angular momentum */

  vec3d_t I;           /* principal moments of inertia */

  real m;              /* mass */
  real V;              /* volume */

  lpindex_t ifunc;     /* index of control function */
  lpindex_t imonitor;  /* index of monitoring function */
  lpindex_t effunc;    /* index of external force/torque function */
  lpindex_t posfunc;   /* index of positioning function */

  uint16_t  flag;      /* status flag */

} body_t;


#endif  /* BODY_H */

