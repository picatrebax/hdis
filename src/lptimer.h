/*!

  \file lptimer.h

  \brief Time main structure

*/

#ifndef LPTIMER_H
#define LPTIMER_H

#include <stdlib.h>
#include <stdbool.h>
#include "defs.h"

/* timer object to handle time in the model */
typedef struct lptimer_s {
  real t;                       /* current time */
  real t0;                      /* initial time */
  real te;                      /* end time */
  real dt;                      /* time step */
  real ts;                      /* saving time */
  real dts;                     /* save time step */
  size_t n;                     /* number of steps */
  size_t ns;                    /* number of savings */
} lptimer_t;


void lptimer_init( lptimer_t * pt, /* lptimer structure */
                   const real t0,  /* initial time */
                   const real te,  /* ending time */
                   const real dt,  /* initial time step */
                   const real ts,  /* next saving time (may be set
                                      to t0 or t0+dts) */
                   const real dts ); /* saving interval */

/*! \brief increments timer  */
int lptimer_inc( lptimer_t * pt );

/*! \brief returns true if it is time to save. It also updates the
    saving time for future in the case it returned true */
bool lptimer_is_savetime( lptimer_t * pt );


#endif  /* LPTIMER_H */

