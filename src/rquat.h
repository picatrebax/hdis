/*!

  \file rquat.h

  \brief Rotational quaternion

*/

#ifndef RQUAT_H
#define RQUAT_H

#include <math.h>

#include "defs.h"               /* real type */
#include "vec3d.h"              /* vec3d type */

  typedef struct rquat_s {
    real s;                     /* scalar part */
    real vx;                    /* vector part */
    real vy;
    real vz;
  } rquat_t;


/*  technically we could use memcpy (check the option) */

/*  assining rquat by another rquat */
#define rquat_assign( rq_copy, rq_orig ) \
  {                                      \
    (rq_copy).s  = (rq_orig).s;          \
    (rq_copy).vx = (rq_orig).vx;         \
    (rq_copy).vy = (rq_orig).vy;         \
    (rq_copy).vz = (rq_orig).vz;         \
  }

/* setting rquat by 4 numbers */
#define rquat_set_vec3d( rq, s_, vec3d )     \
  {                                          \
    (rq).s  = s_;                            \
    (rq).vx = vec3d_getx(vec3d);             \
    (rq).vy = vec3d_gety(vec3d);             \
    (rq).vz = vec3d_getz(vec3d);             \
  }

/* setting rquat by 4 numbers */
#define rquat_set( rq, s_, vx_, vy_, vz_ )   \
  {                                          \
    (rq).s  = s_;                            \
    (rq).vx = vx_;                           \
    (rq).vy = vy_;                           \
    (rq).vz = vz_;                           \
  }

/* setting all to zero */
#define rquat_setnull( rq ) { (rq).s = (rq).vx =        \
                              (rq).vy = (rq).vz = 0.0; }

/* multiplication by a real number */
#define rquat_scale_mult( rq, num )               \
  {                                               \
    (rq).s = (num) * (rq).s;                      \
    (rq).vx = (num) * (rq).vx;                    \
    (rq).vy = (num) * (rq).vy;                    \
    (rq).vz = (num) * (rq).vz;                    \
  }

/* division by a real number */
#define rquat_scale_div( rq, num )               \
  {                                              \
    (rq).s = (rq).s / (num);                     \
    (rq).vx = (rq).vx / (num);                   \
    (rq).vy = (rq).vy / (num);                   \
    (rq).vz = (rq).vz / (num);                   \
  }

#define rquat_norm( rq )                                                \
  sqrt( (rq).s*(rq).s + (rq).vx*(rq).vx + \
        (rq).vy*(rq).vy + (rq).vz*(rq).vz )

#define rquat_renorm( rq )                      \
  {                                             \
    real norm = rquat_norm( rq );               \
    rquat_scale_div( rq, norm );                \
  }

                                /* must follow scaling */
#define rquat_product( q, a, b )                                      \
  {                                                                   \
    real s, vx, vy, vz;           /* result */                        \
    s  = (a).s*(b).s - (a).vx*(b).vx - (a).vy*(b).vy - (a).vz*(b).vz; \
    vx = (a).s*(b).vx + (a).vx*(b).s + (a).vy*(b).vz - (a).vz*(b).vy; \
    vy = (a).s*(b).vy - (a).vx*(b).vz + (a).vy*(b).s + (a).vz*(b).vx; \
    vz = (a).s*(b).vz + (a).vx*(b).vy - (a).vy*(b).vx + (a).vz*(b).s; \
    (q).s = s; (q).vx = vx; (q).vy = vy; (q).vz = vz;                 \
  }


#define rquat_gets(rq)  ((rq).s)
#define rquat_getvx(rq) ((rq).vx)
#define rquat_getvy(rq) ((rq).vy)
#define rquat_getvz(rq) ((rq).vz)


#define rquat_pprint( q ) printf( #q " = ( %f, %f, %f, %f )\n", \
                                  (q).s, (q).vx, (q).vy, (q).vz )


#endif  /* RQUAT_H */

