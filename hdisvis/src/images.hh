#ifndef IMAGES_H
#define IMAGES_H

#include <stdlib.h>
#include "defs.hh"

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include <wx/wx.h>
#include <wx/glcanvas.h>

#ifdef HAVE_PNG
#include <png.h>
#endif

// libavutil/common.h can produce an error with C++ because 
// stdint.h only defines UINT64_C for C and not for C++ 
#ifndef INT64_C
#define INT64_C(c) (c ## LL)
#define UINT64_C(c) (c ## ULL)
#endif

#ifdef HAVE_VIDEO
extern "C" {
#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
#include <libavutil/avutil.h>
}
#endif

/*! \brief Video information and preferences. Frames are captured as screenshots and then compiled with the avconv commandline utility. */
class Video
{
public:
  Video(wxString name, int x, int y, bool capture, bool load, float time, unsigned int bps);

  int w;               /*!< \brief Video (and model) width in pixels */
  int h;               /*!< \brief Video (and model) height in pixels */
  wxString fname;      /*!< \brief Output video file name with path */
  bool auto_record;    /*!< \brief If frames should be automatically captured periodically */
  bool auto_advance;   /*!< \brief If a new h5 files should be loaded after each frame is captured */
  float delay;         /*!< \brief Time delay between automatic captures */
  int nframes;         /*!< \brief Number of frames recorded so far */
  float next_cap;      /*!< \brief Time until next automatic capture */

#ifdef HAVE_VIDEO
  AVCodec* codec;
  AVCodecContext* c;
  int ret;
  FILE *f;
  AVFrame *picture;
  AVPacket pkt;
#endif //HAVE_VIDEO

  void encode_frame(wxGLCanvas *canvas);
  void close();
};

bool LoadPNG(char *name, unsigned int* w, unsigned int* h, GLubyte **outData);
void ScreenshotPNG(wxString fname, wxGLCanvas *canvas);
void ScreenshotTGA(wxString fname, wxGLCanvas *canvas);

#endif //IMAGES_H
