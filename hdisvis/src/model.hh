#ifndef MODEL_H
#define MODEL_H

#define GL_GLEXT_PROTOTYPES
#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glext.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#else
#include <GL/gl.h>
#include <GL/glext.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif

/* LINUX COMPATIBILITY:
      wx/glcanvas.h includes GL/glext.h, so if you need to define GL_GLEXT_PROTOTYPES,
      do it before including wx/glcanvas.h
      */
#include <wx/wx.h>
#include <wx/glcanvas.h>
#include <wx/progdlg.h>

#include <list>
#include <vector>
#include <queue>
#include <algorithm>
#include <cmath>
#include <ctime>
using namespace std;

#include "defs.hh"
#include "utility.hh"

#include "vars.hh"
#include "data.hh"
#include "frame.hh"

class wxhdisvis;

class ForceChainEdge
{
public:
  int id1;
  int id2;
  double f;
  int dist;

  ForceChainEdge(int i, int j, double x, int dist);
};

class CompareForceChainEdge
{
public:
  bool operator()(ForceChainEdge& e1, ForceChainEdge& e2);
};

class Actor
{
public:
  struct vecGL { GLfloat x, y, z; };
  struct Vert { vecGL r, n; };
  std::vector<Vert> verts{};
  GLuint VBOid{};
  GLuint nVerts{};

  void create();
  void perform();
  ~Actor();
};

// =========================================================================

/*! \brief OpenGL render target
 */
class Model : public wxGLCanvas
{
public:
  /*! \brief Model constructor
   * \param parent Parent frame
   * \param var Pointer to application variables
   * \param cpdata Pointer to HDF5 data
   * \param id Unique id
   * \param pos Initial position
   * \param size Initial size
   * \param style GLCanvas style
   * \param name GLCanvas name
   */
  Model(wxWindow *parent, Vars *var, Data *cpdata,
        wxWindowID id = wxID_ANY,
        const wxPoint& pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize, long style = 0,
        const wxString& name = wxT("model"));

  bool InitGL();
  void SetProjection();
  void SetModelview();

  void OnPaint(wxPaintEvent& event);
  void Render();

  void SetSpectrumColor(float v, float alpha);

  void DrawHdisvisAtoms(bool force_color = false);
  void DrawHdisvisAtom(int i, bool force_color);
  void DrawLogo();
  void DrawForceChain();
  void DrawBox();
  void DrawContacts();
  void DrawAxisDisplay();
  void DrawTime();
  void DrawSphere(float r, GLUquadric* pquad, vec3d_t p0);
  void DrawCylinder(float r, GLUquadric* pquad, vec3d_t p0, vec3d_t p1);
  void DrawSprings();
  void DrawPTriangles();
  void DrawSpeed();
  
  void DrawSpheres();

  bool Clipped(vec3d_t pos);

  void ComputeForceChain(int n);
  int GetBody(int x, int y);
  void SetUniqueBodyColor(int atom_id);

  /** @name Mouse/keyboard event handers
   *
   * \param event The caught event
   */
  //@{
  void OnMouseDown(wxMouseEvent &event);
  void OnMouseUp(wxMouseEvent &event);
  void OnMouseMove(wxMouseEvent &event);
  void OnKeyPress(wxKeyEvent &event);
  void OnArrowPress(wxKeyEvent &event);
  //@}

  int KnobIsClicked(int x, int y);
  void AdjustClipPos(int clip_plane);
  void DrawClipPlanes();
  void DrawClipPlane(int clip_plane);
  void DrawClipControl(vec3d_t color, int clip_plane);
  vec3d_t FindIntersection(double p1, double p2, char i, int clip_plane);
  float PolarAngle(vec3d_t a, int clip_plane);

  unsigned int cur_VBOid;  /*!< \brief id of the vbo currently bound to GL_ARRAY_BUFFER */
  int cur_body;            /*!< \brief id of the most recently clicked body */
  int cur_clip;
  list<ForceChainEdge> force_chain;
  bool force_chain_current;
  wxGLContext* context;    /*!< \brief The only openGL context used by the application */
  Vars *v;                 /*!< \brief Pointer to global variables/settings */
  Data *data;              /*!< \brief Pointer to h5 data and information */
  wxhdisvis* hdisvis;        /*!< \brief Pointer to application class */
  
  Actor sphereActor; //!< VBO handler object for drawing spheres
};

#endif //MODEL_H
