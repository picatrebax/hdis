#include "hdisvis.hh"

IMPLEMENT_APP(wxhdisvis)

wxhdisvis::~wxhdisvis()
{
    data->Clear();
    delete data;
    delete v;
    delete model;
    delete log;
}
  
/*! \brief Initialize all windows, vars, and data
 *
 * Creates and initializes all necessary classes, including all
 * windows, variables, and data.  The command line is parsed for input
 * HDF5 and config files. If no config is supplied, the default file
 * is loaded. If no HDF5 files are provided, a file dialog prompts for
 * HDF5 selection.
 */
bool wxhdisvis::OnInit()
{
  v = new Vars();
  data = new Data(&v->redraw_threshold, &v->sphere_adjustment);
  video = NULL;

  logFrame = new HdisvisFrame(FRAME_LOG, _("Debug Log"), data, v, this);
  log = new wxLogTextCtrl((wxTextCtrl*)logFrame->FindWindow(ID_LOG_TEXT));
  wxLog::SetActiveTarget(log);

	// parse the command line
	// if the parsing fails, abort the program
  if (ParseCommandLine() == false) return false;
  logFrame->Show(v->DEBUG);

  SetLogo();
  /* Instead of loading the logo from file, the pixel data is now hard
   *   coded. This was done since the logo is small and now we don't
   *   need to install another file that hdisvis must keep track of.
   *
   * If a new logo is desired, load it from file like this:
#ifdef __APPLE__
  LoadLogo(wxStandardPaths::Get().GetExecutablePath().BeforeLast(wxUniChar('/')).Append(_("/../Resources/logo.png")));
#else
  LoadLogo(_("logo.png"));
#endif
  */

  configFrame = new HdisvisFrame(FRAME_CONFIG, _("Settings"), data, v, this);
  carFrame = new HdisvisFrame(FRAME_CAR, _("Color Properties"), data, v, this);

  // if we don't save the model size before creating the window, then the window is initialized
  // it will reset the model size to wxDefaultSize
  int w = v->model_w;
  int h = v->model_h;
  modelFrame = new HdisvisFrame(FRAME_MODEL, _("Model"), data, v, this);
  model = (Model *) modelFrame->FindWindow(ID_MODEL);
  modelFrame->Show(true);
  model->InitGL();
  OnModelResize(w, h);

  // Make sure controls are in the correct initial state
  OnDisplaymode(v->displaymode);
  OnRendermode(v->rendermode);
  OnArrowmode(v->arrowmode);

  ArrangeFrames();
	// if file names are supplied on the command line, get them with a file dialog
  if (v->filenames.GetCount() == 0) GetFilenames();
  else OpenNewData();

  carFrame->Show(true);
  configFrame->Show(true);
  modelFrame->SetFocus();

  return true;
}

/*! \brief Load logo image from file
 * \param file Image file name
 *
 * Uses libpng to load the logo pixel data as RGBA, and computes the alpha channel since the provided png
 * has an opaque background color that we don't want blocking any of the model.
 */
void wxhdisvis::LoadLogo(wxString file)
{
  logo_w = logo_h = 0;
  logo = NULL; // the array is malloc'd in LoadPNG
  if (LoadPNG(file.char_str(), &logo_w, &logo_h, &logo) == false)
  {
    wxLogMessage(_("Unable to load logo data"));
  }
  else // strip the logo background
  {
    wxLogMessage(_("Loaded %dx%d logo data"), logo_w, logo_h);
    for (unsigned int pix = 0; pix < logo_w*logo_h; ++pix)
    {
      GLubyte r = logo[4*pix + 0];
      GLubyte g = logo[4*pix + 1];
      GLubyte b = logo[4*pix + 2];

      // for shades of grey low blue means close to black, so make it more opaque (and for grey red == green)
      // for shades of orange, there is still a little blue left so adjust by red - green,
      //   which is sort of like "orangeness"
      logo[4*pix + 3] = min(255, 255 - b + (r - g));
    }
  }
}

/*! \brief Set logo pixel data without a file
 * \return
 */
void wxhdisvis::SetLogo()
{
  logo_w = 80;
  logo_h = 40;
  logo = new GLubyte[logo_w*logo_h*4];
  GLubyte raw_pixels[80*40*4] = LOGO_PIXEL_DATA;
  memcpy(logo, raw_pixels, logo_w*logo_h*4);  
}

/****************************************************************
 *
 *              COMMAND LINE
 *
 ****************************************************************/


/*! \brief Read and process any command line arguments
 *  \return True on success
 */
bool wxhdisvis::ParseCommandLine()
{
  wxCmdLineEntryDesc cmdLineDesc[] = {
    {wxCMD_LINE_SWITCH,
      _("d"),
      _("debug"),
      _("Enable command line debugging output")
    },
    {wxCMD_LINE_SWITCH,
      _("h"),
      _("help"),
      _("Print available command line inputs"),
      wxCMD_LINE_VAL_NONE,
      wxCMD_LINE_OPTION_HELP
    },
    {wxCMD_LINE_OPTION,
      _("c"),
      _("config"),
      _("lua configuration file"),
      wxCMD_LINE_VAL_STRING
    },
    {wxCMD_LINE_PARAM,
      NULL,
      NULL,
      _("HDF5 files"),
      wxCMD_LINE_VAL_STRING,
      wxCMD_LINE_PARAM_MULTIPLE | wxCMD_LINE_PARAM_OPTIONAL
    },
    {wxCMD_LINE_NONE}
  };

  wxCmdLineParser parser (cmdLineDesc, argc, argv);
  parser.Parse();

  // Abort on a help request, since it just displays info to
	// the user on the command line
  if (parser.Found(wxT("h"))) return false;

  wxString localPath = wxGetCwd().Append(_("/"));

  //Read in config file
  wxString configName = wxEmptyString;
  parser.Found(wxT("c"), &configName);
  // config file supplied on command line
  if (configName != wxEmptyString)
  {
    if (configName[0] != wxUniChar('/'))
        configName.Prepend(localPath);
    LoadConfig(configName);
  }
  else
  {
#ifdef __APPLE__
    wxString defaultConfig = wxStandardPaths::Get().GetExecutablePath();
    defaultConfig.BeforeLast(wxUniChar('/')).Append(_("/../Resources/DefaultConfig.lua"));
#else
    wxString defaultConfig = wxStandardPaths::Get().GetUserConfigDir();
    defaultConfig.Append(_("/.hdisvis"));
#endif
    if (!LoadConfig(defaultConfig))
    {
      wxLogMessage(_("Writing %s"), defaultConfig);
      v->WriteConfig(defaultConfig, data);
    }
  }
  
  if (parser.Found(wxT("d"))) v->DEBUG = true;

	// everything not supplied as a switch is assumed to be a file name
  int num_files = parser.GetParamCount();
  if (num_files > 0)
  {
    wxString fileName;
    for (int i=0; i<num_files; ++i)
    {
      fileName = parser.GetParam(i);
      v->short_filenames.Add(fileName.AfterLast(wxUniChar('/')));

      //prepend local path for relative paths
      if (fileName[0] != wxUniChar('/'))
        fileName.Prepend(localPath);

      v->filenames.Add(fileName);
    }
  }
  return true;
}

/****************************************************************
 *
 *              FILE HANDLING
 *
 ****************************************************************/

/*! \brief Display the manual using the operating systems natural PDF viewer
 * \return
 */
void wxhdisvis::OpenManual()
{
#ifdef __APPLE__
  wxString doc = wxStandardPaths::Get().GetExecutablePath();
	doc.BeforeLast(wxUniChar('/')).Append(_("/../Resources/manual.pdf"));
#else
  wxString doc = _("/usr/local/share/doc/hdisvis/manual.pdf");
#endif
  wxLaunchDefaultApplication(doc);
}

/*! \brief Get HDF5 filenames from user with a file dialog
 *
 * Allows the user to select multiple HDF5 files, and then loads the file names into
 * Vars::short_filenames and the paths into Vars::filenames. Selecting many files can
 * be very slow on linux due to a general problem with file dialogs that is not specific
 * to hdisvis.
 */
void wxhdisvis::GetFilenames()
{
  wxFileDialog fopen(NULL, _("Select h5 files"), wxEmptyString, wxEmptyString,
      "HDF5 (*.h5)|*.h5", wxFD_OPEN|wxFD_MULTIPLE|wxFD_FILE_MUST_EXIST);

  if (fopen.ShowModal() == wxID_CANCEL)
    return;     // the user changed their mind...

  wxLogMessage(_("Processing file selection..."));

  fopen.GetPaths(v->filenames);
  fopen.GetFilenames(v->short_filenames);

  wxLogMessage(_("Opening new data..."));
  OpenNewData();
}

/*! \brief Process a new set of HDF5 files
 *
 * Resets all necessary windows and variables when a new set of HDF5 files is loaded.
 * This includes centering the clipping plane, adjusting the model size and zoom distance,
 * and arranging all of the windows
 */
void wxhdisvis::OpenNewData(int n)
{
	unsigned int nAGs = 0;
  unsigned int nSGs = 0;
  unsigned int nTGs = 0;
	// if we already have some data loaded, we want to save the colors we have for groups
  if (data->initialized)
  {
		// the number of groups we want to save color data for
    nAGs = data->nAGs;
    nSGs = data->nSGs;
    nTGs = data->nTGs;
  }

	// temp arrays that will hold the old color data
  Color* atom_colors      = new Color[nAGs];
  Color* spring_colors    = new Color[nSGs];
  Color* ptriangle_colors = new Color[nAGs];

	// copy over the color data so it isn't lost during the init routine
  memcpy(atom_colors,      data->group_atom_colors,      sizeof(Color) * nAGs);
  memcpy(spring_colors,    data->group_spring_colors,    sizeof(Color) * nSGs);
  memcpy(ptriangle_colors, data->group_ptriangle_colors, sizeof(Color) * nTGs);

  // reinitialize data with new files
  if (n < (int)v->filenames.size())
    v->file = n;
  else
    v->file = 0;
  
  if (v->file < 0)
    v->file = 0;
  
  data->Initialize(v->filenames, v->rendermode == RENDERMODE_POLY);

	// recenter the clipping plane to make sure it doesn't end up out
	// in the middle of nowhere
  for (unsigned int cp = 0; cp < v->clip_pos.size(); ++cp)
  {
    vec3d_set(v->clip_pos[cp], data->box.x/2, data->box.y/2, data->box.z/2);
    model->AdjustClipPos(cp);
  }

  // determine the initial zoom - values determined by reverse engineering different box sizes
  float A, B, C;
  if (data->box.y < data->box.z)
  {
    A =  0.59;
    B = -1.94;
    C =  4.98;
  }
  else
  {
    A = 0.49;
    B = 0.16;
    C = 3.81;
  }
  v->zoom = A*data->box.x + B*data->box.y + C*data->box.z;
  v->zoom *= 1.2; // zoom out further for time and axis display and stuff

  LoadFile(v->file);

  // start from the colors in the config file, then overwrite with any new user-defined colors
  SetGroupColors();

	// restore any old colors we had from previous data
  memcpy(data->group_atom_colors,      atom_colors,      sizeof(Color) * min(nAGs, data->nAGs));
  memcpy(data->group_spring_colors,    spring_colors,    sizeof(Color) * min(nAGs, data->nSGs));
  memcpy(data->group_ptriangle_colors, ptriangle_colors, sizeof(Color) * min(nAGs, data->nTGs));

  delete[] atom_colors;
  delete[] spring_colors;
  delete[] ptriangle_colors;

	// if we had previous data, make sure the new groups have the same display modes
	for (unsigned int i = 0; i < data->nAGs; ++i)
		data->atom_group_color_by_force[i] = (v->displaymode == DISPLAYMODE_FORCE);

  ArrangeFrames();
  modelFrame->Show(true);
}

void wxhdisvis::ReloadData()
{
  OpenNewData();
}

/*! \brief Open a new config file though a file dialog
 * \return
 */
void wxhdisvis::OpenConfig()
{
  wxFileDialog fopen(NULL, _("Select Lua file"), wxEmptyString, wxEmptyString,
      "LUA (*.lua)|*.lua", wxFD_OPEN|wxFD_FILE_MUST_EXIST);

 
  if (fopen.ShowModal() == wxID_CANCEL)
    return;     // the user changed their mind...

  wxString fname = fopen.GetPath();
  wxString ext = fname.AfterLast(wxUniChar('.'));
  if (!ext.IsSameAs(_("lua")))
  {
    wxMessageDialog(NULL,
          wxString::Format(_("Unsupported config file: *.%s"), ext),
          wxEmptyString, wxOK, wxDefaultPosition).ShowModal();
    return;
  }
  if (LoadConfig(fname))
  {
    SetGroupColors();
    // resize the window in case it has been modified
    OnModelResize(v->model_w, v->model_h);
    UpdateConfigWindow();
    ArrangeFrames();
  }
}

/*! \brief Read and load variables from a config file
 *  \param fname Path to config file, relative to the working directory
 *  \return True on success
 */
bool wxhdisvis::LoadConfig(wxString fname)
{
  return v->LoadConfig(fname);
}

/*! \brief Write current vairiables to a config file
 * \return
 */
void wxhdisvis::SaveConfig()
{
  wxFileDialog fsave(NULL, wxEmptyString, wxEmptyString,
        _("config"),
        _("Lua (*.lua)|*.lua"), wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
  if (fsave.ShowModal() == wxID_OK)
  {
    v->WriteConfig(fsave.GetPath(), data);
  }
}

/*! \brief Update color data with any values specified in the config file
 *  \return
 */
void wxhdisvis::SetGroupColors()
{
	// we loop through all of the groups we need to have a color for,
	// and check to see if we have a color for it in the vector of colors
	// stored in Vars. if there isn't a color waiting for us, we create
	// a new random one.
  for (unsigned int a = 0; a < data->nAGs; ++a)
  {
    if (a >= v->group_atom_colors.size())
      v->group_atom_colors.push_back(Color());
    data->group_atom_colors[a] = v->group_atom_colors[a];
  }

  for (unsigned int s = 0; s < data->nSGs; ++s)
  {
    if (s >= v->group_spring_colors.size())
      v->group_spring_colors.push_back(Color());
    data->group_spring_colors[s] = v->group_spring_colors[s];
  }

  for (unsigned int t = 0; t < data->nTGs; ++t)
  {
    if (t >= v->group_ptriangle_colors.size())
      v->group_ptriangle_colors.push_back(Color());
    data->group_ptriangle_colors[t] = v->group_ptriangle_colors[t];
  }
  
  carFrame->InitializePropWidgets();
  carFrame->UpdatePropWidgets(ATOM, 0);
  model->Render();
}

/*! \brief Reinitialize all variables to the values specified in the source code
 * \return
 */
void wxhdisvis::ResetVars()
{
  v->SetToDefault();
  UpdateConfigWindow();
}

/*! \brief Adjust all controls in the config window to reflect current variable settings
 * \return
 */
void wxhdisvis::UpdateConfigWindow()
{
  SetChoice(configFrame, ID_RENDERMODE, v->rendermode);
  SetTextCtrl(configFrame, ID_RENDER_SWITCH,     _F(_("%d"), v->render_switch));
  SetTextCtrl(configFrame, ID_REDRAW_THRESHOLD,  _F(_("%.4f"), v->redraw_threshold));
  SetTextCtrl(configFrame, ID_SPHERE_ADJUSTMENT, _F(_("%.4f"), v->sphere_adjustment));
  SetTextCtrl(configFrame, ID_ZOOM_SPEED,        _F(_("%.4f"), v->zoom_speed));

  SetChoice(configFrame, ID_ARROWMODE, v->arrowmode);
  SetTextCtrl(configFrame, ID_TRANS_SPEED, _F(_("%.4f"), v->trans_speed));
  SetTextCtrl(configFrame, ID_ROT_SPEED,   _F(_("%.4f"), v->rot_speed));
  SetCheckBox(configFrame, ID_CLIP, v->clip);
  SetTextCtrl(configFrame, ID_CLIP_SPEED,  _F(_("%.4f"), v->clip_speed));

  SetChoice(configFrame, ID_DISPLAYMODE, v->displaymode);
  SetTextCtrl(configFrame, ID_FORCE_MAX,       _F(_("%.4f"), v->force_max));
  SetTextCtrl(configFrame, ID_FORCE_MIN,       _F(_("%.4f"), v->force_min));
  SetTextCtrl(configFrame, ID_SPEED_MAX,       _F(_("%.4f"), v->speed_max));
  SetTextCtrl(configFrame, ID_SPEED_BASE_SIZE, _F(_("%.4f"), v->speed_base_size));
  SetTextCtrl(configFrame, ID_SPEED_VEC_SIZE,  _F(_("%.4f"), v->speed_vec_size));
  SetTextCtrl(configFrame, ID_SPEED_MAX,       _F(_("%.4f"), v->speed_max));

  SetCheckBox(configFrame, ID_SPRINGS,      v->springs);
  SetCheckBox(configFrame, ID_PTRIANGLES,   v->ptriangles);
  SetTextCtrl(configFrame, ID_SPRING_WIDTH, _F(_("%d"), v->spring_width));

  OnModelResize(v->model_w, v->model_h);
  model->Render(); // this call is redundant if model has been resized
}

/****************************************************************
 *
 *              CONTROL FUNCTIONS AND EVENT HANDLING
 *
 ****************************************************************/


/*! \brief Reposition all of the windows
 * \return
 */
void wxhdisvis::ArrangeFrames()
{
	// pixel spacing between window
  int space = 4;

	// start with the color window in the top left
  carFrame->SetPosition(wxPoint(20,50));

	// then position the model window next to it
  modelFrame->SetPosition(wxPoint(carFrame->GetPosition().x + carFrame->GetSize().GetWidth() + space,
      carFrame->GetPosition().y));

	// the debug log goes below the model window
  logFrame->SetSize(modelFrame->GetSize().GetWidth(), 200);
  logFrame->SetPosition(wxPoint(modelFrame->GetPosition().x,
        modelFrame->GetPosition().y + modelFrame->GetSize().GetHeight() + space));

	// and the config window to the right of the model
  configFrame->SetPosition(wxPoint(modelFrame->GetPosition().x + modelFrame->GetSize().GetWidth() + space,
      modelFrame->GetPosition().y));

  model->SetFocus();
}

/*! \brief Play through all h5 files
 * \return
 */
void wxhdisvis::Play()
{
  while (v->playing && v->file != (int)v->filenames.size() - 1)
    LoadFile(v->file + 1);
}

/*! \brief Update all data from an HDF5 file
 *  \param n Index into the filenames array
 *
 * Loads the nth file in the files array stored with the data. If a negative n is specified,
 * it loads the last file and if an n that would be out the end of the array is specified,
 * it loads the first file.
 */
void wxhdisvis::LoadFile(int n)
{
  if (v->filenames.size() == 0 || data->initialized == false) return;

  //Wrap around
  if (n >= (int)v->filenames.size()) n = 0;
  if (n < 0) n = v->filenames.size() - 1;

  v->file = n;
  data->LoadFile(n);

	// readjust the files slider to reflect the new update
  modelFrame->SetTitle(v->short_filenames[n]);
  wxSlider *slider = (wxSlider *) modelFrame->FindWindow(ID_CONTROLS_FILESLIDER);
  slider->SetRange(0, data->files.GetCount() - 1);
  slider->SetValue(n);

  model->force_chain_current = false;
  model->Render();
}

/*! \brief Activate or deactivate springs display
 *  \param val Desired status
 */
void wxhdisvis::OnSpringsToggle(bool val)
{
  v->springs = val;
  SetCheckBox(configFrame, ID_SPRINGS, v->springs);
  
  model->Render();
}

/*! \brief Activate or deactivate pressure triangles display
 *  \param val Desired status
 */
void wxhdisvis::OnPtrianglesToggle(bool val)
{
  v->ptriangles = val;
  SetCheckBox(configFrame, ID_PTRIANGLES, v->ptriangles);
  
  model->Render();
}

/*! \brief Change the current display mode
 *  \param mode Desired mode
 */
void wxhdisvis::OnDisplaymode(int mode)
{
  v->displaymode = mode;
  SetChoice(configFrame, ID_DISPLAYMODE, mode);
  
  // we want to set the per-atom-group flag in this case
  if (data->initialized)
    {
      if (mode == DISPLAYMODE_NORMAL || mode == DISPLAYMODE_FORCE)
        {
          int index = carFrame->GetGroupIndex();
          data->atom_group_color_by_force[index] = (mode == DISPLAYMODE_FORCE);
        }
    }
  
  model->Render();
}

/*! \brief Change the current render mode
 *  \param mode Desired mode
 */
void wxhdisvis::OnRendermode(int mode)
{
	int old_mode = v->rendermode;
  v->rendermode = mode;

	// if we're switch either out of or in to poly render mode, we need
	// to reallocate all the VBOs and primitive data. in order to use
	// all the color saving, clean up, etc. stuff we already have in place,
	// we just go ahead and reload all the data. this is justifiable since
	// the VBOs and primitive data are really the only slow parts of
	// OpenNewData.
	if (old_mode == RENDERMODE_POLY && mode != RENDERMODE_POLY)
		OpenNewData(v->file);
	if (mode == RENDERMODE_POLY)
		OpenNewData(v->file);

  SetChoice(configFrame, ID_RENDERMODE, mode);
  ((wxTextCtrl*) configFrame->FindWindow(ID_RENDER_SWITCH))->Enable(mode == RENDERMODE_MIX);
  model->Render();
}

/*! \brief Change the current arrow mode
 *  \param mode Desired mode
 */
void wxhdisvis::OnArrowmode(int mode)
{
  // need to redraw if we are turning on or off clipping plane arrow mode,
	// since the knob is visibile
  bool redraw = v->clip && (v->arrowmode == ARROWMODE_CLIP || mode == ARROWMODE_CLIP);

  v->arrowmode = mode;
  SetChoice(configFrame, ID_ARROWMODE, mode);

  if (redraw) model->Render();
}

/*! \brief Activate or deactivate the clipping plane
 *  \param val Desired status
 */
void wxhdisvis::OnClipToggle(bool val)
{
  v->clip = val;
  SetCheckBox(configFrame, ID_CLIP, v->clip);
  model->Render();
}

/*! \brief Activate or deactivate the bounding box
 *  \param val Desired status
 */
void wxhdisvis::OnBoxToggle(bool val)
{
  v->box = val;
  SetCheckBox(configFrame, ID_BOX, v->box);
  model->Render();
}

/*! \brief Activate or deactivate the axis display
 *  \param val Desired status
 */
void wxhdisvis::OnAxisDisplayToggle(bool val)
{
  v->axis_display = val;
  SetCheckBox(configFrame, ID_AXIS_DISPLAY, v->axis_display);
  model->Render();
}

/*! \brief Take a screenshot
 * \return
 *
 * Opens a file dialog to select the save location, and lets the user select
 * either TGA or PNG as an output file type. Uses libpng for PNG screenshots,
 * but nothing external for TGA.
 */
void wxhdisvis::OnScreenshot()
{
  if (data->initialized)
  {
#ifdef HAVE_PNG
    wxFileDialog dialog(NULL, wxEmptyString, wxEmptyString,
        v->short_filenames[v->file].SubString(0, v->short_filenames[v->file].Len()-4),
        _("PNG (*.png)|*.png|Truevision (*.tga)|*.tga"), wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
#else //no PNG
    wxFileDialog dialog(NULL, wxEmptyString, wxEmptyString,
        v->short_filenames[v->file].SubString(0, v->short_filenames[v->file].Len()-4),
        _("Truevision (*.tga)|*.tga"), wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
#endif
    if (dialog.ShowModal() == wxID_OK)
    {
#ifdef HAVE_PNG
      if (dialog.GetFilterIndex() == 0)
      {
        ScreenshotPNG(dialog.GetPath(), model);
      }
      else
      {
        ScreenshotTGA(dialog.GetPath(), model);
      }
#else
      ScreenshotTGA(dialog.GetPath(), model);
#endif
    }
  }
}

/*! \brief Begin recording a video
 * \return
 *
 * Opens a dialog to select the output location and then gets all of the video
 * settings from a specialized dialog before creating and starting a new video.
 * If a video is already active, that video is closed instead.
 */
void wxhdisvis::OnVideo()
{
  if (data->initialized)
  {
    if (video == NULL)
    {
			// open a file save dialog to save as an mpeg
      wxFileDialog file(NULL, wxEmptyString, wxEmptyString,
          v->short_filenames[v->file].SubString(0, v->short_filenames[v->file].Len()-4),
          _("MPEG-2 (*.mpeg)|*.mpeg"), wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
      if (file.ShowModal() == wxID_OK)
      {
				// if they "ok" the save, open the custom video dialog to get their video settings
        VideoDialog dlg(wxPoint(600, 300));
        wxLogMessage(_("size: %d %d"), dlg.GetSize().GetWidth(), dlg.GetSize().GetHeight());
        wxPoint position(modelFrame->GetPosition().x - dlg.GetSize().GetWidth() - 4,
              modelFrame->GetPosition().y + modelFrame->GetSize().GetHeight() - dlg.GetSize().GetHeight());
        dlg.SetPosition(position);

				// after they've set everything they want in the video dialog and hit "ok",
				// read their settings and create a new video with them
        if (dlg.ShowModal() == wxID_OK)
        {
          wxString fname = file.GetPath();
          wxLogMessage(_("size: %d %d"), dlg.GetSize().GetWidth(), dlg.GetSize().GetHeight());
          double delay;
          dlg.delay->GetValue().ToDouble(&(delay));
          int w, h;
          model->GetClientSize(&w, &h);
          bool load = dlg.auto_load->GetValue();
          bool capture = dlg.auto_capture->GetValue();
					
					// user sets it in mbps, avcodec uses bps
					unsigned int bps;
					bps = (unsigned int) wxAtoi(dlg.mbps->GetValue())*1000;

          video = new Video(fname, w, h, capture, load, delay, bps);
          videoFrame = new HdisvisFrame(FRAME_VIDEO, _("Video"), data, v, this);
          videoFrame->SetSize(dlg.GetSize());
          videoFrame->SetPosition(position);
          videoFrame->Show();
        }
      }
    }
    else
    {
      OnVideoClose();
    }
  }
  else
  {
    wxMessageBox(_("Cannot create video without model data"));
  }
}

/*! \brief Stop recording and finalize a video
 * \return
 */
void wxhdisvis::OnVideoClose()
{
  videoFrame->timer->Stop();
  delete videoFrame->timer;
  video->close();
  video = NULL;
  videoFrame->Destroy();
  videoFrame = NULL;
}

/*! \brief Add the current model render as a frame to the video
 *
 * Adds a screenshot as the next frame in the active video. If auto loading the next
 * file is enabled, the next file is loaded from here. If the current file is the last
 * file, the first file is loaded and the automatic capture is paused if applicable.
 */
void wxhdisvis::CaptureFrame()
{
  if (video)
  {
    int w, h;
    model->GetClientSize(&w, &h);
    if (w != video->w|| h != video->h)
    {
      wxMessageBox("Model has been resized; cannot continue");
      OnVideoClose();
      return;
    }
    /* capture current model render */
    video->encode_frame(model);

    ((wxStaticText *) videoFrame->FindWindow(ID_VIDEO_FRAMES))->SetLabel(wxString::Format(_("Frames: %d"), video->nframes));
    /* load new h5 file */
    if (video->auto_advance)
    {
      if (v->file + 1 < (int)v->filenames.GetCount())
      {
        LoadFile(v->file + 1);
      }
      /* wrap back to the start and pause recording if the end is reached */
      else
      {
        LoadFile(0);
        if (video->auto_record)
        {
            videoFrame->timer->Stop();
            ((wxButton *) videoFrame->FindWindow(ID_VIDEO_PLAY_PAUSE))->SetLabel(_("Start"));
        }
      }
    }
    videoFrame->Layout();
    modelFrame->SetFocus();
    model->SetFocus();
  }
}

/*! \brief Adjust the zoom distance
 *  \param direction Zoom direction (IN/OUT)
 *
 * Translates the camera position by a fixed distance. If the camera position
 * would become negative, it is instead set to 0 to prevent the camera from coming
 * out the back side of the model and inverting all controls.
 */
void wxhdisvis::OnZoom(int direction)
{
  if (direction == IN)
    v->zoom -= v->zoom_speed;
  if (direction == OUT)
    v->zoom += v->zoom_speed;
  // prevent negative zoom
  v->zoom = max(v->zoom, 0.0);
  model->Render();
}

/*! \brief Align the model to the nearest axis
 * \return
 */
void wxhdisvis::OnAlign()
{
  align_vec(&(v->eye));
  align_vec(&(v->up));
  vec3d_x(v->over, v->up, v->eye);
  model->Render();
}

/*! \brief Reset all model rotations
 * \return
 */
void wxhdisvis::OnResetrot()
{
  vec3d_set(v->eye, 1, 0, 0);
  vec3d_set(v->up, 0, 0, 1);
  vec3d_set(v->over, 0, 1, 0);
  model->Render();
}

/*! \brief Reset all model translations
 * \return
 */
void wxhdisvis::OnResettrans()
{
  vec3d_set(v->trans, 0, 0, 0);
  model->Render();
}

/*! \brief Exit the application
 * \return
 *
 * All windows are destroyed. wxWidgets automatically closes an application once it no longer has any open windows.
 */
void wxhdisvis::OnQuit()
{
    if (video != NULL) OnVideoClose();
    if (modelFrame != NULL) modelFrame->Destroy();
    if (configFrame != NULL) configFrame->Destroy();
    if (carFrame != NULL) carFrame->Destroy();
    if (logFrame!= NULL) logFrame->Destroy();
    // data->Clear();
    // soooo, on Paul's machine something different happens.
    // uninitalized pointers are not NULL, or free is more
    // picky or something. letting wxWidgets handle its own
    // cleanup and closing would crash his computer, so we
    // short circuit all that and exit the program. this
    // probably leaks some memory. womp.
    exit(0);
}

void wxhdisvis::SetForceMax(double new_val)
{
  v->force_max = new_val;
  SetTextCtrl(configFrame, ID_FORCE_MAX, wxString::Format(_("%.4f"), v->force_max));
  model->Render();
}
void wxhdisvis::SetForceMin(double new_val)
{
  v->force_min = new_val;
  SetTextCtrl(configFrame, ID_FORCE_MIN, wxString::Format(_("%.4f"), v->force_min));
  model->force_chain_current = false;
  model->Render();
}
void wxhdisvis::SetSpeedMax(double new_val)
{
  v->speed_max = new_val;
  SetTextCtrl(configFrame, ID_SPEED_MAX, wxString::Format(_("%.4f"), v->speed_max));
  model->Render();
}
void wxhdisvis::SetSpeedVecSize(double new_val)
{
  v->speed_vec_size = new_val;
  SetTextCtrl(configFrame, ID_SPEED_VEC_SIZE, wxString::Format(_("%.4f"), v->speed_vec_size));
  model->Render();
}
void wxhdisvis::SetSpeedBaseSize(double new_val)
{
  v->speed_base_size = new_val;
  SetTextCtrl(configFrame, ID_SPEED_BASE_SIZE, wxString::Format(_("%.4f"), v->speed_base_size));
  model->Render();
}
void wxhdisvis::SetSpeedWidth(int new_val)
{
  v->speed_width = new_val;
  SetTextCtrl(configFrame, ID_SPEED_WIDTH, wxString::Format(_("%d"), v->speed_width));
  model->Render();
}

/*! \brief Resize the model window
 *  \param w Pixel width
 *  \param h Pixel height
 */
void wxhdisvis::OnModelResize(int w, int h)
{
  // all info updated in modelFrame's SizeEventHandler
  // account for the height of the controls on the bottom
  modelFrame->SetClientSize(w, h + 81);
}
