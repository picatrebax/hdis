#ifndef UTILITY_H
#define UTILITY_H

#include <wx/wx.h>
#include <wx/string.h>
#include <wx/log.h>

#include "vec3d.h"

#include <ctime>
using namespace std;

/*! \brief Utility functions
 *
 * A set of light weight, simple functions that don't inherently interact with hdisvis.
 */
namespace hdisvis_utils
{

void LogTime(wxString message, clock_t start, clock_t end);

/** @name Vector manipulation
 */
//@{
void align_vec(vec3d_t* vec);
void rotate(vec3d_t *in, vec3d_t axis, double t);
void rotate_ortho(vec3d_t *vec, vec3d_t *targ, vec3d_t *other, double speed);
//@}

/** @name vec3d helper functions
 *
 * \brief vec3d calls that return the resulting vec3d_t
 *
 * The vec3d_t implementation in hdis/src/vec3d.h never returns a vec3d_t, so these functions
 * allow for shorter expressions of the form\nvec3d_t vec = ...
 */
//@{
vec3d_t get_vec(float x, float y, float z);
vec3d_t get_vec_x(vec3d_t a, vec3d_t b);
vec3d_t get_vec_sum(vec3d_t a, vec3d_t b);
vec3d_t get_vec_diff(vec3d_t a, vec3d_t b);
vec3d_t get_vec_scale(vec3d_t a, float k);
vec3d_t get_vec_normalize(vec3d_t a);
//@}

/** @name Widget modification functions
 *
 * \param parent The HdisvisFrame containing the target widget
 * \param id The unique id of the target widget as defined in Vars.hh
 *
 * Getting pointers to widgets is done with the FindWindow function, so it is convenient to have
 * a set of functions that casts the Window* return values to the appropriate widget type, and
 * modifies the value of that widget.
 */
//@{
void SetChoice(wxFrame *parent, int id, int value);
void SetRadioSelection(wxFrame *parent, int id, int sel);
void SetCheckBox(wxFrame *parent, int id, bool val);
void SetTextCtrl(wxFrame *parent, int id, wxString value);
//@}

//! constexpr deduction of array length
template<class C, size_t N>
size_t len(C (&)[N]) { return N; }

}

#endif
