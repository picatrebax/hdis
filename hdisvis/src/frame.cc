#include "frame.hh"

HdisvisFrame::~HdisvisFrame()
{
    return;
}

/*! \brief HdisvisFrame constructor
 * \param ID Unique frame id
 * \param name Window title
 * \param pdata Pointer to HDF5 data
 * \param pv Pointer to application variables
 * \param app Pointer to application instance
 */
HdisvisFrame::HdisvisFrame(int ID, wxString name, Data *pdata, Vars *pv, wxhdisvis *app, int style)
      : wxFrame(NULL, wxID_ANY, name, wxDefaultPosition, wxSize(50,50), style)
{
  hdisvis = app;
  data = pdata;
  v = pv;
  switch(ID)
  {
    case FRAME_MODEL: ModelFrame(); break;
    case FRAME_VIDEO: VideoFrame(); break;
    case FRAME_CONFIG: ConfigFrame(); break;
    case FRAME_CAR: CarFrame(); break;
    case FRAME_LOG: LogFrame(); break;
    default: wxMessageBox(_("unrecognized HdisvisFrame initializer"));
  }
  wxMenuBar* bar = new wxMenuBar();
  AddAllMenus(bar);
  SetMenuBar(bar);
}

/* Functions to override default close behavior with!
 *   for use when the user clicks the "x" and we don't
 *   really want to destory the window
 */

// just hide the window instead of destroying it
// (the user will never know!)
void HdisvisFrame::Hide(wxCloseEvent &event)
{
  if (event.CanVeto())
  {
    Show(false);
    event.Veto();
  }
  else // when the app closes, CanVeto is false
  {
    Destroy();
  }
}

// ignore commands to close the window
void HdisvisFrame::KeepOpen(wxCloseEvent &event)
{
  if (event.CanVeto())
  {
    event.Veto();
  }
  else // when the app closes, CanVeto is false
  {
    Destroy();
  }
}

// close the model window and clear the loaded data
void HdisvisFrame::CloseModel(wxCloseEvent &event)
{
  delete hdisvis->model;
  hdisvis->model = NULL;
  delete hdisvis->data;
  hdisvis->data = NULL;
  Destroy();
}

// the video window has a special close routine that handles
// finalizing the mpeg and stuff
void HdisvisFrame::CloseVideo(wxCloseEvent &event)
{
  hdisvis->OnVideoClose();
}

/****************************************************************
 *
 *              MENUS
 *
 ****************************************************************/
void HdisvisFrame::AddAllMenus(wxMenuBar* bar)
{
  AddFileMenu(bar);
  AddWindowsMenu(bar);
  AddModelMenu(bar);
  AddControlsMenu(bar);
  AddHelpMenu(bar);
}

void HdisvisFrame::AddFileMenu(wxMenuBar* bar)
{
	// file menu
  wxMenu *file = new wxMenu();
  file->Append(wxID_ABOUT, _("About COUPiD"));
  file->AppendSeparator();
  file->Append(ID_MENU_FILE_OPENH5, _("&Open HDF5\tCtrl+O"));
  file->Append(ID_MENU_FILE_RELOADH5, _("Reload HDF5"));
  file->Append(ID_MENU_MODEL_NEXT, _("Next File"));
  file->Append(ID_MENU_MODEL_PREV, _("Previous File"));
  file->Append(ID_MENU_MODEL_PLAY, _("Play all files"));
  file->AppendSeparator();
  file->Append(ID_MENU_FILE_OPENLUA, _("&Load config\tCtrl+L"));
  file->Append(ID_MENU_FILE_SAVELUA, _("&Save config\tCtrl+S"));
  file->Append(ID_MENU_FILE_RESETVARS, _("Reset to default"));
  file->AppendSeparator();
  file->Append(ID_MENU_FILE_SCREENSHOT, _("Take screenshot\tCtrl+T"));
#ifdef HAVE_VIDEO
  file->Append(ID_MENU_FILE_VIDEO, _("Record video\tCtrl+R"));
#endif
  file->AppendSeparator();
  file->Append(wxID_EXIT, _("&Quit"));

  bar->Append(file, _("&File"));

  //File Menu Connections
  this->Connect(ID_MENU_FILE_OPENH5, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuFileOpenH5));
  this->Connect(ID_MENU_FILE_RELOADH5, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuFileReloadH5));
  this->Connect(ID_MENU_FILE_OPENLUA, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuFileOpenLua));
  this->Connect(ID_MENU_FILE_SAVELUA, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuFileSaveLua));
  this->Connect(ID_MENU_FILE_RESETVARS, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuFileResetVars));
  this->Connect(wxID_ABOUT, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuFileAbout));
  this->Connect(ID_MENU_FILE_SCREENSHOT, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuFileScreenshot));
  this->Connect(ID_MENU_FILE_VIDEO, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuFileVideo));
  this->Connect(wxID_EXIT, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuFileQuit));
  this->Connect(ID_MENU_MODEL_PLAY, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuFilePlay));
}

void HdisvisFrame::AddWindowsMenu(wxMenuBar* bar)
{
  wxMenu *windows = new wxMenu();
  //windows->Append(ID_MENU_WINDOWS_CONTROLS, _("Main controls"));
  windows->Append(ID_MENU_WINDOWS_MODEL, _("Model"));
  //windows->Append(ID_MENU_WINDOWS_QUALITY, _("HdisvisAtom size and quality"));
  windows->Append(ID_MENU_WINDOWS_CAR, _("Color Properties"));
  //windows->Append(ID_MENU_WINDOWS_DISPLAY, _("Display mode controls"));
  windows->Append(ID_MENU_WINDOWS_CONFIG, _("Settings"));
  windows->Append(ID_MENU_WINDOWS_LOG, _("Debug Log"));

  bar->Append(windows, _("&Windows"));

  //Interface Menu this->Connections
  this->Connect(ID_MENU_WINDOWS_CONTROLS, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuWindows));
  this->Connect(ID_MENU_WINDOWS_MODEL, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuWindows));
  this->Connect(ID_MENU_WINDOWS_QUALITY, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuWindows));
  this->Connect(ID_MENU_WINDOWS_CAR, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuWindows));
  this->Connect(ID_MENU_WINDOWS_DISPLAY, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuWindows));
  this->Connect(ID_MENU_WINDOWS_CONFIG, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuWindows));
  this->Connect(ID_MENU_WINDOWS_LOG, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuWindows));
}

void HdisvisFrame::AddModelMenu(wxMenuBar* bar)
{
  //Model_Display Submenu
  wxMenu *model_display = new wxMenu();
  model_display->Append(ID_MENU_MODEL_DISPLAY_NORMAL, _("Normal"));
  model_display->Append(ID_MENU_MODEL_DISPLAY_SPEED, _("Speed"));
  model_display->Append(ID_MENU_MODEL_DISPLAY_FORCE, _("Force"));

  //Model_Display Submenu this->Connections
  this->Connect(ID_MENU_MODEL_DISPLAY_NORMAL, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuModelDisplayNormal));
  this->Connect(ID_MENU_MODEL_DISPLAY_SPEED, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuModelDisplaySpeed));
  this->Connect(ID_MENU_MODEL_DISPLAY_FORCE, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuModelDisplayForce));
  
  //Model Menu
  wxMenu *model = new wxMenu();
  model->AppendSubMenu(model_display, _("Display Mode"));
  model->AppendSeparator();
  model->Append(ID_MENU_MODEL_CLIPTOGGLE, _("Toggle Clipping Plane\tCtrl+C"));
  model->Append(ID_MENU_MODEL_BOX, _("Toggle bounding box"));
  model->Append(ID_MENU_MODEL_AXIS, _("Toggle axis display"));
  model->Append(ID_MENU_MODEL_TIME, _("Toggle time display"));
  model->AppendSeparator();

  model->Append(ID_MENU_MODEL_INFO, _("Model info"));

  bar->Append(model, _("&Model"));

  //Model Menu this->Connections
  this->Connect(ID_MENU_MODEL_NEXT, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuModelNext));
  this->Connect(ID_MENU_MODEL_PREV, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuModelPrev));
  this->Connect(ID_MENU_MODEL_CLIPTOGGLE, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuModelClipToggle));

  this->Connect(ID_MENU_MODEL_BOX, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuModelBox));
  this->Connect(ID_MENU_MODEL_AXIS, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuModelAxis));
  this->Connect(ID_MENU_MODEL_TIME, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuModelTime));

  this->Connect(ID_MENU_MODEL_INFO, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuModelInfo));
}

void HdisvisFrame::AddControlsMenu(wxMenuBar* bar)
{
  //Controls_Arrows Submenu
  wxMenu *controls_arrows = new wxMenu();

  controls_arrows->Append(ID_MENU_CONTROLS_ARROWS_ROTATE,
      _("Rotate Model"));
  controls_arrows->Append(ID_MENU_CONTROLS_ARROWS_TRANSLATE,
      _("Translate Model"));
  controls_arrows->Append(ID_MENU_CONTROLS_ARROWS_CLIP,
      _("Rotate Clipping Plane"));

  //Controls_Arrows Submenu Connections
  this->Connect(ID_MENU_CONTROLS_ARROWS_ROTATE, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuControlsArrowsRotate));
  this->Connect(ID_MENU_CONTROLS_ARROWS_TRANSLATE, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuControlsArrowsTranslate));
  this->Connect(ID_MENU_CONTROLS_ARROWS_CLIP, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuControlsArrowsClip));

  //Controls Menu
  wxMenu *controls = new wxMenu();

  controls->Append(ID_MENU_CONTROLS_ZOOMIN, _("Zoom In\t+"));
  controls->Append(ID_MENU_CONTROLS_ZOOMOUT, _("Zoom Out\t-"));
  controls->AppendSeparator();
  controls->AppendSubMenu(controls_arrows, _("Arrows Key Mode"));
  controls->AppendSeparator();
  controls->Append(ID_MENU_CONTROLS_ALIGN, _("Align Model\tz"));
  controls->Append(ID_MENU_CONTROLS_RESETROT, _("Reset Rotations"));
  controls->Append(ID_MENU_CONTROLS_RESETTRANS, _("Reset Translations\tx"));

  bar->Append(controls, _("&Controls"));

  //Controls Connections
  this->Connect(ID_MENU_CONTROLS_ZOOMIN, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuControlsZoomin));
  this->Connect(ID_MENU_CONTROLS_ZOOMOUT, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuControlsZoomout));
  this->Connect(ID_MENU_CONTROLS_ALIGN, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuControlsAlign));
  this->Connect(ID_MENU_CONTROLS_RESETROT, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuControlsResetrot));
  this->Connect(ID_MENU_CONTROLS_RESETTRANS, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuControlsResettrans));
}

void HdisvisFrame::AddHelpMenu(wxMenuBar* bar)
{
  wxMenu *help = new wxMenu();
  help->Append(ID_MENU_HELP, _("View manual"));

  bar->Append(help, _("Help"));

  this->Connect(ID_MENU_HELP, wxEVT_COMMAND_MENU_SELECTED,
      wxCommandEventHandler(HdisvisFrame::OnMenuHelp));
}

/*** #### MENU EVENT HANDLERS #### ***/
/* for most event handlers that have to do with menus/controls,
 * we only pass responsibility for handling the events up the chain
 * to the hdisvis instance. this reduces code duplication, and ensures
 * consistency with how events are handled (so the menu event for a
 * setting does the same thing as the control panel event, for example).
 */

void HdisvisFrame::OnMenuFileOpenH5(wxCommandEvent &event)
{ hdisvis->GetFilenames(); }

void HdisvisFrame::OnMenuFileReloadH5(wxCommandEvent &event)
{ hdisvis->ReloadData(); }

void HdisvisFrame::OnMenuFileOpenLua(wxCommandEvent &event)
{ hdisvis->OpenConfig(); }

void HdisvisFrame::OnMenuFileSaveLua(wxCommandEvent &event)
{ hdisvis->SaveConfig(); }

void HdisvisFrame::OnMenuFileResetVars(wxCommandEvent &event)
{ hdisvis->ResetVars(); }

void HdisvisFrame::OnMenuFileScreenshot(wxCommandEvent &event)
{ hdisvis->OnScreenshot(); }

void HdisvisFrame::OnMenuFileVideo(wxCommandEvent &event)
{ hdisvis->OnVideo(); }

void HdisvisFrame::OnMenuFileAbout(wxCommandEvent &event)
{ wxMessageBox(ABOUT_STRING, VERSION_STRING); }

void HdisvisFrame::OnMenuFileQuit(wxCommandEvent &event)
{ hdisvis->OnQuit(); }

void HdisvisFrame::OnMenuFilePlay(wxCommandEvent &event)
{ v->playing = true; hdisvis->Play(); }

void HdisvisFrame::OnMenuWindows(wxCommandEvent &event)
{
  HdisvisFrame *frame = NULL;
  switch (event.GetId())
  {
    case ID_MENU_WINDOWS_MODEL:  frame = hdisvis->modelFrame;  break;
    case ID_MENU_WINDOWS_CONFIG: frame = hdisvis->configFrame; break;
    case ID_MENU_WINDOWS_CAR:    frame = hdisvis->carFrame;    break;
    case ID_MENU_WINDOWS_LOG:    frame = hdisvis->logFrame;    break;
  }
  if (frame) frame->Show();
}
void HdisvisFrame::OnMenuModelDisplayNormal(wxCommandEvent &event)
{
	hdisvis->OnDisplaymode(DISPLAYMODE_NORMAL);
}

void HdisvisFrame::OnMenuModelDisplaySpeed(wxCommandEvent &event)
{
  if (v->displaymode != DISPLAYMODE_SPEED)
    hdisvis->OnDisplaymode(DISPLAYMODE_SPEED);
  else
    hdisvis->OnDisplaymode(DISPLAYMODE_NORMAL);
}

void HdisvisFrame::OnMenuModelDisplayForce(wxCommandEvent &event)
{
  if (v->displaymode != DISPLAYMODE_FORCE)
    hdisvis->OnDisplaymode(DISPLAYMODE_FORCE);
  else
    hdisvis->OnDisplaymode(DISPLAYMODE_NORMAL);
}

void HdisvisFrame::OnMenuModelNext(wxCommandEvent &event)
{ hdisvis->LoadFile(v->file + 1); }

void HdisvisFrame::OnMenuModelPrev(wxCommandEvent &event)
{ hdisvis->LoadFile(v->file - 1); }

void HdisvisFrame::OnMenuModelClipToggle(wxCommandEvent &event)
{ hdisvis->OnClipToggle(!v->clip); }

void HdisvisFrame::OnMenuModelBox(wxCommandEvent &event)
{ v->box = !v->box; hdisvis->model->Render(); }

void HdisvisFrame::OnMenuModelAxis(wxCommandEvent &event)
{ v->axis_display = !v->axis_display; hdisvis->model->Render(); }

void HdisvisFrame::OnMenuModelTime(wxCommandEvent &event)
{ v->time_display = !v->time_display; hdisvis->model->Render(); }

void HdisvisFrame::OnMenuControlsZoomin(wxCommandEvent &event)
{ hdisvis->OnZoom(IN); }

void HdisvisFrame::OnMenuControlsZoomout(wxCommandEvent &event)
{ hdisvis->OnZoom(OUT); }

void HdisvisFrame::OnMenuControlsArrowsRotate(wxCommandEvent &event)
{ hdisvis->OnArrowmode(ARROWMODE_ROTATE); }

void HdisvisFrame::OnMenuControlsArrowsTranslate(wxCommandEvent &event)
{ hdisvis->OnArrowmode(ARROWMODE_TRANSLATE); }

void HdisvisFrame::OnMenuControlsArrowsClip(wxCommandEvent &event)
{ hdisvis->OnArrowmode(ARROWMODE_CLIP); }

void HdisvisFrame::OnMenuControlsAlign(wxCommandEvent &event)
{ hdisvis->OnAlign(); }

void HdisvisFrame::OnMenuControlsResetrot(wxCommandEvent &event)
{ hdisvis->OnResetrot(); }

void HdisvisFrame::OnMenuControlsResettrans(wxCommandEvent &event)
{ hdisvis->OnResettrans(); }

void HdisvisFrame::OnMenuModelInfo(wxCommandEvent &event)
{
  wxMessageBox(wxString::Format(_("Prims: \t%u\nBodies: \t%u\nGroups: \t%u\nSprings: \t%u\nPTris: \t%u\nFiles: \t%zu\n"),
	data->nPs, data->nBs, data->nAGs, data->nSs, data->nTs, data->files.GetCount()),
      _("Model Info"), wxOK | wxCENTER | wxICON_NONE);
}

void HdisvisFrame::OnMenuHelp(wxCommandEvent &event)
{ hdisvis->OpenManual(); }

void HdisvisFrame::OnControlsDisplaymode(wxCommandEvent &event)
{ hdisvis->OnDisplaymode(event.GetInt()); }

void HdisvisFrame::OnControlsRendermode(wxCommandEvent &event)
{ hdisvis->OnRendermode(event.GetInt()); }

void HdisvisFrame::OnControlsArrowmode(wxCommandEvent &event)
{ hdisvis->OnArrowmode(event.GetInt()); }


/****************************************************************
 *
 *              LOG WINDOW
 *
 ****************************************************************/
bool HdisvisFrame::LogFrame()
{
  wxTextCtrl* logText = new wxTextCtrl(this, ID_LOG_TEXT, wxEmptyString,
			wxDefaultPosition, wxSize(500,150),
      wxTE_MULTILINE | wxTE_LEFT | wxTE_READONLY);
  wxBoxSizer* logSizer = new wxBoxSizer(wxHORIZONTAL);
  logSizer->Add(logText, 1, wxEXPAND | wxALL, 5);
  this->SetSizer(logSizer);
  logSizer->SetSizeHints(this);

  this->Connect(wxEVT_CLOSE_WINDOW, wxCloseEventHandler(HdisvisFrame::Hide));
  return true;
}

/****************************************************************
 *
 *              MODEL WINDOW
 *
 ****************************************************************/

bool HdisvisFrame::ModelFrame()
{
  //Create Model Rendering Area
  Model *model = new Model(this, v, data, ID_MODEL, wxDefaultPosition,
      wxDefaultSize, wxSUNKEN_BORDER);
  model->hdisvis = hdisvis;
  
  wxBoxSizer* button_sizer = new wxBoxSizer(wxHORIZONTAL);
  wxSize button_size(75, 31);
  int button_border = 3;

  button_sizer->Add(new wxButton(this, ID_CONTROLS_ZOOMOUT, _("-"),
				wxDefaultPosition, button_size, wxBORDER_SIMPLE),
      1, wxEXPAND | wxALL, button_border);
  button_sizer->Add(new wxButton(this, ID_CONTROLS_ZOOMIN, _("+"),
				wxDefaultPosition, button_size, wxBORDER_SIMPLE),
      1, wxEXPAND | wxALL, button_border);

  this->Connect(ID_CONTROLS_ZOOMOUT, wxEVT_COMMAND_BUTTON_CLICKED,
      wxCommandEventHandler(HdisvisFrame::OnControlsZoomout));
  this->Connect(ID_CONTROLS_ZOOMIN, wxEVT_COMMAND_BUTTON_CLICKED,
      wxCommandEventHandler(HdisvisFrame::OnControlsZoomin));

  button_sizer->Add(new wxButton(this, ID_CONTROLS_ALIGN, _("Align"),
				wxDefaultPosition, button_size, wxBORDER_SIMPLE),
      1, wxEXPAND | wxALL, button_border);
  button_sizer->Add(new wxButton(this, ID_CONTROLS_RESETTRANS, _("Center"),
				wxDefaultPosition, button_size, wxBORDER_SIMPLE),
      1, wxEXPAND | wxALL, button_border);

  this->Connect(ID_CONTROLS_ALIGN, wxEVT_COMMAND_BUTTON_CLICKED,
      wxCommandEventHandler(HdisvisFrame::OnControlsAlign));
  this->Connect(ID_CONTROLS_RESETTRANS, wxEVT_COMMAND_BUTTON_CLICKED,
      wxCommandEventHandler(HdisvisFrame::OnControlsResettrans));

  //File Control
  wxSlider* file_slider = new wxSlider(this, ID_CONTROLS_FILESLIDER, 0, 0, 100,
      wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL | wxSL_LABELS);

#ifdef __APPLE__
  this->Connect(ID_CONTROLS_FILESLIDER, wxEVT_SCROLL_THUMBRELEASE,
			wxCommandEventHandler(HdisvisFrame::OnControlsFileslider));
#else
  this->Connect(ID_CONTROLS_FILESLIDER, wxEVT_SCROLL_CHANGED,
			wxCommandEventHandler(HdisvisFrame::OnControlsFileslider));
#endif

  wxFlexGridSizer* control_sizer = new wxFlexGridSizer(2, 1, 3, 3);
  control_sizer->Add(file_slider, 1, wxEXPAND, 0);
  control_sizer->Add(button_sizer, 1, wxEXPAND, 0);

  wxBoxSizer* window_sizer = new wxBoxSizer(wxVERTICAL);
  window_sizer->Add(model, 1, wxEXPAND, 0);
  window_sizer->Add(control_sizer, 0, wxCENTER, 0);
  
  window_sizer->SetSizeHints(this);
  this->SetSizer(window_sizer);

  this->Connect(wxEVT_CLOSE_WINDOW, wxCloseEventHandler(HdisvisFrame::Hide));
  this->Connect(wxEVT_SIZE, wxSizeEventHandler(HdisvisFrame::OnModelFrameResized));

  return true;
}

void HdisvisFrame::OnControlsAlign(wxCommandEvent &event)
{ hdisvis->OnAlign(); hdisvis->model->SetFocus(); }

void HdisvisFrame::OnControlsResettrans(wxCommandEvent &event)
{ hdisvis->OnResettrans(); hdisvis->model->SetFocus(); }

void HdisvisFrame::OnControlsZoomin(wxCommandEvent &event)
{ hdisvis->OnZoom(IN); hdisvis->model->SetFocus(); }

void HdisvisFrame::OnControlsZoomout(wxCommandEvent &event)
{ hdisvis->OnZoom(OUT); hdisvis->model->SetFocus(); }

void HdisvisFrame::OnControlsFileslider(wxCommandEvent &event)
{ hdisvis->LoadFile(event.GetInt()); hdisvis->model->SetFocus(); }

/*! \brief Event handler for when the model is resized
 * \param event Caught event
 *
 * Updates the config window model width/height fields. This deviates from the
 * standard behavior of event handlers in individual frames that need to modify
 * other frames relying on a more general event handler in wxhdisvis. An event
 * handler in wxhdisvis that resized the window would trigger this event handler,
 * so to prevent an infinite loop the config frame updates are done here.
 */
void HdisvisFrame::OnModelFrameResized(wxSizeEvent &event)
{
  Layout();
  v->model_w = hdisvis->model->GetSize().GetWidth();
  v->model_h = hdisvis->model->GetSize().GetHeight();
  SetTextCtrl(hdisvis->configFrame, ID_MODEL_W, wxString::Format(_("%d"), v->model_w));
  SetTextCtrl(hdisvis->configFrame, ID_MODEL_H, wxString::Format(_("%d"), v->model_h));
}

/****************************************************************
 *
 *              VIDEO WINDOW
 *
 ****************************************************************/

bool HdisvisFrame::VideoFrame()
{
  if (this->hdisvis->video != NULL)
  {
    timer = new wxTimer(this);
    Connect(wxEVT_TIMER, wxCommandEventHandler(HdisvisFrame::OnVideoTimerTic));

    wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

    wxBoxSizer *button_sizer = new wxBoxSizer(wxHORIZONTAL);

    button_sizer->Add(new wxButton(this, ID_VIDEO_FINALIZE, _("Finalize"),
					wxDefaultPosition, wxDefaultSize, wxBORDER_SIMPLE),
        1, wxEXPAND | wxRIGHT, 2);
    this->Connect(ID_VIDEO_FINALIZE, wxEVT_COMMAND_BUTTON_CLICKED,
				wxCommandEventHandler(HdisvisFrame::OnVideoClose));

		// if it auto records we need a start/stop button
    if (this->hdisvis->video->auto_record)
    {
      button_sizer->Add(new wxButton(this, ID_VIDEO_PLAY_PAUSE, _("Start"),
						wxDefaultPosition, wxSize(50,30), wxBORDER_SIMPLE),
          1, wxEXPAND | wxLEFT, 2);
      this->Connect(ID_VIDEO_PLAY_PAUSE, wxEVT_COMMAND_BUTTON_CLICKED,
					wxCommandEventHandler(HdisvisFrame::OnVideoPause));
    }
		// if it's full manual wee need a capture frame button
    else
    {
      button_sizer->Add(new wxButton(this, ID_VIDEO_CAP, _("Capture"),
						wxDefaultPosition, wxSize(50,30), wxBORDER_SIMPLE),
          1, wxEXPAND | wxRIGHT, 2);
      this->Connect(ID_VIDEO_CAP, wxEVT_COMMAND_BUTTON_CLICKED,
					wxCommandEventHandler(HdisvisFrame::OnVideoCap));
    }

    wxStaticBoxSizer *info = new wxStaticBoxSizer(wxHORIZONTAL, this);
    info->Add(new wxStaticText(this, ID_VIDEO_FRAMES, wxString::Format(_("Frames: %d"),
            hdisvis->video->nframes)),
          1, wxALIGN_CENTER | wxEXPAND, 0);
    this->Connect(wxEVT_CLOSE_WINDOW, wxCloseEventHandler(HdisvisFrame::CloseVideo));
    if (this->hdisvis->video->auto_record)
    {
      info->Add(new wxStaticText(this, ID_VIDEO_DELAY, wxString::Format(_("Next cap: %.1f"),
              hdisvis->video->next_cap)),
          1, wxALIGN_CENTER | wxEXPAND, 0);
    }

    sizer->Add(button_sizer, 1, wxEXPAND | wxLEFT | wxRIGHT | wxTOP, 5);
    sizer->Add(info, 0, wxEXPAND | wxALL, 5);

    this->SetSizer(sizer);
    sizer->SetSizeHints(this);
    return true;
  }

  return false;
}

void HdisvisFrame::OnVideoTimerTic(wxCommandEvent &event)
{
  hdisvis->video->next_cap = hdisvis->video->next_cap - 0.1;
  if (hdisvis->video->next_cap <= 0)
  {
    hdisvis->video->next_cap = hdisvis->video->delay;
    hdisvis->CaptureFrame();
  }
  if (hdisvis->video)
  {
    wxStaticText* text = (wxStaticText *) FindWindow(ID_VIDEO_DELAY);
		text->SetLabel(wxString::Format(_("Next cap: %.1f"), hdisvis->video->next_cap));
    Layout();
  }
}

void HdisvisFrame::OnVideoPause(wxCommandEvent &event)
{
  if (timer->IsRunning())
  {
    timer->Stop();
    ((wxButton *) FindWindow(ID_VIDEO_PLAY_PAUSE))->SetLabel(_("Resume"));
  }
  else
  {
    timer->Start(100);
    ((wxButton *) FindWindow(ID_VIDEO_PLAY_PAUSE))->SetLabel(_("Pause"));
  }
}

void HdisvisFrame::OnVideoCap(wxCommandEvent &event) { hdisvis->CaptureFrame(); }
void HdisvisFrame::OnVideoClose(wxCommandEvent &event) { hdisvis->OnVideoClose(); }

/****************************************************************
 *
 *              CONFIG WINDOW
 *
 ****************************************************************/
bool HdisvisFrame::ConfigFrame()
{
  wxSize text_size(75, wxDefaultSize.GetHeight());

  /* model stuff */

	// set up a number validator for the text box
  wxFloatingPointValidator<int> v_model_w(0, &(v->model_w),
			wxNUM_VAL_ZERO_AS_BLANK);
	// create the text box with the right initial content and the validator
  wxTextCtrl* model_w = new wxTextCtrl(this, ID_MODEL_W,
			wxString::Format(_("%d"), v->model_w),
      wxDefaultPosition, text_size, wxTE_PROCESS_ENTER, v_model_w);
	// and hook it all up
  Connect(ID_MODEL_W, wxEVT_COMMAND_TEXT_ENTER,
			wxCommandEventHandler(HdisvisFrame::OnModelW));

  wxFloatingPointValidator<int> v_model_h(0, &(v->model_h),
			wxNUM_VAL_ZERO_AS_BLANK);
  wxTextCtrl* model_h = new wxTextCtrl(this, ID_MODEL_H,
			wxString::Format(_("%d"), v->model_h),
      wxDefaultPosition, text_size, wxTE_PROCESS_ENTER, v_model_h);
  Connect(ID_MODEL_H, wxEVT_COMMAND_TEXT_ENTER,
			wxCommandEventHandler(HdisvisFrame::OnModelH));

  wxFloatingPointValidator<double> v_redraw_threshold(4, &(v->redraw_threshold),
			wxNUM_VAL_ZERO_AS_BLANK);
  wxTextCtrl* redraw_threshold = new wxTextCtrl(this, ID_REDRAW_THRESHOLD,
			wxString::Format(_("%.4f"), v->redraw_threshold),
      wxDefaultPosition, text_size, wxTE_PROCESS_ENTER, v_redraw_threshold);
  Connect(ID_REDRAW_THRESHOLD, wxEVT_COMMAND_TEXT_ENTER,
			wxCommandEventHandler(HdisvisFrame::OnRedrawThreshold));

  wxFloatingPointValidator<double> v_sphere_adjustment(4, &(v->sphere_adjustment),
			wxNUM_VAL_ZERO_AS_BLANK);
  wxTextCtrl* sphere_adjustment = new wxTextCtrl(this, ID_SPHERE_ADJUSTMENT,
			wxString::Format(_("%.4f"), v->sphere_adjustment),
      wxDefaultPosition, text_size, wxTE_PROCESS_ENTER, v_sphere_adjustment);
  Connect(ID_SPHERE_ADJUSTMENT, wxEVT_COMMAND_TEXT_ENTER,
			wxCommandEventHandler(HdisvisFrame::OnSphereAdjustment));

  wxFloatingPointValidator<double> v_zoom(4, &(v->zoom_speed),
			wxNUM_VAL_ZERO_AS_BLANK);
  wxTextCtrl* zoom = new wxTextCtrl(this, ID_ZOOM_SPEED,
			wxString::Format(_("%.4f"), v->zoom_speed),
      wxDefaultPosition, text_size, wxTE_PROCESS_ENTER, v_zoom);
  Connect(ID_ZOOM_SPEED, wxEVT_COMMAND_TEXT_ENTER,
			wxCommandEventHandler(HdisvisFrame::OnZoomSpeed));

  wxString c_rendermode[] = { _("VBO"), _("glu"), _("Mix"), _("poly") };
  wxChoice* rendermode = new wxChoice(this, ID_RENDERMODE,
			wxDefaultPosition, text_size, 4, c_rendermode);
  rendermode->SetSelection(v->rendermode);
  Connect(ID_RENDERMODE, wxEVT_COMMAND_CHOICE_SELECTED,
			wxCommandEventHandler(HdisvisFrame::OnControlsRendermode));

  wxFloatingPointValidator<int> v_render_switch(0, &(v->render_switch),
			wxNUM_VAL_ZERO_AS_BLANK);
  wxTextCtrl* render_switch= new wxTextCtrl(this, ID_RENDER_SWITCH,
			wxString::Format(_("%d"), v->render_switch),
      wxDefaultPosition, text_size, wxTE_PROCESS_ENTER, v_render_switch);
  Connect(ID_RENDER_SWITCH, wxEVT_COMMAND_TEXT_ENTER,
			wxCommandEventHandler(HdisvisFrame::OnRenderSwitch));

  /* arrowmode stuff */
  wxString c_arrowmode[] = { _("Rotate"), _("Translate"), _("Clip plane") };
  wxChoice* arrowmode = new wxChoice(this, ID_ARROWMODE,
			wxDefaultPosition, text_size, 3, c_arrowmode);
  arrowmode->SetSelection(v->arrowmode);
  Connect(ID_ARROWMODE, wxEVT_COMMAND_CHOICE_SELECTED,
			wxCommandEventHandler(HdisvisFrame::OnControlsArrowmode));

  wxCheckBox* clip = new wxCheckBox(this, ID_CLIP, wxEmptyString,
			wxDefaultPosition, text_size);
  clip->SetValue(v->clip);
  Connect(ID_CLIP, wxEVT_COMMAND_CHECKBOX_CLICKED,
			wxCommandEventHandler(HdisvisFrame::OnClip));

  wxFloatingPointValidator<double> v_clip_speed(4, &(v->clip_speed),
			wxNUM_VAL_ZERO_AS_BLANK);
  wxTextCtrl* clip_speed = new wxTextCtrl(this, ID_CLIP_SPEED,
			wxString::Format(_("%.4f"), v->clip_speed),
      wxDefaultPosition, text_size, wxTE_PROCESS_ENTER, v_clip_speed);
  Connect(ID_CLIP_SPEED, wxEVT_COMMAND_TEXT_ENTER,
			wxCommandEventHandler(HdisvisFrame::OnClipSpeed));

  wxFloatingPointValidator<double> v_trans_speed(4, &(v->trans_speed),
			wxNUM_VAL_ZERO_AS_BLANK);
  wxTextCtrl* trans_speed = new wxTextCtrl(this, ID_TRANS_SPEED,
			wxString::Format(_("%.4f"), v->trans_speed),
      wxDefaultPosition, text_size, wxTE_PROCESS_ENTER, v_trans_speed);
  Connect(ID_TRANS_SPEED, wxEVT_COMMAND_TEXT_ENTER,
			wxCommandEventHandler(HdisvisFrame::OnTransSpeed));

  wxFloatingPointValidator<double> v_rot_speed(4, &(v->rot_speed),
			wxNUM_VAL_ZERO_AS_BLANK);
  wxTextCtrl* rot_speed = new wxTextCtrl(this, ID_ROT_SPEED,
			wxString::Format(_("%.4f"), v->rot_speed),
      wxDefaultPosition, text_size, wxTE_PROCESS_ENTER, v_rot_speed);
  Connect(ID_ROT_SPEED, wxEVT_COMMAND_TEXT_ENTER,
			wxCommandEventHandler(HdisvisFrame::OnRotSpeed));

  /* displaymode stuff */
  wxString c_displaymode[] = { _("Normal"), _("Speed"), _("Force"), _("Contacts") };
  wxChoice* displaymode = new wxChoice(this, ID_DISPLAYMODE,
			wxDefaultPosition, text_size, 4, c_displaymode);
  displaymode->SetSelection(v->displaymode);
  Connect(ID_DISPLAYMODE, wxEVT_COMMAND_CHOICE_SELECTED,
			wxCommandEventHandler(HdisvisFrame::OnControlsDisplaymode));

  wxFloatingPointValidator<double> v_force_max(4, &(v->force_max),
			wxNUM_VAL_ZERO_AS_BLANK);
  wxTextCtrl* force_max = new wxTextCtrl(this, ID_FORCE_MAX,
			wxString::Format(_("%.4f"), v->force_max),
      wxDefaultPosition, text_size, wxTE_PROCESS_ENTER, v_force_max);
  Connect(ID_FORCE_MAX, wxEVT_COMMAND_TEXT_ENTER,
			wxCommandEventHandler(HdisvisFrame::OnForceMax));

  wxFloatingPointValidator<double> v_force_min(4, &(v->force_min),
			wxNUM_VAL_ZERO_AS_BLANK);
  wxTextCtrl* force_min = new wxTextCtrl(this, ID_FORCE_MIN,
			wxString::Format(_("%.4f"), v->force_min),
      wxDefaultPosition, text_size, wxTE_PROCESS_ENTER, v_force_min);
  Connect(ID_FORCE_MIN, wxEVT_COMMAND_TEXT_ENTER,
			wxCommandEventHandler(HdisvisFrame::OnForceMin));

  wxFloatingPointValidator<double> v_speed_vec_size(4, &(v->speed_vec_size),
			wxNUM_VAL_ZERO_AS_BLANK);
  wxTextCtrl* speed_vec_size = new wxTextCtrl(this, ID_SPEED_VEC_SIZE,
			wxString::Format(_("%.4f"), v->speed_vec_size),
      wxDefaultPosition, text_size, wxTE_PROCESS_ENTER, v_speed_vec_size);
  Connect(ID_SPEED_VEC_SIZE, wxEVT_COMMAND_TEXT_ENTER,
			wxCommandEventHandler(HdisvisFrame::OnSpeedVecSize));

  wxFloatingPointValidator<double> v_speed_base_size(4, &(v->speed_base_size),
			wxNUM_VAL_ZERO_AS_BLANK);
  wxTextCtrl* speed_base_size = new wxTextCtrl(this, ID_SPEED_BASE_SIZE,
			wxString::Format(_("%.4f"), v->speed_base_size),
      wxDefaultPosition, text_size, wxTE_PROCESS_ENTER, v_speed_base_size);
  Connect(ID_SPEED_BASE_SIZE, wxEVT_COMMAND_TEXT_ENTER,
			wxCommandEventHandler(HdisvisFrame::OnSpeedBaseSize));

  wxFloatingPointValidator<double> v_speed_max(4, &(v->speed_max),
			wxNUM_VAL_ZERO_AS_BLANK);
  wxTextCtrl* speed_max = new wxTextCtrl(this, ID_SPEED_MAX,
			wxString::Format(_("%.4f"), v->speed_max),
      wxDefaultPosition, text_size, wxTE_PROCESS_ENTER, v_speed_max);
  Connect(ID_SPEED_MAX, wxEVT_COMMAND_TEXT_ENTER,
			wxCommandEventHandler(HdisvisFrame::OnSpeedMax));

  /* display extras stuff */
  wxCheckBox* ptriangles = new wxCheckBox(this, ID_PTRIANGLES, wxEmptyString,
			wxDefaultPosition, text_size);
  ptriangles->SetValue(v->ptriangles);
  Connect(ID_PTRIANGLES, wxEVT_COMMAND_CHECKBOX_CLICKED,
			wxCommandEventHandler(HdisvisFrame::OnPtriangles));

  wxCheckBox* springs = new wxCheckBox(this, ID_SPRINGS, wxEmptyString,
			wxDefaultPosition, text_size);
  springs->SetValue(v->springs);
  Connect(ID_SPRINGS, wxEVT_COMMAND_CHECKBOX_CLICKED,
			wxCommandEventHandler(HdisvisFrame::OnSprings));
  
  wxCheckBox* box = new wxCheckBox(this, ID_BOX, wxEmptyString,
			wxDefaultPosition, text_size);
  box->SetValue(v->box);
  Connect(ID_BOX, wxEVT_COMMAND_CHECKBOX_CLICKED,
			wxCommandEventHandler(HdisvisFrame::OnBox));

  wxCheckBox* axis_display = new wxCheckBox(this, ID_AXIS_DISPLAY, wxEmptyString,
			wxDefaultPosition, text_size);
  axis_display->SetValue(v->axis_display);
  Connect(ID_AXIS_DISPLAY, wxEVT_COMMAND_CHECKBOX_CLICKED,
			wxCommandEventHandler(HdisvisFrame::OnAxis));

  wxFloatingPointValidator<int> v_spring_width(0, &(v->spring_width),
			wxNUM_VAL_ZERO_AS_BLANK);
  wxTextCtrl* spring_width = new wxTextCtrl(this, ID_SPRING_WIDTH,
			wxString::Format(_("%d"), v->spring_width),
      wxDefaultPosition, text_size, wxTE_PROCESS_ENTER, v_spring_width);
  Connect(ID_SPRING_WIDTH, wxEVT_COMMAND_TEXT_ENTER,
			wxCommandEventHandler(HdisvisFrame::OnSpringWidth));

  /* putting it all together */
  wxSize title_size(225, wxDefaultSize.GetHeight());
  int border = 4;

  vector<wxBoxSizer*> rows;
  unsigned int nrows = 0;

  wxStaticText* model_title = new wxStaticText(this,
			wxID_ANY, _("Model Settings"),
			wxDefaultPosition, title_size);
  wxFont font = model_title->GetFont();
  font.SetWeight(wxFONTWEIGHT_BOLD);
  model_title->SetFont(font);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(model_title,
			0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxLEFT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, _("Model width: ")),
			0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(model_w,
			0, wxRIGHT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, _("Model height: ")),
			0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(model_h,
			0, wxRIGHT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, _("Render method: ")),
			0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(rendermode,
			0, wxRIGHT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, _("glu threshold: ")),
			0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(render_switch,
			0, wxRIGHT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, _("Redraw sensitivity: ")),
			0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(redraw_threshold,
			0, wxRIGHT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, _("Sphere adjustment: ")),
			0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(sphere_adjustment,
			0, wxRIGHT, border);
  
  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, _("Spring width: ")),
			0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(spring_width,
			0, wxRIGHT, border);

  wxStaticText* opts_title = new wxStaticText(this,
			wxID_ANY, _("Optional Displays"), wxDefaultPosition, title_size);
  opts_title->SetFont(font);
  
  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(opts_title,
			0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxLEFT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, _("Bounding box: ")),
			0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(box,
			0, wxRIGHT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, _("Axis display: ")),
			0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(axis_display,
			0, wxRIGHT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, _("Clip plane: ")),
			0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(clip,
			0, wxRIGHT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, _("Springs: ")),
			0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(springs,
			0, wxRIGHT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, _("PTriangles: ")),
			0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(ptriangles,
			0, wxRIGHT, border);

  wxStaticText* arrow_title = new wxStaticText(this,
			wxID_ANY, _("Arrow Keys"), wxDefaultPosition, title_size);
  arrow_title->SetFont(font);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(arrow_title,
			0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxLEFT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, _("Arrow key mode: ")),
			0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(arrowmode,
			0, wxRIGHT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, _("Translate speed: ")),
			0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(trans_speed,
			0, wxRIGHT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, _("Rotate speed: ")),
			0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(rot_speed,
			0, wxRIGHT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, _("Zoom speed: ")),
			0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(zoom,
			0, wxRIGHT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, _("Clip speed: ")),
			0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(clip_speed,
			0, wxRIGHT, border);

  wxStaticText* display_title = new wxStaticText(this,
			wxID_ANY, _("Display Modes"), wxDefaultPosition, title_size);
  display_title->SetFont(font);
  
  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(display_title,
			0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxLEFT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, _("Display mode: ")),
			0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(displaymode,
			0, wxRIGHT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, _("Force max: ")),
			0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(force_max,
			0, wxRIGHT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, _("Force min: ")),
			0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(force_min,
			0, wxRIGHT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, _("Speed base width: ")),
			0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(speed_base_size,
			0, wxRIGHT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, _("Speed vector size: ")),
			0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(speed_vec_size,
			0, wxRIGHT, border);

  rows.push_back(new wxBoxSizer(wxHORIZONTAL)); nrows++;
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, wxEmptyString), 1, wxEXPAND, 0);
  rows[nrows - 1]->Add(new wxStaticText(this, wxID_ANY, _("Speed max: ")),
			0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);
  rows[nrows - 1]->Add(speed_max,
			0, wxRIGHT, border);

  /* final arrangement and stuff */
  wxStaticBoxSizer* model_sizer = new wxStaticBoxSizer(wxVERTICAL, this);
  wxStaticBoxSizer* optional_sizer = new wxStaticBoxSizer(wxVERTICAL, this);
  wxStaticBoxSizer* arrow_sizer = new wxStaticBoxSizer(wxVERTICAL, this);
  wxStaticBoxSizer* display_sizer = new wxStaticBoxSizer(wxVERTICAL, this);

  for (unsigned int r =  0; r <  8; ++r)
		model_sizer->Add(rows[r],    1, wxEXPAND | wxTOP | wxBOTTOM, 2);
  for (unsigned int r =  8; r < 14; ++r)
		optional_sizer->Add(rows[r], 1, wxEXPAND | wxTOP | wxBOTTOM, 2);
  for (unsigned int r = 14; r < 20; ++r)
		arrow_sizer->Add(rows[r],    1, wxEXPAND | wxTOP | wxBOTTOM, 2);
  for (unsigned int r = 20; r < 27; ++r)
		display_sizer->Add(rows[r],  1, wxEXPAND | wxTOP | wxBOTTOM, 2);

  wxBoxSizer* frame_sizer = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer* column1 = new wxBoxSizer(wxVERTICAL);
  wxBoxSizer* column2 = new wxBoxSizer(wxVERTICAL);

  column1->Add(model_sizer,    0, wxEXPAND | wxTOP | wxBOTTOM, 0);
  column1->Add(optional_sizer, 0, wxEXPAND | wxTOP | wxBOTTOM, 0);
  column2->Add(arrow_sizer,    0, wxEXPAND | wxTOP | wxBOTTOM, 0);
  column2->Add(display_sizer,  0, wxEXPAND | wxTOP | wxBOTTOM, 0);

  frame_sizer->Add(column1, 0, wxEXPAND, 0);
  frame_sizer->Add(column2, 0, wxEXPAND, 0);

  frame_sizer->SetSizeHints(this);
  this->SetSizer(frame_sizer);

  this->Connect(wxEVT_CLOSE_WINDOW, wxCloseEventHandler(HdisvisFrame::Hide));

  return true;
}

void HdisvisFrame::OnSphereAdjustment(wxCommandEvent &event)
{
  double temp;
  ((wxTextCtrl *) FindWindow(ID_SPHERE_ADJUSTMENT))->GetValue().ToDouble(&(temp));
  v->sphere_adjustment = temp;
  data->RefreshVBOs();
  hdisvis->model->Render();
}
void HdisvisFrame::OnRedrawThreshold(wxCommandEvent &event)
{
  double temp;
  ((wxTextCtrl *) FindWindow(ID_REDRAW_THRESHOLD))->GetValue().ToDouble(&(temp));
  v->redraw_threshold = temp;
}
void HdisvisFrame::OnRenderSwitch(wxCommandEvent &event)
{
  double temp;
  ((wxTextCtrl *) FindWindow(ID_RENDER_SWITCH))->GetValue().ToDouble(&(temp));
  v->render_switch = (int)temp;
  hdisvis->model->Render();
}
void HdisvisFrame::OnModelW(wxCommandEvent &event)
{
  double temp;
  ((wxTextCtrl *) FindWindow(ID_MODEL_W))->GetValue().ToDouble(&(temp));
  hdisvis->OnModelResize((int)temp, v->model_h);
}
void HdisvisFrame::OnModelH(wxCommandEvent &event)
{
  double temp;
  ((wxTextCtrl *) FindWindow(ID_MODEL_H))->GetValue().ToDouble(&(temp));
  hdisvis->OnModelResize(v->model_w, (int)temp);
}
void HdisvisFrame::OnZoomSpeed(wxCommandEvent &event)
{
  double temp;
  ((wxTextCtrl *) FindWindow(ID_ZOOM_SPEED))->GetValue().ToDouble(&(temp));
  v->zoom_speed = temp;
}
void HdisvisFrame::OnClipSpeed(wxCommandEvent &event)
{
  double temp;
  ((wxTextCtrl *) FindWindow(ID_CLIP_SPEED))->GetValue().ToDouble(&(temp));
  v->clip_speed = temp;
}
void HdisvisFrame::OnTransSpeed(wxCommandEvent &event)
{
  double temp;
  ((wxTextCtrl *) FindWindow(ID_TRANS_SPEED))->GetValue().ToDouble(&(temp));
  v->trans_speed = temp;
}
void HdisvisFrame::OnRotSpeed(wxCommandEvent &event)
{
  double temp;
  ((wxTextCtrl *) FindWindow(ID_ROT_SPEED))->GetValue().ToDouble(&(temp));
  v->rot_speed = temp;
}
void HdisvisFrame::OnForceMax(wxCommandEvent &event)
{
  double temp;
  ((wxTextCtrl *) FindWindow(ID_FORCE_MAX))->GetValue().ToDouble(&(temp));
  hdisvis->SetForceMax(temp);
}
void HdisvisFrame::OnForceMin(wxCommandEvent &event)
{
  double temp;
  ((wxTextCtrl *) FindWindow(ID_FORCE_MIN))->GetValue().ToDouble(&(temp));
  hdisvis->SetForceMin(temp);
}
void HdisvisFrame::OnSpeedWidth(wxCommandEvent &event)
{
  double temp;
  ((wxTextCtrl *) FindWindow(ID_SPEED_WIDTH))->GetValue().ToDouble(&(temp));
  hdisvis->SetSpeedWidth((int)temp);
}
void HdisvisFrame::OnSpeedVecSize(wxCommandEvent &event)
{
  double temp;
  ((wxTextCtrl *) FindWindow(ID_SPEED_VEC_SIZE))->GetValue().ToDouble(&(temp));
  hdisvis->SetSpeedVecSize(temp);
}
void HdisvisFrame::OnSpeedBaseSize(wxCommandEvent &event)
{
  double temp;
  ((wxTextCtrl *) FindWindow(ID_SPEED_BASE_SIZE))->GetValue().ToDouble(&(temp));
  hdisvis->SetSpeedBaseSize(temp);
}
void HdisvisFrame::OnSpeedMax(wxCommandEvent &event)
{
  double temp;
  ((wxTextCtrl *) FindWindow(ID_SPEED_MAX))->GetValue().ToDouble(&(temp));
  hdisvis->SetSpeedMax(temp);
}
void HdisvisFrame::OnSpringWidth(wxCommandEvent &event)
{
  double temp;
  ((wxTextCtrl *) FindWindow(ID_SPRING_WIDTH))->GetValue().ToDouble(&(temp));
  v->spring_width = (int)temp;
  hdisvis->model->Render();
}
void HdisvisFrame::OnSprings(wxCommandEvent &event)
{ hdisvis->OnSpringsToggle(event.GetInt()); }
void HdisvisFrame::OnPtriangles(wxCommandEvent &event)
{ hdisvis->OnPtrianglesToggle(event.GetInt()); }
void HdisvisFrame::OnClip(wxCommandEvent &event)
{ hdisvis->OnClipToggle(event.GetInt()); }
void HdisvisFrame::OnBox(wxCommandEvent &event)
{ hdisvis->OnBoxToggle(event.GetInt()); }
void HdisvisFrame::OnAxis(wxCommandEvent &event)
{ hdisvis->OnAxisDisplayToggle(event.GetInt()); }

/****************************************************************
 *
 *              CAR WINDOW
 *
 ****************************************************************/
bool HdisvisFrame::CarFrame()
{
  wxSize choice_size(120, 30);
  wxSize slider_size(120, wxDefaultSize.GetHeight());
  
  /* atoms */
  wxStaticBoxSizer* atom_sizer = new wxStaticBoxSizer(wxHORIZONTAL, this);

  wxBoxSizer* atom_select_sizer = new wxBoxSizer(wxVERTICAL);
  atom_select_sizer->Add(new wxStaticText(this, wxID_ANY, _("Atom Group")),
      0, wxALIGN_CENTER, 0);
  atom_select_sizer->Add(new wxChoice(this, ID_ATOM_PROP_GROUP, wxDefaultPosition, choice_size, 0, NULL),
      0, wxALIGN_CENTER, 0);
  atom_select_sizer->Add(new wxColourPickerCtrl(this, ID_ATOM_PROP_COLOR, *wxBLACK, wxDefaultPosition, wxDefaultSize),
      0, wxALIGN_CENTER, 0);

  wxFlexGridSizer* atom_control_sizer = new wxFlexGridSizer(4, 2, 5, 5);
  atom_control_sizer->Add(new wxStaticText(this, wxID_ANY, _("Red: ")), 0, wxALIGN_RIGHT, 0);
  atom_control_sizer->Add(new wxSlider(this, ID_ATOM_PROP_R, 0, 0, 100, wxDefaultPosition, slider_size),
      1, wxEXPAND, 0);
  atom_control_sizer->Add(new wxStaticText(this, wxID_ANY, _("Green: ")), 0, wxALIGN_RIGHT, 0);
  atom_control_sizer->Add(new wxSlider(this, ID_ATOM_PROP_G, 0, 0, 100, wxDefaultPosition, slider_size),
      1, wxEXPAND, 0);
  atom_control_sizer->Add(new wxStaticText(this, wxID_ANY, _("Blue: ")), 0, wxALIGN_RIGHT, 0);
  atom_control_sizer->Add(new wxSlider(this, ID_ATOM_PROP_B, 0, 0, 100, wxDefaultPosition, slider_size),
      1, wxEXPAND, 0);
  atom_control_sizer->Add(new wxStaticText(this, wxID_ANY, _("Alpha: ")), 0, wxALIGN_RIGHT, 0);
  atom_control_sizer->Add(new wxSlider(this, ID_ATOM_PROP_A, 0, 0, 100, wxDefaultPosition, slider_size),
      1, wxEXPAND, 0);

  atom_sizer->Add(atom_select_sizer, 0, wxEXPAND | wxRIGHT, 10);
  atom_sizer->Add(atom_control_sizer, 1, wxEXPAND, 0);

  /* springs */
  wxStaticBoxSizer* spring_sizer = new wxStaticBoxSizer(wxHORIZONTAL, this);

  wxBoxSizer* spring_select_sizer = new wxBoxSizer(wxVERTICAL);
  spring_select_sizer->Add(new wxStaticText(this, wxID_ANY, _("Spring Group")),
      0, wxALIGN_CENTER, 0);
  spring_select_sizer->Add(new wxChoice(this, ID_SPRING_PROP_GROUP, wxDefaultPosition, choice_size, 0, NULL),
      0, wxALIGN_CENTER, 0);
  spring_select_sizer->Add(new wxColourPickerCtrl(this, ID_SPRING_PROP_COLOR, *wxBLACK, wxDefaultPosition, wxDefaultSize),
      0, wxALIGN_CENTER, 0);

  wxFlexGridSizer* spring_control_sizer = new wxFlexGridSizer(4, 2, 5, 5);
  spring_control_sizer->Add(new wxStaticText(this, wxID_ANY, _("Red: ")), 0, wxALIGN_RIGHT, 0);
  spring_control_sizer->Add(new wxSlider(this, ID_SPRING_PROP_R, 0, 0, 100, wxDefaultPosition, slider_size),
      1, wxEXPAND, 0);
  spring_control_sizer->Add(new wxStaticText(this, wxID_ANY, _("Green: ")), 0, wxALIGN_RIGHT, 0);
  spring_control_sizer->Add(new wxSlider(this, ID_SPRING_PROP_G, 0, 0, 100, wxDefaultPosition, slider_size),
      1, wxEXPAND, 0);
  spring_control_sizer->Add(new wxStaticText(this, wxID_ANY, _("Blue: ")), 0, wxALIGN_RIGHT, 0);
  spring_control_sizer->Add(new wxSlider(this, ID_SPRING_PROP_B, 0, 0, 100, wxDefaultPosition, slider_size),
      1, wxEXPAND, 0);
  spring_control_sizer->Add(new wxStaticText(this, wxID_ANY, _("Alpha: ")), 0, wxALIGN_RIGHT, 0);
  spring_control_sizer->Add(new wxSlider(this, ID_SPRING_PROP_A, 0, 0, 100, wxDefaultPosition, slider_size),
      1, wxEXPAND, 0);

  spring_sizer->Add(spring_select_sizer, 0, wxEXPAND | wxRIGHT, 10);
  spring_sizer->Add(spring_control_sizer, 1, wxEXPAND, 0);

  /* ptriangles */
  wxStaticBoxSizer* ptriangle_sizer = new wxStaticBoxSizer(wxHORIZONTAL, this);

  wxBoxSizer* ptriangle_select_sizer = new wxBoxSizer(wxVERTICAL);
  ptriangle_select_sizer->Add(new wxStaticText(this, wxID_ANY, _("PTriangle Group")),
      0, wxALIGN_CENTER, 0);
  ptriangle_select_sizer->Add(new wxChoice(this, ID_PTRIANGLE_PROP_GROUP, wxDefaultPosition, choice_size, 0, NULL),
      0, wxALIGN_CENTER, 0);
  ptriangle_select_sizer->Add(new wxColourPickerCtrl(this, ID_PTRIANGLE_PROP_COLOR, *wxBLACK, wxDefaultPosition, wxDefaultSize),
      0, wxALIGN_CENTER, 0);

  wxFlexGridSizer* ptriangle_control_sizer = new wxFlexGridSizer(4, 2, 5, 5);
  ptriangle_control_sizer->Add(new wxStaticText(this, wxID_ANY, _("Red: ")), 0, wxALIGN_RIGHT, 0);
  ptriangle_control_sizer->Add(new wxSlider(this, ID_PTRIANGLE_PROP_R, 0, 0, 100, wxDefaultPosition, slider_size),
      1, wxEXPAND, 0);
  ptriangle_control_sizer->Add(new wxStaticText(this, wxID_ANY, _("Green: ")), 0, wxALIGN_RIGHT, 0);
  ptriangle_control_sizer->Add(new wxSlider(this, ID_PTRIANGLE_PROP_G, 0, 0, 100, wxDefaultPosition, slider_size),
      1, wxEXPAND, 0);
  ptriangle_control_sizer->Add(new wxStaticText(this, wxID_ANY, _("Blue: ")), 0, wxALIGN_RIGHT, 0);
  ptriangle_control_sizer->Add(new wxSlider(this, ID_PTRIANGLE_PROP_B, 0, 0, 100, wxDefaultPosition, slider_size),
      1, wxEXPAND, 0);
  ptriangle_control_sizer->Add(new wxStaticText(this, wxID_ANY, _("Alpha: ")), 0, wxALIGN_RIGHT, 0);
  ptriangle_control_sizer->Add(new wxSlider(this, ID_PTRIANGLE_PROP_A, 0, 0, 100, wxDefaultPosition, slider_size),
      1, wxEXPAND, 0);

  ptriangle_sizer->Add(ptriangle_select_sizer, 0, wxEXPAND | wxRIGHT, 10);
  ptriangle_sizer->Add(ptriangle_control_sizer, 1, wxEXPAND, 0);

  this->Connect(ID_CONTROLS_ALIGN, wxEVT_COMMAND_BUTTON_CLICKED,
      wxCommandEventHandler(HdisvisFrame::OnControlsAlign));
  this->Connect(ID_CONTROLS_RESETTRANS, wxEVT_COMMAND_BUTTON_CLICKED,
      wxCommandEventHandler(HdisvisFrame::OnControlsResettrans));

  /* all three */
  wxBoxSizer* car_sizer = new wxBoxSizer(wxVERTICAL);
  car_sizer->Add(atom_sizer, 1, wxEXPAND, 0);
  car_sizer->Add(spring_sizer, 1, wxEXPAND, 0);
  car_sizer->Add(ptriangle_sizer, 1, wxEXPAND, 0);
  
  car_sizer->SetSizeHints(this);
  this->SetSizer(car_sizer);

  Connect(wxEVT_CLOSE_WINDOW, wxCloseEventHandler(HdisvisFrame::Hide));

  /* connect all the event handlers */
  Connect(ID_ATOM_PROP_GROUP,      wxEVT_COMMAND_CHOICE_SELECTED,
			wxCommandEventHandler(HdisvisFrame::OnGroupSelected));
  Connect(ID_SPRING_PROP_GROUP,    wxEVT_COMMAND_CHOICE_SELECTED,
			wxCommandEventHandler(HdisvisFrame::OnGroupSelected));
  Connect(ID_PTRIANGLE_PROP_GROUP, wxEVT_COMMAND_CHOICE_SELECTED,
			wxCommandEventHandler(HdisvisFrame::OnGroupSelected));

  Connect(ID_ATOM_PROP_COLOR,      wxEVT_COMMAND_COLOURPICKER_CHANGED,
			wxColourPickerEventHandler(HdisvisFrame::OnColorUpdated));
  Connect(ID_SPRING_PROP_COLOR,    wxEVT_COMMAND_COLOURPICKER_CHANGED,
			wxColourPickerEventHandler(HdisvisFrame::OnColorUpdated));
  Connect(ID_PTRIANGLE_PROP_COLOR, wxEVT_COMMAND_COLOURPICKER_CHANGED,
			wxColourPickerEventHandler(HdisvisFrame::OnColorUpdated));

#ifdef __APPLE__

  Connect(ID_ATOM_PROP_R,      wxEVT_SCROLL_THUMBRELEASE,
			wxScrollEventHandler(HdisvisFrame::OnRedSliderUpdated));
  Connect(ID_SPRING_PROP_R,    wxEVT_SCROLL_THUMBRELEASE,
			wxScrollEventHandler(HdisvisFrame::OnRedSliderUpdated));
  Connect(ID_PTRIANGLE_PROP_R, wxEVT_SCROLL_THUMBRELEASE,
			wxScrollEventHandler(HdisvisFrame::OnRedSliderUpdated));

  Connect(ID_ATOM_PROP_G,      wxEVT_SCROLL_THUMBRELEASE,
			wxScrollEventHandler(HdisvisFrame::OnGreenSliderUpdated));
  Connect(ID_SPRING_PROP_G,    wxEVT_SCROLL_THUMBRELEASE,
			wxScrollEventHandler(HdisvisFrame::OnGreenSliderUpdated));
  Connect(ID_PTRIANGLE_PROP_G, wxEVT_SCROLL_THUMBRELEASE,
			wxScrollEventHandler(HdisvisFrame::OnGreenSliderUpdated));

  Connect(ID_ATOM_PROP_B,      wxEVT_SCROLL_THUMBRELEASE,
			wxScrollEventHandler(HdisvisFrame::OnBlueSliderUpdated));
  Connect(ID_SPRING_PROP_B,    wxEVT_SCROLL_THUMBRELEASE,
			wxScrollEventHandler(HdisvisFrame::OnBlueSliderUpdated));
  Connect(ID_PTRIANGLE_PROP_B, wxEVT_SCROLL_THUMBRELEASE,
			wxScrollEventHandler(HdisvisFrame::OnBlueSliderUpdated));

  Connect(ID_ATOM_PROP_A,      wxEVT_SCROLL_THUMBRELEASE,
			wxScrollEventHandler(HdisvisFrame::OnAlphaSliderUpdated));
  Connect(ID_SPRING_PROP_A,    wxEVT_SCROLL_THUMBRELEASE,
			wxScrollEventHandler(HdisvisFrame::OnAlphaSliderUpdated));
  Connect(ID_PTRIANGLE_PROP_A, wxEVT_SCROLL_THUMBRELEASE,
			wxScrollEventHandler(HdisvisFrame::OnAlphaSliderUpdated));

#else

  Connect(ID_ATOM_PROP_R,      wxEVT_SCROLL_CHANGED,
			wxScrollEventHandler(HdisvisFrame::OnRedSliderUpdated));
  Connect(ID_SPRING_PROP_R,    wxEVT_SCROLL_CHANGED,
			wxScrollEventHandler(HdisvisFrame::OnRedSliderUpdated));
  Connect(ID_PTRIANGLE_PROP_R, wxEVT_SCROLL_CHANGED,
			wxScrollEventHandler(HdisvisFrame::OnRedSliderUpdated));

  Connect(ID_ATOM_PROP_G,      wxEVT_SCROLL_CHANGED,
			wxScrollEventHandler(HdisvisFrame::OnGreenSliderUpdated));
  Connect(ID_SPRING_PROP_G,    wxEVT_SCROLL_CHANGED,
			wxScrollEventHandler(HdisvisFrame::OnGreenSliderUpdated));
  Connect(ID_PTRIANGLE_PROP_G, wxEVT_SCROLL_CHANGED,
			wxScrollEventHandler(HdisvisFrame::OnGreenSliderUpdated));

  Connect(ID_ATOM_PROP_B,      wxEVT_SCROLL_CHANGED,
			wxScrollEventHandler(HdisvisFrame::OnBlueSliderUpdated));
  Connect(ID_SPRING_PROP_B,    wxEVT_SCROLL_CHANGED,
			wxScrollEventHandler(HdisvisFrame::OnBlueSliderUpdated));
  Connect(ID_PTRIANGLE_PROP_B, wxEVT_SCROLL_CHANGED,
			wxScrollEventHandler(HdisvisFrame::OnBlueSliderUpdated));

  Connect(ID_ATOM_PROP_A,      wxEVT_SCROLL_CHANGED,
			wxScrollEventHandler(HdisvisFrame::OnAlphaSliderUpdated));
  Connect(ID_SPRING_PROP_A,    wxEVT_SCROLL_CHANGED,
			wxScrollEventHandler(HdisvisFrame::OnAlphaSliderUpdated));
  Connect(ID_PTRIANGLE_PROP_A, wxEVT_SCROLL_CHANGED,
			wxScrollEventHandler(HdisvisFrame::OnAlphaSliderUpdated));

#endif

  return true;
}

int HdisvisFrame::GetGroupIndex()
{
  wxChoice* choice = (wxChoice*) FindWindow(ID_ATOM_PROP_GROUP);
  int selection = choice->GetSelection();
  
  /* short circuit if no item is selected */
  if (selection == wxNOT_FOUND) return 0;
  
  /* find the group name of the selected item */
  wxString label = choice->GetString(selection);
  int group_index = 0;
  
  /* and then loop through where the strings are stored and find the
   * right index */
  for (unsigned int i = 0; i < data->nAGs; ++i)
    {
      if (data->atom_group_strings[i].IsSameAs(label))
        {
          group_index = i;
          break;
        }
    }
  return group_index;
}

void HdisvisFrame::OnGroupSelected(wxCommandEvent& event)
{
  int type = -1;
  switch(event.GetId())
  {
    case ID_ATOM_PROP_GROUP: type = ATOM; break;
    case ID_SPRING_PROP_GROUP: type = SPRING; break;
    case ID_PTRIANGLE_PROP_GROUP: type = PTRIANGLE; break;
  }
  UpdatePropWidgets(type, event.GetInt(), false);

  /* make sure we don't leave an incorrect Normal or Force displaymode selected when
   * we pick a new atom group (since these are now per-atom-group */
  if (type == ATOM &&
      (v->displaymode == DISPLAYMODE_FORCE || v->displaymode == DISPLAYMODE_NORMAL))
    {
      if (data->atom_group_color_by_force[GetGroupIndex()])
        SetChoice(hdisvis->configFrame, ID_DISPLAYMODE, DISPLAYMODE_FORCE);
      else
        SetChoice(hdisvis->configFrame, ID_DISPLAYMODE, DISPLAYMODE_NORMAL);
    }
}

void HdisvisFrame::OnColorUpdated(wxColourPickerEvent& event)
{
  Color* prop = NULL;
  switch (event.GetId())
  {
    case ID_ATOM_PROP_COLOR:
      prop = &data->group_atom_colors[GetGroupIndex()];
      break;
    case ID_SPRING_PROP_COLOR:
      prop = &data->group_spring_colors[((wxChoice*) FindWindow(ID_SPRING_PROP_GROUP))->GetSelection()];
      break;
    case ID_PTRIANGLE_PROP_COLOR:
      prop = &data->group_ptriangle_colors[((wxChoice*) FindWindow(ID_PTRIANGLE_PROP_GROUP))->GetSelection()];
      break;
  }
  if (prop == NULL) { wxLogMessage(_("OnColorUpdated: NULL property")); return; }
  prop->r = event.GetColour().Red()/255.0;
  prop->g = event.GetColour().Green()/255.0;
  prop->b = event.GetColour().Blue()/255.0;
  hdisvis->model->Render();
}

void HdisvisFrame::OnRedSliderUpdated(wxScrollEvent& event)
{
  Color* prop = NULL;
  switch (event.GetId())
  {
    case ID_ATOM_PROP_R:
      prop = &data->group_atom_colors[GetGroupIndex()];
      break;
    case ID_SPRING_PROP_R:
      prop = &data->group_spring_colors[((wxChoice*) FindWindow(ID_SPRING_PROP_GROUP))->GetSelection()];
      break;
    case ID_PTRIANGLE_PROP_R:
      prop = &data->group_ptriangle_colors[((wxChoice*) FindWindow(ID_PTRIANGLE_PROP_GROUP))->GetSelection()];
      break;
  }
  if (prop == NULL) { wxLogMessage(_("OnRedSliderUpdated: NULL property")); return; }
  prop->r_var = event.GetPosition()/100.0;
  hdisvis->model->Render();
}

void HdisvisFrame::OnGreenSliderUpdated(wxScrollEvent& event)
{
  Color* prop = NULL;
  switch (event.GetId())
  {
    case ID_ATOM_PROP_G:
      prop = &data->group_atom_colors[GetGroupIndex()];
      break;
    case ID_SPRING_PROP_G:
      prop = &data->group_spring_colors[((wxChoice*) FindWindow(ID_SPRING_PROP_GROUP))->GetSelection()];
      break;
    case ID_PTRIANGLE_PROP_G:
      prop = &data->group_ptriangle_colors[((wxChoice*) FindWindow(ID_PTRIANGLE_PROP_GROUP))->GetSelection()];
      break;
  }
  if (prop == NULL) { wxLogMessage(_("OnGreenSliderUpdated: NULL property")); return; }
  prop->g_var = event.GetPosition()/100.0;
  hdisvis->model->Render();
}

void HdisvisFrame::OnBlueSliderUpdated(wxScrollEvent& event)
{
  Color* prop = NULL;
  switch (event.GetId())
  {
    case ID_ATOM_PROP_B:
      prop = &data->group_atom_colors[GetGroupIndex()];
      break;
    case ID_SPRING_PROP_B:
      prop = &data->group_spring_colors[((wxChoice*) FindWindow(ID_SPRING_PROP_GROUP))->GetSelection()];
      break;
    case ID_PTRIANGLE_PROP_B:
      prop = &data->group_ptriangle_colors[((wxChoice*) FindWindow(ID_PTRIANGLE_PROP_GROUP))->GetSelection()];
      break;
  }
  if (prop == NULL) { wxLogMessage(_("OnBlueSliderUpdated: NULL property")); return; }
  prop->b_var = event.GetPosition()/100.0;
  hdisvis->model->Render();
}

void HdisvisFrame::OnAlphaSliderUpdated(wxScrollEvent& event)
{
  Color* prop = NULL;
  switch (event.GetId())
  {
    case ID_ATOM_PROP_A:
      prop = &data->group_atom_colors[GetGroupIndex()];
      break;
    case ID_SPRING_PROP_A:
      prop = &data->group_spring_colors[((wxChoice*) FindWindow(ID_SPRING_PROP_GROUP))->GetSelection()];
      break;
    case ID_PTRIANGLE_PROP_A:
      prop = &data->group_ptriangle_colors[((wxChoice*) FindWindow(ID_PTRIANGLE_PROP_GROUP))->GetSelection()];
      break;
  }
  if (prop == NULL) { wxLogMessage(_("OnAlphaSliderUpdated: NULL property")); return; }
  prop->a = event.GetPosition()/100.0;
  hdisvis->model->Render();
}

/*! \brief Update color widgets to reflect a newly selected group
 * \param type Which group selection has changed (ATOM/SPRING/PTRIANGLE)
 * \param index The lookup index of the newly selected group
 * \param set_choice If the group selection widgets needs to be updated
 *
 * The set_choice option is provided to prevent the group choice control from being set again
 * if the user setting it was the source of the event trigger (this looks like a brief flickering
 * of the control).
 */
void HdisvisFrame::UpdatePropWidgets(int type, int index, bool set_choice)
{
  Color* prop = NULL;
  switch(type)
  {
    case ATOM:
      {
        prop = &data->group_atom_colors[GetGroupIndex()];
        if (set_choice) ((wxChoice*) FindWindow(ID_ATOM_PROP_GROUP))->SetSelection(index);
        ((wxColourPickerCtrl*) FindWindow(ID_ATOM_PROP_COLOR))->
          SetColour(wxColour(prop->r*255, prop->g*255, prop->b*255));
        ((wxSlider*) FindWindow(ID_ATOM_PROP_R))->SetValue((int) (prop->r_var * 100));
        ((wxSlider*) FindWindow(ID_ATOM_PROP_G))->SetValue((int) (prop->g_var * 100));
        ((wxSlider*) FindWindow(ID_ATOM_PROP_B))->SetValue((int) (prop->b_var * 100));
        ((wxSlider*) FindWindow(ID_ATOM_PROP_A))->SetValue((int) (prop->a * 100));
      } break;
    case SPRING:
      {
        prop = &data->group_spring_colors[index];
        if (set_choice) ((wxChoice*) FindWindow(ID_SPRING_PROP_GROUP))->SetSelection(index);
        ((wxColourPickerCtrl*) FindWindow(ID_SPRING_PROP_COLOR))->
          SetColour(wxColour(prop->r*255, prop->g*255, prop->b*255));
        ((wxSlider*) FindWindow(ID_SPRING_PROP_R))->SetValue((int) (prop->r_var * 100));
        ((wxSlider*) FindWindow(ID_SPRING_PROP_G))->SetValue((int) (prop->g_var * 100));
        ((wxSlider*) FindWindow(ID_SPRING_PROP_B))->SetValue((int) (prop->b_var * 100));
        ((wxSlider*) FindWindow(ID_SPRING_PROP_A))->SetValue((int) (prop->a * 100));
      } break;
    case PTRIANGLE:
      {
        prop = &data->group_ptriangle_colors[index];
        if (set_choice) ((wxChoice*) FindWindow(ID_PTRIANGLE_PROP_GROUP))->SetSelection(index);
        ((wxColourPickerCtrl*) FindWindow(ID_PTRIANGLE_PROP_COLOR))->
          SetColour(wxColour(prop->r*255, prop->g*255, prop->b*255));
        ((wxSlider*) FindWindow(ID_PTRIANGLE_PROP_R))->SetValue((int) (prop->r_var * 100));
        ((wxSlider*) FindWindow(ID_PTRIANGLE_PROP_G))->SetValue((int) (prop->g_var * 100));
        ((wxSlider*) FindWindow(ID_PTRIANGLE_PROP_B))->SetValue((int) (prop->b_var * 100));
        ((wxSlider*) FindWindow(ID_PTRIANGLE_PROP_A))->SetValue((int) (prop->a * 100));
      } break;
    default:
      wxLogMessage(_("UpdatePropWidgets: unknown type %d"), type); break;
  }
}

/*!\brief Enable/disable a panel of color widgets
 * \param type Which panel should be changed (ATOM/SPRING/PTRIANGLE)
 * \param val Desired widget status
 */
void HdisvisFrame::TogglePropWidgets(int type, bool val)
{
  switch(type)
  {
    case ATOM:
      {
        ((wxChoice*) FindWindow(ID_ATOM_PROP_GROUP))->SetSelection(wxNOT_FOUND); //clear selection
        ((wxChoice*) FindWindow(ID_ATOM_PROP_GROUP))->Enable(val);
        ((wxColourPickerCtrl*) FindWindow(ID_ATOM_PROP_COLOR))->Enable(val);
        ((wxSlider*) FindWindow(ID_ATOM_PROP_R))->Enable(val);
        ((wxSlider*) FindWindow(ID_ATOM_PROP_G))->Enable(val);
        ((wxSlider*) FindWindow(ID_ATOM_PROP_B))->Enable(val);
        ((wxSlider*) FindWindow(ID_ATOM_PROP_A))->Enable(val);
      } break;
    case SPRING:
      {
        ((wxChoice*) FindWindow(ID_SPRING_PROP_GROUP))->SetSelection(wxNOT_FOUND);
        ((wxChoice*) FindWindow(ID_SPRING_PROP_GROUP))->Enable(val);
        ((wxColourPickerCtrl*) FindWindow(ID_SPRING_PROP_COLOR))->Enable(val);
        ((wxSlider*) FindWindow(ID_SPRING_PROP_R))->Enable(val);
        ((wxSlider*) FindWindow(ID_SPRING_PROP_G))->Enable(val);
        ((wxSlider*) FindWindow(ID_SPRING_PROP_B))->Enable(val);
        ((wxSlider*) FindWindow(ID_SPRING_PROP_A))->Enable(val);
      } break;
    case PTRIANGLE:
      {
        ((wxChoice*) FindWindow(ID_PTRIANGLE_PROP_GROUP))->SetSelection(wxNOT_FOUND);
        ((wxChoice*) FindWindow(ID_PTRIANGLE_PROP_GROUP))->Enable(val);
        ((wxColourPickerCtrl*) FindWindow(ID_PTRIANGLE_PROP_COLOR))->Disable();
        ((wxSlider*) FindWindow(ID_PTRIANGLE_PROP_R))->Enable(val);
        ((wxSlider*) FindWindow(ID_PTRIANGLE_PROP_G))->Enable(val);
        ((wxSlider*) FindWindow(ID_PTRIANGLE_PROP_B))->Enable(val);
        ((wxSlider*) FindWindow(ID_PTRIANGLE_PROP_A))->Enable(val);
      } break;
    default:
      wxLogMessage(_("UpdatePropWidgets: unknown type %d"), type); break;
  }
}

/*! \brief Update color widgets to reflect a new set of data
 *
 * The group choice menus are cleared and then repopulated, and the spring/ptriangle controls
 * are toggled on or off as needed.
 */
void HdisvisFrame::InitializePropWidgets()
{
  ((wxChoice*) FindWindow(ID_ATOM_PROP_GROUP))->Clear();
  for (unsigned int b = 0; b < data->nAGs; ++b)
    {
      if (data->atom_group_valid[b])
        ((wxChoice*) FindWindow(ID_ATOM_PROP_GROUP))->Append(data->atom_group_strings[b]);
    }
  
  if (data->nAGs > 0)
    {
      TogglePropWidgets(ATOM, true);
      UpdatePropWidgets(ATOM, 0);
    }
  else
    TogglePropWidgets(ATOM, false);
  
  ((wxChoice*) FindWindow(ID_SPRING_PROP_GROUP))->Clear();
  for (unsigned int s = 0; s < data->nSGs; ++s)
    ((wxChoice*) FindWindow(ID_SPRING_PROP_GROUP))->Append(wxString::Format(_("%u"), s));
  if (data->nSGs > 0)
    {
      TogglePropWidgets(SPRING, true);
      UpdatePropWidgets(SPRING, 0);
    }
  else
    TogglePropWidgets(SPRING, false);
  
  ((wxChoice*) FindWindow(ID_PTRIANGLE_PROP_GROUP))->Clear();
  for (unsigned int t = 0; t < data->nTGs; ++t)
    ((wxChoice*) FindWindow(ID_PTRIANGLE_PROP_GROUP))->Append(wxString::Format(_("%u"), t));
  if (data->nTGs > 0)
    {
      TogglePropWidgets(PTRIANGLE, true);
      UpdatePropWidgets(PTRIANGLE, 0);
    }
  else
    TogglePropWidgets(PTRIANGLE, false);
}
