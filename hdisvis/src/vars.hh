#ifndef VARS_HH
#define VARS_HH

#include "defs.hh"

#include <wx/wx.h>
#include <wx/arrstr.h>

#include "data.hh"

#include <list>
#include <vector>
using namespace std;

/*! \brief Global variables and settings
 *
 * Stores all non-h5 data that needs to be shared across the application. Also handles reading and
 * writing configuration files (which have access to some, but not all, of the settings). 
 */ 
class Vars
{
public:
  Vars();

  /** @name Settable variables
   *
   * These variables are exposed to the user and settable from a config file
   */
  //@{
  int model_w;                                   /*!< \brief Model width (pixels) */
  int model_h;                                   /*!< \brief Model height (pixels) */

  double zoom;                                   /*!< \brief Model distance (world units) */
  double zoom_speed;                             /*!< \brief Zooming speed (world units) */

  int rendermode;                                /*!< \brief Method used for rendering (vbo/glu/mix) */
  int render_switch;                             /*!< \brief vbo/glu transition size for mixed render method (pixels) */
  bool sort_atoms;

  double trans_speed;                            /*!< \brief Model translation speed (world units) */
  double rot_speed;                              /*!< \brief Model rotation speed (world units) */
  double sphere_adjustment;                      /*!< \brief vbo sphere radius adjustment (inscribed at 1) */
  double redraw_threshold;                       /*!< \brief Distance an atom can move before being updated (world units) */

  int displaymode;                               /*!< \brief Displayed information (normal/force colors/velocity vectors) */
  int arrowmode;                                 /*!< \brief Arrow key effects (translate model/rotate model/adjust clip plane) */

  bool clip;                                     /*!< \brief If the clip plane in enabled */
  double clip_speed;                             /*!< \brief Clip plane translation speed (world units) */
  vector<vec3d_t> clip_up;                               /*!< \brief Current clip plane normal vector (unit length) */
  vector<vec3d_t> clip_pos;                              /*!< \brief Clip plane position vector */

  bool logo;                                     /*!< \brief If the hdisvis logo should be rendered */
  bool box;                                      /*!< \brief If the bounding box should be rendered */
  bool axis_display;                             /*!< \brief If the axis rotation display should be rendered */
  bool axis_labels;                              /*!< \brief If the bounding box should have numerical labels on it */
  bool selection;                                /*!< \brief If atom click-to-select is enabled */
  bool ptriangles;                               /*!< \brief If ptriangles are rendered */
	bool time_display;														 /*!< \brief If the current text is rendered */

  bool springs;                                  /*!< \brief If springs are rendered */
  int spring_width;                              /*!< \brief Line width for rendering springs (pixels) */

  double force_max;                              /*!< \brief Minimum force threshold to be colored red in force display mode */
  double force_min;                              /*!< \brief Maximum force threshold to be colored blue in force display mode */

  int speed_width;                               /*!< \brief Line width for rendering velocity vectors (pixels) */
  double speed_max;                              /*!< \brief Minimum speed threshold to be colored red in speed display mode */
  double speed_base_size;                        /*!< \brief Perpendicular velocity vector base width (world units) */
  double speed_vec_size;                         /*!< \brief Velocity vector scaling coefficient */

  vector<Color> group_atom_colors;      /*!< \brief atom group name config colors */
  vector<Color> group_spring_colors;    /*!< \brief spring group name config colors */
  vector<Color> group_ptriangle_colors; /*!< \brief ptriangle group name config colors */
  //@}

  /** @name Unsettable variables
   *
   * These variables are important only to the internal workings of the application, and are not settable by the user
   */
  //@{
  int file;                      /*!< \brief Lookup index of the currently loaded file */
  vec3d_t eye;                   /*!< \brief Position of the camera, looking at the origin (unit length) */
  vec3d_t up;                    /*!< \brief Up direction from the camera (unit length) */
  vec3d_t over;                  /*!< \brief Forms an orthonormal basis with eye and up (unit length) */
  vec3d_t trans;                 /*!< \brief Model translations vector */
  int mouseX;                    /*!< \brief Current x coordinate of the mouse (pixels) */
  int mouseY;                    /*!< \brief Current y coordinate of the mouse (pixels) */
  bool dragging;                 /*!< \brief If the user is currently dragging the model to rotate it */
  bool dragging_knob;            /*!< \brief If the user is currently dragging the clip plane rotation knob */
  wxArrayString filenames;       /*!< \brief Array of HDF5 file paths (relative to working directory) */
  wxArrayString short_filenames; /*!< \brief Array of HDF5 file names without the path */
  bool DEBUG;                    /*!< \brief If the debug log should be displayed when opening the application */
  bool wants_click;              /*!< \brief If hdisvis is waiting for the user to click a point on the model */
  bool playing;                  /*!< \brief If hdisvis is playing through all h5 files */
  //@}

  bool LoadConfig(wxString fname);
  bool WriteConfig(wxString fname, Data* data);

  void SetToDefault();
  
  void AddConfigColor(lua_State *L, vector<Color>* targ_var);

  /** @name Config file data writing functions
   *
   * \brief Functions for writing current application variables to a config file
   * \param fp File pointer to output config file
   * \param entry_name Name for the variable in the config file (specified in DEFINES)
   * \param source_var Value of the variable being written
   */
  //@{
  void WriteInt   (FILE* fp, int     source_var, wxString entry_name);
  void WriteDouble(FILE* fp, double  source_var, wxString entry_name);
  void WriteBool  (FILE* fp, bool    source_var, wxString entry_name);
  void WriteVec   (FILE* fp, vec3d_t source_var, wxString entry_name);
  /**
   * \param fp File pointer to output config file
   * \param source_color Value of the color being written
   * \param source_group Name of the group the color belongs to
   * \param comma If a comma should be written after the group or not (if there are more groups expected to be written)
   */
  void WriteColor (FILE* fp, GROUP_TYPE source_group, Color source_color, bool comma);
  //@}

  /** @name Config file data reading functions
   *
   * \brief Functions for checking the existence of and reading a value from the config file
   * \param L Current lua state created from the config file
   * \param targ_var Pointer to the variable being set
   * \param entry_name Name for the variable in the config file (specified in DEFINES)
   */
  //@{
  void SetInt   (lua_State *L,           int* targ_var, wxString entry_name);
  void SetDouble(lua_State *L,        double* targ_var, wxString entry_name);
  void SetBool  (lua_State *L,          bool* targ_var, wxString entry_name);
  void SetVec   (lua_State *L,       vec3d_t* targ_var, wxString entry_name);
  void SetVector(lua_State *L, vector<Color>* targ_var, wxString entry_name);
  //@}
};

/*! \brief Zoom directions */
enum
{
  IN=0,
  OUT
};

/*! \brief What information to render */
enum
{
  DISPLAYMODE_NORMAL=0, /*!< \brief Render all atoms normally */
  DISPLAYMODE_SPEED,    /*!< \brief Instead of atoms, render velocity vectors for each body */
  DISPLAYMODE_FORCE,    /*!< \brief Color each atom according to it's current net force */
  DISPLAYMODE_CONTACTS,
  DISPLAYMODE_FC       /*!< \brief Instead of atoms, render the current force chain */
};

/*! \brief What do to with arrow key presses when the model is selected */
enum
{
  ARROWMODE_ROTATE=0,   /*!< \brief Rotate the model */
  ARROWMODE_TRANSLATE,  /*!< \brief Translate the model */
  ARROWMODE_CLIP,       /*!< \brief Adjust the clipping plane */
};

/*! \brief How to render spheres/cylinders (for spheres, edges, capsules and tripsules) */
enum
{
  RENDERMODE_VBO=0, /*!< \brief Always use vbos (fast, low detail) */
  RENDERMODE_GLU,   /*!< \brief Always use gluSphere and gluCylinder (slow, high detail) */
  RENDERMODE_MIX,   /*!< \brief Use glu calls for larger atoms, and vbo for smaller atoms */
	RENDERMODE_POLY
};

/*! Unique IDs for each widget or menu item */
enum
{
  ID_MODEL= wxID_HIGHEST+1,

  FRAME_CAR,
  FRAME_MODEL,
  FRAME_VIDEO,
  FRAME_CONFIG,
  FRAME_LOG,

  ID_LOG_TEXT,

  ID_MENU_FILE_OPENH5,
  ID_MENU_FILE_RELOADH5,
  ID_MENU_FILE_OPENLUA,
  ID_MENU_FILE_SAVELUA,
  ID_MENU_FILE_RESETVARS,
  ID_MENU_FILE_QUIT,
  ID_MENU_FILE_SCREENSHOT,
  ID_MENU_FILE_VIDEO,
  ID_MENU_WINDOWS_QUALITY,
  ID_MENU_WINDOWS_CONTROLS,
  ID_MENU_WINDOWS_MODEL,
  ID_MENU_WINDOWS_CAR,
  ID_MENU_WINDOWS_DISPLAY,
  ID_MENU_WINDOWS_CONFIG,
  ID_MENU_WINDOWS_LOG,
  ID_MENU_MODEL_DISPLAY_NORMAL,
  ID_MENU_MODEL_DISPLAY_SPEED,
  ID_MENU_MODEL_DISPLAY_CONTACT,
  ID_MENU_MODEL_DISPLAY_FORCE,
  ID_MENU_MODEL_NEXT,
  ID_MENU_MODEL_PREV,
  ID_MENU_MODEL_CLIPTOGGLE,
  ID_MENU_MODEL_BOX,
  ID_MENU_MODEL_AXIS,
	ID_MENU_MODEL_TIME,

  ID_MENU_MODEL_QUALITY_LOW,
  ID_MENU_MODEL_QUALITY_MEDIUM,
  ID_MENU_MODEL_QUALITY_HIGH,

  ID_MENU_MODEL_SIZE_SMALL,
  ID_MENU_MODEL_SIZE_MEDIUM,
  ID_MENU_MODEL_SIZE_LARGE,

  ID_MENU_MODEL_APPROXIMATION_INSCRIBE,
  ID_MENU_MODEL_APPROXIMATION_OVERLAP,
  ID_MENU_MODEL_APPROXIMATION_CIRCUMSCRIBE,

  ID_MENU_MODEL_INFO,

  ID_MENU_MODEL_PLAY,

  ID_MENU_CONTROLS_ZOOMIN,
  ID_MENU_CONTROLS_ZOOMOUT,
  ID_MENU_CONTROLS_ARROWS_ROTATE,
  ID_MENU_CONTROLS_ARROWS_TRANSLATE,
  ID_MENU_CONTROLS_ARROWS_CLIP,
  ID_MENU_CONTROLS_ARROWS_CONTACTS,
  ID_MENU_CONTROLS_ALIGN,
  ID_MENU_CONTROLS_RESETROT,
  ID_MENU_CONTROLS_RESETTRANS,

  ID_MENU_HELP,

  ID_CONTROLS_FILETEXT,
  ID_CONTROLS_FILESLIDER,
  ID_CONTROLS_DISPLAYMODE,
  ID_CONTROLS_ARROWMODE,
  ID_CONTROLS_ALIGN,
  ID_CONTROLS_RESETROT,
  ID_CONTROLS_RESETTRANS,
  ID_CONTROLS_ZOOMIN,
  ID_CONTROLS_ZOOMOUT,
  ID_CONTROLS_PNGSS,
  ID_CONTROLS_VIDEO,
  ID_CONTROLS_QUALITY,

  ID_REDRAW_THRESHOLD,
  ID_SPHERE_ADJUSTMENT,
  ID_MODEL_W,
  ID_MODEL_H,
  ID_ZOOM_SPEED,

  ID_RENDERMODE,
  ID_RENDER_SWITCH,

  ID_ARROWMODE,
  ID_CLIP,
  ID_CLIP_SPEED,
  ID_TRANS_SPEED,
  ID_ROT_SPEED,
  ID_DISPLAYMODE,
  ID_SPRING_WIDTH,

  ID_GRAPH_ZOOMINX,
  ID_GRAPH_ZOOMINY,
  ID_GRAPH_ZOOMOUTX,
  ID_GRAPH_ZOOMOUTY,

  ID_FORCE_MAX,
  ID_FORCE_MIN,

  ID_SPEED_MAX,
  ID_SPEED_VEC_SIZE,
  ID_SPEED_BASE_SIZE,
  ID_SPEED_WIDTH,

  ID_BOX,
  ID_AXIS_DISPLAY,
  ID_SPRINGS,
  ID_PTRIANGLES,

  ID_VIDEO_CAP,
  ID_VIDEO_DELAY,
  ID_VIDEO_FRAMES,
  ID_VIDEO_PLAY_PAUSE,
  ID_VIDEO_FINALIZE,

  ID_ATOM_PROP_GROUP,
  ID_ATOM_PROP_R,
  ID_ATOM_PROP_G,
  ID_ATOM_PROP_B,
  ID_ATOM_PROP_A,
  ID_ATOM_PROP_COLOR,

  ID_SPRING_PROP_GROUP,
  ID_SPRING_PROP_R,
  ID_SPRING_PROP_G,
  ID_SPRING_PROP_B,
  ID_SPRING_PROP_A,
  ID_SPRING_PROP_COLOR,

  ID_PTRIANGLE_PROP_GROUP,
  ID_PTRIANGLE_PROP_R,
  ID_PTRIANGLE_PROP_G,
  ID_PTRIANGLE_PROP_B,
  ID_PTRIANGLE_PROP_A,
  ID_PTRIANGLE_PROP_COLOR
};

#endif //VARS_HH
