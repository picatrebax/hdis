#include "vars.hh"

// for compatibility with Lua 5.1
#if LUA_VERSION_NUM == 501
# define lua_rawlen lua_objlen
#endif

Vars::Vars()
{
  SetToDefault();

  mouseX = 0;
  mouseY = 0;
  dragging      = false;
  dragging_knob = false;

  DEBUG = false;
  wants_click = false;
  playing = false;
}

/*! \brief Reset all variables to the defaults specified in the source code
 *
 * The default values are hard coded and are not affected by which config file is loaded.
 * In order to reset all variables to the defaults specified in a config file, simply
 * reload that config file.
 */
void Vars::SetToDefault()
{
  /* SETTABLE FROM CONFIG FILE */
  model_w = 500;
  model_h = 450;
  zoom    = 6;

  rendermode = RENDERMODE_MIX;
  render_switch = 20;

  zoom_speed  = 0.10;
  trans_speed = 0.05;
  rot_speed   = 0.01;
  clip_speed  = 0.05;

  sphere_adjustment = 1.100;
  redraw_threshold  = 0.001;

  displaymode = DISPLAYMODE_NORMAL;
  arrowmode   = ARROWMODE_TRANSLATE;

  clip         = false;
  clip_up.clear();
  clip_up.push_back(get_vec(1.0, 0.0, 0.0));
  clip_pos.clear();
  clip_pos.push_back(get_vec(0.5, 0.5, 0.5));

  selection  = false;
  sort_atoms = false;

  logo         = true;
  box          = true;
  axis_display = false;
  axis_labels  = false;
  time_display = true;
  ptriangles   = true;
  springs      = true;
	time_display = true;

  spring_width = 1;

  force_max = 1.00;
  force_min = 0.00;

  speed_width     = 1;
  speed_max       = 4.000;
  speed_base_size = 0.003;
  speed_vec_size  = 0.010;

  group_atom_colors.clear();
  group_spring_colors.clear();
  group_ptriangle_colors.clear();

  eye   = get_vec(1, 0, 0);
  over  = get_vec(0, 1, 0);
  up    = get_vec(0, 0, 1);
  trans = get_vec(0, 0, 0);

  /* NOT SETTABLE FROM CONFIG FILE
  file;
  filenames;
  short_filenames;

  mouseX = 0;
  mouseY = 0;
  dragging      = false;
  dragging_knob = false;

  DEBUG = false;
  wants_click = false;
  */
}

/*! \brief Write all variables to a config file
 * \param fname Output file name with path, relative to the working directory
 * \param data Pointer to application data for accessing group color information
 * \return True on success
 */
bool Vars::WriteConfig(wxString fname, Data* data)
{
  FILE* fp = fopen(fname, "wb");

  WriteBool(fp, DEBUG, CONFIG_DEBUG);

  WriteInt(fp, model_w, CONFIG_MODEL_W);
  WriteInt(fp, model_h, CONFIG_MODEL_H);

  WriteDouble(fp, zoom, CONFIG_ZOOM);
  WriteDouble(fp, zoom_speed, CONFIG_ZOOM_SPEED);

  WriteInt(fp, rendermode, CONFIG_RENDERMODE);
  WriteInt(fp, render_switch, CONFIG_RENDER_SWITCH);

  WriteDouble(fp, trans_speed, CONFIG_TRANS_SPEED);
  WriteDouble(fp, rot_speed, CONFIG_ROT_SPEED);
  WriteDouble(fp, sphere_adjustment, CONFIG_SPHERE_ADJ);
  WriteDouble(fp, redraw_threshold, CONFIG_REDRAW);

  WriteInt(fp, displaymode, CONFIG_DISPLAYMODE);
  WriteInt(fp, arrowmode, CONFIG_ARROWMODE);

  WriteBool(fp, clip, CONFIG_CLIP);
  WriteDouble(fp, clip_speed, CONFIG_CLIP_SPEED);
  //WriteVec(fp, clip_up, CONFIG_CLIP_UP);
  //WriteVec(fp, clip_pos, CONFIG_CLIP_POS);

  WriteBool(fp, logo, CONFIG_LOGO);
  WriteBool(fp, box, CONFIG_BOX);
  WriteBool(fp, axis_display, CONFIG_AXIS_DISPLAY);
  WriteBool(fp, selection, CONFIG_SELECTION);
  WriteBool(fp, ptriangles, CONFIG_PTRIANGLES);
	WriteBool(fp, time_display, CONFIG_TIME_DISPLAY);

  WriteBool(fp, springs, CONFIG_SPRINGS);
  WriteInt(fp, spring_width, CONFIG_SPRING_WIDTH);

  WriteDouble(fp, force_max, CONFIG_FORCE_MAX);
  WriteDouble(fp, force_min, CONFIG_FORCE_MIN);

  WriteInt(fp, speed_width, CONFIG_SPEED_WIDTH);
  WriteDouble(fp, speed_max, CONFIG_SPEED_MAX);
  WriteDouble(fp, speed_base_size, CONFIG_SPEED_BASE_SIZE);
  WriteDouble(fp, speed_vec_size, CONFIG_SPEED_VEC_SIZE);

  fputs(wxString::Format(_("%s = {\n"), CONFIG_ATOM_COLORS), fp);
  for (unsigned int i = 0; i < data->nAGs; ++i)
    WriteColor(fp, i, data->group_atom_colors[i], i<(data->nAGs-1)); // don't add a comma after the last color
  fputs(_("\t}\n"), fp);

  fputs(wxString::Format(_("%s = {\n"), CONFIG_SPRING_COLORS), fp);
  for (unsigned int i = 0; i < data->nSGs; ++i)
    WriteColor(fp, i, data->group_spring_colors[i], i<(data->nSGs-1)); // don't add a comma after the last color
  fputs(_("\t}\n"), fp);

  fputs(wxString::Format(_("%s = {\n"), CONFIG_PTRIANGLE_COLORS), fp);
  for (unsigned int i = 0; i < data->nTGs; ++i)
    WriteColor(fp, i, data->group_ptriangle_colors[i], i<(data->nTGs-1)); // don't add a comma after the last color
  fputs(_("\t}\n"), fp);

  fclose(fp);
  return true;
}

/*! \brief Load variables from a config file
 * \param fname Config file name with path, relative to the working
 * directory
 * \return True on success
 */
bool Vars::LoadConfig(wxString fname)
{
  /* in general, we leave variables that aren't set in the new config
   * file as their current value - however, in order to prevent
   * duplication and unwanted coloring, we reset the color properties
   * of everything. This means if you want non-random colors, you must
   * always specify them in your new config file (cannot rely on
   * defaults)
   */
  group_atom_colors.clear();
  group_spring_colors.clear();
  group_ptriangle_colors.clear();

  lua_State *L = luaL_newstate();

  if (luaL_loadfile(L, fname))
  {
    wxLogMessage(_("LoadConfig: luaL_loadfile failed"));
    lua_close(L);
    return false;
  }
  if (lua_pcall(L, 0, 0, 0))
  {
    wxLogMessage(_("LoadConfig: lua_pcall failed"));
    lua_close(L);
    return false;
  }

  SetBool(L, &DEBUG, CONFIG_DEBUG);

  SetInt(L, &model_w, CONFIG_MODEL_W);
  SetInt(L, &model_h, CONFIG_MODEL_H);

  SetDouble(L, &zoom, CONFIG_ZOOM);
  SetDouble(L, &zoom_speed, CONFIG_ZOOM_SPEED);

  SetInt(L, &rendermode, CONFIG_RENDERMODE);
  SetInt(L, &render_switch, CONFIG_RENDER_SWITCH);

  SetDouble(L, &trans_speed, CONFIG_TRANS_SPEED);
  SetDouble(L, &rot_speed, CONFIG_ROT_SPEED);
  SetDouble(L, &sphere_adjustment, CONFIG_SPHERE_ADJ);
  SetDouble(L, &redraw_threshold, CONFIG_REDRAW);

  SetInt(L, &displaymode, CONFIG_DISPLAYMODE);
  SetInt(L, &arrowmode, CONFIG_ARROWMODE);

  SetBool(L, &clip, CONFIG_CLIP);
  SetDouble(L, &clip_speed, CONFIG_CLIP_SPEED);
  //SetVec(L, &clip_up, CONFIG_CLIP_UP);
  //SetVec(L, &clip_pos, CONFIG_CLIP_POS);

  SetBool(L, &logo, CONFIG_LOGO);
  SetBool(L, &box, CONFIG_BOX);
  SetBool(L, &axis_display, CONFIG_AXIS_DISPLAY);
  SetBool(L, &selection, CONFIG_SELECTION);
  SetBool(L, &ptriangles, CONFIG_PTRIANGLES);
	SetBool(L, &time_display, CONFIG_TIME_DISPLAY);

  SetBool(L, &springs, CONFIG_SPRINGS);
  SetInt(L, &spring_width, CONFIG_SPRING_WIDTH);

  SetDouble(L, &force_max, CONFIG_FORCE_MAX);
  SetDouble(L, &force_min, CONFIG_FORCE_MIN);

  SetInt(L, &speed_width, CONFIG_SPEED_WIDTH);
  SetDouble(L, &speed_max, CONFIG_SPEED_MAX);
  SetDouble(L, &speed_base_size, CONFIG_SPEED_BASE_SIZE);
  SetDouble(L, &speed_vec_size, CONFIG_SPEED_VEC_SIZE);

  SetVector(L, &group_atom_colors, CONFIG_ATOM_COLORS);
  SetVector(L, &group_spring_colors, CONFIG_SPRING_COLORS);
  SetVector(L, &group_ptriangle_colors, CONFIG_PTRIANGLE_COLORS);

  lua_close(L);
  return true;
}

void Vars::WriteInt(FILE* fp, int source_var, wxString entry_name)
{
  fputs(wxString::Format(_("%s = %d\n"), entry_name, source_var), fp);
}

void Vars::WriteDouble(FILE* fp, double source_var, wxString entry_name)
{
  fputs(wxString::Format(_("%s = %f\n"), entry_name, source_var), fp);
}

void Vars::WriteBool(FILE* fp, bool source_var, wxString entry_name)
{
  fputs(wxString::Format(_("%s = %d\n"), entry_name, source_var), fp);
}

void Vars::WriteVec(FILE* fp, vec3d_t source_var, wxString entry_name)
{
  fputs(wxString::Format(_("%s = { x=%f, y=%f, z=%f }\n"), entry_name, source_var.x, source_var.y , source_var.z), fp);
}

void Vars::WriteColor(FILE* fp, GROUP_TYPE source_group, Color source_color, bool comma)
{
  fputs(wxString::Format(_("{ r = %.4f, g = %.4f, b = %.4f, r_var = %.4f, g_var = %.4f, b_var = %.4f, a = %.4f }%s\n"),
          source_color.r, source_color.g, source_color.b,
          source_color.r_var, source_color.g_var, source_color.b_var,
          source_color.a,
          comma  ? "," : ""), fp);
}

void Vars::SetVec(lua_State *L, vec3d_t* targ_var, wxString entry_name)
{
  lua_getglobal(L, entry_name);
  if (lua_istable(L, -1))
  {
    vec3d_set(*targ_var, 0, 0, 0);

    lua_getfield( L, -1, "x" );
    if ( lua_isnumber( L, -1 ) )
      (*targ_var).x = lua_tonumber( L, -1 );
    lua_pop( L, 1 );

    lua_getfield( L, -1, "y" );
    if ( lua_isnumber( L, -1 ) )
      (*targ_var).y = lua_tonumber( L, -1 );
    lua_pop( L, 1 );

    lua_getfield( L, -1, "z" );
    if ( lua_isnumber( L, -1 ) )
      (*targ_var).z = lua_tonumber( L, -1 );
    lua_pop( L, 1 );
  }
}

void Vars::SetInt(lua_State *L, int* targ_var, wxString entry_name)
{
  lua_getglobal(L, entry_name);
  if (lua_isnumber(L, -1))
    *targ_var = (int)lua_tonumber(L, -1);
  lua_pop(L, 1);
}

void Vars::SetDouble(lua_State *L, double* targ_var, wxString entry_name)
{
  lua_getglobal(L, entry_name);
  if (lua_isnumber(L, -1))
    *targ_var = (double)lua_tonumber(L, -1);
  lua_pop(L, 1);
}

void Vars::SetBool(lua_State *L, bool* targ_var, wxString entry_name)
{
  lua_getglobal(L, entry_name);
  if (lua_isnumber(L, -1))
    *targ_var = (bool)lua_tonumber(L, -1);
  lua_pop(L, 1);
}

void Vars::SetVector(lua_State *L, vector<Color>* targ_var, wxString entry_name)
{
  lua_getglobal(L, entry_name);
  if ( lua_istable( L, -1 ) )
  {
    int len = lua_rawlen( L, -1 ) + 1;
    for ( int k = 0; k < len; ++k )
    {
      lua_rawgeti( L, -1, k ); // get next record for cars 
      if ( lua_istable( L, -1 ) ) // table of color data (group,r,g,b,a,var fields)
      {
        AddConfigColor(L, targ_var);
      }
      lua_pop(L, 1);
    }
  }
  lua_pop(L, 1);
}

/*! \brief Add a group color to one of the group_color vectors
 * \param L Current lua state created from the config file
 * \param targ_var Pointer to the group_color map beieng add to
 */
void Vars::AddConfigColor(lua_State *L, vector<Color>* targ_var)
{
  float r = 1;
  float g = 0;
  float b = 0;
  float a = 0;
  float dr = 0;
  float dg = 0;
  float db = 0;

  lua_getfield( L, -1, "r" );
  if ( lua_isnumber( L, -1 ) )
    r = lua_tonumber( L, -1 );
  lua_pop( L, 1 );

  lua_getfield( L, -1, "g" );
  if ( lua_isnumber( L, -1 ) )
    g = lua_tonumber( L, -1 );
  lua_pop( L, 1 );

  lua_getfield( L, -1, "b" );
  if ( lua_isnumber( L, -1 ) )
    b = lua_tonumber( L, -1 );
  lua_pop( L, 1 );

  lua_getfield( L, -1, "a" );
  if ( lua_isnumber( L, -1 ) )
    a = lua_tonumber( L, -1 );
  lua_pop( L, 1 );

  lua_getfield( L, -1, "r_var" );
  if ( lua_isnumber( L, -1 ) )
    dr = lua_tonumber( L, -1 );
  lua_pop( L, 1 );

  lua_getfield( L, -1, "g_var" );
  if ( lua_isnumber( L, -1 ) )
    dg = lua_tonumber( L, -1 );
  lua_pop( L, 1 );

  lua_getfield( L, -1, "b_var" );
  if ( lua_isnumber( L, -1 ) )
    db = lua_tonumber( L, -1 );
  lua_pop( L, 1 );

  (*targ_var).push_back(Color(r, g, b, dr, dg, db, a));
}
