#ifndef WXHDISVIS_H
#define WXHDISVIS_H

#include <wx/wx.h>
#include <wx/glcanvas.h>
#include <wx/spinctrl.h>
#include <wx/slider.h>
#include <wx/event.h>
#include <wx/checkbox.h>
#include <wx/textdlg.h>
#include <wx/filedlg.h>
#include <wx/wfstream.h>
#include <wx/clrpicker.h>
#include <wx/valnum.h>
#include <wx/string.h>
#include <wx/stdpaths.h>
#include <wx/filename.h>
#include <wx/cmdline.h>
#include <wx/log.h>
#include <wx/filefn.h>
#include <wx/utils.h>

#include <list>
#include <ctime>
using namespace std;

#include "defs.hh"
#include "utility.hh"
using namespace hdisvis_utils;

#include "images.hh"
#include "dialog.hh"
#include "model.hh"
#include "data.hh"
#include "frame.hh"


// big circular dependencies (hdisvis.hh included by frame.hh)
class HdisvisFrame;
class Model;

/*! \brief Main application class that oversees everything
 *
 * Main application class responsible for containing everything and
 * providing functions to handle saving and loading information. Also
 * provides functions for anything that needs to be called from
 * multiple event handlers (i.e. things that are both in the menus and
 * on the control panel). */
class wxhdisvis : public wxApp
{
public:
  virtual bool OnInit();
    virtual ~wxhdisvis();
  /** @name Frames
   */
  //@{
  HdisvisFrame *modelFrame;   /*!< \brief GLCanvas for rendering model and basic model manipulation controls */
  HdisvisFrame *videoFrame;   /*!< \brief All video setting controls */
  HdisvisFrame *configFrame;  /*!< \brief All settings and options that can be set in the config file */
  HdisvisFrame *carFrame;     /*!< \brief Base group color, color variation, and transparency */
  HdisvisFrame *logFrame;     /*!< \brief Debug output log */
  //@}
  
  /** @name Logo info
   */
  //@{
  unsigned int logo_w;       /*!< \brief logo pixel width */
  unsigned int logo_h;       /*!< \brief logo pixel height */
  GLubyte* logo;             /*!< \brief pixel data for the hdis logo */
  GLuint logo_id;            /*!< \brief texture id for logo */
  //@}

  Model* model;              /*!< \brief GLCanvas subclass openGL render target */
  Vars* v;                   /*!< \brief Global vairables and settings */
  Data* data;                /*!< \brief h5 file data and derived informaion */
  Video* video;              /*!< \brief Output video */
  wxLogTextCtrl* log;        /*!< \brief Log target */

  void LoadLogo(wxString file);
  void SetLogo();
  bool ParseCommandLine();
  void ArrangeFrames();
  void OnQuit();
  void OpenManual();

  /** @name File handling functions
   */
  //@{
  void GetFilenames();
  void OpenNewData(int n = 0);
  void ReloadData();
  void LoadFile(int n);
  void Play();
  void OpenConfig();
  bool LoadConfig(wxString fname);
  void SaveConfig();
  //@}

  /** @name Output functions
   */
  //@{
  void OnScreenshot();

  void OnVideo();
  void OnVideoClose();
  void CaptureFrame();
  //@}

  /** @name Simple variable modifcation functions
   *
   * \brief Update application variable
   * \param new_val The desired value
   *
   * Sets a variriable, updates its corresponding text field in the config frame, and renders the model.
   */
  //@{
  void SetForceMax(double new_val);
  void SetForceMin(double new_val);
  void SetSpeedMax(double new_val);
  void SetSpeedVecSize(double new_val);
  void SetSpeedBaseSize(double new_val);
  void SetSpeedNum(int new_val);
  void SetSpeedWidth(int new_val);
  //@}
  
  /** @name Other variable modification functions
   *
   * Sets a variable and updates all other necessary information and displays.
   */
  //@{
  void ResetVars();
  void UpdateConfigWindow();
  void SetGroupColors();
  
  void OnModelResize(int w, int h);

  void OnDisplaymode(int mode);
  void OnRendermode(int mode);
  void OnArrowmode(int mode);

  void OnClipToggle(bool val);
  void OnBoxToggle(bool val);
  void OnAxisDisplayToggle(bool val);

  void OnZoom(int direction);
  void OnAlign();
  void OnResetrot();
  void OnResettrans();

  void OnSpringsToggle(bool val);
  void OnPtrianglesToggle(bool val);
  //@}
};

#endif //WXHDISVIS_H
