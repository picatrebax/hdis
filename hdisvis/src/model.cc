#include "model.hh"

namespace sphere
{

static const float f = (float)( (1. + sqrt( 5. )) * 0.5 - 1 ); // ~0.618033989

static const vec3d_t icosVerts[] =
{
    {0, f, 1}, {0, f, -1}, {0, -f, 1}, {0, -f, -1},
    {f, 1, 0}, {f, -1, 0}, {-f, 1, 0}, {-f, -1, 0},
    {1, 0, f}, {-1, 0, f}, {1, 0, -f}, {-1, 0, -f}
};

// Line by line
static const int icosInds[][3] =
{
    {0, 2, 8}, {0, 8,  4}, {0 , 4, 6}, {0, 6,  9}, {0, 9,  2},
    {2, 5, 8}, {8, 10, 4}, {4,  1, 6}, {6, 11, 9}, {9, 7,  2},
    {2, 7, 5}, {8, 5, 10}, {4, 10, 1}, {6, 1, 11}, {9, 11, 7},
    {7, 3, 5}, {5, 3, 10}, {10, 3, 1}, {1, 3, 11}, {11, 3, 7}
};

// from here: http://dmccooey.com/polyhedra/index.html
static const float ra = 4. / ( 3. * (1. + (float)sqrt(5.)) ); // radius adjuster (mid-scribed)
//static const float ra = 2. / (3. * (float)sqrt(3.)); // radius adjuster (circ-scribed)


static const float C0 = .75 * ((float)sqrt(5.) - 1.) * ra;
static const float C1 = .75 * ((float)sqrt(5.) + 1.) * ra;
static const float C2 = 9. / 76. * ((float)sqrt(5.) + 9.) * ra;
static const float C3 = 9. / 76. * ((float)sqrt(5.)*5. + 7.) * ra;
static const float C4 = 1.5 * ra;

static const vec3d_t pentaVerts[] =
{
    { 0.0,  C0,  C1}, { 0.0,  C0, -C1}, { 0.0, -C0,  C1}, { 0.0, -C0, -C1},
    {  C1, 0.0,  C0}, {  C1, 0.0, -C0}, { -C1, 0.0,  C0}, { -C1, 0.0, -C0},
    {  C0,  C1, 0.0}, {  C0, -C1, 0.0}, { -C0,  C1, 0.0}, { -C0, -C1, 0.0},
    {  C2, 0.0,  C3}, {  C2, 0.0, -C3}, { -C2, 0.0,  C3}, { -C2, 0.0, -C3},
    {  C3,  C2, 0.0}, {  C3, -C2, 0.0}, { -C3,  C2, 0.0}, { -C3, -C2, 0.0},
    { 0.0,  C3,  C2}, { 0.0,  C3, -C2}, { 0.0, -C3,  C2}, { 0.0, -C3, -C2},
    { C4, C4, C4}, { C4, C4, -C4}, { C4, -C4, C4}, { C4, -C4, -C4},
    {-C4, C4, C4}, {-C4, C4, -C4}, {-C4, -C4, C4}, {-C4, -C4, -C4},
};

static const int pentaInds[][3] =
{
    { 12,  0,  2 }, { 12,  2, 26 }, { 12, 26,  4 }, { 12,  4, 24 },
    { 12, 24,  0 }, { 13,  3,  1 }, { 13,  1, 25 }, { 13, 25,  5 },
    { 13,  5, 27 }, { 13, 27,  3 }, { 14,  2,  0 }, { 14,  0, 28 },
    { 14, 28,  6 }, { 14,  6, 30 }, { 14, 30,  2 }, { 15,  1,  3 },

    { 15,  3, 31 }, { 15, 31,  7 }, { 15,  7, 29 }, { 15, 29,  1 },
    { 16,  4,  5 }, { 16,  5, 25 }, { 16, 25,  8 }, { 16,  8, 24 },
    { 16, 24,  4 }, { 17,  5,  4 }, { 17,  4, 26 }, { 17, 26,  9 },
    { 17,  9, 27 }, { 17, 27,  5 }, { 18,  7,  6 }, { 18,  6, 28 },

    { 18, 28, 10 }, { 18, 10, 29 }, { 18, 29,  7 }, { 19,  6,  7 },
    { 19,  7, 31 }, { 19, 31, 11 }, { 19, 11, 30 }, { 19, 30,  6 },
    { 20,  8, 10 }, { 20, 10, 28 }, { 20, 28,  0 }, { 20,  0, 24 },
    { 20, 24,  8 }, { 21, 10,  8 }, { 21,  8, 25 }, { 21, 25,  1 },

    { 21,  1, 29 }, { 21, 29, 10 }, { 22, 11,  9 }, { 22,  9, 26 },
    { 22, 26,  2 }, { 22,  2, 30 }, { 22, 30, 11 }, { 23,  9, 11 },
    { 23, 11, 31 }, { 23, 31,  3 }, { 23,  3, 27 }, { 23, 27,  9 },
};

//static const float DR = (float)sqrt( 1. + f*f ) - 1.; // adjuster for circumsphere (icos)
//static const float DR = 0.; // adjuster for circumsphere (penta, circ-scribed)
static const float DR = // adjuster for circumsphere (penta, mid-scribed)
        .75 * (2.*(float)sqrt(3.) - 1. - (float)sqrt(5.)) * ra;
}

// ==========================================================================

int addSphereVerts(Actor& a, vec3d_t p, float r)
{
  using namespace sphere;

  if( r == 0 ) return 0;

  // icosahedron
//  for(size_t i = 0; i < len(icosInds); ++i)
//    for(size_t j = 0; j < 3; ++j)
//    {
//      vec3 v = icosVerts[ icosInds[i][j] ];
//      a.verts.push_back( { v*r + p, v.normalized() } );
//    }

  // Pentakis Dodecahedron
  for(size_t i = 0; i < len(pentaInds); ++i)
    for(size_t j = 0; j < 3; ++j)
    {
      vec3d_t v = pentaVerts[ pentaInds[i][j] ];

      vec3d_scale( v, v, r );
      vec3d_sum( v, v, p );
      vec3d_t n = v;
      vec3d_normalize( n, n );
      
      a.verts.push_back( {
          {(GLfloat)v.x, (GLfloat)v.y, (GLfloat)v.z},
            {(GLfloat)n.x, (GLfloat)n.y, (GLfloat)n.z} } );
    }

  return len(pentaInds);
}

// --------------------------------------------------------------------------

void Actor::create()
{
  if( !verts.size() ) return;

  if( VBOid ) glDeleteBuffers(1, &VBOid);

  nVerts = verts.size();

  glGenBuffers(1, &VBOid);
  glBindBuffer(GL_ARRAY_BUFFER, VBOid);
  glBufferData(GL_ARRAY_BUFFER, nVerts * sizeof(Actor::Vert), verts.data(), GL_STATIC_DRAW);

  return;
}

void Actor::perform()
{
  if( !VBOid ) return;//throw "BAD VBO ID! (Unprepared actor cannot perform)";
  
  glBindBuffer(GL_ARRAY_BUFFER, VBOid);

  glEnableClientState(GL_NORMAL_ARRAY);
  glEnableClientState(GL_VERTEX_ARRAY);
//    glEnableClientState(GL_COLOR_ARRAY);  // colored vertices

  glVertexPointer(3, GL_FLOAT, sizeof(Actor::Vert), 0);  // stride is a PERIOD of data! Not the interval
  glNormalPointer(GL_FLOAT, sizeof(Actor::Vert), (void*)sizeof(Actor::vecGL));
//    glColorPointer(4, GL_FLOAT, 0, 0);  // colored vertices

  glDrawArrays(GL_TRIANGLES, 0, nVerts);
//    glDrawArrays(GL_LINES, 0, nVerts);
//    glDrawArrays(GL_POINTS, 0, nVerts); // TODO: add sceleton drawing

//    glDisableClientState(GL_COLOR_ARRAY);  // colored vertices
  glDisableClientState(GL_VERTEX_ARRAY); // do we need to swith this two
  glDisableClientState(GL_NORMAL_ARRAY); // on/off at each draw??

  glBindBuffer(GL_ARRAY_BUFFER, 0);  // do we need this?
}

Actor::~Actor()
{
  if( VBOid ) glDeleteBuffers(1, &VBOid);
}

// ==========================================================================

ForceChainEdge::ForceChainEdge(int i, int j, double x, int d)
{
  id1 = i; id2 = j; f = x; dist = d;
}

bool CompareForceChainEdge::operator()(ForceChainEdge& e1, ForceChainEdge& e2) { return e1.f < e2.f; }

Model::Model(wxWindow *parent, Vars *var, Data *cpdata,
             wxWindowID id, const wxPoint& pos, const wxSize& size,
             long style, const wxString& name)
    : wxGLCanvas(parent, id, 0, pos, size,
                 style | wxFULL_REPAINT_ON_RESIZE | wxWANTS_CHARS, name, wxNullPalette)
{
  cur_VBOid = 0;
  cur_body = -1;
  cur_clip = 0;
  force_chain_current = false;
  v = var;
  data = cpdata;
  context = new wxGLContext(this);

  //Connect interface responses
  this->Connect(wxEVT_PAINT, wxPaintEventHandler(Model::OnPaint));
  this->Connect(wxEVT_LEFT_DOWN, wxMouseEventHandler(Model::OnMouseDown));
  this->Connect(wxEVT_LEFT_UP, wxMouseEventHandler(Model::OnMouseUp));
  this->Connect(wxEVT_MOTION, wxMouseEventHandler(Model::OnMouseMove));
  this->Connect(wxEVT_KEY_DOWN, wxKeyEventHandler(Model::OnKeyPress));
}

/*! \brief Set up the openGL render context (all lighting/settings/etc)
 *
 * All openGL context intitialization is done here. Things like
 * lighting are enabled/disabled as needed in other functions, but all
 * other functions are expected to return the openGL state to the
 * values specified here if they modify it.
 */
bool Model::InitGL()
{
  SetCurrent(*context);

  glShadeModel(GL_SMOOTH);
  glEnable(GL_NORMALIZE);

  /* Enable global lighting and our directional light */
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  glEnable(GL_COLOR_MATERIAL);
  //  glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, 1);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_DEPTH_TEST);
  char** null_argv = NULL;
  int null_argc = 0;
  glutInit(&null_argc, null_argv);

  /*
  glEnable(GL_TEXTURE_2D);
  glGenTextures(1, &(hdisvis->logo_id));
  glBindTexture(GL_TEXTURE_2D, hdisvis->logo_id);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, hdisvis->logo_w, hdisvis->logo_h, 0, GL_RGBA, GL_UNSIGNED_BYTE, hdisvis->logo);
  */
  
  // -------
  
  addSphereVerts(sphereActor, {0,0,0}, 1);
  sphereActor.create();  

  return true;
}

/*! \brief Reset the projection matrix
 *
 * Sets the projection matrix using gluPerspective, with a near-z
 * clipping plane at 0.1 and a far-z clipping plane at 1000.

 * From Ben's email (09/11/2013):The near/far clipping planes are set
 * by gluPersepective in Model::SetProjection. In the past, there were
 * some problems with depth buffer precision, and particles bled in to
 * each other a little bit. Moving the far clipping plane further back
 * shouldn't cause too much of a problem as long as the near clipping
 * plane isn't too close to the camera (moving the near clipping plane
 * close to 0.0 hugely decreases depth buffer precision).

 */
void Model::SetProjection()
{
  int w, h;
  GetClientSize(&w, &h);
  glViewport(0, 0, (GLint) w, (GLint) h);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(15.0f, (GLfloat)w/h, 0.15, 150.0);
}

/*! \brief Set the modelview matrix
 *
 * Sets the modelview matrix to a translation moving the center of the bounding box to the origin,
 * and positioning and zooming the camera appropriately.
 */
void Model::SetModelview()
{
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  GLfloat pos0[] = {0, 0, 1, 0};
  glLightfv( GL_LIGHT0, GL_POSITION, pos0 );

  // Glfloat diffuse0[] = { 1, 1, 1, 1 };
  // glLightfv( GL_LIGHT0, GL_DIFFUSE, diffuse0 );

  glTranslatef(v->trans.x, v->trans.y, v->trans.z);
  gluLookAt(v->eye.x*v->zoom, v->eye.y*v->zoom, v->eye.z * v->zoom,
            0, 0, 0,
            v->up.x, v->up.y, v->up.z);
  glTranslatef(-.5*data->box.x, -.5*data->box.y, -.5*data->box.z);
}

/*! \brief Event handler for the GLCanvas paint events, only calls Render()
 *
 * This function exists to catch Paint events triggered by the model, but doesn't do any rendering itself.
 * All rendering responsibility is passed to Model::Render(), which is callable from other locations without
 * having to construct a dummy paint event.
 */
void Model::OnPaint(wxPaintEvent& WXUNUSED(event))
{
  Render();
}

/*! \brief Render the model using current settings and data
 *
 * Checks which toggleable displays need drawing (clip
 * plane/springs/ptriangles), and which display mode is currently
 * active, and then calls the appropriate rendering functions.
 */
void Model::Render()
{
  clock_t start = clock();

  /* only render if visible and there is data loaded */
  if (data->initialized && GetParent()->IsShown())
  {
    // must always be here
    SetCurrent(*context);
    wxClientDC dc(this);

    SetProjection();//TODO(Anton): make projection depends on box
    SetModelview();

    // Clear
    glClearColor( 1.0f, 1.0f, 1.0f, 1.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    if (v->axis_display) DrawAxisDisplay();
    if (v->box)          DrawBox();
    if (v->time_display) DrawTime();

    if (v->displaymode == DISPLAYMODE_SPEED)
    {
      DrawSpeed();
    }
    else if (v->displaymode == DISPLAYMODE_FC)
    {
      if (force_chain_current == false) ComputeForceChain(cur_body);
      DrawForceChain();
    }
    // let us try to draw atoms regardless of the situation, same as spheres.
    //    else
    {
      DrawHdisvisAtoms();
    }

    if (v->springs)    DrawSprings();
    if (v->ptriangles) DrawPTriangles();
    if (v->clip)       DrawClipPlanes();
    if (v->logo)       DrawLogo();
    
    if (data->nSph)   DrawSpheres(); // Draw SPH particles
      
    glFlush();
    SwapBuffers();
  }
  clock_t end = clock();
  LogTime(_("render"), start, end);
}

/*! \brief Set glColor4f to a color along a blue->green->yellow->orange->red spectrum
 * \param v Distance along the spectrum (0 = blue, 1 = red)
 * \param alpha Alpha value
 *
 * The color that is set is computed with a linear piecewise function that maps [0,1] to the familar color
 * spectrum from blue to red. All values outside of the expected range are truncated.
 */
void Model::SetSpectrumColor(float v, float alpha)
{
  float r = max(min(-2.0 + 4.0*v, 1.0), 0.0);
  float g = max(min(-4*abs(v - 0.5) + 2, 1.0), 0.0);
  float b = max(min(2.0 + -4.0*v, 1.0), 0.0);
  glColor4f(r, g, b, alpha);
}

/*! \brief Draws the hdisvis logo in the lower right corner
 *
 * Renders the hdisvis logo as a textured square in an ortho projection.
 */
void Model::DrawLogo()
{
  if (hdisvis->logo_w == 0 || hdisvis->logo_h == 0) return;

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  gluOrtho2D(0, v->model_w, 0, v->model_h);

  glDisable(GL_DEPTH_TEST);
  glDisable(GL_LIGHTING);
  //glBindTexture(GL_TEXTURE_2D, hdisvis->logo_id);
  
  int border = 5;
  glRasterPos2f(v->model_w - hdisvis->logo_w - border, border);
  glDrawPixels(hdisvis->logo_w, hdisvis->logo_h, GL_RGBA, GL_UNSIGNED_BYTE, hdisvis->logo);

  /*
  double scale = sqrt(v->model_w*v->model_h);
  glColor4f(1, 1, 1, 1);
  glBegin(GL_QUADS);
  glTexCoord2f(1.0, 0.0);
  glVertex2f(0.0, 0.0);

  glTexCoord2f(1.0, 1.0);
  glVertex2f(0.0, scale/16.0);

  glTexCoord2f(0.0, 1.0);
  glVertex2f(scale/8.0, scale/16.0);

  glTexCoord2f(0.0, 0.0);
  glVertex2f(scale/8.0, 0.0);
  glEnd();
  */

  glEnable(GL_DEPTH_TEST);
  glEnable(GL_LIGHTING);

  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
}

/*! \brief Draws a disaply of the X/Y/Z axis
 *
 * Draws a small display of the the X/Y/Z axis in the lower left corner to make it easier to see what the current
 * rotations are.
 */
void Model::DrawAxisDisplay()
{ 
  glMatrixMode(GL_MODELVIEW); // should already be the current matrix mode, but to prevent weird bugs if that changes
  glPushMatrix();
  glLoadIdentity();
  gluLookAt(v->eye.x, v->eye.y, v->eye.z, // ignore zoom, unlike SetModelview()
            0, 0, 0,
            v->up.x, v->up.y, v->up.z);

  GLint viewport[4];
  GLdouble modelview[16];
  GLdouble projection[16];

  glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
  glGetDoublev(GL_PROJECTION_MATRIX, projection);
  glGetIntegerv(GL_VIEWPORT, viewport);

  // in order to always render the display at a fixed pixel position with fixed pixel size, we want to be using 
  // screen coordinates - so project all position we're interested in
  float size = 0.025;
  vec3d_t O, X, Y, Z, U;
  gluProject(v->over.x*size, v->over.y*size, v->over.z*size, // a vector that doesn't rotate relative to the camera,
             modelview, projection, viewport,                // used to establish the maximum length of an axis vec
             &U.x, &U.y, &U.z);
  gluProject(0, 0, 0,    modelview, projection, viewport, &O.x, &O.y, &O.z); // Origin
  gluProject(size, 0, 0, modelview, projection, viewport, &X.x, &X.y, &X.z);
  gluProject(0, size, 0, modelview, projection, viewport, &Y.x, &Y.y, &Y.z);
  gluProject(0, 0, size, modelview, projection, viewport, &Z.x, &Z.y, &Z.z);

  glLoadIdentity();
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  gluOrtho2D(0, v->model_w, 0, v->model_h);

  // drop the depth value and get pixel length width
  vec3d_set(U, U.x - O.x, U.y - O.y, 0);
  vec3d_set(X, X.x - O.x, X.y - O.y, 0);
  vec3d_set(Y, Y.x - O.x, Y.y - O.y, 0);
  vec3d_set(Z, Z.x - O.x, Z.y - O.y, 0);

  // make sure the lines have length independent of model size/zoom
  float len_U = sqrt(U.x*U.x + U.y*U.y);
  float max_size = 45; // pixels
  vec3d_scale(X, X, max_size/len_U);
  vec3d_scale(Y, Y, max_size/len_U);
  vec3d_scale(Z, Z, max_size/len_U);

  float base = max_size + 20; // pixel border around edge

  glDisable(GL_LIGHTING);
  glColor3f(0, 0, 0);
  glLineWidth(2);
  glBegin(GL_LINES);

  glVertex2f(base,       base);
  glVertex2f(base + X.x, base + X.y);

  glVertex2f(base,       base);
  glVertex2f(base + Y.x, base + Y.y);

  glVertex2f(base,       base);
  glVertex2f(base + Z.x, base + Z.y);

  glEnd();

  // now to add the text
  vec3d_t norm_X = get_vec_normalize(X);
  vec3d_t norm_Y = get_vec_normalize(Y);
  vec3d_t norm_Z = get_vec_normalize(Z);

  vec3d_t text = get_vec(8, 13, 0);
  
  glRasterPos2f(base + X.x + text.x*norm_X.x - 4, base + X.y + text.y*norm_X.y - 4);
  glutBitmapCharacter(GLUT_BITMAP_8_BY_13, wxUniChar('X'));

  glRasterPos2f(base + Y.x + text.x*norm_Y.x - 4, base + Y.y + text.y*norm_Y.y - 4);
  glutBitmapCharacter(GLUT_BITMAP_8_BY_13, wxUniChar('Y'));

  glRasterPos2f(base + Z.x + text.x*norm_Z.x - 4, base + Z.y + text.y*norm_Z.y - 4);
  glutBitmapCharacter(GLUT_BITMAP_8_BY_13, wxUniChar('Z'));

  // and finall restore everything we've messed with
  glLineWidth(1);
  glEnable(GL_LIGHTING);

  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
}

/*! \brief Render the current simulation time
 *
 * Renders Data::cur_time as a string of GLUT_BITMAP_8_BY_13 chars in the upper left corner of the model.
 */
void Model::DrawTime()
{
  glDisable(GL_LIGHTING);

  /* Clear the modelview matrix */
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();
  
  /* Set up a fresh ortho projection */
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  glOrtho(0, v->model_w, v->model_h, 0, 0, 1);

  wxString time_str = _F(_("Time: %.3fs"), data->cur_time);
  glColor3f(0, 0, 0);
  /* Draw string char by char */
  glRasterPos2f(10, 17);
  for (unsigned int i = 0; i < time_str.Len(); ++i)
    glutBitmapCharacter(GLUT_BITMAP_8_BY_13, time_str[i]);

  /* Restore openGL state */
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
  glEnable(GL_LIGHTING);
}

/*! \brief Render the bounding box
 *
 * Draws a simple black wireframe box.
 */
void Model::DrawBox()
{
  glColor4f(0, 0, 0, 1);
  glNormal3f(v->eye.x, v->eye.y, v->eye.z);
  
  glLineWidth(2);
  glBegin(GL_LINE_LOOP);
  glVertex3f(          0,           0,           0);
  glVertex3f(data->box.x,           0,           0);
  glVertex3f(data->box.x, data->box.y,           0);
  glVertex3f(          0, data->box.y,           0);
  glEnd();

  glBegin(GL_LINE_LOOP);
  glVertex3f(          0,           0, data->box.z);
  glVertex3f(data->box.x,           0, data->box.z);
  glVertex3f(data->box.x, data->box.y, data->box.z);
  glVertex3f(          0, data->box.y, data->box.z);
  glEnd();

  glBegin(GL_LINES);
  glVertex3f(          0,           0,           0);
  glVertex3f(          0,           0, data->box.z);
  glVertex3f(data->box.x,           0,           0);
  glVertex3f(data->box.x,           0, data->box.z);
  glVertex3f(data->box.x, data->box.y,           0);
  glVertex3f(data->box.x, data->box.y, data->box.z);
  glVertex3f(          0, data->box.y,           0);
  glVertex3f(          0, data->box.y, data->box.z);
  glEnd();
  glLineWidth(1);
}

/*! \brief Render all atoms
 *
 * Renders transparent atoms only after opaque atoms. This ensures
 * correct transparent-opaque interactions.  Transparent-transparent
 * interactions are still imperfect, but to compute them correctly
 * would require the atoms to be sorted by distance from the camera,
 * which would be very computationally expensive.
 */
void Model::DrawHdisvisAtoms(bool force_color)
{
  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_NORMAL_ARRAY);
  cur_VBOid = 0;
  
  GLint viewport[4];
  GLdouble modelview[16];
  GLdouble projection[16];
  glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
  glGetDoublev(GL_PROJECTION_MATRIX, projection);
  glGetIntegerv(GL_VIEWPORT, viewport);
  vec3d_t screen_pos = get_vec(0, 0, 0);
  
  // vector for collecting all of the partially transparent atoms
  // (z-depth, index)
  vector< pair<double, unsigned int> > transparent;
  
  for (unsigned int i = 0; i < data->nPs; ++i)
    {
    // if the atom is partially transparent, we save it to be rendered
    // after the opaque atoms
    if (data->group_atom_colors[data->atom_groups[i]].a < 1)
    {
			
      // if we want to sort the atoms by depth for accurate
      // transparency interactions, then we need to project it in to
      // screen space and read the depth component
      if (v->sort_atoms)
        {
          gluProject(data->prim_points[i*3].x, 
                     data->prim_points[i*3].y, 
                     data->prim_points[i*3].z,
                     modelview, projection, viewport,
                     &screen_pos.x, &screen_pos.y, &screen_pos.z);
        }
      transparent.push_back(make_pair(-screen_pos.z, i));
    }
    // if the atom is totally opaque we can render it right away
    else
      {
        DrawHdisvisAtom(i, force_color);
      }
  }

	// if we want to sort the atoms, go ahead and do it
  if (v->sort_atoms) sort(transparent.begin(), transparent.end());

  glDepthMask(GL_FALSE);
  for (unsigned int i = 0; i < transparent.size(); ++i)
  {
    DrawHdisvisAtom(transparent[i].second, force_color);
  }
  glDepthMask(GL_TRUE);

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glDisableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_NORMAL_ARRAY);
}


/*! 
 * \brief Render a single atom with the method set by Vars::rendermode
 *
 * \param i Lookup index of the atom
 *
 * All atom vertex data is always stored in a vbo. However, if the
 * rendermode is set to Vars::RENDERMODE_GLU or is set to
 * Vars::RENDERMODE_MIX and the dilation radius is large enough,
 * spheres and cylinders will be rendered in immediate mode with glu
 * function calls that are high detail but very slow.
 */
void Model::DrawHdisvisAtom(int i, bool force_color)
{
  if (v->clip && Clipped(data->prim_points[data->prim_ipoints[3*i]])) return;

  /* short circuit when totally transparent so we don't mess up the
     depth buffer */
  if (data->group_atom_colors[data->atom_groups[i]].a == 0) return;
  
  // we force the coloring when we are doing click hit detection with
  // unique colors
  if (force_color)
  {
    SetUniqueBodyColor(data->prim_bodies[i]);
  }
	// if we are doing a normal render, figure out what color the body should be
  else
    {
      if ((int)(data->prim_bodies[i]) == cur_body)
        {
          glColor4f(0.7, 0.7, 0.7, 1.0);
        }
      // check if this atom's group is set to be colored by force
      else if (data->atom_group_color_by_force[data->atom_groups[i]])
        {
          if (vec3d_abs(data->body_forces[data->prim_bodies[i]]) < v->force_min)
            return;
          
          SetSpectrumColor( vec3d_abs(data->body_forces[data->prim_bodies[i]])/
                            (v->force_max - v->force_min),
                            data->group_atom_colors[data->atom_groups[i]].a);
        }
      // otherwise just use that group's color blended with the body's individual color
      else
        {
          Color group_color = data->group_atom_colors[data->atom_groups[i]];
          vec3d_t body_color = data->body_colors[data->prim_bodies[i]];
          glColor4f(group_color.r*(1 - group_color.r_var) + body_color.x*(group_color.r_var),
                    group_color.g*(1 - group_color.g_var) + body_color.y*(group_color.g_var),
                    group_color.b*(1 - group_color.b_var) + body_color.z*(group_color.b_var),
                    group_color.a);
        }
    }
  
  // for the mixed render mode, we need to figure out the pixel size
  // of the dilation radius.  if the dilation radius appears big
  // enough (large spheres and cylinders), then we will use the slower
  // glu calls instead of rendering with triangles from the VBO
  //
  // WARNING: these function calls do a lot of math, and are sort of
  // slow if you have lots of primitives
  bool use_glu = false;
  if (v->rendermode == RENDERMODE_MIX)
  {
    GLint viewport[4];
    GLdouble modelview[16];
    GLdouble projection[16];

    glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
    glGetDoublev(GL_PROJECTION_MATRIX, projection);
    glGetIntegerv(GL_VIEWPORT, viewport);

    vec3d_t world_center, world_offset, screen_center, screen_offset;

		// world_center is the first point of the atom
    vec3d_assign(world_center, data->prim_points[3*i + 0]);
		// and then we move up from it a distance of the atom's dilation radius
    vec3d_sum(world_offset, world_center, get_vec_scale(v->up, data->prim_r[i]));

		// project both points in to screen space
    gluProject(world_center.x, world_center.y, world_center.z, modelview, projection, viewport,
               &screen_center.x, &screen_center.y, &screen_center.z);
    gluProject(world_offset.x, world_offset.y, world_offset.z, modelview, projection, viewport,
               &screen_offset.x, &screen_offset.y, &screen_offset.z);

		// and see how tall it is
    use_glu = screen_offset.y - screen_center.y > v->render_switch;
  }

  // draw with glu (slow!!!)
  if ( (data->prim_npoints[i] != 3) && (v->rendermode == RENDERMODE_GLU || use_glu))
  {
    GLUquadric* pquad = gluNewQuadric();
    switch (data->prim_npoints[i])
    {
      case 1: DrawSphere(data->prim_r[i], pquad, data->prim_points[data->prim_ipoints[3*i + 0]]);
							break;
      case 2: DrawCylinder(data->prim_r[i], pquad, data->prim_points[data->prim_ipoints[3*i + 0]],
                           data->prim_points[data->prim_ipoints[3*i + 1]]);
        break;
      default: wxLogMessage(_("DrawHdisvisAtom, unhandled glu draw")); break;
    }
  }

  // draw with vbo (fast!!!)
  else
  {
    if (cur_VBOid != data->VBOids[data->prim_VBOs[i]])
    {
      cur_VBOid = data->VBOids[data->prim_VBOs[i]];
      glBindBuffer(GL_ARRAY_BUFFER, cur_VBOid);

      glNormalPointer(GL_FLOAT, sizeof(vertex_t), (char*)NULL + 3*sizeof(GLfloat));
      glVertexPointer(3, GL_FLOAT, sizeof(vertex_t), NULL);
    }

    glDrawArrays(GL_TRIANGLES, data->prim_iverts[i], data->prim_nverts[i]);
  }
}

void Model::DrawContacts()
{
  glLineWidth(1);
  glBegin(GL_LINES);
  for (unsigned int b = 0; b < data->nBs; ++b)
  {
    for (unsigned int c = 0; c < data->interbody_forces[b].size(); ++c)
    {
      // each contact appears twice in the adjacency list - only draw line for one of them
      if (b < data->interbody_forces[b][c].first)
      {
        SetSpectrumColor(data->interbody_forces[b][c].second/v->force_max,
            data->interbody_forces[b][c].second >= v->force_min);
        vec3d_t b1 = data->body_centers[b];
        vec3d_t b2 = data->body_centers[data->interbody_forces[b][c].first];
        if (v->clip && (Clipped(b1) || Clipped(b2))) continue;
        glVertex3f(b1.x, b1.y, b1.z);
        glVertex3f(b2.x, b2.y, b2.z);
      }
    }
  }
  glEnd();
}

/*! \brief Render the force chains as lines between bodies
 * \return
 */
void Model::DrawForceChain()
{
  glLineWidth(1);
  glBegin(GL_LINES);
  for (list<ForceChainEdge>::iterator e = force_chain.begin(); e != force_chain.end(); ++e)
  {
    vec3d_t b1 = data->body_centers[(*e).id1];
    vec3d_t b2 = data->body_centers[(*e).id2];
    if (v->clip && (Clipped(b1) || Clipped(b2))) continue;
    SetSpectrumColor((*e).f/v->force_max, pow(0.85, (double)(*e).dist));
    glVertex3f(b1.x, b1.y, b1.z);
    glVertex3f(b2.x, b2.y, b2.z);
  }
  glEnd();
}

void Model::ComputeForceChain(int n)
{
  if (n < 0) return; // invalid body selected
  if (data->contacts_current == false) data->ReadContacts();

  force_chain.clear();
  bool visited[data->nBs];
  for (unsigned int b = 0; b < data->nBs; ++b) visited[b] = 0;

  deque<ForceChainEdge> sq;
  // seed the search queue with the selected body's connections
  visited[n] = true;
  sq.push_back(ForceChainEdge(-1, n, 0, 0));
  while (sq.empty() == false)
  {
    ForceChainEdge cur = sq.front();
    sq.pop_front();
    force_chain.push_back(cur);
    int new_dist = cur.dist + 1;
    for (unsigned int e = 0; e < data->interbody_forces[cur.id2].size(); ++e)
    {
      int new_id = data->interbody_forces[cur.id2][e].first;
      double new_f = data->interbody_forces[cur.id2][e].second;
      if (visited[new_id] == false && new_f >= v->force_min)
      {
        visited[new_id] = true;
        sq.push_back(ForceChainEdge(cur.id2, new_id, new_f, new_dist));
      }
    }
  }
  force_chain.pop_front(); // this was the bogus first entry we seeded with
  wxLogMessage(_("ComputeForceChain: length %zu"), force_chain.size());
  force_chain_current = true;
}

/*! \brief Render the velocity vector for each body
 * \return
 */
void Model::DrawSpeed()
{
  vec3d_t base, offset, vertex;
  glLineWidth(v->speed_width);
  glNormal3f(v->eye.x, v->eye.y, v->eye.z);
  glBegin(GL_LINES);
  for (unsigned int i = 0; i < data->nBs; ++i)
  {
    if (v->clip && Clipped(data->body_centers[i])) continue;
      
    SetSpectrumColor(vec3d_abs(data->body_speeds[i])/v->speed_max, 1);

    vec3d_assign(base, data->body_centers[i]);
    glVertex3f(base.x, base.y, base.z);

    vec3d_assign(offset, data->body_speeds[i]);
    vec3d_scale(offset, offset, v->speed_vec_size);
    vec3d_sum(vertex, base, offset);
    glVertex3f(vertex.x, vertex.y, vertex.z);

    vec3d_x(offset, data->body_speeds[i], v->eye);
    vec3d_normalize(offset, offset);
    vec3d_scale(offset, offset, v->speed_base_size);
    vec3d_sum(vertex, base, offset);
    glVertex3f(vertex.x, vertex.y, vertex.z);

    vec3d_scale(offset, offset, -1);
    vec3d_sum(vertex, base, offset);
    glVertex3f(vertex.x, vertex.y, vertex.z);
  }

  for(unsigned int i = 0; i < data->nSph; ++i)
    {
      vec3d_t& _v = data->spheres[i].v;
      vec3d_t& _c = data->spheres[i].c;

      if (v->clip && Clipped(_c) ) continue;

      SetSpectrumColor(vec3d_abs(_v)/v->speed_max, 1);
      
      vec3d_assign( base, _c );
      glVertex3f( base.x, base.y, base.z );
      
      vec3d_assign( offset, _v );
      vec3d_scale( offset, offset, v->speed_vec_size);
      vec3d_sum( vertex, base, offset );
      glVertex3f( vertex.x, vertex.y, vertex.z );
      
      vec3d_x( offset, _v, v->eye);
      vec3d_normalize( offset, offset );
      vec3d_scale( offset, offset, v->speed_base_size );
      vec3d_sum( vertex, base, offset );
      glVertex3f(vertex.x, vertex.y, vertex.z);
      
      vec3d_scale(offset, offset, -1);
      vec3d_sum(vertex, base, offset);
      glVertex3f(vertex.x, vertex.y, vertex.z);
    }
  
  glEnd();
  glLineWidth(1);
}


/*! \brief Render all springs in immediate mode
 * \return
 */
void Model::DrawSprings()
{
  glLineWidth(v->spring_width);
  glNormal3f(v->eye.x, v->eye.y, v->eye.z);
  glBegin(GL_LINES);
  for (unsigned int i = 0; i < data->nSs; ++i) 
  {
    if (v->clip && Clipped(get_vec(data->spring_verts[2*i].vx,
                                   data->spring_verts[2*i].vy,
                                   data->spring_verts[2*i].vz))) continue;
    
    Color color = data->group_spring_colors[data->spring_groups[i]];
    glColor4f(color.r, color.g, color.b, color.a);

    glVertex3f(data->spring_verts[2*i + 0].vx, data->spring_verts[2*i + 0].vy, data->spring_verts[2*i + 0].vz);
    glVertex3f(data->spring_verts[2*i + 1].vx, data->spring_verts[2*i + 1].vy, data->spring_verts[2*i + 1].vz);
  }
  glEnd();
  glLineWidth(1);
}

/*! \brief Render all ptriangles in immediate mode
 * \return
 */
void Model::DrawPTriangles()
{
  glBegin(GL_TRIANGLES);
  for (unsigned int i = 0; i < data->nTs; ++i )
  {
    if (v->clip && Clipped(get_vec(data->ptriangle_verts[3*i].vx,
                                   data->ptriangle_verts[3*i].vy,
                                   data->ptriangle_verts[3*i].vz))) continue;
  
    Color color = data->group_ptriangle_colors[data->ptriangle_groups[i]];
    glColor4f(color.r, color.g, color.b, color.a);

    glNormal3f(data->ptriangle_verts[3*i].nx, data->ptriangle_verts[3*i].ny, data->ptriangle_verts[3*i].nz);

    glVertex3f(data->ptriangle_verts[3*i + 0].vx, data->ptriangle_verts[3*i + 0].vy, data->ptriangle_verts[3*i + 0].vz);
    glVertex3f(data->ptriangle_verts[3*i + 1].vx, data->ptriangle_verts[3*i + 1].vy, data->ptriangle_verts[3*i + 1].vz);
    glVertex3f(data->ptriangle_verts[3*i + 2].vx, data->ptriangle_verts[3*i + 2].vy, data->ptriangle_verts[3*i + 2].vz);
  }
  glEnd();
}


/*! \brief Use gluSphere to render a sphere
 * \param r Dilation radius
 * \param pquad Unused quadric that is required by gluSphere
 * \param p0 Center of the sphere
 */
void Model::DrawSphere(float r, GLUquadric* pquad, vec3d_t p0)
{
  glPushMatrix();
  glTranslatef(p0.x, p0.y, p0.z);
  gluSphere(pquad, r, 10, 10);
  glPopMatrix();
}

/*! \brief Use gluCylinder to render a cylinder
 * \param r Dilation radius
 * \param pquad Unused quadric that is required by gluCylinder
 * \param p0 Center of bottom circle
 * \param p1 Center of top circle
 */
void Model::DrawCylinder(float r, GLUquadric* pquad, vec3d_t p0, vec3d_t p1)
{
  vec3d_t a = get_vec_diff(p1, p0);
  float base = vec3d_abs(a);

  glPushMatrix();
  glTranslatef(p0.x, p0.y, p0.z);
  glRotatef(180*acos(a.z / base)/M_PI, -a.y, a.x, 0);
  gluCylinder(pquad, r, r, base, 10, 10);
  glPopMatrix();
}

/*! \brief render spheres representing soft particles
 */
void Model::DrawSpheres()
{
  vec3d_t eye; 
  vec3d_scale( eye, v->eye, v->zoom );
  vec3d_diff( eye, eye, v->trans );
  
  vector< pair<double, unsigned int> > inds{};
  
  for(unsigned int i = 0; i < data->nSph; ++i)
    {
      vec3d_t& v = data->spheres[i].c;
      
      vec3d_t dv;
      vec3d_diff( dv, v, eye );
      double d = vec3d_abs( dv );
      
      inds.push_back({d, i});
    }

  std::sort(inds.rbegin(), inds.rend());

  // -----
  
  glMatrixMode(GL_MODELVIEW);
//  GLdouble modelview[16], scalem[16];
//  glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
  
  glCullFace(GL_BACK);
  glEnable(GL_CULL_FACE);

  glEnable(GL_DEPTH_TEST);
  glDepthMask(GL_FALSE);

  for(unsigned int i = 0; i < inds.size(); ++i)   //// SPH_LABEL
    {
      pair<double, unsigned int>& I = inds[i];
      double r = data->spheres[I.second].h;
      vec3d_t pos = data->spheres[I.second].c;
      int col_id = data->spheres[I.second].group_id;
      int is_by_force = data->atom_group_color_by_force[col_id]; // 1 if plotting by force

      if (v->clip && Clipped(pos))  continue;
                        
      glPushMatrix();     
      
//      glLoadIdentity();
      glTranslatef( pos.x, pos.y, pos.z ); ////////////////////////////
      glScaled( r, r, r );

      Color group_color;
      group_color = data->group_atom_colors[col_id];

      if ( is_by_force )
        {
          real p = data->spheres[I.second].p; // pressure
          // if ( p < v->force_min) return;
          
          SetSpectrumColor( p / (v->force_max - v->force_min), group_color.a );
        }
      else
        {
          glColor4f( group_color.r, group_color.g, group_color.b, group_color.a );
        }

// // Added on Dima`s and removed on Anton`s requests
//      { // point in the center of each sphere
//        glEnable(GL_POINT_SMOOTH);
//        //glColor4f(0,0,0,.7);
//        glPointSize(3.);
//        glBegin(GL_POINTS);
//        glVertex3f(0.,0.,0.);
//        glEnd();
//      }

      sphereActor.perform();
      
      glPopMatrix();      
    }
  glDepthMask(GL_TRUE);

}

/* -----------------------------------------------------*/

          /* MOUSE/KEYBOARD INPUT */

/* -----------------------------------------------------*/

void Model::OnMouseDown(wxMouseEvent &event)
{
  //Allow other event handlers to continue while this one is processed
  event.Skip();
  /* Necessary to manually set focus otherwise the focus is lost and keypresses
   * are not captured correctly */
  SetFocus();
  v->mouseX = event.GetX();
  v->mouseY = event.GetY();

  if (v->wants_click)
  {
		int old_body = cur_body;
    cur_body = GetBody(v->mouseX, GetSize().GetHeight() - v->mouseY);
    if (cur_body >= 0) // successfully clicked a body (-1 indicates a miss)
    {
      wxLogMessage(_("Selected body %d at position (%.3f, %.3f, %.3f)"),
                   cur_body, data->body_centers[cur_body].x, data->body_centers[cur_body].y, data->body_centers[cur_body].z);
      v->wants_click = false;
      //ComputeForceChain(cur_body);
      Render(); // Need to redraw so the newly selected body is colored right

      if (old_body >= 0)
        wxLogMessage(_("Dist to previous body: %.4f"),
                     vec3d_absdiff(data->body_centers[cur_body], data->body_centers[old_body]));
    }
  }

  if (v->arrowmode == ARROWMODE_CLIP)
  {
    int clip_id = KnobIsClicked(v->mouseX, GetSize().GetHeight() - v->mouseY);
    if (clip_id >=0)
    {
      cur_clip = clip_id;
      v->dragging_knob = true;
    }
    else
    {
      // didn't click a knob - drag the model instead
      v->dragging = true;
    }
  }
  else
  {
    v->dragging = true;
  }
}

void Model::OnMouseUp(wxMouseEvent &event)
{
  //Allow other event handlers to continue while this one is processed
  event.Skip();
  v->dragging = false;
  v->dragging_knob = false;
}

void Model::OnMouseMove(wxMouseEvent &event)
{
  //Allow other event handlers to continue while this one is processed
  event.Skip();

  if (v->dragging)
  {
    double dx = event.GetX() - v->mouseX;
    double dy = event.GetY() - v->mouseY;

    vec3d_t change;
    vec3d_set(change, 0, 0, 0);

    dx = dx == 0 ? 0 : 0.2/(1 + pow(2.5, -0.5*abs(dx) + 5))*abs(dx)/dx;
    dy = dy == 0 ? 0 : 0.2/(1 + pow(2.5, -0.5*abs(dy) + 5))*abs(dy)/dy;

    vec3d_scale(change, v->eye, dx);
    vec3d_sum(v->over, v->over, change);
    vec3d_x(v->eye, v->over, v->up);

    vec3d_scale(change, v->up, dy);
    vec3d_sum(v->eye, v->eye, change);
    vec3d_x(v->up, v->eye, v->over);

    vec3d_normalize(v->eye, v->eye);
    vec3d_normalize(v->up, v->up);
    vec3d_normalize(v->over, v->over);

    Render();
  }

  if (v->dragging_knob)
  {
    // project knob position into screen coordinates in order to get the correct depth -
    //   if we rely on reading the pixel depth from the depth buffer, everything goes to
    //   hell as soon as the cursor slips off the knob

    GLint viewport[4];
    GLdouble modelview[16];
    GLdouble projection[16];

    vec3d_t screen_knob, screen_click, world_knob, world_click;
 
    glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
    glGetDoublev(GL_PROJECTION_MATRIX, projection);
    glGetIntegerv(GL_VIEWPORT, viewport);

    vec3d_sum(world_knob, v->clip_pos[cur_clip], get_vec_scale(v->clip_up[cur_clip], CLIP_KNOB_SIZE));

    // push the depth component a little closer to the camera to prevent weird behavior
    //   when clip_up is in the plane of the screen (degenerate triangle issues)
    vec3d_sum(world_knob, world_knob, get_vec_scale(v->eye, 0.01));

    gluProject(world_knob.x,
               world_knob.y,
               world_knob.z,
               modelview, projection, viewport,
               &screen_knob.x,
               &screen_knob.y,
               &screen_knob.z);

    vec3d_set(screen_click, event.GetX(), GetSize().GetHeight() - event.GetY(), screen_knob.z);

    gluUnProject(screen_click.x,
                 screen_click.y,
                 screen_click.z,
                 modelview, projection, viewport,
                 &world_click.x,
                 &world_click.y,
                 &world_click.z);

    vec3d_diff(v->clip_up[cur_clip], world_click, v->clip_pos[cur_clip]);
    vec3d_normalize(v->clip_up[cur_clip], v->clip_up[cur_clip]);

    Render();
  }

  v->mouseX = event.GetX();
  v->mouseY = event.GetY();
}

void Model::OnKeyPress(wxKeyEvent &event)
{
  //event.Skip(); // if this event is skipped then focus will be passed down to the file control
  if (data->initialized)
  {
    int key = event.GetKeyCode();
    switch(key)
    {
      case WXK_UP: case WXK_DOWN: case WXK_LEFT: case WXK_RIGHT: OnArrowPress(event); break;
      case 'A': hdisvis->OnArrowmode((v->arrowmode + 1)%3); break;
      case 'D': hdisvis->OnDisplaymode((v->displaymode + 1)%4); break;
      case 'N': hdisvis->LoadFile(v->file + 1); break;
      case 'P': hdisvis->LoadFile(v->file - 1); break;
      case 'Z': hdisvis->OnAlign(); break;
      case 'X': hdisvis->OnResettrans(); break;
      case 'B': hdisvis->OnBoxToggle(!v->box); break;
      case 'L': v->logo = !v->logo; Render(); break;
      case 'V': hdisvis->OnAxisDisplayToggle(!v->axis_display); break;
      case 'C': hdisvis->CaptureFrame(); break;
      case 'H': wxMessageBox(_(MODEL_HELP_STRING), _("Hotkeys")); break;
      case 'F': v->wants_click = true; break;
      case 'Y': v->playing = true; hdisvis->Play(); break;
      case 'S': v->sort_atoms = !v->sort_atoms;
                wxLogMessage(_("Sort atoms: %d"), (int)v->sort_atoms);
                Render();
                break;
			case 'I': v->time_display = !v->time_display; Render(); break;
      case WXK_ESCAPE: v->playing = false; break;
      default: break;

    }
  }
  SetFocus();
}

void Model::OnArrowPress(wxKeyEvent &event)
{
  int key = event.GetKeyCode();
  switch(v->arrowmode)
  {
    case ARROWMODE_ROTATE:
      switch(key)
      {
        /* Rotate the orthonormal basis of the camera */
        case WXK_UP:    rotate_ortho(&v->eye, &v->up, &v->over,  v->rot_speed); break;
        case WXK_DOWN:  rotate_ortho(&v->eye, &v->up, &v->over, -v->rot_speed); break;
        case WXK_LEFT:  rotate_ortho(&v->over, &v->eye, &v->up,  v->rot_speed); break;
        case WXK_RIGHT: rotate_ortho(&v->over, &v->eye, &v->up, -v->rot_speed); break;
        default: break;
      }
      break;
    case ARROWMODE_TRANSLATE:
      switch(key)
      {
        case WXK_LEFT:  v->trans.x = v->trans.x - v->trans_speed; break;
        case WXK_RIGHT: v->trans.x = v->trans.x + v->trans_speed; break;
        case WXK_UP:    v->trans.y = v->trans.y + v->trans_speed; break;
        case WXK_DOWN:  v->trans.y = v->trans.y - v->trans_speed; break;
        default: break;
      }
      break;
    case ARROWMODE_CLIP:
        switch(key)
        {
          case WXK_UP:    vec3d_sum(v->clip_pos[cur_clip], v->clip_pos[cur_clip],
                              get_vec_scale(v->clip_up[cur_clip],  v->clip_speed));
                          AdjustClipPos(cur_clip);
                          break;
          case WXK_DOWN:  vec3d_sum(v->clip_pos[cur_clip], v->clip_pos[cur_clip],
                              get_vec_scale(v->clip_up[cur_clip], -v->clip_speed));
                          AdjustClipPos(cur_clip);
                          break;
          case WXK_LEFT:  if (v->clip_pos.size() > 1)
                          {
                            v->clip_pos.pop_back();
                            v->clip_up.pop_back();
                          }
                          break;
          case WXK_RIGHT: v->clip_pos.push_back(get_vec(data->box.x/2, data->box.y/2, data->box.z/2));
                          v->clip_up.push_back(get_vec(v->eye.x, v->eye.y, v->eye.z));
                          break;
          default: break;
        }
        break;
    default: break;
  }
  Render();
}
/* -----------------------------------------------------*/

          /* BODY SELECTION */

/* -----------------------------------------------------*/

/*! \brief Checks if a mouse click is on the clip plane rotation knob
 * \param x Click x coordinate (pixels)
 * \param y Click y coordinate (pixels)
 * \return True if the knob was clicked
 *
 * Checking if the mouse click is on the knob is done with color picking.
 * The back buffer is cleared, the knob is drawn on it, and then the pixel data at
 * the mouse position is checked to see if it's the same color as the knob.
 */
int Model::KnobIsClicked(int x, int y)
{
  SetProjection();
  SetModelview();

  // Clear
  glClearColor( 1.0f, 1.0f, 1.0f, 1.0f );
  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

  glDisable(GL_LIGHTING);
  for (unsigned int c = 0; c < v->clip_pos.size(); ++c)
    DrawClipControl(get_vec(c/256.0, 0, 0), c);
  glEnable(GL_LIGHTING);

  GLint viewport[4];
  GLubyte pixel[3];
  GLfloat depth;

  glGetIntegerv(GL_VIEWPORT , viewport);

  glReadPixels(x, y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, (void *)pixel);
  glReadPixels(x, y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &depth);

  if (pixel[1] == 255) return -1;
  return pixel[0];
}

void Model::SetUniqueBodyColor(int body_id)
{
  glColor4ub(body_id%256, body_id/256%256, body_id/256/256%256, 255);
}

int Model::GetBody(int x, int y)
{
  SetProjection();
  SetModelview();

  // Clear
  glClearColor( 1.0f, 1.0f, 1.0f, 1.0f );
  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

  glDisable(GL_LIGHTING);
  DrawHdisvisAtoms(true);
  glEnable(GL_LIGHTING);

  GLint viewport[4];
  GLubyte pixel[3];
  GLfloat depth;

  glGetIntegerv(GL_VIEWPORT , viewport);

  glReadPixels(x, y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, (void *)pixel);
  glReadPixels(x, y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &depth);

  unsigned int n = pixel[0]+256*pixel[1]+256*256*pixel[2];
  if (n < data->nPs) return n;
  else return -1; // didn't click on an atom
}

/* -----------------------------------------------------*/

          /* CLIPPING PLANE */

/* -----------------------------------------------------*/

bool Model::Clipped(vec3d_t pos)
{
  for (unsigned int c = 0; c < v->clip_pos.size(); ++c)
    if (vec3d_dot(v->clip_up[c], get_vec_diff(pos, v->clip_pos[c])) > 0) return true;
  return false;
}

/*! \brief Given two coordinates, finds the third coordinate where that line intersects the clip plane
 * \param p1 The first of the given coordinates
 * \param p2 The second of the given coordinates
 * \param i Which coordinate is missing ('x'/'y'/'z')
 * \return Point with coordinates p1 and p2 that lies on the clipping plane
 */
vec3d_t Model::FindIntersection(double p1, double p2, char i, int clip_plane)
{
  vec3d_t p;
  vec3d_set(p, 0, 0, 0);
  /* Solve for missing coord knowing v->clip_up*(p - v->clip_pos) = 0 */
  switch(i)
  {
    case 'x':
      vec3d_set(p, v->clip_pos[clip_plane].x -
                (v->clip_up[clip_plane].y*(p1 - v->clip_pos[clip_plane].y) +
	            v->clip_up[clip_plane].z*(p2 - v->clip_pos[clip_plane].z))/v->clip_up[clip_plane].x,
	        p1,
            p2);
      if (p.x > data->box.x || p.x < 0) p.x = -1;
      break;
    case 'y':
      vec3d_set(p, p1,
            v->clip_pos[clip_plane].y -
                (v->clip_up[clip_plane].x*(p1 - v->clip_pos[clip_plane].x) +
	            v->clip_up[clip_plane].z*(p2 - v->clip_pos[clip_plane].z))/v->clip_up[clip_plane].y,
	        p2);
      if (p.y > data->box.y || p.y < 0) p.x = -1;
      break;
    case 'z':
      vec3d_set(p, p1,
            p2,
            v->clip_pos[clip_plane].z -
        	    (v->clip_up[clip_plane].x*(p1 - v->clip_pos[clip_plane].x) +
	            v->clip_up[clip_plane].y*(p2 - v->clip_pos[clip_plane].y))/v->clip_up[clip_plane].z);
      if (p.z > data->box.z || p.z < 0) p.x = -1;
    break;
  }
  return p;
}

/*! \brief Projects a point onto a face of the bounding box and converts it to polar coordinates
 * \param a The point to convert
 * \return The theta component of the polar coordinates
 *
 * Find the nearest unit vector to the clipping plane's normal, then project points into the perpendicular
 * plane to prevent any overlapping of points. After projection, convert points to polar coordinates using
 * atan2 and evaluate by comparing thetas.
 */
float Model::PolarAngle(vec3d_t a, int clip_plane)
{
  vec3d_t n;
  vec3d_assign(n, v->clip_up[clip_plane]);
  align_vec(&n);
  //Since n is a unit vector, only one coordinate in non-zero
  if (n.x)
    return atan2(a.y-data->box.y/2, a.z-data->box.z/2);
  if (n.y)
    return atan2(a.x-data->box.x/2, a.z-data->box.z/2);
  if (n.z)
    return atan2(a.x-data->box.x/2, a.y-data->box.y/2);
  return 0;
}

/*! \brief Move the clip plane position to the point on the clip plane nearest to the center of the bounding box
 *
 * This adjustment is made to help prevent the clip plane position from being outside of
 * the bounding box when the clip plane is near to one of the bounding box's sides.
 * It does not guarentee that the position will be in the bounding box, but is usually pretty good.
 */
void Model::AdjustClipPos(int clip_plane)
{
  vec3d_t point = get_vec(data->box.x/2, data->box.y/2, data->box.z/2);
  vec3d_t diff = get_vec_diff(point, v->clip_pos[clip_plane]);
  v->clip_pos[clip_plane] = get_vec_diff(point,
      get_vec_scale(v->clip_up[clip_plane], vec3d_dot(v->clip_up[clip_plane], diff)));
}

bool CompPoints(vec3d_t p1, vec3d_t p2)
{
  //Order based on the x coordinate of each vector
  return p1.x < p2.x;
}


/*! \brief Render the clip plane rotation knob
 * \param color The color to use for the knob
 *
 * A sphere is drawn along the vector starting at Vars::clip_pos and in the direction of Vars::clip_up,
 * which is assumed to have unit length. The color parameter is provided to make color picking for click
 * detection with KnobIsClicked easier.
 */
void Model::DrawClipControl(vec3d_t color, int clip_plane)
{
    GLUquadric* pquad = gluNewQuadric();
    vec3d_t vert;
    vec3d_sum(vert, v->clip_pos[clip_plane], get_vec_scale(v->clip_up[clip_plane],
          CLIP_KNOB_SIZE*(data->box.x + data->box.y + data->box.z)/3.0));

    glColor3f(color.x, color.y, color.z);

    glPushMatrix();
    glTranslatef(vert.x, vert.y, vert.z);
    gluSphere(pquad, 0.02*(data->box.x + data->box.y + data->box.z)/3.0, 14, 14);
    glPopMatrix();

    glDisable(GL_LIGHTING);
    glBegin(GL_LINES);
    glVertex3f(vert.x, vert.y, vert.z);
    glColor3f(0.8, 0.8, 0.0);
    glVertex3f(v->clip_pos[clip_plane].x, v->clip_pos[clip_plane].y, v->clip_pos[clip_plane].z);
    glEnd();
    glEnable(GL_LIGHTING);
}

void Model::DrawClipPlanes()
{
  for (unsigned int c = 0; c < v->clip_pos.size(); ++c)
    DrawClipPlane(c);
}

/*! \brief Render the clip plane as orange lines where it intersects the bounding box
 *
 * All intersections of the clip plane and the edges of the bounding box are computed,
 * sorted into counterclockwise order by converting to polar coordinates, and then
 * rendered as a GL_LINE_STRIP.
 */
void Model::DrawClipPlane(int clip_plane)
{
  if (!v->clip || v->arrowmode != ARROWMODE_CLIP) return;

  glLineWidth(3);
  vec3d_t color;
  if (clip_plane == cur_clip) { vec3d_set(color, 1.0, 0.4, 0.0); }
  else { vec3d_set(color, 0.6, 0.6, 0.7); }

  DrawClipControl(color, clip_plane);

  /* find all intersections of clip plane and the lines of the box */
  vec3d_t all_pts[12] =  {
    FindIntersection(0,0, 'x', clip_plane),
    FindIntersection(0, data->box.z, 'x', clip_plane),
    FindIntersection(data->box.y, 0, 'x', clip_plane),
    FindIntersection(data->box.y, data->box.z, 'x', clip_plane),
    FindIntersection(0, 0, 'y', clip_plane),
    FindIntersection(0, data->box.z, 'y', clip_plane),
    FindIntersection(data->box.x, 0, 'y', clip_plane),
    FindIntersection(data->box.x, data->box.z, 'y', clip_plane),
    FindIntersection(0, 0, 'z', clip_plane),
    FindIntersection(0, data->box.y, 'z', clip_plane),
    FindIntersection(data->box.x, 0, 'z', clip_plane),
    FindIntersection(data->box.x, data->box.y, 'z', clip_plane) };

  /* collect only the points that are within the bounds of the box */
  vec3d_t pts[12];
  int npts = 0;
  for (int i=0; i<12; i++)
  {
    if (all_pts[i].x != -1)
    {
      pts[npts] = all_pts[i];
      npts++;
    }
  }
  /* Vector of <polar_angle, index, UNUSED> vec3ds for sorting */
  vector<vec3d_t> polar_pts;
  for (int i=0; i < npts; ++i)
  {
    vec3d_t temp;
    vec3d_set(temp, PolarAngle(pts[i], clip_plane), i, 0);
    polar_pts.push_back(temp);
  }
  sort(polar_pts.begin(), polar_pts.end(), CompPoints);

  glDisable(GL_LIGHTING);
  glBegin(GL_LINE_LOOP);
  glColor4f(color.x, color.y, color.z, 1);
  vector<vec3d_t>::iterator i;
  for (i = polar_pts.begin(); i != polar_pts.end(); ++i) {
    int pt_index = (*i).y;
    glVertex3f(pts[pt_index].x, pts[pt_index].y, pts[pt_index].z);
  }
  glEnd();
  glEnable(GL_LIGHTING);

  glLineWidth(1);
}
