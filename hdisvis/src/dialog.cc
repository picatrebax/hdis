#include "dialog.hh"

/*! \brief VideoDialog constructor
 * \param pos Initial position
 */
VideoDialog::VideoDialog(wxPoint pos) :
  wxDialog(NULL, -1, wxEmptyString, pos, wxDefaultSize)
{
	// a bunch of controls and settings and stuff
  wxStaticBoxSizer *border = new wxStaticBoxSizer(wxVERTICAL, this, wxEmptyString);
  wxFlexGridSizer *sizer = new wxFlexGridSizer(4, 2, 5, 5);

  sizer->Add(new wxStaticText(this, wxID_ANY, _("Auto load: ")),
      0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);

  auto_load = new wxCheckBox(this, wxID_ANY, wxEmptyString);
  sizer->Add(auto_load, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL);

  sizer->Add(new wxStaticText(this, wxID_ANY, _("Auto capture: ")),
      0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);

  auto_capture = new wxCheckBox(this, ID_VIDEO_DELAY, wxEmptyString);
  sizer->Add(auto_capture, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL);
  this->Connect(ID_VIDEO_DELAY, wxEVT_COMMAND_CHECKBOX_CLICKED,
      wxCommandEventHandler(VideoDialog::OnAutoCapToggle));

  sizer->Add(new  wxStaticText(this, wxID_ANY, _("Time delay: ")),
        0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);

  wxFloatingPointValidator<double> delay_val(3, NULL, wxNUM_VAL_ZERO_AS_BLANK);
  delay = new wxTextCtrl(this, wxID_ANY, wxEmptyString,
      wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER, delay_val);
  delay->SetValue("1.00");
  delay->Enable(false);
  sizer->Add(delay, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL, 0);

	sizer->Add(new  wxStaticText(this, wxID_ANY, _("Quality (Mbps): ")),
        0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 0);

	wxIntegerValidator<unsigned int> mbps_val(NULL, wxNUM_VAL_THOUSANDS_SEPARATOR);
	mbps = new wxTextCtrl(this, wxID_ANY, wxEmptyString,
			wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER, mbps_val);
	mbps->SetValue("2000");
	sizer->Add(mbps, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL, 0);

  border->Add(sizer, 0, wxALIGN_CENTER, 0);
  border->Add(CreateButtonSizer(wxOK | wxCANCEL), 0, wxALIGN_RIGHT, 0);

  border->SetSizeHints(this);
  this->SetSizer(border);
}

/*! \brief Event handler for the auto_capture check box
 * \param event The caught event
 */
void VideoDialog::OnAutoCapToggle(wxCommandEvent &event)
{
  if (event.GetId() == ID_VIDEO_DELAY)
    delay->Enable(event.GetInt());
}
