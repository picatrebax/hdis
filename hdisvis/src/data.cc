#include "data.hh"


/*! \brief Create a vertex and initialize all values to 0
 * \return The new vertex
 */
vertex_t Vertex()
{
  vertex_t vert;
  vert.vx = vert.vy = vert.vz = vert.nx = vert.ny = vert.nz = 0;
  return vert;
}

/*! \brief Create a new vertex with a specified position
 * \param v The position vector
 * \return The new vertex
 */
vertex_t Vertex(vec3d_t v)
{
  vertex_t vert;
  vert.vx = v.x; vert.vy = v.y; vert.vz = v.z;
  vert.nx = vert.ny = vert.nz = 0;
  return vert;
}

/*! \brief Create a new vertex with a specified position and normal
 * \param v The position vector
 * \param n The normal vector
 * \return The new vertex
 */
vertex_t Vertex(vec3d_t v, vec3d_t n)
{
  vertex_t vert;
  vert.vx = v.x; vert.vy = v.y; vert.vz = v.z;
  vert.nx = n.x; vert.ny = n.y; vert.nz = n.z;
  return vert;
}

/*! \brief Default constructor, intializes r/g/b to random values in [0,1], a to 1, and everything else to 0
 */
Color::Color()
{
  r = (float)rand()/RAND_MAX;
  g = (float)rand()/RAND_MAX;
  b = (float)rand()/RAND_MAX;
  r_var = g_var = b_var = 0;
  a = 1;
}

/*! \brief Color constructor
 * \param r Red value
 * \param g Green value
 * \param b Blue value
 * \param r_var Maximum red variation
 * \param g_var Maximum green variation
 * \param b_var Maximum blue variation
 * \param a Alpha value
 */
Color::Color(float r, float g, float b, 
             float r_var, float g_var, float b_var, 
             float a)
{
  this->r = r;
  this->g = g;
  this->b = b;
  this->r_var = r_var;
  this->g_var = g_var;
  this->b_var = b_var;
  this->a = a;
}

hid_t RegisterVertType()
{
  hid_t vert_type = H5Tcreate(H5T_COMPOUND, sizeof(vertex_t));
  H5Tinsert(vert_type, "vx", HOFFSET(vertex_t, vx), H5T_NATIVE_FLOAT);
  H5Tinsert(vert_type, "vy", HOFFSET(vertex_t, vy), H5T_NATIVE_FLOAT);
  H5Tinsert(vert_type, "vz", HOFFSET(vertex_t, vz), H5T_NATIVE_FLOAT);
  H5Tinsert(vert_type, "nx", HOFFSET(vertex_t, nx), H5T_NATIVE_FLOAT);
  H5Tinsert(vert_type, "ny", HOFFSET(vertex_t, ny), H5T_NATIVE_FLOAT);
  H5Tinsert(vert_type, "nz", HOFFSET(vertex_t, nz), H5T_NATIVE_FLOAT);
  return vert_type;
}

void FreeVertType(hid_t vert_type)
{
  H5Tclose(vert_type);
}

/*! \brief Data constructor
 * \param redraw Atom redraw threshold
 * \param sphere Sphere adjustment radius
 */
Data::Data(double* redraw, double* sphere)
{
  redraw_threshold = redraw;
  sphere_adjustment = sphere;

  cur_file = 0;

  initialized = false;
  contacts_current = false;

  nVBOs = 0;
  VBOids = NULL;

  nPs = 0;
  nBs = 0;
  nSs = 0;
  nTs = 0;

  nAGs = 0;
  nSGs = 0;
  nTGs = 0;

  prim_r = NULL;
  prim_VBOs = NULL;
  prim_iverts = NULL;
  prim_nverts = NULL;
  prim_bodies = NULL;
  prim_ipoints = NULL;
  prim_npoints = NULL;
  prim_points = NULL;
  prim_VBO_points = NULL;
  prim_iclosest = NULL;

  body_speeds = NULL;
  body_forces = NULL;
  body_colors = NULL;
  body_centers = NULL;
  
  spring_verts = NULL;
  ptriangle_verts = NULL;

  atom_group_strings = NULL;

  atom_groups = NULL;
  atom_group_valid = NULL;
  atom_group_color_by_force = NULL;
  spring_groups = NULL;
  ptriangle_groups = NULL;

  group_atom_colors = NULL;
  group_spring_colors = NULL;
  group_ptriangle_colors = NULL;

  vec3d_set(box, 0, 0, 0);
  cur_time = 0;
}

/*! \brief Data destructor
 * \return
 */
Data::~Data()
{
  Clear();
}

/*! \brief Reset all values, free all arrays, and delete all vbos
 * \return
 */
void Data::Clear()
{
  files.Clear();
  cur_file = 0;

  initialized = false;
  contacts_current = false;

  nPs = 0;
  nBs = 0;
  nSs = 0;
  nTs = 0;

  nAGs = 0;
  nSGs = 0;
  nTGs = 0;

  DELETE_ARRAY(prim_VBOs);
  DELETE_ARRAY(prim_bodies);
  DELETE_ARRAY(prim_nverts);
  DELETE_ARRAY(prim_iverts);
  DELETE_ARRAY(prim_npoints);
  DELETE_ARRAY(prim_ipoints);
  DELETE_ARRAY(prim_r);
  DELETE_ARRAY(prim_points);
  DELETE_ARRAY(prim_VBO_points);
  DELETE_ARRAY(prim_iclosest);

  DELETE_ARRAY(body_speeds);
  DELETE_ARRAY(body_forces);
  DELETE_ARRAY(body_colors);
  DELETE_ARRAY(body_centers);

  DELETE_ARRAY(spring_verts);
  DELETE_ARRAY(ptriangle_verts);

  DELETE_ARRAY(atom_groups);
  DELETE_ARRAY(atom_group_valid);
  DELETE_ARRAY(atom_group_color_by_force);
  DELETE_ARRAY(spring_groups);
  DELETE_ARRAY(ptriangle_groups);

  DELETE_ARRAY(group_atom_colors);
  DELETE_ARRAY(group_spring_colors);
  DELETE_ARRAY(group_ptriangle_colors);

  if (nVBOs > 0) glDeleteBuffers(nVBOs, VBOids);
  DELETE_ARRAY(VBOids);
  
  delete[] spheres;

  vec3d_set(box, 0, 0, 0);
  cur_time = 0;
}

void Data::ReadContacts()
{
  /*
  clock_t contact_start = clock();

  lpio_t* pio = lpio_alloc(files[cur_file].char_str(), LPIO_ACCESS_F_READONLY);
  assert(pio);
  unsigned int nCs = lpio_get_dim(pio, "Contact List");
  unsigned int nAs = lpio_get_dim(pio, "Atoms");
  if (nCs == (unsigned int)LPIO_DATASET_MISSING) nCs = 0;
  contact_t* contacts = new contact_t[nCs];
  lpio_read(pio, "Contact List", contacts);
  for (unsigned int c = 0; c < nCs; ++c)
  {
    unsigned int id1 = contacts[c].id1;
    unsigned int id2 = contacts[c].id2;
    if (id1 >= nAs || id2 >= nAs) continue; // contact with boundary -- ignore it
    unsigned int body1 = atom_bodies[id1];
    unsigned int body2 = atom_bodies[id2];
    double force = vec3d_abs(contacts[c].f);
    bool new_contact = true;
    for (unsigned int e1 = 0; e1 < interbody_forces[body1].size(); ++e1)
    {
      if (interbody_forces[body1][e1].first == body2)
      {
        interbody_forces[body1][e1].second += force;
        for (unsigned int e2 = 0; e2 < interbody_forces[body2].size(); ++e2)
        {
          if (interbody_forces[body2][e2].first == body1)
          {
            interbody_forces[body2][e2].second += force;
            break;
          }
        }
        new_contact = false;
        break;
      }
    }
    if (new_contact)
    {
      // first contact between these two bodies -- add new edge
      interbody_forces[body1].push_back(make_pair(body2, force));
      interbody_forces[body2].push_back(make_pair(body1, force));
    }
  }
  delete contacts;
  lpio_free(pio);
  contacts_current = true;

  clock_t contact_end = clock();
  LogTime(_F(_("ReadContacts (%u contacts)"), nCs), contact_start, contact_end);
	*/
}

/*! \brief Read an HDF5 file and update all necessary data
 * \param n Index into the files array of the desired file
 *
 * It is assumed that the size of all of the tables in the HDF5 file
 * remain constant, and that atoms do not change type. All data that
 * isn't constant is updated here.  This includes positions, forces,
 * and velocities. All vbos are also updated to reflect the new data.
 */
void Data::LoadFile(unsigned int n)
{
  cur_file = n;
  contacts_current = false;

  lpio_t* pio = lpio_alloc(files[cur_file].char_str(), LPIO_ACCESS_F_READONLY);
  assert(pio);
  
  lptimer_t hdis_timer;
  lpio_read(pio, "Time_Parameters", &hdis_timer);
  cur_time = hdis_timer.t;
  ts = hdis_timer.ts;

  if (initialized == false)
    wxMessageBox(_("Ack! Trying to load data without initializing space for it!"));

  unsigned int nVs = lpio_get_dim(pio, "Verts");
  vert_t* hdis_verts = new vert_t[nVs];
  lpio_read(pio, "Verts", hdis_verts);
  body_t* hdis_bodies = new body_t[nBs]; //leave this open for use
                                          //with ptriangles
  lpio_read(pio, "Bodies", hdis_bodies);

  for (unsigned int b = 0; b < nBs; ++b)
  {
    vec3d_assign(body_forces[b], hdis_bodies[b].f);
    vec3d_assign(body_centers[b], hdis_bodies[b].c);
    vec3d_assign(body_speeds[b], hdis_bodies[b].v);

    if (interbody_forces[b].size() > 0) interbody_forces[b].clear();
  }
  
  if (nSs > 0)
    {
      spring_t* hdis_springs = new spring_t[nSs];
      lpio_read(pio, "Springs", hdis_springs);
      
      for (unsigned int s = 0; s < nSs; ++s)
        {
          spring_verts[2*s + 0] = Vertex(hdis_springs[s].r1);
          spring_verts[2*s + 1] = Vertex(hdis_springs[s].r2);
        }
      
      delete hdis_springs;
    }
  
  if (nTs > 0)
    {
      ptriangle_t* hdis_ptriangles = new ptriangle_t[nTs];
      lpio_read(pio, "PTriangles", hdis_ptriangles);
      
      for (unsigned int t = 0; t < nTs; ++t)
        {
          vec3d_t p0 = hdis_bodies[hdis_ptriangles[t].ib1].c;
          vec3d_t p1 = hdis_bodies[hdis_ptriangles[t].ib2].c;
          vec3d_t p2 = hdis_bodies[hdis_ptriangles[t].ib3].c;
          
          vec3d_t p01, p02, n;
          vec3d_diff(p01, p1, p0);
          vec3d_diff(p02, p2, p0);
          vec3d_x(n, p01, p02);
          vec3d_normalize(n, n);
          
          ptriangle_verts[3*t + 0] = Vertex(p0, n);
          ptriangle_verts[3*t + 1] = Vertex(p1, n);
          ptriangle_verts[3*t + 2] = Vertex(p2, n);
        }
      
      delete hdis_ptriangles;
    }
  
  if (nSph > 0)
    {
      lpio_read(pio, "Dots", spheres);
    }
  
  for (unsigned int v = 0; v < nVs; ++v)
    {
      prim_points[v] = hdis_verts[v].global_pos;
    }
  
  UpdateVBOs();
  
  delete hdis_verts;
  delete hdis_bodies;
  lpio_free(pio);
}

/*! \brief Initialize all variables and allocate all arrays
 *
 * \param filenames Array of full file names, relative to the working
 * directory
 *
 * Space for all of the data arrays is allocated here. Any array that
 * doesn't depend on which file is loaded is filled here. This
 * includes colors, groups, types, and body ids. The
 * atom/spring/ptriangle group information is collected and
 * stored. Atom positions and offsets into vbos are also calculated
 * using CountVerts() assigning MAX_ATOMS_PER_VBO to each vbo.  The
 * vbos are assigned ids and allocated.
 */
void Data::Initialize(wxArrayString filenames, bool only_faces)
{
  Clear();

  files = filenames;

  lpio_t* pio = lpio_alloc(files[0].char_str(), LPIO_ACCESS_F_READONLY);
  assert(pio);

  srand(time(NULL));

  /////// bodies ////////
  {
    nBs = lpio_get_dim(pio, "Bodies");

    body_speeds  = new vec3d_t[nBs];
    body_forces  = new vec3d_t[nBs];
    body_colors  = new vec3d_t[nBs];
    body_centers = new vec3d_t[nBs];
    interbody_forces = new vector< pair<unsigned int, double> >[nBs];

    for (unsigned int b = 0; b < nBs; ++b)
    {
      body_colors[b].x = (float)rand()/RAND_MAX;
      body_colors[b].y = (float)rand()/RAND_MAX;
      body_colors[b].z = (float)rand()/RAND_MAX;
    }
  }

  /////// atoms ////////
  {
/* one approach would be to have separate arrays/VBOS for each of the
 * three primitives, like hdis has. this is a fine way to do it, but
 * means that in order to draw all of the pieces of one polyhedron,
 * we'd need to jump between arrays/VBOs. this is potentially
 * concerning, since we often want to redraw specific polyhedra or
 * something like each polyhedron belonging to a body or group.
 *
 * an alternative approach, and the one I am taking here, is to pack
 * everything into one array/set of VBOs, with all of the data for
 * each polyhedra stored consecutively in memory. */

    unsigned int nAs = lpio_get_dim(pio, "Atoms");

    lpindex_t nFs = lpio_get_dim(pio, "Faces");
    lpindex_t nEs = lpio_get_dim(pio, "Edges");
    lpindex_t nVs = lpio_get_dim(pio, "Verts");
    
    if (nFs == LPIO_DATASET_MISSING) nFs = 0;
    if (nEs == LPIO_DATASET_MISSING) nEs = 0;
    if (nVs == LPIO_DATASET_MISSING) nVs = 0;
    
    atom_t* hdis_atoms = new atom_t[nAs];
    face_t* hdis_faces = new face_t[nFs];
    edge_t* hdis_edges = new edge_t[nEs];
    vert_t* hdis_verts = new vert_t[nVs];
    
    lpio_read(pio, "Atoms", hdis_atoms);
    if (nFs) lpio_read(pio, "Faces", hdis_faces);
    if (nEs) lpio_read(pio, "Edges", hdis_edges);
    lpio_read(pio, "Verts", hdis_verts);
    
    if (only_faces == true)
      nPs = nFs;
    else
      nPs = nFs + nEs + nVs;
    
    // VBO stuff
    prim_VBOs    = new unsigned int[nPs];
    prim_iverts  = new unsigned int[nPs];
    prim_nverts  = new unsigned int[nPs];
    
    // geometry stuff
    prim_bodies  = new unsigned int[nPs];
    atom_groups  = new unsigned int[nPs];
    prim_r       = new float[nPs];
    
    // we lose some packing efficiency here by giving 3 point indices
    // to all feature types. on the other hand, we don't need a
    // prim_iipoints array
    prim_ipoints = new unsigned int[3*nPs];
    prim_npoints = new unsigned int[3*nPs];
    prim_points  = new vec3d_t[3*nPs];//new vec3d_t[3*nVs];
    prim_VBO_points = new vec3d_t[3*nPs];
    
    prim_iclosest = new unsigned int[nPs];
    
    nAGs = 0;
    
    /* we don't know how many atom groups there will be, so make sure
     * there is space for the max number of groups (one per atom) */
    bool* temp_valid_groups = new bool[nAs];
    
    unsigned int cur_V = 0;
    unsigned int cur_E = 0;
    unsigned int cur_F = 0;
    unsigned int cur_prim = 0;

    for (unsigned int a = 0; a < nAs; ++a)
    {
      /* okay, now we go through the poly and pack all of its features
       * into the arrays.
       *
       * IMPORTANT: atom_t doesn't store the locations of its
       * edges/faces in their respective arrays, but we know that
       * they are all stored sequentially in the same order that the
       * atoms are stored (i.e. hdis_verts has verts for poly 0
       * first, then poly 1, etc. this is all we need to figure out
       * where everything is.
       */
      atom_t* patom = hdis_atoms + a;
      int group_id  = patom->group_id;
      int body_indx = patom->body_indx;
      double r      = patom->R;

      for (int v = 0; v < patom->nverts; ++v)
        {
          if (only_faces == false)
            {
              //unsigned int cur_index = nFs + nEs + cur_V;
              unsigned int cur_index = cur_prim;
              
              prim_bodies[cur_index]    = body_indx;
              atom_groups[cur_index]    = group_id;
              prim_r[cur_index]         = r;
              prim_npoints[cur_index]   = 1;
              prim_ipoints[3*cur_index] = cur_V;
              prim_iclosest[cur_index]  = cur_V;
              
              cur_prim++;
            }
          
          prim_points[cur_V] = hdis_verts[cur_V].global_pos;
          cur_V++;
        }
      
      if (nEs)
        {
          for (int e = 0; e < patom->nedges; ++e)
            {
              if (only_faces == false)
                {
                  //unsigned int cur_index = nFs + cur_E;
                  unsigned int cur_index = cur_prim;
                  
                  prim_bodies[cur_index]      = body_indx;
                  atom_groups[cur_index]      = group_id;
                  prim_r[cur_index]           = r;
                  prim_npoints[cur_index]     = 2;
                  prim_ipoints[3*cur_index+0] = hdis_edges[cur_E].itail;
                  prim_ipoints[3*cur_index+1] = hdis_edges[cur_E].ihead;
                  prim_iclosest[cur_index]    = cur_E;
                  
                  cur_E++;
                  cur_prim++;
                }
            }
        }
      
      if (nFs)
        {
          for (int f = 0; f < patom->nfaces; ++f)
            {
              //unsigned int cur_index = cur_F;
              unsigned int cur_index = cur_prim;
              
              prim_bodies[cur_index]      = body_indx;
              atom_groups[cur_index]      = group_id;
              // if we are only drawing faces, we don't want any dilation radius
              if (only_faces)
                prim_r[cur_index]         = 0;
              else
                prim_r[cur_index]         = r;

              prim_npoints[cur_index]     = 3;
              prim_ipoints[3*cur_index+0] = hdis_faces[cur_F].ivert0;
              prim_ipoints[3*cur_index+1] = hdis_faces[cur_F].ivert1;
              prim_ipoints[3*cur_index+2] = hdis_faces[cur_F].ivert2;
              prim_iclosest[cur_index]    = cur_F;
              
              cur_F++;
              cur_prim++;
            }
        }
      
//      nAGs = max(nAGs, (unsigned int)hdis_atoms[a].group_id + 1); // DEPRECATED
      //atom_groups[a] = hdis_atoms[a].group_id;
      
      /* we found a member of this group */
      temp_valid_groups[hdis_atoms[a].group_id] = 1;

      // always set 0 group being valid (this is a crunch to let water be in the
      // list)
      temp_valid_groups[0] = 1;
    }
    
    // important that prim_points is filled already!
    GenerateVBOs();
    
    delete hdis_atoms;
    delete hdis_faces;
    delete hdis_edges;
    delete hdis_verts;

    /* read group name strings from file */
    unsigned int nStrings = lpio_get_dim(pio, "Group Names");
    char** strings = new char*[nStrings];

    lpio_read_astrings(pio, strings, nStrings, "Group Names");
    
    nAGs = nStrings; // NUMBER OF GROUPS _IS_ A NUMBER OF GROUP NAMES!
    group_atom_colors  = new Color[nAGs];
    atom_group_strings = new wxString[nAGs];

    /* initialize all group names to the default */
    for (unsigned int n = 0; n < nAGs; ++n) 
      atom_group_strings[n] = _F(_("Group %u"), n);
    /* and then overwrite with the strings from the file where
       appropriate */
    for (unsigned int n = 0; n < min(nStrings, nAGs); ++n)
      {
        atom_group_strings[n] = _(strings[n]);
      }
    
    delete strings;
    
    /* take only the front part of temp_valid_groups, discarding any
       unused values at the end */
    atom_group_valid = new bool[nAGs];
    for (unsigned int n = 0; n < nAGs; ++n)
      atom_group_valid[n] = temp_valid_groups[n];
    delete temp_valid_groups;
    
    /* create and initialize the array for which displaymode to use
       for each group */
    atom_group_color_by_force = new bool[nAGs];
    for (unsigned int i = 0; i < nAGs; ++i) 
      atom_group_color_by_force[i] = false;
  }
  
  /////// springs /////////
  {
    nSs = lpio_get_dim(pio, "Springs");
    if (nSs == (unsigned int)LPIO_DATASET_MISSING) nSs = 0;
    
    if (nSs > 0)
      {
        spring_verts  = new vertex_t[2*nSs];
        spring_groups = new unsigned int[nSs];
        
        nSGs = 0;
        
        spring_t* hdis_springs = new spring_t[nSs];
        lpio_read(pio, "Springs", hdis_springs);
        
        for (unsigned int s = 0; s < nSs; ++s)
          {
            nSGs = max(nSGs, (unsigned int)hdis_springs[s].group + 1);
            spring_groups[s] = hdis_springs[s].group;
          }
        
        delete hdis_springs;
        
        group_spring_colors = new Color[nSGs];
      }
  }
  
  ////// ptriangles //////
  {
    nTs = lpio_get_dim(pio, "PTriangles");
    if (nTs == (unsigned int)LPIO_DATASET_MISSING) nTs = 0;
    
    if (nTs > 0)
      {
        ptriangle_verts  = new vertex_t[3*nTs];
        ptriangle_groups = new unsigned int[nTs];
        
        nTGs = 0;
        
        ptriangle_t* hdis_ptriangles = new ptriangle_t[nTs];
        lpio_read(pio, "PTriangles", hdis_ptriangles);
        
        for (unsigned int t = 0; t < nTs; ++t)
          {
            nTGs = max(nTGs, (unsigned int)hdis_ptriangles[t].group + 1);
            ptriangle_groups[t] = hdis_ptriangles[t].group;
          }
        
        delete hdis_ptriangles;
        
        group_ptriangle_colors = new Color[nTGs];
      }
  }
  
  ////// spheres //////
  {
    nSph = lpio_get_dim(pio, "Dots");
    if (nSph == (unsigned int)LPIO_DATASET_MISSING) nSph = 0;
    
    if (nSph > 0)
      {
        spheres = new sph_t[nSph];
        //////////////////////////
//        ptriangle_verts  = new vertex_t[3*nTs];
//        ptriangle_groups = new unsigned int[nTs];
//        
//        nTGs = 0;
//        
//        ptriangle_t* hdis_ptriangles = new ptriangle_t[nTs];
//        lpio_read(pio, "PTriangles", hdis_ptriangles);
//        
//        for (unsigned int t = 0; t < nTs; ++t)
//          {
//            nTGs = max(nTGs, (unsigned int)hdis_ptriangles[t].group + 1);
//            ptriangle_groups[t] = hdis_ptriangles[t].group;
//          }
//        
//        delete hdis_ptriangles;
//        
//        group_ptriangle_colors = new Color[nTGs];
      }
  }
  
  box_t hdis_box;
  lpio_read(pio, "Box (domain)", &hdis_box);
  box = hdis_box.fcorner;
  
  initialized = true;
}

/*! \brief Generate buffer ids and initialize vbos
 * \param hdis_points Point data
 * \param hdis_atoms Atom data
 *
 * Each vbo is assigned an id, the number of vertices it will store is
 * calculated, and it's data store is initialized.
 */
void Data::GenerateVBOs()
{
  nVBOs = (unsigned int) ceil((float)nPs / MAX_PRIMS_PER_VBO);
  
  VBOids = new unsigned int[nVBOs];
  glGenBuffers(nVBOs, VBOids);
  
  wxLogMessage(_("GenerateVBOs: created %u buffers"), nVBOs);
  
  // how deep we are into the current VBO
  unsigned int cur_offset = 0;
  
  for (unsigned int p = 0; p < nPs; ++p)
    {
      if (p % MAX_PRIMS_PER_VBO == 0) cur_offset = 0;
      
      int nverts = NVERTS(prim_npoints[p]);
      
      prim_VBOs[p] = p/MAX_PRIMS_PER_VBO;
      prim_nverts[p] = nverts;
      prim_iverts[p] = cur_offset;
      
      cur_offset += nverts;
    }
  
  // this is a big ol' array that we are going to fill with vertex
  // data for the VBOs. we will delete it after we buffer everything
  // that we load into to the VBOs, so it's okay that it's a little
  // oversized
  vertex_t* verts = new vertex_t[MAX_PRIMS_PER_VBO * MAX_VERTS_PER_PRIM];
  
  for (unsigned int vbo = 0; vbo < nVBOs; ++vbo)
    {
      unsigned int nverts = FillVertsCoupi(verts, vbo);
      
      glBindBuffer(GL_ARRAY_BUFFER, VBOids[vbo]);
      glBufferData(GL_ARRAY_BUFFER, 
                   sizeof(vertex_t) * nverts, verts, GL_DYNAMIC_DRAW);
    }
  delete verts;
}


/*! \brief Update all vbo data from file
 *
 * Recomputes the triangles for each atom and updates all of the data
 * in each vbo without attempting to conserve resources or time.
 */
void Data::RefreshVBOs()
{
  lpio_t* pio = lpio_alloc(files[cur_file].char_str(), LPIO_ACCESS_F_READONLY);
  assert(pio);
  
  vertex_t* verts = new vertex_t[MAX_PRIMS_PER_VBO * MAX_VERTS_PER_PRIM];
  for (unsigned int vbo = 0; vbo < nVBOs; ++vbo)
    {
      unsigned int nverts = FillVertsCoupi(verts, vbo);
      
      glBindBuffer(GL_ARRAY_BUFFER, VBOids[vbo]);
      glBufferData(GL_ARRAY_BUFFER, 
                   sizeof(vertex_t) * nverts, verts, GL_DYNAMIC_DRAW);
    }
  delete verts;
  
}

/*! \brief Update vbo data where necessary
 * \param hdis_points Point data
 * \param hdis_atoms Atom data
 *
 * Updates the vertex information stored in the vbos. An atom is only
 * updated if any of its points have moved more than
 * redraw_threshold. When updating atoms, any atoms stored
 * consecutively in the vbo that need updating are all batched and
 * updated with one access to the buffer. This provides a good
 * performance increase when many atoms need updating.
 */
void Data::UpdateVBOs()
{
  {
    unsigned int updated_Ps = 0;
    
    vertex_t* verts = new vertex_t[MAX_PRIMS_PER_VBO * MAX_VERTS_PER_PRIM];
    for (unsigned int v = 0; v < nVBOs; ++v)
      {
        glBindBuffer(GL_ARRAY_BUFFER, VBOids[v]);
        unsigned int start_prim = 0;
        unsigned int total_verts = 0;
        
        // make sure we don't step off the end of the array
        for (unsigned int p = MAX_PRIMS_PER_VBO * v; 
             p < min(nPs, MAX_PRIMS_PER_VBO * (v + 1)); ++p)
          {
            // the maximum displacement of a point in the atom
            double displacement = 0;
            
            for (unsigned int i = 0; i < prim_npoints[p]; ++i)
              {
                // find the largest movement of all of the points in the atom
                // if we only check the movement of the first point, we run 
                // the risk of missing rotations about that point
                vec3d_t delta;
                vec3d_diff(delta, prim_points[prim_ipoints[3*p+i]], 
                           prim_VBO_points[3*p+i]);
                displacement = max(displacement, vec3d_abs(delta));
              }
            
            // if one of the points moved too much, we need to update the VBO
            if (displacement > *redraw_threshold)
              {
                // begin filling the buffer of updated data - we will
                // try to batch any consecutive updates, so we only
                // need to bind the VBO once
                if (total_verts == 0) start_prim = p;
                
                updated_Ps++;
                unsigned int nverts = FillPrim(verts + total_verts, p);
                total_verts += nverts;
              }
            // this atom doesn't need to be updated, so we can push
            // the batch of changes that we've accumulated
            else if (total_verts > 0)
              {
                glBufferSubData(GL_ARRAY_BUFFER, 
                                sizeof(vertex_t) * prim_iverts[start_prim], 
                                sizeof(vertex_t) * total_verts, verts);
                // now that we flushed all the verts, we start over at
                // the start of the array
                total_verts = 0;
              }
          }
        // when all is said and done, make sure we push any remaining
        // updated atoms out to the VBO
        if (total_verts > 0)
          {
            glBufferSubData(GL_ARRAY_BUFFER, 
                            sizeof(vertex_t) * prim_iverts[start_prim], 
                            sizeof(vertex_t) * total_verts, verts);
          }
      }
    delete verts;
    
    wxLogMessage(_("Updated %.2f%% of atoms"), 
                 (float)updated_Ps/(float)nPs * 100);
  }
}

/*! \brief Fill an array with vertex data for an entire vbo
 * \param verts Target array
 * \param vbo Lookup index of the target vbo
 * \param hdis_points Point data
 * \param hdis_atoms Atom data
 * \return Number of vertices added to the array
 */
unsigned int Data::FillVertsCoupi(vertex_t* verts, unsigned int vbo)
{
  unsigned int nverts = 0;
  for (unsigned int p = MAX_PRIMS_PER_VBO * vbo; 
       p < min(nPs, MAX_PRIMS_PER_VBO * (vbo + 1)); ++p)
  {
    nverts += FillPrim(verts + nverts, p);
  }
  return nverts;
}

/*! \brief Fill an array with vertex data for an atom
 * \param verts Target array
 * \param hdis_points Point data
 * \param atom Atom data
 * \return Number of vertices added
 */
unsigned int Data::FillPrim(vertex_t* verts, int p)
{
  unsigned int nverts = 0;
  float r = prim_r[p];
  unsigned int* pindex = prim_ipoints + 3*p;
  
  switch (prim_npoints[p])
    {
    case 1: nverts = nverts + AddSphere(r, prim_points[*(pindex)], 
                                        verts + nverts);
      prim_VBO_points[3*p] = prim_points[*(pindex)];
      break;
      
    case 2: 
      nverts = nverts + 
        AddCylinder(r, prim_points[*(pindex+0)],
                    prim_points[*(pindex+1)], verts + nverts);
      prim_VBO_points[3*p+0] = prim_points[*(pindex+0)];
      prim_VBO_points[3*p+1] = prim_points[*(pindex+1)];
      break;
      
    case 3: 
      nverts = nverts + 
        AddTface(r, prim_points[*(pindex+0)],
                 prim_points[*(pindex+1)],
                 prim_points[*(pindex+2)], verts + nverts);
      prim_VBO_points[3*p+0] = prim_points[*(pindex+0)];
      prim_VBO_points[3*p+1] = prim_points[*(pindex+1)];
      prim_VBO_points[3*p+2] = prim_points[*(pindex+2)];
      break;
      
    default: 
      wxLogMessage(_("UpdateVBOs: Unknown atom type %d"), prim_npoints[p]);
      break;
    }
  
  return nverts;
}

/*! \brief Fill an array with vertex data for a tface
 * \param R Dilation radius
 * \param p0 Coordinates of the first corner
 * \param p1 Coordinates of the second corner
 * \param p2 Coordinates of the third corner
 * \param start_vert Target array
 * \return Number of vertices added
 */
unsigned int Data::AddTface(float R, 
                            vec3d_t p0, vec3d_t p1, vec3d_t p2, 
                            vertex_t* start_vert)
{
  vertex_t* cur_vert = start_vert;
  vec3d_t n, trans, v01, v02;
  
  vec3d_diff(v01, p0, p1);
  vec3d_diff(v02, p0, p2);
  vec3d_x(n, v01, v02);
  vec3d_normalize(n, n);
  
  vec3d_scale(trans, n, R);
  vec3d_sum(p0, p0, trans);
  vec3d_sum(p1, p1, trans);
  vec3d_sum(p2, p2, trans);
  *cur_vert = Vertex(p0, n); cur_vert++;
  *cur_vert = Vertex(p1, n); cur_vert++;
  *cur_vert = Vertex(p2, n); cur_vert++;
  
  // we need to do the second triangle even if R = 0, since all of the
  // offsets and vert counts expect two triangles per tface
  vec3d_scale(n, n, -2);
  vec3d_scale(trans, n, R);
  vec3d_sum(p0, p0, trans);
  vec3d_sum(p1, p1, trans);
  vec3d_sum(p2, p2, trans);
  *cur_vert = Vertex(p0, n); cur_vert++;
  *cur_vert = Vertex(p1, n); cur_vert++;
  *cur_vert = Vertex(p2, n); cur_vert++;
  
  return cur_vert - start_vert;
}

/*! \brief Fill an array with vertex data for a cylinder
 * \param R Dilation radius
 * \param p0 Center of the bottom circle
 * \param p1 Center of the top circle
 * \param start_vert Target array
 * \return Number of vertices added
 *
 * Cylinders are rendered with 20 triangles that are not flat shaded.
 */
unsigned int Data::AddCylinder(float R, 
                               vec3d_t p0, vec3d_t p1, 
                               vertex_t* start_vert)
{
  vec3d_t a;
  vec3d_diff(a, p1, p0);
  float base = vec3d_abs(a);

  vec3d_t normals[20];
  vec3d_set(normals[ 0],  1.000,  0.000, 0); 
  vec3d_set(normals[ 1],  0.809,  0.588, 0);
  vec3d_set(normals[ 2],  0.309,  0.951, 0);
  vec3d_set(normals[ 3], -0.309,  0.951, 0);
  vec3d_set(normals[ 4], -0.809,  0.588, 0);
  vec3d_set(normals[ 5], -1.000,  0.000, 0);
  vec3d_set(normals[ 6], -0.809, -0.588, 0);
  vec3d_set(normals[ 7], -0.309, -0.951, 0);
  vec3d_set(normals[ 8],  0.309, -0.951, 0);
  vec3d_set(normals[ 9],  0.809, -0.588, 0);

  vec3d_set(normals[10],  0.951,  0.309, 0); 
  vec3d_set(normals[11],  0.588,  0.809, 0);
  vec3d_set(normals[12],  0.000,  1.000, 0);
  vec3d_set(normals[13], -0.588,  0.809, 0);
  vec3d_set(normals[14], -0.951,  0.309, 0);
  vec3d_set(normals[15], -0.951, -0.309, 0);
  vec3d_set(normals[16], -0.588, -0.809, 0);
  vec3d_set(normals[17], -0.000, -1.000, 0);
  vec3d_set(normals[18],  0.588, -0.809, 0);
  vec3d_set(normals[19],  0.951, -0.309, 0);
    
  vec3d_t verts[20];
  vec3d_set(verts[ 0],  1.000*R,  0.000*R, base); 
  vec3d_set(verts[ 1],  0.809*R,  0.588*R, base);
  vec3d_set(verts[ 2],  0.309*R,  0.951*R, base);
  vec3d_set(verts[ 3], -0.309*R,  0.951*R, base);
  vec3d_set(verts[ 4], -0.809*R,  0.588*R, base);
  vec3d_set(verts[ 5], -1.000*R,  0.000*R, base);
  vec3d_set(verts[ 6], -0.809*R, -0.588*R, base);
  vec3d_set(verts[ 7], -0.309*R, -0.951*R, base);
  vec3d_set(verts[ 8],  0.309*R, -0.951*R, base);
  vec3d_set(verts[ 9],  0.809*R, -0.588*R, base);

  vec3d_set(verts[10],  0.951*R,  0.309*R, 0); 
  vec3d_set(verts[11],  0.588*R,  0.809*R, 0);
  vec3d_set(verts[12],  0.000*R,  1.000*R, 0);
  vec3d_set(verts[13], -0.588*R,  0.809*R, 0);
  vec3d_set(verts[14], -0.951*R,  0.309*R, 0);
  vec3d_set(verts[15], -0.951*R, -0.309*R, 0);
  vec3d_set(verts[16], -0.588*R, -0.809*R, 0);
  vec3d_set(verts[17], -0.000*R, -1.000*R, 0);
  vec3d_set(verts[18],  0.588*R, -0.809*R, 0);
  vec3d_set(verts[19],  0.951*R, -0.309*R, 0);

  glPushMatrix();
  glLoadIdentity();
  // Axis of rotation: (0, 0, 1) X (x, y, z) = (-y, x, 0)
  glRotatef(-180*acos(a.z/base)/M_PI, -a.y, a.x, 0);
  GLdouble m[16];
  glGetDoublev(GL_MODELVIEW_MATRIX, m);
  glPopMatrix();

	// this is our rotation matrix for the cylinder
  float a11 = m[ 0], a12 = m[ 1], a13 = m[ 2], a14 = m[ 3];
  float a21 = m[ 4], a22 = m[ 5], a23 = m[ 6], a24 = m[ 7];
  float a31 = m[ 8], a32 = m[ 9], a33 = m[10], a34 = m[11];

  float nx, ny, nz;
  for (int i = 0; i < 20; ++i)
  {
    // this is janky manual matrix multiplications to apply the
    // rotation matrix
    nx = a11 * normals[i].x + a12 * normals[i].y + a13 * normals[i].z + a14;
    ny = a21 * normals[i].x + a22 * normals[i].y + a23 * normals[i].z + a24;
    nz = a31 * normals[i].x + a32 * normals[i].y + a33 * normals[i].z + a34;
    vec3d_set(normals[i], nx, ny, nz);
    
    nx = a11 * verts[i].x + a12 * verts[i].y + a13 * verts[i].z + a14;
    ny = a21 * verts[i].x + a22 * verts[i].y + a23 * verts[i].z + a24;
    nz = a31 * verts[i].x + a32 * verts[i].y + a33 * verts[i].z + a34;
    vec3d_set(verts[i], nx, ny, nz);
    vec3d_sum(verts[i], verts[i], p0);
  }
  
  //4th coordinate is the index of the normal perpendicular to the face
  GLint tindices[20][4] =
    { {0, 1, 10, 10}, {1, 2, 11, 11}, {2, 3, 12, 12}, 
      {3, 4, 13, 13}, {4, 5, 14, 14},
      {5, 6, 15, 15}, {6, 7, 16, 16}, {7, 8, 17, 17}, 
      {8, 9, 18, 18}, {9, 0, 19, 19},

      {10, 1, 11, 1}, {11, 2, 12, 2}, {12, 3, 13, 3}, 
      {13, 4, 14, 4}, {14, 5, 15, 5},
      {15, 6, 16, 6}, {16, 7, 17, 7}, {17, 8, 18, 8}, 
      {18, 9, 19, 9}, {19, 0, 10, 0}
    };

  vertex_t* cur_vert = start_vert;
  for (int t = 0; t < 20; ++t)
  {
    *cur_vert = Vertex(verts[tindices[t][0]], 
                       normals[tindices[t][0]]); ++cur_vert;
    *cur_vert = Vertex(verts[tindices[t][1]], 
                       normals[tindices[t][1]]); ++cur_vert;
    *cur_vert = Vertex(verts[tindices[t][2]], 
                       normals[tindices[t][2]]); ++cur_vert;
  }

  return cur_vert - start_vert;
}

/*! \brief Fill an array with vertex data for a sphere
 * \param R Dilation radius
 * \param p0 Center of the sphere
 * \param start_vert Target array
 * \return Number of vertices added
 *
 * Spheres are rendered as an inscribed icosohedron. The triangular
 * faces are not flat shaded - each vertex normal is directly away
 * from the center of the sphere.
 */
unsigned int Data::AddSphere(float R, vec3d_t p0, vertex_t* start_vert)
{
  GLfloat X = .525731112119133606;
  GLfloat Z = .850650808352039932;
  
  vec3d_t normals[12];  
  vec3d_set(normals[ 0], -X,  0,  Z);
  vec3d_set(normals[ 1],  X,  0,  Z);
  vec3d_set(normals[ 2], -X,  0, -Z);
  vec3d_set(normals[ 3],  X,  0, -Z);
  vec3d_set(normals[ 4],  0,  Z,  X);
  vec3d_set(normals[ 5],  0,  Z, -X);
  vec3d_set(normals[ 6],  0, -Z,  X);
  vec3d_set(normals[ 7],  0, -Z, -X);
  vec3d_set(normals[ 8],  Z,  X,  0);
  vec3d_set(normals[ 9], -Z,  X,  0);
  vec3d_set(normals[10],  Z, -X,  0);
  vec3d_set(normals[11], -Z, -X,  0); 

  vec3d_t vertexes[12];  
  vec3d_set(vertexes[ 0], -X,  0,  Z);
  vec3d_set(vertexes[ 1],  X,  0,  Z);
  vec3d_set(vertexes[ 2], -X,  0, -Z);
  vec3d_set(vertexes[ 3],  X,  0, -Z);
  vec3d_set(vertexes[ 4],  0,  Z,  X);
  vec3d_set(vertexes[ 5],  0,  Z, -X);
  vec3d_set(vertexes[ 6],  0, -Z,  X);
  vec3d_set(vertexes[ 7],  0, -Z, -X);
  vec3d_set(vertexes[ 8],  Z,  X,  0);
  vec3d_set(vertexes[ 9], -Z,  X,  0);
  vec3d_set(vertexes[10],  Z, -X,  0);
  vec3d_set(vertexes[11], -Z, -X,  0);

  for (int v = 0; v < 12; ++v)
  {
    //scale icosohedron to have the right radius
    vec3d_scale(vertexes[v], vertexes[v], R*(*sphere_adjustment));
    //and translate all verexes to center the sphere
    vec3d_sum(vertexes[v], vertexes[v], p0);
  }

  GLint tindices[20][3] = { 
     {0, 4, 1}, {0,9, 4}, {9, 5,4}, { 4,5,8}, {4,8, 1},    
     {8,10, 1}, {8,3,10}, {5, 3,8}, { 5,2,3}, {2,7, 3},    
     {7,10, 3}, {7,6,10}, {7,11,6}, {11,0,6}, {0,1, 6}, 
     {6, 1,10}, {9,0,11}, {9,11,2}, { 9,2,5}, {7,2,11}
  };

  vertex_t* cur_vert = start_vert;
  for (int t = 0; t < 20; ++t)
  {
    *cur_vert = Vertex(vertexes[tindices[t][0]], 
                       normals[tindices[t][0]]); ++cur_vert;
    *cur_vert = Vertex(vertexes[tindices[t][1]], 
                       normals[tindices[t][1]]); ++cur_vert;
    *cur_vert = Vertex(vertexes[tindices[t][2]], 
                       normals[tindices[t][2]]); ++cur_vert;
  }
  return cur_vert - start_vert;
}

/*! \brief Generate an auxialry filename from an HDF5 filename
 * \param hdis_file_name The starting HDF5 filename
 * \return The generated output filename
 */
wxString Data::GetHdisvisFileName(wxString hdis_file_name)
{
  return hdis_file_name.BeforeLast(wxUniChar('/')).Append(_("/hdisvis")).Append(hdis_file_name.AfterLast(wxUniChar('/'))).Append(_("c"));
}

/* SO I CAN EDIT RENDER CALL AND VBO IMPLEMENTATION CONCURRENTLY */
//#include "vbo.cc"
