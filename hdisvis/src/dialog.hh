#ifndef DIALOG_H
#define DIALOG_H

#include <wx/wx.h>
#include <wx/dialog.h>
#include <wx/valnum.h>

#include <vector>
using namespace std;

#include "vars.hh"

/*! \brief Video creation dialog */
class VideoDialog : public wxDialog
{
public:
  VideoDialog(wxPoint pos);

  wxTextCtrl *delay;        /*!< \brief Time delay for automatic capture */
  wxCheckBox *auto_load;    /*!< \brief Automatic file load check box */
  wxCheckBox *auto_capture; /*!< \brief Automatic capture check box */
  wxComboBox *fps;          /*!< \brief FPS selection list */
	wxTextCtrl *mbps;

  void OnAutoCapToggle(wxCommandEvent &event);
};

class ForceChainDialog : public wxDialog
{
};


#endif //DIALOG_H
