#include "utility.hh"

namespace hdisvis_utils
{

/*! \brief Print an elapsed time to the log window
 *  \param message Output message
 *  \param start clock reading at the beginning of the interval
 *  \param end clock reading at the end of the interval
 *
 *  Uses the ctime clock() and CLOCKS_PER_SEC to compute elapsed time.
 */
void LogTime(wxString message, clock_t start, clock_t end)
{
  wxLogMessage(_("%s in: \t%.2fms"), message, 1000*((float)(end - start))/CLOCKS_PER_SEC);
}


void SetRadioSelection(wxFrame *parent, int id, int sel)
{
  wxRadioBox *box = (wxRadioBox *) parent->FindWindow(id);
  box->SetSelection(sel);
}

void SetCheckBox(wxFrame *parent, int id, bool value)
{
  wxCheckBox *box = (wxCheckBox *) parent->FindWindow(id);
  box->SetValue(value);
}

void SetTextCtrl(wxFrame *parent, int id, wxString value)
{
  wxTextCtrl *txt = (wxTextCtrl *) parent->FindWindow(id);
  txt->SetValue(value);
}

void SetChoice(wxFrame *parent, int id, int sel)
{
  wxChoice *choice = (wxChoice *) parent->FindWindow(id);
  choice->SetSelection(sel);
}

/* -----------------------------------------------------*/

          /* VECTOR MANIPULATION */

/* -----------------------------------------------------*/

/*! \brief Set a vector to the nearest axis (x/y/z/-x/-y/-z)
 * \param vec Vector to change
 */
void align_vec(vec3d_t* vec)
{
  if (fabs(vec->x) > fabs(vec->y) && fabs(vec->x) > fabs(vec->z)) {
    if (vec->x > 0) {
      vec3d_set(*vec, 1, 0, 0);
    }
    else {
      vec3d_set(*vec, -1, 0, 0);
    }
  }
  else if (fabs(vec->y) > fabs(vec->z)) {
    if (vec->y > 0) {
      vec3d_set(*vec, 0, 1, 0);
    }
    else {
      vec3d_set(*vec, 0, -1, 0);
    }
  }
  else {
    if (vec->z > 0) {
      vec3d_set(*vec, 0, 0, 1);
    }
    else {
      vec3d_set(*vec, 0, 0, -1);
    }
  }
}

/*! \brief Perform an arbitrary rotation of a vector
 * \param in Vector to rotate
 * \param axis Rotation axis
 * \param t Amount to rotate (radians)
 */
void rotate(vec3d_t *in, vec3d_t axis, double t)
{
  /* equations for rotation taken from wikipedia */
  vec3d_set(*in,
      axis.x*(axis.x*in->x + axis.y*in->y + axis.z*in->z)*(1 - cos(t)) +
      in->x*cos(t) +
      (-axis.z*in->y + axis.y*in->z)*sin(t),
      
      axis.y*(axis.x*in->x + axis.y*in->y + axis.z*in->z)*(1 - cos(t)) +
      in->y*cos(t) +
      (axis.z*in->x - axis.x*in->z)*sin(t),
      
      axis.z*(axis.x*in->x + axis.y*in->y + axis.z*in->z)*(1 - cos(t)) +
      in->z*cos(t) +
      (-axis.y*in->x + axis.x*in->y)*sin(t));
  vec3d_normalize(*in, *in);
}

/*! \brief Rotate an entire orthonormal basis
 * \param vec The initial basis vector to rotate
 * \param targ The basis vector to rotate towards
 * \param other The third basis vector
 * \param speed Rotation amount (radians)
 */
void rotate_ortho(vec3d_t *vec, vec3d_t *targ, vec3d_t *other, double speed)
{
  // rotate normalizes the input vector, so as long as the three rotate_ortho input
  //   vectors are unit length, they will remain unit length
  rotate(vec, *other, speed);
  vec3d_x(*targ, *vec, *other);
  vec3d_x(*other, *targ, *vec);
}

vec3d_t get_vec(float x, float y, float z)
{
  vec3d_t vec;
  vec3d_set(vec, x, y, z);
  return vec;
}

vec3d_t get_vec_x(vec3d_t a, vec3d_t b)
{
  vec3d_t vec;
  vec3d_x(vec, a, b);
  return vec;
}

vec3d_t get_vec_sum(vec3d_t a, vec3d_t b)
{
  vec3d_t vec;
  vec3d_sum(vec, a, b);
  return vec;
}

vec3d_t get_vec_diff(vec3d_t a, vec3d_t b)
{
  vec3d_t vec;
  vec3d_diff(vec, a, b);
  return vec;
}

vec3d_t get_vec_scale(vec3d_t a, float k)
{
  vec3d_t vec;
  vec3d_scale(vec, a, k);
  return vec;
}

vec3d_t get_vec_normalize(vec3d_t a)
{
  vec3d_t vec;
  vec3d_normalize(vec, a);
  return vec;
}

}
