#include "images.hh"

bool LoadPNG(char *name, unsigned int* w, unsigned int* h, GLubyte **outData) {
#ifdef HAVE_PNG
    png_structp png_ptr;
    png_infop info_ptr;
    unsigned int sig_read = 0;
    FILE *fp;
 
    if ((fp = fopen(name, "rb")) == NULL)
    {
        return false;
    }
 
    /* Create and initialize the png_struct
     * with the desired error handler
     * functions.  If you want to use the
     * default stderr and longjump method,
     * you can supply NULL for the last
     * three parameters.  We also supply the
     * the compiler header file version, so
     * that we know if the application
     * was compiled with a compatible version
     * of the library.  REQUIRED
     */
    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,
            NULL, NULL, NULL);
 
    if (png_ptr == NULL) {
        fclose(fp);
        return false;
    }
 
    /* Allocate/initialize the memory
     * for image information.  REQUIRED. */
    info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL) {
        fclose(fp);
        png_destroy_read_struct(&png_ptr, NULL, NULL);
        return false;
    }
 
    /* Set error handling if you are
     * using the setjmp/longjmp method
     * (this is the normal method of
     * doing things with libpng).
     * REQUIRED unless you  set up
     * your own error handlers in
     * the png_create_read_struct()
     * earlier.
     */
    if (setjmp(png_jmpbuf(png_ptr))) {
        /* Free all of the memory associated
         * with the png_ptr and info_ptr */
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        fclose(fp);
        /* If we get here, we had a
         * problem reading the file */
        return false;
    }
 
    /* Set up the output control if
     * you are using standard C streams */
    png_init_io(png_ptr, fp);
 
    /* If we have already
     * read some of the signature */
    png_set_sig_bytes(png_ptr, sig_read);
 
    /*
     * If you have enough memory to read
     * in the entire image at once, and
     * you need to specify only
     * transforms that can be controlled
     * with one of the PNG_TRANSFORM_*
     * bits (this presently excludes
     * dithering, filling, setting
     * background, and doing gamma
     * adjustment), then you can read the
     * entire image (including pixels)
     * into the info structure with this
     * call
     *
     * PNG_TRANSFORM_STRIP_16 |
     * PNG_TRANSFORM_PACKING  forces 8 bit
     * PNG_TRANSFORM_EXPAND forces to
     *  expand a palette into RGB
     */
    png_read_png(png_ptr, info_ptr,
				PNG_TRANSFORM_STRIP_16 | PNG_TRANSFORM_PACKING | PNG_TRANSFORM_EXPAND, NULL);
 
    *w = png_get_image_width(png_ptr, info_ptr);
    *h = png_get_image_height(png_ptr, info_ptr);
    
    unsigned int row_bytes = png_get_rowbytes(png_ptr, info_ptr);
    *outData = (unsigned char*) malloc(row_bytes * *h);
 
    png_bytepp row_pointers = png_get_rows(png_ptr, info_ptr);
 
    for (unsigned int i = 0; i < *h; i++) {
        // note that png is ordered top to
        // bottom, but OpenGL expect it bottom to top
        // so the order or swapped
        memcpy(*outData+(row_bytes * (*h-1-i)), row_pointers[i], row_bytes);
    }
 
    /* Clean up after the read,
     * and free any memory allocated */
    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
 
    /* Close the file */
    fclose(fp);
 
    /* That's it */
    return true;
#else
    wxLogMessage(_("PNG not supported. This function should not have been called."));
    return false;
#endif
}

/*! \brief Save the current render as a PNG
 * \param fname File name with path relative to the working directory
 * \param canvas Pointer to the application render target
 */
void ScreenshotPNG(wxString fname, wxGLCanvas *canvas)
{
#ifdef HAVE_PNG
  int w, h;
  canvas->GetClientSize(&w, &h);

  FILE* fp = fopen(fname, "wb");

  /* get the window pixel data */
  unsigned char *data = new unsigned char[w*h*4];
  glReadBuffer(GL_FRONT);
  glReadPixels(0, 0, w, h, GL_RGBA, GL_UNSIGNED_BYTE, data);

  png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING,
						NULL, NULL, NULL);
  if (png_ptr == NULL) {
    fclose(fp);
    return;
  }
  png_infop info_ptr = png_create_info_struct(png_ptr);
  if (info_ptr == NULL) {
    fclose(fp);
    png_destroy_write_struct(&png_ptr,  NULL);
    return;
  }

  /* configure png settings */
  png_init_io(png_ptr, fp);
  png_set_IHDR(png_ptr, info_ptr, w, h, 8,
      PNG_COLOR_TYPE_RGB_ALPHA, PNG_INTERLACE_NONE,
      PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
  png_write_info(png_ptr, info_ptr);

  /* describe pointers to row data */
  png_uint_32 k;
  png_bytep row_pointers[h];

  /* for OpenGL, y=0 is at the bottom and for a png file y=0 is at the top */
  for (k = 0; k < (unsigned int)h; k++)
    row_pointers[h - k - 1] = data + k*w*4;

  png_write_image(png_ptr, row_pointers);
  png_write_end(png_ptr, info_ptr);

  png_destroy_write_struct(&png_ptr, &info_ptr);
  fclose(fp);
  delete data;
#else
    wxLogMessage(_("PNG not supported. This function should not have been called."));
#endif //HAVE_PNG
}

/*! \brief Save the current render as a TGA
 * \param fname File name with path relative to the working directory
 * \param canvas Pointer to the application render target
 */
void ScreenshotTGA(wxString fname, wxGLCanvas *canvas)
{
  int w, h;
  canvas->GetClientSize(&w, &h);

  /* get the window pixel data */
  unsigned char *data = new unsigned char[w*h*4];
  glReadBuffer(GL_FRONT);
  glReadPixels(0, 0, w, h, GL_BGRA, GL_UNSIGNED_BYTE, data);

  int xa = w % 256;
  int xb = (w-xa)/256;

  int ya = h % 256;
  int yb = (h-ya)/256;

  /* assemble the header */
  unsigned char header[18]={0,0,2,0,0,0,0,0,0,0,0,0,
                            (unsigned char)xa,
                            (unsigned char)xb,
                            (unsigned char)ya,
                            (unsigned char)yb,32,0};

  /* write header and data to file */
  FILE* fp = fopen(fname, "wb");
  fwrite (header, 18*sizeof(unsigned char), 1, fp);
  fwrite (data, sizeof (unsigned char)*w*h*4, 1, fp);
  free(data);
  fclose(fp);
}

/*! \brief Video constructor
 * \param name File name with path relative to the working directory
 * \param x Width of the video frames (pixels)
 * \param y Height of the video frames (pixels)
 * \param capture If frames should be automatically added after a 
 * certain time has elapsed
 * \param load If the next HDF5 should be loaded after each frame is captured
 * \param time The time between automatic frame captures, when enabled
 */
Video::Video(wxString name, int x, int y, 
             bool capture, bool load, float time, unsigned int bps)
{
  fname = name;
  w = x;
  h = y;
  auto_record = capture;
  auto_advance = load;
  nframes = 0;
  delay = time;
  next_cap = time;

#ifdef HAVE_VIDEO
  avcodec_register_all();

  codec = avcodec_find_encoder(AV_CODEC_ID_MPEG1VIDEO);
  if (!codec) {
      fprintf(stderr, "codec not found\n");
      exit(1);
  }

  c = avcodec_alloc_context3(codec);
  picture = av_frame_alloc();

  c->width = w;
  c->height = h;
  c->bit_rate = bps;
  c->time_base = (AVRational){1 , 25};
  c->gop_size = 10; /* emit one intra frame every ten frames */
  c->max_b_frames =1;
  c->pix_fmt = AV_PIX_FMT_YUV420P;

  /* open it */
  if (avcodec_open2(c, codec, NULL) < 0) {
      fprintf(stderr, "could not open codec\n");
      exit(1);
  }

  f = fopen(name.mb_str(), "wb");
  if (!f) {
      fprintf(stderr, "could not open video file");
      exit(1);
  }

  ret = av_image_alloc(picture->data, picture->linesize, c->width, c->height, c->pix_fmt, 32);
  if (ret < 0) {
      fprintf(stderr, "could not alloc raw picture buffer\n");
      exit(1);
  }

  picture->format = c->pix_fmt;
  picture->width  = c->width;
  picture->height = c->height;
#else
  wxMessageBox(_("Video not supported. This function should not have been called."));
#endif
}

/*! \brief Record the current render as a video frame
 * \param canvas Pointer to the application render target
 */
void Video::encode_frame(wxGLCanvas* canvas)
{
#ifdef HAVE_VIDEO
  int w, h;
  canvas->GetClientSize(&w, &h);

  /* get the window pixel data */
  unsigned char *data = new unsigned char[w*h*4];
  glReadPixels(0, 0, w, h, GL_BGRA, GL_UNSIGNED_BYTE, data);

  unsigned char R, G, B;
  unsigned char Y, U, V;


  av_init_packet(&pkt);
  pkt.data = NULL;    // packet data will be allocated by the encoder
  pkt.size = 0;

  fflush(stdout);

  /* convert RGB to YUV 420 */
  for(int y = 0; y < c->height; y++)
  {
    for(int x = 0;x < c->width; x++)
    {
      R = data[((c->height - 1 - y) * c->width + x) * 4 + 0];
      G = data[((c->height - 1 - y) * c->width + x) * 4 + 1];
      B = data[((c->height - 1 - y) * c->width + x) * 4 + 2];
      
      Y = 0.257 * R + 0.504 * G + 0.098 * B + 16;
      U = 0.439 * R - 0.386 * G - 0.071 * B + 128;
      V = -.148 * R - 0.291 * G + 0.439 * B + 128;


      picture->data[0][y * picture->linesize[0] + x] = Y;

      if (x % 2 == 0 && y % 2 == 0)
      {
	    picture->data[1][y/2 * picture->linesize[1] + x/2] = U;
	    picture->data[2][y/2 * picture->linesize[2] + x/2] = V;
      }
    }
  }

  picture->pts = nframes;

  int got_output = 0; // not used
  ret = avcodec_encode_video2(c, &pkt, picture, &got_output);

  if (ret < 0) {
    fprintf(stderr, "error encoding frame\n");
    exit(1);
  }

  fwrite(pkt.data, 1, pkt.size, f);
  nframes++;
#else
  wxMessageBox(_("Video not supported. This function should not have been called."));
#endif
}

/*! \brief Compile all frames into a video and delete any allocated resources
 * \return
 */
void Video::close()
{
#ifdef HAVE_VIDEO
  uint8_t endcode[] = { 0, 0, 1, 0xb7 };
  fwrite(endcode, 1, sizeof(endcode), f);
  fclose(f);
  avcodec_close(c);
  av_free(c);
  av_freep(&picture->data[0]);
  //avcodec_free_frame(&picture); TODO: This call was removed because it caused linker errors on OS X. Omitting it probably leaks memory.
#else
  wxMessageBox(_("Video not supported. This function should not have been called."));
#endif
}
