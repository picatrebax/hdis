#ifndef DATA_H
#define DATA_H

#include "defs.hh"

#define GL_GLEXT_PROTOTYPES
#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glext.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#else
#include <GL/gl.h>
#include <GL/glext.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif

#include <wx/wx.h>
#include <wx/progdlg.h>

#include <time.h>
#include <stdlib.h>
#include <vector>
#include <list>
#include <map>
#include <algorithm>
#include <math.h>
using namespace std;

#include "utility.hh"
using namespace hdisvis_utils;

extern "C"
{
#define restrict
#include "box.h"
#include "ptriangle.h"
#include "atom.h"
#include "lpio.h"
#include "sph.h"
}



#define NVERTS(n) 60*( n == 1 || n == 2 ) + 6*( n == 3 )

/*! \brief Struct containing a vertex position and normal
 *
 * Since openGL does per-vertex lighting calculations, it is important
 * to specify a normal for each vertex.  This also allows for rendered
 * polygons that aren't flat-shaded, producing more convincing
 * representations of curved surfaces with triangles. This data is
 * stored as a struct rather than a Class to allow certain HDF5
 * allignment macros to work properly when writing vertex data to
 * file.
 */
typedef struct vertex_s
{
  float vx; /*!< \brief Position x coordinate */
  float vy; /*!< \brief Position y coordinate */
  float vz; /*!< \brief Position z coordinate */
  float nx; /*!< \brief Normal x coordinate */
  float ny; /*!< \brief Normal y coordinate */
  float nz; /*!< \brief Normal z coordinate */
} vertex_t;

/** @name Vertex creation functions
 */
//@{
vertex_t Vertex();
vertex_t Vertex(vec3d_t v);
vertex_t Vertex(vec3d_t v, vec3d_t n);
//@}

/*! \brief Contains base color, color variation, and transparency values
 *
 * Each group of atoms/springs/ptriangles has a base color and
 * transparency for elements of the group, as well as the maximum
 * color variation for each of the RGB channels.
 */
class Color
{
 public:
  Color();
  Color(float r, float g, float b, float r_var, 
        float g_var, float b_var, float a);

  float r;      /*!< \brief Base red value */
  float g;      /*!< \brief Base green value */
  float b;      /*!< \brief Base blue value */
  float r_var;  /*!< \brief Maximum red deviation */
  float g_var;  /*!< \brief Maximum green deviation */
  float b_var;  /*!< \brief Maximum blue deviation */
  float a;      /*!< \brief Alpha */
};

// ==========================================================================

/*! \brief h5 file data storage class.
 */
class Data
{
public:
  Data(double* redraw, double* sphere); 
  ~Data();
  
  // ----------
  
  sph_t* spheres{};
  unsigned int nSph;
  
  // ----------

  double* redraw_threshold; /*!< \brief Maximum distance an atom can
                               move before being redrawn */
  double* sphere_adjustment; /*!< \brief Epansion coefficient for
                                spheres drawn with the VBO */

  wxArrayString files; /*!< \brief Array of full file names, relative
                          to the working directory */
  unsigned int cur_file; /*!< \brief Index of the currently loaded
                            file in the files array */

  bool initialized; /*!< \brief Flag for if space for the arrays has
                       been allocated */
  bool contacts_current; /*!< \brief Flag for if the contact adjacency
                            list is up to date */

  GLuint* VBOids; /*!< \brief Array of the ids returned by
                     glGenBuffers */
  unsigned int nVBOs; /*!< \brief Number of VBOs */

  /** @name Atom data */
  //@{
  unsigned int nPs;                  /*!< \brief Number of atoms */

  unsigned int* prim_VBOs;           /*!< \brief Each prim's lookup index into VBOids */
  unsigned int* prim_bodies;         /*!< \brief Each prim's body index */
  unsigned int* prim_nverts;         /*!< \brief Index into prim_verts of each prim's first coordinates */
  unsigned int* prim_iverts;         /*!< \brief Index into prim_verts of each prim's first coordinates */
  unsigned int* prim_npoints;        /*!< \brief Each prim's number of vertices */
  unsigned int* prim_ipoints;        /*!< \brief Index into prim_verts of each prim's first coordinates */
  float* prim_r;                     /*!< \brief Each prim's dilation radius */
  vec3d_t* prim_points;              /*!< \brief points for each prim containing position vectors */
  vec3d_t* prim_VBO_points;
  unsigned int* prim_iclosest;
  //@}
  
  /** @name Body data */
  //@{
  unsigned int nBs;                  /*!< \brief Number of bodies */
  
  vec3d_t* body_speeds;              /*!< \brief Each body's velocity vector */
  vec3d_t* body_forces;              /*!< \brief Each body's force vector */
  vec3d_t* body_colors;              /*!< \brief Each body's randomly generated base color */
  vec3d_t* body_centers;             /*!< \brief Each body's center of mass */
  vector< pair<unsigned int, double> >* interbody_forces;

  //@}

  /** @name Spring and PTriangle data */
  //@{
  unsigned int nSs;                  /*!< \brief Number of springs*/
  unsigned int nTs;                  /*!< \brief Number of ptriangles */

  vertex_t* spring_verts;            /*!< \brief Two vertices for each spring */
  vertex_t* ptriangle_verts;         /*!< \brief Three vertices for each ptriangle */
  //@}

  /** @name Group data */
  //@{
  unsigned int nAGs;                 /*!< \brief Number of atom groups */
  unsigned int nSGs;                 /*!< \brief Number of spring groups */
  unsigned int nTGs;                 /*!< \brief Number of ptriangle groups */

  unsigned int* atom_groups;         /*!< \brief The names of each atom group */
  bool* atom_group_valid;            /*!< \brief flag for if the group has any atoms in it */
  bool* atom_group_color_by_force;	 /*!< \brief if the group should use force coloring or normal coloring */
  unsigned int* spring_groups;       /*!< \brief The names of each spring group */
  unsigned int* ptriangle_groups;    /*!< \brief The names of each ptriangle group */
  
  wxString* atom_group_strings;      /*!< \brief The names added in the namedgroups branch */

  Color* group_atom_colors;          /*!< \brief Random or config settable atom group color/variation/alpha */
  Color* group_spring_colors;        /*!< \brief Random or config settable spring group color/variation/alpha */
  Color* group_ptriangle_colors;     /*!< \brief Random or config settable ptriangle group color/variation/alpha */
  //@}

  vec3d_t box;                       /*!< \brief Bounding box far corner position */
  float cur_time;                    /*!< \brief Current time */
  float ts;                          /*!< \brief Timestep between files */

  void Clear();
  void LoadFile(unsigned int n);
  void ReadContacts();
  void Initialize(wxArrayString filenames, bool only_faces = false);
  wxString GetHdisvisFileName(wxString hdis_file_name);

  /** @name Vertex buffer object functions
   *
   * The main rendering method is to represent all atoms with
   * triangles, which can be drawn quickly with graphics card
   * acceleration using vbos.
   */
  //@{

  void GenerateVBOs();
  void RefreshVBOs();
  void UpdateVBOs();

  unsigned int FillVertsCoupi(vertex_t* verts, unsigned int vbo);
  unsigned int FillPrim(vertex_t* verts, int i);

  unsigned int AddSphere(float R, vec3d_t p0, vertex_t* start_vert);
  unsigned int AddCylinder(float R, vec3d_t p0, vec3d_t p1, vertex_t* start_vert);
  unsigned int AddTface(float R, vec3d_t p0, vec3d_t p1, vec3d_t p2, vertex_t* start_vert);
  //@}
};
#endif //DATA_H
