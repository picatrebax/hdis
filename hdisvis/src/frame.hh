#ifndef FRAME_H
#define FRAME_H

#include <wx/wx.h>
#include <wx/valnum.h>
#include <wx/timer.h>
#include <wx/clrpicker.h>

#include "defs.hh"

#include "hdisvis.hh"
#include "data.hh"
#include "vars.hh"
#include "model.hh"

/* circular dependency */
class wxhdisvis;

/*! \brief wxFrame subclass for added functionality
 *
 * wxFrame subclass with new event handling for closing windows and pointers to necessary
 * information. Also includes all of the event handlers for the different frames - most of
 * these call the processing functions in hdisvis.cc to prevent code duplication.
 */
class HdisvisFrame : public wxFrame
{
public:
    virtual ~HdisvisFrame();
  HdisvisFrame(int ID, wxString name, Data *pdata, Vars *pv, wxhdisvis *app, int style = wxDEFAULT_FRAME_STYLE);

  wxhdisvis *hdisvis;  /*!< \brief Pointer to hdisvis instance to access other windows/controls */
  Data *data;        /*!< \brief Pointer to h5 data (instead of needing hdisvis->data) */
  Vars *v;           /*!< \brief Pointer to global variables (instead of needing hdisvis->v) */

  wxTimer *timer;    /*!< \brief Pointer to timer - used by the video frame for auto-capture */

  /** @name Menu creation and initialization functions
   *
   * \brief Creates, populates, connects event handlers for, and adds menus to a menu bar
   * \param bar The target menu bar
   *
   * In OS X, menus are specific to individual frames so each HdisvisFrame must have it's own set of the same menus.
   */
  //@{
  void AddAllMenus(wxMenuBar* bar);
  void AddFileMenu(wxMenuBar* bar);
  void AddWindowsMenu(wxMenuBar* bar);
  void AddModelMenu(wxMenuBar* bar);
  void AddControlsMenu(wxMenuBar* bar);
  void AddHelpMenu(wxMenuBar* bar);
  //@}

  /** @name CloseEventHandler overloads
   *
   * \brief Specialze window close behaviors
   * \param event Caught CloseEvent
   *
   * It is generally unnecessary to destroy and recreate a HdisvisFrame, so the default
   * CloseEvent handlers (triggered by clicking the 'x' at the top of the window) are
   * replaced with custom event handlers.
   */
  //@{
  void Hide(wxCloseEvent &event);
  void KeepOpen(wxCloseEvent &event);
  void CloseModel(wxCloseEvent &event);
  void CloseVideo(wxCloseEvent &event);
  //@}

  /** @name Menu item event handlers
   *
   * \param event Caught MenuEvent
   */
  //@{
  void OnMenuFileOpenH5(wxCommandEvent &event);
  void OnMenuFileReloadH5(wxCommandEvent &event);
  void OnMenuFileOpenLua(wxCommandEvent &event);
  void OnMenuFileSaveLua(wxCommandEvent &event);
  void OnMenuFileResetVars(wxCommandEvent &event);
  void OnMenuFileScreenshot(wxCommandEvent &event);
  void OnMenuFileVideo(wxCommandEvent &event);
  void OnMenuFileAbout(wxCommandEvent &event);
  void OnMenuFileQuit(wxCommandEvent &event);
  void OnMenuFilePlay(wxCommandEvent &event);

  void OnMenuWindows(wxCommandEvent &event);
  
  void OnMenuModelDisplayNormal(wxCommandEvent &event);
  void OnMenuModelDisplaySpeed(wxCommandEvent &event);
  void OnMenuModelDisplayForce(wxCommandEvent &event);
  void OnMenuModelNext(wxCommandEvent &event);
  void OnMenuModelPrev(wxCommandEvent &event);

  void OnMenuModelClipToggle(wxCommandEvent &event);
  void OnMenuModelBox(wxCommandEvent &event);
  void OnMenuModelAxis(wxCommandEvent &event);
  void OnMenuModelTime(wxCommandEvent &event);
  
  void OnMenuModelInfo(wxCommandEvent &event);

  void OnMenuControlsZoomin(wxCommandEvent &event);
  void OnMenuControlsZoomout(wxCommandEvent &event);
  void OnMenuControlsArrowsRotate(wxCommandEvent &event);
  void OnMenuControlsArrowsTranslate(wxCommandEvent &event);
  void OnMenuControlsArrowsClip(wxCommandEvent &event);
  void OnMenuControlsAlign(wxCommandEvent &event);
  void OnMenuControlsResetrot(wxCommandEvent &event);
  void OnMenuControlsResettrans(wxCommandEvent &event);

  void OnMenuHelp(wxCommandEvent& event);
  //@}
  
  /** @name Video window event handlers
   *
   * \param event Caught event
   */
  //@{
  void OnVideoCap(wxCommandEvent &event);
  void OnVideoClose(wxCommandEvent &event);
  void OnVideoPause(wxCommandEvent &event);
  void OnVideoTimerTic(wxCommandEvent &event);
  //@}

  /** @name Config window event handlers
   *
   * \param event Caught event
   */
  //@{ 
  void OnSphereAdjustment(wxCommandEvent &event);
  void OnRedrawThreshold(wxCommandEvent &event);
  void OnRenderSwitch(wxCommandEvent &event);
  void OnModelW(wxCommandEvent &event);
  void OnModelH(wxCommandEvent &event);
  void OnZoomSpeed(wxCommandEvent &event);
  void OnClipSpeed(wxCommandEvent &event);
  void OnTransSpeed(wxCommandEvent &event);
  void OnRotSpeed(wxCommandEvent &event);
  void OnForceMax(wxCommandEvent &event);
  void OnForceMin(wxCommandEvent &event);
  void OnSpeedWidth(wxCommandEvent &event);
  void OnSpeedVecSize(wxCommandEvent &event);
  void OnSpeedBaseSize(wxCommandEvent &event);
  void OnSpeedMax(wxCommandEvent &event);
  void OnSpringWidth(wxCommandEvent &event);
  
  void OnControlsDisplaymode(wxCommandEvent &event);
  void OnControlsArrowmode(wxCommandEvent &event);
  void OnControlsRendermode(wxCommandEvent &event);

  void OnSprings(wxCommandEvent &event);
  void OnPtriangles(wxCommandEvent &event);
  void OnClip(wxCommandEvent &event);
  void OnBox(wxCommandEvent &event);
  void OnAxis(wxCommandEvent &event);
  //@}

  void OnModelFrameResized(wxSizeEvent &event);
  void InitializePropWidgets();
  void UpdatePropWidgets(int type, int index, bool set_choice = true);
  void TogglePropWidgets(int type, bool val);

  /** @name Color window event handlers
   *
   * \param event Caught event
   */
  //@{
  void OnGroupSelected(wxCommandEvent& event);
  void OnColorUpdated(wxColourPickerEvent& event);
  void OnRedSliderUpdated(wxScrollEvent& event);
  void OnGreenSliderUpdated(wxScrollEvent& event);
  void OnBlueSliderUpdated(wxScrollEvent& event);
  void OnAlphaSliderUpdated(wxScrollEvent& event);
	int GetGroupIndex();
  //@}

  /** @name Model window event handlers
   *
   * \param event Caught event
   */
  //@{
  void OnControlsFileslider(wxCommandEvent &event);
  void OnControlsAlign(wxCommandEvent &event);
  void OnControlsResettrans(wxCommandEvent &event);
  void OnControlsZoomin(wxCommandEvent &event);
  void OnControlsZoomout(wxCommandEvent &event);
  //@}

  /** @name HdisvisFrame widget creation and arrangement functions
   *
   * Instead of subclassing HdisvisFrame for each different window that is needed,
   * each different window is set up with a function that creates and connects
   * widgets and arranges them on the frame with sizers.
   */
  //@{
  bool ModelFrame();
  bool VideoFrame();
  bool ConfigFrame();
  bool CarFrame();
  bool LogFrame();
  //@}
};

#endif //FRAME_H
